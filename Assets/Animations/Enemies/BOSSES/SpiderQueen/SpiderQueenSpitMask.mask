%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: SpiderQueenSpitMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: root
    m_Weight: 0
  - m_Path: root/abdomen
    m_Weight: 0
  - m_Path: root/abdomen/sting
    m_Weight: 0
  - m_Path: root/left_front_leg1
    m_Weight: 0
  - m_Path: root/left_front_leg1/left_front_leg2
    m_Weight: 0
  - m_Path: root/left_front_leg1/left_front_leg2/left_front_leg3
    m_Weight: 0
  - m_Path: root/left_front_leg1/left_front_leg2/left_front_leg3/left_front_leg4
    m_Weight: 0
  - m_Path: root/left_rear_leg1
    m_Weight: 0
  - m_Path: root/left_rear_leg1/left_rear_leg2
    m_Weight: 0
  - m_Path: root/left_rear_leg1/left_rear_leg2/left_rear_leg3
    m_Weight: 0
  - m_Path: root/left_rear_leg1/left_rear_leg2/left_rear_leg3/left_rear_leg4
    m_Weight: 0
  - m_Path: root/right_front_leg1
    m_Weight: 0
  - m_Path: root/right_front_leg1/right_front_leg2
    m_Weight: 0
  - m_Path: root/right_front_leg1/right_front_leg2/right_front_leg3
    m_Weight: 0
  - m_Path: root/right_front_leg1/right_front_leg2/right_front_leg3/right_front_leg4
    m_Weight: 0
  - m_Path: root/right_rear_leg1
    m_Weight: 0
  - m_Path: root/right_rear_leg1/right_rear_leg2
    m_Weight: 0
  - m_Path: root/right_rear_leg1/right_rear_leg2/right_rear_leg3
    m_Weight: 0
  - m_Path: root/right_rear_leg1/right_rear_leg2/right_rear_leg3/right_rear_leg4
    m_Weight: 0
  - m_Path: root/shoulders
    m_Weight: 1
  - m_Path: root/shoulders/neck
    m_Weight: 1
  - m_Path: root/shoulders/neck/head
    m_Weight: 1
  - m_Path: root/shoulders/neck/head/mandible_down
    m_Weight: 1
  - m_Path: root/shoulders/neck/head/mandible_down/teeth_down
    m_Weight: 1
  - m_Path: root/shoulders/neck/head/mandible_up
    m_Weight: 1
  - m_Path: root/shoulders/neck/head/mandible_up/teeth_up
    m_Weight: 1
  - m_Path: spider_queen
    m_Weight: 0
  - m_Path: spider_queen/cuerpo
    m_Weight: 0
  - m_Path: spider_queen/ojo_1
    m_Weight: 0
  - m_Path: spider_queen/ojo_2
    m_Weight: 0
  - m_Path: spider_queen/ojo_3
    m_Weight: 0
  - m_Path: spider_queen/ojo_4
    m_Weight: 0
  - m_Path: spider_queen/ojo_5
    m_Weight: 0
  - m_Path: spider_queen/ojo_6
    m_Weight: 0
  - m_Path: spider_queen/ojo_7
    m_Weight: 0
  - m_Path: spider_queen/ojo_8
    m_Weight: 0
