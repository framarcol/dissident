%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: PlayerArmsMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Prota_Completa
    m_Weight: 0
  - m_Path: Prota_Completa/Prota_mochila_low
    m_Weight: 0
  - m_Path: Prota_rig_grp
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_cog_ctrl_frzGrp
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_cog_ctrl_frzGrp/Prota_cog_ctrl_frzGrp
      1
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_cog_ctrl_frzGrp/Prota_cog_ctrl_frzGrp
      1/Prota_cog_ctrl
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_cog_ctrl_frzGrp/Prota_cog_ctrl_frzGrp
      1/Prota_cog_ctrl/Prota_pistoleras_low2
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_cog_ctrl_frzGrp/Prota_cog_ctrl_frzGrp
      1/Prota_cog_ctrl/Prota_pistoleras_low5
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_spineHigh_ctrl_frzGrp
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_spineHigh_ctrl_frzGrp/Prota_spineHigh_ctrl_frzGrp
      1
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_spineHigh_ctrl_frzGrp/Prota_spineHigh_ctrl_frzGrp
      1/Prota_spineHigh_ctrl
    m_Weight: 0
  - m_Path: Prota_rig_grp/Prota_spine_rig_grp/Prota_spineHigh_ctrl_frzGrp/Prota_spineHigh_ctrl_frzGrp
      1/Prota_spineHigh_ctrl/Prota_mochila_low2
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/lf_armWeightJnt_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/lf_armWeightJnt_grp/Prota_lf_upArmWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/lf_armWeightJnt_grp/Prota_lf_upArmWeight_jnt/Prota_lf_elbowWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/lf_armWeightJnt_grp/Prota_lf_upArmWeight_jnt/Prota_lf_elbowWeight_jnt/Prota_lf_wristWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/lf_legWeightJnt_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/lf_legWeightJnt_grp/Prota_lf_upLegWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/lf_legWeightJnt_grp/Prota_lf_upLegWeight_jnt/Prota_lf_kneeWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/lf_legWeightJnt_grp/Prota_lf_upLegWeight_jnt/Prota_lf_kneeWeight_jnt/Prota_lf_ankleWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_lf_upLeg_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_lf_upLeg_jnt/Prota_lf_knee_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_lf_upLeg_jnt/Prota_lf_knee_jnt/Prota_lf_ankle_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_lf_upLeg_jnt/Prota_lf_knee_jnt/Prota_lf_ankle_jnt/Prota_lf_ball_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_lf_upLeg_jnt/Prota_lf_knee_jnt/Prota_lf_ankle_jnt/Prota_lf_ball_jnt/Prota_lf_toe_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_rt_upLeg_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_rt_upLeg_jnt/Prota_rt_knee_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_rt_upLeg_jnt/Prota_rt_knee_jnt/Prota_rt_ankle_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_rt_upLeg_jnt/Prota_rt_knee_jnt/Prota_rt_ankle_jnt/Prota_rt_ball_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_hip_jnt/Prota_rt_upLeg_jnt/Prota_rt_knee_jnt/Prota_rt_ankle_jnt/Prota_rt_ball_jnt/Prota_rt_toe_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt/Prota_lf_hand_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt/Prota_lf_hand_jnt/Prota_lf_index_a_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt/Prota_lf_hand_jnt/Prota_lf_index_a_jnt/Prota_lf_index_b_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt/Prota_lf_hand_jnt/Prota_lf_thumb_a_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_lf_clavicle_jnt/Prota_lf_upArm_jnt/Prota_lf_elbow_jnt/Prota_lf_wrist_jnt/Prota_lf_hand_jnt/Prota_lf_thumb_a_jnt/Prota_lf_thumb_b_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_neck_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_neck_jnt/Prota_head_a_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_neck_jnt/Prota_head_a_jnt/Prota_head_b_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt/Prota_rt_hand_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt/Prota_rt_hand_jnt/Prota_rt_index_a_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt/Prota_rt_hand_jnt/Prota_rt_index_a_jnt/Prota_rt_index_b_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt/Prota_rt_hand_jnt/Prota_rt_thumb_a_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/Prota_spine_low_jnt/Prota_spine_mid_jnt/Prota_spine_hi_jnt/Prota_spine_end_jnt/Prota_rt_clavicle_jnt/Prota_rt_upArm_jnt/Prota_rt_elbow_jnt/Prota_rt_wrist_jnt/Prota_rt_hand_jnt/Prota_rt_thumb_a_jnt/Prota_rt_thumb_b_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/rt_armWeightJnt_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/rt_armWeightJnt_grp/Prota_rt_upArmWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/rt_armWeightJnt_grp/Prota_rt_upArmWeight_jnt/Prota_rt_elbowWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/rt_armWeightJnt_grp/Prota_rt_upArmWeight_jnt/Prota_rt_elbowWeight_jnt/Prota_rt_wristWeight_jnt
    m_Weight: 1
  - m_Path: Prota_Skeleton_grp/rt_legWeightJnt_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/rt_legWeightJnt_grp/Prota_rt_upLegWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/rt_legWeightJnt_grp/Prota_rt_upLegWeight_jnt/Prota_rt_kneeWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/rt_legWeightJnt_grp/Prota_rt_upLegWeight_jnt/Prota_rt_kneeWeight_jnt/Prota_rt_ankleWeight_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/weightSpine_grp
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/weightSpine_grp/Prota_weightSpine_b_jnt
    m_Weight: 0
  - m_Path: Prota_Skeleton_grp/weightSpine_grp/Prota_weightSpine_c_jnt
    m_Weight: 0
