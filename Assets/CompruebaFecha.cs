﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompruebaFecha : MonoBehaviour {

   	void Start () {

        string num = System.DateTime.Now.ToShortDateString();

        if(PlayerPrefs.GetInt("Fecha" + num) != 1)
        {
            GetComponent<Button>().interactable = true;
            Color colorTexto = GetComponentInChildren<Text>().color;
            GetComponentInChildren<Text>().color = new Color(colorTexto.r, colorTexto.g, colorTexto.b, 1);
        }
        else
        {
            GetComponent<Button>().interactable = false;
            Color colorTexto = GetComponentInChildren<Text>().color;
            GetComponentInChildren<Text>().color = new Color(colorTexto.r, colorTexto.g, colorTexto.b, 0.7f);
        }

	}


}
