﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using System.Text.RegularExpressions;

public class BL_BuildPostProcess
{

	public static string appControllerFile = "UnityAppController.mm";
	// this matches (void)application:did..something..Notification..something... methods declaration
	private static string regexpForNotificationMethods = "-\\s?\\(void\\)application:\\(UIApplication\\s?\\*\\)application\\sdid.+RemoteNotification.+\\n?{[^-|#.+]+";
#if UNITY_IOS
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	{



        if (buildTarget == BuildTarget.iOS)
		{
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));

			string target = proj.TargetGuidByName("Unity-iPhone");

			proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");
			proj.AddBuildProperty (target, "HEADER_SEARCH_PATHS", "$(inherited)");
			proj.AddBuildProperty (target, "OTHER_CFLAGS", "$(inherited)");
	                proj.AddBuildProperty (target, "OTHER_LDFLAGS", "$(inherited)");           

			foreach (string podFilePath in PodFilePaths)
				CopyAndReplaceFile (podFilePath, Path.Combine (path, Path.GetFileName (podFilePath)));

			File.WriteAllText(projPath, proj.WriteToString());

			Debug.Log("Running Push Notification Entitlement Warning Remover...");

			// check if app controller file exists
			string classesDirectory = Path.Combine(path, "Classes");
			string pathToAppController = Path.Combine(classesDirectory, appControllerFile);

			string code = File.ReadAllText(path);
			string codeWithDeletedNotificationsMethod = Regex.Replace(code, regexpForNotificationMethods, "");

			File.WriteAllText(pathToAppController, codeWithDeletedNotificationsMethod);
			Debug.Log("Push Notification Entitlement Warning Remover Completed");
		}
	}
#endif
	#region Private methods

	internal static void CopyAndReplaceFile(string srcPath, string dstPath)
	{
		if (File.Exists(dstPath))
			File.Delete(dstPath);

		File.Copy(srcPath, dstPath);
	}

#endregion

#region Paths

	static string[] PodFilePaths {
		get {
			return new [] {
				Path.Combine (PodFolderPath, "Podfile"),
			};
		}
	}

	static string PodFolderPath {
		get {
			return Path.Combine (XCodeFilesFolderPath, "Pod/");
		}
	}

	static string XCodeFilesFolderPath {
		get {
			return Path.Combine (UnityProjectRootFolder, "XCodeFiles/");
		}
	}

	static string UnityProjectRootFolder {
		get {
			return ".";
		}
	}

#endregion
}