﻿
0
左右手设置
控制方式
BOTH
灵敏度
{
1
Welcome to Dissident (beta)! In this version of the game you have access to two worlds: Qualdrom (the city) and Yldoon (the desert). Each one has 15 levels. You don't have to complete Qualdrom to unlock Yldoon. Instead, feel free to jump from one world to the other at any moment. You still have to complete each level consecutively, though. Enjoy!
This is the in-game shop. Here you will be able to purchase new skins, weapons, skates and upgrades.
This is the collectible menu. Here you will be able to check your idol collection.
Here you will be able to check your friends highest scores and compare them with yours.
{
2
无网络连接
World
Friends
过关
挑战关卡
ME
POSITION
Complete the level with 
 idols and 
 points.
{
3
您必须先通过前一关才能玩这关。
您必须先完成教程才能玩这关。
{
4
PRESS on the glass door to SHOOT and break it.
TILT your device to the left to MOVE towards that direction.
TILT your device to the right to MOVE towards that direction.
PICK UP the HEALING CAPSULE/LIFE TANK to recover health.
SHAKE your device gently or PRESS the jump button to JUMP over obstacles.
Destroy the BANNERS to gain additional points!
PICK UP the AMMO CLIP to reload your weapon.
Beware the land-mines! DESTROY or AVOID them.
Shoot the enemies and other destroyable objects when the gunsight turns RED. You'll do a CRITICAL HIT that will deal double damage. You'll also save precious ammo.
PICK UP all IDOLS to unlock the stars of each level and obtain tasty rewards.
{
5
CHAIN multiple shots to increase your POINT MULTIPLIER. Avoid being damaged and missing the shot, or you'll lose it.
SHOOT the switches to interact with them!
SHOOT the WOODBOXES to discover what is inside!
{
6
Just like the banners, you can destroy NEON ADS to gain additional points.
{
7
注意雄蜂！ 有些雄蜂需要被射击多次才能被消灭， 其他被射击一次即会死亡。
{
8
要预防抑制物的作用， 因为它会使您在几秒钟内无法灵活地射击、 跳跃和移动。
{
9
继续
重新开始
主菜单
下一关
得分
最高分
过关
神像
{
10
POPPETRONIC
Every kid in Qualdrom yearns for a Poppetronic. The latest version of this electronic housepet adds basic physiological functions.
RED THUNDER
This scale model of the Red Thunder is the consolation prize for those who can’t afford spending the 50.000 Zentens the actual car costs.
SKULLCREST
Angus Topaz, renowned military designer, came up with these helmets, which are lighter and open-faced to make it easier to breathe with them.
WATCHMANIAC
Every soldier in the Imperial army has a tracking chip implanted to make it possible to find them with the Watchmaniac in case of uprising.
STRISCIA
Qualdrom orders the manufacture of the Striscia to the craftsmen of Veltto, one of their most ancient colonies, on top of mount Armai.
ENERGY LOTUS
A single one of these can power up a house for two months, but they are so scarce that families have to look for them on the black market.
DO-IT-ALL 3000
The wealthiest families are replacing their human assistants with the Do-It-All 3000, cheaper to support. Unemployment has never been higher.
CLOUDSURFER
Why flying over highways when you can rise and fly through the toxic gas clouds with complete freedom? Buy your own Cloudsurfer now!
COSMOS S7
The ideal device to play the most outstanding games, like SkatePunk and Rebel Dash. You can even make phone calls now!
THE EYE IN THE SKY
No more units of this infallible UAV were needed until today. Now the Empire has taken note of your audacity and will send to build more.
{
11
BAEL
Wisdom and patience. According to Bedouins, if you lose your way in a sandstorm, only Bael will send you safe and sound back to Yldoon.
BEELKEBU
Devilish power worshipped in Badoon, rival city to Yldoon before the arrival of the Empire. Its proselytes practice cannibalism.
DAGAN
Dagans are always carried by merchants of the Hamad desert to keep the thieves and outlaws away from their valuable load.
NEHISTAN
For generations, yldoonite women have placed their trust in Nehistan, god of immutable beauty, to stay attractive.
NARGEL
During an yldoonite’s last journey, Nargel will prevent the black sand of Beelkebu to blind their eyes and keep them away from Exu.
NISRAC
Every brave yldoonite warrior receives a Nisrac from the Great Shorma once they have killed their first son of Gebu.
QUILIAN
On full-moon nights, yldoonite grandmothers put a Quilian under their grandchildren’s bed to keep the spirits escaped from the Muhah off.
TUMAZ
Fortune will be your ally. You’ll never lose in gambling games as long as you are close to a Tumaz. You can keep them in your pocket or in a travel bag.
NAT
Nat will guarantee you a prolix lineage and a happy relationship with your couple. In order to increase their affection, each spouse must carry one during the marriage ritual.
GEBU
Symbol of power and wealth. To possess a Gebu is one of the ten requirements demanded by the Order to be proclaimed as a Great Shorma. These can only be obtained after defeating a giant Gebu.
{
12
UMIAK
At the end of the Great Exodus the Kaashites arrived in Myräkä by these big boats.
NANOOK
Long Fang Bears are respected by the Kaashites.  It’s only allowed to hunt them in cases of extreme need.
SEDNA
Scarcity of fish it’s believed to be caused by the Handless Goddess anger. When her hair is tangled shamans pray to calm her down.
QAILERTETANG
At the Kaashite temple’s entrance rise the majestic statues that represent the fauna spirit.
INUKSHUK
Stone piles that mark the roads or the better fishing lakes.  Children who are able to scape from the Ijiraq shall follow them to come back home.
IJIRAQ
"The one who hides". These shapeshifters can disguise them as a human to deceive and kidnap kaashite children.
MANJEK
An Ijiraq who seduced the beautiful Sedna. When the girl found his true nature out she preferred to leap into the frozen sea depths.
QELTOEK
Qailertetang’s sprouts. When spring comes everybody  tries to spot one of these elusive animals in the melting ice.
PUNWUSSA
The Fertility Goddess origin lies on the long ago meetings between the Kaashites and the Wulussa matriarchs during the Great Exodus.
THE WENDIGO
The greedy Conquerors dug too deep in the ice. The creature they awoken arrived in Myräkkä aeons ago.
{
13
BAEL
Wisdom and patience. According to Bedouins, if you lose your way in a sandstorm, only Bael will send you safe and sound back to Yldoon.
BEELKEBU
Devilish power worshipped in Badoon, rival city to Yldoon before the arrival of the Empire. Its proselytes practice cannibalism.
DAGAN
Dagans are always carried by merchants of the Hamad desert to keep the thieves and outlaws away from their valuable load.
NEHISTAN
For generations, yldoonite women have placed their trust in Nehistan, god of immutable beauty, to stay attractive.
NARGEL
During an yldoonite’s last journey, Nargel will prevent the black sand of Beelkebu to blind their eyes and keep them away from Exu.
NISRAC
Every brave yldoonite warrior receives a Nisrac from the Great Shorma once they have killed their first son of Gebu.
QUILIAN
On full-moon nights, yldoonite grandmothers put a Quilian under their grandchildren’s bed to keep the spirits escaped from the Muhah off.
TUMAZ
Fortune will be your ally. You’ll never lose in gambling games as long as you are close to a Tumaz. You can keep them in your pocket or in a travel bag.
NAT
Nat will guarantee you a prolix lineage and a happy relationship with your couple. In order to increase their affection, each spouse must carry one during the marriage ritual.
GEBU
Symbol of power and wealth. To possess a Gebu is one of the ten requirements demanded by the Order to be proclaimed as a Great Shorma. These can only be obtained after defeating a giant Gebu.
{
14
You can't add yourself as a friend.
You can't send more than 5 friend requests.
This user is already your friend.
请求已发送。
Pending approval request.
You haven't selected any user.
Request sent successfully.
Username not found.
This user has too many friend requests, please try again later.
Friend added successfully!
Friend deleted successfully.
Friend request denied successfully.
Are you sure you want to decline
friend request?
Are you sure you want to accept 
friend request?
Are you sure you want to delete
from your friend list?
{
15
语言
音频
设置
{
16
得分
神像
过关
{
17
www.agapornigames.com
This beta has been developed by Agaporni Games.
Thank you very much for playing! Should you find any issues or want to give us some feedback, please contact us:
info@agapornigames.com
Follow us for news and promotions! @AgaporniGames"
{
18
正在加载
{
19
正在更新
需要网络连接……
Please check your internet connection before starting the application
推荐WiFi……
The application may download additional content or updates, we recommend use WiFi connection
Don't show this again
下载时发生错误……
下载时发生错误，请稍候重试
{
20
您需要使用在线模式才能执行此操作。
高级模式已购买。
购买未完成.
Offline Mode
Online Mode
(Premium Only)
Game mode
您需要网络连接才能在线游戏。
您需要登录到Google Play账户才能玩，请重新输入以正确登录。
购买失败。 错误代码：
购买进程无法开始。
您已经是高级用户。
You must be logged in to a Game Center user to play, please enter again to access correctly.
恢复购买.
恢复已成功完成。
没有可恢复的购买。
产品恢复失败。
BUY
{
21
下载时发生错误
请稍候重试
确认？
最高分
Thank you!
好好享受您购买的产品！
HEALTH
增加您的最大生命值
AMMO
增加您的最大弹药容量
锁定直到
或
解锁
COMMING SOON
装备
您需要连接网络才能购买。
如果您玩离线游戏， 您将无法购买任何东西。
您需要升级为高级用户才能离线玩游戏。
尝试连接时发生错误。 请稍候再试。
Request
SEND
MY FRIENDS
排名
确定
否
是
正在加载...
您需要下载更多内容才能玩这个游戏世界。
您要马上下载吗？
以后再说
前往我们的商店购买
前往购买
没有广告
2个游戏世界
2套装备 + 50
离线模式
升级为高级版
您骑过恐龙吗？
解锁
试玩
解锁YLDOON世界
现在您可以骑恐龙了。
PREMIUM
您想前往冰冻的Myräkkä世界吗？
解锁Myräkkä世界
现在是助推时间！
请评价我们
恭喜！
您解锁了：
继续
前往
升级
您要升级您的最大生命值或弹药容量吗？
MYRÄKKÄ
world
从检查点继续
观看广告
friend
值
展示橱柜
您获得了10/10：
选择
{
22
您需要激活您的Google Play 账户才能浏览商店
您需要激活您的Google Play 账户才能购买高级模式
You need to activate your Google Play account in order to access the game rankings
You need to activate your Google Play account in order to access your friend list
您需要激活您的Game Center 账户才能浏览商店
您需要激活您的Game Center 账户才能购买高级模式
You need to activate your Game Center account in order to access the game rankings
You need to activate your Game Center account in order to access your friend list
您需要连接网络才能实现您的首次商店浏览
您需要连接网络才能实现您的首次游戏世界下载
您需要登录之后才能使用此功能
您需要更新游戏才能继续。 请前往Play Store更新应用程序。
您要马上下载吗？
{
23
提示
精确度！
仔细瞄准！
如果您使用食指来射击， 那么可以更精确地射击远处的地方和目标。
{
24
暴击
爆头！
轰鸣！
如果您在瞄准器变成红色时射击敌人， 您将完成暴击， 造成双倍伤害。 您还可以节约宝贵的弹药。
有些敌人需要被暴击才容易被消灭。
{
25
倍增乘数
一枪毙命！
限制多次射击，枪枪命中， 避免受到伤害， 进而提高您的得分倍增乘数。
不要忘了射击每面帝国旗帜！
{
26
星级系统
星星收集器！
每关都有三颗星星和三颗钻石。
收集六个神像和高分即可解锁每颗星星并赢得丰厚的奖品。
{
27
神像猎人
了解DISSIDENT世界的更多信息！
找到并收集所有神像。
前往展示橱柜以了解有关每种神像的信息。
{
28
活动和每日挑战
玩我们的每日挑战游戏！
在您接受激烈挑战的同时试用新武器和皮肤。
赢得不同的挑战以获得额外的钻石。
{
29
The city of Qualdrom.
The largest colony of the greatest civilization ever known.
They call it the shadeless empire. I just call it home.
I have always wondered, how do we manage to keep on evolving, if our homeland has run out of resources?
There must be some secrets hidden at the bottom of this glorious city.
When I turned eighteen, I joined a Conqueror Squad.
I needed to witness what was happening beyond the city walls.
Then I saw how we deal with the neighbouring countries...
Our 'admiration of foreign cultures'.
The 'promotion of fair trade'.
The 'respect for their beliefs'.
And the 'negotiations with those who disagree with us'.
Now the shadeless empire is about to know his first shadow.
I’m going to tear it all down.
Brick...
... by...
... brick.
{
30
射击即可捡起神像的次数。
暴击之后子弹弹起并摧毁另一个目标的概率。
暴击之后恢复生命值的概率。
暴击之后恢复弹药的概率。
暴击摧毁目标获得更多分数的概率。
{
31
登录Game Center上传得分、 保存购买、 使用其他设备访问以及解锁游戏成就。
您想登录到Game Center享受更多功能吗？
您想马上登录吗？（可选）
An error occurred when trying to connect to Game Center.
登录Google Play Games上传得分、 保存购买、 使用其他设备访问以及解锁游戏成就。
您想登录到Google Play Games享受更多功能吗？
An error occurred when trying to connect to Google Play Games.
{
32
您已逃脱！ 您现在身处令人晕眩的Qualdrom帝国大都市。 继续免费玩游戏， 决战到最后。
恭喜
{
33
射击!
等待...


