﻿
0
Hand
Control
BOTH
Sensitivity
{
1
Welcome to Dissident (beta)! In this version of the game you have access to two worlds: Qualdrom (the city) and Yldoon (the desert). Each one has 15 levels. You don't have to complete Qualdrom to unlock Yldoon. Instead, feel free to jump from one world to the other at any moment. You still have to complete each level consecutively, though. Enjoy!
This is the in-game shop. Here you will be able to purchase new skins, weapons, skates and upgrades.
This is the collectible menu. Here you will be able to check your idol collection.
Here you will be able to check your friends highest scores and compare them with yours.
{
2
No Internet Conecction!
World
Friends
Complete the level
Play Level
ME
POSITION
Complete the level with 
 idols and 
 points.
{
3
You must complete the previous level in order to play this level.
You must complete the tutorial before playing this level.
{
4
PRESS on the glass door to SHOOT and break it.
TILT your device to the left to MOVE towards that direction.
TILT your device to the right to MOVE towards that direction.
PICK UP the HEALING CAPSULE/LIFE TANK to recover health.
SHAKE your device gently or PRESS the jump button to JUMP over obstacles.
Destroy the BANNERS to gain additional points!
PICK UP the AMMO CLIP to reload your weapon.
Beware the land-mines! DESTROY or AVOID them.
Shoot the enemies and other destroyable objects when the gunsight turns RED. You'll do a CRITICAL HIT that will deal double damage. You'll also save precious ammo.
PICK UP all IDOLS to unlock the stars of each level and obtain tasty rewards.
{
5
CHAIN multiple shots to increase your POINT MULTIPLIER. Avoid being damaged and missing the shot, or you'll lose it.
SHOOT the switches to interact with them!
SHOOT the WOODBOXES to discover what is inside!
{
6
Just like the banners, you can destroy NEON ADS to gain additional points.
{
7
Watch out for the DRONES! Some need several shots to succumb, while others die from a single impact.
{
8
Anticipate the INHIBITOR effect because it will prevent you from shooting, jumping and moving with ease for a few seconds.
{
9
RESUME
RESTART
MAIN MENU
NEXT LEVEL
SCORE
MAX
COMPLETE LEVEL
IDOLS
{
10
POPPETRONIC
Every kid in Qualdrom yearns for a Poppetronic. The latest version of this electronic housepet adds basic physiological functions.
RED THUNDER
This scale model of the Red Thunder is the consolation prize for those who can’t afford spending the 50.000 Zentens the actual car costs.
SKULLCREST
Angus Topaz, renowned military designer, came up with these helmets, which are lighter and open-faced to make it easier to breathe with them.
WATCHMANIAC
Every soldier in the Imperial army has a tracking chip implanted to make it possible to find them with the Watchmaniac in case of uprising.
STRISCIA
Qualdrom orders the manufacture of the Striscia to the craftsmen of Veltto, one of their most ancient colonies, on top of mount Armai.
ENERGY LOTUS
A single one of these can power up a house for two months, but they are so scarce that families have to look for them on the black market.
DO-IT-ALL 3000
The wealthiest families are replacing their human assistants with the Do-It-All 3000, cheaper to support. Unemployment has never been higher.
CLOUDSURFER
Why flying over highways when you can rise and fly through the toxic gas clouds with complete freedom? Buy your own Cloudsurfer now!
COSMOS S7
The ideal device to play the most outstanding games, like SkatePunk and Rebel Dash. You can even make phone calls now!
THE EYE IN THE SKY
No more units of this infallible UAV were needed until today. Now the Empire has taken note of your audacity and will send to build more.
{
11
BAEL
Wisdom and patience. According to Bedouins, if you lose your way in a sandstorm, only Bael will send you safe and sound back to Yldoon.
BEELKEBU
Devilish power worshipped in Badoon, rival city to Yldoon before the arrival of the Empire. Its proselytes practice cannibalism.
DAGAN
Dagans are always carried by merchants of the Hamad desert to keep the thieves and outlaws away from their valuable load.
NEHISTAN
For generations, yldoonite women have placed their trust in Nehistan, god of immutable beauty, to stay attractive.
NARGEL
During an yldoonite’s last journey, Nargel will prevent the black sand of Beelkebu to blind their eyes and keep them away from Exu.
NISRAC
Every brave yldoonite warrior receives a Nisrac from the Great Shorma once they have killed their first son of Gebu.
QUILIAN
On full-moon nights, yldoonite grandmothers put a Quilian under their grandchildren’s bed to keep the spirits escaped from the Muhah off.
TUMAZ
Fortune will be your ally. You’ll never lose in gambling games as long as you are close to a Tumaz. You can keep them in your pocket or in a travel bag.
NAT
Nat will guarantee you a prolix lineage and a happy relationship with your couple. In order to increase their affection, each spouse must carry one during the marriage ritual.
GEBU
Symbol of power and wealth. To possess a Gebu is one of the ten requirements demanded by the Order to be proclaimed as a Great Shorma. These can only be obtained after defeating a giant Gebu.
{
12
UMIAK
At the end of the Great Exodus the Kaashites arrived in Myräkä by these big boats.
NANOOK
Long Fang Bears are respected by the Kaashites.  It’s only allowed to hunt them in cases of extreme need.
SEDNA
Scarcity of fish it’s believed to be caused by the Handless Goddess anger. When her hair is tangled shamans pray to calm her down.
QAILERTETANG
At the Kaashite temple’s entrance rise the majestic statues that represent the fauna spirit.
INUKSHUK
Stone piles that mark the roads or the better fishing lakes.  Children who are able to scape from the Ijiraq shall follow them to come back home.
IJIRAQ
"The one who hides". These shapeshifters can disguise them as a human to deceive and kidnap kaashite children.
MANJEK
An Ijiraq who seduced the beautiful Sedna. When the girl found his true nature out she preferred to leap into the frozen sea depths.
QELTOEK
Qailertetang’s sprouts. When spring comes everybody  tries to spot one of these elusive animals in the melting ice.
PUNWUSSA
The Fertility Goddess origin lies on the long ago meetings between the Kaashites and the Wulussa matriarchs during the Great Exodus.
THE WENDIGO
The greedy Conquerors dug too deep in the ice. The creature they awoken arrived in Myräkkä aeons ago.
{
13
BAEL
Wisdom and patience. According to Bedouins, if you lose your way in a sandstorm, only Bael will send you safe and sound back to Yldoon.
BEELKEBU
Devilish power worshipped in Badoon, rival city to Yldoon before the arrival of the Empire. Its proselytes practice cannibalism.
DAGAN
Dagans are always carried by merchants of the Hamad desert to keep the thieves and outlaws away from their valuable load.
NEHISTAN
For generations, yldoonite women have placed their trust in Nehistan, god of immutable beauty, to stay attractive.
NARGEL
During an yldoonite’s last journey, Nargel will prevent the black sand of Beelkebu to blind their eyes and keep them away from Exu.
NISRAC
Every brave yldoonite warrior receives a Nisrac from the Great Shorma once they have killed their first son of Gebu.
QUILIAN
On full-moon nights, yldoonite grandmothers put a Quilian under their grandchildren’s bed to keep the spirits escaped from the Muhah off.
TUMAZ
Fortune will be your ally. You’ll never lose in gambling games as long as you are close to a Tumaz. You can keep them in your pocket or in a travel bag.
NAT
Nat will guarantee you a prolix lineage and a happy relationship with your couple. In order to increase their affection, each spouse must carry one during the marriage ritual.
GEBU
Symbol of power and wealth. To possess a Gebu is one of the ten requirements demanded by the Order to be proclaimed as a Great Shorma. These can only be obtained after defeating a giant Gebu.
{
14
You can't add yourself as a friend.
You can't send more than 5 friend requests.
This user is already your friend.
The request has already been sent.
Pending approval request.
You haven't selected any user.
Request sent successfully.
Username not found.
This user has too many friend requests, please try again later.
Friend added successfully!
Friend deleted successfully.
Friend request denied successfully.
Are you sure you want to decline
friend request?
Are you sure you want to accept 
friend request?
Are you sure you want to delete
from your friend list?
{
15
LANGUAGE
AUDIO
SETTINGS
{
16
SCORE
IDOLS
COMPLETE LEVEL
{
17
www.agapornigames.com
This beta has been developed by Agaporni Games.
Thank you very much for playing! Should you find any issues or want to give us some feedback, please contact us:
info@agapornigames.com
Follow us for news and promotions! @AgaporniGames"
{
18
LOADING
{
19
UPDATING
Internet connection required...
Please check your internet connection before starting the application
WiFi recommended...
The application may download additional content or updates, we recommend use WiFi connection
Don't show this again
Error downloading...
An error ocurred while downloading, please try again later
{
20
You need to play in online mode to perform this action.
Premium Mode purchased.
Purchase not completed.
Offline Mode
Online Mode
(Premium Only)
Game mode
You need an Internet connection to play online.
You must be logged in to a Google Play user to play, please enter again to access correctly.
Purchase has failed. Error code:
Purchase process could not be started.
You are already a Premium user.
You must be logged in to a Game Center user to play, please enter again to access correctly.
RESTORE PURCHASES
Restoration done successfully.
There are no products to restore.
Failed to restore product.
BUY
{
21
NOT ENOUGH FUNDS. 
BUY SOME?
CONFIRM?
MAX
Thank you!
Enjoy your purchase!
HEALTH
Increase your max health 
AMMO
Increase your max ammo capacity
LOCKED UNTIL
OR
UNLOCKED
COMMING SOON
EQUIP
You need an internet connection in order to perform purchasing.
You cannot purchase anything as long as you are playing offline.
You need to upgrade Premium in order to play offline.
An error occurred when trying to access. Try later.
Request
SEND
MY FRIENDS
RANKING
OK
NO
YES
Loading...
You need to download more content to play this world
Would you like to download it now?
LATER
VISIT OUR SHOP IN ORDER TO BUY
GO SHOPPING
NO MORE ADS
2 WORLDS
2 SETS + 50
MODE OFFLINE
UPGRADE PREMIUM
Have you ever ride a laegoo?
UNLOCK
TRY
UNLOCK YLDOON
It's time to ride your laegoo!
PREMIUM
Would you like to visit frozen Mirakka?
UNLOCK MIRAKKA
It's booster time!
PLEASE, RATE US
CONGRATULATIONS!
YOU UNLOCKED:
CONTINUE
GO
UPGRADES
Would you like to upgrade your max health or ammo?
MYRÄKKÄ
world
CONTINUE FROM CHECKPOINT
WATCH AD
friend
VALUE
DISPLAY CABINET
YOU GOT 10/10:
CHOOSE
{
22
You need to activate your Google Play account in order to access the store
You need to activate your Google Play account in order to purchase the Premium mode
You need to activate your Google Play account in order to access the game rankings
You need to activate your Google Play account in order to access your friend list
You need to activate your Game Center account in order to access the store
You need to activate your Game Center account in order to purchase the Premium mode
You need to activate your Game Center account in order to access the game rankings
You need to activate your Game Center account in order to access your friend list
You need an Internect connection in order to access the store for the first time
You need an Internect connection in order to download the game world for the first time
You need to login before using this feature
You need to update the game before proceeding. Please, go to Play Store to update the APP.
Would you like to do it now?
{
23
TIPS
ACCURACY!
Aim carefully!
If you use your index finger to shoot, you’ll be more precise while shooting far enemies and targets.
{
24
CRITICAL HIT
HEADSHOT!
Boom!
If you shoot the enemies when the gunsight turns RED, you'll do a CRITICAL HIT that will deal double damage. You'll also save precious ammo.
Some enemies are only vulnerable to CRITICAL HITS.
{
25
MULTIPLIER
ONE SHOT, ONE KILL!
CHAIN multiple shots without missing and avoid being damaged to increase your POINT MULTIPLIER. 
Don’t forget to shoot every imperial banner!
{
26
STAR SYSTEM
STAR COLLECTOR!
There are three stars and three diamonds available in every level.
Get the six idols and a high score to unlock every star and earn tasty rewards.
{
27
IDOL HUNTER
LEARN MORE ABOUT DISSIDENT WORLD!
Recover all the idols and complete your collection.
Visit the display cabinet in order to consult each idol lore.
{
28
EVENTS AND DAILY CHALLENGES
PLAY OUR DAILY EVENT!
Try new weapons and skins while you beat intense challenges.
Beat different events to earn extra diamonds.
{
29
The city of Qualdrom.
The largest colony of the greatest civilization ever known.
They call it the shadeless empire. I just call it home.
I have always wondered, how do we manage to keep on evolving, if our homeland has run out of resources?
There must be some secrets hidden at the bottom of this glorious city.
When I turned eighteen, I joined a Conqueror Squad.
I needed to witness what was happening beyond the city walls.
Then I saw how we deal with the neighbouring countries...
Our 'admiration of foreign cultures'.
The 'promotion of fair trade'.
The 'respect for their beliefs'.
And the 'negotiations with those who disagree with us'.
Now the shadeless empire is about to know his first shadow.
I’m going to tear it all down.
Brick...
... by...
... brick.
{
30
Number of times you can pick up an idol by shooting it.
Chance of a bullet to ricochet and destroy another target after a critical hit.
Chance of recovering health after a critical hit.
Chance of recovering ammo after a critical hit.
Chance of getting more points from targets destroyed by critical hits.
{
31
Log into Game Center to upload your scores, save your purchases, access from other devices and unlock game achievements. You can log in later inside the configuration menú.
Would you like to log into Game Center in order to enjoy additional features?
Would you like to log in now? (Optional).
An error occurred when trying to connect to Game Center.
Log into Google Play Games to upload your scores, save your purchases, access from other devices and unlock game achievements. You can log in later inside the configuration menú.
Would you like to log into Google Play Games in order to enjoy additional features?
An error occurred when trying to connect to Google Play Games.
{
32
You have escaped! Now you're in Qualdrom, the dizzy imperial metropolis. Keep on playing for free and fight through it until the final battle.
CONGRATULATIONS
{
33
SHOOT!
WAIT...

