﻿
0
Hand
Steuerung
BEIDE
Empfindlichkeit
{
1
Willkommen zu Dissident (beta)! In dieser Version des Spiels haben Sie Zugang zu zwei Welten: Qualdrom (die Stadt) und Yldoon (die Wüste). Jeder hat 15 Levels. Sie müssen nicht Qualdrom beenden für Yldoon zu spielen. Stattdessen, fühlen Sie sich frei von einer Welt zum anderen zu jeder Zeit zu springen. Du musst immer noch jedes Level nacheinander durchführen. Genießen Sie!
Dies ist der In-Game Shop. Hier können Sie neue Skins, Waffen, Skates und Upgrades erwerben.
Dies ist das Sammelmenü. Hier können Sie Ihre Idol-Sammlung überprüfen.
Hier können Sie Ihre Freunde Highscore überprüfen und Vergleichen Sie sie mit Ihrem.
{
2
Keine Internetverbindung!
Welt
Freunde
Beende den Level
Spielen
ICH
POSITION
Beende den Level mit 
 Idols und 
 Punkte.
{
3
Sie müssen die vorherige Stufe abgeschlossen um dieses Level zu spielen.
Sie müssen das Tutorial abgeschlossen bevor Sie dieses Level spielen können.
{
4
DRÜCKEN SIE auf die geknackte Glastür zu SCHIESSEN und brechen sie.
NEIGEN Sie Ihr Gerät nach links, um in diese Richtung zu BEWEGEN.
NEIGEN Sie Ihr Gerät nach rechts, um in diese Richtung zu BEWEGEN.
Die HEILUNGSKAPSEL/LIFE TANK AUFNEHMEN, um die Gesundheit wiederherzustellen.
SCHÜTTELN Sie Ihr Gerät sanft oder DRÜCKEN Sie die Sprungtaste zu SPRINGEN über Hindernisse.
Zerstöre die BANNERS um zusätzliche Punkte zu gewinnen!
HOLEN Sie den MUNITION AUF, um deine Waffe neu zu laden.
Hütet euch vor den Landminen! ZERSTÖREN Sie sie oder VERMEIDEN Sie sie.
Schießen Sie die Feinde und andere zerstörbare Gegenstände, wenn die Fadenkreuz ROT dreht. Sie werden einen KRITISCHER SCHLAG tun, das wird den Schaden verdoppeln. Sie sparen auch wertvolle Munition.
HOLEN Sie alle IDOLS AUF zu entsperren die Sterne jeder Level und erhalten leckere Belohnungen.
{
5
KETTE mehrfache Schüsse, um Ihren PUNKTE MULTIPLIER zu vergrößern. Vermeiden Sie beschädigt und fehlt der Schuss, oder Sie werden es verlieren.
SCHIESSEN Sie die Schalter um mit ihnen zu interagieren!
SCHIESSEN die HOLZKÄSTEN zu entdecken was drin ist!
{
6
Genau wie die Banners können Sie die NEON WERBUNG zerstören um zusätzliche Punkte zu gewinnen.
{
7
Achten Sie auf die DRONES! Einige benötigen mehrere Schüsse zu erliegen, während andere sterben aus einer einzigen Schüss.
{
8
Erwarten Sie den INHIBITOR-Effekt weil es wird Sie verhindern zu Schießen, Springen und Bewegen mit Leichtigkeit für ein paar Sekunden.
{
9
FORTSETZEN
NEUSTART
HAUPTMENÜ
NEXT LEVEL
ERGEBNIS
MAX
KOMPLETTE LEVEL
IDOLS
{
10
POPPETRONIC
Alle Kinder von Qualdrom sehnen sich ein Poppetronic. Die neueste Version dieses elektronischen Haustier fügt grundlegende physiologische Funktionen.
RED THUNDER
Dieses Modell im Maßstab des Red Thunder es ist die bevorzugte Trost für diejenigen die nicht 50.000 Zentens ausgeben können was den tatsächlichen Auto kostet.
SKULLCREST
Der berühmte Militär Designer, Angus Topaz, erfindet diese Helme, leichter und mit dem Gesicht aufgedeckt so dass es leichter war zu atmen.
WATCHMANIAC
Jeder Soldat in der kaiserlichen Armee hat einen Lage Chip implantiert, für im Falle der Revolte, mit Watchmaniac, dem zu finden.
STRISCIA
Qualdrom befiehlt die Herstellung von Striscia zu dem Meister von Veltto, eine der ältesten Siedlungen, auf dem Berg Armai.
LOTUS ENERGIE
Nur ein einzige von den kann energie bieten für ein Haus für zwei Monate, aber sie sind so knapp, dass Familien müssen auf dem Schwarzmarkt suchen.
MACH-ES-ALLES  3000
Die reichste ersetzen ihre menschlichen Assistenten für den Do-it 3000, billiger. Die Arbeitslosigkeit hat Rekordzahlen erreicht.
CLOUDSURFER
Warum die Autobahnen überfliegen wenn Sie die Wolken und giftiges Gas steigen und überqueren können mit völliger Freiheit? Kaufen Sie jetzt ihr Cloudsurfer!
COSMOS S7
Das ideale Gerät um die herausragenden Spiele zu spielen, wie SkatePunk und Rebel Dash. Jetzt können Sie auch andere Leute anruffen!
DER AUGE IM HIMMEL
Bis heute war nicht mehr benötigt dieses unfehlbar VANT, aber das Reich hat von Ihrer Kenntnis genommen  und senden mehr bauen.
{
11
BAEL
Weisheit und Geduld. Die Beduinen zählen das wenn Sie in einem Sandsturm verloren gehen nur Bael kann Sie wieder gesund zurückkehren zu Yldoon.
BEELKEBU
Höllischen Macht verehrt von Badoon, Rival Stadt von Yldoon bevor der Ankunft des Empire. Seine Missionierung üben Kannibalismus.
DAGAN
Die Händler von der Hamad Wüste tragen immer einen Dagan zu halten Diebe und Banditen weg von ihrer wertvollen Fracht.
NEHISTAN
Seit Generationen, die yldoonite Frauen haben sich auf Nehistan verlassen, unveränderliche Gott der Schönheit, um attraktiv zu bleiben.
NARGEL
Auf der letzte Reise eines yldoonit, Nargel würd verhindern das den schwarzen Sand von Beelzebub Sie die Augen blenden und stoppen Sie vom Erreichen Exu.
NISRAC
Tapferen Yldoonite Krieger erhalten eine Nisrac von der Großen Shorma, sobald sie ihr erstes Kind von Gebu getötet haben.
QUILIAN
Auf Vollmondnächten stellen die yldoonite Omas ein Quilian unter dem Bett ihrer Enkeln zu Die Ausreißer Geister von Muhah zu vertreiben.
TUMAZ
Glück wird deine Verbündeter sein. Sie werden nie in Glücksspiel verlieren wenn Sie in der Nähe eines Tumaz sind. Sie können sie in deiner Taschen oder in Ihrem Reisetasche  halten.
NAT
Nat wird eine lange Linie und eine glückliche Beziehung mit Ihrem Partner zu gewährleisten. Um ihre Wirkung zu erhöhen, muss jeder Ehepartner einen Ritus der Ehe bieten.
GEBU
Symbol von Macht und Reichtum. Eine Gebu zu besitzen ist eine der zehn Anforderungen des Ordens ein yldoonite Große Shorma zu nennen. Sie können nur das im Kampf mit einem riesigen Gebu nach dem Sieg erhalten.
{
12
UMIAK
Am Ende des großen Exodus die kaashitas erreichten Myräkä in diesen großen Schiffen.
NANOOK
Die Bären mit Lange Fangs werden durch kaashitas respektiert. Es ist nur erlaubt den zu  jagen in Zeiten der Not.
SEDNA
Der Mangel an Fisch wird den Zorn der Göttin Ohne Hände zugeschrieben. Wenn ihr Haar ist wirr Schamanen singen Gebete um Sie zu befrieden.
QAILERTETANG
Am Eingang der kaashitas Tempel die majestätischen Statuen, die den Geist der Fauna repräsentieren, erhöhen Sie sich.
INUKSHUK
Steine Hügel die Straßen oder Fischereizonen zeigen. Kinder, die der Ijiraq geflohen haben, müssen Sie die folgen um nach Hause zurückzukehren.
IJIRAQ
“Wer auch immer versteckt”. Diese Wechselbalg können sich verkleiden als Mensch um die kaashitas Kinder zu täuschen und entführen.
MANJEK
Ein Ijiraq die einen schönen Sedna verführt. Als das junges Mädchen entdeckte ihre Natur, sie zog sich in den eisigen Tiefen des Ozeans zu werfen.
QELTOEK
Qailertetang Sprössling. Mit dem Aufkommen des Frühlings alle wollen eine dieser schweren fassbaren Tiere in dem schmelzenden Eis zu sehen.
PUNWUSSA
Göttin der Fruchtbarkeit hat seinen Ursprung in den Sitzungen der Kaashitas mit der Matriarchinnen von Wulussa während des Großen Exodus.
DER WENDIGO
Die gierigen Eroberer gruben zu tief im Eis. Das Lebewesen die Sie erwachten erreichte Myräkkä vor Äonen.
{
13
BAEL
Weisheit und Geduld. Die Beduinen zählen das wenn Sie in einem Sandsturm verloren gehen nur Bael kann Sie wieder gesund zurückkehren zu Yldoon.
BEELKEBU
Höllischen Macht verehrt von Badoon, Rival Stadt von Yldoon bevor der Ankunft des Empire. Seine Missionierung üben Kannibalismus.
DAGAN
Die Händler von der Hamad Wüste tragen immer einen Dagan zu halten Diebe und Banditen weg von ihrer wertvollen Fracht.
NEHISTAN
Seit Generationen, die yldoonite Frauen haben sich auf Nehistan verlassen, unveränderliche Gott der Schönheit, um attraktiv zu bleiben.
NARGEL
Auf der letzte Reise eines yldoonit, Nargel würd verhindern das den schwarzen Sand von Beelzebub Sie die Augen blenden und stoppen Sie vom Erreichen Exu.
NISRAC
Tapferen Yldoonite Krieger erhalten eine Nisrac von der Großen Shorma, sobald sie ihr erstes Kind von Gebu getötet haben.
QUILIAN
Auf Vollmondnächten stellen die yldoonite Omas ein Quilian unter dem Bett ihrer Enkeln zu Die Ausreißer Geister von Muhah zu vertreiben.
TUMAZ
Glück wird deine Verbündeter sein. Sie werden nie in Glücksspiel verlieren wenn Sie in der Nähe eines Tumaz sind. Sie können sie in deiner Taschen oder in Ihrem Reisetasche  halten.
NAT
Nat wird eine lange Linie und eine glückliche Beziehung mit Ihrem Partner zu gewährleisten. Um ihre Wirkung zu erhöhen, muss jeder Ehepartner einen Ritus der Ehe bieten.
GEBU
Symbol von Macht und Reichtum. Eine Gebu zu besitzen ist eine der zehn Anforderungen des Ordens ein yldoonite Große Shorma zu nennen. Sie können nur das im Kampf mit einem riesigen Gebu nach dem Sieg erhalten.
{
14
Sie können sie nicht selbst als Freund hinzufügen.
Sie können nicht mehr als 5 Anwendungen senden.
Diese Person ist bereits dein Freund.
Die Anfrage wurde bereits gesendet.
Die Anfrage anstehend zur Genehmigung.
Sie haben keine Benutzer ausgewählt.
Anfrage erfolgreich gesendet.
Benutzername wurde nicht gefunden.
Der Benutzer hat zu viele Anfragen, bitte versuchen Sie es später noch einmal.
Freund erfolgreich hinzugefügt.
Freund erfolgreich entfernt.
Freund erfolgreich abgelehnt.
Sind Sie sicher dass Sie die Freundschaftsanfrage
ablehnen?
Sind Sie sicher dass Sie die Freundschaftsanfrage
bestätigen?
Sind Sie sicher dass Sie nombre entfernen möchten
von Ihre Freundeliste?
{
15
SPRACHE
AUDIO
KONFIGURATION
{
16
ERGEBNIS
IDOLS
KOMPLETT LEVEL
{
17
www.agapornigames.com
Diese Beta wurde von Agaporni Games entwickelt.
Vielen Dank für Spielen! Sollten Sie irgendwelche Probleme finden oder möchten uns Feedback geben, bitte kontaktieren Sie uns:
info@agapornigames.com
Folgen Sie uns für Neuigkeiten und Promotionen! @AgaporniGames"
{
18
LADEN
{
19
AKTUALISIERUNG
Internetverbindung erforderlich...
Bitte überprüfen Sie Ihre Internetverbindung bevor Sie die Applikation starten
WiFi empfohlen...
Die Applikation kann zusätzliche Inhalte oder Aktualisierungen herunterladen, wir empfehlen die WiFi-Verbindung zu benutzen
Nicht mehr das wieder anzeigen
Fehler beim Herunterladen...
Beim Herunterladen ist ein Fehler aufgetreten, Bitte versuchen Sie es später wieder
{
20
Um dies zu tun, müssen Sie im Online-Modus zu spielen.
Erworbenen Premium-Modus.
Einkauf nicht ausgeführt.
Offline-Modus
Online-Modus
(Nur Premium)
Spielmodus
Um im Online-Modus zu spielen, erfordert Sie eine Internetverbindung.
Um zum spielen Sie müssen ein Nutzer von Google Play verbunden werden, tritt wieder um richtig einloggen.
Der Kaufprozess ist fehlgeschlagen. Fehlercode:
Konnte nicht den Kaufprozess zu starten.
Sie sind bereits ein Premium-User.
Um zum spielen Sie müssen ein Nutzer von Game Center verbunden werden, tritt wieder um richtig einloggen.
WIEDERHERSTELLEN EINKÄUFE
Restaurierung durch erfolgreich aus.
Es gibt keine Produkte wieder herzustellen.
Fehlgeschlagen das Produkt wieder herzustellen.
KAUFEN
{
21
NICHT GENUG FUNDS
WOLLEN SIE MEHR KAUFEN?
BESTÄTIGEN?
MAX
Vielen Dank!
Genießen Sie Ihren Kauf!
GESUNDHEIT
Erhöhe deine maximale Gesundheit
MUNITION
Erhöhe deine maximale Munitionskapazität
VERSCHLOSSEN BIS
ODER
UNVERSCHLOSSEN
DEMNÄCHST
AUSRÜSTEN
Sie benötigen eine Internetverbindung um den Kauf durchzuführen.
Sie können nichts kaufen solange Sie offline spielen.
Sie benötigen einen Premium Account um offline zu spielen.
Ein Fehler ist aufgetreten während dem Versuch der Zugang. Versuche es später.
Request
SENDEN
MEINE FREUNDE
RANKING
OK
NEIN
JA
Laden...
Sie müssen mehr Inhalte herunterladen um diese Welt zu spielen
Möchten Sie es jetzt herunterladen?
SPÄTER
BESUCHEN SIE UNSER SHOP UM ZU KAUFEN
EINKAUFEN GEHEN
AD-FREE
2 WELTEN
2 SETS + 50
MODE OFFLINE
AKTUALISIEREN PREMIUM
Hast du jemals in ein Laegoo gereitet?
UNVERSCHLOSS
PRÜFEN
UNVERSCHLOSS YLDOON
Es ist Zeit dein laegoo zu reiten!
PREMIUM
Möchten Sie die Gefrorene Miräkkä besuchen?
UNVERSCHLOSS MIRÄKKÄ
Es ist Booster Zeit!
SCHÄTZEN SIE UNS!
HERZLICHEN GLÜCKWUNSCH!
Sie haben UNVERSCHLOSSEN:
WEITER
GEHEN
VERBESSERUNG
Möchten Sie Ihre Gesundheit oder Munition zu erhöhen?
MYRÄKKÄ
Welt
WEITER VON CHECKPOINT
SEHEN WERBUNG
freunde
SCHÄTZEN
SALA ÍDOLOS
HAS CONSEGUIDO 10/10:
WÄHLEN
{
22
Um den Speicher zuzugreifen, müssen Sie aktivieren Ihr Google-Konto spielen.
Um die Premium-Version zu erwerben, müssen Sie aktivieren Ihr Google-Konto spielen.
Um die Rangliste des Spiels zuzugreifen, müssen Sie aktivieren Ihre Google-Konto spielen.
Deine Freundesliste zugreifen, müssen Sie aktivieren Ihre Google-Konto spielen.
Um den Speicher zuzugreifen, müssen Sie Ihr Game Center-Konto aktivieren.
Um die Premium-Version des Spiels zu erwerben, müssen Sie Ihr Game Center-Konto aktivieren.
Um die Rangliste des Spiels zuzugreifen, müssen Sie Ihr Game Center-Konto aktivieren.
Deine Freundesliste zuzugreifen, müssen Sie Ihr Game Center-Konto aktivieren.
Um den Laden zum ersten Mal besucht, müssen Sie eine Internetverbindung .
Um die Welt zum ersten Mal herunterladen, benötigen Sie eine Internetverbindung .
Um diese Funktion zu nutzen, müssen Sie angemeldet sein.
Bevor Sie fortfahren, müssen Sie das Spiel aktualisieren. Gehen Sie auf die Play Store die App zu aktualisieren.
Haben Sie jetzt tun?
{
23
TIPS
PRÄZISION!
Ziel sorgfältig!
Sie gewinnen Präzision in weit entfernten Ziele und Gegner zu erreichen wenn Sie Ihren Zeigefinger benutzen um zu schießen.
{
24
TRIGGER CRITIC
ZWISCHEN DEN AUGEN!
Zascas!
Wenn Sie den Schuss treffen im der kritische Moment Sie werden doppelten Schaden Ihre Feinde tun und Munition sparen.
Algunos enemigos sólo son vulnerables durante el momento crítico.
{
25
MULTIPLIER
EIN SCHUSS, EIN WENIGER!
Bespannen Sie Schüsse ohne zu fehlen und vermeiden Sie Schaden zu nehmen um Ihre Punkte-Multiplikator zu erhöhen.
Vergessen Sie nicht alle Plakate zu zerstören!
{
26
STERNE SYSTEM 
STERNE SAMMLER!
Auf jeder Level können Sie 3 Sterne und 3 Diamanten erreichen.
Erhalten Sie 6 Idol und erhalten Sie hohe Punktzahlen um alle Sterne zu entsperren und Belohnungen saftig zu bekommen.
{
27
IDOLE SAMMLER
KENNEN SIE DIE WELTEN VON ERIS!
Sammeln Sie alle Idols und Ihre Sammlung zu vervollständigen.
Besuchen Sie die Schaufenster um den Idols Informationen zu sehen.
{
28
EVENTS UND ZEITUNGEN HERAUSFORDERUNGEN
SPIELEN SIE UNSER TÄGLICH EVENT!
Conoce nuevas armas y trajes superando intensos desafíos y pon a prueba tus habilidades como aventurero disidente.
Exceeds unterschiedliche Herausforderungen um zusätzliche Diamanten zu bekommen.
{
29
Die Stadt von Qualdrom.
Die größte Kolonie von die größte Zivilisation jemals bekannt.
Sie nennen es “das Reich ohne Schatten”. Ich nenne es heimat.
Ich habe mich immer gefragt: wie wir weiter wachsen, wenn unsere Heimat alle seine Ressourcen ausgeschöpft haben.
Da muss ein Geheimnis unter den Tiefen dieser herrlichen Stadt versteckt sein.
Als ich achtzehn wurde, Ich trat ein Geschwader von Eroberern.
Ich hatte mit meinen eigenen Augen zu sehen was hinter den Mauern vorging.
Dann sah ich wie wir die Nachbarländer behandelt ...
Unsere “Bewunderung für fremde Kulturen”.
Unser “Förderung der faire Handel”.
Der “Respekt für ihren Glauben”.
Und wie wir verhandeln mit denen die mit uns nicht einverstanden wurden.
Jetzt, dieses Reich wird seinen ersten Schatten treffen.
Ich will dieses Stadt  niederschlagen.
Stein ...
... zu ...
... Stein.
{
30
Anzahl der Zeiten Sie ein Idol abholen können wenn Sie schießen.
Die Wahrscheinlichkeit dass eine Kugel abprallt und zerstört ein nahe gelegenes Ziel nach einer kritischen Treffer.
Die Wahrscheinlichkeit, Gesundheit nach einem kritischen Treffer zu erholen.
Die Wahrscheinlichkeit, Munition nach einem kritischen Treffer zu erholen.
Die Wahrscheinlichkeit, dass zerstörte Ziele durch kritische Schüsse, gibt mehr Punkte.
{
31
Eine Verbindung mit Game Center zum laden Sie Ihre Ergebnisse, speichern Sie Ihre Einkäufe, den Zugriff von anderen Geräten und entsperren Erfolge Spiel. Sie können später aus dem Menü Einstellungen verbinden.
Eine Verbindung mit Game Center, um zusätzliche Funktionen zu genießen?
Sie wollen jetzt verbinden? (Optional).
Beim Versuch, eine Verbindung zum Game Center herzustellen, ist ein Fehler aufgetreten.
Eine Verbindung mit Google Play Games zum laden Sie Ihre Ergebnisse, speichern Sie Ihre Einkäufe, den Zugriff von anderen Geräten und entsperren Erfolge Spiel. Sie können später aus dem Menü Einstellungen verbinden.
Eine Verbindung mit Google Play Games, um zusätzliche Funktionen zu genießen?
Beim Versuch, eine Verbindung zum Google Play Games herzustellen, ist ein Fehler aufgetreten.
{
32
Du bist entkommen! Jetzt bist du in Qualdrom, die schwindelerregende Kaisermetropole. Halten Sie kostenlos spielen.
GLÜCKWÜNSCHE
{
33
SCHIEßEN!
WARTE AB...
