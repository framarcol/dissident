﻿
0
Mão
Controle
Ambos
Sensibilidade
{
1
Bem-vindos a Dissident beta! Nesta versão do jogo pode ter acesso a dois mundos: Qualdrom (a cidade) e Yldoon (o deserto), cada um com 15 níveis. Não tem de completar Qualdrom para desbloquear Yldoon. Pelo contrário, sinta-se livre para saltar de um mundo para o outro a qualquer momento. Por sua vez terá de completar de consecutivamente cada nível para poder avançar. Divirta-se! 
Está na loja do jogo aqui poderá adquirir novas skins,armas,skates e upgrades.
Este é o menu de coleccionáveis. Aqui poderá verificar a sua colecção de ídolos.
Aqui você poderá ver a pontuação mais alta dos seus amigos e compará-las com as suas.
{
2
Sem ligação à Internet!
Mundo
Amigos
Você deve completar o nível
Jogar nível
ME
POSITION
Você deve completar o nível com 
 ídolos e
pontos
{
3
Você deve completar o nível anterior para poder jogar este nível.
Você deve concluir o tutorial antes de jogar este nível.
{
4
PRESSIONE a porta para atirar e quebrar o vidro.
INCLINE o seu dispositivo para a esquerda para avançar nessa direcção.
INCLINE o seu dispositivo para a direita para avançar nessa direcção.
PICK UP a CÁPSULA DE CURA / TANQUE DE VIDA para recuperar a saúde.
AGITE seu dispositivo suavemente ou pressione o botão de salto para SALTAR sobre obstáculos.
Destrua as BANDEIRAS para ganhar pontos adicionais!
PEGAI VÓS a MUNIÇÃO para recarregar sua arma.
Cuidado com as minas! DESTRUÍ-LOS ou EVITÁ-LOS.
Tira os inimigos e outros objetos destrutíveis quando a mira fica VERMELHO. Você vai fazer um ACERTO CRÍTICO que irá causar dano duplo. Além disso, você vai economizar munição preciosa.
PEGAI VÓS todos os ÍDOLOS para desbloquear as estrelas de cada nível e obter recompensas saborosas.
{
5
ENCADEIA vários tiros para aumentar o seu MULTIPLICADOR DE PONTOS. Evite ser danificados e faltando o tiro, ou você vai perdê-lo.
ATIRE BOTÕES para interagir com as portas!
ATIRAR as CAIXAS DE MADEIRA para descobrir o que está dentro!
{
6
Assim como as BANDEIRAS, você pode destruir os ANÚNCIOS DE NEON para ganhar pontos adicionais!
{
7
Cuidado com DRONES! Alguns drones precisar de vários tiros para morrer. Outros morrem de um único impacto.
{
8
DEVE ANTECIPAR o efeito do INIBIDOR ou perdem a capacidade de atirar, pular e se mover livremente por alguns segundos.
{
9
CONTINUAR
RECOMEÇAR
MENU PRINCIPAL
NEXT LEVEL
SCORE
MAX
NÍVEL CONCLUÍDO
ÍDOLOS
{
10
POPPETRONIC
Todas as crianças de Qualdrom querem um Poppetronic. A versão mais recente deste pet eletrônico adiciona funções fisiológicas básicas.
RED THUNDER
Este modelo em escala do Red Thunder é o substituto preferido para aqueles que não podem pagar os 50.000 Zentens que custa o carro.
SKULLCREST
O designer militar famoso, Angus Topaz, conseguiu esses capacetes, mais leve e com o rosto descoberto, para facilitar a respiração.
WATCHMANIAC
Cada soldado do Exército Imperial tem um chip implantado em sua cabeça. Em caso de insubordinação ou desobediência, eles serão encontrados com a Watchmaniac.
STRISCIA
Qualdrom ordena a fabricação de Striscia. Seus autores são os grandes artesãos de Veltto, um dos mais antigos assentamentos localizados no Monte Armai.
LOTUS ÉNERGÉTIQUE
A única cópia pode fornecer energia para uma casa durante dois meses. Eles são tão escassos que as famílias comprá-los no mercado negro.
ELE-FAZ-TUDO 3000
Os mais ricos estão substituindo seus assistentes humanos para o Ele-faz-tudo 3000, mais barato. O desemprego atingiu figuras históricas.
CLOUDSURFER
Por dirigir nas rodovias se você pode subir e voar sobre as nuvens de gás tóxico com total liberdade? Compre o seu Cloudsurfer!
COSMOS S7
O ideal para jogar os títulos mais famosos, como SkatePunk ou Rebel Dash. Agora você pode até chamar outras pessoas!
OLHO NO CÉU
Durante muitos anos, este tipo de unidades de UAV não são construídas. No entanto, o Império vai construir novas unidades para a sua fiabilidade em combate.
{
11
BAEL
Sabedoria e paciência. Segundo a narram as histórias de beduínos, se você se perde em uma tempestade de areia, Bael pode ajudá-lo a voltar para Yldoon.
BEELKEBU
Poder infernal adorado por Badoon, cidade rival de Yldoon antes da chegada do Império. Prosélitos praticam canibalismo.
DAGAN
Os comerciantes do deserto de Hamad, transportam sempre um Dagan para manter fora os ladrões e bandidos de sua carga valiosa.
NEHISTAN
Por gerações, as mulheres yldoonites têm contado com Nehistan, deus imutável de beleza, para ficar atraente.
NARGEL
Durante a última viagem de um yldoonite, Nargel cuidará de seus olhos, protegendo-os contra a areia preta de Beelkebu, e ajudará a você a chegar a Exu.
NISRAC
Os bravos guerreiros de Yldoon recebem uma Nisrac pelo Grande Shorma depois de terem matado seu primeiro Gebu.
QUILIAN
Durante as noites de lua cheia, os avós yldoonitas colocam um Quilian debaixo da cama de seus netos para afastar os espíritos fugitivos do inferno Muhah.
TUMAZ
Fortune será o seu aliado. Você nunca vai perder em jogos de azar se você estiver perto de um Tumaz. Você pode mantê-los em seus bolsos ou na sua mala de viagem.
NAT
Nat garante uma linhagem pura e um relacionamento feliz com seu parceiro. Para aumentar o seu efeito, cada cônjuge deve fornecer um Nat o rito de casamento.
GEBU
Símbolo de poder e riqueza. A posse de um Gebu é um dos dez requisitos da ordem de nomear um yldoonita como Grande Shorma. Este ídolo, só pode ser obtido após a vitória na batalha com um Gebu gigante.
{
12
UMIAK
No final do Grande Êxodo, o Kaashitas chegaram a Myräkä nestes navios maiores.
NANOOK
Ursos com longas presas são respeitados por Kaashitas. Eles são autorizados a caçá-los apenas em casos de necessidade.
SEDNA
A escassez de peixe é atribuído à ira da Deusa. Quando seu cabelo está emaranhada, xamãs entoam orações para alcançar a calma.
QAILERTETANG
Na entrada dos templos kaashitas, estátuas majestosas representar o espírito da fauna.
INUKSHUK
Cairns que indicam estradas ou áreas de pesca. Crianças que fogem do Ijiraq deve seguir para voltar para casa.
IJIRAQ
"Quem está escondido". Estes “mutáveis” podem disfarçar-se como um ser humano para enganar e raptar as crianças kaashitas.
MANJEK
Um Ijiraq tem seduzido a bela Sedna. Quando ela descobriu sua natureza ela se jogou para as profundezas do oceano de gelo.
QELTOEK
Rebento de Qailertetang. Com o advento da primavera todos querem ver um desses animais esquivos no gelo derretido.
PUNWUSSA
Deusa da fertilidade nasceu em encontros entre Kaashitas e matriarcas de Wulussa durante o Grande Êxodo.
O WENDIGO
Os conquistadores ávidos cavaram muito profundamente no gelo. O temido ser descoberto, veio a Myräkkä eras atrás.
{
13
BAEL
Sabedoria e paciência. Segundo a narram as histórias de beduínos, se você se perde em uma tempestade de areia, Bael pode ajudá-lo a voltar para Yldoon.
BEELKEBU
Poder infernal adorado por Badoon, cidade rival de Yldoon antes da chegada do Império. Prosélitos praticam canibalismo.
DAGAN
Os comerciantes do deserto de Hamad, transportam sempre um Dagan para manter fora os ladrões e bandidos de sua carga valiosa.
NEHISTAN
Por gerações, as mulheres yldoonites têm contado com Nehistan, deus imutável de beleza, para ficar atraente.
NARGEL
Durante a última viagem de um yldoonite, Nargel cuidará de seus olhos, protegendo-os contra a areia preta de Beelkebu, e ajudará a você a chegar a Exu.
NISRAC
Os bravos guerreiros de Yldoon recebem uma Nisrac pelo Grande Shorma depois de terem matado seu primeiro Gebu.
QUILIAN
Durante as noites de lua cheia, os avós yldoonitas colocam um Quilian debaixo da cama de seus netos para afastar os espíritos fugitivos do inferno Muhah.
TUMAZ
Fortune será o seu aliado. Você nunca vai perder em jogos de azar se você estiver perto de um Tumaz. Você pode mantê-los em seus bolsos ou na sua mala de viagem.
NAT
Nat garante uma linhagem pura e um relacionamento feliz com seu parceiro. Para aumentar o seu efeito, cada cônjuge deve fornecer um Nat o rito de casamento.
GEBU
Símbolo de poder e riqueza. A posse de um Gebu é um dos dez requisitos da ordem de nomear um yldoonita como Grande Shorma. Este ídolo, só pode ser obtido após a vitória na batalha com um Gebu gigante.
{
14
Você não pode adicionar a si mesmo como um amigo.
Você não pode enviar mais de 5 pedidos.
Esta pessoa já é seu amigo.
O pedido foi enviado.
Solicitar aguarda aprovação.
Não selecionou qualquer usuário.
Solicitação enviada com sucesso.
Nome de usuário não encontrado.
O usuário tem muitas solicitações, você pode tentar novamente mais tarde.
Amigos adicionado com sucesso.
Amigo excluído com sucesso.
Amigo rejeitado com sucesso.
Tem certeza de que deseja rejeitar o pedido de amizade
?
Tem certeza de que deseja confirmar o pedido de amizade
?
Tem certeza de que deseja excluir
sua lista de amigos?
{
15
IDIOMA
ÁUDIO
CONFIGURAÇÃO
{
16
PONTO
ÍDOLOS
NÍVEL COMPLETO
{
17
www.agapornigames.com
Esta versão beta foi desenvolvida pela Agaporni Games.
Muito obrigado por jogar! Se você encontrar algum problema ou quiser nos dar algum feedback, entre em contato conosco:
Info@agapornigames.com
Siga-nos para notícias e promoções! @AgaporniGames
{
18
CARGANDO
{
19
ACTUALIZANDO
Ligação à Internet necessária...
Verifique a sua ligação à Internet antes de iniciar a aplicação.
WiFi recomendado...
O aplicativo pode fazer download de conteúdo adicional ou atualizações, recomendamos usar a conexão WiFi.
Não mostrar novamente.
Erro ao fazer o download...
Ocorreu um erro durante o download, tente novamente mais tarde.
{
20
Para fazer isso, você precisa jogar no modo on-line.
Modo Premium adquiridos.
A compra não foi feita.
modo offline
modo online
(Apenas Premium)
Modo de jogo
Para jogar no modo on-line, você precisa de uma conexão com a Internet.
Para jogar, você precisa estar logado como um usuário do Google Play. Por favor tente novamente.
O processo de compra falhou. Código de erro:
Não foi possível iniciar o processo.
Você já é um usuário Premium.
Para jogar, você precisa estar logado como um usuário do Game Center. Por favor tente novamente.
RESTAURAR COMPRAS
Restauração realizada com sucesso.
Não há produtos a serem restaurados.
Falha ao restaurar o produto.
COMPRAR
{
21
FUNDOS INSUFICIENTES. 
COMPRAR ALGUNS?
CONFIRM?
MÁX
Obrigado!
Desfruta a sua compra!
SAÚDE
Aumenta a sua saúde máxima
MUNIÇAO
Aumenta a sua capacidade de muniçao máxima
BLOQUEADO ATÉ
OU
DESBLOQUEADO
EM BREVE
EQUIPAR
Você precisa de uma conexão com a Internet para realizar compras.
Você não pode comprar nada enquanto estiver jogando offline.
Você precisa atualizar o Premium para poder jogar offline.
Ocorreu um erro ao tentar acessar. Tente depois.
Request
ENVIAR
MEUS AMIGOS
RANKING
OK
NÃO
SIM
Carregando...
Você precisa fazer o download de mais conteúdo para jogar neste mundo
Deseja fazer o download agora?
MAIS TARDE
VISITE NOSSA LOJA PARA COMPRAR
VAI FAZER COMPRAS
NÃO MAIS ADS
2 MUNDOS
2 SETS + 50
MODO OFFLINE
UPGRADE PREMIUM
Alguma vez você já montou um Laegoo?
DESBLOQUEAR
EXPERIMENTAR
DESBLOQUEAR YLDOON
É hora de montar seu laegoo!
PREMIUM
Gostaria de visitar Myräkkä congelado?
DESBLOQUEAR MYRÄKKÄ
É booster tempo!
POR FAVOR, AVALIE-NOS
PARABÉNS!
VOCÊ DESBLOQUEOU:
CONTINUAR
IR
UPGRADES
Gostaria de atualizar sua saúde ou munição máxima?
MYRÄKKÄ
mundo
CONTINUE DESDE O CHECKPOINT
VER ANÚNCIO
friend
VALORAR
SALA ÍDOLOS
HAS CONSEGUIDO 10/10:
ESCOLHEI
{
22
Para acessar a loja precisa ativar sua conta Google Play.
Para comprar a versão premium do jogo precisa ativar sua conta Google Play.
Para acessar o ranking do jogo precisa ativar sua conta Google Play.
Para acessar sua lista de amigos precisa ativar sua conta Google Play.
Para acessar a loja precisa ativar sua conta Game Center.
Para comprar a versão premium do jogo precisa ativar sua conta Game Center.
Para acessar o ranking do jogo precisa ativar sua conta Game Center.
Para acessar sua lista de amigos precisa ativar sua conta Game Center.
Para acessar a loja pela primeira vez requer conexão com a internet.
Para transferir o mundo pela primeira vez, precisa de uma ligação à Internet.
Para usar este recurso é necessário fazer login.
Antes de prosseguir, é preciso atualizar o jogo. Vá para a Play Store para atualizar o APP.
Você gostaria de fazer agora?
{
23
CONSELHOS
PRECISÃO!
Apontai com cuidado!
Se você usa seu dedo indicador para atirar, você ganha precisão na realização dos objectivos e inimigos distantes.
{
24
TIRO CRÍTICO
ENTRE OS OLHOS!
Boom!
Se você acerta o tiro no momento crítico machucará seus inimigos e salvará munição.
Alguns inimigos só pode ser removido durante o momento crítico.
{
25
MULTIPLICADOR
UMA BALA, UMA VÍTIMA!
Você pode encadear tiros sem falhas e evitai tomar dano para aumentar o seu multiplicador de pontuação.
Não se esqueça de destruir todos os cartazes!
{
26
SISTEMA DE ESTRELA
ESTRELA DO COLETOR!
Em cada nível você pode alcançar 3 estrelas e 3 diamantes.
obtenha 6 ídolos e pontuações mais altas para desbloquear todas as estrelas e obter recompensas suculenta.
{
27
COLECIONADOR DE ÍDOLOS
VOCÊ SABE OS MUNDOS DE ERIS!
Recupere todos os ídolos e complete sua coleção.
Visite a vitrine para ver informações sobre cada ídolo.
{
28
EVENTOS E DESAFIOS
JOGUE O NOSSO EVENTO DIÁRIO!
Conheça novas armas e trajes por estes desafios intensos e teste suas habilidades.
Superar os vários desafios para obter diamantes extras.
{
29
Cidade Qualdrom.
O maior colônia da maior civilização já conheceu.
Eles chamam isso de "o império sem sombra". Para mim, é apenas a minha casa.
Eu sempre me perguntei: como podemos continuar a crescer, se a nossa pátria tenha esgotado todos os seus recursos?
Deve haver algum segredo escondido sob as profundezas desta gloriosa cidade.
Quando fiz dezoito anos, entrei para um esquadrão de Conquistadores.
Eu tinha que ver com meus próprios olhos o que estava acontecendo por trás das paredes.
Então eu vi como nós tratamos os países vizinhos ...
Nosso "admiração por culturas estrangeiras."
Nosso "promover o comércio justo".
O "respeito por suas crenças."
E como estávamos negociando com aqueles que não concordavam com a gente.
Agora, este império está prestes a fazer sua primeira sombra.
Eu acho que derrubar este lugar.
Pedra ...
... para ...
... pedra.
{
30
Número de vezes que você pode pegar um ídolo com um tiro.
Chance de ricochetear de uma bala e destruir outro alvo depois de um impacto crítico.
Chance de recuperar a saúde após um impacto crítico.
Chance de recuperar munição após um acerto crítico.
Probabilidade de que os alvos destruídos pelo tiro crítico deem mais pontuação.
{
31
Ligue para Game Center para fazer upload de pontuação, mantenha compras, acesso a partir de outros dispositivos e desbloquear conquistas. Você pode acessar mais tarde a partir do menu de configurações.
Ligue para Game Center para desfrutar de recursos adicionais?
Você pretende fazer agora? (Opcional).
Ocorreu um erro durante a conexão com sua conta do Game Center.
Ligue para Google Play Games para fazer upload de pontuação, mantenha compras, acesso a partir de outros dispositivos e desbloquear conquistas. Você pode acessar mais tarde a partir do menu de configurações.
Ligue para Google Play Games para desfrutar de recursos adicionais?
Ocorreu um erro durante a conexão com sua conta do Google Play Games.
{
32
Você conseguiu escapar. Agora você está em Qualdrom, a metrópole imperial vertiginosas. Mantenha a jogar de graça e luta até a batalha final.
PARABÉNS
{
33
ATIRAI!
ESPERAI...
