﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public static class BundleManager
{

	static readonly string assetBundleURL = "https://agapornigames.es/dodongo/AssetBundles/";
	static readonly string assetBundleURLBeta = "https://agapornigames.es/dodongo2/AssetBundles/";
#if UNITY_ANDROID
    static readonly string assetBundleVersionURL = "https://agapornigames.com/dodongo/checkBundleVersion2.php";    
    static readonly string assetBundleVersionURLBeta = "https://agapornigames.com/dodongo/checkBundleVersionBeta2.php";
#endif
#if UNITY_IOS
	static readonly string assetBundleVersionURL = "https://agapornigames.com/dodongo/checkBundleVersionGameCenter.php";
	static readonly string assetBundleVersionURLBeta = "https://agapornigames.com/dodongo/checkBundleVersionBeta2.php";
#endif
	public static List<WorldVersion> worldVersions = new List<WorldVersion>();
    public class WorldVersion
	{

		public string worldName;
		public uint version;

		public WorldVersion(string worldName, uint version)
		{
			this.worldName = worldName;
			this.version = version;
		}

	}

	public static Dictionary<string, BundleRef> cachedBundles = new Dictionary<string, BundleRef>();
	public class BundleRef
	{

		public AssetBundle bundle;
		public uint version;

		public BundleRef(AssetBundle bundle, uint version)
		{
			this.bundle = bundle;
			this.version = version;
		}

	}

	public static List<string> scenesList = new List<string>();
	public static List<string> specialScenesList = new List<string>();
	public static bool descargado;

	static WWW www;
	public static float progression
	{

		get
		{

			if (www == null) return 0f;

			return www.progress;

		}

	}
	public static bool downloadError = false;

	public static IEnumerator Initialize(string mundo)
	{

		//Get the most recent version numbers of the bundles from the server database
		yield return GetBundlesVersion();

		Debug.Log("Mundo seleccionado: " + mundo);

		//Load and/or cache every world bundle as seen from the server database
		int count = worldVersions.Count;
		for (int i = 0; i < count; i++)
		{

			WorldVersion worldVersion = worldVersions[i];

			if (worldVersions[i].worldName == mundo && worldVersions[i].worldName == "desert")
			{
				Debug.Log("PlayerPrefGuardado");
				PlayerPrefs.SetInt("desertCache", (int)worldVersions[i].version);
			}
			if (worldVersions[i].worldName == mundo && worldVersions[i].worldName == "snow")
			{
				Debug.Log("PlayerPrefGuardadoSnow");
				PlayerPrefs.SetInt("snowCache", (int)worldVersions[i].version);
			}

			if (mundo == worldVersions[i].worldName)
			{
				Debug.Log("Mundo entrado:" + mundo);
				yield return LoadWorldBundle(worldVersion.worldName, worldVersion.version);
			}

			//   if (downloadError) yield break;

		}

		//DEBUG
		/*for(int i = 0; i < scenePaths.Count; i++) {

		    Debug.Log(scenePaths[i]);

		}*/
		////

	}

	public static IEnumerator InitializeOffline(string mundo)
	{
		if (mundo == "desert")
		{
			yield return LoadWorldBundle(mundo, (uint)PlayerPrefs.GetInt("desertCache"));
		}
		else if (mundo == "snow")
		{
			yield return LoadWorldBundle(mundo, (uint)PlayerPrefs.GetInt("snowCache"));
		}


	}


	public static IEnumerator Initialize()
	{

		//Get the most recent version numbers of the bundles from the server database
		yield return GetBundlesVersion();


		//Load and/or cache every world bundle as seen from the server database
		int count = worldVersions.Count;
		for (int i = 0; i < count; i++)
		{

			WorldVersion worldVersion = worldVersions[i];

			if (worldVersion.worldName == "desert")
			{
				Debug.Log("PlayerPrefGuardado");
				PlayerPrefs.SetInt("desertCache", (int)worldVersions[i].version);
			}
			if (worldVersion.worldName == "snow")
			{
				PlayerPrefs.SetInt("snowCache", (int)worldVersions[i].version);
			}

			yield return LoadWorldBundle(worldVersion.worldName, worldVersion.version);

			//   if (downloadError) yield break;

		}

		//DEBUG
		/*for(int i = 0; i < scenePaths.Count; i++) {

		    Debug.Log(scenePaths[i]);

		}*/
		////

	}

	public static IEnumerator GetBundlesVersion()
	{

		//TODO
		//Populate 'worldVersions' list with data from the database
		WWW itemsData;
		string enlaceFinal;
		itemsData = new WWW("https://agapornigames.com/dodongo/checkBetaTester.php");

		yield return itemsData;
		if (itemsData.text == "0")
			enlaceFinal = assetBundleVersionURL;
		else
			enlaceFinal = assetBundleVersionURLBeta;

		using (www = new WWW(enlaceFinal))
		{

			yield return www;

			string versionData;

			if (!CargaIntro.modoOffline)
			{
				versionData = www.text;
			}
			else
			{
				versionData = "desert;6;\n";
			}

			string[] rows = versionData.Split(null);

			for (int i = 0; i < rows.Length; i++)
			{

				if (!string.IsNullOrEmpty(rows[i]))
				{

					string[] worldVersion = rows[i].Split(';');

					if (!string.IsNullOrEmpty(worldVersion[0]) && !string.IsNullOrEmpty(worldVersion[1]))
					{

						worldVersions.Add(new WorldVersion(worldVersion[0], uint.Parse(worldVersion[1])));

					}

				}

			}

		}


		//TEST
		//worldVersions = new List<WorldVersion> { new WorldVersion("city", 1), new WorldVersion("desert", 1) };
		////

	}

	//Get the actual version of the world
	static uint GetVersion(string world)
	{

		int count = worldVersions.Count;

		for (int i = 0; i < count; i++)
		{

			if (worldVersions[i].worldName == world)
			{
				return worldVersions[i].version;
			}


		}

		return 0;

	}

	//Check if the world bundle is already loaded
	static bool IsWorldBundleLoaded(string world, uint actualVersion)
	{

		//Check if the we got the bundle already
		if (cachedBundles.ContainsKey(world))
		{

			//Take the bundle ref
			BundleRef bundleRef;
			cachedBundles.TryGetValue(world, out bundleRef);

			//Check if the version is the last one or it is obsolete
			if (bundleRef.version == actualVersion)
			{

				return true;

			}

		}

		return false;

	}

	public static bool checkWorldCached(string mundo)
	{
		int versionFinal = 1;

		if (mundo == "desert")
		{
			versionFinal = PlayerPrefs.GetInt("desertCache");
		}

		if (mundo == "snow")
		{
			versionFinal = PlayerPrefs.GetInt("snowCache");
		}
#if UNITY_ANDROID
		if (Caching.IsVersionCached(assetBundleURL + "Android/scenes/" + mundo + ".sp", versionFinal))
		{
			return true;
		}
		else
		{
			return false;
		}
#elif UNITY_IOS
		if (Caching.IsVersionCached(assetBundleURL + "iOS/scenes/" + mundo + ".sp", versionFinal))
		{
			return true;
		}
		else
		{
			return false;
		}
#endif


	}

    //Load a world if it's not already loaded
    static IEnumerator LoadWorldBundle(string world, uint version = 0) {

        uint actualVersion = version;

        if (actualVersion == 0) {

            //Get the actual version of the world
            actualVersion = GetVersion(world);

        }
        
        if (IsWorldBundleLoaded(world, actualVersion)) yield break;

        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        WWW itemsData;

        string enlaceFinal;

        WWWForm datos = new WWWForm();
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
        
#endif


        itemsData = new WWW("https://agapornigames.com/dodongo/checkBetaTester.php", datos);

        yield return itemsData;
        if (itemsData.text == "0")
            enlaceFinal = assetBundleURL;
        else
            enlaceFinal = assetBundleURLBeta;

        if (world == "desert") actualVersion = (uint)PlayerPrefs.GetInt("desertCache");
        if (world == "snow") actualVersion = (uint)PlayerPrefs.GetInt("snowCache");

#if UNITY_ANDROID

        using (www = WWW.LoadFromCacheOrDownload(enlaceFinal + "Android/scenes/" + world + ".sp", (int)actualVersion)) {

#elif UNITY_IOS

        using (www = WWW.LoadFromCacheOrDownload(enlaceFinal + "iOS/scenes/" + world + ".sp", (int)actualVersion)) {

#endif
            while (!www.isDone)
            {
                ShopManager.porcentajeDescargado = ((int)(www.progress * 100)).ToString();
                yield return null;
            }

            yield return www;
            if (www.error != null) {
                downloadError = true;
                Debug.LogError("WWW download had an error: " + www.error);
                yield break;
            }

            AssetBundle bundle = www.assetBundle;

            if (bundle != null) {

                

                BundleRef bundleRef = new BundleRef(bundle, actualVersion);
                cachedBundles.Add(world, bundleRef);

                //Populate the scene paths list with all the scenes of the cached bundles
                string[] scenePaths = bundle.GetAllScenePaths();
                int count = scenePaths.Length;
                for(int i = 0; i < count; i++) {

                    string sceneName = Path.GetFileNameWithoutExtension(scenePaths[i]);

                    if (sceneName.Contains("_special")) {

                        specialScenesList.Add(sceneName);

                    } else {
                       // Debug.Log(sceneName);
                        scenesList.Add(sceneName);

                    }

                }

                //DEBUG
                Debug.Log("Loaded world " + world + ", version: " + actualVersion);

            }

        }
        www = null;

    }

    //Load a scene asynchronously from a loaded world bundle
    /*public static IEnumerator LoadSceneAsyncFromBundle(string world, string levelName) {

        yield return LoadWorldBundle(world);

        yield return SceneManager.LoadSceneAsync(levelName);

    }*/

}
