﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BundleBuilder {

    [MenuItem("Assets/Build Asset Bundles")]
    public static void BuildAssetBundles() {

        //AssetDatabase.GetDependencies()

        if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android) {

            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
            Debug.Log("Built AssetBundles Android");

        } else if(EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS) {

            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/iOS", BuildAssetBundleOptions.None, BuildTarget.iOS);

            Debug.Log("Built AssetBundles iOS");

        }

    }

}
