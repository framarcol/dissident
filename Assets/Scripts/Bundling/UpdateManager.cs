﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UpdateManager : MonoBehaviour {

    public Slider downloadProgressionBar;
    public Text downloadProgressionCount;

    public GameObject wifiRecommendedPopUp;
    public Toggle dontShowWifiRecommended;
    bool dontShowWifiOption = false;
    bool wifiRecommendedPopUpSkipped = false;

    public GameObject internetConnectionRequiredPopUp;
    bool hasInternetConnection = false;

    public GameObject downloadErrorPopUp;

    public bool updateCompleted = false;
    
    void Awake() {
        
        int dontShowWifiInt = PlayerPrefs.GetInt("DontShowWifiOption", 0);

        if (dontShowWifiInt != 0) dontShowWifiOption = true;

    }

	IEnumerator Start () {

        //TEST
        //Caching.CleanCache();
        ////

        //Show WiFi recommended connection pop up
        if (!dontShowWifiOption) {

            wifiRecommendedPopUp.SetActive(true);

            yield return new WaitUntil(() => { return wifiRecommendedPopUpSkipped; });

        }

        //Check internet connection, if not connected then show a message, and close the application
        if (!CargaIntro.modoOffline)
        {
            yield return CheckInternetConnection();
            if (!hasInternetConnection)
            {

                internetConnectionRequiredPopUp.SetActive(true);
                yield break;

            }
        } 

        Debug.Log("Updating...");

        //Start updating content, if there's any download error, display a pop up and then close the application
        yield return StartCoroutine(BundleManager.Initialize("desert"));
        if (BundleManager.downloadError) {

            downloadErrorPopUp.SetActive(true);
            yield break;

        }

        updateCompleted = true;

        Debug.Log("UPDATED");

        //Modify the progression bar to show loading...
        downloadProgressionCount.text = "LOADING...";

        //TEST
        //LoadSc.nextScene = 0;
        //yield return SceneManager.LoadSceneAsync("LoadScreen");
        ////

        //Load the main menu once the update is finished
        LoadSc.nextScene = -1;
        yield return SceneManager.LoadSceneAsync("LoadScreen");

        //TODO LIST
        //0 - Check internet connection and add timeout to the UpdaterScene download process
        //1 - Little API for managing scenes with the scene list of the BundleManager
        //2 - Integrate UpdaterScene with the game
        //3 - Set up level selection with the new scene system (FRAN)
        //4 - Check the skybox of desert levels
        //5 - Add a music controller for the manager to control it through PAUSE, GAME OVER and GAME COMPLETED screens (lowering volume at least)
        //4 - CHECK TEST STUFF, DELETE IT!

    }

    void Update() {

        if (updateCompleted || !hasInternetConnection || BundleManager.downloadError) return;
        
        if (downloadProgressionBar != null) {

            downloadProgressionCount.text = (BundleManager.cachedBundles.Count + 1) + "/" + BundleManager.worldVersions.Count;
            downloadProgressionBar.value = BundleManager.progression;

            //if (BundleManager.progression == 1f) downloadCountIndex++;
            
        }

    }

    public void SkipWifiRecommended() {

        if (dontShowWifiRecommended.isOn) {

            PlayerPrefs.SetInt("DontShowWifiOption", 1);

            PlayerPrefs.Save();

        }

        wifiRecommendedPopUp.SetActive(false);
        wifiRecommendedPopUpSkipped = true;

    }

    IEnumerator CheckInternetConnection() {

        WWW www = new WWW("https://agapornigames.es/dodongo2/");
        yield return www;
        if (www.error != null) {
            Debug.LogError(www.error);
            hasInternetConnection = false;
        } else {
            hasInternetConnection = true;
        }

    }

    public void CloseApplication() {

       Application.Quit();     

    }

}
