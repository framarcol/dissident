﻿using UnityEngine;
using System.Collections;

public class CameraFollowingController : MonoBehaviour {

    //Camera arm transform to be followed
    public Transform cameraArm;
    public CameraArmPositions targetPosition;

    Transform player;
    CameraMovementController cameraMovementController;
    Camera playerCamera;
    PlayerHealth playerHealth;

    //Necessary ref for the velocity
    //Vector3 velocity;

    //float refDistance;

    public float defaultSmoothTimePos = 6f; //0.15f;
    public float defaultSmoothMultiplierRot = 2f;

    float actualSmoothTimePos;

    public float smoothChangeMultiplier = 6f;
    
    public float shakeAmount = 0.5f;
    public float shakeDecreaseFactor = 0.75f;

    bool goLookBackwards = false;
    //float timeLookBackwards = 2f;
    float lookBackwardsTimer;
    float oldAngle;
    Vector3 relativeDistance;

    void Awake() {

        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();
        if (cameraArm == null) cameraArm = cameraMovementController.followingTarget;

    }

    void Start() {

        player = GameObject.FindGameObjectWithTag("Player").transform.parent;

        playerHealth = player.GetComponentInChildren<PlayerHealth>();
        playerHealth.OnPlayerDamaged += TriggerShake;

        playerCamera = GetComponentInChildren<Camera>();
        
        
        actualSmoothTimePos = defaultSmoothTimePos;

    }
    
    void Update() {

        actualSmoothTimePos = Mathf.Lerp(actualSmoothTimePos, defaultSmoothTimePos, smoothChangeMultiplier * Time.deltaTime);

    }

	void LateUpdate () {

        //Update the position and rotation of the camera interpolating to the followed transform

        //transform.position = Vector3.SmoothDamp(transform.position, cameraArm.position, ref velocity, defaultSmoothTimePos);

        /*if (targetPosition == CameraArmPositions.Forward) {

            transform.RotateAround(player.position, player.up, 180f);
            targetPosition = CameraArmPositions.Default;

        }else {*/


        if (goLookBackwards) {

            transform.position = (player.position + relativeDistance);

            lookBackwardsTimer += Time.deltaTime;

            if (lookBackwardsTimer >= smoothChangeMultiplier) {

                lookBackwardsTimer = smoothChangeMultiplier;

                goLookBackwards = false;

            }

            float perc = lookBackwardsTimer / smoothChangeMultiplier;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            float angle = perc * 180f;
            float deltaAngle = angle - oldAngle;
            oldAngle = angle;

            transform.RotateAround(player.position, player.up, deltaAngle);
            relativeDistance = transform.position - player.position;

            if (!goLookBackwards) {

                lookBackwardsTimer = 0f;
                oldAngle = 0f;

            }

        } else {

            transform.position = Vector3.Lerp(transform.position, cameraArm.position, actualSmoothTimePos * Time.deltaTime);
            if(!cameraMovementController.lookingPlayer) transform.rotation = Quaternion.Slerp(transform.rotation, cameraArm.rotation, defaultSmoothMultiplierRot * Time.deltaTime);
            else transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.position - transform.position), defaultSmoothMultiplierRot * Time.deltaTime);

        }

        playerCamera.fieldOfView = cameraMovementController.actualCameraFOV; //Mathf.Lerp(playerCamera.fieldOfView, cameraMovementController.actualCameraFOV, 1.5f * Time.deltaTime);
        
    }

    public void SetCameraArm(Transform newPos) {

        cameraArm = newPos;

    }

    public void SetCameraArm(Transform cameraArm, CameraArmPositions targetPosition) {

        this.cameraArm = cameraArm;
        //this.targetPosition = targetPosition;

        actualSmoothTimePos = 0f;

        if (targetPosition == CameraArmPositions.Forward) {

            //player.GetComponentInParent<PlayerMovementController>().canMove = false;

            if (this.targetPosition == CameraArmPositions.Default) {

                relativeDistance = transform.position - player.position;

                goLookBackwards = true;

            }

        }

        this.targetPosition = targetPosition;

    }

    public void TriggerShake(int lifes) {

        StartCoroutine(CameraShake());

    }

    IEnumerator CameraShake() {
        
        Vector3 startPos = playerCamera.transform.localPosition;

        Vector3 randomVector = new Vector3((Random.Range(0,2) == 0) ? 1f : -1f, 0, 0);
        
        while (randomVector.magnitude > 0.01f) {
            
            playerCamera.transform.localPosition = startPos + (randomVector * shakeAmount);
            
            randomVector = -(shakeDecreaseFactor * randomVector);

            yield return null;

        }

        playerCamera.transform.localPosition = startPos;
        
    }

}
