﻿using UnityEngine;
using System.Collections;

public class CameraMovementController : MonoBehaviour {

    public Transform followingTarget;

    //Can move state
    public bool canMove;

    //Rotation area data to perform the turns
    public RotationArea rotationArea;
    public bool rotatingCamera;
    /*float oldAngle = 0f;
    Vector3 oldOffset = Vector3.zero;
    float cameraRotationTimer = 0f;*/
    //
    public float radius;
    public float angleLeft;

    //
    //Rigidbody cameraRigidbody;

    float defaultDistance;
    float distanceToPlayer;
    float targetDistance;

    public float heightOffset = 0f;

    public bool lookingPlayer = false;

    //[HideInInspector]
    public float interpolationMultiplier = 6f;

    //public float boostToFit = 1.01f;

    public float rollDegrees = 30f;

    public float actualCameraFOV = 60f;
    public float targetCameraFOV;

    [HideInInspector]
    public Transform heightSpot;

    PlayerHealth playerHealth;

    GameManager gameManager;

    //Reference to the player movement script
    PlayerMovementController playerMovementController;

    // Use this for initialization
    void Start () {

        followingTarget = transform.Find("FollowingTarget");

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        playerHealth = playerMovementController.GetComponentInChildren<PlayerHealth>();

        distanceToPlayer = (playerMovementController.transform.position - transform.position).magnitude;
        defaultDistance = distanceToPlayer;
        targetDistance = distanceToPlayer;

        targetCameraFOV = actualCameraFOV;

        heightSpot = transform.GetChild(0);

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        GameManager.OnGameCompleted += FinishedGame;

    }
    
	void Update () {

        if (playerHealth.dead) return;

        //If the camera is rotating then do it and set the can move state to false
        if (rotatingCamera) {

            //canMove = false;
            RotateCamera();

            //if (rotationArea.axis == Axis.y) RollCamera();

        }

        if (canMove) {

            distanceToPlayer = Mathf.Lerp(distanceToPlayer, targetDistance, interpolationMultiplier * Time.deltaTime);

            Vector3 playerPosition;

            //if (playerMovementController.isStepJump || playerMovementController.falling) {

            playerPosition = playerMovementController.playerStuff.transform.localPosition;
            playerPosition.x = 0f;
            playerPosition.y = 0f;

            playerPosition = playerMovementController.transform.TransformPoint(playerPosition);


            //playerPosition.x = playerMovementController.transform.localPosition.x;

            /*} else {

                playerPosition = playerMovementController.transform.position;

            }*/

            transform.position = Vector3.Lerp(transform.position, (transform.position - playerPosition).normalized * distanceToPlayer + playerPosition, 50f * Time.deltaTime);

        }

        actualCameraFOV = Mathf.Lerp(actualCameraFOV, targetCameraFOV, interpolationMultiplier * Time.deltaTime);

    }

    //Function that keep rotating the camera through the Update
    public void RotateCamera() {

        float arcLength = playerMovementController.lerpedSpeed * Time.deltaTime;

        float degreesToRotate = Mathf.Rad2Deg * (arcLength / radius);

        if (angleLeft - degreesToRotate <= 0) {

            canMove = true;

            rotatingCamera = false;

            transform.RotateAround(rotationArea.pivot, rotationArea.actualAxis, angleLeft);

            transform.rotation = rotationArea.finalRotation;

            targetCameraFOV = 60f;

        } else {

            angleLeft -= degreesToRotate;

            transform.RotateAround(rotationArea.pivot, rotationArea.actualAxis, degreesToRotate);

        }

        /*//Update the rotation timer
        cameraRotationTimer += Time.deltaTime;

        //If the timer reach the limit then deactivate the rotation and clamp the timer
        if (cameraRotationTimer >= rotationArea.rotationTime) {
            
            rotatingCamera = false;

            cameraRotationTimer = rotationArea.rotationTime;

        }

        //Calculate the percentage of the timer consumed
        float perc = cameraRotationTimer / rotationArea.rotationTime;

        //Calculate the target angle lerping with the calculated percentage
        float targetAngle = Mathf.Lerp(0, rotationArea.rotationAngle, perc);

        //Calculate the delta angle for this frame, comparing with the previous frame angle
        float deltaAngle = targetAngle - oldAngle;
        oldAngle = targetAngle;

        //If the rotation axis is Z, the camera position must be updated too
        if (rotationArea.axis == Axis.z) {

            Vector3 targetOffset = (rotationArea.pivot - rotationArea.transform.position) * perc;

            Vector3 deltaOffset = targetOffset - oldOffset;
            oldOffset = targetOffset;

            transform.position += deltaOffset;

        }

        //Rotate the camera deltaAngle degrees using the pivot and axis of the associated rotation area
        transform.RotateAround(rotationArea.pivot, rotationArea.actualAxis, deltaAngle);

        //If the camera finished rotating, then reset the variables
        if (!rotatingCamera) {

            oldAngle = 0f;
            cameraRotationTimer = 0f;

            canMove = true;

        }*/

    }

    public void RollCamera() {
        
        if(rotationArea.axis == Axis.y) {

            if (rotationArea.turnDirection == TurnDirection.Positive) transform.Rotate(-transform.forward, rollDegrees, Space.World);
            else if (rotationArea.turnDirection == TurnDirection.Negative) transform.Rotate(transform.forward, rollDegrees, Space.World);

            //targetCameraFOV = 80f;

        }

    }

    public void ChangeForwardPosition(float distance) {

        targetDistance += distance;

    }
    
    public void ChangeSidePosition(float distance) {

        heightSpot.localPosition += new Vector3(distance, 0, 0);

    }

    public void ChangeHeight(float distance) {

        heightSpot.localPosition += new Vector3(0, distance, 0);

    }

    public void ChangeFOV(float fov) {

        interpolationMultiplier = 3f;
        targetCameraFOV = fov;

    }

    public void FinishedGame() {

        canMove = false;

    }

}
