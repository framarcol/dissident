﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CameraPropertiesController : MonoBehaviour {

    public GameObject fogPlane;

    public FogPresetList.Ambience selectedAmbience;

    public bool renderFog = false;

    FogPresetList fogPresetList;

    //GameObject fakeFog;
    
    void Start() {
        
        fogPresetList = Resources.Load("DataObjects/FogPreset", typeof(FogPresetList)) as FogPresetList;

        UpdateRenderFog();

    }



    // Update is called once per frame
    void Update () {

#if UNITY_EDITOR

        if (!Application.isPlaying) {

            UpdateRenderFog();

        }

#endif

        transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0,0,0), 5f * Time.deltaTime);

    }

    public void SetRenderFog(bool enable) {

        renderFog = enable;

        UpdateRenderFog();

    }

    private void UpdateRenderFog() {

        if (renderFog) {
            Shader.EnableKeyword("FOG_ON");

            for (int i = 0; i < fogPresetList.fogPresets.Length; i++) {

                if (fogPresetList.fogPresets[i].ambience == selectedAmbience)
                    Shader.SetGlobalColor("_FogColor", fogPresetList.fogPresets[i].fogColor);

            }

            if (fogPlane != null) fogPlane.SetActive(true);
        } else {
            Shader.DisableKeyword("FOG_ON");
            if (fogPlane != null) fogPlane.SetActive(false);
        }

    }

}
