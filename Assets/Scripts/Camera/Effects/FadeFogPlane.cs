﻿using UnityEngine;
using System.Collections;

public class FadeFogPlane : MonoBehaviour {

    public float fadeTime = 1.5f;

    GameObject fogPlane;

    MeshRenderer fogRenderer;

    bool entered = false;

	// Use this for initialization
	void Start () {

        fogPlane = GameObject.FindGameObjectWithTag("FogPlane");

        fogRenderer = fogPlane.GetComponent<MeshRenderer>();

	}

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if(fogRenderer.sharedMaterial.GetFloat("_GlobalFade") == 0f) {

                StartCoroutine(FadeFog(false));

            } else {

                StartCoroutine(FadeFog(true));

            }

        }

    }

    IEnumerator FadeFog(bool fadeIn) {

        float timer = 0f;

        while(timer < fadeTime) {

            timer += Time.deltaTime;

            float perc = timer / fadeTime;

            if(fadeIn) fogRenderer.sharedMaterial.SetFloat("_GlobalFade", (1f - perc));
            else fogRenderer.sharedMaterial.SetFloat("_GlobalFade", perc);

            yield return null;

        }

        if (fadeIn) fogRenderer.sharedMaterial.SetFloat("_GlobalFade", 0f);
        else fogRenderer.sharedMaterial.SetFloat("_GlobalFade", 1f);

    }

}
