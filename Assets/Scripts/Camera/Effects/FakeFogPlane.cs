﻿using UnityEngine;
using System.Collections;

public class FakeFogPlane : MonoBehaviour {
    
    float height;

    float farness;

    //Transform player;

    void Awake() {

        height = transform.position.y;

        farness = transform.localPosition.z;

    }

    //void Start() {

    //    player = GameObject.FindGameObjectWithTag("Player").transform;

    //}
    
	void LateUpdate () {

        //transform.position = new Vector3(transform.position.x, height, transform.position.z);
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, farness);

        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
        
	}

#if UNITY_EDITOR

    void OnApplicationQuit() {

        GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_GlobalFade", 0f);

    }

#endif

}
