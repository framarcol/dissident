﻿using UnityEngine;
using System.Collections.Generic;

public class FogPresetList : ScriptableObject {

    public enum Ambience { DESERT, CITY_0, CITY_1, CITY_2, CITY_3, CITY_4, CITY_5, SNOW, SNOW_CAVE, JUNGLE }

    [System.Serializable]
    public class FogPreset {
        
        public Ambience ambience;
        
        public Color fogColor;

    };
    
    public FogPreset[] fogPresets;
    
}
