﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class ChronoController : MonoBehaviour {

    public float initTime;
    public bool chronoEnabled;

    public GameObject inGameHUD;
    public ModuleUI countdownModule;
    public GameObject extraTime;
    public float extraTimeAnimationDuration;

    Queue<GameObject> extraTimeCounters = new Queue<GameObject>();

    float timer;
    float minutes, seconds, centiseconds;
    StringBuilder timeString;
    static readonly string separator = ":";

    float extraTimeDisplacement = 100f;

    Text time;
    PlayerHealth playerHealth;
    CanvasGroup inGameHUDCanvas;

	// Use this for initialization
	void Start () {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        time = GetComponentInChildren<Text>();

        timer = initTime;

        timeString = new StringBuilder(10);
        time.text = GetFormatTime(timer);

        inGameHUDCanvas = inGameHUD.GetComponent<CanvasGroup>();

        //Counters' pool
        for(int i = 0; i < 10; i++) {

            GameObject counter = (GameObject)Instantiate(extraTime, transform);

            extraTimeCounters.Enqueue(counter);

        }

	}
	
	// Update is called once per frame
	void Update () {

        if (chronoEnabled) {

            timer -= Time.deltaTime;

            if (timer <= 0) {

                time.text = "0:00:00";
                playerHealth.Damage(playerHealth.lifes);

                chronoEnabled = false;

            } else {

                if (timer <= 3f) time.color = Color.red;
                else if (timer <= 10f) time.color = Color.yellow;
                
                time.text = GetFormatTime(timer);

            }

        }

	}

    public void StartCountdown() {

        StartCoroutine(StartCountdownCoroutine());

    }

    IEnumerator StartCountdownCoroutine() {

        if (countdownModule == null) yield break;

        inGameHUDCanvas.interactable = false;
        countdownModule.gameObject.SetActive(true);

        countdownModule.OnAnimate();
        yield return new WaitUntil(countdownModule.Animated);

        countdownModule.gameObject.SetActive(false);
        inGameHUDCanvas.interactable = true;

        chronoEnabled = true;

    }


    string GetFormatTime(float timeInSeconds) {

        float secondsLeft = timeInSeconds;

        //Minutes
        minutes = Mathf.Floor(secondsLeft / 60f);
        secondsLeft -= minutes * 60f;

        //Seconds
        seconds = Mathf.Floor(secondsLeft);
        secondsLeft -= seconds;

        //Centiseconds
        centiseconds = Mathf.Floor(secondsLeft * 100f);

        sbyte minutesSmall = (sbyte)minutes;
        sbyte secondsSmall = (sbyte)seconds;
        sbyte centisecondsSmall = (sbyte)centiseconds;

        timeString.Length = 0;
        timeString.Append(minutes).Append(separator).Append(secondsSmall.ToString("D2")).Append(separator).Append(centisecondsSmall.ToString("D2")); //X:XX:XX format

        return timeString.ToString();

    }

    public void AddTime(float seconds) {

        timer += seconds;

        if (seconds != 0 && extraTimeCounters.Count > 0) {

            StartCoroutine(AnimateExtraTime(seconds));

        }

    }

    IEnumerator AnimateExtraTime(float seconds) {
        
        Text extraTimeText = extraTimeCounters.Dequeue().GetComponent<Text>();
        extraTimeText.gameObject.SetActive(true);
        
        Vector2 initPos, targetPos;
        float initAlpha, targetAlpha;

        if (seconds < 0) {

            extraTimeText.text = seconds.ToString();

            extraTimeText.color = new Color32(255, 0, 0, 255);
            extraTimeText.rectTransform.anchoredPosition = new Vector2(extraTimeText.rectTransform.anchoredPosition.x, 0);

            targetPos = new Vector2(extraTimeText.rectTransform.anchoredPosition.x, -extraTimeDisplacement);
            targetAlpha = 0f;

        } else if (seconds > 0) {

            extraTimeText.text = "+" + seconds.ToString();

            extraTimeText.color = new Color32(0, 255, 0, 255);
            extraTimeText.rectTransform.anchoredPosition = new Vector2(extraTimeText.rectTransform.anchoredPosition.x, -extraTimeDisplacement);

            targetPos = new Vector2(extraTimeText.rectTransform.anchoredPosition.x, 0);
            targetAlpha = 0f;

        } else { yield break; }

        initPos = extraTimeText.rectTransform.anchoredPosition;
        initAlpha = extraTimeText.color.a;

        Vector2 diffPos = targetPos - initPos;
        float diffAlpha = targetAlpha - initAlpha;

        float timer = 0f;
        while(timer <= extraTimeAnimationDuration) {

            timer += Time.deltaTime;

            float perc = timer / extraTimeAnimationDuration;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            extraTimeText.rectTransform.anchoredPosition = initPos + (perc * diffPos);
            extraTimeText.color = new Color(extraTimeText.color.r, extraTimeText.color.g, extraTimeText.color.b, initAlpha + (perc * diffAlpha));
            
            yield return null;

        }

        extraTimeText.gameObject.SetActive(false);
        extraTimeCounters.Enqueue(extraTimeText.gameObject);
        
    }
    
}
