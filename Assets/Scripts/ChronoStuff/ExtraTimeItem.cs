﻿using UnityEngine;
using System.Collections;

public class ExtraTimeItem : Pickup {

    public float extraSeconds = 5f;

    ChronoController chronoController;
    
	public override void Start () {

        base.Start();

        GameObject chrono = GameObject.FindGameObjectWithTag("ChronoController");
        if(chrono != null) chronoController = chrono.GetComponent<ChronoController>();
	
	}

    public override void OnTake(Collider playerCollider) {

        if(chronoController != null) chronoController.AddTime(extraSeconds);

    }

}
