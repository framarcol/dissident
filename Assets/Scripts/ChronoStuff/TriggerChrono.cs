﻿using UnityEngine;
using System.Collections;

public class TriggerChrono : MonoBehaviour {

    bool entered = false;

    ChronoController chronoController;
    
	void Start () {

        GameObject chrono = GameObject.FindGameObjectWithTag("ChronoController");
        if(chrono != null) chronoController = chrono.GetComponent<ChronoController>();

	}

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if(chronoController != null) chronoController.StartCountdown();

        }

    }

}
