﻿using UnityEngine;

[ExecuteInEditMode]
public class BillboardBehaviour : MonoBehaviour {

    Transform cameraTransform;

	// Use this for initialization
	void Start () {

        cameraTransform = Camera.main.transform;

	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(cameraTransform.position);

	}

}
