﻿using UnityEngine;
using System.Collections;

public class BoostZone : MonoBehaviour {

    public float boostTime = 3f;

    bool entered = false;

    PlayerMovementController playerMovementController;
    CameraMovementController cameraMovementController;

	// Use this for initialization
	void Start () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();

    }

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if(!playerMovementController.jumping)
                StartCoroutine(CalculateBoost());

        }

    }

    IEnumerator CalculateBoost() {

        playerMovementController.SetSpeed(30f);
        playerMovementController.playerAnimator.SetBool("boost", true);
        playerMovementController.skateAnimator.SetBool("boost", true);
        cameraMovementController.ChangeFOV(80f);

        yield return new WaitForSeconds(boostTime);

        playerMovementController.SetSpeed(20f);
        playerMovementController.playerAnimator.SetBool("boost", false);
        playerMovementController.skateAnimator.SetBool("boost", false);
        cameraMovementController.ChangeFOV(60f);

    }

}
