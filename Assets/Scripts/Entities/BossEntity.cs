﻿using UnityEngine;
using System.Collections;

public class BossEntity : Entity {

    public Entity boss;

	// Use this for initialization
	/*public override void Start () {
	
        Shot()

	}*/
	
	//// Update is called once per frame
	//void Update () {
	
	//}

    public override void Shot(ShootPackage shootPackage) {

        //Debug.Log(shootPackage.damage);

        if (isCritical) {

            shootPackage.damage *= 2;

            boss.Shot(shootPackage);

        }

    }

}
