﻿using UnityEngine;
using System.Collections.Generic;

public class BreakableEntity : Entity {

    public GameObject fullEntity;
    public GameObject brokenEntity;

    public float breakForce = 500f;

    public bool alreadyBroken = false;

    public bool damageWithCollision = true;

    List<Rigidbody> parts = new List<Rigidbody>();

    public override void Die() {

        if (alreadyBroken) {

            if (brokenEntity == null) return;

            Rigidbody[] pieces = brokenEntity.GetComponentsInChildren<Rigidbody>();

            if (pieces.Length <= 0) return;

            foreach (Rigidbody rb in pieces) {

                rb.transform.SetParent(null);

                rb.isKinematic = false;

                rb.AddExplosionForce(breakForce, impactPoint, 5f);

                parts.Add(rb);

            }

        } else {

            if (fullEntity == null || brokenEntity == null) return;

            fullEntity.SetActive(false);

            brokenEntity.SetActive(true);
            Rigidbody[] pieces = brokenEntity.GetComponentsInChildren<Rigidbody>();

            if (pieces.Length <= 0) return;

            foreach (Rigidbody rb in pieces) {

                rb.AddExplosionForce(breakForce, impactPoint, 5f);

            }

        }

        GetComponent<Collider>().enabled = false;

        Destroy(brokenEntity, 6f);

        if(parts.Count > 0) {

            foreach(Rigidbody part in parts) {

                Destroy(part.gameObject, 6f);

            }

        }

    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player" && damageWithCollision) {

            impactPoint = other.transform.position;
            Die();

            other.GetComponent<PlayerHealth>().Damage();

        }

    }

}
