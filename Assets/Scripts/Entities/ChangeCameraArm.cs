﻿using UnityEngine;
using System.Collections;

public enum CameraArmPositions { Default, Left, Right, Forward }

public class ChangeCameraArm : MonoBehaviour {

    public CameraArmPositions armPosition;

    [Range(1f, 10f)]
    public float smoothChangeMultiplier = 6f;

    PlayerMovementController playerMovementController;
    CameraMovementController cameraMovementController;
    CameraFollowingController cameraFollowingController;

    bool entered = false;

	// Use this for initialization
	void Start () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();
        cameraFollowingController = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<CameraFollowingController>();

	}

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {
            
            entered = true;

            PlayerMovementController pmc = other.GetComponentInParent<PlayerMovementController>();

            switch (armPosition) {

                case CameraArmPositions.Default:
                    cameraFollowingController.SetCameraArm(cameraMovementController.followingTarget, armPosition);
                    //if (playerMovementController.isRunning) cameraMovementController.ChangeForwardPosition(-1f);

                    if (!playerMovementController.isRunning) {
                        playerMovementController.canJump = true;
                        playerMovementController.playerAnimator.SetTrigger("unaimSide");
                    }
                    playerMovementController.SetInvertedControls(false);
                    break;
                case CameraArmPositions.Left:
                    if (!playerMovementController.isRunning) {
                        cameraFollowingController.SetCameraArm(pmc.cameraPositions.Find("LeftPos"), armPosition);
                        playerMovementController.playerAnimator.SetTrigger("aimRightSide");

                    } else cameraFollowingController.SetCameraArm(pmc.cameraPositions.Find("LeftPosRunning"), armPosition);

                    playerMovementController.canJump = false;
                    break;
                case CameraArmPositions.Right:
                    if (!playerMovementController.isRunning) {
                        cameraFollowingController.SetCameraArm(pmc.cameraPositions.Find("RightPos"), armPosition);
                        playerMovementController.playerAnimator.SetTrigger("aimLeftSide");
                    } else cameraFollowingController.SetCameraArm(pmc.cameraPositions.Find("RightPosRunning"), armPosition);

                    playerMovementController.canJump = false;
                    break;
                case CameraArmPositions.Forward:
                    if (playerMovementController.isRunning) break;

                    cameraFollowingController.SetCameraArm(pmc.cameraPositions.Find("ForwardPos"), armPosition);
                    playerMovementController.canJump = true;
                    playerMovementController.SetInvertedControls(true);
                    playerMovementController.playerAnimator.SetTrigger("unaimSide");
                    break;
                    
            }

            cameraFollowingController.smoothChangeMultiplier = smoothChangeMultiplier;

        }

    }

}
