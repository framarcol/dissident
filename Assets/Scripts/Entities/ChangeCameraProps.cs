﻿using UnityEngine;
using System.Collections;

public class ChangeCameraProps : MonoBehaviour {

    public float positionOffset;
    
    public float targetCameraFOV = 60f;

    [Range(1f, 10f)]
    public float interpolationMultiplier = 1f;

    public float sideOffset;

    public float heightOffset;

    public bool goLookPlayer = false;

    public bool playerCanTrigger = false;

    bool entered = false;

    CameraMovementController cameraMovementController;

    void Start() {

        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();

    }

    void OnTriggerEnter(Collider other) {

        if ((other.tag == "CameraArm" || (playerCanTrigger && other.tag == "Player")) && !entered) {

            entered = true;

            cameraMovementController.ChangeForwardPosition(positionOffset);
            cameraMovementController.targetCameraFOV = targetCameraFOV;

            cameraMovementController.ChangeSidePosition(sideOffset);
            cameraMovementController.ChangeHeight(heightOffset);

            cameraMovementController.lookingPlayer = goLookPlayer;

            cameraMovementController.interpolationMultiplier = interpolationMultiplier;

            

        }

    }

}
