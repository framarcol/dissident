﻿using UnityEngine;
using System.Collections;

public class ChangeSpeedArea : MonoBehaviour {

    public float targetSpeed = 20f;

	void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            other.GetComponentInParent<PlayerMovementController>().SetSpeed(targetSpeed);
        }

    }

}
