﻿using UnityEngine;
using System.Collections;

public class CityAd : Entity {

    public GameObject fullAd;
    public GameObject brokenAd;

    public float force = 500f;

    public override void Die() {

        fullAd.SetActive(false);

        brokenAd.SetActive(true);
        Rigidbody[] pieces = brokenAd.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rb in pieces) {

            rb.AddExplosionForce(force, impactPoint, 5f);

        }

        GetComponent<Collider>().enabled = false;

        Destroy(brokenAd, 3f);

    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            impactPoint = other.transform.position;
            Die();

            other.GetComponent<PlayerHealth>().Damage();

        }

    }

}
