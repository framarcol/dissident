﻿using UnityEngine;

public class CityTurret : Entity {

    public bool standingTurret = false;

    public bool left;
    public bool right;

    public GameObject turret;
    public GameObject halos;

    public GameObject explosionEffect;
    public GameObject smokeEffect;

    Quaternion initRotation;

    bool detection = false;
    Quaternion targetRotation;

    bool dead = false;

    Transform player;

	// Use this for initialization
	public override void Start () {

        base.Start();

        initRotation = turret.transform.rotation;

	}
	
	// Update is called once per frame
	void Update () {

        if (dead) {

            if(!standingTurret) turret.transform.rotation = Quaternion.RotateTowards(turret.transform.rotation, initRotation, 200f * Time.deltaTime);

        } else if (detection) {

            if (standingTurret) {

                Vector3 targetLook = new Vector3(player.position.x, turret.transform.position.y, player.position.z) - turret.transform.position;

                turret.transform.rotation = Quaternion.RotateTowards(turret.transform.rotation, Quaternion.LookRotation(targetLook), 100f * Time.deltaTime);

            } else {

                turret.transform.rotation = Quaternion.RotateTowards(turret.transform.rotation, targetRotation, 100f * Time.deltaTime);

            }
            
        }
        
	}

    public void Detection() {

        detection = true;

        halos.SetActive(true);

        if (standingTurret) player = GameObject.FindGameObjectWithTag("Player").transform;
        else {

            if (left) targetRotation = Quaternion.LookRotation(turret.transform.right, turret.transform.up);
            else if (right) targetRotation = Quaternion.LookRotation(-turret.transform.right, turret.transform.up);

        }

    }

    public void Disable() {

        enabled = false;

    }

    public override void Die() {

        dead = true;

        transform.parent.GetComponentInChildren<Damage>().gameObject.SetActive(false);

        explosionEffect.SetActive(true);
        smokeEffect.SetActive(true);
        
        halos.SetActive(false);
        
    }

}
