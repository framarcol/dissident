﻿using UnityEngine;
using System.Collections;

public class ContainerHook : MonoBehaviour {

    public Rigidbody container;

    Entity triggerSwitch;

    void Start() {

        triggerSwitch = GetComponentInChildren<Entity>();

    }

	void Update() {

        if(triggerSwitch != null && triggerSwitch.lifes <= 0) {

            container.isKinematic = false;

            Destroy(container.gameObject, 4f);

            enabled = false;

        }

    }

}
