﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ElectricWire : MonoBehaviour {

    [Range(0f, 180f)]
    public float startAngle = 90f;

    Transform baseJoint;
    
	// Use this for initialization
	void Start () {

        baseJoint = transform.Find("Graphics").Find("baseJoint");
        
	}
	
	// Update is called once per frame
	void Update () {

        if (!Application.isPlaying) {

            baseJoint.rotation = Quaternion.AngleAxis(startAngle, baseJoint.forward);

        }

	}

    public void GoActive() {
        
        Rigidbody[] chainRigidbodies = baseJoint.GetComponentsInChildren<Rigidbody>();

        foreach(Rigidbody rb in chainRigidbodies) {

            rb.isKinematic = false;

        }
        
    }

}
