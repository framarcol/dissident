﻿using UnityEngine;
using System.Collections;

public class FanWindZone : MonoBehaviour {

    public float pushAmount;

    PlayerMovementController playerMovementController;

	// Use this for initialization
	void Start () {

        playerMovementController = FindObjectOfType<PlayerMovementController>();

	}

    void OnTriggerStay(Collider other) {

        if(other.tag == "Player") {

            playerMovementController.PushPlayer(pushAmount * Time.deltaTime);

            //other.transform.localPosition += new Vector3(pushAmount * Time.deltaTime, 0, 0);

        }

    }

}
