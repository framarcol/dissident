﻿using UnityEngine;
using System.Collections;

public class Inhibitor : MonoBehaviour {

    public float disableTime = 5f;

    ScreenEffects screenEffects;

    PlayerEffectStatus playerEffectStatus;
    PlayerMovementController playerMovementController;

    ShootController shootController;

    bool disabled = false;
    float disableTimer;

    // Use this for initialization
    void Start () {

        screenEffects = GameObject.FindGameObjectWithTag("ScreenEffects").GetComponent<ScreenEffects>();

        playerEffectStatus = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerEffectStatus>();
        playerMovementController = playerEffectStatus.GetComponentInParent<PlayerMovementController>();

        shootController = playerEffectStatus.GetComponent<ShootController>();

    }
	
    void Update() {

        if (disabled) {

            disableTimer += Time.deltaTime;

            if(disableTimer >= disableTime) {

                disabled = false;
                disableTimer = 0f;

                playerEffectStatus.confuseEffect.Stop();

                playerMovementController.canJump = true;

                playerMovementController.moveAccelerometerScale *= 2;
                playerMovementController.moveScaleFromFinger *= 2;

                shootController.canShoot = true;

            }

        }

    }

    public void DisablePlayer() {

        screenEffects.EnableEffect("Flash");
        playerEffectStatus.confuseEffect.Play();

        playerMovementController.canJump = false;

        playerMovementController.moveAccelerometerScale /= 2;
        playerMovementController.moveScaleFromFinger /= 2;

        shootController.canShoot = false;

        disabled = true;

    }

}
