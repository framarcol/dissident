﻿using UnityEngine;
using System.Collections.Generic;

public class ItemBox : BreakableEntity {

    public List<GameObject> items;

    public int selectedItem = -1;

    public bool spawnIdol;
    IdolItem idolItem;

    GameObject instantiatedItem = null;

    float targetHeight;

    public override void Start() {

        base.Start();

        if (spawnIdol) idolItem = GetComponentInChildren<IdolItem>(true);

    }

    void Update() {

        if(instantiatedItem != null) {

            float deltaHeight = Mathf.Lerp(instantiatedItem.transform.position.y, targetHeight, 3f * Time.deltaTime);

            instantiatedItem.transform.position = new Vector3(instantiatedItem.transform.position.x, deltaHeight, instantiatedItem.transform.position.z);

        }

    }

    public override void Die() {

        base.Die();

        if (spawnIdol) {

            instantiatedItem = idolItem.gameObject;

        }else if (items.Count != 0) {

            if (selectedItem == -1) {
                GameObject item = items[Random.Range(0, items.Count)];
                instantiatedItem = Instantiate(item, transform.position, item.transform.rotation) as GameObject;
            } else
                instantiatedItem = Instantiate(items[selectedItem], transform.position, items[selectedItem].transform.rotation) as GameObject;
            
        }

        targetHeight = GameObject.FindGameObjectWithTag("Player").transform.position.y + 0.5f;

    }

}
