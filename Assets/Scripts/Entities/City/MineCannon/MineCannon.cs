﻿using UnityEngine;
using System.Collections;

public class MineCannon : MonoBehaviour {

    [System.Serializable]
    public struct MineEvent {

        public float time;

        public bool firstRail;
        public bool secondRail;
        public bool thirdRail;
        public bool fourthRail;
        public bool fifthRail;

    }

    public GameObject mineCannonObject;
    public GameObject minePrefab;

    public MineEvent[] mineEvents;
    int eventIndex = 0;
    float eventTimer;

    Transform mineOrigin;

    bool active = false;

	// Use this for initialization
	void Start () {

        mineOrigin = mineCannonObject.transform.Find("MineOrigin");

	}
	
	// Update is called once per frame
	void Update () {

        if (active) {

            if(eventIndex > mineEvents.Length - 1) {

                mineOrigin.gameObject.SetActive(false);
                active = false;
                return;

            }

            eventTimer += Time.deltaTime;
            if (eventTimer >= mineEvents[eventIndex].time) {

                //TODO

                Vector3 position = transform.position + transform.forward * mineCannonObject.transform.localPosition.z;
                position.y = mineCannonObject.transform.position.y;

                if (mineEvents[eventIndex].firstRail)   CreateMineToPosition(position - 4f * transform.right);
                if (mineEvents[eventIndex].secondRail)  CreateMineToPosition(position - 2f * transform.right);
                if (mineEvents[eventIndex].thirdRail)   CreateMineToPosition(position);
                if (mineEvents[eventIndex].fourthRail)  CreateMineToPosition(position + 2f * transform.right);
                if (mineEvents[eventIndex].fifthRail)   CreateMineToPosition(position + 4f * transform.right);

                eventTimer = 0f;

                eventIndex++;

            }

            mineCannonObject.transform.Translate(transform.forward * 20f * Time.deltaTime, Space.World);

        }

	}

    private void CreateMineToPosition(Vector3 position) {

        MineMovement mine = ((GameObject)Instantiate(minePrefab, mineOrigin.position, mineOrigin.rotation)).GetComponent<MineMovement>();

        mine.targetPosition = position;

    }

    public void GoActive() {

        mineOrigin.gameObject.SetActive(true);

        active = true;

    }

}
