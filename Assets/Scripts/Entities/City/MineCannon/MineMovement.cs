﻿using UnityEngine;
using System.Collections;

public class MineMovement : MonoBehaviour {

    public Vector3 targetPosition;
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.Lerp(transform.position, targetPosition, 5f * Time.deltaTime);

	}
    
}
