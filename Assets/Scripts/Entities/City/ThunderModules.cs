﻿using UnityEngine;
using System.Collections;

public class ThunderModules : MonoBehaviour {

    public Entity leftModule;
    public Entity rightModule;

    public GameObject shock;

    int lifes;
    bool tilting;
    
	// Update is called once per frame
	void Update () {

        lifes = leftModule.lifes + rightModule.lifes;

        if (/*!tilting &&*/ lifes == 1) {

            //tilting = true;
            StartCoroutine(ShockTilt());

            

        }else if(lifes == 0) {

            StopAllCoroutines();
            shock.SetActive(false);

        }

	}

    IEnumerator ShockTilt() {

        while (lifes > 0) {
            shock.SetActive(false);
            yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
            shock.SetActive(true);
            yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        }

    }

}
