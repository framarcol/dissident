﻿using UnityEngine;

public class TrainArea : MonoBehaviour {

    public GameObject train;

    public float trainSpeed = 15f;

    public float timeToDissappear = 7.5f;

    public bool active;

    float timer;

    void Update() {

        if (active) {

            train.transform.Translate(train.transform.forward * trainSpeed * Time.deltaTime, Space.World);

            timer += Time.deltaTime;

            if (timer >= timeToDissappear) {

                train.SetActive(false);
                active = false;

            }

        }

    }

    public void GoActive() {

        active = true;

    }

}
