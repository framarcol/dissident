﻿using UnityEngine;
using System.Collections;

public class Vehicle : MonoBehaviour {

    public float speed = 10f;

    public float sideDistance = 0f;
    public float sideTime = 0f;
    float sideTimer = 0f;
    float oldDistance = 0f;

    //float targetAngle;

    bool goSideMove = false;

    Transform graphics;

    void Start() {

        graphics = transform.Find("Graphics");

        /*if (sideDistance > 0)
            targetAngle = -15f;
        else if (sideDistance < 0)
            targetAngle = 15f;*/

    }

	void Update () {

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

        if (goSideMove) {

            sideTimer += Time.deltaTime;

            if(sideTimer > sideTime) {

                sideTimer = sideTime;

                goSideMove = false;

            }/*else if(sideTimer > (sideTime / 2)) {

                targetAngle = 0f;

            }*/

            float percentage = sideTimer / sideTime;
            float anglePercentage = -0.5f * Mathf.Cos(2f * Mathf.PI * percentage) + 0.5f;
            
            //percentage = Mathf.Sin(Mathf.PI * percentage * 0.5f);
            percentage = percentage * percentage * (3f - 2f * percentage);

            float deltaSideDistance = sideDistance * percentage;

            transform.Translate(transform.right * (deltaSideDistance - oldDistance), Space.World);

            oldDistance = deltaSideDistance;

            //VEHICLE ANGLE
            float actualAngle = (sideDistance < 0) ? 15f * anglePercentage : -15f * anglePercentage;
            graphics.rotation = Quaternion.AngleAxis(actualAngle, transform.forward);

            //Debug.Log(percentage + " - " + anglePercentage + " -> " + actualAngle);

        }

	}

    public void SideMove() {

        goSideMove = true;

    }

}
