﻿using UnityEngine;
using System.Collections;

public class VehicleGroup : MonoBehaviour {

    public GameObject vehicleGroup;

    public bool enable = true;

    bool entered = false;

    void Start() {

        if (enable) vehicleGroup.SetActive(false);

    }

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            vehicleGroup.SetActive(enable);

        }

    }

}
