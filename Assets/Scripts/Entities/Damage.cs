﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Damage : MonoBehaviour {

    public int lifeHits = 1;

    //public bool goRagdoll = false;

    //Optional object to be disable just in case
    public GameObject objectToDisable;

    //ScoreController scoreController;

    bool entered;

    /*public virtual void Start() {

        scoreController = FindObjectOfType<ScoreController>();

    }*/

    //If the player becomes too close then damage it
    void OnTriggerEnter(Collider choque){

        if (!entered && choque.tag == "Player"){

            PlayerHealth health = choque.gameObject.GetComponentInChildren<PlayerHealth>();
            health.Damage(lifeHits);

            //scoreController.ResetMultiplier();

            OnHit();

            entered = true;
             
        }

    }

    public virtual void OnHit() {

        if (objectToDisable != null) objectToDisable.SetActive(false);

    }

    public void ResetDamage() {

        entered = false;

    }
    
}
