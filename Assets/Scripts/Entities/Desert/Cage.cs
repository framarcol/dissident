﻿using UnityEngine;
using System.Collections;

public class Cage : Entity {

    public GameObject lasers;
    public GameObject baseCage;
    public Material brokenCageMat;
    public GameObject liquid;
    public Animator yldoonita;

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        lasers.SetActive(false);
        baseCage.GetComponent<Renderer>().material = brokenCageMat;
        liquid.SetActive(true);
        yldoonita.SetTrigger("freed");

    }

}
