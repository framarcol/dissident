﻿using UnityEngine;
using System.Collections;

public class CampSwitch : Entity {

    public GameObject brokenSwitch;
    public ParticleSystem smoke;
    
    public override void Die() {

        GetComponent<MeshRenderer>().enabled = false;

        brokenSwitch.SetActive(true);

        smoke.gameObject.SetActive(true);

        GetComponent<Collider>().enabled = false;

    }

}
