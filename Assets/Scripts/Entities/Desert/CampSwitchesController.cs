﻿using UnityEngine;

public class CampSwitchesController : MonoBehaviour {

    public GameObject[] switches;

    public GameObject[] rays;

    bool entered = false;
	
	void Update () {

        if (entered) {

            bool switchesOff = true;

            foreach (GameObject switchObject in switches) {

                if (switchObject.GetComponent<Entity>().lifes > 0) switchesOff = false;

            }

            if (switchesOff) {

                EnableRays(false);

                enabled = false;

                return;

            }

        }

	}

    public void EnableRays(bool state) {

        foreach (GameObject ray in rays) {

            ray.SetActive(state);

        }

    }

    void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            entered = true;

            EnableRays(true);

        }

    }

}
