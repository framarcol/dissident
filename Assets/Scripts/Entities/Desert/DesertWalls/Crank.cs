﻿using UnityEngine;
using System.Collections;

public class Crank : MonoBehaviour {
    
    public GameObject doorL;
    public GameObject doorR;
    public bool puertaIzquierda;
    public bool puertaDerecha;
    public float speed;

    public Transform threatFocus;
    public Camera focusCamera;

    public Animator crank;
    public Animator crankSoldier;

    ThreatManager threatManager;

    bool stop = true;
    [HideInInspector]
    public bool alreadyShot = false;

    float distanceMoved = 0f;

    void Start() {

        threatManager = FindObjectOfType<ThreatManager>();

    }

    //void Start()
    //{
    //    // Inicia las puertas abiertas cuando se seleccionan los engranajes
    //    if (puertaIzquierda)
    //    {
    //        doorL.transform.Translate(-4.5f, 0, 0);
    //    }
    //    if (puertaDerecha)
    //    {
    //        doorR.transform.Translate(4.5f, 0, 0);
    //    }
        
    //}

    public void Shot(ShootPackage shootPackage){

        // Para el cierre de puertas cuando el engranaje es disparado
        stop = true;
        alreadyShot = true;

        if (crank != null) crank.SetTrigger("close");
        if (crankSoldier != null) crankSoldier.SetTrigger("dead");

        if (focusCamera != null) StartCoroutine(DisableCamera());

    }

    void Update(){
        // Cierre de puerta
        if (!stop){

            if (puertaIzquierda){
                doorL.transform.Translate(speed * Time.deltaTime, 0, 0);
                distanceMoved += speed * Time.deltaTime;
            }

            if (puertaDerecha){
                doorR.transform.Translate(-speed * Time.deltaTime, 0, 0);
            }

            if (distanceMoved >= 4.5f) stop = true;

        }
        
    }

    // Método llamado por Crank para activar el cierre de puerta una vez el usuario entra en el collider de la entidad
    public void Activar(){

        stop = false;

        //if (threatSignal != null) threatSignal.SetActive(true);
        if (threatManager != null) threatManager.CreateThreat(null, threatFocus);
        if (focusCamera != null) focusCamera.gameObject.SetActive(true);

        if (crank != null) crank.SetTrigger("open");
        if (crankSoldier != null) crankSoldier.SetTrigger("open");

    }

    IEnumerator DisableCamera() {

        yield return new WaitForSeconds(2f);

        focusCamera.gameObject.SetActive(false);

    }

}