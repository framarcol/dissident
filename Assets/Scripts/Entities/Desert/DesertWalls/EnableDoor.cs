﻿using UnityEngine;
using System.Collections;

public class EnableDoor : MonoBehaviour
{

    public GameObject leftFrame;
    public GameObject rightFrame;

    public Crank crank1;
    public Crank crank2;

    bool entered = false;

    void Start() {

        if (crank1.gameObject.activeSelf) {

            if(crank1.puertaIzquierda) leftFrame.transform.Translate(-4.5f, 0, 0);
            if(crank1.puertaDerecha) rightFrame.transform.Translate(4.5f, 0, 0);

        }

        if (crank2.gameObject.activeSelf) {

            if (crank2.puertaIzquierda) leftFrame.transform.Translate(-4.5f, 0, 0);
            if (crank2.puertaDerecha) rightFrame.transform.Translate(4.5f, 0, 0);

        }
        

    }
    
    void OnTriggerEnter(Collider choque){

        if (!entered && choque.tag == "Player"){

            if(!crank1.alreadyShot) crank1.Activar();
            if(!crank2.alreadyShot) crank2.Activar();

            entered = true;
             
        }

    }
}