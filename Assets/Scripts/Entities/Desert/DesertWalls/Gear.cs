﻿using UnityEngine;
using System.Collections;

public class Gear : MonoBehaviour {

    public GameObject doorL;
    public GameObject doorR;
    public float numDisparos = 3;
    public bool puertaIzquierda;
    public bool puertaDerecha;
    public bool esAyudante;
    float moverI=-1.5f, moverD=1.5f;

    void Start() {

        moverD = doorR.transform.localPosition.x;
        moverI = doorL.transform.localPosition.x;

    }

    // Cuando disparas la puerta se mueve 1 metro
    public void Shot(ShootPackage shootPackage){

        if (puertaDerecha)
        {
            if (doorR.transform.localPosition.x < 4.4)
            {
                if (!esAyudante)
                {
                    moverD += 3.0f / numDisparos;
                }
                else
                {
                    moverD += 3;
                }
                
            }
        }

        if (puertaIzquierda)
        {
            if (doorL.transform.localPosition.x > -4.4)
            {
                if (!esAyudante)
                {
                    moverI -= 3.0f / numDisparos;
                }
                else
                {
                    moverI -= 3;
                }
                
            }
        }

    }

    void Update()
    {
        // Método para desplazar la puerta lentamente
        if (puertaDerecha)
        {
            if (doorR.transform.localPosition.x < moverD)
            {
                doorR.transform.Translate(1 * (Time.deltaTime * 4), 0, 0);
            }
        }

        if (puertaIzquierda)
        {
           
           if (doorL.transform.localPosition.x > moverI)
           {
               doorL.transform.Translate(-1 * (Time.deltaTime * 4), 0, 0);
            }
        }


    }

}
