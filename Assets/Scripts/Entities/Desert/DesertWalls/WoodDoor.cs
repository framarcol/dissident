﻿using UnityEngine;
using System.Collections;

public class WoodDoor : Entity {

    public GameObject fullDoor;
    public GameObject brokenDoor;

    public override void Shot(ShootPackage shootPackage) {

        base.Shot(shootPackage);

        if(lifes > 0) {

            fullDoor.SetActive(false);
            brokenDoor.SetActive(true);

        }

    }

    public override void Die() {

        foreach(Transform child in brokenDoor.transform) {

            Rigidbody piece = child.GetComponent<Rigidbody>();

            piece.isKinematic = false;
            piece.AddExplosionForce(250f, impactPoint, 5f, 0.25f);

        }

        Destroy(brokenDoor, 3f);

        GetComponent<Collider>().enabled = false;

    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            if(lifes == 1) {

                other.GetComponent<PlayerHealth>().Damage();

                impactPoint = other.transform.position;
                Die();

            }

        }

    }

}
