﻿using UnityEngine;

public class DryBranches : MonoBehaviour {
    
    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            foreach(Transform child in transform) {

                child.GetComponent<Rigidbody>().isKinematic = false;

                Destroy(child.gameObject, 3f);

            }

        }

    }

}
