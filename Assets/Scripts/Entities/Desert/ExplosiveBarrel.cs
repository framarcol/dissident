﻿using UnityEngine;
using System.Collections;

public class ExplosiveBarrel: Entity {

    public int damage = 2;
    public float force = 1000f;

    public ParticleSystem explosionParticle;

    bool entered = false;
    
    public override void Die() {

        if (explosionParticle != null) explosionParticle.Play();

        Collider[] affected = new Collider[10];
        Physics.OverlapSphereNonAlloc(transform.position, 4f, affected, LayerMask.GetMask("Shootable"), QueryTriggerInteraction.Collide);

        foreach (Collider col in affected) {

            if (col != null && col != GetComponent<Collider>()) {

                LivingEntity livingEntity = col.GetComponent<LivingEntity>();

                if (livingEntity != null) {

                    ShootPackage shootPackage = new ShootPackage();
                    shootPackage.damage = damage;
                    shootPackage.hitPoint = transform.position;

                    livingEntity.force = force;
                    livingEntity.Shot(shootPackage);

                } else {

                    ExplosiveBarrel explosiveBarrel = col.GetComponent<ExplosiveBarrel>();
                    
                    if (explosiveBarrel != null) {

                        //ExplosiveBarrel explosiveBarrel = col.GetComponent<ExplosiveBarrel>();
                        StartCoroutine(explosiveBarrel.GoExplode());

                    } else {

                        Entity entity = col.GetComponent<Entity>();

                        if(entity != null && entity.lifes > 0) {

                            entity.GetScore();
                            entity.Die();

                            /*ShootPackage shootPackage = new ShootPackage();
                            shootPackage.damage = damage;
                            shootPackage.hitPoint = transform.position;

                            entity.Shot(shootPackage);*/

                        }

                    }

                }

            }

        }

        transform.Find("Graphics").gameObject.SetActive(false);

        GetComponent<Collider>().enabled = false;

    }

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            other.GetComponent<PlayerHealth>().Damage();

            Die();

        }

    }

    IEnumerator GoExplode() {

        yield return new WaitForSeconds(0.25f);

        Die();

    }

}
