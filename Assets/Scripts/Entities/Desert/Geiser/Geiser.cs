﻿using UnityEngine;
using System.Collections;

public class Geiser : MonoBehaviour {

    public GameObject cap;

    public ParticleSystem geiserParticle;

    Rigidbody capRigidbody;

    void Start() {

        if (cap != null) capRigidbody = cap.GetComponent<Rigidbody>();

    }

    public void Activate() {

        StartCoroutine(InitGeiser());

    }

    IEnumerator InitGeiser() {

        geiserParticle.gameObject.SetActive(true);

        geiserParticle.Play();

        if (capRigidbody != null) capRigidbody.AddForce(capRigidbody.transform.up * 7.5f, ForceMode.Impulse);

        yield return new WaitForSeconds(0.5f);

        geiserParticle.Stop();

        yield return new WaitForSeconds(1f);

        if (capRigidbody != null) {

            capRigidbody.AddForce(capRigidbody.transform.up * 25f, ForceMode.Impulse);
            capRigidbody.AddTorque(capRigidbody.transform.forward * Random.Range(-2.5f, 2.5f), ForceMode.Impulse);

        }

        geiserParticle.Play();

        if (capRigidbody != null) Destroy(cap, 5f);

    }

}
