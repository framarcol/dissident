﻿using UnityEngine;
using System.Collections;

public class GeiserArea : MonoBehaviour {

    Geiser geiser;

    bool entered = false;

	// Use this for initialization
	void Start () {

        geiser = GetComponentInChildren<Geiser>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            geiser.Activate();

        }

    }

}
