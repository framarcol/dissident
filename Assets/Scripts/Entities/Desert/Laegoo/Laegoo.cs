﻿using UnityEngine;
using System.Collections;

public class Laegoo : LivingEntity {

    public float speed = 10;

    public bool willRun = false;

    public bool goRun = false;

    float lifeTimer;

    void Update() {

        if (goRun) {

            lifeTimer += Time.deltaTime;

            if(lifeTimer >= 3f) {

                goRun = false;
                gameObject.SetActive(false);

            }

            transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
            
        }

    }

    public override void Shot(ShootPackage shootPackage) {
        
        scoreController.AddPoints(score);
        scoreController.ResetMultiplier();

        entityAnimator.SetTrigger("hit");

    }

}
