﻿using UnityEngine;
using System.Collections;

public class LaegooRunArea : MonoBehaviour {

    public Laegoo laegoo;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if (laegoo.willRun) {

                laegoo.goRun = true;
                laegoo.entityAnimator.SetTrigger("run");

            }

        }

    }

}
