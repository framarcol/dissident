﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PipeSmokeController : MonoBehaviour {

    public GameObject smokeObject;
    public bool isSmokeActive = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
        if(smokeObject != null) {

            if (isSmokeActive) smokeObject.SetActive(true);
            else smokeObject.SetActive(false);

#if UNITY_EDITOR

            ParticleSystem particles = smokeObject.GetComponentInChildren<ParticleSystem>();
            particles.transform.localScale = new Vector3(1, 1, -transform.lossyScale.x);

#endif

        }
        
	}

}
