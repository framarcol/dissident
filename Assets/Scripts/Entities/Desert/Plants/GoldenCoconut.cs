﻿using UnityEngine;
using System.Collections;

public class GoldenCoconut : MonoBehaviour {

    public float pickSpeed = 5f;

    Transform player;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	// Update is called once per frame
	void Update () {

        Vector3 direction = (player.position - transform.position).normalized;

        transform.Translate(direction * pickSpeed * Time.deltaTime);

	}

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            gameObject.SetActive(false);

        }

    }

}
