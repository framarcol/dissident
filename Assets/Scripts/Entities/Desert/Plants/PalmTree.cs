﻿using UnityEngine;
using System.Collections;

public class PalmTree : Entity {
    
    public bool isGold = false;
    public float goldScore = 300f;

    public GameObject goldenCoconut;
    public GameObject normalCoconuts;

    public override void Start() {

        base.Start();

        if (isGold) score = goldScore;

    }

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        if (isGold) {

            goldenCoconut.SetActive(true);
            /*goldenCoconut.GetComponent<Rigidbody>().AddExplosionForce(100f, impactPoint, 5f);

            Destroy(goldenCoconut, 3f);*/

        }

        foreach(Transform coconut in normalCoconuts.transform) {

            Rigidbody rb = coconut.GetComponent<Rigidbody>();

            rb.isKinematic = false;
            rb.AddExplosionForce(100f, impactPoint, 5f);

        }

        Destroy(normalCoconuts, 3f);

    }

}
