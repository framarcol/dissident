﻿using UnityEngine;
using System.Collections;

public class ThornPlantEntity : Entity {

    public override void Shot(ShootPackage shootPackage) {

        base.Shot(shootPackage);

        if(lifes <= 0) {

            GetComponent<Collider>().enabled = false;

        }

    }

    public override void Die() {

        ThornsDamage thornsDamage = GetComponentInChildren<ThornsDamage>();
        if(thornsDamage != null) thornsDamage.OnHit();

    }

}
