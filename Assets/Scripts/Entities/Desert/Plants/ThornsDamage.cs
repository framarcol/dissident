﻿using UnityEngine;
using System.Collections;

public class ThornsDamage : Damage {

    public float explosionTime = 1f;
    public float explosionDistance = 3f;

    public bool thornExplosion = false;

    float explosionTimer = 0f;
    float oldDistance = 0f;

    float initThornScale;

    /*public override*/
    void Start() {

        //base.Start();

        initThornScale = transform.GetChild(0).localScale.x;

    }

    void Update() {

        if (thornExplosion) {

            explosionTimer += Time.deltaTime;

            if(explosionTimer >= explosionTime) {

                explosionTimer = explosionTime;
                thornExplosion = false;

            }

            float perc = explosionTimer / explosionTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            float actualDistance = perc * explosionDistance;
            float deltaDistance = actualDistance - oldDistance;
            oldDistance = actualDistance;

            foreach (Transform thorns in transform) {

                thorns.Translate(thorns.forward * deltaDistance, Space.World);
                thorns.localScale = new Vector3(1 - perc, 1 - perc, 1 - perc) * initThornScale;

            }

            if (!thornExplosion) {

                //foreach (Transform thorns in transform) thorns.gameObject.SetActive(false);
                gameObject.SetActive(false);

            }

        }

    }

    public override void OnHit() {

        thornExplosion = true;

    }

}
