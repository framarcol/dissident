﻿using UnityEngine;
using System.Collections;

public class SpinController : MonoBehaviour {

    public Axis axis = Axis.z;

    public GameObject spinObject;
    public float speed = 5f;

	// Update is called once per frame
	void Update () {

        if (spinObject != null) {

            switch (axis) {

                case Axis.x:
                    spinObject.transform.Rotate(transform.right, speed * Time.deltaTime, Space.World);
                    break;

                case Axis.y:
                    spinObject.transform.Rotate(transform.up, speed * Time.deltaTime, Space.World);
                    break;

                case Axis.z:
                    spinObject.transform.Rotate(transform.forward, speed * Time.deltaTime, Space.World);
                    break;

            }
            
        }

	}

}
