﻿using UnityEngine;

public class Waterfall : MonoBehaviour {

    PlayerHealth playerHealth;

    void Start() {

        playerHealth = FindObjectOfType<PlayerHealth>();
        
    }

	public void ClearStuck() {

        Spiderweb.ClearStuckMeasure();

        playerHealth.spiderwebPiece1.SetActive(false);
        playerHealth.spiderwebPiece2.SetActive(false);

    }

}
