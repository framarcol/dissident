﻿using UnityEngine;

public class EffectLifetime : MonoBehaviour {
    
	void Start () {

        ParticleSystem particleSystem = GetComponent<ParticleSystem>();

        Destroy(gameObject, particleSystem.duration);

	}

}
