﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {
    
    void OnTriggerEnter(Collider other) {

        if (other.tag == "CameraArm") {

            GameManager.GameCompleted();

        }

    }

}
