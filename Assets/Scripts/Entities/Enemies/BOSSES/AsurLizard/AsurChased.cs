﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsurChased : MonoBehaviour {
    
    public GameObject asur;

    public float speed = 20f;

    //public float dissolveTime = 0.5f;

    public GameObject targetPositions;
    List<Transform> targetPositionList;
    public enum TargetPositions { UP_LEFT = 0, LEFT = 1, MID = 2, RIGHT = 3, UP_RIGHT = 4 }
    public TargetPositions initPosition;
    TargetPositions activePosition;

    public LineRenderer laser;
    public float laserFireTime = 0.5f;
    public float laserInitWidth = 1f;
    public ParticleSystem snowExplosion;

    Material asurMaterial;
    int cutoutProperty;

    bool initialized;

    AudioSource audioSource;
    public AudioClip[] teleportSounds;

    void OnEnable() {

        /*if (asur == null || targetPositions == null) {
            gameObject.SetActive(false);
            return;
        }*/

        if (initialized) {
            StartCoroutine(FadeAsur(true, 1f));
            if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);
        }

    }

	void Start () {

        if (asur != null) {
            asurMaterial = asur.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial;

            cutoutProperty = Shader.PropertyToID("_Cutout");
        }

        if(targetPositions != null) {

            targetPositionList = new List<Transform>();

            foreach(Transform child in targetPositions.transform) {

                targetPositionList.Add(child);

            }

            if (asur != null) {
                asur.transform.position = targetPositionList[(int)initPosition].position;
                activePosition = initPosition;
            }

        }

        gameObject.SetActive(false);

        if(asur != null && targetPositions != null) initialized = true;

        audioSource = GetComponent<AudioSource>();

	}
	
	void Update () {

        transform.position += new Vector3(0, 0, speed * Time.deltaTime);

	}

    public void TeleportAsur(TargetPositions target, float time) {

        if (activePosition == target) return;

        activePosition = target;

        StartCoroutine(TeleportAsurCoroutine(targetPositionList[(int)target], time));

        if(teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);

    }

    IEnumerator TeleportAsurCoroutine(Transform targetPosition, float time) {

        yield return FadeAsur(false, time);

        asur.transform.position = targetPosition.position;
        
        yield return FadeAsur(true, time);

    }

    IEnumerator FadeAsur(bool fadeIn, float time) {

        if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 1f);
        else asurMaterial.SetFloat(cutoutProperty, 0f);

        float perc = 0;
        float timer = 0f;
        while (timer < time) {

            timer += Time.deltaTime;

            perc = timer / time;

            if(fadeIn) asurMaterial.SetFloat(cutoutProperty, 1 - perc);
            else asurMaterial.SetFloat(cutoutProperty, perc);

            yield return null;

        }

        if(fadeIn) asurMaterial.SetFloat(cutoutProperty, 0f);
        else asurMaterial.SetFloat(cutoutProperty, 1f);

    }

    public void FireLaser(Vector3 target) {

        if(laser != null) {

            laser.gameObject.SetActive(true);

            GameObject snowExplosionInstance = Instantiate(snowExplosion.gameObject, target, Quaternion.identity) as GameObject;
            //snowExplosion.transform.position = target;
            snowExplosionInstance.GetComponent<ParticleSystem>().Play();

            //laser.SetPosition(1, laser.transform.InverseTransformPoint(target.position));

            StartCoroutine(FireLaserCoroutine(target));

        }

    }

    IEnumerator FireLaserCoroutine(Vector3 target) {

        float timer = 0f;
        while(timer < laserFireTime) {

            timer += Time.deltaTime;

            laser.SetPosition(1, laser.transform.InverseTransformPoint(target));

            laser.sharedMaterial.mainTextureOffset = new Vector2(20f * Time.deltaTime, 0);

            float perc = timer / laserFireTime;

            laser.SetWidth( (1f - perc) * laserInitWidth, (1f - perc) * laserInitWidth );

            yield return null;

        }

        laser.SetWidth(laserInitWidth, laserInitWidth);
        laser.gameObject.SetActive(false);

    }

    public void DisableAsur() {

        StartCoroutine(DisableAsurCoroutine());

        if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);

    }

    IEnumerator DisableAsurCoroutine() {

        yield return FadeAsur(false, 1f);

        asurMaterial.SetFloat(cutoutProperty, 0f);

        gameObject.SetActive(false);

    }

}
