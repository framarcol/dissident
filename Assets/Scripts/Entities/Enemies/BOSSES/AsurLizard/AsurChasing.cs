﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsurChasing : MonoBehaviour {

    public GameObject asur;
    Animator asurAnimator;

    public float speed = 20f;

    public GameObject asurClon;
    Queue<GameObject> asurClonQueue;
    
    public GameObject targetPositions;
    List<Transform> targetPositionList;
    public enum TargetPositions { LEFT = 0, MID = 1, RIGHT = 2, ALL_IN = 3 }
    public TargetPositions initPosition;
    TargetPositions activePosition;
    
    Material asurMaterial;
    int cutoutProperty;

    bool initialized;

    AudioSource audioSource;
    public AudioClip[] teleportSounds;

    void OnEnable() {

        if (initialized) {
            StartCoroutine(FadeAsur(true, 1f));
            if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);
        }

    }

    void Start () {

        if (asur != null) {

            asurMaterial = asur.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial;

            cutoutProperty = Shader.PropertyToID("_Cutout");

            asurAnimator = asur.GetComponent<Animator>();

        }

        if (targetPositions != null) {

            targetPositionList = new List<Transform>();

            foreach (Transform child in targetPositions.transform) {

                targetPositionList.Add(child);

            }

            if (asur != null) {
                asur.transform.position = targetPositionList[(int)initPosition].position;
                activePosition = initPosition;
            }

        }

        if (asurClon != null) {

            asurClonQueue = new Queue<GameObject>();

            for(int i = 0; i < 6; i++) {

                GameObject clonInstance = Instantiate(asurClon, transform) as GameObject;

                clonInstance.SetActive(false);

                clonInstance.GetComponent<AsurClon>().asurChasing = this;

                asurClonQueue.Enqueue(clonInstance);

            }

        }

        gameObject.SetActive(false);

        initialized = true;

        audioSource = GetComponent<AudioSource>();
        
    }
	
	void Update () {

        transform.position += new Vector3(0,0, speed * Time.deltaTime);

	}

    public void TeleportAsur(TargetPositions target, float time, float waitForCharge, bool leftCrit, bool midCrit, bool rightCrit) {

        if (target == activePosition) return;

        activePosition = target;

        if (target == TargetPositions.ALL_IN) {

            StartCoroutine(AllInTeleport(time, waitForCharge, leftCrit, midCrit, rightCrit));

        } else {

            StartCoroutine(TeleportAsurCoroutine(targetPositionList[(int)target], time, waitForCharge));

        }

        if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);

    }

    IEnumerator TeleportAsurCoroutine(Transform targetPosition, float time, float waitForCharge) {

        CreateClon(asur.transform.position, waitForCharge, false);

        yield return FadeAsur(false, time);
        
        asur.transform.position = targetPosition.position;
        
        yield return FadeAsur(true, time);

    }

    IEnumerator AllInTeleport(float time, float waitForCharge, bool leftCrit, bool midCrit, bool rightCrit) {
        
        CreateClon(new Vector3(4, asur.transform.position.y, asur.transform.position.z), waitForCharge, leftCrit);
        CreateClon(new Vector3(0, asur.transform.position.y, asur.transform.position.z), waitForCharge, midCrit);
        CreateClon(new Vector3(-4, asur.transform.position.y, asur.transform.position.z), waitForCharge, rightCrit);

        yield return FadeAsur(false, time);

        asur.SetActive(false);

    }

    IEnumerator FadeAsur(bool fadeIn, float time) {

        if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 1f);
        else asurMaterial.SetFloat(cutoutProperty, 0f);

        float perc = 0;
        float timer = 0f;
        while (timer < time) {

            timer += Time.deltaTime;

            perc = timer / time;

            if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 1 - perc);
            else asurMaterial.SetFloat(cutoutProperty, perc);

            yield return null;

        }

        if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 0f);
        else asurMaterial.SetFloat(cutoutProperty, 1f);

    }

    public void CreateClon(Vector3 position, float waitForCharge, bool doCritical) {

        if (asurClonQueue.Count <= 0) return;

        GameObject clon = asurClonQueue.Dequeue();
        AsurClon asurClon = clon.GetComponent<AsurClon>();

        asurClon.waitForCharge = waitForCharge;

        clon.SetActive(true);
        clon.transform.position = position;

        Animator animator = clon.GetComponent<Animator>();
        animator.Play("Run", 0, asurAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);

        

        if (doCritical) {

            asurClon.ActivateCritic();
            asurClon.special = true;

        }

    }

    public void DisableClon(GameObject clon) {

        clon.SetActive(false);

        asurClonQueue.Enqueue(clon);

    }

    public void DisableAsur() {

        StartCoroutine(DisableAsurCoroutine());

        if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);

    }

    IEnumerator DisableAsurCoroutine() {

        yield return FadeAsur(false, 1f);

        asurMaterial.SetFloat(cutoutProperty, 0f);

        gameObject.SetActive(false);

    }

}
