﻿using UnityEngine;
using System.Collections;

public class AsurClon : Entity {
    
    public AsurChasing asurChasing;

    public float waitForCharge = 2f;
    public bool special;

    void OnEnable() {

        if(!special) StartCoroutine(AttackCharge());

    }

    public override void Shot(ShootPackage shootPackage) {

        if(isCritical) base.Shot(shootPackage);

    }

    IEnumerator AttackCharge() {

        yield return new WaitForSeconds(waitForCharge);

        GetComponent<Animator>().speed = 2f;

        float timer = 2f;
        while(timer > 0) {

            timer -= Time.deltaTime;

            transform.position += new Vector3(0, 0, 25f * Time.deltaTime);

            yield return null;
            
        }

        GetComponent<Animator>().speed = 1f;

        asurChasing.DisableClon(gameObject);

    }

    public void ActivateCritic() {

        StartCoroutine(ActivateCriticCoroutine(0.5f));

    }

    IEnumerator ActivateCriticCoroutine(float time) {

        yield return new WaitForSeconds(time);

        threatManager.CreateThreat(this, threatTarget);

        //yield return new WaitForSeconds(1.5f);

        yield return AttackCharge();

    }

}
