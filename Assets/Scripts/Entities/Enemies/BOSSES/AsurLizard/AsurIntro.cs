﻿using UnityEngine;
using System.Collections;

public class AsurIntro : MonoBehaviour {

    public float glowFadeTime = 2f;
    float defaultGlowIntensity;

    public float statueRecoveryTime = 2f;

    public float dissolveTime = 2f;

    public Transform cameraIntro;
    public float cameraSpeed = 0.1f;
    public float flyCameraTime = 4f;

    Material mat;
    Animator asurAnimator;

    int glowIntensityProperty;
    int cutoutProperty;

    AudioSource audioSource;
    public AudioClip[] teleportSounds;

    void Start () {

        mat = GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial;
        asurAnimator = GetComponent<Animator>();

        glowIntensityProperty = Shader.PropertyToID("_GlowIntensity");
        cutoutProperty = Shader.PropertyToID("_Cutout");

        defaultGlowIntensity = mat.GetFloat(glowIntensityProperty);
        mat.SetFloat(glowIntensityProperty, 0f);
        mat.SetFloat(cutoutProperty, 0f);

        audioSource = GetComponent<AudioSource>();

    }

    public void GoAnimate() {

        StartCoroutine(Animate());

    }

    IEnumerator Animate() {
        
        float timer = 0f;
        float perc = 0f;

        yield return new WaitForSeconds(2f);

        while(timer < glowFadeTime) {

            timer += Time.deltaTime;

            perc = timer / glowFadeTime;

            mat.SetFloat(glowIntensityProperty, perc * defaultGlowIntensity);

            yield return null;

        }
        mat.SetFloat(glowIntensityProperty, defaultGlowIntensity);

        if(cameraIntro != null) StartCoroutine(ApproachCamera());

        asurAnimator.SetTrigger("goIdle");

        timer = 0f;
        while(timer < statueRecoveryTime) {

            timer += Time.deltaTime;

            perc = timer / statueRecoveryTime;
            perc = 1 - Mathf.Cos(Mathf.PI * perc * 0.5f);

            asurAnimator.SetFloat("statueSpeed", perc);

            yield return null;

        }

        asurAnimator.SetFloat("statueSpeed", 1f);

        yield return new WaitForSeconds(1.5f);

        if (teleportSounds.Length > 0) audioSource.PlayOneShot(teleportSounds[Random.Range(0, teleportSounds.Length)]);

        timer = 0f;
        while(timer < dissolveTime) {

            timer += Time.deltaTime;

            perc = timer / dissolveTime;

            mat.SetFloat(cutoutProperty, perc);

            yield return null;

        }

        mat.SetFloat(cutoutProperty, 0f);
        gameObject.SetActive(false);

    }

    IEnumerator ApproachCamera() {

        float timer = 0f;
        while(timer < flyCameraTime) {

            timer += Time.deltaTime;

            //float perc = timer / flyCameraTime;

            cameraIntro.position += new Vector3(0,0, cameraSpeed * Time.deltaTime);

            yield return null;

        }

    }

#if UNITY_EDITOR

    void OnApplicationQuit() {

        mat.SetFloat(glowIntensityProperty, 1.75f);
        mat.SetFloat(cutoutProperty, 0f);

    }

#endif

}
