﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsurRaysAndFall : MonoBehaviour {

    public GameObject jumpablePieces;
    List<GameObject> jumpablePieceList;
    public GameObject obstaclePieces;
    List<GameObject> obstaclePieceList;

    [Header("Positions")]
    public bool left;
    public bool mid;
    public bool right;

    [Header("Size")]
    public bool bigLeft;
    public bool bigMid;
    public bool bigRight;

    List<GameObject> activablePieces = new List<GameObject>();

	void Start () {

        if(jumpablePieces != null) {

            jumpablePieceList = new List<GameObject>();

            foreach(Transform child in jumpablePieces.transform) {

                jumpablePieceList.Add(child.gameObject);

            }

        }

        if(obstaclePieces != null) {
            
            obstaclePieceList = new List<GameObject>();

            foreach (Transform child in obstaclePieces.transform) {

                obstaclePieceList.Add(child.gameObject);

            }

        }

        if(jumpablePieceList.Count > 0 && obstaclePieceList.Count > 0) {

            if (left) {

                GameObject piece;

                if (bigLeft) {

                    piece = obstaclePieceList[Random.Range(0, obstaclePieceList.Count)];
                    obstaclePieceList.Remove(piece);

                } else {

                    piece = jumpablePieceList[Random.Range(0, jumpablePieceList.Count)];
                    jumpablePieceList.Remove(piece);

                }

                piece.transform.position -= new Vector3(4, 0, 0);
                activablePieces.Add(piece);

            }

            if (mid) {

                GameObject piece;

                if (bigMid) {

                    piece = obstaclePieceList[Random.Range(0, obstaclePieceList.Count)];
                    obstaclePieceList.Remove(piece);

                } else {

                    piece = jumpablePieceList[Random.Range(0, jumpablePieceList.Count)];
                    jumpablePieceList.Remove(piece);

                }

                activablePieces.Add(piece);

            }

            if (right) {

                GameObject piece;

                if (bigRight) {

                    piece = obstaclePieceList[Random.Range(0, obstaclePieceList.Count)];
                    obstaclePieceList.Remove(piece);

                } else {

                    piece = jumpablePieceList[Random.Range(0, jumpablePieceList.Count)];
                    jumpablePieceList.Remove(piece);

                }

                piece.transform.position += new Vector3(4, 0, 0);
                activablePieces.Add(piece);

            }

        }
	
	}
	
	public void Fall() {

        for(int i = 0; i < activablePieces.Count; i++) {

            activablePieces[i].SetActive(true);
            activablePieces[i].GetComponentInChildren<Rigidbody>().isKinematic = false;

        }

    }

    void OnTriggerEnter(Collider other) {

        AsurChased asurChased = other.GetComponent<AsurChased>();

        if(asurChased != null) {

            asurChased.FireLaser(jumpablePieces.transform.position + new Vector3(0,45,0));
            Fall();

        }

    }
    
}
