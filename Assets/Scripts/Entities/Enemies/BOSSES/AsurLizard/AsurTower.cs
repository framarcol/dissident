﻿using UnityEngine;
using System.Collections;

public class AsurTower : Entity {

    public Animator collapseColumns;

    Animator asurAnimator;

    Material asurMaterial;
    int cutoutProperty;

    public ParticleSystem leftCharge;
    public ParticleSystem rightCharge;

    bool initialized = false;

    void OnEnable() {

        if (initialized) {

            FadeAsur(true, 1f);

            StartCoroutine(StartAttack());

        }

    }

    // Use this for initialization
    public override void Start () {

        base.Start();

        asurAnimator = GetComponent<Animator>();

        asurMaterial = GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial;
        cutoutProperty = Shader.PropertyToID("_Cutout");
        
        gameObject.SetActive(false);

        initialized = true;

	}

    public override void Shot(ShootPackage shootPackage) {

        if(isCritical) base.Shot(shootPackage);

    }

    IEnumerator FadeAsur(bool fadeIn, float time) {

        if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 1f);
        else asurMaterial.SetFloat(cutoutProperty, 0f);

        float perc = 0;
        float timer = 0f;
        while (timer < time) {

            timer += Time.deltaTime;

            perc = timer / time;

            if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 1 - perc);
            else asurMaterial.SetFloat(cutoutProperty, perc);

            yield return null;

        }

        if (fadeIn) asurMaterial.SetFloat(cutoutProperty, 0f);
        else asurMaterial.SetFloat(cutoutProperty, 1f);

    }

    IEnumerator StartAttack() {

        yield return new WaitForSeconds(1f);

        asurAnimator.SetFloat("attackSpeed", 0.15f);
        asurAnimator.SetTrigger("attack");

        if(leftCharge != null) leftCharge.Play();
        if(rightCharge != null) rightCharge.Play();

        yield return new WaitForSeconds(0.5f);

        threatManager.CreateThreat(this, threatTarget);

        yield return new WaitForSeconds(1.5f);

        threatManager.CreateThreat(this, threatTarget);

        yield return new WaitForSeconds(1f);

        if(lifes > 0) {

            asurAnimator.SetFloat("attackSpeed", 1f);

            yield return new WaitForSeconds(0.2f);

            if (leftCharge != null) leftCharge.Stop();
            if (rightCharge != null) rightCharge.Stop();

            collapseColumns.SetTrigger("collapse");

            yield return FadeAsur(false, 0.1f);
            gameObject.SetActive(false);

        }
        
    }

    public override void Die() {

        StartCoroutine(DisableAsur());

    }

    IEnumerator DisableAsur() {

        asurAnimator.SetTrigger("hit");

        yield return new WaitForSeconds(1f);

        yield return FadeAsur(false, 1f);
        gameObject.SetActive(false);

    }

}
