﻿using UnityEngine;
using System.Collections;

public class CreateAsurClon : MonoBehaviour {

    public AsurChasing.TargetPositions targetPosition;
    public float time = 0.5f;
    public float waitForCharge = 2f;

    public bool leftCritic;
    public bool midCritic;
    public bool rightCritic;
    
    void OnTriggerEnter(Collider other) {

        AsurChasing asurChasing = other.GetComponent<AsurChasing>();

        if(asurChasing != null) {

            asurChasing.TeleportAsur(targetPosition, time, waitForCharge, leftCritic, midCritic, rightCritic);

        }

    }

}
