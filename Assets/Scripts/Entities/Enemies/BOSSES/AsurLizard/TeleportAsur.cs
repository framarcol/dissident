﻿using UnityEngine;
using System.Collections;

public class TeleportAsur : MonoBehaviour {

    public AsurChased.TargetPositions targetPosition;
    public float dissolveTime = 0.5f;

    void OnTriggerEnter(Collider other) {

        AsurChased asurChased = other.GetComponent<AsurChased>();

        if(asurChased != null) {

            asurChased.TeleportAsur(targetPosition, dissolveTime);

        }

    }

}
