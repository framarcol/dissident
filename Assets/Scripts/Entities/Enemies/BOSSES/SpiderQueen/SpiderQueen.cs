﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpiderQueen : Entity {
    
    public GameObject spiderwebBall;
    public Transform spiderwebBallOrigin;

    public Transform spiderwebBallTarget;

    //TEST
    //public Text hpLabel;
    //

    public BossBarController bossBar;
    public int bossLifes;

    public int activeAct = 1;

    public float speed = 0f;
    float lerpedSpeed = 0f;

    Animator spiderQueenAnimator;

    PlayerHealth playerHealth;

    AudioSource audioSource;
    public AudioClip[] spitSounds;

    void OnEnable() {

        //TEST
        //hpLabel.text = lifes.ToString();
        //

        //if (bossBar != null) {

            bossBar.gameObject.SetActive(true);
            bossBar.ResetBar(bossLifes);

        //}

    }

    void OnDisable() {

        bossBar.gameObject.SetActive(false);

    }

    // Use this for initialization
    public override void Start () {

        base.Start();

        spiderQueenAnimator = GetComponent<Animator>();

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        
        //Select ACT for animation tree
        switch (activeAct) {

            case 1: spiderQueenAnimator.SetBool("1st ACT", true); break;

            case 2: spiderQueenAnimator.SetBool("2nd ACT", true); break;

            default: spiderQueenAnimator.SetBool("1st ACT", true); break;

        }

        //If any speed is set, set the lerped speed to the maximum
        if (speed != 0) lerpedSpeed = speed;

        audioSource = GetComponent<AudioSource>();

	}

    // Update is called once per frame
    void Update () {
	
        if(speed != 0) {

            lerpedSpeed = Mathf.Lerp(lerpedSpeed, speed, 5f * Time.deltaTime);

            transform.Translate(transform.forward * lerpedSpeed * Time.deltaTime, Space.World);

        }

	}

    public override void Shot(ShootPackage shootPackage) {

        Damage(shootPackage.damage);

    }

    public void Damage(int numLifes, int type = -1) {

        lifes -= numLifes;
        //TEST
        //hpLabel.text = lifes.ToString();
        ////

        //if (lifes <= 0) {

        if(activeAct == 1) {

            if(type == 0) spiderQueenAnimator.SetTrigger("hit");
            else spiderQueenAnimator.SetTrigger("hitAttack");

        }else if(activeAct == 2) {

            if(type != 0) spiderQueenAnimator.SetTrigger("hitCatch");

        }

        /*if (bossBar != null)*/ bossBar.HitBar(numLifes);

        /*if (spiderQueenAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AttackLeg")) {

            spiderQueenAnimator.SetTrigger("hitAttack");

        } else if (spiderQueenAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AttackCatch")) {

            spiderQueenAnimator.SetTrigger("hitCatch");

        } else {

            spiderQueenAnimator.SetTrigger("hit");

        }*/

    }

    //SPIT STUFF
    public void StartSpitAnim() {

        spiderQueenAnimator.SetTrigger("spit");

        if(spitSounds.Length > 0) audioSource.PlayOneShot(spitSounds[Random.Range(0, spitSounds.Length)]);

    }

    public void SpitSpiderwebBalls() {

        SpiderwebBall sB = ((GameObject)Instantiate(spiderwebBall, spiderwebBallOrigin.position, spiderwebBallOrigin.rotation)).GetComponent<SpiderwebBall>();

        sB.spiderQueen = this;

        if (speed != 0) {

            sB.speed = 40f;
            sB.reducedSpeed = 30f;

        }

    }
    ////

    //ATTACK STUFF
    public void SetAttackFactor(string factor) {

        spiderQueenAnimator.SetFloat("attackFactor", float.Parse(factor));

    }

    public void KillPlayer() {

        playerHealth.KillPlayer(true);

    }
    ////

    //RUN STUFF
    public void SetSpeed(int speed) {

        this.speed = speed * 1.0f;

    }
    ////

}
