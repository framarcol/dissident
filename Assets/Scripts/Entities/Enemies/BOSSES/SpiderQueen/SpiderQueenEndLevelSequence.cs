﻿using UnityEngine;
using System.Collections;

public class SpiderQueenEndLevelSequence : MonoBehaviour {

    public bool leftStatueTriggered;
    public bool rightStatueTriggered;

    public GameObject deathCamera;
    public GameObject noDeathCamera;

    public GameObject fullDeathCinematic;
    public GameObject rightDeathCinematic;
    public GameObject leftDeathCinematic;
    public GameObject noDeathCinematic;

    PlayerMovementController playerMovementController;
    CameraFollowingController cameraFollowingController;
    
    void Start() {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        cameraFollowingController = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<CameraFollowingController>();

    }

    public void LeftStatueHit() {

        leftStatueTriggered = true;

    }

    public void RightStatueHit() {

        rightStatueTriggered = true;

    }

    public void TriggerEndLevelCinematic() {

        if(leftStatueTriggered || rightStatueTriggered) {

            deathCamera.SetActive(true);
            Transform target = deathCamera.transform.GetChild(0).GetChild(0);
            cameraFollowingController.transform.position = target.position;
            cameraFollowingController.transform.rotation = target.rotation;
            cameraFollowingController.cameraArm = target;

            playerMovementController.GetComponentInChildren<SkyboxController>().transform.parent = null;

            if (leftStatueTriggered && rightStatueTriggered) {

                fullDeathCinematic.SetActive(true);

            } else if (leftStatueTriggered) {

                leftDeathCinematic.SetActive(true);

            } else if (rightStatueTriggered) {

                rightDeathCinematic.SetActive(true);

            }

        } else {

            playerMovementController.SetSpeed(5f);
            StartCoroutine(PlayerDeathSequence());

            noDeathCamera.SetActive(true);
            Transform target = noDeathCamera.transform.GetChild(0).GetChild(0);
            cameraFollowingController.transform.position = target.position;
            cameraFollowingController.transform.rotation = target.rotation;
            cameraFollowingController.cameraArm = target;

            noDeathCinematic.SetActive(true);

        }

        //gameObject.SetActive(false);
        transform.Find("FinalStatues").gameObject.SetActive(false);

    }

    IEnumerator PlayerDeathSequence() {

        yield return new WaitForSeconds(4f);

        playerMovementController.transform.Find("PlayerStuff").gameObject.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        GameManager.GameOver();

    }

}
