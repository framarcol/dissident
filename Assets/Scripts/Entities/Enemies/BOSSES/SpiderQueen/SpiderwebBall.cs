﻿using UnityEngine;
using System.Collections;

public class SpiderwebBall : Entity {

    public float speed = 25f;
    public float reducedSpeed = 12.5f;
    float rotateFactor = 0f;
    float rotateTimer = 0f;

    public GameObject toxicExplosion;

    public SpiderQueen spiderQueen;
    Transform player;

    Transform target;

	// Use this for initialization
	public override void Start () {

        base.Start();

        player = GameObject.FindGameObjectWithTag("Player").transform;

        target = player;

	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

        //transform.Translate((player.position - transform.position).normalized * speed * Time.deltaTime, Space.World);
        
        rotateFactor = Mathf.Lerp(rotateFactor, 100f, Time.deltaTime);
        //Debug.Log(rotateFactor);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(target.position - transform.position), rotateFactor * Time.deltaTime);

        if(target == spiderQueen.spiderwebBallTarget) {

            if((target.position - transform.position).sqrMagnitude < 16) {

                //DAMAGE SPIDER QUEEN
                spiderQueen.Damage(1,0);
                Kill();

            }

        }

	}

    public override void Shot(ShootPackage shootPackage) {

        lifes--;

        /*if (lifes == 1) return;

        base.Shot(shootPackage);
        
        if(lifes == 2) {

            speed = reducedSpeed;

        }else if(lifes == 1) {

            target = spiderQueen.spiderwebBallTarget;

            speed = 50f;
            transform.rotation = Quaternion.LookRotation(target.position - transform.position);

        }*/

    }

    public override void Die() {

        /*Instantiate(toxicExplosion, transform.position, transform.rotation);

        Destroy(gameObject);*/

        target = spiderQueen.spiderwebBallTarget;

        speed = 50f;
        transform.rotation = Quaternion.LookRotation(target.position - transform.position);

    }

    public void DamagePlayer() {

        player.GetComponent<PlayerHealth>().Damage();

        Kill();

    }

    public void Kill() {

        Instantiate(toxicExplosion, transform.position, transform.rotation);

        Destroy(gameObject);

    }

}
