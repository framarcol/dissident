﻿using UnityEngine;
using System.Collections;

public class MissileController : Entity {

    public float speed = 50f;
    public float timeUntilFollow = 2f;
    public float turningSpeed = 50f;

    public GameObject explosion;

    float followTimer;

    Transform player;
    
    public override void Start () {

        base.Start();

        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	void Update () {

        if (player == null || player.GetComponent<PlayerHealth>().dead) Die();

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

        followTimer += Time.deltaTime;
        if (followTimer >= timeUntilFollow) {

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(player.position - transform.position), turningSpeed * Time.deltaTime);

        }

    }

    public override void Die() {

        Instantiate(explosion, transform.position, transform.rotation);

        gameObject.SetActive(false);

    }

}
