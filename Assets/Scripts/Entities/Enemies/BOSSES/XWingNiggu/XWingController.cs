﻿using UnityEngine;
using System.Collections;

public class XWingController : MonoBehaviour {

    Transform player;

    Animator xWingAnimator;
    readonly int rollFactor = Animator.StringToHash("RollFactor");
    readonly int thrusterThreat = Animator.StringToHash("ThrusterThreat");
    readonly int laserTraceModeOn = Animator.StringToHash("LaserTraceModeOn");
    readonly int laserTraceModeOff = Animator.StringToHash("LaserTraceModeOff");
    readonly int laserTraceAttack = Animator.StringToHash("LaserTraceAttack");
    readonly int hit = Animator.StringToHash("Hit");

    public GameObject xWingMissile;
    Transform missileBatteryLeft;
    Transform missileBatteryRight;

    [System.Serializable]
    public class XWingEvent {

        public float time;

        public Vector3 offset;
        public float offsetTime;

        public bool launchMissiles;
        public bool backwards;
        public int missilesNumber;

        public bool laserTrace;
        public float traceTime;

        public bool thrusterExplosion;

        public bool goCritical;
        public float criticalOffsetTime;

    }

    public XWingEvent[] xWingEvents;
    int eventIndex;
    float eventTimer;

    //Damage
    Damage[] damageComponents;

    //Movement
    float movementTimer;
    Vector3 lastPosition;

    //Laser Trace
    float traceTimer;
    bool laserTraceAttacking;

    //Critical
    ThreatManager threatManager;
    Entity xWingEntity;
    int lifes;
    public int maxCrits;
    int crits;
    
    public BossBarController bossBarController;
    public GameObject doorActivator;
    public GameObject explosion;

	void Start () {

        xWingAnimator = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        transform.SetParent(player.parent);

        damageComponents = GetComponentsInChildren<Damage>(true);

        missileBatteryLeft = transform.Find("MissileBatteryLeft");
        missileBatteryRight = transform.Find("MissileBatteryRight");

        threatManager = GameObject.FindGameObjectWithTag("ThreatManager").GetComponent<ThreatManager>();
        xWingEntity = GetComponentInChildren<Entity>();
        lifes = xWingEntity.lifes;

        eventIndex = 0;

        //gameObject.SetActive(false);

	}
	
	void Update () {

        if(eventIndex >= xWingEvents.Length) {

            gameObject.SetActive(false);
            return;

        }

        eventTimer += Time.deltaTime;

        if(eventTimer >= xWingEvents[eventIndex].time) {

            XWingEvent actualEvent = xWingEvents[eventIndex];

            //Movement
            if (actualEvent.offsetTime > 0f) StartCoroutine(GoMove(actualEvent.offset, actualEvent.offsetTime));

            //Thruster
            if (actualEvent.thrusterExplosion) xWingAnimator.SetTrigger(thrusterThreat);
            //Missiles
            if (actualEvent.launchMissiles) StartCoroutine(LaunchMissiles(actualEvent.missilesNumber, actualEvent.backwards));
            //Laser Trace
            if (actualEvent.laserTrace) StartCoroutine(LaserTrace(actualEvent.traceTime));

            //Critical
            if (actualEvent.goCritical) StartCoroutine(StartCritical(actualEvent.criticalOffsetTime));

            eventIndex++;
            eventTimer = 0f;

        }

        //Check life changes
        /*if(xWingEntity.lifes == 0) {

            xWingEntity.lifes = 1;

        }
        */
        /*int actualLifes = xWingEntity.lifes;
        if (lifes != actualLifes) {

            xWingAnimator.SetTrigger(hit);

            if (bossBarController != null) bossBarController.HitBar();

            crits++;

            lifes = actualLifes;

        }*/

	}

    public void AnimateHit() {

        //if (xWingEntity.lifes > 0) return;

        xWingAnimator.SetTrigger(hit);

        if (bossBarController != null) bossBarController.HitBar();

        crits++;

    }

    //Movement
    IEnumerator GoMove(Vector3 offset, float offsetTime) {
        
        while(movementTimer < offsetTime) {

            movementTimer += Time.deltaTime;

            float perc = movementTimer / offsetTime;

            float movePerc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            Vector3 actualPos = movePerc * offset;
            transform.localPosition += actualPos - lastPosition;
            lastPosition = actualPos;

            if (offset.x != 0) {

                float rollPerc = 0.5f + 0.5f * Mathf.Sin((perc * 2f * Mathf.PI) - (Mathf.PI / 2));
                if (offset.x < 0) rollPerc = -rollPerc;
                xWingAnimator.SetFloat(rollFactor, rollPerc);

            }

            yield return null;

        }

        movementTimer = 0f;
        lastPosition = new Vector3(0,0,0);

    }

    //Missiles
    IEnumerator LaunchMissiles(int missilesNumber, bool backwards) {

        for(int i = 0; i < missilesNumber; i++) {

            if (Random.value < 0.5f) {

                Instantiate(xWingMissile, missileBatteryLeft.position, missileBatteryLeft.rotation * ((backwards) ? Quaternion.Euler(0f, 180f, 0f) : Quaternion.identity) * Quaternion.Euler(Random.Range(0f,10f), Random.Range(0f, 10f), 0f));

            } else {

                Instantiate(xWingMissile, missileBatteryRight.position, missileBatteryRight.rotation * ((backwards) ? Quaternion.Euler(0f, 180f, 0f) : Quaternion.identity) * Quaternion.Euler(Random.Range(0f, 10f), Random.Range(0f, 10f), 0f));

            }

            yield return new WaitForSeconds(0.75f);

        }

    }

    //Laser Trace
    IEnumerator LaserTrace(float traceTime) {
        
        float attackTimerOffset = 4f;

        xWingAnimator.SetTrigger(laserTraceModeOn);
        
        while (traceTimer < traceTime) {

            if (!laserTraceAttacking) {

                traceTimer += Time.deltaTime;

                transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(player.transform.localPosition.x, transform.localPosition.y, transform.localPosition.z), 3.5f * Time.deltaTime);

            }

            if (traceTimer > attackTimerOffset) {

                laserTraceAttacking = true;

                xWingAnimator.SetTrigger(laserTraceAttack);

                attackTimerOffset += 4f;

            }

            yield return null;
            
        }

        xWingAnimator.SetTrigger(laserTraceModeOff);
        traceTimer = 0f;

        StartCoroutine(ResetOffsetFromLaserTrace());

    }

    IEnumerator ResetOffsetFromLaserTrace() {
        
        while (transform.localPosition.x >= 0.05f || transform.localPosition.x <= -0.05f) {

            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0f, transform.localPosition.y, transform.localPosition.z), 5f * Time.deltaTime);

            yield return null;

        }

    }

    public void StopLaserTraceAttack() {

        laserTraceAttacking = false;

        ResetDamageComponents();

    }

    private void ResetDamageComponents() {

        for(int i = 0; i < damageComponents.Length; i++) {

            damageComponents[i].ResetDamage();

        }

    }

    //Critical
    IEnumerator StartCritical(float offsetTime) {

        yield return new WaitForSeconds(offsetTime);

        threatManager.CreateThreat(xWingEntity, xWingEntity.transform);

    }

    public void CheckLifes() {

        Debug.Log(crits + " - " + maxCrits);

        if (crits >= maxCrits) {

            gameObject.SetActive(false);

            Instantiate(explosion, transform.position, transform.rotation);

            doorActivator.SetActive(false);

        }
        
    }

}
