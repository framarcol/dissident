﻿using UnityEngine;
using System.Collections;

public class XWingEntity : Entity {

    XWingController xWingController;

    //bool waitingShot;

    bool oldCritical;
    bool vulnerable;

    public override void Start() {

        lifes = 1;

        xWingController = GetComponentInParent<XWingController>();

    }

    void Update() {

        if (!oldCritical && isCritical) {

            vulnerable = true;

        }

        oldCritical = isCritical;

    }

    public override void Shot(ShootPackage shootPackage) {

        if (lifes > 0 && isCritical) {

            lifes = 0;

            //waitingShot = true;

            vulnerable = false;

        }

    }

    public override void Die() {

        //if (waitingShot) {

            xWingController.AnimateHit();

            lifes = 1;

            //waitingShot = false;

        //}

    }
    
}
