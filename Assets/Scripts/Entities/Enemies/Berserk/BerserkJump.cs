﻿using UnityEngine;
using System.Collections;

public class BerserkJump : MonoBehaviour {
    
    Animator berserkController;

    Transform player;

    bool attacking;

	void Start () {

        berserkController = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	void Update () {

        if (attacking) {

            Vector3 direction = (player.position - transform.position).normalized;

            transform.position += direction * 20f * Time.deltaTime;

            transform.rotation = Quaternion.LookRotation(direction);

        }

	}

    public void Jump() {

        attacking = true;

        berserkController.SetTrigger("attack");

        StartCoroutine(Finish());

    }

    IEnumerator Finish() {

        yield return new WaitForSeconds(1.2f);

        attacking = false;

    }

}
