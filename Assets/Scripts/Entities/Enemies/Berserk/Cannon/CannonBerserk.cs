﻿using UnityEngine;
using System.Collections;

public class CannonBerserk : Entity {

    public GameObject cannonRay;

    Animator berserkAnimator;
    
	// Use this for initialization
	public override void Start () {

        base.Start();

        berserkAnimator = GetComponent<Animator>();

	}

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        if (berserkAnimator != null) berserkAnimator.SetTrigger("dead");

        if (cannonRay != null) cannonRay.SetActive(false);

    }

    public void FinishSweep() {

        if (cannonRay != null) cannonRay.SetActive(false);

    }

}
