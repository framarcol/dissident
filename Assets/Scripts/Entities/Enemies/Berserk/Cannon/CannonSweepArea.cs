﻿using UnityEngine;
using System.Collections;

public class CannonSweepArea : MonoBehaviour {

    public Animator berserkAnimator;
    public GameObject cannonRay;

    public bool left = false;
    public bool right = false;

    CannonBerserk cannonBerserk;

    bool entered = false;
    
	// Use this for initialization
	void Start () {

        cannonBerserk = berserkAnimator.GetComponent<CannonBerserk>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {

        if (cannonBerserk.lifes <= 0) return;

        if (!entered && other.tag == "Player") {

            entered = true;

            if (berserkAnimator != null) {

                if(left) berserkAnimator.SetTrigger("sweepLeft");
                else if (right) berserkAnimator.SetTrigger("sweepRight");

            }

            if (cannonRay != null) cannonRay.SetActive(true);

        }

    }

}
