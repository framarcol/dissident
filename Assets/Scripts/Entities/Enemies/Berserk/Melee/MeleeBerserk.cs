﻿using UnityEngine;
using System.Collections;

public class MeleeBerserk : LivingEntity {

    public float chargeSpeed = 6f;
    public float chargeTime = 1f;

    public bool goCharge = false;

    //Animator meleeBerserk;
    
    float chargeTimer = 0f;

    public override void Start() {

        base.Start();

        //meleeBerserk = GetComponent<Animator>();

    }

    void Update() {

        if (goCharge) {

            if (lifes <= 0) return;

            chargeTimer += Time.deltaTime;

            if(chargeTimer >= chargeTime) {

                goCharge = false;
                chargeTimer = 0f;

            }

            //transform.Translate(-transform.forward * chargeSpeed * Time.deltaTime, Space.Self);

            transform.localPosition += new Vector3(0, 0, -chargeSpeed * Time.deltaTime);

        }

    }

    public void GoCharge() {

        entityAnimator.SetTrigger("attack");
        
    }

    public void JumpCharge() {

        goCharge = true;

    }

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        //meleeBerserk.SetTrigger("dead");

        base.Die();

    }

}
