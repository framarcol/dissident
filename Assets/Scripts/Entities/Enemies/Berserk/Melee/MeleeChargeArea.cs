﻿using UnityEngine;
using System.Collections;

public class MeleeChargeArea : MonoBehaviour {

    public MeleeBerserk meleeBerserk;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if (meleeBerserk != null && meleeBerserk.lifes > 0) meleeBerserk.GoCharge();

        }

    }

}
