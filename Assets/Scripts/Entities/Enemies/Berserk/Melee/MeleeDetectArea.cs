﻿using UnityEngine;
using System.Collections;

public class MeleeDetectArea : MonoBehaviour {

    public MeleeBerserk meleeBerserk;

    public float speed = 5f;
    
    Transform player;

    Animator berserkMeleeAnimator;

    bool entered = false;

	// Use this for initialization
	void Start () {

        berserkMeleeAnimator = meleeBerserk.GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {

        if (entered && meleeBerserk.lifes > 0) {

            if (meleeBerserk.goCharge) return;

            float diff = (player.transform.localPosition.x + 4) - (meleeBerserk.transform.localPosition.x + 4);
            
            if(diff > 0.05f) {

                //meleeBerserk.transform.Translate(meleeBerserk.transform.right * speed * Time.deltaTime, Space.Self);

                meleeBerserk.transform.localPosition += new Vector3(speed * Time.deltaTime, 0, 0);

            }else if(diff < -0.05f) {

                //meleeBerserk.transform.Translate(-meleeBerserk.transform.right * speed * Time.deltaTime, Space.Self);

                meleeBerserk.transform.localPosition += new Vector3(-speed * Time.deltaTime, 0, 0);

            }

            berserkMeleeAnimator.SetFloat("sideMove", diff);
            
        }

	}

    void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            entered = true;

            player = other.transform;

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            enabled = false;

        }

    }

}
