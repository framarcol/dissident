﻿using UnityEngine;
using System.Collections;

public class ThrowRockBerserk : LivingEntity {

    public GameObject snowball;
    public float throwForce;

    public float chargeSpeed = 15f;
    public float chargeTime = 0.75f;
    float chargeTimer;

    bool prepareCharge = false;
    bool goCharge = false;

    Transform player;

    Rigidbody snowballRigidbody;
    //bool thrown;

	// Use this for initialization
	public override void Start () {

        base.Start();

        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	// Update is called once per frame
	void Update () {

        if (prepareCharge) {
            
            Vector3 playerDirection = (player.position - transform.position);
            playerDirection.y = 0;

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(playerDirection), 10f * Time.deltaTime);

        } else if (goCharge) {

            if (lifes <= 0) return;

            chargeTimer += Time.deltaTime;

            if (chargeTimer >= chargeTime) {

                goCharge = false;
                chargeTimer = 0f;

            }

            //transform.Translate(-transform.forward * chargeSpeed * Time.deltaTime, Space.Self);

            //transform.localPosition += new Vector3(0, 0, -chargeSpeed * Time.deltaTime);

            transform.Translate(transform.forward * chargeSpeed * Time.deltaTime, Space.World);

        }

    }

    public void AnimThrowRock() {

        entityAnimator.SetTrigger("throw");

    }

    public void ThrowRock() {

        snowball.transform.parent = null;

        snowballRigidbody = snowball.GetComponent<Rigidbody>();

        snowballRigidbody.isKinematic = false;

        snowballRigidbody.AddForce((player.position - snowball.transform.position).normalized * throwForce, ForceMode.Impulse);

        //thrown = true;

    }

    public void GoCharge() {

        entityAnimator.SetTrigger("attack");

        prepareCharge = true;

    }

    public void JumpCharge() {

        Vector3 futurePlayerPosition = player.position + player.transform.forward * 7.5f;

        Vector3 playerDirection = (futurePlayerPosition - transform.position);
        playerDirection.y = 0;

        transform.rotation = Quaternion.LookRotation(playerDirection);

        prepareCharge = false;
        goCharge = true;

    }

    public void Disable() {

        snowball.SetActive(false);
        enabled = false;

    }

    public override void Die() {

        snowball.SetActive(false);

        base.Die();

    }

}
