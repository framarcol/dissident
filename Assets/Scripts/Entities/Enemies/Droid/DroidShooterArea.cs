﻿using UnityEngine;
using System.Collections;

public class DroidShooterArea : BreakableEntity {

    public Animator animator;

    public GameObject damageArea;
    public GameObject aimArea;

    public GameObject explosion;

    public GameObject leftMuzzleFlash;
    public GameObject rightMuzzleFlash;

    public GameObject parachute;
    bool fly;

    void Update() {

        if(fly && parachute != null) parachute.transform.position += new Vector3(0, 1, 0) * 30f * Time.deltaTime;

    }

    public override void Die() {
        
        if (damageArea != null) damageArea.SetActive(false);
        if (aimArea != null) aimArea.SetActive(false);

        GetComponent<Collider>().enabled = false;

        animator.enabled = false;

        explosion.SetActive(true);

        if (parachute != null) fly = true;

        base.Die();

    }

    public void Aim() {

        animator.SetTrigger("Aim");

    }

    public void Shoot() {

        animator.SetTrigger("Shoot");

        if(leftMuzzleFlash != null && rightMuzzleFlash != null) StartCoroutine(ShowMuzzleFlashes());

    }

    IEnumerator ShowMuzzleFlashes() {

        leftMuzzleFlash.SetActive(true);
        rightMuzzleFlash.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        leftMuzzleFlash.SetActive(false);
        rightMuzzleFlash.SetActive(false);

    }

}
