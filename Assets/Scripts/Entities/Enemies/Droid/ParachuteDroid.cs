﻿using UnityEngine;
using System.Collections;

public class ParachuteDroid : MonoBehaviour {

    public float speedDown;

    Entity droid;

    bool enableDown;

	void Start () {

        droid = GetComponent<Entity>();

	}
	
	void Update () {

        if (enableDown) {

            //if (droid.lifes > 0)

                transform.position += new Vector3(0, -1, 0) * speedDown * Time.deltaTime;

            /*else

                transform.position += new Vector3(0, 1, 0) * 1.5f * speedDown * Time.deltaTime;
                */
            if (transform.localPosition.y <= 0) enableDown = false;
            
        }

	}

    public void EnableParachuteDown() {

        enableDown = true;

    }

}
