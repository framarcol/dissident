﻿using UnityEngine;
using System.Collections;

public class DronController : Entity {

    public GameObject explosion;
    public GameObject laserShoot;

    public Animator dronAnimator;

    public Transform healthBarPos;

    public float forwardSpeed = 20f;
    public float sideSpeed;
    public float verticalSpeed;

    Vector3 lerpedMovement;

    Transform player;
    PlayerHealth playerHealth;
    Transform graphics;

    float previousLifes;

    static readonly int forwardSpeedParameter = Animator.StringToHash("forwardSpeed");
    static readonly int sideSpeedParameter = Animator.StringToHash("sideSpeed");
    static readonly int hitParameter = Animator.StringToHash("hit");

    [System.Serializable]
    public struct DronEvent {

        public float time;
        public float forwardSpeed;
        public float sideSpeed;
        public float verticalSpeed;

        public bool shoot;

    }
    public DronEvent[] dronEvents;

    float timer;
    int eventIndex = 0;

    EnemyInfoUI enemyInfoUI;
    UnityEngine.UI.Slider healthBar;

	public override void Start () {

        base.Start();
        
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = player.GetComponent<PlayerHealth>();

        graphics = transform.Find("Graphics");

        previousLifes = lifes;

        if (lifes == 2) return;

        //Get the health bar from the manager
        enemyInfoUI = GameObject.FindGameObjectWithTag("EnemyInfoManager").GetComponent<EnemyInfoUI>();
        healthBar = enemyInfoUI.GetEnemyHealthBar();
        healthBar.maxValue = lifes;
        healthBar.value = healthBar.maxValue;

    }

    public override void Shot(ShootPackage shootPackage) {

        if (isCritical) {

            base.Shot(shootPackage);

            if (healthBar != null) healthBar.value = lifes;

        }

    }

    public override void Die() {

        gameObject.SetActive(false);

        Instantiate(explosion, transform.position, transform.rotation);

        if(healthBar != null) enemyInfoUI.DisposeEnemyHealthBar(healthBar);

    }

    void Update () {
        
        EventControl();

        Vector3 movement = (transform.forward * forwardSpeed) + (transform.right * sideSpeed) + (transform.up * verticalSpeed);
        lerpedMovement = Vector3.Lerp(lerpedMovement, movement, 2f * Time.deltaTime);
        transform.Translate(lerpedMovement * Time.deltaTime, Space.World);

        graphics.LookAt(player);

        dronAnimator.SetFloat(forwardSpeedParameter, lerpedMovement.z / Mathf.Abs(movement.z));
        dronAnimator.SetFloat(sideSpeedParameter, lerpedMovement.x / Mathf.Abs(movement.x));

        if (previousLifes != lifes) dronAnimator.SetTrigger(hitParameter);
        previousLifes = lifes;
        
    }

    void LateUpdate() {

        if (healthBar != null) healthBar.transform.position = Camera.main.WorldToScreenPoint(healthBarPos.position);

    }

    void EventControl() {
        
        timer += Time.deltaTime;

        if(timer >= dronEvents[eventIndex].time) {

            forwardSpeed = dronEvents[eventIndex].forwardSpeed;
            sideSpeed = dronEvents[eventIndex].sideSpeed;
            verticalSpeed = dronEvents[eventIndex].verticalSpeed;

            if (dronEvents[eventIndex].shoot) StartCoroutine(DamageCoroutine());

            timer = 0f;
            eventIndex++;

        }

        if (eventIndex >= dronEvents.Length) {

            gameObject.SetActive(false);

            if (healthBar != null) enemyInfoUI.DisposeEnemyHealthBar(healthBar);

        }

    }

    IEnumerator DamageCoroutine() {

        threatManager.CreateThreat(this, threatTarget);

        float oldLifes = lifes;

        yield return new WaitForSeconds(1f);

        if (oldLifes == lifes) {

            Transform laserOrigin = transform.Find("LaserOrigin");

            LaserShoot lS = ((GameObject)Instantiate(laserShoot, laserOrigin.position, laserOrigin.rotation)).GetComponent<LaserShoot>();
            lS.target = player;

            playerHealth.Damage();

        }

    }

}
