﻿using UnityEngine;

[System.Serializable]
public class FlyingShipEvent {

    public float eventTime;
    [Space]
    public float targetSpeed;
    [Space]
    public float sideMovement;
    public float sideTime;
    [Space]
    public bool goShoot;
    public Entity entity;

}

public class FlyingShip : MonoBehaviour {

    public float targetSpeed = 25f;
    float speed;

    public FlyingShipEvent[] flyingShipEvents;
    float eventTimer;
    int eventIndex = 0;

    bool goSideMove = false;
    float sideTimer;
    float sideMovement;
    float sideTime;
    float oldDistance;

    float totalTimer;
    float oldSine;

	// Use this for initialization
	void Start () {

        speed = targetSpeed;

        gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

        totalTimer += Time.deltaTime;

        speed = Mathf.Lerp(speed, targetSpeed, 5f * Time.deltaTime);

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);


        if(flyingShipEvents.Length > eventIndex) {
            
            eventTimer += Time.deltaTime;
            if (eventTimer > flyingShipEvents[eventIndex].eventTime) {
              
                targetSpeed = flyingShipEvents[eventIndex].targetSpeed;

                if (flyingShipEvents[eventIndex].sideMovement != 0) {
                    sideMovement = flyingShipEvents[eventIndex].sideMovement;
                    sideTime = flyingShipEvents[eventIndex].sideTime;
                    
                    sideTimer = 0f;
                    oldDistance = 0f;
                    goSideMove = true;
                }

                if (flyingShipEvents[eventIndex].goShoot) {

                    flyingShipEvents[eventIndex].entity.Invoke("GoActive", 0f);

                }

                eventTimer = 0f;
                eventIndex++;
                
            }

        }


        if (goSideMove) {

            sideTimer += Time.deltaTime;

            if (sideTimer > sideTime) {

                sideTimer = sideTime;

                goSideMove = false;

            }

            float percentage = sideTimer / sideTime;
            float anglePercentage = -0.5f * Mathf.Cos(2f * Mathf.PI * percentage) + 0.5f;
            
            percentage = percentage * percentage * (3f - 2f * percentage);

            float deltaSideDistance = sideMovement * percentage;

            transform.Translate(transform.right * (deltaSideDistance - oldDistance), Space.World);

            oldDistance = deltaSideDistance;

            //VEHICLE ANGLE
            float actualAngle = (sideMovement < 0) ? 15f * anglePercentage : -15f * anglePercentage;
            transform.Find("Graphics").rotation = Quaternion.AngleAxis(actualAngle, transform.forward);
            
        }


        float actualSine = 0.5f * Mathf.Sin(totalTimer);
        transform.position += new Vector3(0, actualSine - oldSine, 0);
        oldSine = actualSine;

    }

    public void GoActive() {

        gameObject.SetActive(true);

    }

}
