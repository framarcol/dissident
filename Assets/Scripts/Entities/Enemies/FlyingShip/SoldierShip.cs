﻿using UnityEngine;
using System.Collections;

public class SoldierShip : LivingEntity {
    
    GameObject shield;
    PlayerHealth playerHealth;
    BasicShoot basicShoot;

    bool active;
    float damageTimer;

    public bool slow;
    public float targetTimeScale;
    public bool freeze;

    HUDWarning hudWarning;
    GameObject critWarning;

    GameObject waitText;
    GameObject shootText;

    bool oldCritical = false;

    // Use this for initialization
    public override void Start () {

        base.Start();

        playerHealth = FindObjectOfType<PlayerHealth>();
        basicShoot = playerHealth.GetComponent<BasicShoot>();
        shield = transform.Find("Shield").gameObject;

        hudWarning = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<HUDWarning>();

        if(slow || freeze) {

            waitText = hudWarning.transform.Find("WaitText").gameObject;
            shootText = hudWarning.transform.Find("ShootText").gameObject;

        }

    }
	
	// Update is called once per frame
	void Update () {

        if (active) {

            damageTimer += Time.deltaTime;

            if(damageTimer >= 1.5f) {

                if(critWarning != null) critWarning.gameObject.SetActive(false);
                if (shootText != null) shootText.SetActive(false);

                if (lifes <= 0) return;

                entityAnimator.SetTrigger("shoot");

                playerHealth.Damage();

                shield.SetActive(true);

                active = false;

                if (slow) Time.timeScale = 1f;

                return;
                
            }

            if (freeze) {

                if (damageTimer > 0.55f && lifes > 0) {

                    critWarning.gameObject.SetActive(true);
                    critWarning.transform.position = Camera.main.WorldToScreenPoint(threatTarget.position);

                    if (waitText != null) waitText.SetActive(false);
                    if (shootText != null) shootText.SetActive(true);

                    Time.timeScale = 0f;

                    if (!basicShoot.canShoot) basicShoot.canShoot = true;

                }

            } else if (slow) {

                if (damageTimer > 0.55f && lifes > 0) {

                    if (isCritical) {

                        critWarning.gameObject.SetActive(true);
                        critWarning.transform.position = Camera.main.WorldToScreenPoint(threatTarget.position);

                        if (waitText != null) waitText.SetActive(false);
                        if (shootText != null) shootText.SetActive(true);

                        if (!basicShoot.canShoot) basicShoot.canShoot = true;

                    }

                } else if (damageTimer > 1.5f) {
                    critWarning.gameObject.SetActive(false);
                    if (shootText != null) shootText.SetActive(false);
                }

            }

        }

        if(!isCritical && oldCritical) {

            if (critWarning != null) critWarning.gameObject.SetActive(false);

        }

        oldCritical = isCritical;

	}

    public override void Shot(ShootPackage shootPackage) {

        if (isCritical) {

            if (freeze || slow) {

                critWarning.gameObject.SetActive(false);
                if (shootText != null) shootText.SetActive(false);
                if (waitText != null) waitText.SetActive(false);
                Time.timeScale = 1f;

                active = false;

                if (freeze) basicShoot.canShoot = false;

            }

            base.Shot(shootPackage);
            
        }

    }

    public void GoActive() {

        critWarning = hudWarning.GetCritWarning();

        if (slow) {
            Time.timeScale = targetTimeScale;
            if(waitText != null) waitText.SetActive(true);
        }

        shield.SetActive(false);

        entityAnimator.SetTrigger("aimIn");

        threatManager.CreateThreat(this, threatTarget);

        active = true;

    }

    public override void Die() {

        active = false;

        base.Die();

    }

}
