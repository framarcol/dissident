﻿using UnityEngine;
using System.Collections;

public class TurretShip : Entity {

    public GameObject explosionEffect;
    public GameObject smokeEffect;
    public GameObject halos;

    bool active;
    float damageTimer;
    PlayerHealth playerHealth;

    public bool slow;
    public float targetTimeScale;
    public bool freeze;

    HUDWarning hudWarning;
    GameObject critWarning;

    bool oldCritical;
    
    public override void Start () {

        base.Start();

        playerHealth = FindObjectOfType<PlayerHealth>();

        hudWarning = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<HUDWarning>();
        
    }
	
	void Update () {

        if (active) {

            Vector3 targetLook = new Vector3(playerHealth.transform.position.x, threatTarget.position.y, playerHealth.transform.position.z) - threatTarget.position;
            threatTarget.rotation = Quaternion.RotateTowards(threatTarget.transform.rotation, Quaternion.LookRotation(targetLook), 100f * Time.deltaTime);

            damageTimer += Time.deltaTime;

            if (damageTimer >= 1.5f) {

                if(critWarning != null) critWarning.gameObject.SetActive(false);

                if (lifes <= 0) return;

                playerHealth.Damage();
                
                halos.SetActive(false);

                active = false;

                if (slow) Time.timeScale = 1f;

                return;

            }

            if (freeze) {

                if (damageTimer > 0.55f && lifes > 0) {

                    critWarning.gameObject.SetActive(true);
                    critWarning.transform.position = Camera.main.WorldToScreenPoint(threatTarget.position);

                    Time.timeScale = 0f;

                }

            } else if (slow) {

                if (damageTimer > 0.55f) {

                    if (isCritical) {

                        critWarning.gameObject.SetActive(true);
                        critWarning.transform.position = Camera.main.WorldToScreenPoint(threatTarget.position);

                    }

                } else if (damageTimer >= 1.5f) critWarning.gameObject.SetActive(false);

            }

        }

        if (!isCritical && oldCritical) {

            if (critWarning != null) critWarning.gameObject.SetActive(false);

        }

        oldCritical = isCritical;

    }

    public override void Shot(ShootPackage shootPackage) {

        if (isCritical) {

            if (freeze || slow) {

                critWarning.gameObject.SetActive(false);
                Time.timeScale = 1f;

                active = false;

            }

            base.Shot(shootPackage);

        }
        
    }

    public void GoActive() {

        critWarning = hudWarning.GetCritWarning();

        if (slow) Time.timeScale = targetTimeScale;

        halos.SetActive(true);

        threatManager.CreateThreat(this, threatTarget);

        active = true;

    }

    public override void Die() {

        active = false;

        explosionEffect.SetActive(true);
        smokeEffect.SetActive(true);

        halos.SetActive(false);

    }

}
