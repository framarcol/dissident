﻿using UnityEngine;

public class LaserTraceArea : MonoBehaviour {

    public LaserTracer laserTracer;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            laserTracer.gameObject.SetActive(true);
            laserTracer.transform.parent = other.transform.parent;
            laserTracer.player = other.transform;

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            laserTracer.GoDeactivate();

        }
        
    }

}
