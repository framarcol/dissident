﻿using UnityEngine;
using System.Collections;

public class LaserTracer : MonoBehaviour {

    public Transform player;

    public float setupTime = 2.5f;

    public bool ready = false;
    public bool preparing = false;

    public bool playerInside = false;

    float setupTimer = 0f;

    Animator laserAnimator;
    
	void Start () {

        laserAnimator = GetComponent<Animator>();

	}

	void Update () {

        if (!preparing) {

            Vector3 newPos = Vector3.Lerp(transform.position, player.position, 1f * Time.deltaTime);
            newPos.y = transform.position.y;
            //newPos.z = transform.pl

            transform.position = newPos;
            //transform.localPosition += new Vector3(0, 0, 1);

        }

        if (!ready) {

            setupTimer += Time.deltaTime;

            if(setupTimer > setupTime) {

                ready = true;
                setupTimer = 0f;

            } else {

                return;

            }

        }

	}

    public void Shoot() {

        preparing = false;

        if (playerInside) {

            player.GetComponent<PlayerHealth>().Damage();

        }

    }

    public void GoDeactivate() {

        laserAnimator.SetTrigger("deactivate");

        Destroy(gameObject, 0.5f);

    }

    /*void OnTriggerEnter(Collider other) {
        
        

    }*/

    void OnTriggerStay(Collider other) {

        if (other.tag == "Player") playerInside = true;

        if (ready && !preparing && other.tag == "Player") {

            preparing = true;
            ready = false;

            laserAnimator.SetTrigger("prepare&shoot");

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player")  playerInside = false;

    }

}
