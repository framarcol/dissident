﻿using UnityEngine;
using System.Collections;

public class LilDragonAttackArea : MonoBehaviour {

    public LilDragonController lilDragonController;

    bool entered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            lilDragonController.attack = true;
            lilDragonController.lastAttackPosition = other.transform.position;
            lilDragonController.entityAnimator.SetTrigger("attackBlow");

        }

    }

}
