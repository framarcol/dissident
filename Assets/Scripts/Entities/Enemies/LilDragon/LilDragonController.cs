﻿using UnityEngine;
using System.Collections;

public class LilDragonController : LivingEntity {

    public float speed = 25f;

    public bool falling = false;
    public bool attack = false;
    public bool flee = false;

    public Transform attackTarget;
    public Vector3 lastAttackPosition;

    Damage damageArea;

    public override void Start() {

        base.Start();

        damageArea = GetComponentInChildren<Damage>();

    }

    // Update is called once per frame
    void Update () {

        if (lifes <= 0 && damageArea.enabled) {
            falling = false;
            attack = false;
            flee = false;
            entityAnimator.SetTrigger("idle");
            damageArea.enabled = false;
        }

        if (flee) {

            transform.Translate((transform.up) * speed * Time.deltaTime);
            
            return;

        }

        if (attack) {

            Vector3 playerDirection = (lastAttackPosition - transform.position);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(playerDirection), 250f * Time.deltaTime);

            //transform.localPosition += transform.rotation * new Vector3(0, 0, speed * Time.deltaTime);

            transform.Translate(playerDirection.normalized * speed * Time.deltaTime, Space.World);

            if(playerDirection.sqrMagnitude < 1f) {

                flee = true;

                StartCoroutine(Deactivate(1.5f));

            }

            return;

        }

        if (falling) {

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(attackTarget.position - transform.position), 250f * Time.deltaTime);

            //transform.localPosition += /*transform.rotation **/ new Vector3(0, 0, speed * Time.deltaTime);

            transform.Translate((attackTarget.position - transform.position).normalized * speed * Time.deltaTime, Space.World);

        }

	}

    IEnumerator Deactivate(float seconds) {

        yield return new WaitForSeconds(seconds);

        gameObject.SetActive(false);

    }

    /*public override void Die() {

        //GetComponent<Collider>().enabled = false;

        //GetComponent<Rigidbody>().isKinematic = false;

        entityAnimator.SetTrigger("dead");

    }*/

}
