﻿using UnityEngine;
using System.Collections;

public class LilDragonFallArea : MonoBehaviour {

    public LilDragonController lilDragonController;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            lilDragonController.falling = true;
            lilDragonController.attackTarget = other.transform;
            lilDragonController.entityAnimator.SetTrigger("attackFall");

        }

    }

    void OnTriggerExit(Collider other) {

        if (entered && other.tag == "Player") {

            lilDragonController.gameObject.SetActive(false);

        }

    }

}
