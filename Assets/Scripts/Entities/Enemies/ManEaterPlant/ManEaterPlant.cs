﻿using UnityEngine;
using System.Collections;

public class ManEaterPlant : MonoBehaviour {

    public bool plantEnabled;

    Transform plant;
    Animator plantAnimator;

    Transform player;

    public ItemBox itemSpawn;
    
	void Start () {

        plant = transform.Find("Graphics");
        plantAnimator = plant.GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        
	}
	
	void Update () {

        if (plantEnabled) {
            
            plant.rotation = Quaternion.RotateTowards(plant.rotation, Quaternion.LookRotation(player.position - transform.position), 2.5f);

        }

	}

    public void EnablePlant() {

        plantEnabled = true;

        plantAnimator.SetTrigger("lift");

    }

    public void DisablePlant() {

        plantEnabled = false;

        if(itemSpawn != null) StartCoroutine(SpawnItem());

    }

    IEnumerator SpawnItem() {

        yield return new WaitForSeconds(1f);

        itemSpawn.Die();

    }

    public void AttackPlant() {

        plantAnimator.SetTrigger("attack");

    }

}
