﻿using UnityEngine;
using System.Collections;

public class MechaWorm : MonoBehaviour {

    public MechaWormPiece[] pieces;

    int pieceIndex = 0;

    //public float actualPosition = 0;
    
    float wavePeriodTimer;

    float followPeriodTimer;
    float followPeriodTime = 5f;

    bool killState = false;

    Transform player;

	// Use this for initialization
	void Start () {

        pieces = transform.GetComponentsInChildren<MechaWormPiece>();

        //actualPosition = pieces[pieceIndex].transform.localPosition.x;
        pieces[pieceIndex].activePiece = true;

        player = GameObject.FindGameObjectWithTag("Player").transform;

        for(int i = 0; i < pieces.Length; i++) {

            pieces[i].gameObject.SetActive(false);

        }

        enabled = false;

	}
	
	// Update is called once per frame
	void Update () {

        if (player.GetComponent<PlayerHealth>().dead) enabled = false;

        float distanceToPlayer = transform.InverseTransformPoint(player.transform.position).z - pieces[pieceIndex].transform.localPosition.z;
        if(distanceToPlayer < 2.5f) {

            if (pieces[pieceIndex].speed != 20f) {

                for (int i = 0; i < pieces.Length; i++) {

                    pieces[i].speed = 20f;

                }

                followPeriodTime = 1f;
                killState = true;

            }

        }

        if (!killState) {

            wavePeriodTimer += Time.deltaTime;
            if (wavePeriodTimer > 3f) {

                StartCoroutine(WavePieces());
                wavePeriodTimer = 0f;

            }


            if (pieces[pieceIndex].hit) {

                pieces[pieceIndex].hit = false;

                float randomDistance = GetRandomDistance();
                StartCoroutine(MovePieces(randomDistance));

            }

        }

        followPeriodTimer += Time.deltaTime;
        if(followPeriodTimer > followPeriodTime) {

            StartCoroutine(MovePieces(GetFollowDistance()));
            followPeriodTimer = 0f;

        }
        
        if (pieces[pieceIndex].lifes == 0) {

            pieceIndex++;

            if (pieceIndex > pieces.Length - 1) enabled = false;
            else {

                pieces[pieceIndex].activePiece = true;

            }

        }
        
	}

    private float GetRandomDistance() {

        float firstDistance = Random.Range(2f, 4f);
        if (Random.value >= 0.5f) firstDistance = -firstDistance;

        float targetDistance = pieces[pieceIndex].transform.localPosition.x + firstDistance;

        if (targetDistance < -4f) firstDistance = -4f - firstDistance;
        else if(targetDistance > 4f) targetDistance = 4f - firstDistance;

        //actualPosition = transform.localPosition.x + firstDistance;

        return firstDistance;

    }

    private float GetFollowDistance() {

        float distance = Mathf.Abs(player.localPosition.x - pieces[pieceIndex].transform.localPosition.x);

        if (player.localPosition.x < pieces[pieceIndex].transform.localPosition.x) distance = -distance;

        //actualPosition += distance;

        return distance;

    }

    private IEnumerator MovePieces(float distance) {

        for (int i = 0; i < pieces.Length; i++) {

            pieces[i].GoMove(distance);

            yield return new WaitForSeconds(0.05f);

        }

    }

    private IEnumerator WavePieces() {

        for (int i = 0; i < pieces.Length; i++) {

            pieces[i].GoWave();

            yield return new WaitForSeconds(0.05f);

        }

    }

    public void EnableMechaWorm() {

        enabled = true;

        for (int i = 0; i < pieces.Length; i++) {

            pieces[i].gameObject.SetActive(true);

        }

    }

}
