﻿using UnityEngine;
using System.Collections;

public class MechaWormPiece : BreakableEntity {

    public float speed;

    public bool activePiece = false;

    public bool hit = false;

    public GameObject explosion;
    public GameObject halos;

    bool move = false;
    float distance;
    float moveTimer;
    float oldDistance;
    float oldAngle;

    bool wave = false;
    float waveTimer;

    Animator animator;

    // Use this for initialization
    public override void Start () {

        base.Start();

        animator = GetComponent<Animator>();

        if (transform.GetSiblingIndex() % 2 == 0) animator.SetFloat("attackOffset", 0.5f);

	}

    public override void Shot(ShootPackage shootPackage) {

        if (activePiece && !move) {

            base.Shot(shootPackage);

            hit = true;

        }

    }

    // Update is called once per frame
    void Update () {

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
        
	}

    void LateUpdate() {

        //SIDE MOVE LOGIC
        if (move) {

            moveTimer += Time.deltaTime;
            if (moveTimer > 0.4f) {

                moveTimer = 0.4f;

                animator.SetTrigger("attack");
                move = false;

            }

            float perc = moveTimer / 0.4f;
            perc = Mathf.Sin(Mathf.PI * 0.5f * perc);

            float actualDistance = perc * distance;
            float deltaDistance = actualDistance - oldDistance;
            oldDistance = actualDistance;

            transform.Translate(transform.right * deltaDistance, Space.World);
            
            float actualAngle = perc * ((distance > 0) ? 360f : -360f);
            float deltaAngle = actualAngle - oldAngle;
            oldAngle = actualAngle;

            threatTarget.Rotate(threatTarget.transform.right, deltaAngle, Space.World);

            //threatTarget.transform.rotation *= Quaternion.AngleAxis(deltaAngle, threatTarget.transform.right);



            if (moveTimer == 0.4f) {

                moveTimer = 0f;
                oldDistance = 0f;
                oldAngle = 0f;

            }

        }

        //WAVE MOVE LOGIC
        if (wave) {

            waveTimer += Time.deltaTime;
            if (waveTimer > 0.5f) {

                waveTimer = 0.5f;

                wave = false;

            }

            float perc = waveTimer / 0.5f;
            perc = Mathf.Sin(Mathf.PI * perc);

            threatTarget.Translate(threatTarget.transform.forward * perc, Space.World);

            if(waveTimer == 0.5f) {

                waveTimer = 0f;
                
            }

        }

    }

    public void GoMove(float distance) {

        if (move) return;

        this.distance = distance;
        move = true;

        animator.SetTrigger("shrink");

    }

    public void GoWave() {

        if(!move) wave = true;

    }

    public override void Die() {

        base.Die();

        //transform.Find("Graphics").gameObject.SetActive(false);
        //GetComponent<Collider>().enabled = false;

        explosion.SetActive(true);
        halos.SetActive(false);

        enabled = false;

    }

}
