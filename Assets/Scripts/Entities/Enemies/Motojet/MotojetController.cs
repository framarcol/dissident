﻿using UnityEngine;
using System.Collections;

public class MotojetController : Entity {

    public Vector3 movementOffset;
    public float time;

    public bool ready;

    //public bool goShoot = false;

    public Animator motojetAnimator;
    public SoldierMotojetController soldierMotojetController;
    public Animator soldierMotojetAnimator;

    public GameObject explosion;

    float moveTimer;
    Vector3 deltaVector = Vector3.zero;
    Vector3 oldVector = Vector3.zero;

    float sideBlend = 0f;

    bool alreadyShot = false;
    bool oldCritical = false;

    //ThreatManager threatManager;

    /*public override void Start() {

        base.Start();

        threatManager = FindObjectOfType<ThreatManager>();

    }*/

    // Update is called once per frame
    void Update () {

        sideBlend = Mathf.Lerp(sideBlend, 10 * deltaVector.x, 5 * Time.deltaTime);
        if (motojetAnimator != null) motojetAnimator.SetFloat("sideBlend", sideBlend);
        if (soldierMotojetAnimator != null) soldierMotojetAnimator.SetFloat("sideBlend", sideBlend);

        if (ready) {

            moveTimer += Time.deltaTime;

            if (moveTimer >= time) {

                moveTimer = time;
                ready = false;

            }

            float perc = moveTimer / time;
            perc = Mathf.Sin(Mathf.PI * perc * 0.5f);

            Vector3 percVector = perc * movementOffset;

            deltaVector = percVector - oldVector;
            oldVector = percVector;

            transform.localPosition += deltaVector;
            
            if (!ready) {

                moveTimer = 0f;
                oldVector = Vector3.zero;
                deltaVector = Vector3.zero;

            }

        }

        if (oldCritical != isCritical) alreadyShot = false;
        oldCritical = isCritical;

	}

    public void SetMoveEvent(MotojetSpawner.MotojetEvents motojetEvent) {

        if (lifes <= 0) return;

        if (motojetEvent.moveTime > 0) {

            movementOffset = motojetEvent.movement;
            time = motojetEvent.moveTime;
            ready = true;

        }

        if (motojetEvent.shoot) {
            
            threatManager.CreateThreat(this, threatTarget);

        }

    }

    public override void Shot(ShootPackage shootPackage) {

        if (isCritical) {

            if (alreadyShot) return;
            alreadyShot = true;

            soldierMotojetController.TakeHit();

            base.Shot(shootPackage);

        }

    }

    public override void Die() {

        GetComponent<Collider>().enabled = false;
        transform.Find("MotojetObstacle").GetComponent<Collider>().enabled = false;

        Instantiate(explosion, transform.position, transform.rotation);

        soldierMotojetController.Die();

        motojetAnimator.enabled = false;
        motojetAnimator.GetComponentInChildren<Rigidbody>().isKinematic = false;
        motojetAnimator.GetComponentInChildren<Rigidbody>().AddForce(motojetAnimator.transform.up * 150f, ForceMode.Impulse);

        transform.SetParent(null);

        Destroy(gameObject, 4f);

        /*if (motojetAnimator != null) motojetAnimator.SetTrigger("dead");
        if (soldierMotojetAnimator != null) soldierMotojetAnimator.SetTrigger("dead");

        StartCoroutine(GoDie());*/
        
    }

    IEnumerator GoDie() {

        yield return new WaitForSeconds(1.1f);

        movementOffset = new Vector3(0, -3, -20);
        time = 3f;
        ready = true;

        moveTimer = 0f;
        oldVector = Vector3.zero;
        deltaVector = Vector3.zero;

        yield return new WaitForSeconds(time);

        Destroy(gameObject);

    }

}
