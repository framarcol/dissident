﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MotojetSpawner : MonoBehaviour {

    public GameObject motojet;

    public GameObject cameraArmChanger;

    public enum startPositions { DEFAULT, LEFT, RIGHT, BACK }
    public startPositions startPosition;

    [Range(1f,10f)]
    public float interpolationMultiplier = 6f;

    [System.Serializable]
    public class MotojetEvents {

        public float waitTime;

        public Vector3 movement;
        public float moveTime;

        public bool shoot;

        public bool changeCamera;
        public CameraArmPositions cameraArmPosition;
        [Range(1f,10f)]
        public float interpolationMultiplier = 6f;

    }

    public List<MotojetEvents> motojetEvents;

    MotojetController motojetController;
    SoldierMotojetController soldierMotojetController;

    //bool active = false;
    bool active = false;

    float feedTimer;
    int eventIndex = 0;
    CameraArmPositions actualCameraPosition;

	// Use this for initialization
	void Start () {

        if (startPosition != startPositions.DEFAULT) {

            ChangeCameraArm changer = ((GameObject)Instantiate(cameraArmChanger, transform.position, transform.rotation)).GetComponent<ChangeCameraArm>();
            changer.transform.parent = transform;

            switch (startPosition) {

                case startPositions.LEFT:
                    changer.armPosition = CameraArmPositions.Right;
                    changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = 0;
                    changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 0;
                    actualCameraPosition = CameraArmPositions.Right;
                    break;

                case startPositions.RIGHT:
                    changer.armPosition = CameraArmPositions.Left;
                    changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = 0;
                    changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 0;
                    actualCameraPosition = CameraArmPositions.Left;
                    break;

                case startPositions.BACK:
                    changer.armPosition = CameraArmPositions.Forward;
                    actualCameraPosition = CameraArmPositions.Forward;
                    break;

            }

            changer.smoothChangeMultiplier = interpolationMultiplier;

        }

        float totalTime = 0f;

        foreach (MotojetEvents motojetEvent in motojetEvents) {

            totalTime += motojetEvent.waitTime;

            if (motojetEvent.changeCamera) {

                ChangeCameraArm changer = Instantiate(cameraArmChanger).GetComponent<ChangeCameraArm>();
                changer.transform.parent = transform;

                changer.transform.localPosition = new Vector3(0, 0, totalTime * 20);

                switch (motojetEvent.cameraArmPosition) {

                    case CameraArmPositions.Default:
                        changer.armPosition = CameraArmPositions.Default;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = -4;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 4;
                        break;

                    case CameraArmPositions.Left:
                        changer.armPosition = CameraArmPositions.Left;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = 0;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 0;
                        break;

                    case CameraArmPositions.Right:
                        changer.armPosition = CameraArmPositions.Right;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = 0;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 0;
                        break;

                    case CameraArmPositions.Forward:
                        changer.armPosition = CameraArmPositions.Forward;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().leftSideLimit = -4;
                        changer.transform.GetChild(0).GetComponent<SideLimitController>().rightSideLimit = 4;
                        break;

                }

                changer.smoothChangeMultiplier = motojetEvent.interpolationMultiplier;

            }

        }

	}
	
	// Update is called once per frame
	void Update () {
        
        if (active) {

            if (eventIndex > motojetEvents.Count - 1) {
                if(motojetController.lifes > 0) StartCoroutine(Finish(motojetEvents[eventIndex-1].moveTime));
                active = false;
                return;
            }

            feedTimer += Time.deltaTime;

            if(feedTimer >= motojetEvents[eventIndex].waitTime) {

                motojetController.SetMoveEvent(motojetEvents[eventIndex]);

                /*if (motojetEvents[eventIndex].moveTime > 0) {

                    motojetController.movementOffset = motojetEvents[eventIndex].movement;
                    motojetController.time = motojetEvents[eventIndex].moveTime;
                    motojetController.ready = true;

                }*/
                
                if (motojetEvents[eventIndex].shoot) {

                    soldierMotojetController.ShootAnim(actualCameraPosition);

                }

                if (motojetEvents[eventIndex].changeCamera) {

                    actualCameraPosition = motojetEvents[eventIndex].cameraArmPosition;

                }

                feedTimer = 0f;
                eventIndex++;

            }

        }

	}

    void OnTriggerEnter(Collider other) {

        if(!active && other.tag == "Player") {

            motojetController = ((GameObject)Instantiate(motojet, transform.position, transform.rotation, other.transform.parent)).GetComponent<MotojetController>();
            //motojetController.transform.parent = other.transform.parent;

            //motojet.transform.rotation = transform.rotation;

            switch (startPosition) {

                case startPositions.DEFAULT:
                    motojetController.transform.localPosition = new Vector3(0, 1, -8);
                    break;

                case startPositions.LEFT:
                    motojetController.transform.localPosition = new Vector3(-5, 1, -8);
                    break;

                case startPositions.RIGHT:
                    motojetController.transform.localPosition = new Vector3(5, 1, -8);
                    break;

                case startPositions.BACK:
                    motojetController.transform.localPosition = new Vector3(0, 1, -8);
                    break;

            }

            soldierMotojetController = motojetController.GetComponentInChildren<SoldierMotojetController>();

            active = true;



        }

    }

    IEnumerator Finish(float seconds) {

        yield return new WaitForSeconds(seconds);

        Destroy(motojetController.gameObject);
        enabled = false;

    }

}


