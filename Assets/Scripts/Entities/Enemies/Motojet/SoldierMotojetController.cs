﻿using UnityEngine;
using System.Collections;

public class SoldierMotojetController : LivingEntity {

    public int damage = 1;

    public GameObject leftWeaponHalo;
    public GameObject rightWeaponHalo;

    MotojetController motojetController;
    PlayerHealth playerHealth;

    Animator soldierMotojetAnimator;

	// Use this for initialization
	public override void Start () {

        base.Start();

        motojetController = GetComponentInParent<MotojetController>();
        playerHealth = FindObjectOfType<PlayerHealth>();
        
        soldierMotojetAnimator = GetComponent<Animator>();

    }

    public void ShootAnim(CameraArmPositions actualCameraPosition) {
        
        switch (actualCameraPosition) {

            case CameraArmPositions.Left:
                soldierMotojetAnimator.SetTrigger("shootLeftSide");
                leftWeaponHalo.SetActive(true);
                break;

            case CameraArmPositions.Right:
                soldierMotojetAnimator.SetTrigger("shootRightSide");
                rightWeaponHalo.SetActive(true);
                break;

            case CameraArmPositions.Forward:

                if (motojetController.transform.localPosition.x > playerHealth.transform.localPosition.x) {

                    soldierMotojetAnimator.SetTrigger("shootLeftForward");
                    leftWeaponHalo.SetActive(true);

                } else {

                    soldierMotojetAnimator.SetTrigger("shootRightForward");
                    rightWeaponHalo.SetActive(true);

                }

                break;

            case CameraArmPositions.Default:

                if (motojetController.transform.localPosition.x > playerHealth.transform.localPosition.x) {

                    soldierMotojetAnimator.SetTrigger("shootLeftBack");
                    leftWeaponHalo.SetActive(true);

                } else {

                    soldierMotojetAnimator.SetTrigger("shootRightBack");
                    rightWeaponHalo.SetActive(true);

                }

                break;

        }
        
    }

    public void ShootPlayer() {

        playerHealth.Damage(damage);

        leftWeaponHalo.SetActive(false);
        rightWeaponHalo.SetActive(false);

    }

    public void TakeHit() {

        if(motojetController.transform.localPosition.x > playerHealth.transform.localPosition.x) soldierMotojetAnimator.SetTrigger("hitLeft");
        else soldierMotojetAnimator.SetTrigger("hitRight");

        leftWeaponHalo.SetActive(false);
        rightWeaponHalo.SetActive(false);

    }

}
