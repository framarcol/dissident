﻿using UnityEngine;
using System.Collections;

public class RagdollController : MonoBehaviour {

    //public GameObject topRagdollBone;

    public float lifeTime = 6f;

    float lifeTimer = 0f;
	
	// Update is called once per frame
	void Update () {

        if (lifeTime == -1) { enabled = false; return; }

        lifeTimer += Time.deltaTime;

        if(lifeTimer > lifeTime) {

            gameObject.SetActive(false);

        }

	}

}
