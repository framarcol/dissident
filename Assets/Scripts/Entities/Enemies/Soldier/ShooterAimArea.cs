﻿using UnityEngine;
using System.Collections;

public class ShooterAimArea : ThreatArea {

    public Animator shooterSoldier;

    public override void Detected() {

        shooterSoldier.SetTrigger("aim");

    }

}
