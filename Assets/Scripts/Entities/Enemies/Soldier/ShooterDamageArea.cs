﻿using UnityEngine;

public class ShooterDamageArea : Damage {

    public Animator shooterSoldier;

    public override void OnHit() {
      
        if (shooterSoldier != null) shooterSoldier.SetTrigger("shoot");

    }

}
