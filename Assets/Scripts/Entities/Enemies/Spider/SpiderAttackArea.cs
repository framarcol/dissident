﻿using UnityEngine;
using System.Collections;

public class SpiderAttackArea : MonoBehaviour {

    public SpiderController spiderController;
    
    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            spiderController.prepare = true;

            //if (spiderController.fromBack) spiderController.attackPositionFromBack = other.transform.position;

            spiderController.attackTarget = other.transform;

            spiderController.entityAnimator.SetTrigger("jumpAttack");

        }

    }

    public void Attack() {

        spiderController.attack = true;

    }

    public void Stop() {

        spiderController.stop = true;

    }

}
