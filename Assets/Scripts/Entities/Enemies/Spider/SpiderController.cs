﻿using UnityEngine;
using System.Collections;

public class SpiderController : LivingEntity {

    public float speed = 10f;

    public bool fromBack = false;
    public bool sides = false;

    [Space]

    public bool running = false;
    public bool prepare = false;
    public bool attack = false;
    public bool stop = false;

    [Space]

    public Transform attackTarget;
    //public Vector3 attackPositionFromBack;

    //public Animator spiderAnimator;
    public Transform rootBone;

    SphereCollider sphereCollider;

	public override void Start () {

        base.Start();

        sphereCollider = GetComponent<SphereCollider>();

        if (fromBack) gameObject.SetActive(false);
        
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 localRootBonePosition = transform.InverseTransformPoint(rootBone.position);
        sphereCollider.center = localRootBonePosition;

        if (stop) return;
        
        if (attack) {
            
            if (!fromBack) transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(attackTarget.position - transform.position), 250f * Time.deltaTime);
            else transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(attackTarget.position - transform.position), 250f * Time.deltaTime);

            if (!fromBack) transform.position += (attackTarget.position - transform.position).normalized * 20f * Time.deltaTime;
            else transform.position += (attackTarget.position - transform.position).normalized * 50f * Time.deltaTime;

            return;

        }

        if (prepare) {

            if(!sides) transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(attackTarget.position - transform.position), 250f * Time.deltaTime);

            return;

        }

        if (running) {
            
            if(!fromBack)
                transform.localPosition += new Vector3(0, 0, -speed * Time.deltaTime);
            else
                transform.localPosition += new Vector3(0, 0, speed * Time.deltaTime);

            return;

        }

	}

    public override void Die() {
        
        running = false;
        
        sphereCollider.enabled = false;

        stop = true;

        if (sides) {

            base.Die();

        } else {

            entityAnimator.SetTrigger("dead");

        }
        
    }

}
