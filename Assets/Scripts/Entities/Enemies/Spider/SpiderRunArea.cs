﻿using UnityEngine;
using System.Collections;

public class SpiderRunArea : MonoBehaviour {

    public SpiderController spiderController;

    bool entered = false;
    
    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if (!spiderController.gameObject.activeSelf) spiderController.gameObject.SetActive(true);
            
            spiderController.running = true;

        }

    }

    void OnTriggerExit(Collider other) {

        if(entered && other.tag == "Player") {

            if(!spiderController.fromBack)
                spiderController.gameObject.SetActive(false);

        }

    }

}
