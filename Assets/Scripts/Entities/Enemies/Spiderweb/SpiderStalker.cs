﻿using UnityEngine;
using System.Collections;

public class SpiderStalker : MonoBehaviour {

    public float speed = 2f;

    Animator animator;

    PlayerHealth playerHealth;

    // Use this for initialization
	void Start () {

        animator = GetComponent<Animator>();

        playerHealth = FindObjectOfType<PlayerHealth>();

	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

	}

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            animator.SetTrigger("eat");
            enabled = false;

        }

    }

    public void PlayHit() {

        playerHealth.GetComponent<PlayerEffectStatus>().hitEffect.Play();

    }

}
