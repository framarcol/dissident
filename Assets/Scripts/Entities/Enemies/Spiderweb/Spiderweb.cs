﻿using UnityEngine;

public class Spiderweb : MonoBehaviour {

    public GameObject spiderStalker;

    public static int stuckMeasure;

    PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {

        if(stuckMeasure != 0) stuckMeasure = 0;

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

	}

    public void AddStuckMeasure() {

        stuckMeasure++;
        //Debug.Log(stuckMeasure);

        if(stuckMeasure >= 3) {

            playerHealth.KillStuckPlayer();
            EnableSpiderStalker();

        } else {

            playerHealth.hit = true;

            if (stuckMeasure == 1) playerHealth.spiderwebPiece1.SetActive(true);
            else if (stuckMeasure == 2) playerHealth.spiderwebPiece2.SetActive(true);

        }

    }

    public static void ClearStuckMeasure() {

        stuckMeasure = 0;

    }

    private void EnableSpiderStalker() {

        spiderStalker.SetActive(true);

        spiderStalker.transform.localPosition = new Vector3(playerHealth.transform.localPosition.x, spiderStalker.transform.localPosition.y, spiderStalker.transform.localPosition.z);

    }

}
