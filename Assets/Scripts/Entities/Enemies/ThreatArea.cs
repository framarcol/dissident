﻿using UnityEngine;
using System.Collections;

public class ThreatArea : MonoBehaviour {

    public Entity targetEntity;
    public Transform targetThreatPosition;

    ThreatManager threatManager;

    HairCrossUI hairCross;

    bool entered = false;

	void Start () {

        threatManager = GameObject.FindGameObjectWithTag("ThreatManager").GetComponent<ThreatManager>();

	}

    //void Update() {

    //    if(hairCross != null) {

    //        targetEntity.isCritical = hairCross.isCritical;

    //    }/* else {

    //        targetEntity.isCritical = false;

    //    }*/

    //}

    void OnTriggerEnter(Collider other) {

        if (targetEntity.lifes <= 0) return;

        if(!entered && other.tag == "Player") {

            entered = true;

            if(threatManager != null) {

                hairCross = threatManager.CreateThreat(targetEntity, targetThreatPosition);

                Detected();

            }

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            enabled = false;

        }

    }

    public virtual void Detected() { }

}
