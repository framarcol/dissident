﻿using UnityEngine;
using System.Collections;

public class Turret : Entity {

    public Transform turretHead;
    public Transform target;

    public GameObject halo;
    public GameObject charge;
    public GameObject smoke;
    public GameObject explosion;
    public AudioClip[] explosionSounds;

    public GameObject projectile;
    public AudioClip[] shootSounds;

    public GameObject turretDamage;

    AudioSource audioSource;

    private bool _active;
    public bool active {

        get { return _active; }
        set {

            _active = value;

            if (value) {

                turretAnimator.SetTrigger("engage");
                halo.SetActive(true);
                if(charge != null) charge.SetActive(true);
                

            } else {

                halo.SetActive(false);
                if (charge != null) charge.SetActive(false);

            }

        }

    }

    Animator turretAnimator;

	// Use this for initialization
	public override void Start () {

        base.Start();

        turretAnimator = GetComponent<Animator>();

        audioSource = GetComponentInChildren<AudioSource>(true);

	}
	
	// Update is called once per frame
	void Update () {
        
        if (active) {

            turretHead.rotation = Quaternion.RotateTowards(turretHead.rotation, Quaternion.LookRotation(target.position - turretHead.position) * Quaternion.AngleAxis(90f, turretHead.up), 20f * Time.deltaTime);

        }

	}

    public void Shoot() {

        turretAnimator.SetTrigger("shoot");

        if (charge != null) charge.SetActive(false);

        if (projectile != null) StartCoroutine(ShootProjectile(100f));
        else GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().Damage();

        if (shootSounds.Length > 0) audioSource.PlayOneShot(shootSounds[Random.Range(0, shootSounds.Length)]);

    }

    IEnumerator ShootProjectile(float speed) {

        projectile.SetActive(true);

        Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        Vector3 trajectory = player.position - projectile.transform.position;

        while (trajectory.sqrMagnitude > 0.5f) {

            projectile.transform.position += trajectory.normalized * speed * Time.deltaTime;

            trajectory = player.position - projectile.transform.position;

            yield return null;

        }

        player.GetComponent<PlayerHealth>().Damage();

        projectile.SetActive(false);

    }

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        active = false;
        turretAnimator.SetTrigger("disengage");

        if (charge != null) charge.SetActive(false);
        turretDamage.SetActive(false);
        explosion.SetActive(true);
        smoke.SetActive(true);

        turretHead.SetParent(null);
        Rigidbody rb = turretHead.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        //rb.AddExplosionForce(1000f, transform.position, 7f, 10f, ForceMode.Impulse);
        //Vector3 forceDirection = turretHead.transform.up * ;
        rb.AddForce(turretHead.transform.up * Random.Range(250f,750f), ForceMode.Impulse);
        rb.AddTorque(turretHead.transform.right * Random.Range(-20f, 20f), ForceMode.Impulse);

        if (explosionSounds.Length > 0) audioSource.PlayOneShot(explosionSounds[Random.Range(0, explosionSounds.Length)]);

    }

}
