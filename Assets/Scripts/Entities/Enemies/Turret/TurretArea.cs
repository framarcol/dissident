﻿using UnityEngine;
using System.Collections;

public class TurretArea : MonoBehaviour {

    bool entered = false;

    public Turret turret;

    AudioSource audioSource;

	//Use this for initialization
	void Start () {

        Turret turretInstance = GetComponentInChildren<Turret>();

        if (turret == null) turret = turretInstance;

        audioSource = GetComponentInChildren<AudioSource>(true);
       
    }

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            if (turret == null) return;

            if (turret.lifes <= 0) return;

            entered = true;

            turret.active = true;
            turret.target = other.transform;

            if (audioSource != null) audioSource.enabled = true;

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            turret.active = false;

            if (audioSource != null) audioSource.enabled = false;

        }

    }

}
