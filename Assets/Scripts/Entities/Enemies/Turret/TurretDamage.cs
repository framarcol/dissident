﻿using UnityEngine;
using System.Collections;

public class TurretDamage : MonoBehaviour {

    public Turret turret;

    bool entered = false;

    void OnTriggerEnter(Collider choque) {

        if (!entered && choque.tag == "Player") {

            if (turret != null) turret.Shoot();

            entered = true;

        }

    }

    /*public override void OnHit() {

        

    }*/

}
