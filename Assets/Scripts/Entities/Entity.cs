﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {
    
    public int lifes = 1;
    
    public float score = 0f;
    public bool addMultiplierHit;

    public bool isCritical = false;

    public bool isDetectableThreat = false;
    public Transform threatTarget;
    public Renderer entityRenderer;

    //public GameObject hitEffect;
    //public GameObject critEffect;

    protected Vector3 impactPoint;
    protected int accumulatedMultiplier = 0;

    protected ScoreController scoreController;
    protected ThreatManager threatManager;

    public virtual void Start() {

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        threatManager = GameObject.FindGameObjectWithTag("ThreatManager").GetComponent<ThreatManager>();

    }

    //Override this to perform something when shot (always override this if inheriting the class)
    public virtual void Shot(ShootPackage shootPackage) {

        impactPoint = shootPackage.hitPoint;

        //Display hit effects
        /*if (isCritical) {

            if (critEffect != null) {

                Instantiate(critEffect, impactPoint, Quaternion.LookRotation(Camera.main.transform.position - impactPoint));
                
            }

        } else {

            if (hitEffect != null) {

                Instantiate(hitEffect, impactPoint, Quaternion.LookRotation(Camera.main.transform.position - impactPoint));
                
            }

        }*/

        //Damage logic
        if (lifes <= 0) return;

        int damage = shootPackage.damage;
        if (isCritical) damage *= 2;

        if (scoreController != null && addMultiplierHit) {

            if (damage > lifes) {

                accumulatedMultiplier += lifes;

            } else {

                accumulatedMultiplier += damage;

            }

        }

        lifes -= damage;
        
    }

    public void GetScore() {

        if (lifes <= 0) {

            if (scoreController != null) {

                scoreController.AddHit(accumulatedMultiplier);
                scoreController.AddPoints(score);


            }
            
        }

    }

    //Override this to perform something different when the entity dies
    public virtual void Die() {

        gameObject.SetActive(false);

    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "ThreatDetectionArea" && isDetectableThreat) {

            threatManager.CreateDetection(this, threatTarget);
            
        }

    }

}
