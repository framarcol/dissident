﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[System.Serializable]
public class EntityEvent : UnityEvent { }

public class GenericEntity : Entity {

    public bool reactOnCritOnly = false;

    public EntityEvent onDieEvent;

    public override void Shot(ShootPackage shootPackage) {

        if (reactOnCritOnly) {

            if (isCritical) base.Shot(shootPackage);

        } else {

            base.Shot(shootPackage);

        }

    }

    public override void Die() {

        onDieEvent.Invoke();

    }

}
