﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

[ExecuteInEditMode(), RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HaloCustom : MonoBehaviour {

    public Color32 color = Color.white;

    Mesh haloQuad;

	// Use this for initialization
	void Start () {

        haloQuad = CreateQuadMesh(color);
        GetComponent<MeshFilter>().mesh = haloQuad;

        GetComponent<MeshRenderer>().sharedMaterial = Resources.Load("HaloCustom", typeof(Material)) as Material;

        GetComponent<MeshFilter>().hideFlags = HideFlags.HideInInspector;
        GetComponent<MeshRenderer>().hideFlags = HideFlags.None;
        GetComponent<MeshRenderer>().sharedMaterial.hideFlags = HideFlags.HideInInspector;

    }
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(Camera.main.transform.position);

        if (!Application.isPlaying) {

            haloQuad = CreateQuadMesh(color);
            GetComponent<MeshFilter>().mesh = haloQuad;

        }

    }

    private Mesh CreateQuadMesh(Color32 color) {

        Mesh mesh = new Mesh();

        mesh.name = "SelfQuad";

        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(-1, +1, 0);
        vertices[1] = new Vector3(+1, +1, 0);
        vertices[2] = new Vector3(-1, -1, 0);
        vertices[3] = new Vector3(+1, -1, 0);
        mesh.vertices = vertices;

        Vector2[] uvs = new Vector2[4];
        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(1, 0);
        uvs[2] = new Vector2(0, 1);
        uvs[3] = new Vector2(1, 1);
        mesh.uv = uvs;

        //int[] indices = new int[] { 0, 1, 2, 3, 2, 1 };
        int[] indices = new int[] { 0, 2, 1, 1, 2, 3 };
        mesh.SetIndices(indices, MeshTopology.Triangles, 0);

        Color32[] colors = new Color32[4];
        colors[0] = color;
        colors[1] = color;
        colors[2] = color;
        colors[3] = color;
        mesh.colors32 = colors;

        mesh.RecalculateNormals();
        return mesh;

    }

}
