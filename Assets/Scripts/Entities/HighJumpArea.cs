﻿using UnityEngine;

public class HighJumpArea : MonoBehaviour {

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            PlayerMovementController playerMovementController = other.GetComponentInParent<PlayerMovementController>();

            if (playerMovementController.jumping) return;

            playerMovementController.ResetMovementFocus();

            playerMovementController.jumping = true;
            
            playerMovementController.jumpHeight = 4f;
            playerMovementController.SetTrigger("highJump");
            
        }

    }

}
