﻿using UnityEngine;
using System.Collections;

public class SpiderTrap : MonoBehaviour {

    public Cloth spiderCloth;

    public Transform[] corners;
    public Transform target;

    public float time = 5f;

    bool enabledTrap;
    int cornersCount;

    float distance;
    float oldDistance;
    float timer;
    
    bool entered;

    PlayerHealth playerHealth;
    ShootController shootController;
    Transform spaceSphere;
    LineRenderer targetLineRenderer;
    
	void Start () {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerHealth>();
        shootController = playerHealth.GetComponent<ShootController>();

        spaceSphere = transform.Find("SpaceSphere");

        targetLineRenderer = target.GetComponent<LineRenderer>();

        cornersCount = corners.Length;

        distance = (target.position - corners[0].position).magnitude;
        
    }
	
	void Update () {

        if (enabledTrap) {

            timer += Time.deltaTime;

            if(timer >= 0.3f) {

                enabledTrap = false;

                if (entered) {

                    for (int i = 0; i < cornersCount; i++) {

                        corners[i].position = playerHealth.transform.position + new Vector3(0,1f,0.5f);
                        corners[i].SetParent(playerHealth.transform);

                    }
                    spiderCloth.transform.SetParent(playerHealth.transform);

                    StartCoroutine(PlayerDisabled());

                    /*Transform bone = playerHealth.KillPlayerRagdoll(new Vector3(0, 0, 0));
                    bone.position = spaceSphere.position;
                    bone.GetComponent<Rigidbody>().isKinematic = true;*/

                }

            }

            float perc = timer / 0.3f;
            float actualDistance = perc * distance;

            for(int i = 0; i < cornersCount; i++) {

                corners[i].position += (target.position - corners[i].position).normalized * (actualDistance - oldDistance);

            }

            oldDistance = actualDistance;

            float width = perc * 0.1f;
            targetLineRenderer.SetWidth(width, width);
            
        }

	}

    public void EnableTrap() {

        spiderCloth.enabled = true;
        targetLineRenderer.enabled = true;

        enabledTrap = true;

    }

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            EnableTrap();

        }

    }

    void OnTriggerExit(Collider other) {

        if (entered && other.tag == "Player") {

            entered = false;

        }

    }

    IEnumerator PlayerDisabled() {

        shootController.canShoot = false;

        yield return new WaitForSeconds(time);

        shootController.canShoot = true;
        for (int i = 0; i < cornersCount; i++) {

            //corners[i].position = playerHealth.transform.position + new Vector3(0, 1f, 0.5f);
            corners[i].SetParent(null);

        }

        yield return new WaitForSeconds(1f);

        spiderCloth.gameObject.SetActive(false);

    }

}
