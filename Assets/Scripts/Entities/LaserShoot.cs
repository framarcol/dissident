﻿using UnityEngine;
using System.Collections;

public class LaserShoot : MonoBehaviour {

    public Transform target;
    
	void Update () {

        if (target != null) {

            transform.LookAt(target.position);

            transform.position += transform.forward * 100f * Time.deltaTime;

            if ((target.position - transform.position).sqrMagnitude < 1f) Destroy(gameObject);

        }

	}

}
