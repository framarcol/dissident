﻿using UnityEngine;
using System.Collections;

public class LiftPiece : MonoBehaviour {

    public float distance = 4f;
    public float liftTime = 4f;

    public bool active = false;
        
    float moveTimer = 0f;
    float oldDistance = 0;
	
	// Update is called once per frame
	void Update () {

        if (active) {

            moveTimer += Time.deltaTime;

            if (moveTimer >= liftTime) {

                moveTimer = liftTime;

            }

            float perc = moveTimer / liftTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            float actualDistance = distance * perc;
            float deltaDistance = actualDistance - oldDistance;

            oldDistance = actualDistance;

            transform.position += transform.up * deltaDistance;

            if (moveTimer == liftTime) active = false;

        }

	}

    public void GoActive() {

        active = true;

    }

}
