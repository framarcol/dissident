﻿using UnityEngine;
using System.Collections.Generic;

public class LivingEntity : Entity {

    public GameObject entityRagdoll;
    public Transform topEntityBone;
    public float force = 500f;

    public Animator entityAnimator;

	/*public override void Start () {
        base.Start();
	}*/

    public override void Die() {

        if(entityRagdoll != null) {

            gameObject.SetActive(false);
            
            ThrowRagdoll();

            return;

        }

        if (entityAnimator != null) entityAnimator.SetTrigger("dead");
        
    }

    protected void ThrowRagdoll() {

        GameObject ragdoll = Instantiate(entityRagdoll);

        TransferPoseAndExplosion(ragdoll);
        
    }

    protected Transform GetRagdollTopBone(Transform ragdollPiece) {

        Transform ragdollTopBone = null;

        foreach (Transform child in ragdollPiece) {

            if(child.name == topEntityBone.name) {

                return child;

            } else {

                ragdollTopBone = GetRagdollTopBone(child);

            }

        }

        return ragdollTopBone;

    }

    protected void TransferPoseAndExplosion(GameObject ragdollInstance) {

        Transform topRagdollBone = GetRagdollTopBone(ragdollInstance.transform);
        
        Transform[] ragdollBones = topRagdollBone.GetComponentsInChildren<Transform>();
        Transform[] entityBones = topEntityBone.GetComponentsInChildren<Transform>();

        for(int i = 0; i < ragdollBones.Length; i++) {
            
            ragdollBones[i].position = entityBones[i].position;
            ragdollBones[i].rotation = entityBones[i].rotation;
            
        }

        if (force == 0) return;

        for (int i = 0; i < ragdollBones.Length; i++) {

            Rigidbody piece = ragdollBones[i].GetComponent<Rigidbody>();

            if (piece != null) {

                piece.AddExplosionForce(force, impactPoint, 10f, 1f, ForceMode.Acceleration);

            }

        }

    }
    
}
