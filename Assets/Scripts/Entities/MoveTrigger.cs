﻿using UnityEngine;
using System.Collections;

public class MoveTrigger : MonoBehaviour {

    public GameObject objectToMove;

    public float speed;
    public float maxDistance;
    
    bool active = false;
    float distance;
    
	void Update () {

        if (active) {
            
            objectToMove.transform.Translate(objectToMove.transform.forward * speed * Time.deltaTime, Space.World);

            distance += speed * Time.deltaTime;
            if (distance >= maxDistance) {
                enabled = false;
            }

        }

	}

    void OnTriggerEnter(Collider other) {

        if(!active && other.tag == "Player") {

            active = true;
            
        }

    }

}
