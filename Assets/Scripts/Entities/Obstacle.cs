﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour {

    public bool isPhysicsObstacle = false;

    //public bool getRagdoll = false;

    bool entered = false;

    AnalyticsManager analyticsManager;

    void Start() {

        analyticsManager = GameObject.FindGameObjectWithTag("AnalyticsManager").GetComponent<AnalyticsManager>();

    }

    //If the player becomes too close to the obstacle then kill it
	void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            entered = true;

            if (isPhysicsObstacle) {

                //Array.ForEach(GetComponentsInChildren<Collider>(), (Collider col) => { col.isTrigger = false; });

                Collider[] colliders = GetComponentsInChildren<Collider>();
                int length = colliders.Length;
                for(int i = 0; i < length; i++) {

                    colliders[i].isTrigger = false;

                }
                
                other.GetComponentInChildren<PlayerHealth>().KillPlayer(true);
                
            } else if (!other.isTrigger)

                other.GetComponentInChildren<PlayerHealth>().KillPlayer();

            analyticsManager.deathByObstacle = true;

        }

    }

    void OnCollisionEnter(Collision col) {

        if(!entered && isPhysicsObstacle && col.collider.tag == "Player") {

            entered = true;

            /*if(!getRagdoll)*/ col.collider.GetComponentInChildren<PlayerHealth>().KillPlayer(true);

            //else col.collider.GetComponentInChildren<PlayerHealth>().KillPlayerRagdoll();

            analyticsManager.deathByObstacle = true;

        }

    }

}
