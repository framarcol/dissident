﻿using UnityEngine;
using System.Collections;

public class AmmoRefill : Pickup {

    public int ammo = 5;
    
    public override void Start() {

        base.Start();

        //graphics.localScale *= (ammo * 0.3f);

    }

    public override void OnTake(Collider playerCollider) {

        playerCollider.GetComponent<ShootController>().AddAmmo(ammo);

    }
    
}
