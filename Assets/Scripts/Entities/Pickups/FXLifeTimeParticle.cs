﻿using UnityEngine;
using System.Collections;

public class FXLifeTimeParticle : MonoBehaviour {

    public float lifeTime = 2f;

	// Use this for initialization
	void Start () {

        Destroy(gameObject, lifeTime);

	}

}
