﻿using UnityEngine;
using System.Collections;

public class Healer : Pickup {

    public bool maxHeal = false;

    public float scoreIfMaxHealth;

    public override void OnTake(Collider playerCollider) {

        PlayerHealth playerHealth = playerCollider.GetComponentInChildren<PlayerHealth>();

        if(playerHealth.lifes == playerHealth.maxLife) {

            scoreController.AddPoints(scoreIfMaxHealth);

        } else {

            if (maxHeal) playerHealth.HealMax();
            else playerHealth.Heal();

        }
        
    }

}
