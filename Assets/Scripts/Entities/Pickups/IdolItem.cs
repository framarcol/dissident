﻿using UnityEngine;
using System.Collections;

public class IdolItem : Pickup {

    [Header("Idol Identification")]
    [Tooltip("Type of the idol, from 1 to 9")]
    public uint type;
    [Tooltip("Fragment of the idol, from 1 to 10")]
    public uint fragment;
    [Tooltip("Icon for this idol")]
    public Sprite idolIcon;

    GameManager gameManager;

	// Use this for initialization
	public override void Start () {

        base.Start();

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

	}

    public override void OnTake(Collider playerCollider) {

        gameManager.IncrementIdolCounter(this);

        if(!gameManager.specialLevel) GameManager.CheckIdol(gameManager.worldId, type - 1, fragment - 1);
        
    }

    void OnValidate() {

        if(type < 1) type = 1;
        else if (type > 9) type = 9;

        if (fragment < 1) fragment = 1;
        else if (fragment > 10) fragment = 10;

    }

}
