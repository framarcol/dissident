﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    public float score;

    public bool spin = false;

    public float speed = 45f;

    public GameObject pickupEffect;

    public AudioClip[] pickupSounds;

    protected Transform graphics;

    protected ScoreController scoreController;

    float sfxVolumeScale;

	// Use this for initialization
	public virtual void Start () {

        graphics = transform.Find("Graphics");

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        sfxVolumeScale = PlayerPrefs.GetFloat(GameManager.SoundEffectVolumePref, 1f);

	}
	
	// Update is called once per frame
	void Update () {

        if (spin) {

            graphics.rotation *= Quaternion.AngleAxis(speed * Time.deltaTime, graphics.up);

            //graphics.Rotate(graphics.up, speed * Time.deltaTime);

        }

	}

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            if (pickupEffect != null) Instantiate(pickupEffect, transform.position, transform.rotation);

            OnTake(other);

            scoreController.AddPoints(score);

            if(pickupSounds != null && pickupSounds.Length > 0) AudioSource.PlayClipAtPoint(pickupSounds[Random.Range(0, pickupSounds.Length)], Camera.main.transform.position, sfxVolumeScale);

            gameObject.SetActive(false);

        }

    }

    public virtual void OnTake(Collider playerCollider) { }

}
