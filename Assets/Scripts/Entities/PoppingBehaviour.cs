﻿using UnityEngine;
using System.Collections;

public class PoppingBehaviour : MonoBehaviour {

    //public bool active = true;

    GameObject graphics;

    MeshRenderer meshRenderer;

    LayerMask mask;

	// Use this for initialization
	void Start () {

        //WARNING!!!!!
        //WARNING!!!!!
        //WARNING!!!!!
        //GetComponent<Collider>().enabled = false;
        /*enabled = false;
        return;*/
        //WARNING!!!!!

        //graphics = transform.Find("Graphics").gameObject;
        //if(graphics != null) graphics.SetActive(false);

        if (transform.Find("Graphics") != null) {

            graphics = transform.Find("Graphics").gameObject;
            graphics.SetActive(false);

        } else { 

        //if(graphics == null) {

            meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.enabled = false;

        }

        mask = LayerMask.GetMask("ControlArea");

	}

    void OnTriggerEnter(Collider other) {

        if (((1 << other.gameObject.layer) & mask) > 0) {

            if (graphics != null) graphics.SetActive(true);
            else meshRenderer.enabled = true;

        }

    }

    void OnTriggerExit(Collider other) {

        if (((1 << other.gameObject.layer) & mask) > 0) {

            if(graphics != null) graphics.SetActive(false);
            else meshRenderer.enabled = false;

        }

    }

}
