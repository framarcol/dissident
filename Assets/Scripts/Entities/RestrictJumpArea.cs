﻿using UnityEngine;
using System.Collections;

public class RestrictJumpArea : MonoBehaviour {

    public bool enable;

    PlayerMovementController playerMovementController;

    bool entered;

	// Use this for initialization
	void Start () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
	
	}

    void OnTriggerEnter(Collider other) {
    
        if(!entered && other.tag == "Player") {

            entered = true;

            playerMovementController.RestrictJump(enable);

        }

    }

}
