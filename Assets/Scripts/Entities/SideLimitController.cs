﻿using UnityEngine;

[ExecuteInEditMode]
public class SideLimitController : MonoBehaviour {

    public float leftSideLimit = -4f;
    public float rightSideLimit = 4f;

#if UNITY_EDITOR

    void Update() {
        
        transform.GetChild(0).localPosition = new Vector3(leftSideLimit, 1, 0);
        transform.GetChild(1).localPosition = new Vector3(rightSideLimit, 1, 0);

    }

#endif

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            other.GetComponentInParent<PlayerMovementController>().ChangeSideLimit(leftSideLimit, rightSideLimit);

        }

    }

    void OnValidate() {

        if(leftSideLimit > rightSideLimit) {

            leftSideLimit = rightSideLimit;

        }else if(rightSideLimit < leftSideLimit) {

            rightSideLimit = leftSideLimit;

        }

    }

}
