﻿using UnityEngine;
using System.Collections;

public class SkyboxController : MonoBehaviour {

    static readonly Quaternion identity = new Quaternion(0, 0, 0, 1);

    GameManager gameManager;

    void Start() {

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        GameManager.OnGameCompleted += Unparent;

    }
    
	void Update () {

        transform.rotation = identity;

	}

    public void Unparent() {

        transform.parent = null;

    }

}
