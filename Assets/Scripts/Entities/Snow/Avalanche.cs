﻿using UnityEngine;
using System.Collections;

public class Avalanche : Entity {

    public GameObject snowCloud;
    public GameObject obstacle;

    public DroidShooterArea droidShooterArea;
    public bool gotSoldier;

    Animator avalancheAnimator;

	// Use this for initialization
	public override void Start () {

        base.Start();

        avalancheAnimator = GetComponent<Animator>();

        if (!gotSoldier) droidShooterArea.transform.parent.gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Die() {

        avalancheAnimator.SetTrigger("fall");

        obstacle.SetActive(true);
        
        if (gotSoldier) {

            ShootPackage package = new ShootPackage();
            package.damage = droidShooterArea.lifes;

            droidShooterArea.Shot(package);

        }

    }

    public void PlaySnowCloud() {

        snowCloud.SetActive(true);

    }

    public void TriggerFall() {

        if (!gotSoldier) {

            Die();

        }

    }

}
