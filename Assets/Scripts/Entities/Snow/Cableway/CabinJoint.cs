﻿using UnityEngine;
using System.Collections;

public class CabinJoint : Entity {

    public Cableway cableway;

    public GameObject fullCabin;
    public GameObject badCabin;
    public GameObject brokenCabin;

    public GameObject cabinHitParticles;

    Rigidbody cabinRigidbody;

    public override void Start() {

        base.Start();

        cabinRigidbody = GetComponent<Rigidbody>();

    }

    public override void Die() {

        transform.parent = null;

        cabinRigidbody.isKinematic = false;

        if (!cableway.broken) {

            cableway.soldierDoor.GetComponent<LivingEntity>().Die();
            cableway.soldierWindow.GetComponent<LivingEntity>().Die();

        }

        GetComponent<Collider>().enabled = false;

    }

    void OnCollisionEnter(Collision col) {

        if (col.collider.GetType() == typeof(MeshCollider)) {

            fullCabin.SetActive(false);
            badCabin.SetActive(false);
            brokenCabin.SetActive(true);

            cabinHitParticles.SetActive(true);

            scoreController.AddHit(2);
            scoreController.AddPoints(200f);

        }

    }

    public void ChangeCabin(bool bad) {

        if (bad) {
            fullCabin.SetActive(false);
            badCabin.SetActive(true);
        } else {
            fullCabin.SetActive(true);
            badCabin.SetActive(false);
        }

    }

}
