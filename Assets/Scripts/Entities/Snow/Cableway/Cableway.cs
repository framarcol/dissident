﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Cableway : MonoBehaviour {

    public Transform cable;
    public Transform cabinBase;
    public Transform cabin;

    public GameObject soldierDoor;
    public GameObject soldierDoorAreas;

    public GameObject soldierWindow;
    public GameObject soldierWindowAreas;

    public CabinJoint cabinJoint;
    public GameObject fallArea;

    [Range(0, 180f)]
    public float cablewayRotation = 90f;

    [Range(-35f, 35f)]
    public float cablewayAngle;

    [Range(-50f, 50f)]
    public float cabinOffset;
    
    public float cabinSpeed;

    //public bool enableSoldierDoor = true;
    //public bool enableSoldierWindow = true;

    public bool broken = false;

    Quaternion cabinStartRotation; //ANGLE => x: 337.3362

    bool active = false;

    // Use this for initialization
    void Start () {

        cabinStartRotation = cabin.rotation;

    }
	
	// Update is called once per frame
	void Update () {
        
        if (!Application.isPlaying) {

            cabin.rotation = cabinStartRotation;
            cabin.rotation *= Quaternion.AngleAxis(cablewayRotation - 90f, transform.up);

            cable.rotation = Quaternion.AngleAxis(cablewayRotation, transform.up) * Quaternion.AngleAxis(cablewayAngle, transform.right);

            cabinBase.transform.localPosition = new Vector3(0, 0, cabinOffset);

            if (broken) {

                //enableSoldierDoor = false;
                //enableSoldierWindow = false;

                cabinSpeed = 0f;

                fallArea.SetActive(true);

                cabinJoint.ChangeCabin(true);

            } else {

                fallArea.SetActive(false);

                cabinJoint.ChangeCabin(false);

            }

            /*soldierDoor.SetActive(enableSoldierDoor);
            soldierDoorAreas.SetActive(enableSoldierDoor);

            soldierWindow.SetActive(enableSoldierWindow);
            soldierWindowAreas.SetActive(enableSoldierWindow);*/

        } else if (active) {

            cabinBase.Translate(transform.forward * cabinSpeed * Time.deltaTime, Space.Self);

            if (cabinBase.localPosition.z > 50 || cabinBase.localPosition.z < -50) enabled = false;

        }

	}

    public void GoActive() {

        active = true;

    }

}
