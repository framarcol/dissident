﻿using UnityEngine;
using System.Collections;

public class IcebergController : MonoBehaviour {

    public float speed;

    public float duration;
    float durationTimer;

    GameObject graphics;
    
	void Start () {

        graphics = transform.Find("Graphics").gameObject;

	}
	
	void Update () {

        if (graphics.activeSelf) {

            durationTimer += Time.deltaTime;
            if(durationTimer >= duration) {

                enabled = false;
                return;

            }

            transform.Translate(transform.right * speed * Time.deltaTime, Space.World);

        }
	
	}

}
