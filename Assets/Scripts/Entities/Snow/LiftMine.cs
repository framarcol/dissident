﻿using UnityEngine;
using System.Collections;

public class LiftMine : Entity {

    public GameObject damageArea;
    public GameObject explosion;

    Transform mine;
    float targetHeight = -0.26f;
    bool goUp = false;

	// Use this for initialization
	public override void Start () {

        base.Start();

        mine = transform.Find("Graphics");

	}
	
	// Update is called once per frame
	void Update () {

        if(goUp) mine.localPosition = Vector3.Lerp(mine.localPosition, new Vector3(mine.localPosition.x, targetHeight, mine.localPosition.z), 10f * Time.deltaTime);

	}

    public void LiftMineUp() {

        goUp = true;

    }

    public override void Shot(ShootPackage shootPackage) {

        if(goUp) base.Shot(shootPackage);

    }

    public override void Die() {

        explosion.SetActive(true);
        damageArea.SetActive(false);

        mine.gameObject.SetActive(false);

    }

}
