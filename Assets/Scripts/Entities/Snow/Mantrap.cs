﻿using UnityEngine;
using System.Collections;

public class Mantrap : MonoBehaviour {

    public Transform focusRight;
    public Transform focusLeft;

    public Animator animator;

    bool entered = false;

    Transform bone;
    float timer;

	// Use this for initialization
	/*void Start () {

        //animator = GetComponent<Animator>();

	}*/
	
	// Update is called once per frame
	void Update () {

        if (bone != null) {

            bone.localPosition = Vector3.zero;

            timer += Time.deltaTime;
            if (timer > 0.2f) enabled = false;

        }

	}

    public void GoShut() {

        animator.SetTrigger("shut");

    }

    void OnCollisionEnter(Collision col) {
        
        if(!entered && col.collider.tag == "Player") {

            entered = true;

            col.collider.GetComponent<PlayerHealth>().Damage();

            /*bone = col.collider.GetComponent<PlayerHealth>().KillPlayerRagdoll(Vector3.zero);

            if (col.collider.transform.localPosition.x < 0) bone.parent = focusRight;
            else if (col.collider.transform.localPosition.x >= 0) bone.parent = focusLeft;*/

        }

    }

}
