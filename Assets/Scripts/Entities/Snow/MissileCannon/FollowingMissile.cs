﻿using UnityEngine;
using System.Collections;

public class FollowingMissile : Entity {

    public float speed = 20f;

    public float timeUntilFollow = 2f;

    public float turningSpeed = 50f;

    public GameObject explosion;

    public float threatTime = 2f;

    Transform player;
    float followTimer;

    bool threatened = false;

	// Use this for initialization
	public override void Start () {

        base.Start();

        player = GameObject.FindGameObjectWithTag("Player").transform;

        //Invoke("Die", lifeTime);

	}

    // Update is called once per frame
    void Update() {

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

        followTimer += Time.deltaTime;
        if (followTimer >= timeUntilFollow) {

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(player.position - transform.position), turningSpeed * Time.deltaTime);

        }

        if (!threatened && followTimer >= threatTime) {

            threatManager.CreateThreat(this, threatTarget);

            threatened = true;

        }

	}

    public override void Die() {

        entityRenderer.enabled = false;

        explosion.SetActive(true);

        enabled = false;

    }

    void OnTriggerEnter(Collider other) {

        if(other.GetType() == typeof(TerrainCollider)) {

            Die();

        }

    }

}
