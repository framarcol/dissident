﻿using UnityEngine;
using System.Collections;

public class MissileCannon : MonoBehaviour {

    public GameObject leftMissile;
    public GameObject rightMissile;



    Animator animator;

	// Use this for initialization
	void Start () {

        animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShootMissiles() {

        if (leftMissile != null) {
            leftMissile.transform.parent = null;
            leftMissile.SetActive(true);
        }

        if (rightMissile != null) {
            rightMissile.transform.parent = null;
            rightMissile.SetActive(true);
        }

    }

    public void Engage() {

        animator.SetTrigger("engage");

    }

}
