﻿using UnityEngine;
using System.Collections;

public class SnowWind : MonoBehaviour {

    public float targetStrenght;
    
    WindZone windZone;

	// Use this for initialization
	void Start () {

        windZone = GetComponent<WindZone>();

	}
	
	// Update is called once per frame
	void Update () {

        windZone.windMain = Mathf.Lerp(windZone.windMain, targetStrenght, 5f * Time.deltaTime);
	
	}

    public void GoRight() {

        transform.rotation = Quaternion.LookRotation(transform.parent.right);

        targetStrenght = 20f;

    }

    public void GoLeft() {

        transform.rotation = Quaternion.LookRotation(-transform.parent.right);

        targetStrenght = 20f;

    }

    public void Disable() {

        targetStrenght = 0f;

    }

}
