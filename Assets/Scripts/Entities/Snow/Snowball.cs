﻿using UnityEngine;
using System.Collections;

public class Snowball : MonoBehaviour {

    public float followForce = 5f;

    public GameObject snowParticle;

    public GameObject snowballBreak;

    Rigidbody snowball;
    Transform player;

	// Use this for initialization
	void Start () {

        snowball = GetComponent<Rigidbody>();

        gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        snowball.AddForce((player.position - transform.position).normalized * followForce, ForceMode.Acceleration);

	}

    public void GoActive() {

        player = GameObject.FindGameObjectWithTag("Player").transform;

        gameObject.SetActive(true);

    }

    public void GoDisable() {

        gameObject.SetActive(false);

    }

    void OnCollisionEnter(Collision col) {

        snowParticle.SetActive(true);

    }

    void OnCollisionStay(Collision col) {

        snowParticle.transform.position = col.contacts[0].point;

    }

    public void GoBreak() {

        Instantiate(snowballBreak, transform.position, transform.rotation);

    }

    /*void OnCollisionExit(Collision col) {

        snowParticle.SetActive(false);

    }*/

}
