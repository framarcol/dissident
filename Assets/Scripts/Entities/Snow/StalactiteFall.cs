﻿using UnityEngine;
using System.Collections;

public class StalactiteFall : MonoBehaviour {

    public GameObject snowHitParticle;

    bool entered = false;

    void Start() {



    }
    
    void OnCollisionEnter(Collision col) {

        if (GetComponent<Rigidbody>().isKinematic) return;

        //if(!entered && col.collider.GetType() == typeof(TerrainCollider)) {

            entered = true;

            snowHitParticle.SetActive(true);

        //}

        StartCoroutine(TriggerDeactivation());

    }

    IEnumerator TriggerDeactivation() {

        yield return new WaitForSeconds(2.5f);

        gameObject.SetActive(false);

    }

}
