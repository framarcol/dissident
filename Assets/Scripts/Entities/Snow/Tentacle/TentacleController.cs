﻿using UnityEngine;
using System.Collections;

public class TentacleController : MonoBehaviour {

    public GameObject waterSplash;
    public GameObject attackWaterSplash;

    public Entity tentacle;

    Animator animator;
    PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {

        animator = GetComponent<Animator>();
        playerHealth = FindObjectOfType<PlayerHealth>();

	}
	
	// Update is called once per frame
	void Update () {
	    
        if(tentacle.lifes <= 0) {

            animator.SetTrigger("hit");

            enabled = false;

        }

	}

    public void Lift() {

        animator.SetTrigger("lift");

        waterSplash.SetActive(true);

    }

    public void Attack() {

        animator.SetTrigger("attack");

    }

    public void Damage() {

        playerHealth.Damage();
        attackWaterSplash.SetActive(true);

    }

}
