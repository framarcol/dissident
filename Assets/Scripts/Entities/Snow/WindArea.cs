﻿using UnityEngine;
using System.Collections;

public class WindArea : MonoBehaviour {

    public float pushAmount;

    SnowWind snowWind;
    PlayerMovementController playerMovementController;

    bool entered = false;

	// Use this for initialization
	void Start () {

        snowWind = FindObjectOfType<SnowWind>();

        playerMovementController = FindObjectOfType<PlayerMovementController>();

    }
    
    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if (pushAmount < 0) snowWind.GoLeft();
            else if (pushAmount > 0) snowWind.GoRight();

        }

    }

    void OnTriggerStay(Collider other) {

        if(entered && other.tag == "Player") {

            playerMovementController.PushPlayer(pushAmount * Time.deltaTime);

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            snowWind.Disable();

        }

    }
    
}
