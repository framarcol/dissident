﻿using UnityEngine;

public class StepFallZone : MonoBehaviour {

    public float jumpStepFall = 2.5f;

    public bool longFall = false;

    bool entered = false;

	void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            PlayerMovementController playerMovementController = other.GetComponentInParent<PlayerMovementController>();

            playerMovementController.jumpStepHeight = jumpStepFall;

            if (longFall) {
                playerMovementController.jumpFallTime = 1f;
                playerMovementController.Fall(2f);
            } else {
                playerMovementController.Fall(-1f);    
            }

            entered = true;

        }

    }

}
