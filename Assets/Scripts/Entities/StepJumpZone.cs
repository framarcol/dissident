﻿using UnityEngine;

public class StepJumpZone : MonoBehaviour {

    public float jumpStepHeight = 5;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            PlayerMovementController playerMovementController = other.GetComponentInParent<PlayerMovementController>();

            if (!playerMovementController.jumping) {

                playerMovementController.jumpStepHeight = jumpStepHeight;
                playerMovementController.isStepJump = true;

            }

            entered = true;

        }

    }

    void OnTriggerExit(Collider other) {

        if(other.tag == "Player") {

            PlayerMovementController playerMovementController = other.GetComponentInParent<PlayerMovementController>();

            if (!playerMovementController.jumping) {

                playerMovementController.isStepJump = false;

            }
            
        }

    }

}
