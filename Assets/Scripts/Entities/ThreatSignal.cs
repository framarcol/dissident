﻿using UnityEngine;
using System.Collections;

public class ThreatSignal : MonoBehaviour {

    public float threatTime = 1f;

    SpriteRenderer spriteRenderer;

    bool goFade;

	// Use this for initialization
	void Start () {

        StartCoroutine(LifeTime());

        transform.localScale = new Vector3(5,5,5);

        spriteRenderer = GetComponent<SpriteRenderer>();

	}
	
	// Update is called once per frame
	void Update () {

        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, 5f * Time.deltaTime);

        if (goFade) {

            Color color = spriteRenderer.color;
            color.a = Mathf.Lerp(color.a, 0, 5f * Time.deltaTime);

            spriteRenderer.color = color;

        }

	}

    IEnumerator LifeTime() {

        yield return new WaitForSeconds(threatTime);

        //gameObject.SetActive(false);
        goFade = true;

    }

}
