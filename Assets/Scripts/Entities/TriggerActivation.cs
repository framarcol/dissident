﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TriggerEvent : UnityEvent { }

public class TriggerActivation : MonoBehaviour {

    public TriggerEvent triggerEvent;

    public TriggerEvent triggerOnExitEvent;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if(triggerEvent != null) triggerEvent.Invoke();

        }

    }

    void OnTriggerExit(Collider other) {

        if (entered) {

            if (triggerOnExitEvent != null) triggerOnExitEvent.Invoke();

        }

    }

}
