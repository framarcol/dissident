﻿using UnityEngine;
using System.Collections;

public class BarriersController : MonoBehaviour {

    public GameObject[] barrierPieces;

    public float signalTime = 0.5f;
    float signalTimer;
    bool barriersActive;

    public bool liftLeft = true;
    public bool liftCenter = true;
    public bool liftRight = true;
	
	// Update is called once per frame
	void Update () {

        if (barriersActive) {

            signalTimer += Time.deltaTime;

            if(signalTimer >= signalTime) {

                foreach (GameObject barrierPiece in barrierPieces) {

                    GameObject childOne = barrierPiece.transform.GetChild(0).gameObject;
                    childOne.SetActive(!childOne.activeSelf);

                    GameObject childTwo = barrierPiece.transform.GetChild(1).gameObject;
                    childTwo.SetActive(!childTwo.activeSelf);

                }

                signalTimer = 0f;

            }

        }

	}

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            barriersActive = true;

            if (liftLeft) barrierPieces[0].GetComponent<LiftPiece>().active = true;
            if (liftCenter) barrierPieces[1].GetComponent<LiftPiece>().active = true;
            if (liftRight) barrierPieces[2].GetComponent<LiftPiece>().active = true;

        }

    }

}
