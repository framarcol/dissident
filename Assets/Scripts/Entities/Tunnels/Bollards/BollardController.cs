﻿using UnityEngine;
using System.Collections;

public class BollardController : MonoBehaviour {

    public GameObject[] bollardPieces;

    public enum BollardSetup { Left = 0, Center = 1, Right = 2 }
    public BollardSetup bollardSetup;

    //GameObject[] selectedPieces;

	// Use this for initialization
	void Start () {

        /*switch (bollardSetup) {

            case BollardSetup.Left:

                break;

            case BollardSetup.Center:

                break;

            case BollardSetup.Right:

                break;

        }*/

	}
	
    private IEnumerator LiftBollards() {

        if(bollardSetup == BollardSetup.Center) {

            bollardPieces[0].GetComponent<LiftPiece>().active = true;
            bollardPieces[5].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.3f);
            bollardPieces[1].GetComponent<LiftPiece>().active = true;
            bollardPieces[4].GetComponent<LiftPiece>().active = true;

        } else if (bollardSetup == BollardSetup.Left) {

            bollardPieces[5].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[4].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[3].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[2].GetComponent<LiftPiece>().active = true;

        } else if (bollardSetup == BollardSetup.Right) {

            bollardPieces[0].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[1].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[2].GetComponent<LiftPiece>().active = true;
            yield return new WaitForSeconds(0.1f);
            bollardPieces[3].GetComponent<LiftPiece>().active = true;

        }

        yield return null;

    }

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            StartCoroutine(LiftBollards());

        }

    }

}
