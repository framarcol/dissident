﻿using UnityEngine;
using System.Collections;

public class BreakableDoor : Entity {
    
    //Layer mask for the possible receiver layers
    public LayerMask receiverLayers;

    //Force that will be used to break the door
    public float breakForce = 100f;

    //Material to be changed into in order to show that the glass is partially broken
    public Material brokenGlassMaterial;

    //Disabled object that will contain the pieces of the broken door
    public GameObject brokenDoor;

    //Time to disable the broken door when triggered
    public float disableTime = 2f;

	public override void Start () {

        base.Start();

        //If there's no broken door set then return
        if(brokenDoor == null) {

            enabled = false;
            return;

        }

        //Ensure that the broken door object is inactive
        brokenDoor.SetActive(false);

	}

    //Break the door using a point where the force is coming from
    private void BreakDoor(Vector3 forcePosition) {

        //If there's no broken door...
        if (brokenDoor != null) {

            //Enable the broken door
            brokenDoor.SetActive(true);

            //Iterate through the pieces and apply a force from the point parameter passed to the function
            foreach (Transform trans in brokenDoor.transform) {

                Rigidbody rb = trans.GetComponent<Rigidbody>();

                if (rb != null) {

                    rb.AddExplosionForce(breakForce, forcePosition, 5f, 0.25f);

                }

            }

            //KEEP?
            //Destroy the broken door (pieces floating) after the disable time
            Destroy(brokenDoor, disableTime);

        }

        //Disable this object once broken
        gameObject.SetActive(false);
        
    }

    //Function called by message, when something shot this
    public override void Shot(ShootPackage shootPackage) {

        base.Shot(shootPackage);

        if(lifes > 0) {

            GetComponent<Renderer>().material = brokenGlassMaterial;

        }
        
    }

    public override void Die() {

        BreakDoor(impactPoint);

    }

    //Check if the player comes close to the door, then break it
    void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            BreakDoor(other.transform.position);

        }

    }

}
