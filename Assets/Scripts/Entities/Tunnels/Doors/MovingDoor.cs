﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MovingDoor : MonoBehaviour {

    public Plate plate;

    public enum MovingDoorSetup { Left = 0, Right = 1 }
    public MovingDoorSetup movingDoorSetup;
    
	// Update is called once per frame
	void Update () {

#if UNITY_EDITOR

        if (Application.isPlaying) return;

        if(movingDoorSetup == MovingDoorSetup.Left) {

            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);

        }else if (movingDoorSetup == MovingDoorSetup.Right) {

            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);

        }

        UnityEditor.EditorUtility.SetDirty(this);

#endif

    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            plate.gameObject.SetActive(true);
            if(movingDoorSetup == MovingDoorSetup.Left) plate.direction = -transform.right;
            else if (movingDoorSetup == MovingDoorSetup.Right) plate.direction = transform.right;
            plate.active = true;

        }

    }

}
