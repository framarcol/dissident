﻿using UnityEngine;
using System.Collections;

public class Plate : MonoBehaviour {

    public float distance = 12f;
    public float closeTime = 3f;

    public Vector3 direction;

    public bool active = false;

    float moveTimer;
    float oldDistance;
	
	// Update is called once per frame
	void Update () {

        if (active) {

            moveTimer += Time.deltaTime;

            if(moveTimer >= closeTime) {

                moveTimer = closeTime;
                active = false;

            }

            float perc = moveTimer / closeTime;

            float actualDistance = perc * distance;
            float deltaDistance = actualDistance - oldDistance;
            oldDistance = actualDistance;

            transform.position += direction * deltaDistance;

        }

	}

}
