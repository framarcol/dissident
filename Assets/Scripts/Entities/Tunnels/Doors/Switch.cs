﻿using UnityEngine;
using System.Collections;

public class Switch : Entity {

    //Reference to the switch controller of this switch
    SwitchController switchController;

    public override void Start() {

        base.Start();

        //Get the switch controller from the parent
        switchController = GetComponentInParent<SwitchController>();
        
    }

    /*public override void Shot(ShootPackage shootPackage) {
        
        base.Shot(shootPackage);

    }*/

    public override void Die() {

        //If the switch was shot then deactivate it calling the controller function
        switchController.DeactivateSwitch(GetComponent<Collider>());

    }

}
