﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SwitchController : MonoBehaviour {

    //Laser door and effects gameobjects for the controller
    public GameObject laserDoor;
    public GameObject effects;

    //Lists to maintain the collider of the switches and their states (enabled/disabled, working/not working)
    public List<Collider> switches = new List<Collider>();
    public List<bool> enabledSwitches = new List<bool>();
    public List<bool> workingSwitches = new List<bool>();

    //The count of working switches
    public int workingSwitchCount;

    ScoreController scoreController;
    public float points = 50f;

    void Start () {

        if (Application.isPlaying) {

            scoreController = FindObjectOfType<ScoreController>();

            //When the game start count how many switches are working
            workingSwitchCount = 0;
            foreach (bool working in workingSwitches) {

                if (working) workingSwitchCount++;

            }

        }

#if UNITY_EDITOR

        if (Application.isPlaying) return;

        if (switches.Count > 0) return;

        //Cache all the switches and initialize their default states (disabled and not working)
        foreach (Transform trans in transform) {

            switches.Add(trans.GetComponent<Collider>());
            enabledSwitches.Add(false);
            workingSwitches.Add(false);

        }

#endif

    }

    void Update() {

#if UNITY_EDITOR

        if (!Application.isPlaying) {

            //Activate/Deactivate the switches depending of their enabled states
            for (int i = 0; i < enabledSwitches.Count; i++) {

                if (!enabledSwitches[i]) switches[i].gameObject.SetActive(false);
                else switches[i].gameObject.SetActive(true);

            }

        }

#endif

        if (Application.isPlaying) {
            
            //If no switches are working then deactivate the laser door
            if (workingSwitchCount == 0) {

                laserDoor.SetActive(false);
                scoreController.AddPoints(points);

                enabled = false;

            } else {

                laserDoor.SetActive(true);

            }

        }
        
    }

    //Function that manages the deactivation of a switch
    public void DeactivateSwitch(Collider switchCollider) {

        //Get the list index of the collider passed to the function
        int index = switches.IndexOf(switchCollider);

        //If the passed switch is working, then set it to not working and activate the corresponding effect if present
        if (workingSwitches[index]) {

            workingSwitches[index] = false;
            switchCollider.gameObject.SetActive(false);

            if (effects != null) {

                effects.transform.GetChild(index).gameObject.SetActive(true);

            }

            //Update the working switch count
            workingSwitchCount--;

        }

    }

}
