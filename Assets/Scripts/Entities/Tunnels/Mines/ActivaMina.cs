﻿using UnityEngine;
using System.Collections;

public class ActivaMina : MonoBehaviour {

    //The graphics mine gameobject
    //public GameObject mina;

    AudioSource audioSource;

    bool entered = false;

    void Start() {

        audioSource = GetComponentsInParent<AudioSource>(true)[0];

    }

    //If the player becomes too close then activate the mine (set animator trigger)
    void OnTriggerEnter(Collider col) {

        if (!entered && col.tag == "Player") {

            entered = true;

            //mina.GetComponent<Animator>().SetTrigger("activate");

            audioSource.enabled = true;

        }

    }

    void OnTriggerExit(Collider col) {

        if(entered && col.tag == "Player") {

            audioSource.enabled = false;

        }

    }

}
