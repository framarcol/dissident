﻿using UnityEngine;
using System.Collections;

public class Mine : Entity {

    public GameObject mineGraphics;
    public GameObject mineExplosion;
    public AudioClip[] explosionSounds;
    public GameObject mineDamage;

    AudioSource audioSource;

    public override void Start() {

        base.Start();

        audioSource = GetComponentInChildren<AudioSource>(true);

    }

    public override void Die() {

        GetComponent<Collider>().enabled = false;

        mineGraphics.SetActive(false);
        
        mineExplosion.SetActive(true);
        mineExplosion.GetComponent<ParticleSystem>().Play();

        mineDamage.SetActive(false);

        if(explosionSounds.Length > 0) audioSource.PlayOneShot(explosionSounds[Random.Range(0, explosionSounds.Length)], 1.5f);

        Destroy(gameObject, 1.5f);

    }

}
