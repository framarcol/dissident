﻿using UnityEngine;
using System.Collections;

public class MineDamage : Damage {

    public override void OnHit() {

        GetComponentInParent<Mine>().Die();

        /*mineGraphics.SetActive(false);

        mineExplosion.SetActive(true);
        mineExplosion.GetComponent<ParticleSystem>().Play();

        Destroy(gameObject, 0.5f);*/
        
    }

}
