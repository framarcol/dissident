﻿using UnityEngine;
using System.Collections;

public class LaserRays : MonoBehaviour {

    public bool active = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (active) {

            float newYPos = Mathf.Lerp(transform.localPosition.y, 0, 5f * Time.deltaTime);

            transform.localPosition = new Vector3(transform.localPosition.x, newYPos, transform.localPosition.z);

        }

	}

}
