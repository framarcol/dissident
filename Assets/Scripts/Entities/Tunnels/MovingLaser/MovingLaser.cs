﻿using UnityEngine;
using System.Collections;

public class MovingLaser : MonoBehaviour {

    //PlayerMovementController playerMovement;
    
    public GameObject generator;

    public float speed;

    public bool goLeft;
    public bool goRight;

    /*public Transform leftLimit;
    public Transform rightLimit;*/

    bool active = false;

    AudioSource audioSource;

    /* public float generatorOffset;

     public int directionSelected;
     public int targetSelected;

     public Vector3 firstTarget;
     public Vector3 lastTarget;

     Vector3 target;
     float speed;
     */

   // Use this for initialization

    void Start () {

        audioSource = GetComponentInChildren<AudioSource>(true);

        //playerMovement = FindObjectOfType<PlayerMovementController>();

        /*target = firstTarget;

        CalculateSpeed();*/

    }

    // Update is called once per frame
    void Update () {

        if (active) {

            if (goLeft) {

                generator.transform.Translate(-generator.transform.right * speed * Time.deltaTime, Space.World);

                if(generator.transform.localPosition.x <= -3) {

                    goLeft = false;
                    goRight = true;

                }

            }else if (goRight) {

                generator.transform.Translate(generator.transform.right * speed * Time.deltaTime, Space.World);

                if (generator.transform.localPosition.x >= 3) {

                    goLeft = true;
                    goRight = false;

                }

            }

            /*Vector3 direction = (target - generator.transform.position);

            generator.transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

            if (direction.magnitude <= 0.05f) {

                if (target == rightLimit.position) target = leftLimit.position;
                else target = rightLimit.position;

            }*/

        }

	}

    /*public void CalculateSpeed() {

        float firstDistance = (firstTarget - generator.transform.position).magnitude;

        if (firstTarget == lastTarget) {

            speed = firstDistance / 2f;

        } else {

            speed = (firstDistance + 6f) / 2f;

        }

    }*/

    void OnTriggerEnter(Collider other) {

        if(!active && other.tag == "Player") {

            active = true;

            generator.GetComponentInChildren<Animator>().SetTrigger("setRays");

            audioSource.enabled = true;
            audioSource.Play();

        }

    }

    void OnTriggerExit(Collider other) {

        if(active && other.tag == "Player") {

            active = false;

            audioSource.Stop();
            audioSource.enabled = false;
            
        }

    }

}
