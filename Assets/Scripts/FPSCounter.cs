﻿using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour {

    int frameCount = 0;
    float dt = 0.0f;
    float fps = 0.0f;
    float updateRate = 4.0f;  // 4 updates per sec.

    float minFps;
    float maxFps;

    Rect fpsRect;

    void Start() {

        Application.targetFrameRate = 60;

        fpsRect = new Rect(new Vector2(Screen.width - 60, 0), new Vector2(100, 30));

        minFps = 100f;
        maxFps = 0f;

    }

    void Update() {

        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }

        if (GameDataController.gameData.betaUser)
        {
            frameCount++;
            dt += Time.deltaTime;

            if (dt > 1.0f / updateRate)
            {

                fps = frameCount / dt;
                frameCount = 0;
                dt -= 1.0f / updateRate;

                if (fps < minFps) minFps = fps;
                if (fps > maxFps) maxFps = fps;

            }
        }
    }

    void OnGUI() {

        if (GameDataController.gameData.betaUser)
        {
            GUI.Label(fpsRect, fps.ToString());
            GUI.Label(new Rect(new Vector2(Screen.width - 60, 15), new Vector2(100, 30)), minFps.ToString());
            GUI.Label(new Rect(new Vector2(Screen.width - 60, 30), new Vector2(100, 30)), maxFps.ToString());
        }
    }

}
