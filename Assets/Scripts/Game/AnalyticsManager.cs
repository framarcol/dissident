﻿using System;
using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public class AnalyticsManager : MonoBehaviour {
    
    [Serializable]
    public struct GameOverData {

        public string level;

        public bool obstacleDeath;
        
        public int score;

        public int maxMultiplier;

        public int idols;

        public float levelCompletion;

        public int totalDamageReceived;

        public int ammoLeftPercentage;

        public float shotAccuracy;

        public string weaponEquipped;

    }

    [Serializable]
    public struct GameWinData {

        public string level;

        public int score;

        public int maxMultiplier;

        public int idols;

        public int stars;

        public float hpLeftPercentage;

        public float totalDamageReceived;

        public int ammoLeftPercentage;

        public float shotAccuracy;

        public string weaponEquipped;

    }

    public GameOverData gameOverData;
    public GameWinData gameWinData;

    [HideInInspector]
    public bool deathByObstacle;

    int earnedStars = 0;

    GameManager gameManager;
    ScoreController scoreController;
    ProgressionBarUI progressionBarUI;
    PlayerHealth playerHealth;
    ShootController shootController;
    PlayerConfiguration playerConfiguration;

	// Use this for initialization
	void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();
        progressionBarUI = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<ProgressionBarUI>();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        shootController = playerHealth.GetComponent<ShootController>();
        playerConfiguration = playerHealth.GetComponentInParent<PlayerConfiguration>();

        if(gameManager == null || 
            scoreController == null ||
            progressionBarUI == null ||
            playerHealth == null ||
            shootController == null ||
            playerConfiguration == null) {

            gameObject.SetActive(false);
            return;

        }

        GameManager.OnGameOver += GameOverAnalytics;

        GameManager.OnGameCompleted += GameWinAnalytics;

	}
	
    public void GameOverAnalytics() {

        gameOverData = PrepareGameOverData();

        SendGameOverEvent(gameOverData);

    }

    public void GameWinAnalytics() {

        earnedStars = gameManager.CheckStars();

        gameWinData = PrepareGameWinData();

        SendGameWinEvent(gameWinData);

    }

    public GameOverData PrepareGameOverData() {

        GameOverData gameOverData = new GameOverData();

        gameOverData.level = GetLevelName();
        gameOverData.obstacleDeath = deathByObstacle;
        //gameOverData.killerEntity = "";
        gameOverData.score = Mathf.FloorToInt(scoreController.totalScore);
        gameOverData.maxMultiplier = scoreController.maxMultiplier;
        gameOverData.idols = gameManager.idolCounter;
        gameOverData.levelCompletion = (float) Math.Round(((progressionBarUI.actualProgression / progressionBarUI.maxLength) * 100f), 2);
        gameOverData.totalDamageReceived = playerHealth.totalDamage;
        gameOverData.ammoLeftPercentage = Mathf.RoundToInt((1.0f * shootController.actualAmmo / shootController.maxAmmo) * 100f);
        gameOverData.shotAccuracy = (shootController.totalShots <= 0) ? 0f : (float) Math.Round((1f - (1.0f * shootController.missedShots / shootController.totalShots)) * 100f, 2);
        gameOverData.weaponEquipped = GetWeaponEquipped();

        return gameOverData;

    }

    public GameWinData PrepareGameWinData() {

        GameWinData gameWinData = new GameWinData();

        gameWinData.level = GetLevelName();
        gameWinData.score = Mathf.FloorToInt(scoreController.totalScore);
        gameWinData.maxMultiplier = scoreController.maxMultiplier;
        gameWinData.idols = gameManager.idolCounter;
        gameWinData.stars = earnedStars;
        gameWinData.hpLeftPercentage = Mathf.RoundToInt((1.0f * playerHealth.lifes / playerHealth.maxLife) * 100f);
        gameWinData.totalDamageReceived = playerHealth.totalDamage;
        gameWinData.ammoLeftPercentage = Mathf.RoundToInt((1.0f * shootController.actualAmmo / shootController.maxAmmo) * 100f);
        gameWinData.shotAccuracy = (shootController.totalShots <= 0) ? 0f : (float)Math.Round((1f - (1.0f * shootController.missedShots / shootController.totalShots)) * 100f, 2);
        gameWinData.weaponEquipped = GetWeaponEquipped();

        return gameWinData;

    }

    private string GetLevelName() {

        string levelName = "";

        if (gameManager.specialLevel) {

            if(gameManager.specialLevelRequisites != null) levelName = gameManager.specialLevelRequisites.levelName;

        } else {

            switch (gameManager.worldId) {

                case 0: levelName = "Qualdrom"; break;
                case 1: levelName = "Yldoon"; break;
                case 2: levelName = "Myräkkä"; break;
                case 3: levelName = "Wulussa"; break;

            }

            levelName += "-" + (gameManager.levelId + 1);

        }

        return levelName;

    }

    public string GetWeaponEquipped() {

        string weaponName = "";

        if(playerConfiguration.weaponSkin == null) {

            weaponName = "00-DISSIDENT";

        }else {

            weaponName = playerConfiguration.weaponSkin.itemId + "-" + playerConfiguration.weaponSkin.itemName;

        }

        return weaponName;

    }

    public void SendGameOverEvent(GameOverData gameOverData) {

        Dictionary<string, object> gameOverDict = new Dictionary<string, object>() {

            { "level", gameOverData.level },
            { "obstacleDeath", gameOverData.obstacleDeath },
            //{ "killerEntity", gameOverData.killerEntity },
            { "score", gameOverData.score },
            { "maxMultiplier", gameOverData.maxMultiplier },
            { "idols", gameOverData.idols },
            { "levelCompletion", gameOverData.levelCompletion },
            { "totalDamageReceived", gameOverData.totalDamageReceived },
            { "ammoLeftPercentage", gameOverData.ammoLeftPercentage },
            { "shotAccuracy", gameOverData.shotAccuracy },
            { "weaponEquipped", gameOverData.weaponEquipped }

        };

        AnalyticsResult result = Analytics.CustomEvent("dissident.gameOver", gameOverDict);

        Debug.Log("Analytics 'gameOver' event result: " + result.ToString());

    }

    public void SendGameWinEvent(GameWinData gameWinData) {

        Dictionary<string, object> gameWinDict = new Dictionary<string, object>() {

            { "level", gameWinData.level },
            { "score", gameWinData.score },
            { "maxMultiplier", gameWinData.maxMultiplier },
            { "idols", gameWinData.idols },
            { "stars", gameWinData.stars },
            { "hpLeftPercentage", gameWinData.hpLeftPercentage },
            { "totalDamageReceived", gameWinData.totalDamageReceived },
            { "ammoLeftPercentage", gameWinData.ammoLeftPercentage },
            { "shotAccuracy", gameWinData.shotAccuracy },
            { "weaponEquipped", gameWinData.weaponEquipped }

        };

        AnalyticsResult result = Analytics.CustomEvent("dissident.gameWin", gameWinDict);

        Debug.Log("Analytics 'gameWin' event result: " + result.ToString());

    }

}
