﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateDataObjects {

    /*[MenuItem("Assets/Create/Player Build Data")]
    public static void CreatePlayerBuildData() {

        PlayerBuildData playerBuildData = ScriptableObject.CreateInstance<PlayerBuildData>();

        AssetDatabase.CreateAsset(playerBuildData, "Assets/Resources/DataObjects/PlayerBuildData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = playerBuildData;

    }*/

    /*[MenuItem("Assets/Create/Player Configuration")]
    public static void CreatePlayerConfiguration() {

        PlayerConfiguration playerConfiguration = ScriptableObject.CreateInstance<PlayerConfiguration>();

        AssetDatabase.CreateAsset(playerConfiguration, "Assets/Resources/DataObjects/PlayerConfiguration.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = playerConfiguration;

    }*/

    [MenuItem("Assets/Create/Fog Presets")]
    public static void CreateFogPreset() {

        FogPresetList fogPreset = ScriptableObject.CreateInstance<FogPresetList>();

        AssetDatabase.CreateAsset(fogPreset, "Assets/Resources/DataObjects/FogPreset.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = fogPreset;

    }

    [MenuItem("Assets/Create/Player Mount Set")]
    public static void CreatePlayerMountSet() {

        PlayerMountSet playerMountSet = ScriptableObject.CreateInstance<PlayerMountSet>();

        AssetDatabase.CreateAsset(playerMountSet, "Assets/Resources/DataObjects/PlayerMountSet.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = playerMountSet;

    }

    [MenuItem("Assets/Create/Stars Requisites")]
    public static void CreateStarsRequisites() {

        StarsRequisites starsRequisites = ScriptableObject.CreateInstance<StarsRequisites>();

        AssetDatabase.CreateAsset(starsRequisites, "Assets/Resources/DataObjects/StarsRequisites.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = starsRequisites;

    }

}