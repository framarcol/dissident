﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {

    GameManager gameManager;

    bool levelIdolsFoldout = true;

    public override void OnInspectorGUI() {

        gameManager = (GameManager)target;
        
        if (!gameManager.specialLevel) {

            //World & Level selection
            gameManager.world = (World)EditorGUILayout.EnumPopup("World", gameManager.world);
            gameManager.level = (uint)EditorGUILayout.IntSlider("Level", (int)gameManager.level, 1, 15);

            //Initialize the list with maximum amount of idols per level
            if (gameManager.levelIdols.Count == 0) {

                for (int i = 0; i < GameManager.MaxIdolsPerLevel; i++) {

                    gameManager.levelIdols.Add(null);
                    gameManager.idolsCheck.Add(false);

                }

            }

        } else {

            gameManager.specialLevelID = EditorGUILayout.IntField("Special level ID", gameManager.specialLevelID);

        }
        
        //Draw the idols list
        levelIdolsFoldout = EditorGUILayout.Foldout(levelIdolsFoldout, "Level Idols");
        if (levelIdolsFoldout) {

            EditorGUI.indentLevel++;

            for (int i = 0; i < GameManager.MaxIdolsPerLevel; i++) {

                EditorGUILayout.BeginHorizontal();

                gameManager.levelIdols[i] = EditorGUILayout.ObjectField("Idol " + (i + 1), gameManager.levelIdols[i], typeof(IdolItem), true) as IdolItem;

                GUI.enabled = false;
                gameManager.idolsCheck[i] = EditorGUILayout.Toggle(gameManager.idolsCheck[i], GUILayout.MaxWidth(50f));
                GUI.enabled = true;

                EditorGUILayout.EndHorizontal();

            }

            EditorGUI.indentLevel--;

        }

        if (!gameManager.specialLevel) { 

            //Chrono level
            gameManager.chronoLevel = EditorGUILayout.Toggle("Chrono level", gameManager.chronoLevel);

        }

        //Special level
        gameManager.specialLevel = EditorGUILayout.Toggle("Special level", gameManager.specialLevel);

        //End point level
        gameManager.endPoint = EditorGUILayout.Toggle("End point level", gameManager.endPoint);

        EditorUtility.SetDirty(gameManager);
        Undo.RecordObject(gameManager, "Game Manager changed");

    }

}
