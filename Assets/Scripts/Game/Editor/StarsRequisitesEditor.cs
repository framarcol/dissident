﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(StarsRequisites))]
public class StarsRequisitesEditor : Editor {

    StarsRequisites starsRequisites;

    public override void OnInspectorGUI() {

        starsRequisites = (StarsRequisites)target;

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Add world")) {

            StarsRequisites.WorlStarsdRequisites worldRequisite = new StarsRequisites.WorlStarsdRequisites();
            StarsRequisites.LevelRequisite levelRequisite = new StarsRequisites.LevelRequisite();

            for (int i = 0; i < GameData.MaxLevelsPerWorld; i++) {

                worldRequisite.levelRequisites.Add(levelRequisite);

            }

            starsRequisites.worldStarsRequisites.Add(worldRequisite);

            starsRequisites.foldoutControllers.Add(false);

        }

        if (GUILayout.Button("Delete world")) {

            starsRequisites.worldStarsRequisites.RemoveAt(starsRequisites.worldStarsRequisites.Count - 1);
            starsRequisites.foldoutControllers.RemoveAt(starsRequisites.foldoutControllers.Count - 1);

        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        //Special level requisites
        EditorGUILayout.BeginHorizontal();
        starsRequisites.specialLevelsFoldout = EditorGUILayout.Foldout(starsRequisites.specialLevelsFoldout, "Special level requisites");
        if (GUILayout.Button("+", GUILayout.Width(50f))) {

            StarsRequisites.SpecialLevelRequisites specialLevelRequisite = new StarsRequisites.SpecialLevelRequisites();
            starsRequisites.specialLevelRequisites.Add(specialLevelRequisite);

        }
        EditorGUILayout.EndHorizontal();

        if (starsRequisites.specialLevelsFoldout) {

            EditorGUI.indentLevel++;

            for(int i = 0; i < starsRequisites.specialLevelRequisites.Count; i++) {

                StarsRequisites.SpecialLevelRequisites specialRequisite = starsRequisites.specialLevelRequisites[i];

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Name");
                EditorGUILayout.LabelField("ID", GUILayout.Width(130f));
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                specialRequisite.levelName = EditorGUILayout.TextField(specialRequisite.levelName);
                specialRequisite.levelId = EditorGUILayout.IntField(specialRequisite.levelId, GUILayout.Width(100f));
                if (GUILayout.Button("-", GUILayout.Width(30f))) {

                    starsRequisites.specialLevelRequisites.RemoveAt(i);

                }
                EditorGUILayout.EndHorizontal();

                EditorGUI.indentLevel++;

                specialRequisite.diamondReward = EditorGUILayout.IntField("Diamond reward", specialRequisite.diamondReward);
                specialRequisite.score = EditorGUILayout.IntField("Target score", specialRequisite.score);
                specialRequisite.idols = EditorGUILayout.IntField("Target idols", specialRequisite.idols);
                specialRequisite.weaponId = EditorGUILayout.IntField("Weapon id", specialRequisite.weaponId);
                specialRequisite.nextLevel = EditorGUILayout.IntField("Next level", specialRequisite.nextLevel);

                EditorGUI.indentLevel--;

                EditorGUILayout.Space();

            }

            EditorGUI.indentLevel--;
            
        }

        //World levels requisites
        for (int i = 0; i < starsRequisites.worldStarsRequisites.Count; i++) {

            StarsRequisites.WorlStarsdRequisites worldRequisite = starsRequisites.worldStarsRequisites[i];

            starsRequisites.foldoutControllers[i] = EditorGUILayout.Foldout(starsRequisites.foldoutControllers[i], "World " + (i + 1));

            if (starsRequisites.foldoutControllers[i]) {

                EditorGUI.indentLevel++;

                worldRequisite.world = (World)EditorGUILayout.EnumPopup(worldRequisite.world);

                for (int j = 0; j < worldRequisite.levelRequisites.Count; j++) {

                    StarsRequisites.LevelRequisite levelRequisite = worldRequisite.levelRequisites[j];

                    EditorGUILayout.LabelField("Level " + (j + 1));
                    
                    EditorGUI.indentLevel++;

                    levelRequisite.nextLevel = EditorGUILayout.IntField("Next level", levelRequisite.nextLevel);

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.BeginVertical();

                    EditorGUILayout.LabelField("2-Star Requisite");

                    levelRequisite.twoStarsRequisite.score = EditorGUILayout.IntField("Score", levelRequisite.twoStarsRequisite.score);
                    levelRequisite.twoStarsRequisite.idols = EditorGUILayout.IntField("Idols", levelRequisite.twoStarsRequisite.idols);

                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical();

                    EditorGUILayout.LabelField("3-Star Requisite");

                    levelRequisite.threeStarsRequisite.score = EditorGUILayout.IntField("Score", levelRequisite.threeStarsRequisite.score);
                    levelRequisite.threeStarsRequisite.idols = EditorGUILayout.IntField("Idols", levelRequisite.threeStarsRequisite.idols);

                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndHorizontal();

                    EditorGUI.indentLevel--;

                }

                EditorGUI.indentLevel--;

            }

        }

        EditorUtility.SetDirty(starsRequisites);
        Undo.RecordObject(starsRequisites, "Stars Requisites changed");

    }

}
