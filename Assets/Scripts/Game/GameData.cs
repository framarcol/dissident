﻿using System;
using System.Collections.Generic;

[Serializable]
public class GameData {

    public const ushort MaxWorlds = 15;
    public const ushort MaxLevelsPerWorld = 15;
    public const ushort IdolsPerWorld = 9;
    public const ushort MaxFragmentsPerIdol = 10;
    //const uint StarsPerLevel = 3;

    public int playerId;
    public string playerName;
    public bool premiumState;
    public bool offlineState;
    public bool betaUser;

    public uint goldCurrency; //UNUSED -> DELETE IT?
    public uint diamondCurrency;
    public int diamondQueue;

    //public int lastUnlockedNode; //DELETE IT!
    //public int lastPlayedNode; //DELETE IT!

    //RANKING -> SET of worlds with LISTs of max player scores in the levels
    public uint[,] playerRecords;

    //IDOLS MATRIX
    public bool[,,] idols;

    //STARS MATRIX
    public ushort[,] stars;

    //SPECIAL LEVEL COMPLETION
    public bool[] specialLevelsCompleted;
       
    //SKINS
    public uint bodySkinEquipped;
    public uint skateSkinEquipped;
    public uint weaponSkinEquipped;

    //UPGRADES
    public uint lifeUpgradeEquipped;
    public uint ammoUpgradeEquipped;

    //WORLDS
    public bool[] worldsObtained;

    //SHOP STUFF
    public uint[,] shopItemsObtained;
    
    
    public GameData() {

        playerId = 0;
        playerName = "";
        premiumState = false;

        offlineState = false;
        betaUser = false;

        goldCurrency = 0;
        diamondCurrency = 0;
        diamondQueue = 0;

        //lastUnlockedNode = 0;
        //lastPlayedNode = -1;

        //15 possible worlds with 15 levels each
        playerRecords = new uint[MaxWorlds, MaxLevelsPerWorld];
        for (int i = 0; i < MaxWorlds; i++) {

            for(int j = 0; j < MaxLevelsPerWorld; j++) {

                playerRecords[i, j] = 0;
                
            }

        }

        //15 possible worlds with 9 idols per world with 10 maximum fragments per idol
        idols = new bool[MaxWorlds, IdolsPerWorld, MaxFragmentsPerIdol];
        for(int i = 0; i < MaxWorlds; i++) {

            for(int j = 0; j < IdolsPerWorld; j++) { 
                    
                for(int k = 0; k < MaxFragmentsPerIdol; k++) {

                    idols[i, j, k] = false;

                }

            }

        }

        //15 possible worlds with 15 levels per world with 3 stars per level, default 0
        stars = new ushort[MaxWorlds, MaxLevelsPerWorld];
        for (int i = 0; i < MaxWorlds; i++) {

            for (int j = 0; j < MaxLevelsPerWorld; j++) {

                stars[i, j] = 0;

            }

        }

        specialLevelsCompleted = new bool[200];
        for(int i = 0; i < 200; i++) {

            specialLevelsCompleted[i] = false;

        }

        bodySkinEquipped = 0;
        skateSkinEquipped = 0;
        weaponSkinEquipped = 0;

        lifeUpgradeEquipped = 0;
        ammoUpgradeEquipped = 0;

        worldsObtained = new bool[MaxWorlds];
        for(int i = 0; i < MaxWorlds; i++) {

            worldsObtained[i] = false;

        }

        shopItemsObtained = new uint[10,200];
        for(int i = 0; i < 10; i++) {

            for(int j = 0; j < 200; j++) {

                shopItemsObtained[i, j] = 0;

            }

        }
        

    }

}
