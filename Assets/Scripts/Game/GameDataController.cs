﻿using UnityEngine;
using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;

public static class GameDataController {

    private const string topPath = "/SkatePunk";
    private const string savePath = "/SkatePunk/skatePunk.sav";
    private const string dataPath = "/SkatePunk/skatePunkData.data";
    private const string backupPath = "/SkatePunk/skatePunk.bckup";
    private const int maxBackupNumber = 5;

    private static byte[] key32 = { 192, 234, 96, 72, 119, 21, 48, 123, 233, 67, 148, 73, 107, 229, 191, 195, 169, 44, 217, 43, 140, 150, 234, 185, 204, 244, 248, 165, 10, 23, 239, 39 };
    private static byte[] iv16 = { 141, 241, 116, 25, 181, 235, 153, 132, 240, 5, 27, 53, 111, 205, 176, 52 };

    private static GameData _gameData;
    public static GameData gameData {

        get { return _gameData; }

        set { _gameData = value; }

    }

    private static RijndaelManaged rm;

    public static bool initialized;

    public static void Initialize() {

        if (initialized) return;

        initialized = true;

        Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");

        if(!Directory.Exists(Application.persistentDataPath + topPath)) {

            Directory.CreateDirectory(Application.persistentDataPath + topPath);

        }

        rm = new RijndaelManaged();
        rm.Key = key32;
        rm.IV = iv16;
        rm.Mode = CipherMode.CBC;
        rm.Padding = PaddingMode.PKCS7;
        
        //If can't load the game, initialize a new empty game data object
        if (!Load()) {

            CreateSaveBackup();

            gameData = new GameData();

        }
        
    }

    // === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
    // Do not change this
    public sealed class VersionDeserializationBinder : SerializationBinder {
        public override Type BindToType(string assemblyName, string typeName) {
            if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName)) {
                Type typeToDeserialize = null;

                assemblyName = Assembly.GetExecutingAssembly().FullName;

                // The following line of code returns the type. 
                typeToDeserialize = Type.GetType(string.Format("{0}, {1}", typeName, assemblyName));

                return typeToDeserialize;
            }

            return null;
        }
    }

    public static bool Save() {
        
        BinaryFormatter bf = new BinaryFormatter();
        
        using (FileStream file = File.Create(Application.persistentDataPath + savePath)) {

            using (CryptoStream cryptoStream = new CryptoStream(file, rm.CreateEncryptor(), CryptoStreamMode.Write)) {

                bf.Serialize(cryptoStream, gameData);
                
            }
        }

        Debug.Log("Secure number: " + CalculateSecureNumber(true));

        if (File.Exists(Application.persistentDataPath + savePath)) {

            Debug.Log("Save filed created: " + Application.persistentDataPath + savePath);

            return true;

        } else {

            Debug.Log("Couldn't save the game data");

            return false;

        }

    }

    public static bool Load() {

        if (File.Exists(Application.persistentDataPath + savePath)) {

            BinaryFormatter bf = new BinaryFormatter();

            int realSecureNumber = RetrieveSecureNumber();
            int actualSecureNumber = CalculateSecureNumber(false);

            //Debug.Log(realSecureNumber + " <=> " + actualSecureNumber);

            if (realSecureNumber != actualSecureNumber) return false;

            try {

                using (FileStream file = File.Open(Application.persistentDataPath + savePath, FileMode.Open)) {

                    using (CryptoStream cryptoStream = new CryptoStream(file, rm.CreateDecryptor(), CryptoStreamMode.Read)) {

                        gameData = (GameData)bf.Deserialize(cryptoStream);

                    }

                }

            }catch(Exception e) {

                Debug.LogWarning("Couldn't load the game. Error: " + e.ToString());
                return false;

            }

            Debug.Log("Game data loaded from: " + Application.persistentDataPath + savePath);

            return true;

        } else {

            Debug.Log("No save file found");

            return false;

        }

    }

    public static int CalculateSecureNumber(bool update) {

        int secureNumber = 0;

        using (FileStream file = File.Open(Application.persistentDataPath + savePath, FileMode.Open)) {

            byte[] saveBytes = new byte[file.Length];
            file.Read(saveBytes, 0, (int)file.Length);

            for (int i = 0; i < saveBytes.Length; i++) {

                secureNumber += saveBytes[i];

            }

        }

        if (update) {

            using (FileStream file = File.Open(Application.persistentDataPath + dataPath, FileMode.OpenOrCreate, FileAccess.Write)) {

                using (StreamWriter sw = new StreamWriter(file)) {

                    sw.Write(secureNumber);

                }

            }

        }

        return secureNumber;

    }

    public static int RetrieveSecureNumber() {

        int secureNumber = 0;

        using (FileStream file = File.Open(Application.persistentDataPath + dataPath, FileMode.Open, FileAccess.Read)) {

            using (StreamReader sr = new StreamReader(file)) {

                secureNumber = int.Parse(sr.ReadLine());

            }

        }

        return secureNumber;

    }

    private static void CreateSaveBackup() {

        if (File.Exists(Application.persistentDataPath + savePath)) {
            
            File.Copy(Application.persistentDataPath + savePath, Application.persistentDataPath + backupPath, true);

            Debug.Log("Save backup created");

        }
        
    }

    public static void DeleteGameData() {

        if(File.Exists(Application.persistentDataPath + savePath)) {

            File.Delete(Application.persistentDataPath + savePath);

        }

        if (File.Exists(Application.persistentDataPath + dataPath)) {

            File.Delete(Application.persistentDataPath + dataPath);

        }

        gameData = new GameData();

    }

}
