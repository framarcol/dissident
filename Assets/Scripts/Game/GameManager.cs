﻿using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.Advertisements;
using System.Collections;
using System.Collections.Generic;

public enum World { SPECIAL_LEVEL = -1, CITY = 0, DESERT = 1, SNOW = 2, JUNGLE = 3 }

public enum GameState { RUNNING = 0, PAUSED = 1, GAME_OVER = 2, COMPLETED = 3 }

public class PlayerCheckpointState {

    public float score;
    public float multiplier;

    public int idolCounter;

    public float levelProgression;

    public PlayerMountSet.Mount mount;

    public bool activeFog;

    public int soundBaseLayerLoop;
    public int soundLayer1Loop;
    public int soundLayer2Loop;
    public int soundExtraLayerLoop;

    public PlayerCheckpointState() {

        score = 0f;
        multiplier = 0f;

        idolCounter = 0;

        levelProgression = 0;

        mount = 0;

        activeFog = true;

        soundBaseLayerLoop = -1;
        soundLayer1Loop = -1;
        soundLayer2Loop = -1;
        soundExtraLayerLoop = -1;

    }

}

public class GameManager : MonoBehaviour {

    public World world;
    public uint level;

    public int worldId { get { return ((int)world); } }
    public uint levelId { get { return level - 1; } }

    public const float GoldConversionRate = 0.01f;

    public const int MaxIdolsPerLevel = 6;

    //Idol Counter
    int m_idolCounter = 0;
    public int idolCounter { get { return m_idolCounter; } }

    //Level idols
    public List<IdolItem> levelIdols = new List<IdolItem>(MaxIdolsPerLevel);
    public List<bool> idolsCheck = new List<bool>(MaxIdolsPerLevel);

    //Chrono level
    public bool chronoLevel = false;

    //Special level => Tutorial, Minilevel or Event
    public bool specialLevel = false;
    public int specialLevelID = 0;
    public StarsRequisites.SpecialLevelRequisites specialLevelRequisites;

    public bool endPoint = false;
    /*public string specialLevelName = "ESPECIAL :*";
    public int diamondCompletionRewardForSpecialLevel = 1;
    public int scoreRequisiteForSpecialLevel = 0;
    public int idolRequisiteForSpecialLevel = 0;*/

    public List<CheckpointArea> checkpoints = new List<CheckpointArea>();
    static int _activeCheckpoint = -1;
    public static int activeCheckpoint { get { return _activeCheckpoint; } }
    public static PlayerCheckpointState actualPlayerState = null;

    GameObject playerSetup;
    Vector3 initPlayerPos;
    Quaternion initPlayerRot;
    Vector3 initCameraArmPos;
    Quaternion initCameraArmRot;
    Vector3 initCameraPos;
    Quaternion initCameraRot;
    public Transform respawnLocation = null;

    //2-Stars requisites
    public int twoStarsScore;
    public int twoStarsIdols;

    //3-Stars requisites
    public int threeStarsScore;
    public int threeStarsIdols;

    //Earned stars
    int m_earnedStars = 0;
    public int earnedStars { get { return m_earnedStars; } }

    //Player prefs keys
    public static readonly string JumpOptionPref = "JumpOption";
    public static readonly string JumpSensitivityPref = "JumpSensitivity";
    public static readonly string HandPref = "Hand";
    public static readonly string MusicVolumePref = "MusicVolume";
    public static readonly string SoundEffectVolumePref = "SoundEffectVolume";
    public static readonly string LanguagePref = "Language";
    public static readonly string ammoDisplayPref = "AmmoDisplay";

    //Game State
    static GameState m_gameState;
    public static GameState gameState { get { return m_gameState; } set { m_gameState = value; } }

    public delegate void GameStateDelegate();
    public static GameStateDelegate OnGamePaused;
    public static GameStateDelegate OnGameResumed;
    public static GameStateDelegate OnGameOver;
    public static GameStateDelegate OnGameCompleted;
    public GameStateDelegate OnIdolTaken;

    PlayerMovementController playerMovementController;
    PlayerCinematicController playerCinematicController;
    Mounts mounts;
    CameraMovementController cameraMovementController;
    CameraFollowingController cameraFollowingController;
    float cameraDeathOffset = 7.5f;
    PlayerHealth playerHealth;
    ScoreController scoreController;
    StarsRequisites starsRequisites;
    ChronoController chronoController;
    ProgressionBarUI progressionBar;
    InGameSound inGameSound;
    MeshRenderer fogPlane;
    public PlayerAnimEvents playerAnimEvents;

    void Awake() {

        Initialize();

        //ControladorId.cambiarUsuarioApple = true;


        //Get the player health component to track the health
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        mounts = playerHealth.GetComponentInChildren<Mounts>();
        //Get base-player components
        playerMovementController = playerHealth.GetComponentInParent<PlayerMovementController>();
        playerCinematicController = playerMovementController.GetComponent<PlayerCinematicController>();
        playerAnimEvents = playerHealth.GetComponentInChildren<PlayerAnimEvents>();
        //Get the score controller
        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();
        //Progression Bar
        progressionBar = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<ProgressionBarUI>();
        //In Game Sound
        inGameSound = FindObjectOfType<InGameSound>();

        GameObject fogObject = GameObject.FindGameObjectWithTag("FogPlane");

        if(fogObject != null) fogPlane = fogObject.GetComponent<MeshRenderer>();

        playerSetup = GameObject.Find("PlayerSetup");
        checkpoints = new List<CheckpointArea>(FindObjectsOfType<CheckpointArea>());
        checkpoints.Sort(
            
            (CheckpointArea c1, CheckpointArea c2) => {

                if (c1.transform.GetSiblingIndex() < c2.transform.GetSiblingIndex()) return -1;
                else if (c1.transform.GetSiblingIndex() > c2.transform.GetSiblingIndex()) return 1;
                else return 0;

            }
            
        );
        if (activeCheckpoint != -1) {

            SetPlayerOnCheckpoint();

        }

        //Chrono level
        chronoController = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<ChronoController>(true);
        if(chronoLevel) chronoController.gameObject.SetActive(true);

    }
    
    public static void Initialize() {

        //If the Game Data Controller is not initialized, init it
        if (!GameDataController.initialized) GameDataController.Initialize();

        //Reset delegates
        OnGamePaused = null;
        OnGameResumed = null;
        OnGameOver = null;
        OnGameCompleted = null;

        //Target frame rate to 60 FPS
        Application.targetFrameRate = 60;

    }

    void Start() {
        
        //TESTING PURPOSES -> REAL GAME DATA MUST BE COMING FROM APPLICATION START (MENU, ETC)
        //Try to load a saved game, if there's no saved game then initialize a new one
        //if (!GameDataController.Load()) GameDataController.gameData = new GameData();
        
        //Get the camera movement controller
        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();
        
        //
        cameraFollowingController = Camera.main.GetComponentInParent<CameraFollowingController>();
        
        //Get the stars requisites data
        starsRequisites = Resources.Load("DataObjects/StarsRequisites", typeof(StarsRequisites)) as StarsRequisites;
        if (specialLevel) {

            specialLevelRequisites = starsRequisites.GetSpecialLevelRequisites(specialLevelID);

        } else {

            GetStarsRequisites();

        }
        
        /*initPlayerPos = playerMovementController.transform.localPosition;
        initPlayerRot = playerMovementController.transform.localRotation;
        initCameraArmPos = cameraMovementController.transform.localPosition;
        initCameraArmRot = cameraMovementController.transform.localRotation;
        initCameraPos = cameraFollowingController.transform.localPosition;
        initCameraRot = cameraFollowingController.transform.localRotation;*/

        //Initialize game state to RUNNING by default
        m_gameState = GameState.RUNNING;
        if (Time.timeScale != 1) Time.timeScale = 1f;

        if (activeCheckpoint != -1) {

            GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<CommonStateScreenManager>().TriggerCountdown();

            AmmoUI ammoUI = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<AmmoUI>();
            if (ammoUI != null && ammoUI.ammoDisplayOption == 0) ammoUI.gameObject.SetActive(true);

        }

    }

    void Update() {

        //Track the player's health to set game over if player is dead
        if (m_gameState == GameState.RUNNING && playerHealth.lifes <= 0) {

            //TEST
            //GetNearbyEntities(playerHealth.transform.position);
            ////
            cameraMovementController.transform.position += cameraMovementController.transform.forward * (-cameraDeathOffset);

            GameOver();

        }

    }

    public static bool PauseGame() {

        if (m_gameState == GameState.RUNNING) {

            Time.timeScale = 0;
            m_gameState = GameState.PAUSED;
            if(OnGamePaused != null)OnGamePaused();
            return true;

        }

        return false;

    }

    public static bool ResumeGame() {

        if(m_gameState == GameState.PAUSED || m_gameState == GameState.GAME_OVER) {

            Time.timeScale = 1;
            m_gameState = GameState.RUNNING;
            if(OnGameResumed != null) OnGameResumed();
            return true;

        }

        return false;

    }

    public static bool GameOver() {

        if(m_gameState == GameState.RUNNING) {

            m_gameState = GameState.GAME_OVER;
            if(OnGameOver != null) OnGameOver();
            return true;

        }

        return false;

    }

    public static bool GameCompleted() {

        if(m_gameState == GameState.RUNNING) {

            m_gameState = GameState.COMPLETED;
            if(OnGameCompleted != null) OnGameCompleted();
            return true;

        }

        return false;

    }

    //Adds new record score for the player given the world, level and the new score
    public static void SetNewRecord(int worldId, uint levelId, uint score) {

        if (worldId == -1) return;

        GameDataController.gameData.playerRecords[worldId, levelId] = score;
        Debug.Log("New record of " + score + " points on level [" + worldId + ", " + levelId + "]");

    }

    //Adds or substracts the amount of gold given
    public static void AddGold(uint gold) {

        GameDataController.gameData.goldCurrency += gold;
        Debug.Log("Added " + gold + " gold. Total gold " + GameDataController.gameData.goldCurrency);

    }
    public static void SubstractGold(uint gold) {

        GameDataController.gameData.goldCurrency -= gold;
        Debug.Log("Substracted " + gold + " gold" + ". Total gold " + GameDataController.gameData.goldCurrency);

    }

    //Adds or substracts the amount of diamonds given
    public static void AddDiamonds(int diamonds) {

        if (diamonds < 0) {
            uint positiveDiamonds = (uint)(-1 * diamonds);
            if (positiveDiamonds > GameDataController.gameData.diamondCurrency) GameDataController.gameData.diamondCurrency = 0;
            else GameDataController.gameData.diamondCurrency -= positiveDiamonds;
        } else
            GameDataController.gameData.diamondCurrency += (uint)diamonds;
        Debug.Log("Added " + diamonds + " diamonds. Total diamonds " + GameDataController.gameData.diamondCurrency);

    }
    public static void AddDiamondsToQueue(int diamonds) {

        GameDataController.gameData.diamondQueue += diamonds;
        Debug.Log("Added " + diamonds + " diamonds to queue. Total diamonds " + GameDataController.gameData.diamondQueue);

    }
    public static void SubtractDiamonds(int diamonds) {
        int diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diamonds);
        if (diamantesRestantes >= 0)
            GameDataController.gameData.diamondCurrency -= (uint)diamonds;
        else GameDataController.gameData.diamondCurrency = 0;
        Debug.Log("Substracted " + diamonds + " diamonds. Total diamonds " + GameDataController.gameData.diamondCurrency);

    }
    public static void SubtractDiamondsToQueue(int diamonds) {

        GameDataController.gameData.diamondQueue -= diamonds;
        Debug.Log("Substracted " + diamonds + " diamonds to queue. Total diamonds " + GameDataController.gameData.diamondQueue);

    }
    public static bool BuyWithDiamonds(uint diamonds) {

        if (GameDataController.gameData.diamondCurrency < diamonds) return false;

        SubtractDiamonds((int)diamonds);
        SubtractDiamondsToQueue((int)diamonds);
        return true;

    }

    //Set special level completed
    public static bool SetSpecialLevelCompleted(int id) {

        if (id >= GameDataController.gameData.specialLevelsCompleted.Length) return false;

        GameDataController.gameData.specialLevelsCompleted[id] = true;
        return true;

    }

    //Adds one to the idol counter of the level
    public void IncrementIdolCounter(IdolItem idol) {

        //Find the idol in the list, and check it as taken
        int index = levelIdols.IndexOf(idol);
        if (index != -1) idolsCheck[index] = true;

        m_idolCounter++;
        OnIdolTaken();
        Debug.Log("Idol counter: " + m_idolCounter);

    }

    //Checks if the idol was already taken, marking it if not. Returns true if it was a new idol
    public static bool CheckIdol(int worldId, uint type, uint fragment) {

        if (worldId == -1) return false;

        if(!GameDataController.gameData.idols[worldId, type, fragment]) {

            GameDataController.gameData.idols[worldId, type, fragment] = true;
            Debug.Log("New idol [" + worldId + ", " + type + ", " + fragment + "] taken");
            return true;

        }

        Debug.Log("Idol [" + worldId + ", " + type + ", " + fragment + "] already taken");
        return false;

    }

    //When level is completed check the stars earned by the player
    public int CheckStars() {

        float totalScore = scoreController.totalScore;

        if (specialLevel) {

            if(specialLevelRequisites != null && totalScore > specialLevelRequisites.score && m_idolCounter >= specialLevelRequisites.idols) {

                m_earnedStars = 1;
            }

            Debug.Log("Special level STAR: " + m_earnedStars);

            /*bool specialLevelCompleted = GameDataController.gameData.specialLevelsCompleted[specialLevelRequisites.levelId];

            //int specialLevelCompleted = PlayerPrefs.GetInt("SpecialLevel" + specialLevelRequisites.levelId, 0);

            if (!specialLevelCompleted) {

                if(m_earnedStars >= 1) {

                    //TODO => CHECK IF ACHIEVED BEFORE
                    AddDiamonds(specialLevelRequisites.diamondReward);
                    AddDiamondsToQueue(specialLevelRequisites.diamondReward);

                    if (!SetSpecialLevelCompleted(specialLevelRequisites.levelId)) {

                        Debug.LogWarning("Couldn't save special level as completed");

                    }

                }
                
            }*/

            return m_earnedStars;

        }

        m_earnedStars = 1;
        
        //3-stars requisites fullfilled?
        if(totalScore > threeStarsScore && m_idolCounter >= threeStarsIdols) {

            m_earnedStars = 3;

        //2-stars requisites fullfilled?
        } else if (totalScore > twoStarsScore && m_idolCounter >= twoStarsIdols) {

            m_earnedStars = 2;

        }

        /*//Update the earned stars if more were achieved and its diamond value
        if(GameDataController.gameData.stars[worldId, levelId] < m_earnedStars) {

            //Add diamonds to the player currency
            int starsDiff = m_earnedStars - GameDataController.gameData.stars[worldId, levelId];
            AddDiamonds(starsDiff);
            AddDiamondsToQueue(starsDiff);

            //Update level's stars
            GameDataController.gameData.stars[worldId, levelId] = (ushort)m_earnedStars;
            Debug.Log("NEW BEST stars achieved: " + m_earnedStars);

            

        } else {

            Debug.Log("EQUAL or WORSE stars achieved: " + m_earnedStars);

        }*/
        
        return m_earnedStars;

    }

    public void UpdateStars() {

        if (specialLevel) {

            bool specialLevelCompleted = GameDataController.gameData.specialLevelsCompleted[specialLevelRequisites.levelId];

            //int specialLevelCompleted = PlayerPrefs.GetInt("SpecialLevel" + specialLevelRequisites.levelId, 0);

            if (!specialLevelCompleted) {

                if (m_earnedStars >= 1) {

                    //TODO => CHECK IF ACHIEVED BEFORE
                    AddDiamonds(specialLevelRequisites.diamondReward);
                    AddDiamondsToQueue(specialLevelRequisites.diamondReward);

                    if (!SetSpecialLevelCompleted(specialLevelRequisites.levelId)) {

                        Debug.LogWarning("Couldn't save special level as completed");

                    }

                }

            }

        } else {

            //Update the earned stars if more were achieved and its diamond value
            if (GameDataController.gameData.stars[worldId, levelId] < m_earnedStars) {

                //Add diamonds to the player currency
                int starsDiff = m_earnedStars - GameDataController.gameData.stars[worldId, levelId];
                AddDiamonds(starsDiff);
                AddDiamondsToQueue(starsDiff);

                //Update level's stars
                GameDataController.gameData.stars[worldId, levelId] = (ushort)m_earnedStars;
                Debug.Log("NEW BEST stars achieved: " + m_earnedStars);



            } else {

                Debug.Log("EQUAL or WORSE stars achieved: " + m_earnedStars);

            }

        }

    }

    private void GetStarsRequisites() {

        int worldCount = starsRequisites.worldStarsRequisites.Count;
        for (int i = 0; i < worldCount; i++) {

            if (starsRequisites.worldStarsRequisites[i].world == world) {

                int levelRequisitesCount = starsRequisites.worldStarsRequisites[i].levelRequisites.Count;
                for (int j = 0; j < levelRequisitesCount; j++) {

                    if(j == levelId) {

                        StarsRequisites.LevelRequisite levelRequisite = starsRequisites.worldStarsRequisites[i].levelRequisites[j];
                           
                        //2-STAR
                        twoStarsScore = levelRequisite.twoStarsRequisite.score;
                        twoStarsIdols = levelRequisite.twoStarsRequisite.idols;
                        //3-STAR
                        threeStarsScore = levelRequisite.threeStarsRequisite.score;
                        threeStarsIdols = levelRequisite.threeStarsRequisite.idols;

                    }

                }

            }

        }

    }

#if UNITY_EDITOR

    void OnApplicationQuit() {

        //Save the game before the game quits
        GameDataController.Save();
        //WARNING!! Maybe on iOS 'OnApplicationPause' event must be called in order to save the game properly

    }

#endif

    void OnApplicationPause() {

        GameDataController.Save();

    }

    public void Respawn() {

        int sceneIndex;

        if (BundleManager.scenesList == null || BundleManager.scenesList.Count == 0 || LoadSc.worldSelected == 0)
            sceneIndex = SceneManager.GetActiveScene().buildIndex;
        else {
            if (specialLevel) {
                sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);
                LoadSc.specialPrevLevelScene = true;
                LoadSc.specialNextLevelScene = true;
                LoadSc.fromBundle = true;
            } else {
                sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);
                LoadSc.fromBundle = true;
            }
        }

        LoadSc.prevScene = sceneIndex;
        LoadSc.nextScene = sceneIndex;

        SceneManager.LoadSceneAsync("LoadScreen");

    }

    public void TryAgain() {

        if (respawnLocation != null) {

            playerSetup.transform.position = respawnLocation.position;
            playerSetup.transform.rotation = respawnLocation.rotation;

            playerMovementController.transform.localPosition = initPlayerPos;
            playerMovementController.transform.localRotation = initPlayerRot;
            cameraMovementController.transform.localPosition = initCameraArmPos;
            cameraMovementController.transform.localRotation = initCameraArmRot;
            cameraFollowingController.transform.localPosition = initCameraPos;
            cameraFollowingController.transform.localRotation = initCameraRot;

            if(playerCinematicController.startPlatform.activeSelf) playerCinematicController.startPlatform.SetActive(false);

        } else {

            GetAndDisableNearbyEntities(playerHealth.transform.position);

            cameraMovementController.transform.position += cameraMovementController.transform.forward * cameraDeathOffset;
            
        }

        playerMovementController.ResetPosition();

        playerHealth.RevivePlayer();
            
        playerHealth.gameObject.SetActive(true);
        
    }

    void GetAndDisableNearbyEntities(Vector3 centerPosition) {

        Collider[] stuff = Physics.OverlapSphere(centerPosition, 40f, -1, QueryTriggerInteraction.Collide);

        for(int i = 0; i < stuff.Length; i++) {

            Collider col = stuff[i];

            RoadEntity roadEntity = col.GetComponentInParent<RoadEntity>();

            if (roadEntity != null && roadEntity.threatLevel == RoadEntity.ThreatLevel.DANGEROUS) roadEntity.gameObject.SetActive(false);
            
        }

    }

    public void SetRespawnLocation(Transform location) {

        respawnLocation = location;

    }

    public void SetCheckpoint(CheckpointArea checkpoint) {

        if (checkpoint == null) {

            actualPlayerState = null;

            _activeCheckpoint = -1;

        } else {

            actualPlayerState = GetPlayerState();

            int checkpointIndex = checkpoints.IndexOf(checkpoint);
            _activeCheckpoint = checkpointIndex;

        }

    }

    public PlayerCheckpointState GetPlayerState() {
        
        PlayerCheckpointState playerState = new PlayerCheckpointState();

        playerState.mount = mounts.mounts[mounts.activeMount].mountType;
        playerState.score = scoreController.totalScore;
        playerState.multiplier = scoreController.multiplier;

        playerState.idolCounter = m_idolCounter;

        playerState.levelProgression = progressionBar.actualProgression;

        if (fogPlane != null) {

            float fade = fogPlane.sharedMaterial.GetFloat("_GlobalFade");

            if (fade == 0) playerState.activeFog = true;
            else if(fade == 1) playerState.activeFog = false;

        }

        if(inGameSound != null) {

            playerState.soundBaseLayerLoop = inGameSound.layerBase.activeClip;
            playerState.soundLayer1Loop = inGameSound.layer1.activeClip;
            playerState.soundLayer2Loop = inGameSound.layer2.activeClip;
            playerState.soundExtraLayerLoop = inGameSound.layerExtra.activeClip;

        }

        return playerState;

    }

    public void SetPlayerOnCheckpoint() {

        playerCinematicController.initLevel = false;

        checkpoints[activeCheckpoint].gameObject.SetActive(false);

        playerSetup.transform.position = checkpoints[activeCheckpoint].respawnLocation.position;
        playerSetup.transform.rotation = checkpoints[activeCheckpoint].respawnLocation.rotation;

        scoreController.totalScore = actualPlayerState.score;
        scoreController.multiplier = actualPlayerState.multiplier;
        m_idolCounter = actualPlayerState.idolCounter;
        //Debug.Log(actualPlayerState.mount);
        playerMovementController.ChangeMount(actualPlayerState.mount);

        progressionBar.actualProgression = actualPlayerState.levelProgression;

        if (fogPlane != null) {

            if (actualPlayerState.activeFog) {

                fogPlane.sharedMaterial.SetFloat("_GlobalFade", 0f);

            } else {

                fogPlane.sharedMaterial.SetFloat("_GlobalFade", 1f);

            }

        }

        if(inGameSound != null) {

            inGameSound.initLoopLayerBase = actualPlayerState.soundBaseLayerLoop; //ChangeLoop(InGameSound.SoundLayerTypes.LAYER_BASE, actualPlayerState.soundBaseLayerLoop, 0.5f);
            inGameSound.initLoopLayer1 = actualPlayerState.soundLayer1Loop; //ChangeLoop(InGameSound.SoundLayerTypes.LAYER1, actualPlayerState.soundLayer1Loop, 0.5f);
            inGameSound.initLoopLayer2 = actualPlayerState.soundLayer2Loop; //ChangeLoop(InGameSound.SoundLayerTypes.LAYER2, actualPlayerState.soundLayer2Loop, 0.5f);
            inGameSound.initLoopLayerExtra = actualPlayerState.soundExtraLayerLoop; //ChangeLoop(InGameSound.SoundLayerTypes.LAYER_EXTRA, actualPlayerState.soundExtraLayerLoop, 0.5f);

        }

    }

}
