﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class LoadAssetBundleScene : MonoBehaviour {

    public Button loadScene;

    public bool specialLevel;

    IEnumerator Start() {

		Caching.ClearCache ();

        loadScene.interactable = false;

        yield return BundleManager.Initialize();

        loadScene.interactable = true;

    }
    
    public void LoadScene(int index) {

        if(specialLevel)
            SceneManager.LoadSceneAsync(BundleManager.specialScenesList[index]);
        else
            SceneManager.LoadSceneAsync(BundleManager.scenesList[index]);

    }

    void OnGUI() {

        GUILayout.Label("Progression..." + BundleManager.progression.ToString());

        Rect pos = new Rect(250f, 0, 250f, 25f);

        if(BundleManager.scenesList != null && BundleManager.scenesList.Count > 0) {

            GUI.Label(pos, "Scenes List");

            for(int i = 0; i < BundleManager.scenesList.Count; i++) {

                pos.y += 25f;

                GUI.Label(pos, (i + 1) + " - " + BundleManager.scenesList[i]);

            }

        }

        pos.x += 250f;
        pos.y = 0;

        if (BundleManager.specialScenesList != null && BundleManager.specialScenesList.Count > 0) {

            GUI.Label(pos, "Special Scenes List");

            for (int i = 0; i < BundleManager.specialScenesList.Count; i++) {

                pos.y += 25f;

                GUI.Label(pos, (i + 1) + " - " + BundleManager.specialScenesList[i]);

            }

        }

    }

}
