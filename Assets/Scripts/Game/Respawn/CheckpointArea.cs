﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CheckpointArea : MonoBehaviour {
    
    public Transform respawnLocation;

    public GameObject yujuEffect;

    public AudioClip yujuSound;

    GameManager gameManager;
    bool playerInside = false;

    [SerializeField]
    public float distanceFromStart;

    ProgressionBarUI progressionBar;

    AudioSource audioSource;

    // Use this for initialization
    void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        progressionBar = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<ProgressionBarUI>();

        audioSource = GetComponent<AudioSource>();

        //respawnLocation = transform.GetChild(0);

	}
    
    void OnTriggerEnter(Collider other) {

        if(!playerInside && other.tag == "Player") {

            playerInside = true;

            gameManager.SetCheckpoint(this);

            if(yujuEffect != null) yujuEffect.SetActive(true);

            if (yujuSound != null && audioSource != null) audioSource.PlayOneShot(yujuSound);

            if (progressionBar != null) progressionBar.ActivateCheckpointMarker();

            //Debug.Log(GameManager.activeCheckpoint);
            
        }

    }

    /*void OnTriggerExit(Collider other) {

        if(playerInside && other.tag == "Player") {

            playerInside = false;

            gameManager.SetRespawnLocation(null);

        }

    }*/

}
