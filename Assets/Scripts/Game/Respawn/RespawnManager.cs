﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RespawnManager : MonoBehaviour {

    public GameObject continueButton;
    public GameObject watchAdOption;
    public Slider watchAdCountdown;
    public float countdownSeconds = 10f;
    public GameObject payDiamondOption;

    GameManager gameManager;

    RewardedVideoLoader rewardedVideoLoader;

    RespawnManager respawnManager;

    //RewardBasedVideoAd rewardedVideo;
    //bool rewardedVideoEventHandlersSet = false;

    bool shown = false;
    bool callback = false;
    string type = "";
    double amount = 0;
    object sender;
    
	void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        rewardedVideoLoader = GameObject.FindGameObjectWithTag("HUD").GetComponent<RewardedVideoLoader>();

        respawnManager = this;

        StartCoroutine(AnimateCountdown());

        if (GameDataController.gameData.diamondCurrency < 1) payDiamondOption.GetComponent<Button>().interactable = false;

	}

    void Update() {

        if (rewardedVideoLoader.rewardedVideoRewarded) {

            ActivateContinue();

        }

    }
    
    public void ActivateContinue() {
        
        StopAllCoroutines();

        watchAdOption.SetActive(false);
        watchAdCountdown.gameObject.SetActive(false);
        payDiamondOption.SetActive(false);

        continueButton.GetComponent<Button>().interactable = true;
        continueButton.GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);

    }

    public void ContinueFromCheckpoint() {

        PlayerPrefs.SetInt("CheckPoint", PlayerPrefs.GetInt("CheckPoint") + 1);
        gameManager.Respawn();

    }

    public void RespawnWithAd() {

   //     Debug.Log("ADMOB - Rewarded video loaded: " + RewardedVideoLoader.rewardedVideo.IsLoaded());

      /*  if (RewardedVideoLoader.rewardedVideo.IsLoaded()) {

            Debug.Log("ADMOB - Trying to show video...");

            RewardedVideoLoader.rewardedVideo.Show();
            
            shown = true;

        }*/

    }

    /*public void RewardedVideoRewarded(object sender, Reward args) {

        ActivateContinue();

        callback = true;

        string type = args.Type;
        this.type = type;
        double amount = args.Amount;
        this.amount = amount;
        //this.sender = sender;

        //rewardedVideoLoader.SetValue();

        respawnManager.ActivateContinue();

        continueButton.GetComponent<Button>().interactable = true;
        continueButton.GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);
        
        //ActivateContinue();

        
        
    }*/

    /*public void RewardedAdCallback(ShowResult result) {

        switch (result) {

            case ShowResult.Finished:
                gameManager.Respawn();
                break;

            default:
                break;

        }

    }*/

    public void RespawnWithDiamonds(int diamonds) {

        if (diamonds <= 0) return;

        if (GameManager.BuyWithDiamonds((uint)diamonds)) {

            ActivateContinue();

        }
        
    }

    IEnumerator AnimateCountdown() {

        float timer = 0f;
        float perc;
        while(timer < countdownSeconds) {

            timer += Time.deltaTime;

            perc = timer / countdownSeconds;
            watchAdCountdown.value = 1 - perc;

            yield return null;

        }

        watchAdCountdown.gameObject.SetActive(false);
        watchAdOption.SetActive(false);

    }

    /*void OnGUI() {

        GUILayout.Label("SHOWN " + shown.ToString());
        GUILayout.Label("CALLBACK " + callback.ToString() + "\n");
        GUILayout.Label("Type " + type);
        GUILayout.Label("Amount " + amount.ToString());
        if(sender != null) GUILayout.Label("SENDER " + sender.GetType().ToString());
        //GUILayout.Label("SHIT " + rewardedVideoLoader.shit.ToString());

    }*/
    
}
