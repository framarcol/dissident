﻿using UnityEngine;
using UnityEngine.UI;
//using Firebase;
using System.Collections;

public class RespawnManagerMenu : MonoBehaviour {
    
    RewardedVideoLoader rewardedVideoLoader;

    RespawnManagerMenu respawnManagerMenu;

    float timerFade;

    //RewardBasedVideoAd rewardedVideo;
    //bool rewardedVieoEventHandlersSet = false;

    public GameObject InappPurchasing;

    bool aparecer, desaparecer;

    bool shown = false;
    bool callback = false;
    bool recompensado = false;
    string type = "";
    double amount = 0;
    object sender;
    string num;
    
	void Start () {

        rewardedVideoLoader = GetComponent<RewardedVideoLoader>();

        respawnManagerMenu = this;

        GetComponent<CanvasGroup>().interactable = false;
        aparecer = false;
        desaparecer = false;
        num = System.DateTime.Now.ToShortDateString();

    }

    void Update() {

        if (!recompensado &&rewardedVideoLoader.rewardedVideoRewarded) {

            recompensado = true;
            StartCoroutine(InappPurchasing.GetComponent<EfectoCompras>().ActualizarDiamantes(3));
            PlayerPrefs.SetInt("Fecha" + num, 1);
            GetComponent<CanvasGroup>().interactable = false;
            gameObject.SetActive(false);
        }

        if (aparecer)
        {
           AparicionVentana();
        }

        if (desaparecer)
        {
           DesaparicionVentana();
        }
      

    }

    public void RespawnWithAd() {

       /* Debug.Log("ADMOB - Rewarded video loaded: " + RewardedVideoLoader.rewardedVideo.IsLoaded());

        if (RewardedVideoLoader.rewardedVideo.IsLoaded()) {

            Debug.Log("ADMOB - Trying to show video...");

            RewardedVideoLoader.rewardedVideo.Show();
            
            shown = true;

        }*/

    }

    public void ActivarVentana()
    {
        aparecer = true;
    }

    public void DesactivarVentana()
    {
        desaparecer = true;
    }

    public void AparicionVentana()
    {
        if (GetComponent<CanvasGroup>().alpha < 1)
        {
            GetComponent<CanvasGroup>().alpha += 0.04f;
        }
        else
        {
            GetComponent<CanvasGroup>().interactable = true;
            aparecer = false;
        }
    }

    public void DesaparicionVentana()
    {
        if (GetComponent<CanvasGroup>().alpha > 0)
        {
            GetComponent<CanvasGroup>().alpha -= 0.04f;
        }
        else
        {
            GetComponent<CanvasGroup>().interactable = false;
            desaparecer = false;
        }
    }

}
