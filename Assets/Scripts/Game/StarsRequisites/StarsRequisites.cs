﻿using UnityEngine;
using System.Collections.Generic;

public class StarsRequisites : ScriptableObject {

    [System.Serializable]
	public class TwoStarsRequisite {

        public int score;
        public int idols;

    }

    [System.Serializable]
    public class ThreeStarsRequisite {

        public int score;
        public int idols;

    }

    [System.Serializable]
    public class LevelRequisite {

        public int nextLevel = -1;

        public TwoStarsRequisite twoStarsRequisite;
        public ThreeStarsRequisite threeStarsRequisite;

        public LevelRequisite() {

            twoStarsRequisite = new TwoStarsRequisite();
            threeStarsRequisite = new ThreeStarsRequisite();

        }

    }

    [System.Serializable]
    public class WorlStarsdRequisites {

        public World world;

        public List<LevelRequisite> levelRequisites;

        public WorlStarsdRequisites() {

            levelRequisites = new List<LevelRequisite>();

        }

    }

    [System.Serializable]
    public class SpecialLevelRequisites {

        public int levelId;

        public string levelName = "Special :D";

        public int diamondReward = 1;

        public int score;

        public int idols;

        public int weaponId = -1;

        public int nextLevel = -1;

    }

    public List<bool> foldoutControllers = new List<bool>();

    public List<WorlStarsdRequisites> worldStarsRequisites = new List<WorlStarsdRequisites>();

    public bool specialLevelsFoldout = false;

    public List<SpecialLevelRequisites> specialLevelRequisites = new List<SpecialLevelRequisites>();

    public SpecialLevelRequisites GetSpecialLevelRequisites(int id) {

        for(int i = 0; i < specialLevelRequisites.Count; i++) {

            if(specialLevelRequisites[i].levelId == id) {

                return specialLevelRequisites[i];

            }

        }

        return null;

    }

    public int GetNextLevel(World world, uint levelId) {

        int worldCount = worldStarsRequisites.Count;
        for (int i = 0; i < worldCount; i++) {

            if (worldStarsRequisites[i].world == world) {

                int levelRequisitesCount = worldStarsRequisites[i].levelRequisites.Count;
                for (int j = 0; j < levelRequisitesCount; j++) {

                    if (j == levelId) {

                        LevelRequisite levelRequisite = worldStarsRequisites[i].levelRequisites[j];

                        return levelRequisite.nextLevel;

                    }

                }

            }

        }

        return -1;

    }

}