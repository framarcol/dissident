﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Collider))]
public class HologramMap : MonoBehaviour {

    public Transform mapParent;

    public float minLimit;
    public float maxLimit;
    public int numMundo;

    public float introAnimationDuration = 1f;
    public float initAnimationAlpha = 0.25f;

    Collider scrollAreaCollider;

    Plane mapPlane;

    Vector3 lastPos;
#if UNITY_EDITOR
    bool scrolling;
#endif
    int fingerId = -1;

    public List<Material> baseHologramMaterials = new List<Material>();
    List<Material> instancedMaterials = new List<Material>();
    int offsetProperty;
    public float bandsOverlayDuration = 1f;
    public AnimationCurve hologramFlickerCurve;
    public float flickerDuration;
    int powerProperty;

    int globalAlphaProperty;

    [Header("Background terrain")]
    public GameObject backgroundParent;
    public int backgroundRenderQueueOffset = -1;
    List<GameObject> backgroundChunks = new List<GameObject>();

    public float chunkSize;
    float chunkOffset;
    float actualRightLimit, actualLeftLimit;
    int activeChunk;

    [Header("Scroll inertia")]
    public float inertiaDuration = 1.5f;
    public float inertiaFactor = 1f;
    public float inertiaVelocityLimit = 50f;
    float touchEndedTime;
    float scrollVelocity;
    Vector3 deltaPosition;

    [Header("Scroll overflow")]
    public float overflowDampFactor = 1f;
    public float overflowLimitFactor = 1f;

    Vector3 mapPosition;

    bool initialized = false;

	// Use this for initialization
	IEnumerator Start () {

        Initialize();

        scrollAreaCollider.enabled = false;
        int estrellas = 0;
        if (GameDataController.gameData.premiumState)
        {
            if (numMundo != -1)
            {
                for (int i = 0; i < 15; i++)
                {
                    if (GameDataController.gameData.stars[numMundo, i] > 0) estrellas = i + 1;
                }
            }
            else
            {
                // for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;
            }
        }
        else
        {
            if (numMundo != -1)
            {
                for (int i = 0; i < 15; i++)
                {
                    if (GameDataController.gameData.stars[numMundo, i] == 0)
                    {
                        estrellas = i;
                        break;
                    }
                    
                }
            }
        }
     

        mapPosition.x = (estrellas * -3) + maxLimit;
        mapParent.localPosition = mapPosition;

        yield return AnimateIntro();

        if (baseHologramMaterials.Count != 0) StartCoroutine(AnimateOverlayBands());
        if (baseHologramMaterials.Count != 0) StartCoroutine(AnimateFlicker());

        scrollAreaCollider.enabled = true;

        initialized = true;

    }
	
    void OnEnable() {

        StartCoroutine(Start());

    }

    private void Initialize() {

        mapPosition = mapParent.localPosition;

        scrollAreaCollider = GetComponent<Collider>();

        mapPlane = new Plane(transform.up, transform.position);

        //Get the background chunks
        if (backgroundParent != null) {

            int childCount = backgroundParent.transform.childCount;
            for (int i = 0; i < childCount; i++) {

                backgroundChunks.Add(backgroundParent.transform.GetChild(i).gameObject);

                backgroundParent.transform.GetChild(i).gameObject.SetActive(false);

            }
            backgroundChunks[0].SetActive(true);
            activeChunk = 0;

            UpdateBackgroundRenderQueues();

        }
        
        chunkOffset = chunkSize / 2;
        actualRightLimit = chunkOffset;

        offsetProperty = Shader.PropertyToID("_OverlayBandsOffset");
        powerProperty = Shader.PropertyToID("_Power");
        globalAlphaProperty = Shader.PropertyToID("_GlobalAlpha");

    }
    
    private void UpdateBackgroundRenderQueues() {

        for (int i = 0; i < backgroundChunks.Count; i++) {

            Renderer renderer = backgroundChunks[i].GetComponent<Renderer>();

            int actualRenderQueue = renderer.material.shader.renderQueue;

            renderer.material.renderQueue = actualRenderQueue + backgroundRenderQueueOffset;

            instancedMaterials.Add(renderer.material);

        }

    }
    
    IEnumerator AnimateIntro() {
        
        float alphaDiff = 1 - initAnimationAlpha;

        float timer = 0f;
        while (timer < introAnimationDuration) {

            timer += Time.deltaTime;

            float perc = timer / introAnimationDuration;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            transform.localScale = new Vector3(perc, perc, perc);

            for (int i = 0; i < baseHologramMaterials.Count; i++) {

                baseHologramMaterials[i].SetFloat(globalAlphaProperty, initAnimationAlpha + (perc * alphaDiff));

            }

            for (int i = 0; i < instancedMaterials.Count; i++) {

                instancedMaterials[i].SetFloat(globalAlphaProperty, initAnimationAlpha + (perc * alphaDiff));

            }

            yield return null;

        }

        transform.localScale = new Vector3(1, 1, 1);

        for (int i = 0; i < baseHologramMaterials.Count; i++) {

            baseHologramMaterials[i].SetFloat(globalAlphaProperty, 1f);

        }

        for (int i = 0; i < instancedMaterials.Count; i++) {

            instancedMaterials[i].SetFloat(globalAlphaProperty, 1f);

        }
        
    }

    public IEnumerator DisableMap() {

        if (!gameObject.activeSelf) yield break;

        scrollAreaCollider.enabled = false;

        yield return StartCoroutine(AnimateOutro());

        gameObject.SetActive(false);

    }

    IEnumerator AnimateOutro() {

        //float alphaDiff = 1 - initAnimationAlpha;

        float timer = 0f;
        while (timer < introAnimationDuration) {

            timer += Time.deltaTime;

            float perc = timer / introAnimationDuration;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            transform.localScale = new Vector3(1 - perc, 1 - perc, 1 - perc);

            for (int i = 0; i < baseHologramMaterials.Count; i++) {

                baseHologramMaterials[i].SetFloat(globalAlphaProperty, 1 - perc);

            }

            for (int i = 0; i < instancedMaterials.Count; i++) {

                instancedMaterials[i].SetFloat(globalAlphaProperty, 1 - perc);

            }

            yield return null;

        }

        transform.localScale = new Vector3(1, 1, 1);

        for (int i = 0; i < baseHologramMaterials.Count; i++) {

            baseHologramMaterials[i].SetFloat(globalAlphaProperty, 1f);

        }

        for (int i = 0; i < instancedMaterials.Count; i++) {

            instancedMaterials[i].SetFloat(globalAlphaProperty, 1f);

        }

    }

    // Update is called once per frame
    void Update () {

        if (!initialized) return;

        MapScroll();

        if(backgroundParent != null) UpdateBackgroundChunks();

	}

    private void MapScroll() {

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit)) {

                if (raycastHit.collider == scrollAreaCollider) {

                    lastPos = GetHitPoint(Input.mousePosition);

                    scrolling = true;
                    
                }

            }

        }

        if (scrolling) {

            Vector3 actualPos = GetHitPoint(Input.mousePosition);

            deltaPosition = actualPos - lastPos;

            lastPos = actualPos;
            
        }

        if (Input.GetMouseButtonUp(0)) {

            deltaPosition = new Vector3(0, 0, 0);

            scrolling = false;

        }

#endif

        int touchCount = Input.touchCount;

        if(fingerId == -1 && scrollVelocity != 0) {

            float t = (Time.time - touchEndedTime) / inertiaDuration;
            float frameVelocity = Mathf.Lerp(scrollVelocity, 0f, t);

            mapPosition.x += frameVelocity * Time.deltaTime;

            if (t > inertiaDuration) scrollVelocity = 0f;

        }
       
        for(int i = 0; i < touchCount; i++) {

            Touch touch = Input.touches[i];

            switch (touch.phase) {

                case TouchPhase.Began:

                    if (fingerId != -1) break;

                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
                    RaycastHit raycastHit;

                    if (Physics.Raycast(ray, out raycastHit)) {

                        if (raycastHit.collider == scrollAreaCollider) {

                            lastPos = GetHitPoint(touch.position);

                            fingerId = touch.fingerId;

                        }

                    }

                    scrollVelocity = 0f;

                    break;

                case TouchPhase.Moved:
                case TouchPhase.Stationary:

                    if (touch.fingerId != fingerId) break;

                    Vector3 actualPos = GetHitPoint(touch.position);

                    deltaPosition = actualPos - lastPos;

                    lastPos = actualPos;

                    if (touch.deltaTime == 0f) break;

                    float actualVelocity = (deltaPosition.x / touch.deltaTime) * inertiaFactor;
                    scrollVelocity = 0.8f * actualVelocity + 0.2f * scrollVelocity;
                    
                    break;

                case TouchPhase.Ended:

                    if(touch.fingerId == fingerId) {

                        deltaPosition = new Vector3(0,0,0);

                        if (scrollVelocity > inertiaVelocityLimit) scrollVelocity = inertiaVelocityLimit;
                        else if (scrollVelocity < -inertiaVelocityLimit) scrollVelocity = -inertiaVelocityLimit;

                        touchEndedTime = Time.time;
                        fingerId = -1;
                        
                    }

                    break;

            }

        }

        MoveMap(deltaPosition);

    }

    private void MoveMap(Vector3 deltaPosition) {

        if (
            #if UNITY_EDITOR 
                scrolling || 
            #endif
                fingerId != -1) {

            float overflowOffset = 0f;
            if (mapPosition.x < minLimit) overflowOffset = minLimit - mapPosition.x;
            else if (mapPosition.x > maxLimit) overflowOffset = mapPosition.x - maxLimit;

            float compensation = 1 / (overflowOffset + 1);
            mapPosition.x += deltaPosition.x * (Mathf.Pow(compensation, overflowDampFactor));

        } else {

            if (mapPosition.x < minLimit) {

                mapPosition.x = Mathf.Lerp(mapPosition.x, minLimit, overflowLimitFactor * Time.deltaTime);
                if (scrollVelocity != 0) scrollVelocity = 0f;

            } else if (mapPosition.x > maxLimit) {

                mapPosition.x = Mathf.Lerp(mapPosition.x, maxLimit, overflowLimitFactor * Time.deltaTime);
                if (scrollVelocity != 0) scrollVelocity = 0f;

            }

        }

        mapParent.localPosition = mapPosition;

    }

    private Vector3 GetHitPoint(Vector3 pixelCoordinates) {

        Vector3 point = new Vector3(0,0,0);

        Ray ray = Camera.main.ScreenPointToRay(pixelCoordinates);

        float distance;

        if (mapPlane.Raycast(ray, out distance)) {

            point = ray.GetPoint(distance);

        }

        return point;

    }

    private void UpdateBackgroundChunks() {

        if(mapParent.localPosition.x < -actualRightLimit) {

            if (activeChunk >= backgroundChunks.Count - 1) return;

            backgroundChunks[activeChunk].SetActive(false);
            backgroundChunks[++activeChunk].SetActive(true);

            actualLeftLimit = actualRightLimit;
            actualRightLimit += chunkOffset;
            
        }else if(mapParent.localPosition.x > -actualLeftLimit) {

            if (activeChunk <= 0) return;

            backgroundChunks[activeChunk].SetActive(false);
            backgroundChunks[--activeChunk].SetActive(true);

            actualRightLimit = actualLeftLimit;
            actualLeftLimit -= chunkOffset;
            

        }
        
    }

    IEnumerator AnimateOverlayBands() {

        float initOffset = baseHologramMaterials[0].GetFloat(offsetProperty);
        float perc;
        float timer = 0f;

        while (true) {

            yield return new WaitForSeconds(5f);

            timer = 0f;
            while (timer < bandsOverlayDuration) {

                timer += Time.deltaTime;

                perc = timer / bandsOverlayDuration;

                for (int i = 0; i < baseHologramMaterials.Count; i++) {

                    baseHologramMaterials[i].SetFloat(offsetProperty, initOffset - perc);

                }

                for(int i = 0; i < instancedMaterials.Count; i++) {

                    instancedMaterials[i].SetFloat(offsetProperty, initOffset - perc);

                }

                yield return null;

            }

            for (int i = 0; i < baseHologramMaterials.Count; i++) {

                baseHologramMaterials[i].SetFloat(offsetProperty, initOffset);

            }

            for (int i = 0; i < instancedMaterials.Count; i++) {

                instancedMaterials[i].SetFloat(offsetProperty, initOffset);

            }

        }

    }

    IEnumerator AnimateFlicker() {

        float initPower = baseHologramMaterials[0].GetFloat(powerProperty);
        float perc;
        float timer;

        while (true) {

            yield return new WaitForSeconds(8f);

            timer = 0f;
            while (timer < flickerDuration) {

                timer += Time.deltaTime;

                perc = timer / flickerDuration;

                float offset = hologramFlickerCurve.Evaluate(perc);

                for (int i = 0; i < baseHologramMaterials.Count; i++) {

                    baseHologramMaterials[i].SetFloat(powerProperty, initPower + offset);

                }

                for (int i = 0; i < instancedMaterials.Count; i++) {

                    instancedMaterials[i].SetFloat(powerProperty, initPower + offset);

                }

                yield return null;

            }

            for (int i = 0; i < baseHologramMaterials.Count; i++) {

                baseHologramMaterials[i].SetFloat(powerProperty, initPower);

            }

            for (int i = 0; i < instancedMaterials.Count; i++) {

                instancedMaterials[i].SetFloat(powerProperty, initPower);

            }

        }

    }
    
    /*
    void OnGUI() {
        
        GUILayout.Label(scrollVelocity.ToString());

    }*/

}
