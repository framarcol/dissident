﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LevelEvent : UnityEvent { }

[ExecuteInEditMode]
public class LevelMap : MonoBehaviour {

    public World world;
    //[Range(1,15)]
    public int level;
    
    List<GameObject> medalList = new List<GameObject>();
    List<GameObject> numberList = new List<GameObject>();

    [Range(0.2f, 1.5f)]
    public float levelHeight = 1;

    Collider medalCollider;

    Transform medal;
    float medalInitOffset;
    Transform linePointer;

    int fingerId = -1;
#if UNITY_EDITOR
    bool initClick;
#endif

    public LevelEvent OnClick;

    void Start () {

        medal = transform.Find("Medal");
        medalInitOffset = medal.position.y - levelHeight;
        linePointer = transform.Find("LinePointer");

        Transform medalListParent = medal.Find("MedalList");
        for(int i = 0; i < medalListParent.childCount; i++) {

            medalList.Add(medalListParent.GetChild(i).gameObject);

        }

        Transform numberListParent = medal.Find("NumberList");
        for (int i = 0; i < numberListParent.childCount; i++) {

            numberList.Add(numberListParent.GetChild(i).gameObject);

            if (Application.isPlaying) {

                Renderer renderer = numberListParent.GetChild(i).GetComponent<Renderer>();

                renderer.material.renderQueue = renderer.material.shader.renderQueue + 1;

            }

        }

        medalCollider = GetComponentInChildren<Collider>();

	}
	
	void Update () {

        UpdateLevelHeight();

        UpdateLevelNumber();

        CheckLevelTap();

	}

    private void CheckLevelTap() {

        Ray ray;
        RaycastHit rayHit;

#if UNITY_EDITOR
        
        if (Input.GetMouseButtonDown(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == medalCollider) {

                    initClick = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == medalCollider) {

                    if (initClick) {

                        //TODO INVOKE METHOD
                        //Debug.Log("CLICK");
                        OnClick.Invoke();
                        
                    }
                    
                }

            }else {

                initClick = false;

            }

        }

#endif

        int touchCount = Input.touchCount;
        for(int i = 0; i < touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            switch (touch.phase) {

                case TouchPhase.Began:

                    if (fingerId != -1) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if(rayHit.collider == medalCollider) {

                            fingerId = touch.fingerId;

                        }

                    }

                    break;

                case TouchPhase.Ended:

                    if (touch.fingerId != fingerId) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == medalCollider) {

                            //TODO Invoke TAP METHOD
                            OnClick.Invoke();

                            fingerId = -1;

                        }

                    }

                    break;

            }

        }

    }

    private void UpdateLevelHeight() {

        if (Application.isPlaying) return;

        Vector3 lineScale = new Vector3(linePointer.localScale.x, levelHeight, linePointer.localScale.z);
        linePointer.localScale = lineScale;

        //medal.transform.position = new Vector3(medal.transform.position.x, medalInitOffset + levelHeight, medal.transform.position.z);

    }

    private void UpdateLevelNumber() {

        if (Application.isPlaying) return;

        numberList.ForEach(
            
            (GameObject go) => {

                int index = numberList.IndexOf(go);

                if (index == level-1) go.SetActive(true);
                else go.SetActive(false);

            }
            
        );

    }

}
