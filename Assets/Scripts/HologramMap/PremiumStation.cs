﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[System.Serializable]
public class PremiumEvent : UnityEvent { }

public class PremiumStation : MonoBehaviour {

    public PremiumEvent OnClick;

    Collider premiumCollider;

    bool initClick;
    int fingerId = -1;
    
	void Start () {

        premiumCollider = GetComponent<Collider>();

	}
	
	void Update () {

        CheckTap();

	}

    private void CheckTap() {

        Ray ray;
        RaycastHit rayHit;

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {
                
                if (rayHit.collider == premiumCollider) {

                    Debug.Log("CLICKDOWN");
                    initClick = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == premiumCollider) {

                    if (initClick) {

                        //TODO INVOKE METHOD
                        Debug.Log("CLICK");
                        OnClick.Invoke();

                    }

                }

            } else {

                initClick = false;

            }

        }

#endif

        int touchCount = Input.touchCount;
        for (int i = 0; i < touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            switch (touch.phase) {

                case TouchPhase.Began:

                    if (fingerId != -1) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == premiumCollider) {

                            fingerId = touch.fingerId;

                        }

                    }

                    break;

                case TouchPhase.Ended:

                    if (touch.fingerId != fingerId) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == premiumCollider) {

                            //TODO Invoke TAP METHOD
                            OnClick.Invoke();

                            fingerId = -1;

                        }

                    }

                    break;

            }

        }

    }

}
