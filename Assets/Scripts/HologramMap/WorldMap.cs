﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldMap : MonoBehaviour {

    public World world;

    WorldSelectorMap worldSelector;
    Collider worldMapCollider;

    public AudioClip miClip;
    public AudioSource miOrigenSonido;

    public Animator popap;
    public Text textoPopap;
    bool[] MundoCacheado;

    public GameObject popapLoading;
    public GameObject botonAceptarLoading;
    public Text textoPopapLoading;
    public GameObject popapConfirmarDescarga;
    public GameObject popapCofirmarDescargaSnow;
    public GameObject popapYldonTC, popapYldonTSC, popapMirakaTC, popapMirakaTSC;
    public Animator popapYldon, popapMiraka;
    public string mundoConfirmar;
    public static int worldRanking;

    public GameObject panelEventoAbajo, panelEventoArriba;


    int fingerId = -1;
    #if UNITY_EDITOR
    public bool initClick;
    #endif

    void Start () {

        MundoCacheado = new bool[3];

        worldSelector = GetComponentInParent<WorldSelectorMap>();

        worldMapCollider = GetComponent<Collider>();
            
    }
	
    public void TriggerWorldSelection() {

        worldSelector.SelectWorld((int)world);

    }

    public void BtnConfirmarDescarga()
    {
        StartCoroutine(InisialisarTienda(mundoConfirmar));
        popapConfirmarDescarga.SetActive(false);
        popapCofirmarDescargaSnow.SetActive(false);
    }

    public void BtnRechazarDescarga()
    {
        MovimientoCamaraMenu.descargaIniciada = false;
        popapConfirmarDescarga.SetActive(false);
        popapCofirmarDescargaSnow.SetActive(false);
    }

    IEnumerator InisialisarTienda(string mundoS)
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null)
        {
            botonAceptarLoading.SetActive(false);
            popapLoading.SetActive(true);

            MovimientoCamaraMenu.descargaIniciada = true;

            int numMundi = 0;

            switch (mundoS)
            {
                case "desert":
                    numMundi = 1;
                    break;
                case "snow":
                    numMundi = 2;
                    break;
            }
            
            if(MundoCacheado[numMundi] == false)
            {
                MundoCacheado[numMundi] = true;
                yield return BundleManager.Initialize(mundoS);
            }

            MovimientoCamaraMenu.descargaIniciada = false;

            if (BundleManager.scenesList.Count > 0)
            {
                popapLoading.SetActive(false);
                miOrigenSonido.PlayOneShot(miClip);

                switch (mundoS)
                {
                    case "desert":
                        if (GameDataController.gameData.worldsObtained[0] || GameDataController.gameData.premiumState) TriggerWorldSelection();
                        break;
                    case "snow":
                        if (GameDataController.gameData.worldsObtained[1] || GameDataController.gameData.premiumState) TriggerWorldSelection();
                        break;
                }
            }
            else
            {
                botonAceptarLoading.SetActive(true);
                textoPopapLoading.text = "Error al acceder. Inténtelo más tarde.";
            }
        }
        else
        {
            if (BundleManager.checkWorldCached(mundoS)) {

                botonAceptarLoading.SetActive(false);
                popapLoading.SetActive(true);

                MovimientoCamaraMenu.descargaIniciada = true;

                yield return BundleManager.InitializeOffline(mundoS);

                MovimientoCamaraMenu.descargaIniciada = false;

                if (BundleManager.scenesList.Count > 0)
                {
                    popapLoading.SetActive(false);
                    miOrigenSonido.PlayOneShot(miClip);
                    switch (mundoS)
                    {
                        case "desert":
                            if (GameDataController.gameData.worldsObtained[0]) TriggerWorldSelection();
                            break;
                        case "snow":
                            if (GameDataController.gameData.worldsObtained[1]) TriggerWorldSelection();
                            break;
                    }
                }
                else
                {
                    botonAceptarLoading.SetActive(true);
                    textoPopapLoading.text = "Error al acceder. Inténtelo más tarde.";
                }
            } 
            else
            {
                popap.SetTrigger("Fade");
                textoPopap.text = SeleccionaIdiomas.gameText[22, 10];
            }
        }
    }

    void cargarBundleDesierto()
    {
        if (!BundleManager.checkWorldCached("desert"))
        {
            MovimientoCamaraMenu.descargaIniciada = true;
            popapConfirmarDescarga.SetActive(true);
        
        }
        else
        {
            StartCoroutine(InisialisarTienda("desert"));
        }

    }

    void cargarMiraka()
    {
        if (!BundleManager.checkWorldCached("snow"))
        {
            MovimientoCamaraMenu.descargaIniciada = true;
            popapCofirmarDescargaSnow.SetActive(true);
            
        }
        else
        {
            StartCoroutine(InisialisarTienda("snow"));
        }

    }

    void Update () {

        Ray ray;
        RaycastHit rayHit;

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == worldMapCollider && !MovimientoCamaraMenu.descargaIniciada) {

                    initClick = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == worldMapCollider && !MovimientoCamaraMenu.descargaIniciada) {

                    if (initClick) {

                        //TODO INVOKE METHOD
                        EventoCLick();


                    }

                    initClick = false;

                }

            } else {

                initClick = false;

            }

        }

#endif

        int touchCount = Input.touchCount;
        for (int i = 0; i < touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            switch (touch.phase) {

                case TouchPhase.Began:

                    if (fingerId != -1) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == worldMapCollider && !MovimientoCamaraMenu.descargaIniciada) {

                            fingerId = touch.fingerId;

                        }

                    }

                    break;

                case TouchPhase.Ended:

                    if (touch.fingerId != fingerId) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == worldMapCollider && !MovimientoCamaraMenu.descargaIniciada) {

                            //TODO INVOKE METHOD
                            EventoCLick();

                            fingerId = -1;

                        }

                    }

                    break;

            }

        }


    }

    void EventoCLick()
    {
     //   Debug.Log("da");
        bool esAppleBundle;
#if UNITY_IOS
        esAppleBundle = true;
#elif UNITY_ANDROID
        esAppleBundle = false;
#endif
#if UNITY_EDITOR 
        GameDataController.gameData.worldsObtained[0] = false;
        GameDataController.gameData.worldsObtained[1] = false;
        GameDataController.gameData.specialLevelsCompleted[4] = true;
#endif

        if (world.ToString() == "CITY" || esAppleBundle)
        {
            if(world.ToString() == "CITY")
            {
                worldRanking = 0;
                miOrigenSonido.PlayOneShot(miClip);
                if (GameDataController.gameData.premiumState || GameDataController.gameData.specialLevelsCompleted[4])
                    TriggerWorldSelection();
                else
                {
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popap.SetTrigger("Fade");
                    textoPopap.text = SeleccionaIdiomas.gameText[3, 2];
                }
            }
            else if (world.ToString() == "DESERT")
            {
                worldRanking = 1;
                miOrigenSonido.PlayOneShot(miClip);
                if(GameDataController.gameData.premiumState || GameDataController.gameData.worldsObtained[0])
                TriggerWorldSelection();
                else
                {
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popapYldon.SetTrigger("Fade");
                    if (!GameDataController.gameData.specialLevelsCompleted[30])
                    {
                        popapYldonTC.SetActive(true);
                        popapYldonTSC.SetActive(false);
                    }
                    else
                    {
                        popapYldonTC.SetActive(false);
                        popapYldonTSC.SetActive(true);
                    }

                }
            }
            else if(world.ToString() == "SNOW")
            {
                worldRanking = 2;
                miOrigenSonido.PlayOneShot(miClip);
                if (GameDataController.gameData.premiumState || GameDataController.gameData.worldsObtained[1])
                    TriggerWorldSelection();
                else
                {
                    MovimientoCamaraMenu.descargaIniciada = true;

                    popapMiraka.SetTrigger("Fade");

                    if (!GameDataController.gameData.specialLevelsCompleted[50])
                    {
                        popapMirakaTC.SetActive(true);
                        popapMirakaTSC.SetActive(false);
                    }
                    else
                    {
                        popapMirakaTC.SetActive(false);
                        popapMirakaTSC.SetActive(true);
                    }

                }

            }
            else if(world.ToString() == "JUNGLE")
            {
                TriggerWorldSelection();
                miOrigenSonido.PlayOneShot(miClip);
                worldRanking = 3;
            }

        }
        else if (world.ToString() == "DESERT")
        {
            worldRanking = 1;
            // GameDataController.gameData.specialLevelsCompleted[5] = true;
            // GameDataController.gameData.worldsObtained[0] = true;
#if UNITY_EDITOR
            GameDataController.gameData.worldsObtained[0] = true;
            GameDataController.gameData.premiumState = true;
#endif
            if (GameDataController.gameData.worldsObtained[0] || GameDataController.gameData.premiumState)
            {
                TriggerWorldSelection();
                // cargarBundleDesierto();
            }
            else
            {
               /* if (BundleManager.checkWorldCached("desert"))
                {*/
                  //  cargarBundleDesierto();
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popapYldon.SetTrigger("Fade");

                    if (!GameDataController.gameData.specialLevelsCompleted[30])
                    {
                        popapYldonTC.SetActive(true);
                        popapYldonTSC.SetActive(false);
                    }
                    else
                    {
                        popapYldonTC.SetActive(false);
                        popapYldonTSC.SetActive(true);
                    }
               /* }
                else
                {
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popapConfirmarDescarga.SetActive(true);
                }*/

                   
            }
        }
        else if ((world.ToString() == "SNOW"))
        {
            worldRanking = 2;
#if UNITY_EDITOR
            GameDataController.gameData.worldsObtained[1] = true;
           // GameDataController.gameData.premiumState = false;
#endif
            if (GameDataController.gameData.worldsObtained[1] || GameDataController.gameData.premiumState)
            {
                TriggerWorldSelection();
            }
            else
            {
                /*if (BundleManager.checkWorldCached("snow"))
                {
                    cargarMiraka();*/
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popapMiraka.SetTrigger("Fade");

                    if (!GameDataController.gameData.specialLevelsCompleted[50])
                    {
                        popapMirakaTC.SetActive(true);
                        popapMirakaTSC.SetActive(false);
                    }
                    else
                    {
                        popapMirakaTC.SetActive(false);
                        popapMirakaTSC.SetActive(true);
                    }
               /* }
                else
                {
                    MovimientoCamaraMenu.descargaIniciada = true;
                    popapCofirmarDescargaSnow.SetActive(true);
                }*/

                  
            }
        }
        else if ((world.ToString() == "JUNGLE"))
        {
            TriggerWorldSelection();
            worldRanking = 3;
            
        }


    }

    public void DescargarMundo(string mundoS)
    {
        MovimientoCamaraMenu.descargaIniciada = true;
        if(mundoS == "snow")
        {   mundoConfirmar = mundoS;
            popapCofirmarDescargaSnow.SetActive(true);
        }else if(mundoS == "desert"){
            mundoConfirmar = mundoS;
            popapConfirmarDescarga.SetActive(true);
        }
        
    }

    public void DescargarMundoE(string mundoS)
    {
        MovimientoCamaraMenu.descargaIniciada = true;
        if (mundoS == "snow")
        {
            mundoConfirmar = mundoS;
            popapCofirmarDescargaSnow.SetActive(true);
        }
        else if (mundoS == "desert")
        {
            mundoConfirmar = mundoS;
            popapConfirmarDescarga.SetActive(true);
        }

        panelEventoAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        panelEventoArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
    }

    public IEnumerator IniciarMundo(string mundoS)
    {
        botonAceptarLoading.SetActive(false);
        popapLoading.SetActive(true);

        MovimientoCamaraMenu.descargaIniciada = true;

        yield return BundleManager.InitializeOffline(mundoS);

        MovimientoCamaraMenu.descargaIniciada = false;

        if (BundleManager.scenesList.Count > 0)
        {
            popapLoading.SetActive(false);
        }
        else
        {
            botonAceptarLoading.SetActive(true);
            textoPopapLoading.text = "Error al acceder. Inténtelo más tarde.";
        }
    }

    public void IniciarMundoE(string mundoS)
    {
     /*   botonAceptarLoading.SetActive(false);
        popapLoading.SetActive(true);

        MovimientoCamaraMenu.descargaIniciada = true;

        yield return BundleManager.InitializeOffline(mundoS);

        MovimientoCamaraMenu.descargaIniciada = false;

        if (BundleManager.scenesList.Count > 0)
        {
            popapLoading.SetActive(false);
        }
        else
        {
            botonAceptarLoading.SetActive(true);
            textoPopapLoading.text = "Error al acceder. Inténtelo más tarde.";
        }*/

        panelEventoAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        panelEventoArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
    }

}
