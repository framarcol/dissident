﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldSelectorMap : MonoBehaviour {

    public List<GameObject> worldIslands;

    public List<GameObject> worldMaps;
    int activeWorld = -1;

    public AudioClip miClip;
    public AudioSource miOrigenSonido;

    Collider worldSelectorCollider;

    int fingerId = -1;
#if UNITY_EDITOR
    public bool initClick;
#endif

    public float transitionDuration = 2f;

    public bool worldSelectorActive = true;
    
    void Start () {

        worldSelectorCollider = GetComponent<Collider>();

        InitWorldSelector();

    }

    void Update() {

        CheckSelectorTap();

    }

    public void InitWorldSelector() {

        gameObject.SetActive(true);

        //StartCoroutine(OpenWorldSelector());

    }

    public void EndWorldSelector() {

        StartCoroutine(TransitionToWorld(-1));

    }

    public void SelectWorld(int worldId) {

        if (worldIslands == null || worldIslands.Count < worldId) return;

        for (int i = 0; i < worldIslands.Count; i++) {

            worldIslands[i].GetComponent<Collider>().enabled = false;

        }

        worldSelectorCollider.enabled = false;

        StartCoroutine(TransitionToWorld(worldId));

        worldSelectorActive = false;

    }

    IEnumerator TransitionToWorld(int worldId) {

        float timer = 0f;
        while(timer < transitionDuration) {

            timer += Time.deltaTime;

            float perc = timer / transitionDuration;

            for(int i = 0; i < worldIslands.Count; i++) {

                worldIslands[i].transform.localScale = new Vector3(1 - perc, 1 - perc, 1 - perc);

            }

            yield return null;

        }

        for (int i = 0; i < worldIslands.Count; i++) {

            worldIslands[i].transform.localScale = new Vector3(1, 1, 1);
            worldIslands[i].SetActive(false);

        }

        if (worldId != -1) {

            worldMaps[worldId].SetActive(true);
            activeWorld = worldId;

        } else {

            gameObject.SetActive(false);

        }

        worldSelectorCollider.enabled = true;

    }

    public IEnumerator OpenWorldSelector() {

        worldSelectorActive = true;

        if (activeWorld != -1) yield return StartCoroutine(worldMaps[activeWorld].GetComponent<HologramMap>().DisableMap());

        yield return StartCoroutine(TransitionToWorldSelector());

        for (int i = 0; i < worldIslands.Count; i++) {

            worldIslands[i].GetComponent<Collider>().enabled = true;

        }

    }

    IEnumerator TransitionToWorldSelector() {

        for (int i = 0; i < worldIslands.Count; i++) {

            worldIslands[i].transform.localScale = new Vector3(0, 0, 0);
            worldIslands[i].SetActive(true);

        }

        float timer = 0f;
        while (timer < transitionDuration) {

            timer += Time.deltaTime;

            float perc = timer / transitionDuration;

            for (int i = 0; i < worldIslands.Count; i++) {

                worldIslands[i].transform.localScale = new Vector3(perc, perc, perc);

            }

            yield return null;

        }

        activeWorld = -1;
        
    }

    private void CheckSelectorTap() {

        Ray ray;
        RaycastHit rayHit;

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == worldSelectorCollider) {
                    
                    initClick = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit)) {

                if (rayHit.collider == worldSelectorCollider) {

                    if (initClick) {

                        //TODO INVOKE METHOD
                        miOrigenSonido.PlayOneShot(miClip);
                        if (!worldSelectorActive) StartCoroutine(OpenWorldSelector());

                    }

                    initClick = false;

                }

            } else {

                initClick = false;

            }

        }

#endif

        int touchCount = Input.touchCount;
        for (int i = 0; i < touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            switch (touch.phase) {

                case TouchPhase.Began:

                    if (fingerId != -1) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == worldSelectorCollider) {

                            fingerId = touch.fingerId;

                        }

                    }

                    break;

                case TouchPhase.Ended:

                    if (touch.fingerId != fingerId) break;

                    ray = Camera.main.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out rayHit)) {

                        if (rayHit.collider == worldSelectorCollider) {

                            //TODO Invoke TAP METHOD
                            miOrigenSonido.PlayOneShot(miClip);
                            if (!worldSelectorActive) StartCoroutine(OpenWorldSelector());

                            fingerId = -1;

                        }

                    }

                    break;

            }

        }

    }

}
