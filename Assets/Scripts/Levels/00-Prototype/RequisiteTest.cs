﻿using UnityEngine;
using System.Collections;

public class RequisiteTest : MonoBehaviour {

    public int timesPassed = 0;

    public void RequisiteTimesPassed(GameObject rotationArea) {

        timesPassed++;

        if (timesPassed >= 2) {

            rotationArea.SetActive(false);

        }

    }

}
