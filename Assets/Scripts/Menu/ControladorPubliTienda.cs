﻿using UnityEngine;
using System.Collections;

public class ControladorPubliTienda : MonoBehaviour {

    public Animator publiTienda;

	void OnEnable()
    {
#if UNITY_EDITOR
    //    PlayerPrefs.SetInt("PubliTiendaArmas", 0);
#endif
        if (PlayerPrefs.GetInt("PubliTiendaArmas") != 1)
        {
            PlayerPrefs.SetInt("PubliTiendaArmas", 1);
            publiTienda.SetTrigger("Fade");
        }
    }

    public void CerrarVentana()
    {
        publiTienda.SetTrigger("FadeOut");
    }

    public void AbrirVentana()
    {
        publiTienda.SetTrigger("Fade");
    }
}
