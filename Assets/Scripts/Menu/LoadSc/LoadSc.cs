﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSc : MonoBehaviour {
    
    public GameObject[] fondos;
    public GameObject[] dot;

    public int i;

    float tiempoActual = 0;
    float tiempoMax = 1;

    public static int worldSelected = -1;

    public static bool normalScene = false;
    public static int prevScene = -1;
    public static bool specialPrevLevelScene = false;
    public static int nextScene = -1;
    public static bool specialNextLevelScene = false;
    public static bool fromBundle = false;
    public static float ejeX;
    public static float ejeY;

	void Start () {

        if(GameDataController.initialized) GameDataController.Save();

        i = 1;

        if (nextScene == -1) {

            fondos[4].SetActive(true);
            SceneManager.LoadSceneAsync("MenuPrincipal");

        } else {

            if(worldSelected != -1)
            {
                fondos[worldSelected].SetActive(true);
            }
            else
            {
                fondos[4].SetActive(true);
            }


            //SceneManager.LoadSceneAsync(nextScene);
            if (worldSelected == 0)
            {
                normalScene = true;
            }
            else
            {
                normalScene = false;
            }
           


            if (normalScene) {
                SceneManager.LoadSceneAsync(nextScene);
                Debug.Log("NextSceneNormal: " + nextScene);
            } 

            else if (fromBundle) {

                if (normalScene || BundleManager.scenesList == null || BundleManager.scenesList.Count == 0) { 
                    SceneManager.LoadSceneAsync(nextScene);
                  //  Debug.Log("NextScene: " + nextScene);
                } else if (specialNextLevelScene) {
                    if (fromBundle) SceneManager.LoadSceneAsync(BundleManager.specialScenesList[nextScene]);
                } else {
                    if (fromBundle) SceneManager.LoadSceneAsync(BundleManager.scenesList[nextScene]);
                }

            } else {

                SceneManager.LoadSceneAsync(nextScene);

            }
                
            

            nextScene = -1;
            specialNextLevelScene = false;
            fromBundle = false;

        }

    }

    
	
	void Update () {

        tiempoActual+= Time.deltaTime;

        if(tiempoActual >= tiempoMax)
        {
            tiempoActual = 0;

            if (dot[dot.Length - 1].activeInHierarchy)
            {
                for (int j = 1; j < dot.Length; j++) dot[j].SetActive(false);
                i = 1;
            }
            else
            {
                    dot[i].SetActive(true);
                    i++;
            }

            
        }

    }
}
