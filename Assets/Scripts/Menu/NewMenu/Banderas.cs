﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Banderas : MonoBehaviour {

    public Toggle[] bandera;


	void Start () {

        SeleccionarBandera(PlayerPrefs.GetString(GameManager.LanguagePref));
    }

    void SeleccionarBandera(string idioma)
    {
        Debug.Log(idioma);
        switch (idioma)
        {
            
            case "Spanish":
                bandera[0].isOn = true;
                break;
            case "English":
                bandera[1].isOn = true;
                break;
            case "Portuguese":
                bandera[2].isOn = true;
                break;
            case "German":
                bandera[3].isOn = true;
                break;
            case "French":
                bandera[4].isOn = true;
                break;
            case "Japanese":
                bandera[7].isOn = true;
                break;
            case "Italian":
                bandera[8].isOn = true;
                break;
            case "ChineseTraditional":
                bandera[5].isOn = true;
                break;
            case "ChineseSimplified":
                bandera[6].isOn = true;
                break;
            default:
                bandera[1].isOn = true;
                break;
        }
    }

}
