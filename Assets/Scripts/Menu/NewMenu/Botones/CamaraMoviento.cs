﻿using UnityEngine;
using System.Collections;
using System;

public class CamaraMoviento : MonoBehaviour {


    public static Vector3 posicionObjetivo;
    public static float Velocidad;
    Vector3 VectorHaciaObjetivo;
    public static bool rotar;
    public static float timer;
    Vector3 origen;
    public static bool nuevoOrigen, nuevaRotacion;
    Quaternion origenRotacion;
    public static Quaternion rotacionObjetivo;
    public GameObject panelTienda;
    public GameObject panelIdolos;

    void Start()
    {

        float resultado = (float) Screen.width / (float)Screen.height;

   //     Debug.Log(Screen.width);
   //     Debug.Log(Screen.height);

   //     Debug.Log(resultado);

        if (resultado == 16/10f)
        {
            GetComponent<Camera>().fieldOfView = 47;
        }

        if(resultado == 4 / 3f)
        {
            GetComponent<Camera>().fieldOfView = 54.5f;
        }

        if (LoadSc.ejeX != 0 || LoadSc.ejeY != 0)
        {
            Vector3 vec = new Vector3(LoadSc.ejeX, LoadSc.ejeY, 0);
            this.transform.localPosition = vec;
            if (LoadSc.ejeY == -8.31f) panelTienda.gameObject.SetActive(true);
            if (LoadSc.ejeY == 10.26f) panelIdolos.gameObject.SetActive(true);
            LoadSc.ejeX = 0;
            LoadSc.ejeY = 0;
        }


        origen = this.transform.localPosition;
        origenRotacion = this.transform.localRotation;

    }


    void Update()
    {
        if (nuevoOrigen)
        {
        //    enMovimiento = true;
            IrHaciaHito();
        }

        if (rotar)
        {
           // enMovimiento = true;
            RotarHaciaHito();
        }
       
    }

    public void IrHaciaHito()
    {
        Vector3 destino = posicionObjetivo;

        Vector3 distancia = destino - origen;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if(timer < Velocidad)
        {
            transform.localPosition = distancia * porcentaje + origen;
        }
        else
        {          
            origen = this.transform.localPosition;
            nuevoOrigen = false;
         //   enMovimiento = false;
        }    

    }

    public void Rotar()
    {
        rotar = true;
    }
   
    public void RotarHaciaHito()
    {
        Quaternion miRota = rotacionObjetivo;   //rotacionObjetivo.transform.localRotation;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad)
        {
            transform.localRotation = Quaternion.Lerp(origenRotacion,miRota, porcentaje);
        }
        else
        {
            rotar = false;
          //  enMovimiento = false;
            origenRotacion = this.transform.localRotation;
        }
    }

}
