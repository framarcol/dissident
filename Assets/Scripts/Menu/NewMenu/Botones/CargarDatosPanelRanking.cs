﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CargarDatosPanelRanking : MonoBehaviour {

    public int numNivel;
    public Text textoNombre;
    public Text textoPuntos;
    public bool esRecarga;
    public Text textoMundo;

    void OnEnable()
    {
        if(RankingController.rankingCargado) Recargar();
    }

    public void CargarCorutinaPuntuacionNivel()
    {
         gameObject.GetComponentInChildren<Button>().interactable = false;
         StartCoroutine(CargarPuntuacionNivel());
    }

    public IEnumerator CargarPuntuacionNivel()
    {
        WWW itemsData2;
        WWWForm datos = new WWWForm();

        datos.AddField("num", numNivel);
        datos.AddField("mundo", WorldMap.worldRanking);

        itemsData2 = new WWW("https://agapornigames.com/dodongo/cargarPuntuacion2.php", datos);

        yield return itemsData2;    

        string[] resultado = itemsData2.text.Split(';');

        textoNombre.text = "";
        textoPuntos.text = "";

        for (int i = 0, j = 0; i < resultado.Length - 2; i += 2, j++)
        {
            textoNombre.text += resultado[i]+ "\n";
            textoPuntos.text += resultado[i + 1] + "\n";
        }

        yield return new WaitForSeconds(3);

        gameObject.GetComponentInChildren<Button>().interactable = true;
    }

    public IEnumerator CargarPuntuacionNivelAmigos()
    {
        yield return new WaitForSeconds(3);

        gameObject.GetComponentInChildren<Button>().interactable = true;
    }

    public void Recargar()
    {
     //   Debug.Log("Nivel: " + numNivel);
     //   Debug.Log("Mundo: " + WorldMap.worldRanking);
        switch (WorldMap.worldRanking)
        {
            case 0:
                textoMundo.text = "QUALDROOM - " + (numNivel + 1).ToString();
                break;
            case 1:
                textoMundo.text = "YLDOON - " + (numNivel + 1).ToString();
                break;
            case 2:
                textoMundo.text = "MIRÄKKÄ - " + (numNivel + 1).ToString();
                break;
        }

        textoNombre.text = "";
        textoPuntos.text = "";

        for (int i = 1; i < 15; i++)
        {
            textoNombre.text += RankingController.rankingNames[WorldMap.worldRanking ,numNivel, i] + "\n";
            textoPuntos.text += RankingController.rankingValues[WorldMap.worldRanking, numNivel, i] + "\n";
        }
    }


}
