﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadTextIdolo : MonoBehaviour {

    public int numIdolo;
    public int numMundo;
    public Text tituloIdolo;
    public Text textoIdolo;
  //  RaycastHit hitInfo;

    public void CargarTexto()
    {
        tituloIdolo.text = SeleccionaIdiomas.gameText[numMundo, numIdolo];
        textoIdolo.text = SeleccionaIdiomas.gameText[numMundo, numIdolo + 1];
    }


    /* void Update()
     {
         if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
         {
             hitInfo = new RaycastHit();

             bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
             if (hit)
             {

                 if (hitInfo.transform.gameObject.name == this.transform.gameObject.name) 
                 {
                  //   tituloIdolo.text = SeleccionaIdiomas.gameText[numMundo, numIdolo];
                 //    textoIdolo.text = SeleccionaIdiomas.gameText[numMundo, numIdolo+1];
                 }
             }
         }
     }
     */

}
