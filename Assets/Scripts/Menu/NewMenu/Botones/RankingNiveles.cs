﻿using UnityEngine;
using System.Collections;

public class RankingNiveles : MonoBehaviour {

    public GameObject rankingArriba;
    public GameObject mundo;

    bool fondoMostrado;

    void Update()
    {
        if(rankingArriba.transform.localPosition.y < -750)
        {
            mundo.SetActive(false);
        }

        if(!fondoMostrado && rankingArriba.transform.localPosition.y > -750)
        {
            mundo.SetActive(true);
            fondoMostrado = true;
        }
    }

    public void QuitarFondo()
    {
        fondoMostrado = false;
    }

	
}
