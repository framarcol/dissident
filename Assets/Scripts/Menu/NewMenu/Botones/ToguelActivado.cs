﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToguelActivado : MonoBehaviour {


    public void ToggleChanged(bool Value)
    {
        if (Value)
        {
            this.gameObject.GetComponentInChildren<Text>().color = new Color(1, 0.2f, 0, 1);
        }
        else
        {
            this.gameObject.GetComponentInChildren<Text>().color = new Color(0, 0, 0, 1);
        }
    }


}
