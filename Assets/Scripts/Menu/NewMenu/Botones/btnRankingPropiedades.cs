﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class btnRankingPropiedades : MonoBehaviour {

    public GameObject worldSelector;

	void Update () {

        if (!worldSelector.GetComponent<WorldSelectorMap>().worldSelectorActive && WorldMap.worldRanking != 3)
        {
            GetComponent<Button>().interactable = true;
        }
        else
        {
            GetComponent<Button>().interactable = false;
        }

	}
}
