﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class confBtnOpciones : MonoBehaviour {

    public GameObject objeto;

    public void ToggleChanged(bool Value)
    {
        if (Value)
        {
            this.gameObject.GetComponentInChildren<Text>().color = new Color(0.047f, 0.61f, 0.95f, 1);
            objeto.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);
            objeto.gameObject.SetActive(false);
        }
    }
}
