﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnMundo : MonoBehaviour {

    public Animator fade;
    public Image fadeColor;
    public GameObject subMenu, hangar;
    public Animator puerta, bolaMundo;
    public Transform puertaPos;
    bool fadeHecho;

    void Update()
    {
        if (puertaPos.localPosition.z > 290 && puertaPos.localPosition.z < 300) HacerFade();

        if (fadeColor.color.a > 0.95f && fadeHecho)
        { 
            hangar.SetActive(false);
            subMenu.SetActive(true);
            fadeHecho = false;
        }
    }

    void HacerFade()
    {
        fade.SetTrigger("fade");
        fadeHecho = true;
    }


    void OnMouseDown()
    {
        puerta.SetTrigger("Abrir");
        bolaMundo.SetTrigger("Girar");

    }
}
