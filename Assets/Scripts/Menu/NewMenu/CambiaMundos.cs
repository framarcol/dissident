﻿using UnityEngine;
using System.Collections;

public class CambiaMundos : MonoBehaviour {

    public GameObject[] islotes;
    float distancia, ejeZ, escala;
    int mundoSeleccionado;
    float[] timer;
    int Velocidad;
    Vector3[] posicionObjetivo, origen, escalaOrigen, escalaDestino;
  //  public GameObject mapaObjetos;
    bool nuevoOrigen;

	void Start () {

        posicionObjetivo = new Vector3[10];
        origen = new Vector3[10];
        timer = new float[10];
        escalaOrigen = new Vector3[10];
        escalaDestino = new Vector3[10];

        nuevoOrigen = true;
        Velocidad = 1;
        distancia = -1.639f;
        ejeZ = 0.236f;
        escala = 0;


        if (GameDataController.gameData.specialLevelsCompleted[5])
        {
            mundoSeleccionado = 1;
        }
        else if(GameDataController.gameData.stars[0, 14] > 0)
        {
            mundoSeleccionado = 2;
        }
        else if(GameDataController.gameData.stars[1,14] > 0)
        {
            mundoSeleccionado = 3;
        }
        else
        {
            mundoSeleccionado = 0;
        }

        if(LoadSc.worldSelected != -1)
        {
            mundoSeleccionado = LoadSc.worldSelected + 1;
        }

        ControladorActivacionMundos();

        CambiaPosiciones(mundoSeleccionado);


    }

    public void ControladorActivacionMundos()
    {

        for(int i = mundoSeleccionado -1; i <= mundoSeleccionado +1; i++)
        {
            if(i < islotes.Length && i > -1)
            islotes[i].SetActive(true);
        }
    }

    public void ControladorDesactivacionMundo()
    {
        for (int i = mundoSeleccionado + 2; i < islotes.Length; i++)
        {
            islotes[i].SetActive(false);
        }

        for (int i = 0; i <= mundoSeleccionado - 2; i++)
        {
            islotes[i].SetActive(false);
        }

    }

    public void BtnDerecha()
    {
        if (GetComponentInParent<WorldSelectorMap>().worldSelectorActive)
        {
            if (islotes.Length > mundoSeleccionado + 1)
            {
                mundoSeleccionado++;
                ControladorActivacionMundos();
                CambiaPosiciones(mundoSeleccionado);
            }
        }
        
          
    }

    public void BtnIzquierda()
    {
        if (GetComponentInParent<WorldSelectorMap>().worldSelectorActive)
        {
            if (mundoSeleccionado > 0)
            {
                mundoSeleccionado--;
                ControladorActivacionMundos();
                CambiaPosiciones(mundoSeleccionado);
            }
        }
    }

    void CambiaPosiciones(int mundo)
    {
        for (int i = 0; i < islotes.Length; i++)
        {
            if(i == mundoSeleccionado)
            {
                escala = 1;
                ejeZ = 0;
            }
            else
            {
                escala = 0.6f;
                ejeZ = 0.236f;
            }

            origen[i] = islotes[i].transform.localPosition;
            posicionObjetivo[i] = new Vector3((mundoSeleccionado-i)*-1.639f, 0, ejeZ);
            escalaOrigen[i] = islotes[i].transform.localScale;
            escalaDestino[i] = Vector3.one * escala;

            timer[i] = 0;

        }
        nuevoOrigen = true;
    }

    void Update()
    {
        if (nuevoOrigen) {

            for (int i = 0; i < islotes.Length; i++) IrHaciaHito(i);
        }
    }

    public void IrHaciaHito(int islote)
    {
        Vector3 destino = posicionObjetivo[islote];

        Vector3 distancia = destino - origen[islote];

        Vector3 destino2 = escalaDestino[islote];

        Vector3 distancia2 = destino2 - escalaOrigen[islote];

        timer[islote] += Time.deltaTime;

        float porcentaje = timer[islote] / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer[islote] < Velocidad)
        {
            islotes[islote].transform.localPosition = distancia * porcentaje + origen[islote];
            islotes[islote].transform.localScale = distancia2 * porcentaje + escalaOrigen[islote];
        }
        else
        {
            ControladorDesactivacionMundo();
            nuevoOrigen = false;
        }

    }




}
