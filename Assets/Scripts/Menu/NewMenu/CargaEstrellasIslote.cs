﻿using UnityEngine;
using System.Collections;

public class CargaEstrellasIslote : MonoBehaviour {

    public MeshRenderer numI, numD;
    public GameObject numIGO, numDGO, barGO, starGO, numIGO2, numDGO2;
    public int numMundo;
    public Texture[] numeros;
    public GameObject superPremio;
	
	void Start () {

        CargaMedallas();
	}

    public void CargaMedallas()
    {
        if (numMundo == -1)
        {
            int estrellas = 0;
            for (int i = 0; i < 6; i++)
            {
                if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;
            }

            if (estrellas >= ControladorId.nivelesTutorial)
            {
                numIGO.SetActive(false);
                barGO.SetActive(false);
                starGO.SetActive(false);
                numDGO.SetActive(false);
                numIGO2.SetActive(false);
                numDGO2.SetActive(false);
                superPremio.SetActive(true);
            }
            else
            {
                numIGO.SetActive(true);
                barGO.SetActive(true);
                starGO.SetActive(true);
                numDGO.SetActive(true);
                numIGO2.SetActive(true);
                numDGO2.SetActive(true);
                superPremio.SetActive(false);

                numI.sharedMaterial.mainTexture = numeros[estrellas];
            }


        }
        else
        {
            int estrellas = 0;
            for (int i = 0; i < 15; i++)
            {
                estrellas += GameDataController.gameData.stars[numMundo, i];
            }

            if (estrellas == 45)
            {

                numIGO.SetActive(false);
                barGO.SetActive(false);
                starGO.SetActive(false);
                numDGO.SetActive(false);
                numDGO2.SetActive(false);
                numIGO2.SetActive(false);
                superPremio.SetActive(true);

            }
            else
            {
                numIGO.SetActive(true);
                barGO.SetActive(true);
                starGO.SetActive(true);
                numDGO.SetActive(true);
                numDGO2.SetActive(true);
                numIGO2.SetActive(true);
                superPremio.SetActive(false);

                int numIF = estrellas / 10;
                int numDF = estrellas % 10;

                if (numIF != 0)
                {
                    numIGO.SetActive(true);
                    numI.sharedMaterial.mainTexture = numeros[numIF];
                }
                else numIGO.SetActive(false);

                numD.sharedMaterial.mainTexture = numeros[numDF];
            }


        }

    }


}
