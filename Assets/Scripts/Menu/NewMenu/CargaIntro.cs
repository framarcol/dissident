﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
#if UNITY_ANDROID
//using GooglePlayGames;
#endif
//using UnityEngine.SocialPlatforms.GameCenter;
using System.IO;
using UnityEngine.UI;
//using Facebook.Unity;

public class CargaIntro : MonoBehaviour {

    public GameObject[] dot;

    public int i;

    float tiempoActual = 0;
    float tiempoMax = 1;

    string savePath = "/SkatePunk/skatePunk.sav";

    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789"; //add the characters you want

    public Text textoPopap;
    public Animator popap;

    public static bool modoOffline;

    void Start()
    {
      //  GameManager.Initialize();

      /*  if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() => {
                FB.ActivateApp();
            });
        }*/

        //        StartCoroutine(checkInternetConnection2());
        // textoPopap.text = SeleccionaIdiomas.gameText[20, 7];
        IniciarJuego();
    }

    void IniciarJuego () {
        modoOffline = true;

        LoadSc.nextScene = 2;
        SistemaPublicitarioGamePlay.esIntro = true;
        SceneManager.LoadScene("LoadScreen");
/*
#if UNITY_EDITOR
        GameDataController.gameData.specialLevelsCompleted[1] = false;
#endif

        if (!GameDataController.gameData.specialLevelsCompleted[1])
        {
            LoadSc.nextScene = 4;
            SceneManager.LoadScene("LoadScreen");
        }
        else
        {
            LoadSc.nextScene = 2;
            SistemaPublicitarioGamePlay.esIntro = true;
            SceneManager.LoadScene("LoadScreen");
        }  
        */
    }
    void Iniciar()
    {
        SceneManager.LoadScene(2);
    }

    IEnumerator checkInternetConnection2()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            if (GameDataController.gameData.premiumState)
            {
                modoOffline = true;
                Iniciar();
            }
            else
            {
                popap.SetTrigger("Fade");
                textoPopap.text = SeleccionaIdiomas.gameText[21, 18];
            }

        }
        else
        {
            modoOffline = false;
            IniciarJuego();
            /*    WWW itemsData4;

                itemsData4 = new WWW("https://agapornigames.com/dodongo/checkGameVersion.php");

                yield return itemsData4;

                int versionBD = int.Parse(itemsData4.text);

                string[] nums = Application.version.Split('.');

                int numFinal = 0;
                for (int i = 0; i < nums.Length; i++)
                {
                    numFinal = (numFinal * 10) + int.Parse(nums[i]);
                }

                if (numFinal < versionBD)
                {
                    popap.SetTrigger("Fade");
                    textoPopap.text = SeleccionaIdiomas.gameText[22, 12];
                }
                else
                {
                    IniciarJuego();
                }*/

        }
    }

    public void Salir()
    {
        Application.Quit();
    }

    void Update()
    {

        tiempoActual += Time.deltaTime;

        if (tiempoActual >= tiempoMax)
        {
            tiempoActual = 0;

            if (dot[dot.Length - 1].activeInHierarchy)
            {
                for (int j = 0; j < dot.Length; j++) dot[j].SetActive(false);
                i = 0;
            }
            else
            {
                dot[i].SetActive(true);
                i++;
            }


        }

    }
}
