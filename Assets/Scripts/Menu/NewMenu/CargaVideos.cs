﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CargaVideos : MonoBehaviour {

    public GameObject logo;
    public GameObject reproductor;
    public GameObject reproLogo;
    public GameObject intro;
    public bool saltavideo;
    public Text subtitulos;
    RaycastHit hitInfo;

    void Start()
    {
        saltavideo = false;
        //      GameManager.Initialize();
        //      Handheld.PlayFullScreenMovie("Logo.mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);

       // Screen.sleepTimeout = SleepTimeout.NeverSleep;
        

       // reproLogo.GetComponent<UniversalMediaPlayer>().Play();

        //   Handheld.PlayFullScreenMovie("Intro.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);

        /*  if(!GameDataController.gameData.specialLevelsCompleted[1])
           {
               SistemaPublicitarioGamePlay.esIntro = true;
               LoadSc.worldSelected = 0;
               LoadSc.nextScene = 4;
               SceneManager.LoadScene("LoadScreen");
           }
           else
           {*/
        //     SistemaPublicitarioGamePlay.esIntro = true;
        //  LoadSc.nextScene = -1;
        //      SceneManager.LoadScene("LoadScreen");
        // }

        comprobarVideos();
    }

    void comprobarVideos()
    {
        StartCoroutine(comprobarVideo());
    }

    public IEnumerator comprobarVideo()
    {
        yield return new WaitForSeconds(2f);
        if (!saltavideo)
        {
            SistemaPublicitarioGamePlay.esIntro = true;
            LoadSc.nextScene = -1;
            SceneManager.LoadScene("LoadScreen");
        }
    }

    public void IrAEscena()
    {
        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.nextScene = -1;
        SceneManager.LoadScene("LoadScreen");
    }

    public void ActivarEscena()
    {
        saltavideo = true;
        logo.gameObject.SetActive(true);
    }

    public void ActivarIntro()
    {
      //  reproductor.GetComponent<UniversalMediaPlayer>().Play();
    }

    public void ActivarObjetoIntro()
    {
        logo.SetActive(false);
        intro.SetActive(true);
    }

 /*   void Update()
    {
        int num = 1;
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit) if (hitInfo.transform.gameObject.tag == "HUD") IrAEscena();
        }
        // Debug.Log(reproductor.GetComponent<UniversalMediaPlayer>().Position);
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.08f) {
            subtitulos.text = SeleccionaIdiomas.gameText[29, num];
            num++;
        } 

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.095f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.109f) subtitulos.text = SeleccionaIdiomas.gameText[29, num];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.142f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.152f) subtitulos.text = SeleccionaIdiomas.gameText[29, 3];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.198f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.22f) subtitulos.text = SeleccionaIdiomas.gameText[29, 4];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.29f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.30f) subtitulos.text = SeleccionaIdiomas.gameText[29, 5];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.34f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.39f) subtitulos.text = SeleccionaIdiomas.gameText[29, 6];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.43f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.46f) subtitulos.text = SeleccionaIdiomas.gameText[29, 7];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.49f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.53f) subtitulos.text = SeleccionaIdiomas.gameText[29, 8];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.56f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.65f) subtitulos.text = SeleccionaIdiomas.gameText[29, 9];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.68f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.70f) subtitulos.text = SeleccionaIdiomas.gameText[29, 10];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.72f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.735f) subtitulos.text = SeleccionaIdiomas.gameText[29, 11];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.76f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.77f) subtitulos.text = SeleccionaIdiomas.gameText[29, 12];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.80f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.82f) subtitulos.text = SeleccionaIdiomas.gameText[29, 13];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.87f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.88f) subtitulos.text = SeleccionaIdiomas.gameText[29, 14];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.90f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.91f) subtitulos.text = SeleccionaIdiomas.gameText[29, 15];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.92f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.92f) subtitulos.text = SeleccionaIdiomas.gameText[29, 16];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.93f) subtitulos.text = "";
        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.93f) subtitulos.text = SeleccionaIdiomas.gameText[29, 17];

        if (reproductor.GetComponent<UniversalMediaPlayer>().Position > 0.94f) subtitulos.text = "";



    }*/

   
}
