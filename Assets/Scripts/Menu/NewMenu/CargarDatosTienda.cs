﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CargarDatosTienda : MonoBehaviour {


    public Text textoDiamantes;
    public Text textoEstrellas;
    int estrellas = 0;

    // Use this for initialization
    void OnEnable () {

        estrellas = 0;

        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++) estrellas += GameDataController.gameData.stars[j, i];

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;
	}

    void Update()
    {
        textoEstrellas.text = estrellas.ToString();
        textoDiamantes.text = GameDataController.gameData.diamondCurrency.ToString();
    }



}
