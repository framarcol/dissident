﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CargarNiveles : MonoBehaviour
{

	public MeshRenderer[] nivel1, nivel2, nivel3;
	public Material[] medallasMat;
	public GameObject[] EventosEspeciales;
	public Material[] medallasEspecial;

	public static bool[,] nivelComprado;
	public GameObject desierto, nieve;

	//Sprites Idolos Niveles
	public Sprite[] nivel1S, nivel2S, nivel3S;
	public Image[] nivelSeleccionadoS;
	int[] numeros;
	public Sprite[] simbolosMundos;
	public Image simboloMundo;

	bool esTutorial;
	int numEvento;
	int numEscenaCiudad;

	//Llamada txtPruebas
	public Text txtPrue;

	//Cargar logros
	public GameObject[] logrosLogrados;
	// public GameObject playTutorial, playNiveles;

	//Cargar textos
	public Text txtLogro2, txtLogro3, txtIdolo2, txtIdolo3, txtMiPuntuacion;

	//Cargar Popap
	public Animator popap;
	public Text txtPp;

	//Elementos del panel de niveles
	public Text textoMundoMasNivel;
	public Image[] colorIdolo;
	public GameObject panelNivelArriba, panelNivelAbajo;
	public GameObject panelEventoArriba, panelEventoAbajo;

	//Elementos del tutorial
	public GameObject estrellaTutorial;
	public MeshRenderer[] medallasTutorial;
	public Material[] medTut;
	public Text textoNivelTutorial;
	// public Text puntosTutorial;
	public GameObject panelTutorialArriba, panelTutorialAbajo;
	public Text textoEvento, nombreEvento;

	public Image imgPistolaEvento;
	public Image imgPropiedad, imgDescPropiedad;
	public Text textxtPropiedad;
	public Sprite[] propiedades;
	public GameObject[] textosVersiones;
	public Sprite[] imgMundosSimbolo;
	public Image imgSimbolo;
	public Text txtNombreArma;
	public GameObject starOff, starOn;
    public GameObject controladorMenu;


	int nivelActual, i2, world2;


	StarsRequisites starsRequisites;

	void Start()
	{
        GameDataController.gameData.premiumState = true;
		numeros = new int[12];
		nivelActual = 0;
		esTutorial = false;
		nivelComprado = new bool[100, 100];

		starsRequisites = Resources.Load("DataObjects/StarsRequisites", typeof(StarsRequisites)) as StarsRequisites;

		CargaMedallero();

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 15; j++)
			{
				if (GameDataController.gameData.stars[i, j] > 0) nivelComprado[i, j + 1] = true;
				else nivelComprado[i, j + 1] = false;
			}
		}

	//	if (GameDataController.gameData.stars[0, 0] == 0) nivel1[0].material = medallasMat[7];
	//	if (GameDataController.gameData.stars[1, 0] == 0) nivel2[0].material = medallasMat[7];
	//	if (GameDataController.gameData.stars[2, 0] == 0) nivel3[0].material = medallasMat[7];
		nivelComprado[1, 0] = true;
		nivelComprado[2, 0] = true;
		//  if (!CargaIntro.modoOffline) ComprobadorPuntuaciones();

	}

	public void CargaMedallero()
	{
		CargaMedallas(nivel1, 0);
		CargaMedallas(nivel2, 1);
		CargaMedallas(nivel3, 2);
		CargaMedallasTutorial();
		CargarMedallasNivelesEspeciales();

	}

	public void CargaMedallas(MeshRenderer[] cargaNivel, int mundo)
	{
		for (int i = 0; i < 15; i++)
		{
            if (i != 0) cargaNivel[i].material = medallasMat[GameDataController.gameData.stars[mundo, i] + 1];
            else {
                if (GameDataController.gameData.stars[0, mundo] == 0) cargaNivel[i].material = medallasMat[7];
                else cargaNivel[i].material = medallasMat[GameDataController.gameData.stars[mundo, i] + 1];
            } 
			if (i != 14)
			{
				if (GameDataController.gameData.stars[mundo, i + 1] == 0 && GameDataController.gameData.stars[mundo, i] > 0)
				{
					cargaNivel[i + 1].material = medallasMat[7];
					i++;
				}
			}
		}

	}

	public void CargaMedallasTutorial()
	{
		for (int i = 0; i < medallasTutorial.Length; i++)
		{
			if (GameDataController.gameData.specialLevelsCompleted[i + 1]) medallasTutorial[i].material = medTut[1];
			else medallasTutorial[i].material = medTut[0];

			if (i != 4)
			{
				if (!GameDataController.gameData.specialLevelsCompleted[i + 2] && GameDataController.gameData.specialLevelsCompleted[i + 1])
				{
					medallasTutorial[i + 1].material = medTut[2];
					i++;
				}
			}
		}
		if (!GameDataController.gameData.specialLevelsCompleted[1]) medallasTutorial[0].material = medTut[2];
		else medallasTutorial[0].material = medTut[1];
	}

	public void SelecionarMundo(int mundo)
	{
		world2 = mundo;
	}

	public void CargarDatosNivelUno()
	{
		//playNiveles.SetActive(true);
		//  LoadSc.specialNextLevelScene = false;
		nivelActual = 0;
		world2 = 0;

		//  Cargar();
#if UNITY_EDITOR
		GameDataController.gameData.premiumState = true;
        GameDataController.gameData.specialLevelsCompleted[4] = true;
#endif
		if (GameDataController.gameData.specialLevelsCompleted[ControladorId.nivelesTutorial] || GameDataController.gameData.premiumState) Cargar();
		else
		{
			if (GameDataController.gameData.specialLevelsCompleted[ControladorId.nivelesTutorial]) txtPp.text = SeleccionaIdiomas.gameText[3, 1];
			else txtPp.text = SeleccionaIdiomas.gameText[3, 2];
			popap.SetTrigger("Fade");
		}
    }

    public void CargarDatosNivel(int numNivel)
    {
      //  playNiveles.SetActive(true);
        nivelActual = numNivel -1;
        //  LoadSc.specialNextLevelScene = false;
   //     Debug.Log(world2 + "/" + nivelActual);
        if ((nivelComprado[world2,nivelActual] && GameDataController.gameData.specialLevelsCompleted[ControladorId.nivelesTutorial]) || GameDataController.gameData.premiumState) Cargar();
        else
        {
            
			if (GameDataController.gameData.specialLevelsCompleted[ControladorId.nivelesTutorial])txtPp.text = SeleccionaIdiomas.gameText[3, 1];
			else txtPp.text = SeleccionaIdiomas.gameText[3, 2];
            popap.SetTrigger("Fade");

        }

    }

    public void CargarDatosTutorialUno()
    {
     //   playTutorial.SetActive(true);
     //   LoadSc.specialNextLevelScene = true;
        nivelActual = 0;
        world2 = 0;
        CargarTutorial();
    }

    public void CargarDatosTutorial(int numNivel)
    {
      //  playTutorial.SetActive(true);
        nivelActual = numNivel - 1;
       // Debug.Log(nivelActual);
        world2 = 0;
    //    LoadSc.specialNextLevelScene = true;
        if (GameDataController.gameData.specialLevelsCompleted[nivelActual] || GameDataController.gameData.premiumState) CargarTutorial();
        else
        {
            txtPp.text = SeleccionaIdiomas.gameText[3, 1];
            popap.SetTrigger("Fade");
        }

    }

    public void Cargar()
    {
        switch (world2)
        {
            case 0:
                textoMundoMasNivel.text = "QUALDROM - " + (nivelActual + 1).ToString();
                break;
            case 1:
                textoMundoMasNivel.text = "YLDOON - " + (nivelActual + 1).ToString();
                break;
            case 2:
                textoMundoMasNivel.text = "MYRAKKA - " + (nivelActual + 1).ToString();
                break;
            case 3:
                textoMundoMasNivel.text = "WULUSSA - " + (nivelActual + 1).ToString();
                break;
        }

        txtLogro2.text = starsRequisites.worldStarsRequisites[world2].levelRequisites[nivelActual].twoStarsRequisite.idols.ToString();
        txtLogro3.text = starsRequisites.worldStarsRequisites[world2].levelRequisites[nivelActual].threeStarsRequisite.idols.ToString();
        txtIdolo2.text = starsRequisites.worldStarsRequisites[world2].levelRequisites[nivelActual].twoStarsRequisite.score.ToString();
        txtIdolo3.text = starsRequisites.worldStarsRequisites[world2].levelRequisites[nivelActual].threeStarsRequisite.score.ToString();

        for (int i = 0; i < logrosLogrados.Length; i++) logrosLogrados[i].gameObject.SetActive(false);

        int numeroEstrellas = GameDataController.gameData.stars[world2, nivelActual];

        switch (numeroEstrellas)
        {
            case 0:
                logrosLogrados[0].SetActive(true);
                logrosLogrados[1].SetActive(true);
                logrosLogrados[2].SetActive(true);
                break;
            case 1:
                logrosLogrados[3].SetActive(true);
                logrosLogrados[1].SetActive(true);
                logrosLogrados[2].SetActive(true);
                break;
            case 2:
                logrosLogrados[3].SetActive(true);
                logrosLogrados[4].SetActive(true);
                logrosLogrados[2].SetActive(true);
                break;
            case 3:
                logrosLogrados[3].SetActive(true);
                logrosLogrados[4].SetActive(true);
                logrosLogrados[5].SetActive(true);
                break;
        }

        txtMiPuntuacion.text = GameDataController.gameData.playerRecords[world2, nivelActual].ToString();

        //Bajar Panel
        panelNivelArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        panelNivelAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        //  LoadSc.specialNextLevelScene = false;
        esTutorial = false;
    }

    public void CargarTutorial()
    {
        textoNivelTutorial.text = "RUNAWAY - " + (nivelActual + 1).ToString();

        estrellaTutorial.SetActive(false);

        if (GameDataController.gameData.specialLevelsCompleted[nivelActual+1]) estrellaTutorial.SetActive(true);

        panelTutorialArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        panelTutorialAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        esTutorial = true;
        //    puntosTutorial.text = GameDataController.gameData.playerRecords[world2, nivelActual].ToString();
    }

    public void CargaIdolosNivel(string uno)
    {
        string[] resultado = uno.Split(';');
        for (int i = 0; i < numeros.Length; i++) numeros[i] = int.Parse(resultado[i]) - 1;
        
        CargaIdolos();
    }

    public void CargaIdolos()
    {
        for(int i = 0, j = 0; i < numeros.Length; i+=2, j++)
        {
            if (GameDataController.gameData.idols[world2, numeros[i], numeros[i+1]])
            {
                nivelSeleccionadoS[j].color = new Color(1, 1, 1, 1);
                colorIdolo[j].color = new Color(1, 1, 1, 1);
                if(world2 == 0)nivelSeleccionadoS[j].sprite = nivel1S[numeros[i]];
                else if(world2 == 1)nivelSeleccionadoS[j].sprite = nivel2S[numeros[i]];
                else nivelSeleccionadoS[j].sprite = nivel3S[numeros[i]];

            }
            else
            {
                colorIdolo[j].color = new Color(1, 1, 1, 0.5f);
                nivelSeleccionadoS[j].color = new Color(1, 1, 1, 0.5f);
                if(world2 == 0)nivelSeleccionadoS[j].sprite = nivel1S[numeros[i]];
                else if(world2 == 1) nivelSeleccionadoS[j].sprite = nivel2S[numeros[i]];
                else nivelSeleccionadoS[j].sprite = nivel3S[numeros[i]];
            }
            
        }

        simboloMundo.sprite = simbolosMundos[world2];
    }

    public void CargarMedallasNivelesEspeciales()
    {

        for (int i = 0; i < EventosEspeciales.Length; i++)
        {
            int numEvento = int.Parse(EventosEspeciales[i].name);
            int medalla = starsRequisites.GetSpecialLevelRequisites(numEvento).diamondReward;

            if (GameDataController.gameData.specialLevelsCompleted[numEvento])
                EventosEspeciales[i].GetComponentInChildren<MeshRenderer>().sharedMaterial = medallasEspecial[0];
            else
            EventosEspeciales[i].GetComponentInChildren<MeshRenderer>().sharedMaterial = medallasEspecial[medalla];  
        }   
                           
    }

	public void BtnStartGame()
	{

        if (CargaIntro.modoOffline || GameDataController.gameData.premiumState)
        {
            EmpezarNivel();
        }
        else BtnStartGameCheck();
     

    }

    void BtnStartGameCheck()
    {
        EmpezarNivel();
       /* WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null || esTutorial)
        {
            
        }
        else
        {
            txtPp.text = SeleccionaIdiomas.gameText[20, 8];
            popap.SetTrigger("Fade");
        }*/
    }

    void EmpezarNivel()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        int worldSeleccionado;

        if (world2 == 0 || soyApple)
        {
            worldSeleccionado = world2;
            LoadSc.normalScene = true;
            if (world2 == 0)
            {            
                if (esTutorial) nivelActual += 4;
                else nivelActual += 9;

            }
            else if(world2 == 1)
            {
                nivelActual += 34;

            }
            else if(world2 == 2)
            {
                nivelActual += 57;
            }
            
            if (soyApple)
            {
                SistemaPublicitarioGamePlay.esIntro = true;
                LoadSc.worldSelected = world2;
                LoadSc.nextScene = nivelActual;
                SceneManager.LoadScene("LoadScreen");
                return;
            }
        }
        else
        {
            worldSeleccionado = world2 - 1;
            LoadSc.normalScene = false;
        }

        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.worldSelected = world2;

      //  if (world2 == 1 || world2 == 2) LoadSc.fromBundle = true;

      //  string mundoF = "";

      //  if (world2 == 1) mundoF = "desert";
      //  if (world2 == 2) mundoF = "snow";

        LoadSc.nextScene = (15 * worldSeleccionado) + nivelActual;

        /* if (world2 == 0)
         {
             LoadSc.nextScene = (15 * worldSeleccionado) + nivelActual;
         }

         else
         {
             nivelActual++;
             if(nivelActual < 10) LoadSc.nextScene = BundleManager.scenesList.IndexOf(mundoF + "0" + nivelActual);
             else LoadSc.nextScene = BundleManager.scenesList.IndexOf(mundoF + nivelActual);
         }*/

        SceneManager.LoadScene("LoadScreen");
    }

    public void PlayLevelSpecial(int level)
    {
        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.worldSelected = -1;
        LoadSc.nextScene = level;
        SceneManager.LoadScene("LoadScreen");
    }

    public void PlayteaserDesierto()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.worldSelected = 1;
        if (soyApple)
        {
            LoadSc.nextScene = 56;
        }
        else
        {              
            LoadSc.fromBundle = true;
            LoadSc.specialNextLevelScene = true;
            LoadSc.nextScene = BundleManager.specialScenesList.IndexOf("desertteaser_special_30");           
        }

        SceneManager.LoadScene("LoadScreen");

    }

    public void PlayteaserSnow()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.worldSelected = 2;

        if (soyApple)
        {
            LoadSc.nextScene = 79;
        }
        else
        {           
            LoadSc.fromBundle = true;
            LoadSc.specialNextLevelScene = true;
            LoadSc.nextScene = BundleManager.specialScenesList.IndexOf("snowteaser_special_50");         
        }

        SceneManager.LoadScene("LoadScreen");
    }

    public void numEscena(int numEscena)
    {
        numEscenaCiudad = numEscena;
    }

    public void PlayEvento()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        SistemaPublicitarioGamePlay.esIntro = true;
        LoadSc.worldSelected = world2;

        if (!soyApple)
        {
            if (world2 != 0) LoadSc.fromBundle = true;
            if (world2 != 0) LoadSc.specialNextLevelScene = true;

            switch (world2)
            {
                case 0:
                    LoadSc.nextScene = numEscenaCiudad;
                    break;
                case 1:
                    LoadSc.nextScene = BundleManager.specialScenesList.IndexOf("desert_special_" + numEvento);
                    break;
                case 2:
                    LoadSc.nextScene = BundleManager.specialScenesList.IndexOf("snow_special_" + numEvento);
                    break;
            }
        }
        else
        {
            LoadSc.nextScene = numEscenaCiudad;
        }

        SceneManager.LoadScene("LoadScreen");
    }

    public void CargarEventoEspecial(int num)
    {
        numEvento = num;
        if (ShopManager.shopData != null)
        {
            nombreEvento.text = starsRequisites.GetSpecialLevelRequisites(num).levelName;         
            int puntos = starsRequisites.GetSpecialLevelRequisites(num).score;
            int idolos = starsRequisites.GetSpecialLevelRequisites(num).idols;

            CargarElementos(num);

            if (puntos == 0 && idolos == 0) textoEvento.text = SeleccionaIdiomas.gameText[9, 7];
            else if (puntos == 0) textoEvento.text = SeleccionaIdiomas.gameText[9, 8] + ": " + starsRequisites.GetSpecialLevelRequisites(num).idols;
            else textoEvento.text = SeleccionaIdiomas.gameText[9, 5] + ": " + starsRequisites.GetSpecialLevelRequisites(num).score;



            panelEventoAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
            panelEventoArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        }
        else
        {          
            StartCoroutine(EsperaCargaTienda(0));
        }

    }

    public IEnumerator EsperaCargaTienda(int opcion)
    {
        yield return controladorMenu.GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda(false);
        while (ShopManager.shopData == null)
        {
            yield return null;
        }

        switch (opcion)
        {
            case 0:
                CargarEventoEspecial(numEvento);
                break;
            case 1:
                CargarEventoDelDia(numEvento);
                break;
        }
       
    }

    public void AbrirPestanas()
    {

        panelEventoAbajo.GetComponent<MovimientoPaneles>().BtnSacarPanel();
        panelEventoArriba.GetComponent<MovimientoPaneles>().BtnSacarPanel();
    }

    public  void CargarElementos(int idPistol)
    {
        int idPistola = starsRequisites.GetSpecialLevelRequisites(idPistol).weaponId;
        imgPistolaEvento.sprite = ShopManager.shopData.GetWeaponSkin(idPistola).itemThumbnail;

        if (GameDataController.gameData.specialLevelsCompleted[idPistol])
        {
            starOff.SetActive(false);
            starOn.SetActive(true);
        }
        else
        {
            starOff.SetActive(true);
            starOn.SetActive(false);
        }

        imgSimbolo.sprite = imgMundosSimbolo[world2];
        txtNombreArma.text = ShopManager.shopData.GetWeaponSkin(idPistola).itemName.ToString();

        string cadena = ShopManager.shopData.GetWeaponSkin(idPistola).itemDescription.ToString();

        switch (cadena.ToUpper())
        {
            case "AMMO":
                for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                textosVersiones[0].SetActive(true);
                textosVersiones[1].SetActive(true);
                textosVersiones[0].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().effectProbability * 100).ToString() + "%";
                textosVersiones[1].GetComponent<Text>().text = "+" + ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().ammoRefill.GetComponent<AmmoRefill>().ammo.ToString();
                imgPropiedad.sprite = imgDescPropiedad.sprite = propiedades[0];
                textxtPropiedad.text = SeleccionaIdiomas.gameText[30, 4];
                break;
            case "IDOL":
                for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                textosVersiones[2].SetActive(true);
                textosVersiones[2].GetComponent<Text>().text = "x " + ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<IdolCatcherProjectile>().startingWeaponUses.ToString();
                imgPropiedad.sprite = imgDescPropiedad.sprite = propiedades[1];
                textxtPropiedad.text = SeleccionaIdiomas.gameText[30, 1];
                break;
            case "SCORE":
                for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                textosVersiones[3].SetActive(true);
                textosVersiones[4].SetActive(true);
                textosVersiones[3].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<ScoreBoosterProjectile>().effectProbability * 100).ToString() + "%";
                textosVersiones[4].GetComponent<Text>().text = "x " + ((ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<ScoreBoosterProjectile>().scoreBoost) + 1).ToString();
                imgPropiedad.sprite = imgDescPropiedad.sprite = propiedades[2];
                textxtPropiedad.text = SeleccionaIdiomas.gameText[30, 5];
                break;
            case "HEAL":
                for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                textosVersiones[5].SetActive(true);
                textosVersiones[5].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().effectProbability * 100).ToString() + "%";
                imgPropiedad.sprite = imgDescPropiedad.sprite = propiedades[3];
                textxtPropiedad.text = SeleccionaIdiomas.gameText[30, 3];
                break;
            case "RICOCHET":
                for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                textosVersiones[6].SetActive(true);
                textosVersiones[6].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(idPistola).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<RicochetProjectile>().effectProbability * 100).ToString() + "%";
                imgPropiedad.sprite = imgDescPropiedad.sprite = propiedades[4];
                textxtPropiedad.text = SeleccionaIdiomas.gameText[30, 2];
                break;

        }
    }

    public void CargarPanelEventosDelDia()
    {

    }

    public void CargarEventoDelDia(int escenaFinal)
    {
        Debug.Log(escenaFinal);
        if(ShopManager.shopData != null) {

            int mundoCargado = 0;
            if (escenaFinal < 20)
            {
                world2 = 0;
                mundoCargado = 1;
                numEscenaCiudad = escenaFinal + 13;
                panelEventoAbajo.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
                panelEventoArriba.GetComponent<MovimientoPaneles>().BtnMostrarPanel();
            }
            else if (escenaFinal < 40)
            {
#if UNITY_IOS || UNITY_ANDROID
            mundoCargado = 2;
            GetComponentInChildren<WorldMap>().IniciarMundoE("desert");
                //numEscenaCiudad = ControladorId.eventoDelDia + 18;
                numEscenaCiudad = escenaFinal +18 ;
            world2 = 1;
#elif UNITY_ANDROID
                if (BundleManager.checkWorldCached("desert"))
                {

                    StartCoroutine(GetComponentInChildren<WorldMap>().IniciarMundoE("desert"));
                    mundoCargado = 2;
                }
                else
                {
                    desierto.SetActive(true);
                    GetComponentInChildren<WorldMap>().DescargarMundoE("desert");
                }
                world2 = 1;
#endif

            }
            else if (escenaFinal < 60)
            {
#if UNITY_IOS || UNITY_ANDROID
            mundoCargado = 3;
            numEscenaCiudad = escenaFinal +21;
            GetComponentInChildren<WorldMap>().IniciarMundoE("snow");
            world2 = 2;
#elif UNITY_ANDROID
                if (BundleManager.checkWorldCached("snow"))
                {
                    StartCoroutine(GetComponentInChildren<WorldMap>().IniciarMundoE("snow"));

                    mundoCargado = 3;
                }
                else
                {
                    nieve.SetActive(true);
                    GetComponentInChildren<WorldMap>().DescargarMundoE("snow");
                }

                world2 = 2;
#endif

            }

            CargarElementos(escenaFinal);

            if (mundoCargado != 0)
            {
                nombreEvento.text = starsRequisites.GetSpecialLevelRequisites(escenaFinal).levelName;
                numEvento = escenaFinal;
                int puntos = starsRequisites.GetSpecialLevelRequisites(escenaFinal).score;
                int idolos = starsRequisites.GetSpecialLevelRequisites(escenaFinal).idols;


                if (puntos == 0 && idolos == 0)
                {
                    textoEvento.text = SeleccionaIdiomas.gameText[9, 7];
                }
                else if (puntos == 0)
                {
                    textoEvento.text = SeleccionaIdiomas.gameText[9, 8] + ": " + starsRequisites.GetSpecialLevelRequisites(escenaFinal).idols;
                }
                else
                {
                    textoEvento.text = SeleccionaIdiomas.gameText[9, 5] + ": " + starsRequisites.GetSpecialLevelRequisites(escenaFinal).score;
                }

            }
        }
        else
        {
            numEvento = escenaFinal;
            StartCoroutine(EsperaCargaTienda(1));
        }
        

    }

    public void BtnEjecutarPublicidad()
    {
        LoadSc.worldSelected = 1;
        LoadSc.nextScene = 2;
        LoadSc.normalScene = true;
        SceneManager.LoadScene("LoadScreen");
    }

}
