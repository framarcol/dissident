﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeTitle : MonoBehaviour {

    public RectTransform fondo;
    public Text txtTitulo;

    void Update()
    {
        if(fondo.localPosition.x > 2000)
        {
            if (fondo.localPosition.x <= 3570) CambiarTextoTitulo("El desierto", 3570, new Color32(21,137,190,1));
            else CambiarTextoTitulo("La ciudad", 3570, new Color32(221, 153, 51, 1));
        }

        if(fondo.localPosition.x < 2000 && fondo.localPosition.x > -500)
        {
            if (fondo.localPosition.x <= 406) CambiarTextoTitulo("La nieve", 406, new Color32(51, 92, 44, 1));
            else CambiarTextoTitulo("El desierto", 406, new Color32(21, 137, 190, 1));
        }

        if(fondo.localPosition.x < -500)
        {
            if (fondo.localPosition.x <= -3000) CambiarTextoTitulo("La jungla", -3000, new Color32(66, 18, 38, 1));
            else CambiarTextoTitulo("La nieve", -3000, new Color32(51, 92, 44, 1));
        }
        

        
    }

    void CambiarTextoTitulo(string textoTitulo, int cambiaBase, Color colorTitulo)
    {

        float nuevoEntero = cambiaBase - fondo.localPosition.x;

        if (fondo.localPosition.x <= cambiaBase && fondo.localPosition.x >= cambiaBase - 500f)
        {          
            colorTitulo.a = nuevoEntero / 500;
            txtTitulo.color = colorTitulo;
        }
         if (fondo.localPosition.x <= cambiaBase + 500f && fondo.localPosition.x >= cambiaBase)
        {
            nuevoEntero *= -1;
            colorTitulo.a = nuevoEntero / 500;
            txtTitulo.color = colorTitulo;
        }

        txtTitulo.text = textoTitulo;
        
    }
}
