﻿using UnityEngine;
using System.Collections;

public class ControladorBaseDeDatos
{

    public static IEnumerator SubirPuntuacion(uint fase, uint puntuacion)
    {
        WWW itemsData;
        WWWForm datos = new WWWForm();
        WWWForm datos2 = new WWWForm();
#if UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
            datos2.AddField("id", Social.localUser.userName.ToString());
            itemsData = new WWW("https://agapornigames.com/dodongo/checkIdGooglePlay.php", datos2);

  
            yield return itemsData;

            datos.AddField("id", itemsData.text);
            datos.AddField("puntuacion", puntuacion.ToString());
            datos.AddField("fase", fase.ToString());

            itemsData = new WWW("https://agapornigames.com/dodongo/subirPuntuacion.php", datos);

        }
        else
        {
            itemsData = new WWW("www.google.es");
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            itemsData = new WWW("www.google.es");
        }
        else
        {
            if (Social.localUser.authenticated)
            {
                datos2.AddField("id", Social.localUser.userName.ToString());
                itemsData = new WWW("https://agapornigames.com/dodongo/checkIdGooglePlay.php", datos2);


                yield return itemsData;

                datos.AddField("id", itemsData.text);
                datos.AddField("puntuacion", puntuacion.ToString());
                datos.AddField("fase", fase.ToString());

                itemsData = new WWW("https://agapornigames.com/dodongo/subirPuntuacion.php", datos);

            }
            else
            {
                itemsData = new WWW("www.google.es");
            }
        }
     
#endif
        yield return itemsData;
    }
}
