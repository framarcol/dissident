﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorEventosDiarios : MonoBehaviour {

    public GameObject[] frames;
    int[] eventos;
    public Sprite[] imgFrame;
    public Sprite[] medallas;
    int eventoInicial;
    StarsRequisites starsRequisites;
    public GameObject flechaDerecha, flechaIzquierda;
    int numPagina;
    int eventoDeldia;
    string[] FechasEventos;
    public GameObject niveles;


    private void Start()
    {
        starsRequisites = Resources.Load("DataObjects/StarsRequisites", typeof(StarsRequisites)) as StarsRequisites;
        eventos = new int[] {11,34,51,12,32,52,13,14,33,53,15,36,54,19,17,35,55,18,31,56,16,37,57,20 };
        eventoDeldia = 0;
        for (int i = 0; i < eventos.Length; i++)
        {
            if(ControladorId.eventoDelDia == eventos[i])
            {
                eventoDeldia = i; 
            }
        }

        FechasEventos = new string[24];

        for (int i = 0; i < FechasEventos.Length; i++)
        {
            double dias = i - eventoDeldia;
            FechasEventos[i] = System.DateTime.Now.AddDays(dias).ToShortDateString();
        }

        numPagina = eventoDeldia / 6;
        CompruebaPagina();
        CargarEventos(numPagina * 6);

    }



    void CargarEventos(int inicio)
    {
        for (int i = 0; i < frames.Length; i++)
        {
            int numImagen = 0;
            for (int j = 0; j < imgFrame.Length; j++) if (imgFrame[j].name == eventos[inicio + i].ToString()) numImagen = j;
            frames[i].GetComponentsInChildren<Image>()[0].sprite = imgFrame[numImagen];
            frames[i].GetComponentInChildren<Text>().text = starsRequisites.GetSpecialLevelRequisites(eventos[inicio + i]).levelName.ToUpper();
            int numDia = starsRequisites.GetSpecialLevelRequisites(eventos[inicio + i]).diamondReward;
            frames[i].GetComponentsInChildren<Image>()[1].sprite = medallas[numDia];
            frames[i].GetComponentsInChildren<Text>()[1].text = FechasEventos[inicio + i];
            if (GameDataController.gameData.specialLevelsCompleted[eventos[inicio + i]]) frames[i].GetComponentsInChildren<Image>()[1].sprite = medallas[0];

            if (eventoDeldia == (inicio + i) || GameDataController.gameData.specialLevelsCompleted[eventos[inicio + i]])
            {
                frames[i].GetComponentsInChildren<Image>()[0].color = new Color(1, 1, 1, 1);
                frames[i].GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);
                frames[i].GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 1);
                frames[i].GetComponent<Button>().interactable = true;

               
;            }
            else
            {
                frames[i].GetComponentsInChildren<Image>()[0].color = new Color(1, 1, 1, 1);
                frames[i].GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);
                frames[i].GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 1);
                frames[i].GetComponent<Button>().interactable = true;
                /*  frames[i].GetComponentsInChildren<Image>()[0].color = new Color(1, 1, 1, 0.2f);
                  frames[i].GetComponentInChildren<Text>().color = new Color(1, 1, 1, 0.2f);
                  frames[i].GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 0.2f);
                  frames[i].GetComponent<Button>().interactable = false;*/
            }
        }
    }

    public void CargaEvento(int num)
    {
        int numEventoFinal = eventos[num + (numPagina * 6)];
        //  if (numEventoFinal < 20) niveles.GetComponent<CargarNiveles>().SelecionarMundo(0);
        //  else if (numEventoFinal < 50) niveles.GetComponent<CargarNiveles>().SelecionarMundo(1);
        // else niveles.GetComponent<CargarNiveles>().SelecionarMundo(2);
        niveles.GetComponent<CargarNiveles>().CargarEventoDelDia(numEventoFinal);
    }

    public void PaginaDerecha()
    {
        numPagina++;
        eventoInicial = numPagina * 6;
        CompruebaPagina();
        CargarEventos(eventoInicial);
    }

    public void PaginaIzquierda()
    {
        numPagina--;
        eventoInicial = numPagina * 6;
        CompruebaPagina();
        CargarEventos(eventoInicial);
    }

    void CompruebaPagina()
    {
        if (numPagina <= 0)
        {
            flechaIzquierda.SetActive(false);
        }
        else
        {
            flechaIzquierda.SetActive(true);
        }

        if (3 > numPagina)
        {
            flechaDerecha.SetActive(true);
        }
        else
        {
            flechaDerecha.SetActive(false);
        }
    }



}
