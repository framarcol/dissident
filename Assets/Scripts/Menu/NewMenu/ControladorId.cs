﻿using UnityEngine;
using System.Collections;
//sing GooglePlayGames;
//using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
//using Facebook.Unity;

public class ControladorId : MonoBehaviour
{

    public string url = "https://agapornigames.com/images/publi.png";

    public Animator popap;
	public Text txtPopap;
    public Text txtProgressPopap;

	public GameObject palabraPremium;
	public Material materialPremium;
	WWW itemsData, itemsData2, itemsData3, itemsData4, itemsData5, itemsData6, itemsData7, itemsData8;
	public static bool Logueado;
    public static int nivelesTutorial;
    public static bool publiInicioMostrada;
	public GameObject objetoEventoDelDia;
	public Text textoNombreEvento;
    int intentosRegistro;

	public GameObject[] eventos;
	public Sprite[] eventosMedallas;
	public GameObject EventoInfo;
	public GameObject EventoInfo2;
	public GameObject botonBetaBorrarAsset;
	public GameObject cargaNiveles;
    public GameObject confirmarLogueoApple, confirmarLogueoApple2, confirmarLogueoPlayJuegos, confirmarLogueoPlayJuegos2;
    public GameObject popapPubliInGame;
    public Text txtPubliInGame, txtTituloPubliInGame;
    public Image imgPubliInGame;
    int numIntentosConexion;

	public GameObject ControladorMenu;

#if UNITY_IOS
    string idEstrellas1 = "estrellas.1";
    string idEstrellas5 = "estrellas.5";
    string idEstrellas10 = "estrellas.10";
    string idEstrellas20 = "estrellas.20";
    string idEstrellas50 = "estrellas.50";
    string idEstrellas100 = "estrellas.100";
    string idEstrellas150 = "estrellas.150";
    string DesbloqueaYldon = "yldon";
    string DesbloqueaMiraka = "miraka";
    string CambiaSkate = "skate";
    string CambiaArma = "arma";
    string CambiaSkin = "skin";
    string eventos1 = "eventos.1";
    string eventos5 = "eventos.5";
    string eventos10 = "eventos.10";
    string eventos15 = "eventos.15";
    string eventos20 = "eventos.20";
#elif UNITY_ANDROID
    string idEstrellas1 = "CgkI6Pm71YgZEAIQAQ";
    string idEstrellas5 = "CgkI6Pm71YgZEAIQAw";
    string idEstrellas10 = "CgkI6Pm71YgZEAIQBA";
    string idEstrellas20 = "CgkI6Pm71YgZEAIQBQ";
    string idEstrellas50 = "CgkI6Pm71YgZEAIQBg";
    string idEstrellas100 = "CgkI6Pm71YgZEAIQDg";
    string idEstrellas150 = "CgkI6Pm71YgZEAIQDw";
    string DesbloqueaYldon = "CgkI6Pm71YgZEAIQEA";
    string DesbloqueaMiraka = "CgkI6Pm71YgZEAIQEQ";
    string CambiaSkate = "CgkI6Pm71YgZEAIQEg";
    string CambiaArma = "CgkI6Pm71YgZEAIQEw";
    string CambiaSkin = "CgkI6Pm71YgZEAIQFA";
    string eventos1 = "CgkI6Pm71YgZEAIQFQ";
    string eventos5 = "CgkI6Pm71YgZEAIQFg";
    string eventos10 = "CgkI6Pm71YgZEAIQFw";
    string eventos15 = "CgkI6Pm71YgZEAIQGA";
    string eventos20 = "CgkI6Pm71YgZEAIQGQ";
#endif

    // Relacion tipos
    // 0: Body
    // 1: Armas
    // 2: Monopatines
    // 3: Sets
    // 4: Upgrades

    public static uint[,] ObjetoCompradosPorTipo;
	public static int totalEstrellas;
	public static int[] limiteObjetos;
	public static int eventoDelDia;
	public GameObject[] islotesMedallas;
    public static bool cambiarUsuarioApple;

	public string textoPopap;

	void OnApplicationPause(bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus)
		{
#if !UNITY_EDITOR
          /*  if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    //Handle FB.Init
                    FB.Init(() => {
                        FB.ActivateApp();
                    });
                }*/
#endif
		}
	}

	void Awake()
	{
        intentosRegistro = 0;
        numIntentosConexion = 0;
        nivelesTutorial = 4;
		Logueado = false;
		ObjetoCompradosPorTipo = new uint[10, 100];
		limiteObjetos = new int[10];


		if (!PlayerPrefs.HasKey(GameManager.LanguagePref))
		{
			PlayerPrefs.SetString(GameManager.LanguagePref, Application.systemLanguage.ToString());
		}

		if (!PlayerPrefs.HasKey(GameManager.HandPref))
		{
			PlayerPrefs.SetInt("Hand", 0);
		}

		if (!PlayerPrefs.HasKey(GameManager.JumpOptionPref))
		{
			PlayerPrefs.SetInt(GameManager.JumpOptionPref, 0);
		}

		if (!PlayerPrefs.HasKey(GameManager.JumpSensitivityPref))
		{
			PlayerPrefs.SetFloat(GameManager.JumpSensitivityPref, 1);
		}

		if (!PlayerPrefs.HasKey(GameManager.SoundEffectVolumePref))
		{
			PlayerPrefs.SetFloat(GameManager.SoundEffectVolumePref, 1);
		}

		if (!PlayerPrefs.HasKey(GameManager.MusicVolumePref))
		{
			PlayerPrefs.SetFloat(GameManager.MusicVolumePref, 1);
		}

        if (!PlayerPrefs.HasKey(GameManager.ammoDisplayPref))
        {
            PlayerPrefs.SetFloat(GameManager.ammoDisplayPref, 0);
        }

        if (!PlayerPrefs.HasKey("SleepMode"))
		{
			PlayerPrefs.SetInt("SleepMode", 0);
		}

		if (!PlayerPrefs.HasKey("Muertes"))
		{
			PlayerPrefs.SetInt("Muertes", 0);
		}

		if (!PlayerPrefs.HasKey("MuertesPu"))
		{
			PlayerPrefs.SetInt("MuertesPu", 0);
		}

		if (!PlayerPrefs.HasKey("NivelesConseguidos"))
		{
			PlayerPrefs.SetInt("NivelesConseguidos", 0);
		}

        if (!PlayerPrefs.HasKey("CheckPoint"))
        {
            PlayerPrefs.SetInt("CheckPoint", 0);
        }

        if (!PlayerPrefs.HasKey("Anuncios"))
        {
            PlayerPrefs.SetInt("Anuncios", 0);
        }

        if (!PlayerPrefs.HasKey("VecesTienda"))
        {
            PlayerPrefs.SetInt("VecesTienda", 0);
        }

        if (!PlayerPrefs.HasKey("NivelesConseguidosPu"))
		{
			PlayerPrefs.SetInt("NivelesConseguidosPu", 0);
		}

		if (!PlayerPrefs.HasKey("PermisosGoogleAceptados"))
		{
			PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
		}

        if (!PlayerPrefs.HasKey("PermisosAppleAceptados"))
        {
            PlayerPrefs.SetInt("PermisosAppleAceptados", 0);
        }

        if (!PlayerPrefs.HasKey("PermisosPlayJuegosAceptados"))
        {
            PlayerPrefs.SetInt("PermisosPlayJuegosAceptados", 0);
        }

        if (PlayerPrefs.GetInt("SleepMode") == 0)
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}
		else
		{
			Screen.sleepTimeout = SleepTimeout.SystemSetting;
		}

	}

    void Update()
    {
        txtProgressPopap.text = ShopManager.porcentajeDescargado + "%";
    }

    public void ConfPopap()
	{
		popap.SetTrigger("FadeOut");
		MovimientoCamaraMenu.descargaIniciada = false;
	}

	void Start()
	{
#if UNITY_IOS
        PlayerPrefs.SetInt("PreguntadoGameCenter", 1);
        if(PlayerPrefs.GetInt("PreguntadoGameCenter") != 1)
        {
            confirmarLogueoApple.SetActive(true);
        }
        else
        {
            CargaTienda();
        }
#elif UNITY_ANDROID

        PlayerPrefs.SetInt("PreguntadoPlayJuegos", 1);

        if(PlayerPrefs.GetInt("PreguntadoPlayJuegos") != 1)
        {
            confirmarLogueoPlayJuegos.SetActive(true);
        }
        else
        {
            CargaTienda();
        }
        
#endif


        CambiarColorPremium();
	}

	void CambiarColorPremium()
	{
		if (GameDataController.gameData.premiumState)
		{
			palabraPremium.gameObject.SetActive(false);

			materialPremium.color = new Color(0.809f, 0.497f, 0.077f, 1);
		}
		else
		{
			materialPremium.color = new Color(0.161f, 0.161f, 0.776f, 1);
		}
#if UNITY_EDITOR
		materialPremium.color = new Color(0.161f, 0.161f, 0.776f, 1);
		palabraPremium.gameObject.SetActive(true);
#endif
	
	}

    public void AbrirPestanaPlayJuegos()
    {
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            confirmarLogueoPlayJuegos2.SetActive(true);
        }
        else
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 4];
            popap.SetTrigger("Fade");
        }
    }

    public void AbrirPestanaApple()
    {
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            confirmarLogueoApple2.SetActive(true);
        }
        else
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 4];
            popap.SetTrigger("Fade");
        }         
    }

    public void AppleLogueoSi(bool esConf)
    {
#if UNITY_EDITOR
#elif UNITY_IOS
        PlayerPrefs.SetInt("PreguntadoGameCenter", 1);
#endif
        PlayerPrefs.SetInt("PermisosAppleAceptados", 1);
      
        if (!esConf)
        {
            CargaTienda();
        }
        else
        {
            cambiarUsuarioApple = true;
            CargaTienda();
        }
     
        if(!esConf)
        confirmarLogueoApple.SetActive(false);
        else confirmarLogueoApple2.SetActive(false);
    }

    public void PlayJuegosLogueoSi(bool esConf)
    {
        PlayerPrefs.SetInt("PreguntadoPlayJuegos", 1);
        PlayerPrefs.SetInt("PermisosPlayJuegosAceptados", 1);      

        if (!esConf)
        {
            CargaTienda();
        }
        else
        {
            cambiarUsuarioApple = true;
            CargaTienda();
        }

        if (!esConf)
            confirmarLogueoPlayJuegos.SetActive(false);
        else confirmarLogueoPlayJuegos2.SetActive(false);
    }

    public void PlayJuegosLogueoNo(bool esConf)
    {
        PlayerPrefs.SetInt("PreguntadoPlayJuegos", 1);
#if UNITY_EDITOR
        PlayerPrefs.SetInt("PreguntadoPlayJuegos", 0);
#endif
        if (!esConf)
        {
            CargaTienda();
        }

        if (!esConf)
            confirmarLogueoPlayJuegos.SetActive(false);
        else confirmarLogueoPlayJuegos2.SetActive(false);
    }

    public void AppleLogueoNo(bool esConf)
    {
#if UNITY_EDITOR
#elif UNITY_IOS
        PlayerPrefs.SetInt("PreguntadoGameCenter", 1);
#endif
        if (!esConf)
        {
            CargaTienda();
        }

        if (!esConf)
            confirmarLogueoApple.SetActive(false);
        else confirmarLogueoApple2.SetActive(false);
    }

    public void CargaTienda()
	{
        PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);

        if (PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1)
		{
			if (!CargaIntro.modoOffline)
			{
				GetComponent<MovimientoCamaraMenu>().LoadingActivar();
				StartCoroutine(CheckInternetConnection(true));
			}
			else
			{
				ObjetoCompradosPorTipo = GameDataController.gameData.shopItemsObtained;
                if (GameDataController.gameData.ammoUpgradeEquipped != 0 || GameDataController.gameData.lifeUpgradeEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 72
         || GameDataController.gameData.weaponSkinEquipped != 71 || GameDataController.gameData.bodySkinEquipped != 1)
                {
                    if (GameDataController.gameData.weaponSkinEquipped != 0
                    || GameDataController.gameData.bodySkinEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 0)
                    {
                        StartCoroutine(GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda(false));
                    }

                }
            }
		}
	}

	public IEnumerator ObtenerId()
	{
		WWWForm datos = new WWWForm();

		float estrellas = 0, estrellasUno = 0, estrellasDos = 0, estrellasTres = 0;

       /* if(GameDataController.gameData.playerName == "Playerfinal")
        {
            estrellas
        }*/

		for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++)
			{

				estrellas += GameDataController.gameData.stars[j, i];
				if (GameDataController.gameData.stars[j, i] == 1)
				{
					estrellasUno++;
				}
				else if (GameDataController.gameData.stars[j, i] == 2)
				{
					estrellasDos++;
				}
				else if (GameDataController.gameData.stars[j, i] == 3)
				{
					estrellasTres++;
				}
			}

		for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;

#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
            datos.AddField("idG", SystemInfo.deviceUniqueIdentifier);
            datos.AddField("SO", "Android");
        }
        else
        {
            datos.AddField("id", Social.localUser.userName.ToString());
            datos.AddField("idG", Social.localUser.id.ToString());
            datos.AddField("SO", "Android");
        }
#endif
#if UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
            datos.AddField("idG", SystemInfo.deviceUniqueIdentifier);
        datos.AddField("SO", "Apple");
        }
        else
        {
            datos.AddField("id", Social.localUser.userName.ToString());
            datos.AddField("idG", Social.localUser.id.ToString());
        datos.AddField("SO", "Apple");
        }


#endif

        string FasesSuperadas = "";

        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++) if (GameDataController.gameData.stars[j, i] > 0) FasesSuperadas += j + ";" + i + ";" + GameDataController.gameData.stars[j, i].ToString() + ";";

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) FasesSuperadas += 10 + ";" + i + ";" + 1 + ";";

#if UNITY_EDITOR
          FasesSuperadas = "10;12;1;10;14;1;10;4;1;";
#endif
        string IdolosConseguidos = "";

        for (int j = 0; j < 4; j++) for (int i = 0; i < 9; i++) for (int k = 0; k < 10; k++) if (GameDataController.gameData.idols[j, i, k]) IdolosConseguidos += j + ";" + i + ";" + k + ";";

#if UNITY_EDITOR
         IdolosConseguidos = "0;2;3;";
#endif

        datos.AddField("SOV", SystemInfo.operatingSystem.ToString());
		datos.AddField("Stars", estrellas.ToString());
		datos.AddField("StarsU", estrellasUno.ToString());
		datos.AddField("StarsD", estrellasDos.ToString());
		datos.AddField("StarsT", estrellasTres.ToString());
		datos.AddField("MuertesJugador", PlayerPrefs.GetInt("Muertes").ToString());
		datos.AddField("NivelesCompletados", PlayerPrefs.GetInt("NivelesConseguidos").ToString());
        datos.AddField("AnunciosVistos", PlayerPrefs.GetInt("Anuncios"));
        datos.AddField("CheckPoints", PlayerPrefs.GetInt("CheckPoint"));
        datos.AddField("VecesTienda", PlayerPrefs.GetInt("VecesTienda"));
        datos.AddField("idioma", PlayerPrefs.GetString(GameManager.LanguagePref));
        datos.AddField("FasesSuperadas", FasesSuperadas);
        datos.AddField("IdolosConseguidos", IdolosConseguidos);

        //r(int i = 0; i < GameDataController.gameData.playerRecords.LongLength)
        uint puntosTotales = 0;
        foreach(uint i in GameDataController.gameData.playerRecords)
        {
            puntosTotales += i;
        }

        datos.AddField("PuntosTotalesJuego", puntosTotales.ToString());

        if (cambiarUsuarioApple)
        {
            datos.AddField("idGA", SystemInfo.deviceUniqueIdentifier);
        }

        //Debug.Log("Llego");

        if (!cambiarUsuarioApple)
        {
            itemsData = new WWW("https://agapornigames.com/dodongo/checkId2.php", datos);
        }
        else
        {
            itemsData = new WWW("https://agapornigames.com/dodongo/changeAppleUser.php", datos);
        }

        while (!itemsData.isDone)
        {
           // txtProgressPopap.text = itemsData.progress.ToString() + "%";
            yield return null;
        }

        yield return itemsData;

        Debug.Log(itemsData.text);

        if (!cambiarUsuarioApple)
        {
            ComprobarRegistro();
        }
		
	}


	public void ComprobarRegistro()
	{
        string[] cadena = itemsData.text.Split('}');

        if (cadena[0] == "8") {

            if (cadena[1] == "2")
            {
                popap.SetTrigger("Fade");
                txtPopap.text = "Registration Failed";
            }

            CheckLevelDay(cadena[2]);

            CheckPubliInGame(cadena[3]);
        }
		else
		{
           
            if (cadena[1] == "1")
            {
                GameDataController.gameData.premiumState = true;
                CambiarColorPremium();
            }
            else if (cadena[1] == "0") GameDataController.gameData.premiumState = true;

            GameDataController.gameData.diamondCurrency = uint.Parse(cadena[2]);

            if (GameDataController.gameData.diamondQueue > 0 && GameDataController.gameData.diamondQueue < 1000)
            {
                StartCoroutine(AnadirDiamantesEstrellas());

            }
            else if (GameDataController.gameData.diamondQueue < 0)
            {
                StartCoroutine(AnadirDiamantesEstrellas());
            }

            if (cadena[3] == "1")
            {
                GameDataController.gameData.betaUser = true;
                botonBetaBorrarAsset.SetActive(true);
                eventoDelDia = int.Parse(cadena[3]);

                for (int i = 0; i < eventos.Length; i++)
                {
                    eventos[i].SetActive(true);
                }
            }

            CheckLevelDay(cadena[4]);

            CheckPubliInGame(cadena[5]);

            ComprobarFases(cadena[6]);

            ComprobarIdolosObtenidos(cadena[7]);

            ComprobarDatosItemsComprados(cadena[8]);
		}

	}

    public void CheckLevelDay(string cadena)
    {
        if (cadena == "0")
        {
            objetoEventoDelDia.SetActive(false);

            for (int i = 0; i < eventos.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[int.Parse(eventos[i].name)]) eventos[i].SetActive(true);

        }
        else
        {
            if (PlayerPrefs.GetInt(EventoInfo.name) != 1 || PlayerPrefs.GetInt(EventoInfo2.name) != 1)
            {
                ControladorMenu.GetComponent<EventosController>().LlamaEvento();
                EventoInfo.SetActive(true);
                EventoInfo2.SetActive(true);
                PlayerPrefs.SetInt(EventoInfo.name, 1);
                PlayerPrefs.SetInt(EventoInfo2.name, 1);
            }

            objetoEventoDelDia.SetActive(true);
            eventoDelDia = int.Parse(cadena);
            StarsRequisites starsRequisites = Resources.Load("DataObjects/StarsRequisites", typeof(StarsRequisites)) as StarsRequisites;
            textoNombreEvento.text = starsRequisites.GetSpecialLevelRequisites(eventoDelDia).levelName;
            if (!GameDataController.gameData.specialLevelsCompleted[eventoDelDia])
                objetoEventoDelDia.GetComponent<Image>().sprite = eventosMedallas[starsRequisites.GetSpecialLevelRequisites(eventoDelDia).diamondReward];
            else
            {
                objetoEventoDelDia.GetComponent<Image>().sprite = eventosMedallas[0];
            }
        }

        for (int i = 0; i < eventos.Length; i++)
        {
            if (GameDataController.gameData.specialLevelsCompleted[int.Parse(eventos[i].name)] || cadena == eventos[i].name)
                eventos[i].SetActive(true);
        }
    }

    public void ComprobarFases(string cadena)
	{
		if (cadena != "0")
		{
			string[] fasesPasadas = cadena.Split(';');

			for (int i = 0; i < fasesPasadas.Length - 3; i += 3)
			{
				if (fasesPasadas[i] != "10") GameDataController.gameData.stars[int.Parse(fasesPasadas[i]), int.Parse(fasesPasadas[i + 1])] = ushort.Parse(fasesPasadas[i + 2]);
				else GameDataController.gameData.specialLevelsCompleted[int.Parse(fasesPasadas[i + 1])] = true;
			}

			cargaNiveles.GetComponent<CargarNiveles>().CargaMedallero();
			for (int i = 0; i < islotesMedallas.Length; i++) islotesMedallas[i].GetComponent<CargaEstrellasIslote>().CargaMedallas();
		}
	}

	public void ComprobarIdolosObtenidos(string cadena)
	{
		if (cadena != "0")
		{
			string[] fasesPasadas = cadena.Split(';');

			for (int i = 0; i < fasesPasadas.Length - 3; i += 3) GameDataController.gameData.idols[int.Parse(fasesPasadas[i]), int.Parse(fasesPasadas[i + 1]), int.Parse(fasesPasadas[i + 2])] = true;
		}

	}

	public void ComprobarDatosItemsComprados(string cadena)
	{
		if (cadena != "0")
		{
			string[] objetos = cadena.Split(';');

			for (int i = 0, j = 0, k = 0; i < objetos.Length - 1; i++)
			{
				if (objetos[i] != "L")
				{
					ObjetoCompradosPorTipo[j, k] = uint.Parse(objetos[i]);
					GameDataController.gameData.shopItemsObtained[j, k] = (uint)ObjetoCompradosPorTipo[j, k];
                    k++;
				}
				else
				{
					limiteObjetos[j] = k;
					k = 0;
					j++;
				}

			}
		}

#if UNITY_EDITOR
        GameDataController.gameData.ammoUpgradeEquipped = 0;
        GameDataController.gameData.bodySkinEquipped = 1;
        GameDataController.gameData.weaponSkinEquipped = 71;
        GameDataController.gameData.skateSkinEquipped = 72;
        GameDataController.gameData.lifeUpgradeEquipped = 0;
#endif

        if (GameDataController.gameData.ammoUpgradeEquipped != 0 || GameDataController.gameData.lifeUpgradeEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 72
            || GameDataController.gameData.weaponSkinEquipped != 71 || GameDataController.gameData.bodySkinEquipped != 1)
        {
            if (GameDataController.gameData.weaponSkinEquipped != 0
            || GameDataController.gameData.bodySkinEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 0)
            {
                StartCoroutine(GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda(false));
            }
           
        }
        
    }

    public IEnumerator CheckPubliInGame(string cadena)
    {
        if(cadena != "0" && !publiInicioMostrada)
        {
            string[] textosPlay = cadena.Split(';');


            txtTituloPubliInGame.text = textosPlay[0];
            txtPubliInGame.text = textosPlay[1];

            WWW www = new WWW(url);

            yield return www;

            Sprite miSp = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

            imgPubliInGame.sprite = miSp;

            popapPubliInGame.SetActive(true);
            publiInicioMostrada = true;
        }
       
    }

    public void CerrarPubliInGame()
    {
        popapPubliInGame.SetActive(false);
    }

	public IEnumerator AnadirDiamantesEstrellas()
	{
		WWWForm datos2 = new WWWForm();
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos2.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos2.AddField("idUsuario", Social.localUser.id.ToString());
        }
#endif
#if UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos2.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos2.AddField("idUsuario", Social.localUser.id.ToString());
        }
       
#endif

        datos2.AddField("diamantes", GameDataController.gameData.diamondQueue.ToString());

		itemsData5 = new WWW("https://agapornigames.com/dodongo/updateDiamantes.php", datos2);

        while (!itemsData5.isDone)
        {
            //txtProgressPopap.text = itemsData5.progress.ToString() + "%";
            yield return null;
        }

        yield return itemsData5;

		GameManager.AddDiamonds(GameDataController.gameData.diamondQueue);
		GameDataController.gameData.diamondQueue = 0;
		GameDataController.Save();
	}

	public IEnumerator CheckInternetConnection(bool primeraVez)
	{
		WWW www = new WWW("https://agapornigames.com/dodongo/");
		yield return www;
		if (www.error == null)
		{

#if UNITY_EDITOR

#elif UNITY_ANDROID
          /*  if(PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") == 1){
                PlayGamesPlatform.Activate();
            }  */        
#endif

#if UNITY_ANDROID

            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") == 1)
            {
                if (Social.localUser.authenticated)
                {

                    PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                    Logueado = true;
                    StartCoroutine(ObtenerId());
                    GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                    ComprobarLogros();

#if !UNITY_EDITOR
			/*	if (FB.IsInitialized) FB.ActivateApp();
                else FB.Init(() => { FB.ActivateApp(); });*/
#endif
                }
                else
                {
                    Autentificar();
                }
            }
            else
            {
                PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                Logueado = true;
                GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                ComprobarLogros();
                StartCoroutine(ObtenerId());
           }
#endif
#if UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                Debug.Log("No lo hago");
                PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                Logueado = true;
                GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                ComprobarLogros();
                StartCoroutine(ObtenerId());
               // StartCoroutine(GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda());

            }
            else
            {
                if (Social.localUser.authenticated)
                {

                    PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                    Logueado = true;
                    StartCoroutine(ObtenerId());
                    GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                 //   StartCoroutine(GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda());
                    ComprobarLogros();

#if !UNITY_EDITOR
				if (FB.IsInitialized) FB.ActivateApp();
                else FB.Init(() => { FB.ActivateApp(); });
#endif
                }
                else
                {
                    Autentificar();
                }

            }
#endif
        }
        else
        {
            if (!primeraVez)
            {
                popap.SetTrigger("Fade");
                txtPopap.text = SeleccionaIdiomas.gameText[19, 2];
            }     
            ObjetoCompradosPorTipo = GameDataController.gameData.shopItemsObtained;

            if (GameDataController.gameData.ammoUpgradeEquipped != 0 || GameDataController.gameData.lifeUpgradeEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 72
         || GameDataController.gameData.weaponSkinEquipped != 71 || GameDataController.gameData.bodySkinEquipped != 1)
            {
                if (GameDataController.gameData.weaponSkinEquipped != 0
                || GameDataController.gameData.bodySkinEquipped != 0 || GameDataController.gameData.skateSkinEquipped != 0)
                {
                    StartCoroutine(GetComponent<MovimientoCamaraMenu>().ComprobarEstadoTienda(false));
                }

            }
        }
    }

    public void ComprobarLogros()
    {
        int estrellas = 0, estrellasEventos =0;
        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++)  estrellas += GameDataController.gameData.stars[j, i];

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;

        for (int i = 6; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) if (i != 50 && i != 30) estrellasEventos++;

#if UNITY_EDITOR
/*
        if(GameDataController.gameData.stars[1,0] > 0 && PlayerPrefs.GetInt(DesbloqueaYldon) != 1)
        {
            Social.ReportProgress(DesbloqueaYldon, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(DesbloqueaYldon, 1);
                }
            });
        }

        if (GameDataController.gameData.stars[2, 0] > 0 && PlayerPrefs.GetInt(DesbloqueaMiraka) != 1)
        {
            Social.ReportProgress(DesbloqueaMiraka, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(DesbloqueaMiraka, 1);
                }
            });
        }

        if (estrellasEventos >= 1 && PlayerPrefs.GetInt(eventos1) != 1)
        {
            Social.ReportProgress(eventos1, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(eventos1, 1);
                }
            });
        }

        if (estrellasEventos >= 5 && PlayerPrefs.GetInt(eventos5) != 1)
        {
            Social.ReportProgress(eventos5, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(eventos5, 1);
                }
            });
        }

        if (estrellasEventos >= 10 && PlayerPrefs.GetInt(eventos10) != 1)
        {
            Social.ReportProgress(eventos10, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(eventos10, 10);
                }
            });
        }

        if (estrellasEventos >= 15 && PlayerPrefs.GetInt(eventos15) != 1)
        {
            Social.ReportProgress(eventos15, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(eventos15, 1);
                }
            });
        }

        if (estrellasEventos >= 20 && PlayerPrefs.GetInt(eventos20) != 1)
        {
            Social.ReportProgress(eventos20, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(eventos20, 1);
                }
            });
        }

        if (estrellas >= 1 && PlayerPrefs.GetInt(idEstrellas1) != 1)
        {
            Social.ReportProgress(idEstrellas1, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas1, 1);
                }
            });
        }

        if (estrellas >= 5 && PlayerPrefs.GetInt(idEstrellas5) != 1)
        {
            Social.ReportProgress(idEstrellas5, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas5, 1);
                }
            });
        }

        if (estrellas >= 10 && PlayerPrefs.GetInt(idEstrellas10) != 1)
        {
            Social.ReportProgress(idEstrellas10, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas10, 1);
                }
            });
        }

        if (estrellas >= 20 && PlayerPrefs.GetInt(idEstrellas20) != 1)
        {
            Social.ReportProgress(idEstrellas20, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas20, 1);
                }
            });
        }

        if (estrellas >= 50 && PlayerPrefs.GetInt(idEstrellas50) != 1)
        {
            Social.ReportProgress(idEstrellas50, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas50, 1);
                }
            });
        }

        if (estrellas >= 100 && PlayerPrefs.GetInt(idEstrellas100) != 1)
        {
            Social.ReportProgress(idEstrellas100, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas100, 1);
                }
            });
        }

        if (estrellas >= 150 && PlayerPrefs.GetInt(idEstrellas150) != 1)
        {
            Social.ReportProgress(idEstrellas150, 100, (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt(idEstrellas150, 1);
                }
            });
        }
*/
#endif

    }

    public void Equiparse(string objeto)
    {
#if UNITY_EDITOR
        /*
        if(Logueado == true)
        {
            switch (objeto)
            {
                case "monopatin":
                    Social.ReportProgress(CambiaSkate, 100, (bool success) =>
                    {
                        if (success)
                        {
                            PlayerPrefs.SetInt(CambiaSkate, 1);
                        }
                    });
                    break;
                case "arma":
                    Social.ReportProgress(CambiaArma, 100, (bool success) =>
                    {
                        if (success)
                        {
                            PlayerPrefs.SetInt(CambiaArma, 1);
                        }
                    });
                    break;
                case "skin":
                    Social.ReportProgress(CambiaSkin, 100, (bool success) =>
                    {
                        if (success)
                        {
                            PlayerPrefs.SetInt(CambiaSkin, 1);
                        }
                    });
                    break;    
            }
        }
        */
#endif
    }

    void NuevaAutentificacion()
    {
        Autentificar();
    }

    public void Autentificar()
    {

        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                Logueado = true;
                GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                ComprobarLogros();
                StartCoroutine(ObtenerId());
                

               /* if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    //Handle FB.Init
                    FB.Init(() => {
                        FB.ActivateApp();
                    });
                }*/

            }
            else
            {
                if(numIntentosConexion < 1)
                {
                    numIntentosConexion++;
                    NuevaAutentificacion();
                }
                else
                {
                    popap.SetTrigger("Fade");
#if UNITY_ANDROID
                    txtPopap.text = SeleccionaIdiomas.gameText[31, 7];
#elif UNITY_IOS
                    txtPopap.text = SeleccionaIdiomas.gameText[31, 4];
#endif

                    PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
                    Logueado = true;
                    GetComponent<MovimientoCamaraMenu>().LoadingDesactivar();
                    ComprobarLogros();
                    StartCoroutine(ObtenerId());
                }
            }

        });

    }

}
