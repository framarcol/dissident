﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControladorMolinoIdolos : MonoBehaviour {

    public Button botonRotar;
    public Image pulsado;
    public Animator girador, girador2;
    int giros;

    public Transform posicionCamara;

    public Sprite[] simbolosIdolos;
    public Image idoloSim;

    public GameObject[] idolosCiudad, idolosDesierto, idolosNieve, idolosJungla;
    public GameObject idoloJefeCiudad, idoloJefeDesierto, idoloJefeNieve;

    public GameObject panelIdolos;

    int nunMundo = 4, numIdolosPorMundo = 9, numFragmentos = 10;

    string[] triguer;

    void Start()
    {
        triguer = new string[4] { "Primero", "Segundo", "Tercero", "Cuarto" };
        giros = 0;
        int contador = 0;
        for (int i = 0; i < nunMundo; i++)
        {
            for(int j = 0; j < numIdolosPorMundo; j++)
            {
                for (int k = 0; k < numFragmentos; k++) if (GameDataController.gameData.idols[i, j, k]) contador++;

                if (contador == numFragmentos)
                {
                    switch (i)
                    {
                        case 0:
                            idolosCiudad[j].SetActive(true);
                            break;
                        case 1:
                            idolosDesierto[j].SetActive(true);
                            break;
                        case 2:
                            idolosNieve[j].SetActive(true);
                            break;
                        case 3:
                            idolosJungla[j].SetActive(true);
                            break;
                    }
                }
                contador = 0;             
            }
        }
#if UNITY_EDITOR
        for(int i = 0; i < idolosCiudad.Length; i++)
        {
            idolosCiudad[i].SetActive(true);
            idolosDesierto[i].SetActive(true);
            idolosNieve[i].SetActive(true);
        }            
#endif

        int contador2 = 0;

        for (int i = 0; i < idolosCiudad.Length; i++)
        {
            if (idolosCiudad[i].activeInHierarchy) contador2++;
        }

        if (contador2 == 9) idoloJefeCiudad.SetActive(true);

        contador2 = 0;
        for (int i = 0; i < idolosDesierto.Length; i++)
        {
            if (idolosDesierto[i].activeInHierarchy) contador2++;
        }

        if (contador2 == 9) idoloJefeDesierto.SetActive(true);

        contador2 = 0;
        for (int i = 0; i < idolosNieve.Length; i++)
        {
            if (idolosNieve[i].activeInHierarchy) contador2++;
        }

        if (contador2 == 9) idoloJefeNieve.SetActive(true);

    }

    public void BtnRotar()
    {
        girador.SetTrigger(triguer[giros]);
        girador2.SetTrigger(triguer[giros]);
        giros++;
        if (giros == 4) giros = 0;
        botonRotar.interactable = false;
        pulsado.color = new Color(1, 1, 1, 1);
        StartCoroutine(compruebaAnimacion());
    }

    IEnumerator compruebaAnimacion()
    {
        yield return new WaitForSeconds(2);
        idoloSim.sprite = simbolosIdolos[giros];
        botonRotar.interactable = true;
        pulsado.color = new Color(1, 1, 1, 0);
        if(posicionCamara.transform.localPosition.y < 1)  panelIdolos.SetActive(false);
    }

    public void ActivarBotonRotacion()
    {
        botonRotar.interactable = true;
        pulsado.color = new Color(1, 1, 1, 0);
    }

}
