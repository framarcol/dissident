﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EfectoCompras : MonoBehaviour {

    public GameObject movCamMenu;
    public bool SoyMenu;
    public Text textopoapLoading;
   
    public IEnumerator AsignarPremium()
    {
        WWWForm datos = new WWWForm();
        WWW itemsData3;
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
  
#endif


        itemsData3 = new WWW("https://agapornigames.com/dodongo/PComprado.php", datos);

        yield return itemsData3;

        textopoapLoading.text += "";

     //   GetComponent<Purchaser>().ProductoTerminado();

        GameDataController.Save();

        if (SoyMenu) {

            movCamMenu.GetComponent<MovimientoCamaraMenu>().AsignarSetsPremium();
            gameObject.GetComponent<Purchaser>().CambiaColorPremium();
        }
        

    }

    public void ActualizarDiamantes2(int diaComp)
    {
        textopoapLoading.text += "";
        StartCoroutine(ActualizarDiamantes(diaComp));
    }

    public void AsignarPremium2()
    {
        textopoapLoading.text += "";
        StartCoroutine(AsignarPremium());
    }

    public IEnumerator ActualizarDiamantes(int DiamantesComprados)
    {
        WWWForm datos = new WWWForm();
        WWW itemsData3;
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("idUsuario", Social.localUser.id.ToString());
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("idUsuario", Social.localUser.id.ToString());
        }

#endif
        
        datos.AddField("diamantes", DiamantesComprados);

        itemsData3 = new WWW("https://agapornigames.com/dodongo/updateDiamantesCompras.php", datos);

        yield return itemsData3;

        textopoapLoading.text += "";

     //   GetComponent<Purchaser>().ProductoTerminado();

        int diBaseI = int.Parse(itemsData3.text);
        uint diBase = 0;

        if (diBaseI < 0) diBase = 0;
        else diBase = (uint)diBaseI;

        GameDataController.gameData.diamondCurrency = diBase;
        GameDataController.Save();
    }

}
