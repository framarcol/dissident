﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ElegirIdioma : MonoBehaviour {

    public string nombreIdioma;

    public void ToggleChanged(bool Value)
    {
        if (Value)
        {
            PlayerPrefs.SetString(GameManager.LanguagePref, nombreIdioma);
            SeleccionaIdiomas.ActualizaIdioma();
        }

    }



}
