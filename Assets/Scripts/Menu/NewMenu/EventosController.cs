﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EventosController : MonoBehaviour {

    public GameObject[] eventos;
    public Text textoNumNotificaciones, textoNumNotificacionesPanelHologramas;
    public GameObject fleIz;
    public GameObject fleDer;
    int numMostrado;
    public static bool SoyEvento;
    public Image imgInfo;
    public Sprite[] imgInfoNew;
    public GameObject goTextoNumNots;
    

    int numElementosFlecha;

    public static bool EventoPopapDesbloqueado;
    public static int numNots;


    void Awake()
    {
        numMostrado = 0;
        for(int i = 0; i < eventos.Length; i++) if (!PlayerPrefs.HasKey(eventos[i].name)) PlayerPrefs.SetInt(eventos[i].name, 0);
       
    }

	void Start () {
#if UNITY_EDITOR
    //    for (int i = 0; i < eventos.Length; i++) PlayerPrefs.DeleteKey(eventos[i].name);
        //numElementosFlecha = 5;
#endif
        numNots = 0;
        numElementosFlecha = 3;

        if (GameDataController.gameData.stars[0, 1] > 0 || PlayerPrefs.GetInt(eventos[3].name) == 1) numElementosFlecha+=2;

        if (PlayerPrefs.GetInt(eventos[0].name) == 1) EventoPopapDesbloqueado = true;

        int iFinal;

        if (EventoPopapDesbloqueado) iFinal = 0;
        else iFinal = 1;

        for (int i = iFinal; i <= numElementosFlecha; i++)
        {
            if (PlayerPrefs.GetInt(eventos[i].name) == 0)
            {
                numMostrado = i;
                numNots++;
            }
        }
        

        ComprobadorFlecha();
       
    }

    public void MostrarEvento()
    {
        for (int i = 0; i < eventos.Length; i++) eventos[i].SetActive(false);
        eventos[numMostrado].SetActive(true);
        ComprobadorFlecha();
    }

    public void LlamaEvento()
    {
        numMostrado = 0;
        EventoPopapDesbloqueado = true;
        numNots++;
        MostrarEvento();
    }


    public void BtnDer()
    {
        eventos[numMostrado].SetActive(false);
        numMostrado++;
        eventos[numMostrado].SetActive(true);
        ComprobadorFlecha();
    }

    public void BtnIzq()
    {
        eventos[numMostrado].SetActive(false);
        numMostrado--;
        eventos[numMostrado].SetActive(true);
        ComprobadorFlecha();
    }

    void ComprobadorFlecha()
    {
        if (numMostrado > numElementosFlecha-1) fleIz.SetActive(false);
        else fleIz.SetActive(true);

        if (EventoPopapDesbloqueado)
        {
            if (numMostrado > 0) fleDer.SetActive(true);
            else fleDer.SetActive(false);
        }
        else
        {
            if (numMostrado > 1) fleDer.SetActive(true);
            else fleDer.SetActive(false);
        }

      
       
    }

    void Update()
    {
        if (numNots < 0) numNots = 0;

        if(numNots == 0)
        {
            goTextoNumNots.SetActive(false);
            imgInfo.sprite = imgInfoNew[0];
        }
        else
        {
            imgInfo.sprite = imgInfoNew[1];
            goTextoNumNots.SetActive(true);
            textoNumNotificaciones.text = (numNots).ToString();
            textoNumNotificacionesPanelHologramas.text = textoNumNotificaciones.text;
        }

    }

}
