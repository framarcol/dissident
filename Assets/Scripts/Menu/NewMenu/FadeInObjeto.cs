﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInObjeto : MonoBehaviour {

    public bool aparecer, desaparecer;

    private void Start()
    {
        GetComponent<CanvasGroup>().interactable = false;
        aparecer = true;
        desaparecer = false;
    }

    void Update () {

        if (aparecer)
        {
            AparicionVentana();
        }

        if (desaparecer)
        {
            DesaparicionVentana();
        }

    }

    public void ActivarVentana()
    {
        gameObject.SetActive(true);
        aparecer = true;
    }

    public void DesactivarVentana()
    {
        desaparecer = true;
    }

    public void AparicionVentana()
    {
        if (GetComponent<CanvasGroup>().alpha < 1)
        {
            GetComponent<CanvasGroup>().alpha += 0.08f;
        }
        else
        {
            GetComponent<CanvasGroup>().interactable = true;
            aparecer = false;
        }
    }

    public void DesaparicionVentana()
    {
        if (GetComponent<CanvasGroup>().alpha > 0)
        {
            GetComponent<CanvasGroup>().alpha -= 0.08f;
        }
        else
        {
            gameObject.SetActive(false);
            GetComponent<CanvasGroup>().interactable = false;
            desaparecer = false;
        }
    }
}
