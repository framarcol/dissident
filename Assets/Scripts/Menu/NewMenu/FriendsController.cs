﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FriendsController : MonoBehaviour {

    public static string[] friendList;

    void Start()
    {
        friendList = new string[50];
        for (int i = 0; i < friendList.Length; i++) friendList[i] = "";
        if (!CargaIntro.modoOffline)
        {
            StartCoroutine(FriendListFinal());
        }  
    }

    public IEnumerator FriendListFinal()
    {
        WWWForm datos = new WWWForm();
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("nick", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("nick", Social.localUser.userName);
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("nick", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("nick", Social.localUser.userName);
        }
            
#endif

        WWW itemsData2;
        itemsData2 = new WWW("https://agapornigames.com/dodongo/checkFriendList.php", datos);

        yield return itemsData2;

        string[] resultado = itemsData2.text.Split(';');

        for(int i = 0; i < resultado.Length -1; i++)
        {
            friendList[i] = resultado[i];
        }


        
    }
}
