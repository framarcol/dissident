﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadText : MonoBehaviour {

    public Text[] misTextos;
    public int numeroDeCanvas;

    public bool updateInRuntime = true;

    public void Update()
    {
        if (!updateInRuntime) return;

        if (misTextos[1].text != SeleccionaIdiomas.gameText[numeroDeCanvas, 0 + 2] || misTextos[0].text != SeleccionaIdiomas.gameText[numeroDeCanvas, 0 + 1])
        {
            for (int i = 0; i < misTextos.Length; i++) misTextos[i].text = SeleccionaIdiomas.gameText[numeroDeCanvas, i + 1];
        }
        
    }
}
