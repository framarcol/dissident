﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadTextGamePlay : MonoBehaviour {

    public Text[] misTextos;
    public int numeroDeBloque;
    public int[] numeroDeLinea;

    public void Start()
    {
            for (int i = 0; i < misTextos.Length; i++) misTextos[i].text = SeleccionaIdiomas.gameText[numeroDeBloque, numeroDeLinea[i]];
    }
}
