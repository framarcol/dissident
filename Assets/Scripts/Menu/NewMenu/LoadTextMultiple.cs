﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadTextMultiple : MonoBehaviour {


    public Text[] misTextos;
    public int numeroDeBloque;
    public int[] numeroDeLinea;

    public void Update()
    {
        for (int i = 0; i < misTextos.Length; i++) misTextos[i].text = SeleccionaIdiomas.gameText[numeroDeBloque, numeroDeLinea[i]];
    }
}
