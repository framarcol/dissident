﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadTextUno : MonoBehaviour {

    public Text miTexto;
    public int numeroDeCanvas;

    public void Update()
    {
       miTexto.text = SeleccionaIdiomas.gameText[numeroDeCanvas, 1];
    }
}
