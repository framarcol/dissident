﻿using UnityEngine;
using System.Collections;
using System;

public class MoverCamaraAObjetivo : MonoBehaviour {

    public Vector3 posicionObjetivo;
    public float tiempo;

    public void MoverCamara()
    {
       if (CamaraMoviento.nuevoOrigen == false && CamaraMoviento.rotar == false)
       {
                CamaraMoviento.posicionObjetivo = posicionObjetivo;
                CamaraMoviento.timer = 0;
                CamaraMoviento.nuevoOrigen = true;
                CamaraMoviento.Velocidad = tiempo;
       }
       
    }

}
