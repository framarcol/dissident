﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoverPanelTienda : MonoBehaviour {


	Vector3 origen;
	Vector3 posInicial;
	Vector3 posFinal;
	public  Vector3 posicionObjetivo;
	public float Velocidad;
	bool nuevoOrigen;
	float timer;

	public Image[] imagePaneles;
	public GameObject[] panelesAMostrar;


	void Start()
	{
		origen = this.transform.localPosition;
		posInicial = this.transform.localPosition;
		posFinal = posicionObjetivo;
	}

	public void OnValueChanged(bool Value){

		if (Value) {
			for (int i = 0; i < imagePaneles.Length; i++)
				imagePaneles [i].color = new Color (imagePaneles [i].color.r, imagePaneles [i].color.g, imagePaneles [i].color.b, 1);
			timer = 0;
			posicionObjetivo = posFinal;
			nuevoOrigen = true;
		}
	}

	public void BtnMostrarPanel(GameObject panelAMostrar){

		for (int i = 0; i < panelesAMostrar.Length; i++) {

			panelesAMostrar [i].SetActive (false);

		}

		panelAMostrar.SetActive (true);


	}

	public void BtnIntroducirPanel(){
	    
		posicionObjetivo = posFinal;
		nuevoOrigen = true;
	
	}


	public void BtnSacarPanel(){

		for (int i = 0; i < imagePaneles.Length; i++)
			imagePaneles [i].color = new Color (imagePaneles [i].color.r, imagePaneles [i].color.g, imagePaneles [i].color.b, 0);
		posicionObjetivo = posInicial;
		timer = 0;
		nuevoOrigen = true;

	}

    public void BtnCambiarPanel(int idpanel)
    {
        for (int i = 0; i < panelesAMostrar.Length; i++) panelesAMostrar[i].gameObject.SetActive(false);

        panelesAMostrar[idpanel].SetActive(true);
    }


	void Update()
	{
		if (nuevoOrigen)  IrHaciaHito();
	}

	public void IrHaciaHito()
	{
		Vector3 destino = posicionObjetivo;

		Vector3 distancia = destino - origen;

		timer += Time.deltaTime;

		float porcentaje = timer / Velocidad;

		porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

		if(timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
		else
		{          
			origen = this.transform.localPosition;
			nuevoOrigen = false;
		}    

	}
}
