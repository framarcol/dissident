﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovimientoCamaraMenu : MonoBehaviour {

    public Animator lucesShop, tipaMenu;
    public Transform camara, laTipa2;
    public GameObject panelNiveles, panelOpciones, panelTienda, panelCreditos, panelIdolos, panelPrincipal;
    bool volandoVengo;
    Quaternion miCuaterbag;
    Quaternion cuaterbagMenu;
    Vector3 vectorTipaMenu, vectorTipaShop;
    public Toggle tBody;
    public static int[] recompensaPorEstrellas, idRecompensa, idTipoRecompensa;
    public static bool descargaIniciada;

    public Light miLus;

    public GameObject mesaHolograma;
    Vector3 posicionActualHolograma;
    public GameObject[] elementoSeleccionado;
    public GameObject panelAmigos, panelRanking, panelNivel, panelTutorial;

    public Animator popapInfo;
    public Text txtPopapInfo;

    public Button botonTienda, botonOptions, botonLevels;
    public int canvasTextoPopap;

    public CapsuleCollider coliderPremium;
    public GameObject PlatPremium;

    public GameObject popap;
    public Text textoPopap;
    int metodo;
    bool activarPopup, activarPopup2, activarFadeOut, apagar, encender,internet;
    public Toggle tgArmas;

    public GameObject btnTiendaObjeto;
    public GameObject popapComprarArmaEvento;
    public GameObject popapLoading;
    public GameObject popapConfirmarPermisosGoogle;
    public GameObject popapConfirmarLogueo;
    public GameObject mesaNiveles;
    public GameObject panelInformacion;
    public Text txtPopapConfirmarLogueo;
    public Text txtPopapConfirmarGoogle;
    public Text textoPopapLoading;
    public GameObject botonAceptarLoading;
    bool tiendaIniciada;
    public static int idAceptoGoogle;
    public bool oyeQueHayInternet;
    public int apagarSobre;
    public GameObject Publicidad;

    //ElementosApagarSobre
    public Text textoSobre;
    public Image imagenSobre;
    public Image imagenFondoSobre;
    public Button botonSobre;
    bool textoSobreB, imagenSobreB, imagenFondoSobreB;
    float textoSobreF, imagenSobreF, imagenFondoSobreF;
    float num, num2, num3;
    public CanvasGroup canvasEventos;

	public Text txtPopapDeseaHacerloAhora, txtYes, txtNo;

    void Awake()
    {
        ShopManager.Iniciar();
        CargaIntro.modoOffline = true;
        if (LoadSc.ejeY != -8.30f) panelTienda.SetActive(false);
        oyeQueHayInternet = false;
#if UNITY_EDITOR
      //  Caching.CleanCache();
#endif
        descargaIniciada = false;
       // panelTienda.SetActive(false);
        GameManager.Initialize();

        textoSobreF = textoSobre.color.a;
        imagenFondoSobreF = imagenFondoSobre.color.a;
        imagenSobreF = imagenSobre.color.a;

        recompensaPorEstrellas = new int[50];
        idTipoRecompensa = new int[50];
        idRecompensa = new int[50];

        if (GameDataController.gameData.bodySkinEquipped == 0)
        {
            GameDataController.gameData.bodySkinEquipped = 1;
        }

        if (GameDataController.gameData.weaponSkinEquipped == 0)
        {
            GameDataController.gameData.weaponSkinEquipped = 71;
        }

        if (GameDataController.gameData.skateSkinEquipped == 0)
        {
            GameDataController.gameData.skateSkinEquipped = 72;
        }
    }

    void Start()
    {
       // if(PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1) StartCoroutine(ComprobarEstadoTienda());   
        tiendaIniciada = false;
        posicionActualHolograma = mesaHolograma.transform.localPosition;
        activarFadeOut = false;
        activarPopup = false;
        metodo = 0;
        vectorTipaMenu = laTipa2.localPosition;
		cuaterbagMenu = laTipa2.localRotation;
     //   if (PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1) popapLoading.SetActive(true);
    }

    void OnApplicationQuit()
    {
        GameDataController.Save();
    }


    public IEnumerator ComprobarEstadoTienda(bool esBoton)
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null)
        {
            oyeQueHayInternet = true;
            StartCoroutine(IniciarTienda(esBoton));
        }
        else
        {
            if (ShopManager.checkShopCache())
            {
                StartCoroutine(IniciarTienda(esBoton));
            }
            else
            {
                popapLoading.SetActive(false);
            }

        }
    }

    public void AceptarPermisosTienda()
    {
        StartCoroutine(this.gameObject.GetComponent<ControladorId>().CheckInternetConnection(false));
        //StartCoroutine(ComprobarEstadoTienda());
              
        popapConfirmarPermisosGoogle.SetActive(false);
    }

    public void RechazarPemisosTienda()
    {
        popapConfirmarPermisosGoogle.SetActive(false);
    }

    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }

        GameDataController.gameData.premiumState = true;
        CargaIntro.modoOffline = true;
     //   if (camara.localPosition.x > 3) PlatPremium.SetActive(false);
     //   else  PlatPremium.SetActive(true);

        if (camara.localPosition.x < -2.5f)
        {       
            if (miLus.intensity > 0.02f)
            {
                miLus.intensity -= 0.05f;
            }
        }

        if (camara.localPosition.x > -2.5f)
        {           
            if (miLus.intensity < 1.05f)
            {
                miLus.intensity += 0.05f;
            }
        }

        if (camara.localPosition.y < -5 && camara.localPosition.y > -10) lucesShop.SetTrigger("Encender");

        if(elementoSeleccionado[0].transform.localPosition.y < 20 || elementoSeleccionado[6].transform.localPosition.y < 20 || elementoSeleccionado[5].transform.localPosition.y < 20 || elementoSeleccionado[1].transform.localPosition.y < -800 
            || elementoSeleccionado[2].transform.localPosition.y < -800 || camara.localPosition.x > 3.25f || canvasEventos.alpha > 0.95f) mesaHolograma.transform.localPosition = new Vector3(-100,-100,-100);
        else  mesaHolograma.transform.localPosition = posicionActualHolograma;      

        if(volandoVengo && camara.localPosition.x < 0.2 && camara.localPosition.x > -0.1 && camara.localPosition.y < 0.4 && camara.localPosition.y > -0.1)
        {
            
            volandoVengo = false;
            panelOpciones.SetActive(false);
            panelTienda.SetActive(false);
            panelCreditos.SetActive(false);
        }

        if (camara.localPosition.y < -80 || camara.localPosition.y > 80)
        {
            if (activarPopup)
            {
                popap.GetComponent<Animator>().SetTrigger("Fade");
                activarPopup = false;
            }
            panelPrincipal.SetActive(false);
        }
        else if (!panelPrincipal.activeInHierarchy) panelPrincipal.SetActive(true);

        if(camara.localPosition.x > 60 || camara.localPosition.x < 60)
        {
            if (activarPopup)
            {
                popap.GetComponent<Animator>().SetTrigger("Fade");
                activarPopup = false;
            }
        }

        if (activarPopup2)
        {
             popap.GetComponent<Animator>().SetTrigger("Fade");
             activarPopup2 = false;
        }

        if (activarFadeOut)
        {
            popap.GetComponent<Animator>().SetTrigger("FadeOut");
            activarFadeOut = false;
        }

        if (apagarSobre!= 0)
        {
            if(apagarSobre == 1)
            {
                if (!imagenSobreB)
                {
                    num = imagenSobre.color.a + 0.02f;
                    imagenSobre.color = new Color(imagenSobre.color.r, imagenSobre.color.g, imagenSobre.color.b, num);

                }

                if (!textoSobreB)
                {
                    num2 = textoSobre.color.a + 0.02f;
                    textoSobre.color = new Color(textoSobre.color.r, textoSobre.color.g, textoSobre.color.b, num2);
                }

                if (!imagenFondoSobreB)
                {
                    num3 = imagenFondoSobre.color.a + 0.02f;
                    imagenFondoSobre.color = new Color(imagenFondoSobre.color.r, imagenFondoSobre.color.g, imagenFondoSobre.color.b, num3);
                }



                if (num > imagenSobreF) imagenSobreB = true;
                if (num2 > textoSobreF) textoSobreB = true;
                if (num3 > imagenFondoSobreF) imagenFondoSobreB = true;


                if (imagenSobreB && textoSobreB && imagenFondoSobreB)
                {
                    botonSobre.interactable = true;
                    apagarSobre = 0;
                }
            }   
            
            if (apagarSobre == 2)
            {
                if (!imagenSobreB)
                {
                    num = imagenSobre.color.a - 0.02f;
                    imagenSobre.color = new Color(imagenSobre.color.r, imagenSobre.color.g, imagenSobre.color.b, num);

                }

                if (!textoSobreB)
                {
                    num2 = textoSobre.color.a - 0.02f;
                    textoSobre.color = new Color(textoSobre.color.r, textoSobre.color.g, textoSobre.color.b, num2);
                }

                if (!imagenFondoSobreB)
                {
                    num3 = imagenFondoSobre.color.a - 0.02f;
                    imagenFondoSobre.color = new Color(imagenFondoSobre.color.r, imagenFondoSobre.color.g, imagenFondoSobre.color.b, num3);
                }
           


                if (num < 0.1f) imagenSobreB = true;
                if (num2 < 0.1f) textoSobreB = true;
                if (num3 < 0.01f) imagenFondoSobreB = true;


                if(imagenSobreB && textoSobreB && imagenFondoSobreB)
                {
                    botonSobre.interactable = false;
                    apagarSobre = 0;
                }
                
            }        
        }
       
    }

    public void BtnOpciones(bool esVolver)
    {
        if (!esVolver) {
           // coliderPremium.enabled = false;
           // PlatPremium.SetActive(false);
            panelOpciones.SetActive(true);
        } 
        else
        {
           // PlatPremium.SetActive(true);
           // coliderPremium.enabled = true;
            volandoVengo = true;
            PlayerPrefs.Save();
        }

    }

    public void DeleteCache()
    {
        Caching.ClearCache();
    }

    public void BtnLevels(bool esVolver)
    {
        if (esVolver) volandoVengo = true;
    }

    public void ApagarLus()
    {
        textoSobreB = false;
        imagenSobreB = false;
        imagenFondoSobreB = false;
        apagarSobre = 2;
        apagar = true;
    }

    public void ActivarPopapArmaEvento()
    {
        if(PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1 || ShopManager.shopData != null)
        {
          //  camara.transform.localPosition = new Vector3(0, -8.30f, 0);
            camara.transform.localRotation = new Quaternion(0, 0, 0, 0);
            panelTienda.SetActive(true);
          //  tgArmas.isOn = true;
        } 
    }

    public void DesactivarPopapArmaEvento()
    {
        popapComprarArmaEvento.SetActive(false);
    }



    public void EncenderLus()
    {
        if (!mesaNiveles.GetComponent<WorldSelectorMap>().worldSelectorActive)
        {
            mesaNiveles.GetComponent<WorldSelectorMap>().InitWorldSelector();
        }
        
        encender = true;
        textoSobreB = false;
        imagenSobreB = false;
        imagenFondoSobreB = false;
        apagarSobre = 1;
    }

    public void IrDelTironALaTienda()
    {
        panelTienda.SetActive(true);
        btnTiendaObjeto.GetComponent<MoverCamaraAObjetivo>().MoverCamara();
    }

    public void BtnTienda(bool esVolver)
    {
        IrDelTironALaTienda();
        /* if (ShopManager.checkShopCache())
         {
             if (!esVolver)
             {
                 if (ShopManager.shopData == null)
                 {
                     StartCoroutine(ComprobarEstadoTienda(true));
                 }
                 else
                 {

                 }

             }
             else
             {
                 tBody.isOn = true;
                 laTipa2.localPosition = vectorTipaMenu;
                 laTipa2.localRotation = cuaterbagMenu;
                 tipaMenu.SetTrigger("Salir");
                 volandoVengo = true;
             }
         }
         else
         {
             StartCoroutine(ComprobarEstadoTienda(true));
         }*/
    }

    public void Popap2(string texto)
    {
        textoPopap.text = texto;
        activarPopup2 = true;
    }

    public void Popap(int idMetodo)
    {
        textoPopap.text = SeleccionaIdiomas.gameText[canvasTextoPopap-1, idMetodo];
        metodo = idMetodo;
        activarPopup = true;
    }

    public void PopapInicio()
    {
        if(GameDataController.gameData.stars[0,0] < 1)
        {
            textoPopap.text = SeleccionaIdiomas.gameText[canvasTextoPopap-1, 1];
            metodo = 0;
            activarPopup = true;
        }
    }

    public void PopapMetodo()
    {
       activarFadeOut = true;
    }

    public void BtnIdolos(bool esVolver)
    {
        if (!esVolver) {
            panelIdolos.SetActive(true);
        } 
        else volandoVengo = true;
    }

    public void BtnCreditos(bool esVolver)
    {
        if (!esVolver) {
            panelCreditos.SetActive(true);
            panelIdolos.SetActive(true);
        } 
        else volandoVengo = true;
    }

    public void BtnRanking()
    {
#if UNITY_IOS
        PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
#endif
        if (PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1)
        {
            if (ControladorId.Logueado)
            {
                panelRanking.SetActive(true);
                if (CargaIntro.modoOffline)
                {
                    textoPopap.text = SeleccionaIdiomas.gameText[20, 1];
                    popap.GetComponent<Animator>().SetTrigger("Fade");
                    panelRanking.SetActive(false);
                }
                else
                {
                    StartCoroutine(BtnRankingCheck());
                }
            }
            else
            {
                popapConfirmarLogueo.SetActive(true);
                txtPopapConfirmarLogueo.text = SeleccionaIdiomas.gameText[22, 11];
            }
           
        }
        else
        {
            idAceptoGoogle = 3;
            popapConfirmarPermisosGoogle.SetActive(true);
#if UNITY_ANDROID
            txtPopapConfirmarGoogle.text = SeleccionaIdiomas.gameText[22, 3];
#endif
#if UNITY_IOS
		txtPopapConfirmarGoogle.text = SeleccionaIdiomas.gameText[22, 7];
#endif
			txtPopapDeseaHacerloAhora.text = SeleccionaIdiomas.gameText[22, 13];
			txtYes.text = SeleccionaIdiomas.gameText[21, 26];
			txtNo.text = SeleccionaIdiomas.gameText[21, 25];
        }


    }

    public void BtnConfirmarLogueo()
    {
        StartCoroutine(this.gameObject.GetComponent<ControladorId>().CheckInternetConnection(false));
        popapConfirmarLogueo.SetActive(false);
    }

    public void BtnFriends()
    {
        if (PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1)
        {
            if (ControladorId.Logueado)
            {
                panelAmigos.SetActive(true);
                if (CargaIntro.modoOffline)
                {
                    textoPopap.text = SeleccionaIdiomas.gameText[20, 1];
                    popap.GetComponent<Animator>().SetTrigger("Fade");
                    panelAmigos.SetActive(false);
                }
                else
                {
                    StartCoroutine(BtnFriendsCheck());
                }
            }
            else
            {
                popapConfirmarLogueo.SetActive(true);
                txtPopapConfirmarLogueo.text = SeleccionaIdiomas.gameText[22,11];
            }
           
        }
        else
        {
            idAceptoGoogle = 4;
            popapConfirmarPermisosGoogle.SetActive(true);
#if UNITY_ANDROID
            txtPopapConfirmarGoogle.text = SeleccionaIdiomas.gameText[22, 4];
#endif
#if UNITY_IOS
		txtPopapConfirmarGoogle.text = SeleccionaIdiomas.gameText[22, 8];
#endif
			txtPopapDeseaHacerloAhora.text = SeleccionaIdiomas.gameText[22, 13];
			txtYes.text = SeleccionaIdiomas.gameText[21, 26];
			txtNo.text = SeleccionaIdiomas.gameText[21, 25];

        }


    }

    IEnumerator BtnFriendsCheck()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            textoPopap.text = SeleccionaIdiomas.gameText[20, 8];
            popap.GetComponent<Animator>().SetTrigger("Fade");
            panelAmigos.SetActive(false);
        }
        else
        {
            elementoSeleccionado[1].GetComponent<MovimientoPaneles>().BtnMostrarPanel();
            elementoSeleccionado[4].GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        }
    }

    IEnumerator BtnRankingCheck()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            textoPopap.text = SeleccionaIdiomas.gameText[20, 8];
            popap.GetComponent<Animator>().SetTrigger("Fade");
            panelRanking.SetActive(false);
        }
        else
        {
            elementoSeleccionado[2].GetComponent<MovimientoPaneles>().BtnMostrarPanel();
            elementoSeleccionado[3].GetComponent<MovimientoPaneles>().BtnMostrarPanel();
        }
    }

    IEnumerator IniciarTienda(bool esBoton)
    {

        botonAceptarLoading.SetActive(false);
        popapLoading.SetActive(true);

        if(ShopManager.shopData == null)
        {
            if (!ShopManager.checkShopCache() || oyeQueHayInternet) yield return ShopManager.Initialize();
            else yield return ShopManager.InitializeOffline();
        }       

        int estrellas = 0;

        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++) estrellas += GameDataController.gameData.stars[j, i];

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;

        ControladorId.totalEstrellas = estrellas;
        if (ShopManager.shopData != null)
        {
            int ultimaRecompensa = 0;
            int[] guardapor = new int[3];
            for(int z = 0, k = 0, m = 0; z < ShopManager.shopData.standardShopItems.Count; z++)
            {
                for (int i = 0; i < ShopManager.shopData.standardShopItems[z].standardItemList.Count; i++)
                {
                    for (int j = 0; j < ShopManager.shopData.standardShopItems[z].standardItemList[i].costPacks.Count; j++)
                    {
                        if (ShopManager.shopData.standardShopItems[z].standardItemList[i].costPacks[j].currency.ToString() == "STARS")
                        {
                            recompensaPorEstrellas[k] = ShopManager.shopData.standardShopItems[z].standardItemList[i].costPacks[j].cost;
                            idRecompensa[k] = ShopManager.shopData.standardShopItems[z].standardItemList[i].itemId;
                            idTipoRecompensa[k] = z;

                            if (ShopManager.shopData.standardShopItems[z].standardItemList[i].costPacks[j].cost <= estrellas)
                            {
                               for (int l = 0; l < 30; l++)

                                    if (ControladorId.ObjetoCompradosPorTipo[z, l] == 0)
                                    {
                                        ControladorId.ObjetoCompradosPorTipo[z, l] = (uint)ShopManager.shopData.standardShopItems[z].standardItemList[i].itemId;
                                        break;
                                    }    
                                
                            }

                            k++;
                            ultimaRecompensa = k;
                            guardapor[z] = ControladorId.limiteObjetos[z];
                        }
                    }
                }                
            }

            for (int i = 0, k = 0; i < ShopManager.shopData.shopItemSets.Count; i++)
            {
                for (int j = 0; j < ShopManager.shopData.shopItemSets[i].costPacks.Count; j++)
                {
                    if (ShopManager.shopData.shopItemSets[i].costPacks[j].currency.ToString() == "STARS")
                    {
                        recompensaPorEstrellas[ultimaRecompensa] = ShopManager.shopData.shopItemSets[i].costPacks[j].cost;
                        idRecompensa[ultimaRecompensa] = ShopManager.shopData.shopItemSets[i].itemId;
                        idTipoRecompensa[ultimaRecompensa] = 3;

                        if (ShopManager.shopData.shopItemSets[i].costPacks[j].cost <= estrellas)
                        {
                            ControladorId.ObjetoCompradosPorTipo[3, ControladorId.limiteObjetos[3]+k] = (uint)ShopManager.shopData.shopItemSets[i].itemId;
                            k++;
                        }

                        ultimaRecompensa++;
                    }
                }

            }

            AsignarSetsPremium();


            for (int i = 0; i < ShopManager.shopData.GetUpgrade(79).costPacks.Count; i++)
            {
                if(ShopManager.shopData.GetUpgrade(79).costPacks[i].currency.ToString() == "STARS")  if (ShopManager.shopData.GetUpgrade(79).costPacks[i].cost <= estrellas) GameDataController.gameData.worldsObtained[0] = true;
            }

            for (int i = 0; i < ShopManager.shopData.GetUpgrade(80).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetUpgrade(80).costPacks[i].currency.ToString() == "STARS") if (ShopManager.shopData.GetUpgrade(80).costPacks[i].cost <= estrellas) GameDataController.gameData.worldsObtained[1] = true;
            }

            popapLoading.SetActive(false);
            tiendaIniciada = true;
        }
        else
        {
            botonAceptarLoading.SetActive(true);
            textoPopapLoading.text = SeleccionaIdiomas.gameText[21, 19];
        }

        if (esBoton)
        {
            panelTienda.SetActive(true);
            btnTiendaObjeto.GetComponent<MoverCamaraAObjetivo>().MoverCamara();
        }
        
    }

    public void AsignarSetsPremium()
    {
        if (GameDataController.gameData.premiumState)
        {
            for (int i = 0; i < 40; i++)
            {
                if (ControladorId.ObjetoCompradosPorTipo[3, i] == 0)
                {
                    ControladorId.ObjetoCompradosPorTipo[3, i] = 63;
                    ControladorId.ObjetoCompradosPorTipo[3, i + 1] = 64;
                }
            }
            for (int i = 0; i < 40; i++)
            {
                if (ControladorId.ObjetoCompradosPorTipo[0, i] == 0)
                {
                    ControladorId.ObjetoCompradosPorTipo[0, i] = (uint)ShopManager.shopData.GetSet(63).linkedItemIds[0];
                    ControladorId.ObjetoCompradosPorTipo[0, i + 1] = (uint)ShopManager.shopData.GetSet(64).linkedItemIds[0];
                    break;
                }
            }

            for (int i = 0; i < 40; i++)
            {
                if (ControladorId.ObjetoCompradosPorTipo[1, i] == 0)
                {
                    ControladorId.ObjetoCompradosPorTipo[1, i] = (uint)ShopManager.shopData.GetSet(63).linkedItemIds[2];
                    ControladorId.ObjetoCompradosPorTipo[1, i + 1] = (uint)ShopManager.shopData.GetSet(64).linkedItemIds[2];
                    break;
                }
            }

            for (int i = 0; i < 40; i++)
            {
                if (ControladorId.ObjetoCompradosPorTipo[2, i] == 0)
                {
                    ControladorId.ObjetoCompradosPorTipo[2, i] = (uint)ShopManager.shopData.GetSet(63).linkedItemIds[1];
                    ControladorId.ObjetoCompradosPorTipo[2, i + 1] = (uint)ShopManager.shopData.GetSet(64).linkedItemIds[1];
                    break;
                }
            }
        }
    }

    public void BotonAceptarPanelLoading()
    {
        popapLoading.SetActive(false);
    }

    public void LoadingActivar()
    {
        popapLoading.SetActive(true);
    }
    public void LoadingDesactivar()
	{
		popapLoading.SetActive(false);
	}

    public void BtnInformacionUsuario()
    {
        panelInformacion.GetComponent<FadeInObjeto>().ActivarVentana();
    }

    public void BtnVolverInformacion()
    {
        panelInformacion.GetComponent<FadeInObjeto>().DesactivarVentana();
    }

}
