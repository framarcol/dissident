﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovimientoFlecha : MonoBehaviour {

    Transform plataforma;
    Vector3 posicionOriginal;
    Vector3 moverEjes;
    public float distancia;
    public float Velocidad;
    public string nombreFlecha;
    public bool ejeY;
    public bool ejeX;
    public bool condicion;
    bool hayFragmento;

    private void Start()
    {
#if UNITY_EDITOR
        PlayerPrefs.DeleteKey(nombreFlecha);
#endif
        if(PlayerPrefs.GetInt(nombreFlecha) == 1){

            gameObject.SetActive(false);

        }

        if (ejeX)
        {
            moverEjes = new Vector3(1, 0, 0);
        }

        if (ejeY)
        {
            moverEjes = new Vector3(0, 1, 0);
        }

        if (ejeX && ejeY)
        {
            moverEjes = new Vector3(1, 1, 0);
        }

        plataforma = GetComponent<Transform>();
        posicionOriginal = plataforma.position;
        hayFragmento = false;

        if (condicion)
        {
            int contador = 0;
            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    for (int k = 0; k < 10; k++) if (GameDataController.gameData.idols[i, j, k]) contador++;

                    if (contador == 10)
                    {
                        switch (i)
                        {
                            case 0:
                                hayFragmento = true;
                                break;
                        }
                    }
                    contador = 0;
                }
            }
        }

    }

    private void Update()
    {
        if (!condicion)
        {
            plataforma.position = posicionOriginal + moverEjes * Mathf.PingPong(Time.time * Velocidad, distancia);
            if (!GetComponentInParent<Button>().interactable) GetComponent<Image>().color = new Color(1, 1, 1, 0);
            else GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
        else
        {
            plataforma.position = posicionOriginal + moverEjes * Mathf.PingPong(Time.time * Velocidad, distancia);
            if (!hayFragmento) GetComponent<Image>().color = new Color(1, 1, 1, 0);
            else GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }

    }

    public void SinFlecha()
    {
        if (PlayerPrefs.GetInt(nombreFlecha) != 1)
        {
            GetComponentInParent<ParticleSystemMultiplier>().Explotar();
            GetComponentInParent<ExplosionPhysicsForce>().Explotar();
            gameObject.SetActive(false);
            PlayerPrefs.SetInt(nombreFlecha, 1);
        }

    }

}
