﻿using UnityEngine;
using System.Collections;

public class MovimientoPaneles : MonoBehaviour {

	Vector3 origen;
	Vector3 posInicial;
	Vector3 posFinal;
	public  Vector3 posicionObjetivo;
	public float Velocidad;
	bool nuevoOrigen, sacar;
	float timer;

	void Start()
	{
		origen = this.transform.localPosition;
		posInicial = this.transform.localPosition;
		posFinal = posicionObjetivo;
	}

	public void BtnMostrarPanel()
	{
        sacar = false;
		this.gameObject.SetActive (true);
		timer = 0;
		posicionObjetivo = posFinal;
		nuevoOrigen = true;
	}

	public void BtnSacarPanel()
	{
        sacar = true;
		posicionObjetivo = posInicial;
		timer = 0;
		nuevoOrigen = true;
	}

	void Update()
	{
		if (nuevoOrigen) IrHaciaHito();
	}

	public void IrHaciaHito()
	{
		Vector3 destino = posicionObjetivo;

		Vector3 distancia = destino - origen;

		timer += Time.deltaTime;

		float porcentaje = timer / Velocidad;

		porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

		if(timer < Velocidad) transform.localPosition = distancia * porcentaje + origen;
		else
		{          
			origen = this.transform.localPosition;
			nuevoOrigen = false;
			if(sacar) this.gameObject.SetActive(false);
		}    

	}
}
