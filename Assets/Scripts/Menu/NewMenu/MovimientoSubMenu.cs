﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovimientoSubMenu : MonoBehaviour {

    public GameObject miSensibili;
    
    public void Limites()
    {
       miSensibili.transform.localPosition = new Vector2(Mathf.Clamp(miSensibili.transform.localPosition.x, -6000, 100), 0);
    }
}
