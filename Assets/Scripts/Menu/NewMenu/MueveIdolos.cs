﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MueveIdolos : MonoBehaviour {


    Vector3 posicionOrigenObjeto;
    RaycastHit hitInfo;
    GameObject objetoAlcanzado;
    public Animator salaIdolos;
    public GameObject activarLuz;
    GameObject objetoRotacion;
    bool primero, activarRotacion, activarRotacionVuelta, activarVolver, activarMovimiento, seEstaMoviendo;
    public float Velocidad;
    bool menuLoreActivado;
    string hornacina, hornacinaGuardado;
    public Button btnMueveMolino;

    // Private variables
    private Transform thisTransform;

    // Nuevas variables
    float timer, timerRotacion;
    public Vector3 posicionObjetivo, posicionObjetivoYldon, posicionObjetivoMiraka;
    public Quaternion rotacionObjetivo;

  //  public static bool ActivarRotacionVuelta { get; set; }

    void Start()
    {
        primero = false;
        menuLoreActivado = false;
        hornacina = "HUD";
        hornacinaGuardado = "HUD";
    }


    void Update()
    {
        // Otro load text idolo
#if UNITY_EDITOR
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0) && !menuLoreActivado && btnMueveMolino.interactable == true && !seEstaMoviendo)
        {
            hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {

                if (hitInfo.transform.gameObject.tag == hornacina)
                {
                    objetoAlcanzado = hitInfo.transform.gameObject;
                    salaIdolos.SetTrigger("Aparecer");
                    posicionOrigenObjeto = objetoAlcanzado.transform.localPosition;
                    rotacionObjetivo = objetoAlcanzado.transform.localRotation;
                    activarVolver = false;
                    timer = 0;
                    activarMovimiento = true;
                    activarRotacionVuelta = false;
                    activarRotacion = true;
                    menuLoreActivado = true;
                    objetoAlcanzado.GetComponent<LoadTextIdolo>().CargarTexto();
                    thisTransform = objetoAlcanzado.transform;
                }
            }
        }
#else
        if (Input.touchCount > 0 && !menuLoreActivado && btnMueveMolino.interactable == true && !seEstaMoviendo)
        {
            hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
      
                if (hitInfo.transform.gameObject.tag == hornacina)
                {
                    objetoAlcanzado = hitInfo.transform.gameObject;
                    salaIdolos.SetTrigger("Aparecer");
                    posicionOrigenObjeto = objetoAlcanzado.transform.localPosition;
                    rotacionObjetivo = objetoAlcanzado.transform.localRotation;
                    activarVolver = false;
                    timer = 0;
                    activarMovimiento = true;
                    activarRotacionVuelta = false;
                    activarRotacion = true;
                    menuLoreActivado = true;
                    objetoAlcanzado.GetComponent<LoadTextIdolo>().CargarTexto();
                    thisTransform = objetoAlcanzado.transform;
                }
            }
        }   
#endif
        if (activarRotacion) objetoAlcanzado.transform.Rotate(Vector3.up, -Time.deltaTime * 24);

        if (activarRotacionVuelta) RotarHaciaHito();

        if (activarMovimiento)
        {
            seEstaMoviendo = true;
            switch (hornacina)
            {
                case "HUD":
                    IrHaciaElHito(posicionObjetivo, posicionOrigenObjeto);
                    break;
                case "NotBone":
                    IrHaciaElHito(posicionObjetivoYldon, posicionOrigenObjeto);
                    break;
                case "MasterLight":
                    IrHaciaElHito(posicionObjetivoMiraka, posicionOrigenObjeto);
                    break;
                case "Cuarto":
                    break;
            }
            
        }  
        
        if (activarVolver)
        {
            seEstaMoviendo = true;
            activarMovimiento = false;
            switch (hornacina)
            {
                case "HUD":
                    IrHaciaElHito(posicionOrigenObjeto, posicionObjetivo);
                    break;
                case "NotBone":
                    IrHaciaElHito(posicionOrigenObjeto, posicionObjetivoYldon);
                    break;
                case "MasterLight":
                    IrHaciaElHito(posicionOrigenObjeto, posicionObjetivoMiraka);
                    break;
                case "Cuarto":
                    break;
            }
            
        }

        if (!activarVolver && !activarMovimiento) seEstaMoviendo = false;
        

    }

    public void RotarHaciaHito()
    {
        Quaternion miRota = rotacionObjetivo;   //rotacionObjetivo.transform.localRotation;

        timerRotacion += Time.deltaTime;

        float porcentaje = timerRotacion / 10;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timerRotacion < 10)
        {
            thisTransform.localRotation = Quaternion.Lerp(objetoAlcanzado.transform.localRotation, miRota, porcentaje);
        }
        else
        {
            activarRotacionVuelta = false;
        }
    }

    public void IrHaciaElHito(Vector3 posOb, Vector3 posOr)
    {
        Vector3 destino = posOb;

        Vector3 distancia = destino - posOr;

        timer += Time.deltaTime;

        float porcentaje = timer / Velocidad;

        porcentaje = Mathf.Sin(porcentaje * Mathf.PI * 0.5f);

        if (timer < Velocidad)
        {
            thisTransform.localPosition = distancia * porcentaje + posOr;
        }
        else
        {
            activarVolver = false;
            activarMovimiento = false;
        }

    }

    public void btnVolver()
    {
       timer = 0;
       timerRotacion = 0;
       salaIdolos.SetTrigger("Desaparecer");
       activarVolver = true;
       menuLoreActivado = false;
       activarRotacion = false;
       activarRotacionVuelta = true;
       objetoRotacion = objetoAlcanzado;
    }

    public void CambiarNumHornacina()
    {
        if (hornacina == "HUD")
        {      
            hornacina = "NotBone";
            hornacinaGuardado = hornacina;
        }
        else if (hornacina == "NotBone")
        {
            hornacina = "MasterLight";
            hornacinaGuardado = hornacina;

        }else if(hornacina == "MasterLight")
        {
            hornacina = "Cuarto";
            hornacinaGuardado = hornacina;
        }else if(hornacina == "Cuarto")
        {          
            hornacina = "HUD";
            hornacinaGuardado = hornacina;
        }
        
    }
}
