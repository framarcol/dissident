﻿using UnityEngine;
using System.Collections;

public class NotificacionVista : MonoBehaviour {

	void OnEnable()
    {
        if(PlayerPrefs.GetInt(gameObject.name) != 1) EventosController.numNots--;
        PlayerPrefs.SetInt(gameObject.name, 1);  
    }
}
