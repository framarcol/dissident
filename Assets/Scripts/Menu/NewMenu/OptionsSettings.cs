﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsSettings : MonoBehaviour
{

    public Slider sliderSensibilidad;
    public Toggle[] tManos;
    public Toggle[] tModoSalto;
    public Toggle tSleepMode;
    public GameObject ambos;
    public GameObject restorePurchases;
    public GameObject controlUsuario;
    public GameObject manoUsuario, sensibilidad, sleepMode;
    void Start()
    {
#if UNITY_IOS
        //restorePurchases.SetActive(true);
      //  gameCenterAct.SetActive(true);
        controlUsuario.transform.localPosition = new Vector3(8.1f, 38, 0);
        manoUsuario.transform.localPosition = new Vector3(184.1f, -11.7f, 0);
        sensibilidad.transform.localPosition = new Vector3(252f, 30.6f, 0);
        sleepMode.transform.localPosition = new Vector3(-17.8f, -83f, 0);
       // playJuegosAct.SetActive(false);
#elif UNITY_ANDROID
      //  playJuegosAct.SetActive(true);
#endif

        sliderSensibilidad.value = PlayerPrefs.GetFloat(GameManager.JumpSensitivityPref);
        int hand = PlayerPrefs.GetInt(GameManager.HandPref);

        ActualizarModoSalto(PlayerPrefs.GetInt(GameManager.JumpOptionPref));

        if(hand == 0)
        {
            tManos[0].isOn = true;
            tManos[1].isOn = false;
        }
        else
        {
            tManos[0].isOn = false;
            tManos[1].isOn = true;
        }

        if (PlayerPrefs.GetInt(GameManager.ammoDisplayPref) == 0) tSleepMode.isOn = false;
        else tSleepMode.isOn = true;

    }

    public void ActualizarSensibilidad()
    {
        PlayerPrefs.SetFloat(GameManager.JumpSensitivityPref, sliderSensibilidad.value);
    }

    public void ActualizarMano(bool Value)
    {
        if (Value)
        {
            if (tManos[0].isOn)
            {
                PlayerPrefs.SetInt(GameManager.HandPref, 0);
            }
            else
            {
                PlayerPrefs.SetInt(GameManager.HandPref, 1);
            }
        }
       
        
    }

    public void ActualizarSleepMode (bool Value)
    {
        if (Value)
        {
           // Screen.sleepTimeout = SleepTimeout.SystemSetting;
            PlayerPrefs.SetInt(GameManager.ammoDisplayPref, 1);
        }
        else
        {
            //Screen.sleepTimeout = SleepTimeout.NeverSleep;
            PlayerPrefs.SetInt(GameManager.ammoDisplayPref, 0);
        }
        
    }

    public void ActualizarModoSalto(int salto)
    {
        if (salto == 1)
        {
            tModoSalto[0].isOn = true;
            tModoSalto[1].isOn = false;
        }
        else if (salto == 0)
        {
            tModoSalto[0].isOn = false;
            tModoSalto[1].isOn = true;
        }
    }

    public void ActualizarSalto(bool Value)
    {

        if (tModoSalto[0].isOn) PlayerPrefs.SetInt(GameManager.JumpOptionPref, 1);
        else {

          //  if (SystemInfo.supportsGyroscope)
                PlayerPrefs.SetInt(GameManager.JumpOptionPref, 0);
        //    else PlayerPrefs.SetInt(GameManager.JumpOptionPref, 1);
        } 

    }
}