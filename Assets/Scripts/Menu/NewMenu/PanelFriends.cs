﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelFriends : MonoBehaviour {

    public InputField nombreUsuario;
    public Text txtPopap;
    public Animator popap;
    public Toggle[] inputsSolicitudes;
    string textoSolicitud;
    public Toggle[] textosAmigos;
    string textoAmigo;
    string[] amigosSoliEnviadas;
    bool esEliminar;
    bool esActualizar;
    public Button botonActualizar;
    int contadorSolicitudesEnviadas;

    //Botones que se activan cuando tienes un nombre seleccionado
    public Image btnAceptarSolicitud, btnRechazarSolicitud, btnEliminarAmigo;



    //Botones para el popap confirmacion
    public int idMetodoPopap;

    public void IniciarPanel()
    {
        if (!CargaIntro.modoOffline)
        {
            esActualizar = false;
            amigosSoliEnviadas = new string[10];
            for (int i = 0; i < amigosSoliEnviadas.Length; i++) amigosSoliEnviadas[i] = "";
            StartCoroutine(FriendRequest());
        }
    }

    void Start()
    {
        idMetodoPopap = 0;
    }

    void Update()
    {
        for (int i = 0; i < inputsSolicitudes.Length; i++) if (inputsSolicitudes[i].isOn)  textoSolicitud = inputsSolicitudes[i].GetComponentInChildren<Text>().text;    
        for (int i = 0; i < textosAmigos.Length; i++) if (textosAmigos[i].isOn) textoAmigo = textosAmigos[i].GetComponentInChildren<Text>().text;      
    }

    public void EnviarSolicitud()
    {
        int cuentaNombre = 0;

        for (int i = 0; i < textosAmigos.Length; i++) if (textosAmigos[i].GetComponentInChildren<Text>().text.ToLower() == nombreUsuario.text.ToLower()) cuentaNombre = 1;   

        for (int i = 0; i < amigosSoliEnviadas.Length; i++) if(amigosSoliEnviadas[i] != "") if (amigosSoliEnviadas[i].ToLower() == nombreUsuario.text.ToLower()) cuentaNombre = 2;     

        for (int i = 0; i < inputsSolicitudes.Length; i++) if (inputsSolicitudes[i].GetComponentInChildren<Text>().text.ToLower() == nombreUsuario.text.ToLower()) cuentaNombre = 3;


        if (nombreUsuario.text.ToLower() == Social.localUser.userName.ToLower())
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 1];
            popap.SetTrigger("Fade");
        }
        else if (contadorSolicitudesEnviadas == 5)
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 2];
            popap.SetTrigger("Fade");
        }
        else if (cuentaNombre == 1)
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 3];
            popap.SetTrigger("Fade");
        }
        else if (cuentaNombre == 2){
            txtPopap.text = SeleccionaIdiomas.gameText[14, 4];
            popap.SetTrigger("Fade");
        }
        else if (cuentaNombre == 3)
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 5];
            popap.SetTrigger("Fade");
        }
        else StartCoroutine(checkUsername());
       
    }

    public void BtnAceptarSolicitud()
    {
        if (textoSolicitud != null) {
            StartCoroutine(AceptarFriend(textoSolicitud));
            textoSolicitud = null;
        } 
        else
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
            popap.SetTrigger("Fade");
        }
    }

    public void BtnActualizarListaSolicitudes()
    {
        esActualizar = true;
        botonActualizar.interactable = false;
        StartCoroutine(FriendRequest());
    }

    public void BtnEliminarFriend()
    {
        if (textoAmigo != null)
        {

            StartCoroutine(EliminarFriend(textoAmigo));
            esEliminar = true;
            textoAmigo = null;

        }
       else
       {
           txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
            popap.SetTrigger("Fade");
       }
    }

    public void BtnRechazarSolicitud()
    {
        if (textoSolicitud != null)
        {
            StartCoroutine(EliminarFriend(textoSolicitud));
            esEliminar = false;
            textoSolicitud = null;
        }
       else
       {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
           popap.SetTrigger("Fade");
        }
    }

    public IEnumerator checkUsername()
    {
        WWWForm datos = new WWWForm();

        datos.AddField("userName", nombreUsuario.text);
        datos.AddField("idGoogle", Social.localUser.userName);

        WWW itemsData2;

        itemsData2 = new WWW("https://agapornigames.com/dodongo/buscarUsuario.php", datos);

        yield return itemsData2;

        nombreUsuario.text = "";

        if(itemsData2.text == "1")
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 7];
            StartCoroutine(FriendRequest());
        }
        else if (itemsData2.text == "12")
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 9];
        }
        else
        {
            txtPopap.text = SeleccionaIdiomas.gameText[14, 8];
        }

        popap.SetTrigger("Fade");

    }

    public IEnumerator FriendRequest()
    {
        WWWForm datos2 = new WWWForm();
        datos2.AddField("user", Social.localUser.userName);

        WWW itemsData2;
        itemsData2 = new WWW("https://agapornigames.com/dodongo/checkFriendRequest.php", datos2);

        yield return itemsData2;

        contadorSolicitudesEnviadas = 0;

        string[] resultado = itemsData2.text.Split(';');

        if (resultado.Length > 1)
        {
            for (int i = 0; i < inputsSolicitudes.Length; i++) inputsSolicitudes[i].gameObject.SetActive(false);
            for (int i = 0, j = 0; i < resultado.Length - 2 && j <= 5; i += 2)
            {
                inputsSolicitudes[j].GetComponentInChildren<Text>().text = resultado[i];
                      
                if (resultado[i + 1] == "2")
                {
                    amigosSoliEnviadas[contadorSolicitudesEnviadas] = resultado[i];
                    contadorSolicitudesEnviadas++;
                   // inputsSolicitudes[j].gameObject.SetActive(true);
                  //  j++;
                    /* Sistema antiguo porel que los amigos de solicitudes enviadas aparecían con sombrita
                     * inputsSolicitudes[j].interactable = false;
                     Color miColor = inputsSolicitudes[j].GetComponentInChildren<Text>().color;
                     miColor.a = 0.7f;
                     inputsSolicitudes[j].GetComponentInChildren<Text>().color = miColor;*/
                }
                else
                {                   
                    inputsSolicitudes[j].gameObject.SetActive(true);
                    j++;
                }

                
            }
        }

        if (esActualizar)
        {
            yield return new WaitForSeconds(2);
            botonActualizar.interactable = true;
            esActualizar = false;
        }

        StartCoroutine(FriendList());

    }

    public IEnumerator AceptarFriend(string userD)
    {
        WWWForm datos2 = new WWWForm();
        datos2.AddField("nick", userD);
        datos2.AddField("userName", Social.localUser.userName);

        WWW itemsData2;
        itemsData2 = new WWW("https://agapornigames.com/dodongo/aceptarAmigo.php", datos2);

        yield return itemsData2;

        txtPopap.text = SeleccionaIdiomas.gameText[14, 10];

        if (!inputsSolicitudes[1].IsActive()) inputsSolicitudes[0].gameObject.SetActive(false);

        StartCoroutine(FriendRequest());

    }

    public IEnumerator FriendList()
    {
        WWWForm datos = new WWWForm();

        datos.AddField("nick", Social.localUser.userName);

        WWW itemsData2;
        itemsData2 = new WWW("https://agapornigames.com/dodongo/checkFriendList.php", datos);

        yield return itemsData2;

        string[] resultado = itemsData2.text.Split(';');

        for (int i = 0; i < textosAmigos.Length; i++) textosAmigos[i].gameObject.SetActive(false);

        for (int i = 0;  i < textosAmigos.Length && i < resultado.Length -1; i++)
        {
            FriendsController.friendList[i] = resultado[i];
            textosAmigos[i].gameObject.SetActive(true);
            textosAmigos[i].GetComponentInChildren<Text>().text = resultado[i];
        }

        textoAmigo = null;

    }

    public IEnumerator EliminarFriend(string userD)
    {
        WWWForm datos2 = new WWWForm();
        datos2.AddField("nick", userD);
        datos2.AddField("userName", Social.localUser.userName);

        WWW itemsData2;
        itemsData2 = new WWW("https://agapornigames.com/dodongo/deleteFriend.php", datos2);

        yield return itemsData2;

        if(esEliminar) txtPopap.text = SeleccionaIdiomas.gameText[14, 11];
        else txtPopap.text = SeleccionaIdiomas.gameText[14, 12];

        StartCoroutine(FriendRequest());

    }

    public void PopapActivar(int id)
    {
        idMetodoPopap = id;


        switch (id)
        {
            case 1:
                if(textoAmigo != null)
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 17] + " " + textoAmigo + " " + SeleccionaIdiomas.gameText[14, 18];
                    popap.SetTrigger("FadeConf");
                }
                else
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
                    popap.SetTrigger("Fade");
                }
                break;
            case 2:
                if(textoSolicitud != null)
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 13] + " " + textoSolicitud + " " + SeleccionaIdiomas.gameText[14, 14];
                    popap.SetTrigger("FadeConf");
                }
                else
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
                    popap.SetTrigger("Fade");
                }
               
                break;
            case 3:
                if (textoSolicitud != null)
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 15] + " " + textoSolicitud + " " + SeleccionaIdiomas.gameText[14, 16];
                    popap.SetTrigger("FadeConf");
                }
                else
                {
                    txtPopap.text = SeleccionaIdiomas.gameText[14, 6];
                    popap.SetTrigger("Fade");
                }
                break;
        }

    }

    public void PopapMetodo()
    {
        switch (idMetodoPopap)
        {
            case 1:
                BtnEliminarFriend();
                break;
            case 2:
                BtnRechazarSolicitud();
                break;
            case 3:
                BtnAceptarSolicitud();
                break;
        }

        popap.SetTrigger("FadeOutConf");
    }
}
