﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pestanas : MonoBehaviour {

    public GameObject[] opciones;
    public Toggle[] misTo;
    public Image[] img;

	public void btnPestanas()
    {
        for (int i = 0; i < opciones.Length; i++)
        {
            if (misTo[i].isOn) {

                img[i].color = new Color(1, 1, 1, 1);
                opciones[i].SetActive(true);
            }  
            else
            {
                img[i].color = new Color(1, 1, 1, 0); 
                opciones[i].SetActive(false);
            }
            
        }
       
    }
}
