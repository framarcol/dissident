﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class PlayEvent : UnityEvent { }

public class PlayButton : MonoBehaviour {

    public PlayEvent OnClick;
    RaycastHit hitInfo;

    void Update()
    {
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);


            if (hit)
            {
                if (hitInfo.transform.gameObject.tag == "MasterLight")
                {
                    OnClick.Invoke();
                }
            }
        }
    }

  }
