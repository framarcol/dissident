﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Purchasing;
using UnityEngine.UI;

public class Purchaser : MonoBehaviour
    {
      //  private static IStoreController m_StoreController;          // The Unity Purchasing system.
    //    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        public static string kProductIDNonConsumable = "ModoPremium";

        private static string kProductNameGooglePlayNonConsumable = "com.premium.mode_1";
	    private static string kProductNameGameCenterNonConsumable = "modo.premium2";


    public static string kProductIDConsumable = "100";
    public static string kProductIDConsumable2 = "200";
    public static string kProductIDConsumable3 = "550";

    private static string kProductNameGooglePlayConsumable = "diamantes.100";
    private static string kProductNameGooglePlayConsumable2 = "diamantes.200";
    private static string kProductNameGooglePlayConsumable3 = "diamantes.550";

    private static string kProductNameGameCenterConsumable = "diamantes.100";
    private static string kProductNameGameCenterConsumable2 = "diamantes.250";
    private static string kProductNameGameCenterConsumable3 = "diamantes.550";
    public static bool sinDiamantes;
    public static int dineroComprado;

    bool comprando;

   // Product product;

    string idProducto;
    public Animator Popap;
    public Text textoPopap;
    public Animator popapPremium;
    public GameObject palabraPremium;
    public Material materialPremium;
    public GameObject popapAcetarGoogle;
    public Text txtPopapAceptarGoogle;
    public Text txtPopapDeseaHacerloAhora, txtYes, txtNo;
    public GameObject popapLoading;
    public Text textoPopapLoaging;

    void Awake()
    {
        textoPopapLoaging.text = "";
        dineroComprado = 0;
        comprando = false;
        sinDiamantes = false;
    }

    void Start()
    {
        idProducto = "9";
        // If we haven't set up the Unity Purchasing reference
       /* if (m_StoreController == null)
        {
            if (!CargaIntro.modoOffline)
            {

                InitializePurchasing();

            }
        }*/
    }

    public void InitializePurchasing()
    {/*
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        // Continue adding the non-consumable product.
        builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable, new IDs() {
            { kProductNameGooglePlayNonConsumable, GooglePlay.Name },
            { kProductNameGameCenterNonConsumable, AppleAppStore.Name } });

        builder.AddProduct(kProductIDConsumable, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable, GooglePlay.Name },
            });

        builder.AddProduct(kProductIDConsumable2, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable2, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable2, GooglePlay.Name },
            });

        builder.AddProduct(kProductIDConsumable3, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable3, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable3, GooglePlay.Name },
            });

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
        */
    }


   /* private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
      //  return m_StoreController != null && m_StoreExtensionProvider != null;
    }*/

    public void BuyConsumable100()
    {
        popapLoading.SetActive(true);
        comprando = true;
        BuyProductID(kProductIDConsumable);
    }

    public void CambiaColorPremium()
    {
        if (GameDataController.gameData.premiumState)
        {
            palabraPremium.gameObject.SetActive(false);

            materialPremium.color = new Color(0.809f, 0.497f, 0.077f, 1);
        }
        else  materialPremium.color = new Color(0.161f, 0.161f, 0.776f, 1);
    }

    public void BuyConsumable250()
    {
        popapLoading.SetActive(true);
        comprando = true;
        idProducto = kProductIDConsumable2;
        BuyProductID(kProductIDConsumable2);
    }

    public void BuyConsumable550()
    {
        popapLoading.SetActive(true);
        comprando = true;
        idProducto = kProductIDConsumable3;
        BuyProductID(kProductIDConsumable3);
    }

    public void InfoCompra()
    {
        if (PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1)
        {
            if (!GameDataController.gameData.premiumState)
            popapPremium.SetTrigger("Fade");
        }
        else
        {
               MovimientoCamaraMenu.idAceptoGoogle = 2;
                popapAcetarGoogle.SetActive(true);
#if UNITY_ANDROID
                txtPopapAceptarGoogle.text = SeleccionaIdiomas.gameText[22, 2];
#endif
#if UNITY_IOS
		txtPopapAceptarGoogle.text = SeleccionaIdiomas.gameText[22, 6];
#endif
			txtPopapDeseaHacerloAhora.text = SeleccionaIdiomas.gameText[22, 13];
			txtYes.text = SeleccionaIdiomas.gameText[21, 26];
			txtNo.text = SeleccionaIdiomas.gameText[21, 25];
        }
    }

    public void BuyNonConsumable()
    {/*
        popapLoading.SetActive(true);
        comprando = true;
#if UNITY_IOS
        PlayerPrefs.SetInt("PermisosGoogleAceptados", 1);
#endif
        if (!CargaIntro.modoOffline && PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1)
        {
            if (!GameDataController.gameData.premiumState)
            {
            //    Product producto = m_StoreController.products.WithID(kProductIDNonConsumable);

                if (producto != null && producto.hasReceipt)
                {
                    popapLoading.SetActive(false);
                    textoPopap.text = SeleccionaIdiomas.gameText[20, 12];
                    Popap.SetTrigger("Fade");
                    GetComponent<EfectoCompras>().AsignarPremium2();
                    GameDataController.gameData.premiumState = true;
                    GameDataController.Save();
                    dineroComprado = 1000;
                    CambiaColorPremium();
                }
                else
                {
                    idProducto = kProductIDNonConsumable;
                    BuyProductID(kProductIDNonConsumable);
                }
            }
        }
        else
        {
            if (PlayerPrefs.GetInt("PermisosGoogleAceptados") != 1)
            {
                popapAcetarGoogle.SetActive(true);
#if UNITY_ANDROID
                txtPopapAceptarGoogle.text = SeleccionaIdiomas.gameText[22, 2];
#endif
#if UNITY_IOS
		txtPopapAceptarGoogle.text = SeleccionaIdiomas.gameText[22, 6];
#endif
				txtPopapDeseaHacerloAhora.text = SeleccionaIdiomas.gameText[22, 13];
				txtYes.text = SeleccionaIdiomas.gameText[21, 26];
				txtNo.text = SeleccionaIdiomas.gameText[21, 25];

            }
            else
            {
                textoPopap.text = SeleccionaIdiomas.gameText[20, 1];
                Popap.SetTrigger("Fade");
            }
        }
        */
    }

    void BuyProductID(string productId)
    {
       /* // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
      //      product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
        //        m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                textoPopap.text = SeleccionaIdiomas.gameText[20, 11];
                Popap.SetTrigger("Fade");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            textoPopap.text = SeleccionaIdiomas.gameText[20, 11];
            Popap.SetTrigger("Fade");
        }*/
    }

    public void RestorePurchases()
    {
        /*// If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            textoPopap.text = SeleccionaIdiomas.gameText[20, 3];
            Popap.SetTrigger("Fade");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {

            popapLoading.SetActive(true);
             // Fetch the Apple store-specific subsystem.
          //  var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                if (result)
                {
                    textoPopap.text = SeleccionaIdiomas.gameText[20, 15];
                    Popap.SetTrigger("Fade");
                    popapLoading.SetActive(false);
                }
                else
                {
                    textoPopap.text = SeleccionaIdiomas.gameText[20, 17];
                    Popap.SetTrigger("Fade");
                    popapLoading.SetActive(false);
                }
            });
        }
        else
        {
            textoPopap.text = "Nothing to restore";
            Popap.SetTrigger("Fade");
        }*/
    }

 /*   public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
        
    }*/


  /*  public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        textoPopap.text = SeleccionaIdiomas.gameText[20, 10] + " " + error.ToString();
        Popap.SetTrigger("Fade");
    }*/


   /* public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (comprando)
        {
            if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
            {
                GetComponent<EfectoCompras>().AsignarPremium2();
                GameDataController.gameData.premiumState = true;
                GameDataController.Save();
                textoPopap.text = SeleccionaIdiomas.gameText[20, 2];
                Popap.SetTrigger("Fade");
            }
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
            {
                textoPopapLoaging.text += "";
                GetComponent<EfectoCompras>().ActualizarDiamantes2(100);
            }
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
            {
                textoPopapLoaging.text += "";
                GetComponent<EfectoCompras>().ActualizarDiamantes2(250);
            }
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
            {
                textoPopapLoaging.text += "";
                GetComponent<EfectoCompras>().ActualizarDiamantes2(550);
            }
            else
            {
                textoPopap.text = SeleccionaIdiomas.gameText[20, 3];
                Popap.SetTrigger("Fade");
            }
        }*/

      /*  if (comprando)
        {
            return PurchaseProcessingResult.Pending;
        }
        else
        {
            return PurchaseProcessingResult.Complete;
        }*/
        

   // }

  /*  public void ProductoTerminado()
    {
       // popapLoading.SetActive(false);
       // m_StoreController.ConfirmPendingPurchase(product);
    }*/

   /* public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        popapLoading.SetActive(false);
        if (comprando)
        {
            textoPopap.text = SeleccionaIdiomas.gameText[20, 10] + " " + failureReason.ToString();
            Popap.SetTrigger("Fade");
        }
    }*/

}




