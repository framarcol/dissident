﻿using System;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Purchasing;
using UnityEngine.UI;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace CompleteProject
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class PurchaserDiamonds : MonoBehaviour
    {
      //  private static IStoreController m_StoreController;          // The Unity Purchasing system.
     //   private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        public static string kProductIDConsumable = "100";
        public static string kProductIDConsumable2 = "200";
        public static string kProductIDConsumable3 = "500";

        private static string kProductNameGooglePlayConsumable = "diamantes.100";
        private static string kProductNameGooglePlayConsumable2 = "diamantes.200";
        private static string kProductNameGooglePlayConsumable3 = "diamantes.500";

        private static string kProductNameGameCenterConsumable = "modo.premium2";
        private static string kProductNameGameCenterConsumable2 = "modo.premium2";
        private static string kProductNameGameCenterConsumable3 = "modo.premium2";

        public Animator Popap;
        public Text textoPopap;

        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
      /*      if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }*/
        }

        public void InitializePurchasing()
        {
          /*  // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            builder.AddProduct(kProductIDConsumable, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable, GooglePlay.Name },
            });

            builder.AddProduct(kProductIDConsumable2, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable2, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable2, GooglePlay.Name },
            });

            builder.AddProduct(kProductIDConsumable3, ProductType.Consumable, new IDs(){
                { kProductNameGameCenterConsumable3, AppleAppStore.Name },
                { kProductNameGooglePlayConsumable3, GooglePlay.Name },
            });

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);*/
        }


      /*  private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
       //     return m_StoreController != null && m_StoreExtensionProvider != null;
        }*/


        public void BuyConsumable100()
        {
            // Buy the consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            BuyProductID(kProductIDConsumable);
        }

        public void BuyConsumable250()
        {
            // Buy the consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            BuyProductID(kProductIDConsumable2);
        }

        public void BuyConsumable550()
        {
            // Buy the consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            BuyProductID(kProductIDConsumable3);
        }

    /*    public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            textoPopap.text = SeleccionaIdiomas.gameText[20, 10] + " " + error.ToString();
            Popap.SetTrigger("Fade");
        }*/

        void BuyProductID(string productId)
        {
           /* // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }*/
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {/*
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }*/
        }


        //  
        // --- IStoreListener
        //

     /*   public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
           // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
        }*/


      /*  public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
            {
                uint diamantesActual = GameDataController.gameData.diamondCurrency;
                GameDataController.gameData.diamondCurrency = 100 + diamantesActual;
            }
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
            {
                uint diamantesActual = GameDataController.gameData.diamondCurrency;
                GameDataController.gameData.diamondCurrency = 100 + diamantesActual;
            }
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
            {
                uint diamantesActual = GameDataController.gameData.diamondCurrency;
                GameDataController.gameData.diamondCurrency = 100 + diamantesActual;
            }
            else
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }*/

      /*  public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            textoPopap.text = SeleccionaIdiomas.gameText[20, 10] + " " + failureReason.ToString();
            Popap.SetTrigger("Fade");
        }*/

    }
}