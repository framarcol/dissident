﻿using UnityEngine;
using System.Collections;

public class RankingController : MonoBehaviour {


    public static string[,,] rankingValues;
    public static string[,,] rankingNames;
    public static string[] mePoints;
    public static bool rankingCargado;
    int numMundos;

    //PAra calcular el numero de niveles comprobados que subir
    int nivelesComprobados, mundosComprobados;
    string puntosSubir, nivelesSubir, mundosSubir;

    public GameObject[] rankingsFinales;

	void Start () {
        puntosSubir = "";
        nivelesSubir = "";
        mundosSubir = "";
        nivelesComprobados = 0;
        mundosComprobados = 0;
        numMundos = 3;
        rankingCargado = false;
        rankingValues = new string[20,50,50];
        rankingNames = new string[20, 50, 50];
        mePoints = new string[100];
        if (!CargaIntro.modoOffline && ControladorId.Logueado)
        {
            ComprobadorPuntuaciones();
        }       
	}

    public void ComprobadorPuntuaciones()
    {
#if UNITY_EDITOR
        //    GameDataController.gameData.stars[0, 2] = 2;
        //   GameDataController.gameData.playerRecords[0, 2] = 1002;
#endif

        // Debug.Log("Llega a comprobar");
        // Debug.Log(mundosComprobados + "/" + nivelesComprobados);
        bool activarRanking = false;
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            activarRanking = false;
        }
        else activarRanking = true;
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            activarRanking = false;
        }
        else activarRanking = true;
           
#endif
        if (activarRanking)
        {
            if (GameDataController.gameData.stars[mundosComprobados, nivelesComprobados] > 0)
            {

                //Debug.Log("Las estrellas si que me las detecta");
                if (GameDataController.gameData.playerRecords[mundosComprobados + 4, nivelesComprobados] < GameDataController.gameData.playerRecords[mundosComprobados, nivelesComprobados])
                {
                    Debug.Log("Guardo un mundo");
                    uint puntos = GameDataController.gameData.playerRecords[mundosComprobados, nivelesComprobados];
                    nivelesSubir += nivelesComprobados + "r";
                    mundosSubir += mundosComprobados + "r";
                    puntosSubir += puntos + "r";
                    SiguienteNivel();

                }
                else SiguienteNivel();
            }
            else SiguienteNivel();
        }
        else
        {
            StartCoroutine(CargarPuntuacion());
        }
       
    }

    public void SiguienteNivel()
    {

        if (nivelesComprobados < 14) {

            nivelesComprobados++;

        }
        else {

            mundosComprobados++;
            nivelesComprobados = 0;

        }

        if (mundosComprobados < numMundos) ComprobadorPuntuaciones();
        else {
#if UNITY_EDITOR
       /*   nivelesSubir ="3r";
            mundosSubir = "0r";
            puntosSubir = "1500r";
            Debug.Log("Subiendo");*/
#endif
            if(nivelesSubir != "")
            {
                StartCoroutine(SPuntuacion(nivelesSubir, puntosSubir, mundosSubir));
            }
            else
            {
                Debug.Log("Estoy en el else");
                StartCoroutine(CargarPuntuacion());
            }
           
        }
        
    }

    public IEnumerator SPuntuacion(string nivel, string puntuacion, string mundo)
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null)
        {
            WWW itemsData;
            WWWForm datos = new WWWForm();
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("id", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("id", Social.localUser.id.ToString());
            }
                
#endif

            datos.AddField("puntuacion", puntuacion);
            datos.AddField("nivel", nivel);
            datos.AddField("mundo", mundo);

            itemsData = new WWW("https://agapornigames.com/dodongo/subirPuntuacion2.php", datos);

            yield return itemsData;

            Debug.Log("Gola");

            string[] niveles = nivel.Split('r');
            string[] puntitos = puntuacion.Split('r');
            string[] munditos = mundo.Split('r');

            for(int i = 0; i < niveles.Length-1; i++)
            {
                GameDataController.gameData.playerRecords[int.Parse(munditos[i]) +4, int.Parse(niveles[i])] = uint.Parse(puntitos[i]);
            }

            GameDataController.Save();

            StartCoroutine(CargarPuntuacion());

        }        
    }

    public IEnumerator CargarPuntuacion()
    {
        Debug.Log("Cargando puntuacion...");
        WWW itemsData;

        itemsData = new WWW("https://agapornigames.com/dodongo/cargarPM12.php");

        yield return itemsData;

        string[] resultado = itemsData.text.Split(';');
       

        for (int j = 0; j < resultado.Length - 5; j += 5)
        {
            rankingNames[int.Parse(resultado[j+3]), int.Parse(resultado[j + 2]), int.Parse(resultado[j + 4])] = resultado[j];
            rankingValues[int.Parse(resultado[j + 3]), int.Parse(resultado[j + 2]), int.Parse(resultado[j + 4])] = resultado[j + 1];
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                if (resultado[j] == SystemInfo.deviceUniqueIdentifier) mePoints[int.Parse(resultado[j + 2])] = resultado[j + 1];
            }
            else
            {
                if (resultado[j] == Social.localUser.userName) mePoints[int.Parse(resultado[j + 2])] = resultado[j + 1];
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                if (resultado[j] == SystemInfo.deviceUniqueIdentifier) mePoints[int.Parse(resultado[j + 2])] = resultado[j + 1];
            }
            else
            {
                if (resultado[j] == Social.localUser.userName) mePoints[int.Parse(resultado[j + 2])] = resultado[j + 1];
            }
                
#endif
        }

        for(int i = 0; i < rankingsFinales.Length; i++)
        {
            rankingsFinales[i].GetComponent<CargarDatosPanelRanking>().Recargar();
        }

        rankingCargado = true;

    }

    public IEnumerator CargarPuntosJugador()
    {
        yield return 1;
    }

}
