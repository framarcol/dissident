﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RankingWorldYFriends : MonoBehaviour {


    public Text[] txtMundo1;
    public Text[] txtMundoP1;
    bool valido;
    public static bool esWorld;

    public Text textoFriend, textoMundo;

    void Start()
    {
        esWorld = true;
    }

    void Update()
    {
        if (esWorld)
        {
            textoMundo.color = new Color(1, 1, 1, 1);
            textoFriend.color = new Color(0, 0, 0, 1);
        }
        else
        {
            textoFriend.color = new Color(1, 1, 1, 1);
            textoMundo.color = new Color(0, 0, 0, 1);
        }
    }


    public void BtnWorld()
    {
        esWorld = true;
        for(int j = 0; j < txtMundo1.Length; j++)
        {
            txtMundo1[j].text = "";
            txtMundoP1[j].text = "";

            for (int i = 0; i < 15; i++)
            {
                //txtMundo1[j].text += RankingController.rankingNames[i, j] + "\n";
               // txtMundoP1[j].text += RankingController.rankingValues[i, j] + "\n";
            }
        }

    }

    public void BtnFriend()
    {
        esWorld = false;
        StartCoroutine(CargarPuntuacion());
    }

    public IEnumerator CargarPuntuacion()
    {
        WWW itemsData;
        WWWForm datos = new WWWForm();
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.userName);
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.userName);
        }
       
#endif


        itemsData = new WWW("https://agapornigames.com/dodongo/cargarPM12F.php", datos);

        yield return itemsData;

        string[] resultado = itemsData.text.Split(';');

        int num = 0;
        int num2 = 1;

  
        for(int i = 0; i < txtMundo1.Length; i++)
        {
            txtMundo1[i].text = "";
            txtMundoP1[i].text = "";
        }


        for (int j = 0, k = 0; j < resultado.Length - 3; j += 3)
        {
            num = int.Parse(resultado[j + 2]);
            if (num2 != num) k = 0;
            if (num < 16)
            {
                txtMundo1[num - 1].text += resultado[j] + "\n";
                txtMundoP1[num - 1].text += resultado[j + 1] + "\n";
                k++;
            }          

            num2 = num;
        }

    }
}
