﻿using UnityEngine;
using System.Collections;

public class RotarCamara : MonoBehaviour {

    public GameObject rotacionObjetivo;
    public float tiempo;

    public void MoverCamara()
    {
        if(CamaraMoviento.rotar == false)
        {
            CamaraMoviento.rotacionObjetivo = rotacionObjetivo.transform.localRotation;
            CamaraMoviento.timer = 0;
            CamaraMoviento.rotar = true;
            CamaraMoviento.Velocidad = tiempo;
        }

    }
}
