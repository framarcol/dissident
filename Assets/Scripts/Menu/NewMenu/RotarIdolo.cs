﻿using UnityEngine;
using System.Collections;

public class RotarIdolo : MonoBehaviour {

    private Transform thisTransform;
    bool activarRotacionVuelta;
    void Start()
    {
        thisTransform = transform;
    }

    void Update()
    {
        if (activarRotacionVuelta)
        {
            if ((transform.localRotation.y > -0.999 && transform.localRotation.y < -0.998) || (transform.localRotation.y < 0.999 && transform.localRotation.y > 0.998))
            {
                activarRotacionVuelta = false;
            }
            else
            {
                transform.Rotate(Vector3.up, -Time.deltaTime * 200);
            }
        }
    }
}
