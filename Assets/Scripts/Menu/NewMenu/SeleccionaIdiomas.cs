﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class SeleccionaIdiomas : MonoBehaviour {


    public static string[,] gameText = new string[100, 1000];

    void Awake() {

        ActualizaIdioma();
    }

    public static void ActualizaIdioma()
    {
        TextAsset lector;

        if (PlayerPrefs.HasKey(GameManager.LanguagePref))
        {
            if (PlayerPrefs.GetString(GameManager.LanguagePref) == "Spanish" 
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "German" 
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "French" 
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "Portuguese" 
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "English"
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "Chinese"
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "ChineseSimplified"
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "ChineseTraditional"
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "Japanese"
                || PlayerPrefs.GetString(GameManager.LanguagePref) == "Italian")
            {
                lector = Resources.Load("Idiomas/" + PlayerPrefs.GetString(GameManager.LanguagePref)) as TextAsset;
            }
            else
            {
                lector = Resources.Load("Idiomas/" + "English") as TextAsset;
            }

        }
        else
        {
            string idioma = Application.systemLanguage.ToString();

            if(idioma == "Spanish" || idioma == "German" 
                || idioma == "French" || idioma == "Portuguese" 
                || idioma == "English" || idioma == "Chinese" 
                || idioma == "Japanese" || idioma == "ChineseSimplified" 
                || idioma == "ChineseTraditional" || idioma == "Italian")
            {
                lector = Resources.Load("Idiomas/" + idioma) as TextAsset;
            }
            else
            {
                lector = Resources.Load("Idiomas/" + "English") as TextAsset;
            }
            
        }


        string[] guardaBloques = lector.text.Split('{');

        for (int i = 0; i < guardaBloques.Length; i++)
        {
            string[] guardarPalabras = guardaBloques[i].Split('\n');

            for (int j = 1; j < guardarPalabras.Length - 1; j++)
            {
                gameText[i, j-1] = guardarPalabras[j];
            }
            
        }

    }

    }
