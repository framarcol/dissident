﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SistemaPublicitario : MonoBehaviour {

    public Animator popapTienda, popapPremium, popapValoranos, popapYldoon, popapMirakka, popapYldoon2, popapMirakka2;
    public GameObject popapConfirmarMiraka, popapConfirmarYldon, popapNoMoneyMiraka, popapNoMoneyYldon;

    public GameObject panelTiendaDeComprar;


    string tutorialCompletado = "TutorialCompletado";
    string nivel5Completado = "Nivel5Completado";
    public GameObject btnTiendaObjeto;

    public Animator popapInfo;
    public Text txtPopapInfo;
	
    void Start()
    {
        int total = PlayerPrefs.GetInt("MuertesPu") + PlayerPrefs.GetInt("NivelesConseguidosPu");

        //Debug.Log(total);

        if (!PlayerPrefs.HasKey("A1"))
        {
            PlayerPrefs.SetInt("A1", 0);
        }
        if (!PlayerPrefs.HasKey("A2"))
        {
            PlayerPrefs.SetInt("A2", 0);
        }

        if (!GameDataController.gameData.premiumState)
        {
            if (total > 10 && total < 15 && PlayerPrefs.GetInt("A1") == 0)
            {
                popapTienda.SetTrigger("Fade");
                PlayerPrefs.SetInt("A1", 1);
            }
            else if (total > 14 && total < 35 && PlayerPrefs.GetInt("A2") == 0)
            {
                popapPremium.SetTrigger("Fade");
                PlayerPrefs.SetInt("A2", 1);
            }
            else if (total > 34)
            {
                popapValoranos.SetTrigger("Fade");
                PlayerPrefs.SetInt("A1", 0);
                PlayerPrefs.SetInt("A2", 0);
                PlayerPrefs.SetInt("MuertesPu", 0);
                PlayerPrefs.SetInt("NivelesConseguidosPu", 0);
                PlayerPrefs.SetInt("muertesPubli", 0);
                PlayerPrefs.SetInt("muertesPubli2", 0);

                for(int i = 5; i <= 35; i += 5)
                {
                    PlayerPrefs.SetInt("Publi" + i, 0);
                }
            }
        }
    }

    public void BtnActivarPopap(int id)
    {
        switch (id)
        {
            case 0:
                popapYldoon.SetTrigger("Fade");
                break;
            case 1:
                popapMirakka.SetTrigger("Fade");
                break;
            case 2:
                popapYldoon2.SetTrigger("Fade");
                break;
            case 3:
                popapMirakka2.SetTrigger("Fade");
                break;
            case 4:
                popapPremium.SetTrigger("Fade");
                break;
            case 5:
                popapValoranos.SetTrigger("Fade");
                break;
            case 6:
                popapTienda.SetTrigger("Fade");
                break;
        }
    }

    public void BtnUnlock()
    {
        MovimientoCamaraMenu.descargaIniciada = true;
        popapYldoon.SetTrigger("FadeOut");
        popapYldoon2.SetTrigger("Fade");
    }

    public void BtnUnlockMiraka()
    {
        MovimientoCamaraMenu.descargaIniciada = true;
        popapMirakka.SetTrigger("FadeOut");
        popapMirakka2.SetTrigger("Fade");
    }

    public void BtnPremium()
    {
        popapYldoon2.SetTrigger("FadeOut");
        popapPremium.SetTrigger("Fade");
    }

    public void BtnQuitarPremium()
    {
        MovimientoCamaraMenu.descargaIniciada = false;
        popapPremium.SetTrigger("FadeOut");
    }

    public void BtnPremiumMiraka()
    {
        popapMirakka2.SetTrigger("FadeOut");
        popapPremium.SetTrigger("Fade");
    }

    public void PopapConfirmnarYldon()
    {
        if(GameDataController.gameData.diamondCurrency >= ShopManager.shopData.GetUpgrade(79).costPacks[0].cost)
        {
            popapConfirmarYldon.SetActive(true);
        }
        else
        {
            popapNoMoneyYldon.SetActive(false);
        }
        
    }

    public void PopapConfirmarMiraka()
    {
        if (GameDataController.gameData.diamondCurrency >= ShopManager.shopData.GetUpgrade(80).costPacks[0].cost)
        {
            popapConfirmarMiraka.SetActive(true);
        }
        else
        {
            popapNoMoneyMiraka.SetActive(false);
        }
    }

    public void BtnRechazarCompraYldon()
    {
       popapConfirmarYldon.SetActive(false);
     //  popapYldoon2.SetTrigger("FadeOut");
    //    MovimientoCamaraMenu.descargaIniciada = false;
    }

    public void BtnRechazarComprarMiraka()
    {
        popapConfirmarMiraka.SetActive(false);
       // popapMirakka2.SetTrigger("FadeOut");
     //   MovimientoCamaraMenu.descargaIniciada = false;
    }

    public void BtnOkMiraka()
    {
        popapNoMoneyMiraka.SetActive(false);
    }

    public void BtnOkYldoon()
    {
        popapNoMoneyYldon.SetActive(false);
    }

    public void ActivarMesaHolograma()
    {
        MovimientoCamaraMenu.descargaIniciada = false;
    }

    public void ComprarMiraka()
    {
        if(GameDataController.gameData.diamondCurrency > ShopManager.shopData.GetUpgrade(80).costPacks[0].cost)
        {
            StartCoroutine(ComprarMirakaInternet());
        }
        popapMirakka2.SetTrigger("FadeOut");
       
    }

    public void ComprarYldoon()
    {
        if (GameDataController.gameData.diamondCurrency > ShopManager.shopData.GetUpgrade(80).costPacks[0].cost)
        {
            StartCoroutine(ComprarYldoonInternet());
        }
        popapYldoon2.SetTrigger("FadeOut");
    }

    public void BtnValorar()
    {
        popapValoranos.SetTrigger("FadeOut");
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/apps/testing/com.agapornigames.dissident");
#elif UNITY_IOS
        Application.OpenURL("https://itunes.apple.com/app/dissident/id1201277611?mt=8");
#endif

    }

    public void BtnGoToShop()
    {
        popapTienda.SetTrigger("FadeOut");
        panelTiendaDeComprar.SetActive(true);
        btnTiendaObjeto.GetComponent<MoverCamaraAObjetivo>().MoverCamara();
    }

    public IEnumerator ComprarYldoonInternet()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21,16];
        }
        else
        {
     
            //GameDataController.gameData.diamondCurrency -= (uint) ;
            GameDataController.gameData.worldsObtained[0] = true;

            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
        
#endif

            datos.AddField("idItem", 79);
            datos.AddField("diamantes", ShopManager.shopData.GetUpgrade(79).costPacks[0].cost);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;

            GameDataController.gameData.diamondCurrency = diBase;
            GameDataController.Save();
            MovimientoCamaraMenu.descargaIniciada = false;
        }
    }

    public IEnumerator ComprarMirakaInternet()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {

           // GameDataController.gameData.diamondCurrency -= (uint);
            GameDataController.gameData.worldsObtained[1] = true;

            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
       
#endif

            datos.AddField("idItem", 80);
            datos.AddField("diamantes", ShopManager.shopData.GetUpgrade(80).costPacks[0].cost);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;


            GameDataController.gameData.diamondCurrency = diBase;
            GameDataController.Save();

            MovimientoCamaraMenu.descargaIniciada = false;
        }
    }
}
