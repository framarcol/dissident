﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SistemaPublicitarioGamePlay : MonoBehaviour {

    int estrellas;
    int muertes, partidas, pA, pUM;
    public Animator popapRecompensaTienda, popapUpgrades;

    public Animator popapInfo;
    public Text txtPopapInfo;

    public GameObject[] idolos; 

    public Animator popapPremium, popapYldoon, popapMirakka, popapYldoon2, popapMirakka2, popapIdoloConseguido;
    public GameObject popapConfirmarMiraka, popapConfirmarYldon, popapNoMoneyMiraka, popapNoMoneyYldon;
    public GameObject popapPremiumG, popapYldoonG, popapMirakkaG, popapYldoon2G, popapMirakka2G, popapIdoloConseguidoG, popapInfoG, popapRecompensaTiendaG, popapUpgradesG;
    public GameObject popapQualdrom, popapHUDAMMO;

    public Text txtEstrellasPopapMiraka, txtDiamantesPopapMiraka, txtEstrellasPopapYldon, txtDiamantesPopapYldon, txtEstrellasObjetoConseguido;
    public Image imgObjetoConseguido, imgObjetoConseguido2, imgObjetoConseguido3;

    public Sprite[] imagenesElementos;
    public Sprite[] imagenesElementosSet;

    public GameObject pTUM, pTM, pTUY, pTY, imagenesExtra;

    bool popapMostrado;
    public bool hayPublicidad;
    public static bool esIntro;
    public GameObject btnContinuar;

    int nunMundo = 4, numIdolosPorMundo = 9, numFragmentos = 10;

    public GameObject popapConfirmarDescargaSnow, popapConfirmarDescargaDesert, popapLoading;

    public int[] recompensaPorEstrellas;
    public int[] idTipoRecompensa;

    void Start () {


        recompensaPorEstrellas = new int[] { 1, 2, 5, 10, 15, 25, 35, 50, 70, 100, 125, 150};
        idTipoRecompensa = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};

        if (!PlayerPrefs.HasKey("QuaPu"))
        {
            PlayerPrefs.SetInt("QuaPu", 0);
        }

        if (!PlayerPrefs.HasKey("HUDPu"))
        {
            PlayerPrefs.SetInt("HUDPu", 0);
        }

        StartCoroutine(CargaBoton());
        GameDataController.Initialize();
#if UNITY_ANDROID
        hayPublicidad = true;
#elif UNITY_IOS
        hayPublicidad = true;
#elif UNITY_EDITOR
        hayPublicidad = false;
#endif

        if (GameManager.activeCheckpoint != -1) hayPublicidad = false;

    //    Debug.Log(esIntro);
        if (hayPublicidad && !esIntro) {
             IniciarPublicidad();
        } 
        else
        {
            esIntro = false;   
            SceneManager.LoadScene("Publicidad");
        }
    }

    public IEnumerator CargaBoton()
    {
        yield return new WaitForSeconds(0.5f);

        btnContinuar.SetActive(true);
    }

    void IniciarPublicidad()
    {
       // Debug.Log("IniciarPublicidad");
        partidas = PlayerPrefs.GetInt("MuertesPu") + PlayerPrefs.GetInt("NivelesConseguidosPu");
        muertes = PlayerPrefs.GetInt("MuertesPu");

        if (!CambiaAmmoHUD())
        {
            if (!IrAQualdrom())
            {
                if (!PublicidadIdoloConseguido())
                {
                    if (!PublicidadObjetosConseguidos())
                    {
                        if (!IrATiendaPorqueTeMatanMucho())
                        {
                            if (!PublicidadYldonMirakaPremium())
                            {
                                SceneManager.LoadScene("Publicidad");
                            }
                        }
                    }
                }
            }
        }
        else
        {
               PublicidadAmmoHudMetodo();       
        }

        txtDiamantesPopapMiraka.text = "";
        txtDiamantesPopapYldon.text = "";
        txtEstrellasPopapMiraka.text = "";
        txtEstrellasPopapYldon.text = "";
    }

    public void IniciarPublicidad2()
    {
        if (!IrAQualdrom())
        {
            if (!PublicidadIdoloConseguido())
            {

                if (!PublicidadObjetosConseguidos())
                {

                    if (!IrATiendaPorqueTeMatanMucho())
                    {

                        if (!PublicidadYldonMirakaPremium())
                        {
                            SceneManager.LoadScene("Publicidad");
                        }
                    }
                }
            }
        }

        txtDiamantesPopapMiraka.text = "";
        txtDiamantesPopapYldon.text = "";
        txtEstrellasPopapMiraka.text = "";
        txtEstrellasPopapYldon.text = "";
    }

    public void MantenerMun()
    {
        popapHUDAMMO.SetActive(false);
        IniciarPublicidad();
    }

    public void CambiarMun()
    {
        PlayerPrefs.SetInt(GameManager.ammoDisplayPref, 1);
        popapHUDAMMO.SetActive(false);
        IniciarPublicidad();
    }

    public void PublicidadAmmoHudMetodo()
    {
        if (GameDataController.gameData.specialLevelsCompleted[3])
        {
            PlayerPrefs.SetInt("HUDPu", 1);
            popapHUDAMMO.SetActive(true);
        }
        else
        {
            IniciarPublicidad2();
        }
    }

    public bool CambiaAmmoHUD()
    {
        if (PlayerPrefs.GetInt("HUDPu") == 0)
            return true;
        else return false;
    }

    public bool PublicidadObjetosConseguidos()
    {
        estrellas = 0;

        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++) estrellas += GameDataController.gameData.stars[j, i];

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++; ;

#if UNITY_EDITOR
             estrellas = 150;
             //for(int i = 0; i < 150; i++) PlayerPrefs.DeleteKey(i.ToString());
             
#endif

        int pos = 0;
        int numero = 0;
        int i2 = 0;

        for (int i = 0; i < recompensaPorEstrellas.Length; i++)
        {
            if (estrellas >= recompensaPorEstrellas[i] && recompensaPorEstrellas[i] != 0)
            {
                if (!PlayerPrefs.HasKey(recompensaPorEstrellas[i].ToString()))
                {
                    popapMostrado = true;
                    switch (idTipoRecompensa[i])
                    {
                        case 0:
                            txtEstrellasObjetoConseguido.text = estrellas.ToString();
                           
                            for (i2 = 0; i2 < imagenesElementos.Length; i2++)
                            {
                                numero = int.Parse(imagenesElementos[i2].name);
                                if (numero == recompensaPorEstrellas[i]) pos = i2;
                            }
                           
                            imgObjetoConseguido.sprite = imagenesElementos[pos];
                            imagenesExtra.SetActive(false);
                            break;
                        case 1:
                            txtEstrellasObjetoConseguido.text = estrellas.ToString();

                            int[] ids = new int[3];

                            imagenesExtra.SetActive(true);
                            for(i2 = 0; i2 < imagenesElementosSet.Length; i2++)
                            {
                                numero = int.Parse(imagenesElementos[i2].name);
                                if (numero == (recompensaPorEstrellas[i] * 10) + 1) ids[0] = i2;
                                if (numero == (recompensaPorEstrellas[i] * 10) + 2) ids[1] = i2;
                                if (numero == (recompensaPorEstrellas[i] * 10) + 3) ids[2] = i2;
                            }

                            imgObjetoConseguido.sprite = imagenesElementosSet[ids[0]];
                            imgObjetoConseguido2.sprite = imagenesElementosSet[ids[1]];
                            imgObjetoConseguido3.sprite = imagenesElementosSet[ids[1]];
                            break;
                    }                 
                    popapRecompensaTiendaG.SetActive(true);
                    PlayerPrefs.SetInt(recompensaPorEstrellas[i].ToString(), 2);
                    return true;
                  
                }

            }
        }

        return false;
    }

    public bool PublicidadIdoloConseguido()
    {
#if UNITY_EDITOR
//        LoadSc.worldSelected = 0;
#endif
        if (LoadSc.worldSelected <= 0) LoadSc.worldSelected = 0;
        int contador = 0;
        for (int j = 0; j < numIdolosPorMundo; j++)
        {
            for (int k = 0; k < numFragmentos; k++) if (GameDataController.gameData.idols[LoadSc.worldSelected, j, k]) contador++;

            int numFinal2 = (LoadSc.worldSelected * 9) + j;
#if UNITY_EDITOR
        //   contador = 10;
     //      PlayerPrefs.DeleteKey("IdolCon" + numFinal2);
#endif
            if (contador == numFragmentos && PlayerPrefs.GetInt("IdolCon" + numFinal2) != 1)
            {
                
                PlayerPrefs.SetInt("IdolCon" + numFinal2, 1);
                idolos[numFinal2].SetActive(true);
                idolos[numFinal2].GetComponent<LoadTextIdolo>().CargarTexto();
             //   popapIdoloConseguido.SetTrigger("Fade");
                popapIdoloConseguidoG.SetActive(true);
                return true;
            }
            contador = 0;
        }

        return false;
    }

    public bool IrAQualdrom()
    {
#if UNITY_EDITOR
        GameDataController.gameData.stars[0, 0] = 0;
        GameDataController.gameData.specialLevelsCompleted[4] = true;
     //   PlayerPrefs.DeleteKey("QuaPu");
#endif
        if(GameDataController.gameData.stars[0,0] == 0)
        {
            if (GameDataController.gameData.specialLevelsCompleted[4])
            {
                if(PlayerPrefs.GetInt("QuaPu") != 1)
                {
                    popapQualdrom.SetActive(true);
                    PlayerPrefs.SetInt("QuaPu", 1);
                    return true;
                }          
            }
        }

        return false;

    }

    public void IrAQ1()
    {
        LoadSc.nextScene = 9;
        LoadSc.normalScene = true;
        LoadSc.fromBundle = false;
        LoadSc.specialNextLevelScene = false;
        LoadSc.specialPrevLevelScene = false;
        LoadSc.worldSelected = 0;
        SceneManager.LoadScene("Publicidad");
    }

    public bool IrATiendaPorqueTeMatanMucho()
    {
#if UNITY_EDITOR
    //    muertes = 40;
#endif
        if (GameDataController.gameData.lifeUpgradeEquipped < 1 && GameDataController.gameData.ammoUpgradeEquipped < 1)
        {
            if (muertes > 10 && PlayerPrefs.GetInt("muertesPubli") != 1)
            {
                popapMostrado = true;
                //popapUpgrades.SetTrigger("Fade");
                popapUpgradesG.SetActive(true);
                PlayerPrefs.SetInt("muertesPubli", 1);
                return true;
            }

            if (muertes > 30 && PlayerPrefs.GetInt("muertesPubli2") != 1)
            {
                popapMostrado = true;
                popapUpgradesG.SetActive(true);
                //popapUpgrades.SetTrigger("Fade");
                PlayerPrefs.SetInt("muertesPubli2", 1);
                return true;
            }
        }

        return false;
    }

    public bool PublicidadYldonMirakaPremium()
    {
#if UNITY_EDITOR
   /*     partidas = 36;

        for(int i = 0; i < 40; i += 10)
        {
            PlayerPrefs.DeleteKey("Publi" + i);
        }
        GameDataController.gameData.worldsObtained[0] = true;
        GameDataController.gameData.worldsObtained[1] = true;
        GameDataController.gameData.premiumState = false;
        GameDataController.gameData.specialLevelsCompleted[30] = true;
        StartCoroutine(ShopManager.Initialize());*/
        Social.localUser.Authenticate((bool success) =>
        {
      
        });

#endif

        Debug.Log("ComienzoPubliFinal");

        if (!GameDataController.gameData.premiumState)
        {
            if(partidas > 5 && PlayerPrefs.GetInt("Publi5") != 1)
            {
               Debug.Log("Publi5Partidas");
               PlayerPrefs.SetInt("Publi5", 1);
               if (!GameDataController.gameData.worldsObtained[0]) MostrarYldon();
               else if (!GameDataController.gameData.worldsObtained[1]) MostrarMiraka();
               else return false;
               return true;
            }
            else if(partidas > 10 && PlayerPrefs.GetInt("Publi10") != 1)
            {
                Debug.Log("Publi10Partidas");
                PlayerPrefs.SetInt("Publi10", 1);
                popapPremiumG.SetActive(true);
              //  popapPremium.SetTrigger("Fade");
                return true;
            }
            else if (partidas > 15 && PlayerPrefs.GetInt("Publi15") != 1)
            {
                Debug.Log("Publi5Partidas");
                PlayerPrefs.SetInt("Publi15", 1);
                if (!GameDataController.gameData.worldsObtained[0])
                {
                    MostrarYldon();
                }
                else if (!GameDataController.gameData.worldsObtained[1]) MostrarMiraka();
                else return false;
                return true;
            }
            else if (partidas > 20 && PlayerPrefs.GetInt("Publi20") != 1)
            {
                Debug.Log("Publi5Partidas");
                PlayerPrefs.SetInt("Publi20", 1);
                if (!GameDataController.gameData.worldsObtained[1]) MostrarMiraka();
                else return false;
                return true;
            }
            else if (partidas > 25 && PlayerPrefs.GetInt("Publi25") != 1)
            {
                Debug.Log("Publi5Partidas");
                PlayerPrefs.SetInt("Publi25", 1);
                //popapPremium.SetTrigger("Fade");
                popapPremiumG.SetActive(true);
                return true;
            }
            else if (partidas > 30 && PlayerPrefs.GetInt("Publi30") != 1)
            {
                Debug.Log("Publi5Partidas");
                PlayerPrefs.SetInt("Publi30", 1);
                if (!GameDataController.gameData.worldsObtained[1]) MostrarMiraka();
                else return false;
                return true;
            }
            else if (partidas > 35 && PlayerPrefs.GetInt("Publi35") != 1)
            {
                Debug.Log("Publi5Partidas");
                PlayerPrefs.SetInt("Publi35", 1);
                //popapPremium.SetTrigger("Fade");
                popapPremiumG.SetActive(true);
                return true;
            }
        }

        Debug.Log("Gola");

        return false;
    }

    public void MostrarYldon()
    {
        //   popapYldoon.SetTrigger("Fade");
        popapYldoonG.SetActive(true);

        if (!GameDataController.gameData.specialLevelsCompleted[30])
        {
            pTY.SetActive(true);
            pTUY.SetActive(false);
        }
        else
        {
            pTY.SetActive(false);
            pTUY.SetActive(true);
        }
    }

    public void MostrarMiraka()
    {
        popapMirakkaG.SetActive(true);
      //  popapMirakka.SetTrigger("Fade");

        if (!GameDataController.gameData.specialLevelsCompleted[30])
        {
            pTM.SetActive(true);
            pTUM.SetActive(false);
        }
        else
        {
            pTM.SetActive(false);
            pTUM.SetActive(true);
        }
    }

    public void BtnUnlock()
    {
        //popapYldoon.SetTrigger("FadeOut");
        //popapYldoon2.SetTrigger("Fade");
        popapYldoonG.SetActive(false);
        popapYldoon2G.SetActive(true);
    }

    public void BtnUnlockMiraka()
    {
        //  popapMirakka.SetTrigger("FadeOut");
        //  popapMirakka2.SetTrigger("Fade");

        popapMirakkaG.SetActive(false);
        popapMirakka2G.SetActive(true);
    }

    public void BtnPremium()
    {
      //  popapYldoon2.SetTrigger("FadeOut");
        popapYldoon2G.SetActive(false);
      //  popapPremium.SetTrigger("Fade");
        popapPremiumG.SetActive(true);
    }

    public void BtnPremiumMiraka()
    {
        //  popapMirakka2.SetTrigger("FadeOut");
        //    popapPremium.SetTrigger("Fade");

        popapMirakka2G.SetActive(false);
        popapPremiumG.SetActive(true);
    }

    public void PopapConfirmnarYldon()
    {
        if (GameDataController.gameData.diamondCurrency >= ShopManager.shopData.GetUpgrade(79).costPacks[0].cost) popapConfirmarYldon.SetActive(true);
        else  popapNoMoneyYldon.SetActive(false);

    }

    public void PopapConfirmarMiraka()
    {
        if (GameDataController.gameData.diamondCurrency >= ShopManager.shopData.GetUpgrade(80).costPacks[0].cost)
        {
            popapConfirmarMiraka.SetActive(true);
        }
        else
        {
            popapNoMoneyMiraka.SetActive(false);
        }
    }

    public void BtnRechazarCompraYldon()
    {
        popapConfirmarMiraka.SetActive(false);
    }

    public void BtnRechazarComprarMiraka()
    {
        popapConfirmarMiraka.SetActive(false);
    }

    public void BtnOkMiraka()
    {
        popapNoMoneyMiraka.SetActive(false);
    }

    public void BtnOkYldoon()
    {
        popapNoMoneyYldon.SetActive(false);
    }

    public void ComprarMiraka()
    {
        StartCoroutine(ComprarMirakaInternet());
    }

    public void ComprarYldoon()
    {
        StartCoroutine(ComprarYldoonInternet());
    }

    public IEnumerator ComprarYldoonInternet()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            //            popapInfo.SetTrigger("Fade");
            popapInfoG.SetActive(true);
            txtPopapInfo.text = "Para comprar necesita conexion a internet.";
        }
        else
        {
            GameDataController.gameData.worldsObtained[0] = true;

            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
          
#endif

            datos.AddField("idItem", 79);
            datos.AddField("diamantes", ShopManager.shopData.GetUpgrade(79).costPacks[0].cost);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;

            GameDataController.gameData.diamondCurrency = diBase;
            GameDataController.Save();
        }
    }

    public void BtnLater()
    {
        SceneManager.LoadScene("Publicidad");
    }

    public IEnumerator ComprarMirakaInternet()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            //            popapInfo.SetTrigger("Fade");
            popapInfoG.SetActive(true);
            txtPopapInfo.text = "Para comprar necesita conexion a internet.";
        }
        else
        {
          //  GameManager.SubtractDiamonds(ShopManager.shopData.GetUpgrade(80).costPacks[0].cost);
            GameDataController.gameData.worldsObtained[1] = true;
            GameDataController.gameData.diamondQueue = 0;

            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
     
#endif

            datos.AddField("idItem", 80);
            datos.AddField("diamantes", ShopManager.shopData.GetUpgrade(80).costPacks[0].cost);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;

            GameDataController.gameData.diamondCurrency = diBase;
            GameDataController.Save();
        }
    }

    public void PlayteaserDesierto()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        if (soyApple)
        {
            LoadSc.worldSelected = 1;
            LoadSc.fromBundle = false;
            LoadSc.specialNextLevelScene = false;
            LoadSc.nextScene = 56;
            LoadSc.normalScene = true;
            SceneManager.LoadScene("Publicidad");
        }
        else
        {
            if (BundleManager.checkWorldCached("desert"))
            {
                StartCoroutine(IniciarMundo("desert"));
            }
            else
            {
                popapConfirmarDescargaDesert.SetActive(true);
            }
        }
       
      
    }

    public void PlayteaserSnow()
    {
        bool soyApple;
#if UNITY_IOS
        soyApple = true;
#elif UNITY_ANDROID
        soyApple = true;
#endif
        if (soyApple)
        {
            LoadSc.worldSelected = 2;
            LoadSc.fromBundle = false;
            LoadSc.specialNextLevelScene = false;
            LoadSc.nextScene = 79;
            LoadSc.normalScene = true;
            SceneManager.LoadScene("Publicidad");
        }
        else
        {
            if (BundleManager.checkWorldCached("snow"))
            {
                StartCoroutine(IniciarMundo("snow"));
            }
            else
            {
                popapConfirmarDescargaSnow.SetActive(true);
            }
        }
    }

    public void CerrarPopapDescargaDesert()
    {
        popapConfirmarDescargaDesert.SetActive(false);
    }

    public void CerrarPopapDescargaSnow()
    {
        popapConfirmarDescargaSnow.SetActive(false);
    }

    public void DescargarMundo(string mundo)
    {
        StartCoroutine(IniciarMundo(mundo));
        popapConfirmarDescargaDesert.SetActive(false);
        popapConfirmarDescargaSnow.SetActive(false);
    }

    IEnumerator IniciarMundo(string mundoS)
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null)
        {
            popapLoading.SetActive(true);

            yield return BundleManager.Initialize(mundoS);

            if(mundoS == "desert") IniciarEscena("desertteaser_special_30", mundoS);
            else IniciarEscena("snowteaser_special_50", mundoS);

            popapLoading.SetActive(false);
         
        }
        else
        {
            if (BundleManager.checkWorldCached(mundoS))
            {

                popapLoading.SetActive(true);

                yield return BundleManager.InitializeOffline(mundoS);

                IniciarEscena("", mundoS);

                popapLoading.SetActive(false);                  
            }
            else
            { //            popapInfo.SetTrigger("Fade");
                popapInfoG.SetActive(true);
                txtPopapInfo.text = SeleccionaIdiomas.gameText[22, 10];
            }
        }
    }

    void IniciarEscena(string nombreEscena, string mundo)
    {
        esIntro = true;
        if (mundo == "desert") LoadSc.worldSelected = 1;
        else LoadSc.worldSelected = 2;
        LoadSc.fromBundle = true;
        LoadSc.specialNextLevelScene = true;
        LoadSc.nextScene = BundleManager.specialScenesList.IndexOf(nombreEscena);
        SceneManager.LoadScene("Publicidad");

    }

    public void IrMenuEspecial(float posY)
    {
        LoadSc.nextScene = -1;
        LoadSc.normalScene = true;
        LoadSc.fromBundle = false;
        LoadSc.specialNextLevelScene = false;
        LoadSc.specialPrevLevelScene = false;
        if(PlayerPrefs.GetInt("PermisosGoogleAceptados") == 1 && ShopManager.shopData != null)  LoadSc.ejeY = posY;
        SceneManager.LoadScene("Publicidad");
    }

    public void IrMenu()
    {
        LoadSc.nextScene = -1;
        LoadSc.normalScene = true;
        LoadSc.fromBundle = false;
        LoadSc.specialNextLevelScene = false;
        LoadSc.specialPrevLevelScene = false;
        SceneManager.LoadScene("Publicidad");
    }

}
