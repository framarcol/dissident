﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sonido : MonoBehaviour {

    float musica, eSonido;

    public Image[] iMusica, iEfectos;
    public Toggle tEfectos, tMusica;
    float guardaEstado, guardaEstado2;


    void Start()
    {

        //Coger volumenes juego
        musica = PlayerPrefs.GetFloat(GameManager.SoundEffectVolumePref) * 10;
        eSonido = PlayerPrefs.GetFloat(GameManager.MusicVolumePref) * 10;

        CambiaVolumenSprite();

    }

	public void btnMasM()
    {
        if(musica < 10) musica++;
        tMusica.isOn = false;
        CambiaVolumenSprite();
    }

    public void btnMenosM()
    {
        if (musica > -1) musica--;
        CambiaVolumenSprite();
    }

    public void btnMasE()
    {
        if (eSonido < 10) eSonido++;
        tEfectos.isOn = false;
        CambiaVolumenSprite();
    }

    public void btnMenosE()
    {
        if (eSonido > -1) eSonido--;
        CambiaVolumenSprite();

    }


    void CambiaVolumenSprite()
    {
        PlayerPrefs.SetFloat(GameManager.MusicVolumePref, eSonido / 10);
        PlayerPrefs.SetFloat(GameManager.SoundEffectVolumePref, musica / 10);

        for (int i = 0; i < iEfectos.Length; i++)
        {
            if (i < musica) iMusica[i].gameObject.SetActive(true);
            else iMusica[i].gameObject.SetActive(false);

            if (i < eSonido) iEfectos[i].gameObject.SetActive(true);
            else iEfectos[i].gameObject.SetActive(false);
        }
    }

    public float getMusica()
    {
        return musica;
    }

    public float getESonido()
    {
        return eSonido;
    }

    public void ToggleChanged(bool Value)
    {
        if (Value)
        {
            guardaEstado = getMusica();
            musica = 0;
        }
        else
        {
            musica = guardaEstado;
            
        }

        CambiaVolumenSprite();

    }

    public void ToguelEfectos(bool Value)
    {
        if (Value)
        {
            guardaEstado2 = getESonido();
            eSonido = 0;
        
        }
        else
        {
            eSonido = guardaEstado2;

        }

        CambiaVolumenSprite();
    }

}
