﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsElementos : MonoBehaviour {

    public Slider miStat;
    public int stat;

    public void AsignarValor()
    {
        miStat.value = stat;
    }

    public int getStat()
    {
        return stat;
    }

}
