﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tienda : MonoBehaviour {


    public Animator animacionProta;
    string[] nombreAnimaciones;
    public GameObject[] elementosPrincipales, btnElementos;
    public GameObject contenido;
    public CanvasGroup[] stats;
    MeshRenderer[] objetos;
    public Slider[] sliderBarras;
    int idBoton2, idBotonElemento;
    int[] elementosSeleccionados;
    float[] valorElementosSeleccionados;
    public Sprite[] spritesMonopatines, spritesArmas, spritesProta;
    Image[] imagenes;
    Button[] botonesImagenes;

    //Elaborar Script cargarElementosSeleccionados;
    
    void Start()
    {
       imagenes = contenido.GetComponentsInChildren<Image>();
       botonesImagenes = contenido.GetComponentsInChildren<Button>();
       idBoton2 = -1;
       nombreAnimaciones = new string[] { "Disparo", "CambioRopa", "LimpiarBrazo", "CambiarPeso" };
       elementosSeleccionados = new int[] { 0, 0, 0 };
       valorElementosSeleccionados = new float[] { 10, 15, 1 };
    }

    IEnumerator MovimientoProta()
    {
        while (true)
        {
            yield return new WaitForSeconds(8f);
            animacionProta.SetTrigger(nombreAnimaciones[Random.Range(0, 4)]);
        }
    }

    void Update()
    {
       if(idBoton2  > 0 && !Input.GetMouseButton(0)) elementosPrincipales[idBoton2].transform.Rotate(0, Time.deltaTime * 20, 0);
    }

    public void btnCambioElmentoPrincipal(int idBoton)
    {
        if (idBoton2 != -1)for (int i = 0; i < objetos.Length; i++) objetos[i].gameObject.SetActive(true); // Deja todos los objetos activados para después recogerlos de nuevo
        for (int i = 0; i < elementosPrincipales.Length; i++) elementosPrincipales[i].gameObject.SetActive(false); // desactiva los elementos principales
        elementosPrincipales[idBoton].gameObject.SetActive(true); // Activa el elemento principal correspondiente
        idBoton2 = idBoton; // Pasar parámetro para el movimiento   
        for (int i = 0; i < sliderBarras.Length; i++) sliderBarras[i].value = valorElementosSeleccionados[i];
        objetos = elementosPrincipales[idBoton].GetComponentsInChildren<MeshRenderer>(); // Carga los objetos correspondientes
        for (int i = 0; i < objetos.Length; i++) objetos[i].gameObject.SetActive(false); // Desactivar todos los elementos de la tienda para que no se muestren todos a la vez
        objetos[elementosSeleccionados[idBoton]].gameObject.SetActive(true); //Activar el primer elemento de la tienda
        AsignarValorBarraStats(elementosSeleccionados[idBoton]); // Asignar el primer valor de la barra

        for (int i = 0; i < imagenes.Length; i++)
        {
            imagenes[i].color = new Color(imagenes[i].color.r, imagenes[i].color.g, imagenes[i].color.b, 1); //Visibilizar Sprites
            botonesImagenes[i].interactable = true;
        } 

        if (idBoton == 0) for (int i = 0; i < spritesProta.Length; i++) imagenes[i].sprite = spritesProta[i];
        else if (idBoton == 1) for (int i = 0; i < spritesArmas.Length; i++) imagenes[i].sprite = spritesArmas[i]; //Cargar sprites que sirven como previsualización del nivel
        else for (int i = 0; i < spritesMonopatines.Length; i++) imagenes[i].sprite = spritesMonopatines[i];


        if (elementosPrincipales[0].gameObject.activeSelf == true) StartCoroutine(MovimientoProta()); // Activar animación movimientos prota
        for (int i = 0; i < btnElementos.Length; i++) btnElementos[i].gameObject.SetActive(false); // Desativar numero elementos para seleccionar tipo de arma/armadura/monopatin
        for (int i = 0; i < objetos.Length && i < 10; i++) btnElementos[i].gameObject.SetActive(true); // Activar solo el número de elementos que haya de armas/armadura/monopatin
        for (int i = 0; i < stats.Length; i++) stats[i].alpha = 0.3f; // Poner transparentes las estadísticas
        stats[idBoton].alpha = 1; // Poner visible la estadística seleccionada
    }

    public void btnCambioElementoSecundario(int idBoton)
    {
        for (int i = 0; i < objetos.Length; i++) objetos[i].gameObject.SetActive(false);
        objetos[idBoton].gameObject.SetActive(true);
        idBotonElemento = idBoton;
        AsignarValorBarraStats(idBoton);
    }

    public void BtnConfirmarCambios()
    {
        elementosSeleccionados[idBoton2] = idBotonElemento;
        StatsElementos miE = objetos[idBotonElemento].GetComponent<StatsElementos>();
        valorElementosSeleccionados[idBoton2] = miE.getStat();
    }

    public void AsignarValorBarraStats(int id)
    {
        StatsElementos miE = objetos[id].GetComponent<StatsElementos>();
        miE.AsignarValor();
    }


}