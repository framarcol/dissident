﻿using UnityEngine;
using System.Collections;

public class AnimacionTiendaController : MonoBehaviour {

    int skinSeleccionada, armaSeleccionada, monopatinSeleccionado;

    public SkinnedMeshRenderer skinBody, skinMonopatin;
    public MeshRenderer armaDerecha, armaIzquierda, armaDerechaDefecto, armaIzquierdaDefecto;

    public GameObject MonoDefecto, MonoNormal;

    public MeshFilter armaDerechaFilter, armaIzquierdaFilter, armaDerechaFilterDefecto, armaIzquierdaFilterDefecto;

    public Animator skinAnimacion, monopatinAnimacion;
    public GameObject pistID, pistDD, pistI, pistD;

    void Start()
    {
          CargarMeshBody();
          CargarMeshPistola();
          CargarMeshSkate();
        
    }

    void OnEnable()
    {
        GetComponent<Transform>().localPosition = new Vector3(0, 0, 0);
        GetComponent<Transform>().localRotation = new Quaternion(0, 0, 0, 0);
        StartCoroutine(CargarAnimaciones());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    public void CargarMeshBody()
    {
        skinSeleccionada = (int)GameDataController.gameData.bodySkinEquipped;
        skinBody.sharedMesh = ShopManager.shopData.GetBodySkin(skinSeleccionada).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
        skinBody.sharedMaterial = ShopManager.shopData.GetBodySkin(skinSeleccionada).itemOverrideMaterial;

    }

    public void CargarMeshPistola()
    {
        armaSeleccionada = (int)GameDataController.gameData.weaponSkinEquipped;
        //armaSeleccionada = 41
        if (armaSeleccionada == 71)
        {
            pistDD.SetActive(true);
            pistID.SetActive(true);
            pistI.SetActive(false);
            pistD.SetActive(false);
            armaDerechaFilterDefecto.sharedMesh = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[0].sharedMesh;
            armaIzquierdaFilterDefecto.sharedMesh = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[1].sharedMesh;
            armaDerechaDefecto.sharedMaterial = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemOverrideMaterial;
            armaIzquierdaDefecto.sharedMaterial = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemOverrideMaterial;

        }
        else
        {
            pistDD.SetActive(false);
            pistID.SetActive(false);
            pistI.SetActive(true);
            pistD.SetActive(true);
            armaDerechaFilter.sharedMesh = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[0].sharedMesh;
            armaIzquierdaFilter.sharedMesh = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[1].sharedMesh;
            armaDerecha.sharedMaterial = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemOverrideMaterial;
            armaIzquierda.sharedMaterial = ShopManager.shopData.GetWeaponSkin(armaSeleccionada).lowpolyItemOverrideMaterial;
        }

       

    }

    public void CargarMeshSkate()
    {
        monopatinSeleccionado = (int)GameDataController.gameData.skateSkinEquipped;

        if(monopatinSeleccionado == 72)
        {
            MonoDefecto.SetActive(true);
            MonoNormal.SetActive(false);
        }
        else
        {
            MonoDefecto.SetActive(false);
            MonoNormal.SetActive(true);
            skinMonopatin.sharedMesh = ShopManager.shopData.GetSkateSkin(monopatinSeleccionado).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
            skinMonopatin.sharedMaterial = ShopManager.shopData.GetSkateSkin(monopatinSeleccionado).itemOverrideMaterial;
        }

    }

    IEnumerator CargarAnimaciones()
    {
        yield return new WaitForSeconds(5);

        string num = Random.Range(1, 9).ToString();

        skinAnimacion.SetTrigger("Anim" + num);
        monopatinAnimacion.SetTrigger("Anim" + num);

        MetodoCargarAnimaciones();

    }

    void MetodoCargarAnimaciones()
    {
        StartCoroutine(CargarAnimaciones());
    }

}
