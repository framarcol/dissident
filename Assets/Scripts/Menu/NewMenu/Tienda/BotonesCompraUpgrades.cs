﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotonesCompraUpgrades : MonoBehaviour
{


    int idTransaccion;

    public GameObject popapConfirmacion;
    public GameObject MirakaComprado;
    public GameObject YldonComprado;
    public GameObject MirakaComprar;
    public GameObject YldonComprar;
    public GameObject controladorIdMenu;

    public Toggle tgVida;
    public Toggle tgMunicion;
    public Toggle tgYldon;
    public Toggle tgMiraka;
    public Text maxVida;
    public Text maxMunicion;
    public GameObject VidaComprar;
    public GameObject MunicionComprar;
    public GameObject NoMoneyPopap;
    public GameObject popapConfirmacionLogueo;
    public Text txtPopapLogueo;

    int vidaCompradaI;
    int municionCompradaI;

    public GameObject[] panelesUpgrades;
    public GameObject[] imgBarras;

    string variablePrefsVidas, variablePrefsMunicion;


    //Elementos a cargar
    public Text textoNumVidas;
    public Text precioVidas;
    public Text textoNumBalas;
    public Text precioNumBalas;

    public Text precioYldon;
    public Text precioMiraka;
    public Text precioEstrellasYldon;
    public Text precioEstrellasMiraka;

    public Animator popapInfo;
    public Text txtPopapInfo;
    public int[] ObjetosComprados;
    bool yldonComprada, mirakaComprada;
    public Toggle tgDiamonds;
    int precioNVida, precioNumBalasN, precioMirakaN, precioYldonN;

    public GameObject panelVida, BarraVida;

    void OnEnable()
    {
        variablePrefsVidas = "VidaSeleccionada";
        variablePrefsMunicion = "MunicionSeleccionada";

        ObjetosComprados = new int[100];
        yldonComprada = false;
        mirakaComprada = false;

        panelVida.gameObject.SetActive(true);
        BarraVida.gameObject.SetActive(true);

        for (int i = 0; i < ObjetosComprados.Length; i++)
        {
            if (ControladorId.ObjetoCompradosPorTipo[4, i] != 0)
                ObjetosComprados[i] = (int)ControladorId.ObjetoCompradosPorTipo[4, i];
            else break;
        }
		if(GameDataController.gameData.worldsObtained[0]){

			yldonComprada = true;

		}

		if (GameDataController.gameData.worldsObtained[1])
		{

			mirakaComprada = true;

		}

#if UNITY_EDITOR
        GameDataController.gameData.ammoUpgradeEquipped = 0;
        GameDataController.gameData.lifeUpgradeEquipped = 0;
#endif



        for (int i = 0; i < ObjetosComprados.Length; i++)
			{
				if (ObjetosComprados[i] != 0)
				{
					if (ObjetosComprados[i] == 79)
					{
						GameDataController.gameData.worldsObtained[0] = true;
					        yldonComprada = true;
						
					}
					if (ObjetosComprados[i] == 80)
					{
						GameDataController.gameData.worldsObtained[1] = true;
						mirakaComprada = true;
					}


					if (ShopManager.shopData.GetUpgrade(ObjetosComprados[i]).updgradeType.ToString() == "MAX_AMMO")
					{
						if (ObjetosComprados[i] > GameDataController.gameData.ammoUpgradeEquipped)
						{
							GameDataController.gameData.ammoUpgradeEquipped = (uint)ObjetosComprados[i];
						}
					}

					if (ShopManager.shopData.GetUpgrade(ObjetosComprados[i]).updgradeType.ToString() == "LIFE")
					{
						if (ObjetosComprados[i] > GameDataController.gameData.lifeUpgradeEquipped)
						{
							GameDataController.gameData.lifeUpgradeEquipped = (uint)ObjetosComprados[i];
						}
					}
				}
			}

       
        if (yldonComprada || GameDataController.gameData.premiumState)
        {
            YldonComprado.SetActive(true);
            YldonComprar.SetActive(false);
        }
        else
        {
            precioYldon.text = ShopManager.shopData.GetUpgrade(79).costPacks[0].cost.ToString();
            precioYldonN = ShopManager.shopData.GetUpgrade(79).costPacks[0].cost;
            precioEstrellasYldon.text = ShopManager.shopData.GetUpgrade(79).costPacks[1].cost.ToString();
            YldonComprado.SetActive(false);
            YldonComprar.SetActive(true);
        }

        if (mirakaComprada || GameDataController.gameData.premiumState)
        {
            MirakaComprado.SetActive(true);
            MirakaComprar.SetActive(false);
        }
        else
        {
            precioMiraka.text = ShopManager.shopData.GetUpgrade(80).costPacks[0].cost.ToString();
            precioMirakaN = ShopManager.shopData.GetUpgrade(80).costPacks[0].cost;
            precioEstrellasMiraka.text = ShopManager.shopData.GetUpgrade(80).costPacks[1].cost.ToString();
            MirakaComprado.SetActive(false);
            MirakaComprar.SetActive(true);
        }

        for (int i = 0; i < ShopManager.shopData.upgradeItems.Count; i++)
        {
            if (GameDataController.gameData.lifeUpgradeEquipped == ShopManager.shopData.upgradeItems[i].itemId)
            {
                precioVidas.text = ShopManager.shopData.upgradeItems[i + 1].costPacks[0].cost.ToString();
                precioNVida = ShopManager.shopData.upgradeItems[i + 1].costPacks[0].cost;
                textoNumVidas.text = ShopManager.shopData.upgradeItems[i + 1].extraLife.ToString();
                vidaCompradaI = i + 1;
            }

            if (GameDataController.gameData.ammoUpgradeEquipped == ShopManager.shopData.upgradeItems[i].itemId)
            {
                precioNumBalas.text = ShopManager.shopData.upgradeItems[i + 1].costPacks[0].cost.ToString();
                precioNumBalasN = ShopManager.shopData.upgradeItems[i + 1].costPacks[0].cost;
                textoNumBalas.text = ShopManager.shopData.upgradeItems[i + 1].maxAmmo.ToString();
                municionCompradaI = i + 1;
            }
        }

        if (GameDataController.gameData.lifeUpgradeEquipped == 0)
        {

            precioVidas.text = ShopManager.shopData.upgradeItems[0].costPacks[0].cost.ToString();
            precioNVida = ShopManager.shopData.upgradeItems[0].costPacks[0].cost;
            textoNumVidas.text = ShopManager.shopData.upgradeItems[0].extraLife.ToString();
            vidaCompradaI = 0;

        }
        if (GameDataController.gameData.ammoUpgradeEquipped == 0)
        {
            precioNumBalas.text = ShopManager.shopData.upgradeItems[3].costPacks[0].cost.ToString();
            precioNumBalasN = ShopManager.shopData.upgradeItems[3].costPacks[0].cost;
            textoNumBalas.text = ShopManager.shopData.upgradeItems[3].maxAmmo.ToString();
            municionCompradaI = 3;
        }


        if (GameDataController.gameData.lifeUpgradeEquipped == 75) ActivarVidaMaxima();
        if (GameDataController.gameData.ammoUpgradeEquipped == 78) ActivarMunicionMaxima();

    }

    public void OnDisable()
    {
        for (int i = 0; i < imgBarras.Length; i++)
        {
            panelesUpgrades[i].SetActive(false);
            imgBarras[i].SetActive(false);
           
        }

        if (GameDataController.gameData.lifeUpgradeEquipped != 75)
        {
            tgVida.isOn = true;
            tgMunicion.isOn = false;
            tgYldon.isOn = false;
            tgMiraka.isOn = false;
        }
        else if (GameDataController.gameData.ammoUpgradeEquipped != 78)
        {
            tgVida.isOn = false;
            tgMunicion.isOn = true;
            tgYldon.isOn = false;
            tgMiraka.isOn = false;
        }
        else
        {
            tgVida.isOn = false;
            tgMunicion.isOn = false;
            tgYldon.isOn = false;
            tgMiraka.isOn = false;
        }      
    }

    public void ActivarVidaMaxima()
    {
        maxVida.gameObject.SetActive(true);
        VidaComprar.SetActive(false);
        imgBarras[0].SetActive(false);
        if (GameDataController.gameData.ammoUpgradeEquipped == 78)
        {
            tgYldon.isOn = true;
            tgVida.interactable = false;
        }
        else
        {
            tgMunicion.isOn = true;
            tgVida.interactable = false;
        }

    }

    public void ActivarMunicionMaxima()
    {
        maxMunicion.gameObject.SetActive(true);
        imgBarras[1].SetActive(false);
        MunicionComprar.SetActive(false);
        tgYldon.isOn = true;
        tgMunicion.interactable = false;
    }

    public void ComprarAumentoVida()
    {
        ComprarVidaInternet();
    }

    public void ComprarAumentoMunicion()
    {
       ComprarMunicionInternet();
    }

    public void ComprarYldon()
    {
        ComprarYldoonInternet();
    }

    public void ComprarMiraka()
    {
      ComprarMirakaInternet();
    }


    public void BtnConfirmar()
    {
        switch (idTransaccion)
        {
            case 1:
                ComprarAumentoVida();
                break;
            case 2:
                ComprarAumentoMunicion();
                break;
            case 3:
                ComprarYldon();
                break;
            case 4:
                ComprarMiraka();
                break;
        }

        popapConfirmacion.SetActive(false);
    }

    public void SelectId(int id)
    {
        idTransaccion = id;
        int diaman = 0;
        int diamantesRestantes;

        switch (idTransaccion)
        {
            case 1:
                diaman = precioNVida;
                diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                if (diamantesRestantes < 0) NoMoneyPopap.SetActive(true);
                else popapConfirmacion.SetActive(true);
                break;
            case 2:
                diaman = precioNumBalasN;
                diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                if (diamantesRestantes < 0) NoMoneyPopap.SetActive(true);
                else popapConfirmacion.SetActive(true);
                break;
            case 3:
                diaman = precioYldonN;
                Debug.Log("Precio Yldon: " + precioYldon);
                diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                if (diamantesRestantes < 0) NoMoneyPopap.SetActive(true);
                else popapConfirmacion.SetActive(true);
                break;
            case 4:
                diaman = precioMirakaN;
                diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                if (diamantesRestantes < 0) NoMoneyPopap.SetActive(true);
                else popapConfirmacion.SetActive(true);
                break;
        }
/*
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            if (ControladorId.Logueado)
            {
               
            }
            else
            {
                popapConfirmacionLogueo.SetActive(true);
                txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
            }
        }
        else
        {
            if (Social.localUser.authenticated)
            {
                if (ControladorId.Logueado)
                {
                    switch (idTransaccion)
                    {
                        case 1:
                            diaman = precioNVida;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 2:
                            diaman = precioNumBalasN;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 3:
                            diaman = precioYldonN;
                            Debug.Log("Precio Yldon: " + precioYldon);
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 4:
                            diaman = precioMirakaN;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                    }
                }
                else
                {
                    popapConfirmacionLogueo.SetActive(true);
                    txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
                }
            }
            else
            {
                popapInfo.SetTrigger("Fade");
                txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                controladorIdMenu.GetComponent<ControladorId>().Autentificar();
            }
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            if (ControladorId.Logueado)
            {
                switch (idTransaccion)
                {
                    case 1:
                        diaman = precioNVida;
                        diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                        if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                        else popapConfirmacion.SetActive(true);
                        break;
                    case 2:
                        diaman = precioNumBalasN;
                        diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                        if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                        else popapConfirmacion.SetActive(true);
                        break;
                    case 3:
                        diaman = precioYldonN;
                        Debug.Log("Precio Yldon: " + precioYldon);
                        diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                        if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                        else popapConfirmacion.SetActive(true);
                        break;
                    case 4:
                        diaman = precioMirakaN;
                        diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                        if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                        else popapConfirmacion.SetActive(true);
                        break;
                }
            }
            else
            {
                popapConfirmacionLogueo.SetActive(true);
                txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
            }
        }
        else
        {
            if (Social.localUser.authenticated)
            {
                if (ControladorId.Logueado)
                {
                    switch (idTransaccion)
                    {
                        case 1:
                            diaman = precioNVida;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 2:
                            diaman = precioNumBalasN;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 3:
                            diaman = precioYldonN;
                            Debug.Log("Precio Yldon: " + precioYldon);
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                        case 4:
                            diaman = precioMirakaN;
                            diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - diaman);
                            if (diamantesRestantes < 0) NoMoneyPopap.SetActive(false);
                            else popapConfirmacion.SetActive(true);
                            break;
                    }
                }
                else
                {
                    popapConfirmacionLogueo.SetActive(true);
                    txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
                }
            }
            else
            {
                popapInfo.SetTrigger("Fade");
                txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                controladorIdMenu.GetComponent<ControladorId>().Autentificar();
            }
        }
#endif*/
    }

    public void BtnRechazar()
    {
        popapConfirmacion.SetActive(false);
    }

    public void BtnRechazarMoney()
    {
        NoMoneyPopap.SetActive(false);
    }

    public void BtnComprarDiamantes()
    {
        NoMoneyPopap.SetActive(false);
        tgDiamonds.isOn = true;
    }


    public void ComprarMirakaInternet()
    {
       /* WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {*/

            int di =0;

            for (int i = 0; i < ShopManager.shopData.GetUpgrade(80).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetUpgrade(80).costPacks[i].currency.ToString() == "DIAMONDS")
                {
                    di = ShopManager.shopData.GetUpgrade(80).costPacks[i].cost;
                }
            }

            GameDataController.gameData.worldsObtained[1] = true;

            MirakaComprado.SetActive(true);
            MirakaComprar.SetActive(false);

       //     WWWForm datos = new WWWForm();
         //   WWW itemsData;

        //    datos.AddField("idTipo", 4);
/*#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
    
#endif*/
         //   datos.AddField("idItem", 80);
         //   datos.AddField("diamantes", di);

          //  itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

          //  yield return itemsData;

          //  int diBaseI = int.Parse(itemsData.text);
          //  uint diBase = 0;

         //   if (diBaseI < 0) diBase = 0;
         //   else diBase = (uint)diBaseI;

            GameDataController.gameData.diamondCurrency -= (uint)di;
            GameDataController.Save();
        //}
    }

    public void ComprarVidaInternet()
    {
      /*  WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {*/

            int di = 0;

            for (int i = 0; i < ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[vidaCompradaI].itemId).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[vidaCompradaI].itemId).costPacks[i].currency.ToString() == "DIAMONDS")
                {
                    di = ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[vidaCompradaI].itemId).costPacks[i].cost;
                }
            }

         //   GameManager.SubtractDiamonds(di);

            GameDataController.gameData.lifeUpgradeEquipped = (uint)ShopManager.shopData.upgradeItems[vidaCompradaI].itemId;
            GameDataController.Save();
            vidaCompradaI++;
            if (GameDataController.gameData.lifeUpgradeEquipped == 75) ActivarVidaMaxima();
            else
            {
                precioVidas.text = ShopManager.shopData.upgradeItems[vidaCompradaI].costPacks[0].cost.ToString();
                precioNVida = ShopManager.shopData.upgradeItems[vidaCompradaI].costPacks[0].cost;
                textoNumVidas.text = ShopManager.shopData.upgradeItems[vidaCompradaI].extraLife.ToString();
            }

/*            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
    
#endif

            datos.AddField("idItem", GameDataController.gameData.lifeUpgradeEquipped.ToString());
            datos.AddField("diamantes", di);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;*/

            GameDataController.gameData.diamondCurrency -= (uint)di;
            GameDataController.Save();

       // }
    }

    public void ComprarMunicionInternet()
    {
      /*  WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {*/

            int di = 0;

            for (int i = 0; i < ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[municionCompradaI].itemId).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[municionCompradaI].itemId).costPacks[i].currency.ToString() == "DIAMONDS")
                {
                    di = ShopManager.shopData.GetUpgrade(ShopManager.shopData.upgradeItems[municionCompradaI].itemId).costPacks[i].cost;
                }
            }

        //    GameManager.SubtractDiamonds(di);

            GameDataController.gameData.ammoUpgradeEquipped = (uint)ShopManager.shopData.upgradeItems[municionCompradaI].itemId;
            GameDataController.Save();
            municionCompradaI++;
            if (GameDataController.gameData.ammoUpgradeEquipped == 78) ActivarMunicionMaxima();
            else
            {
                precioNumBalas.text = ShopManager.shopData.upgradeItems[municionCompradaI].costPacks[0].cost.ToString();
                precioNumBalasN = ShopManager.shopData.upgradeItems[municionCompradaI].costPacks[0].cost;
                textoNumBalas.text = ShopManager.shopData.upgradeItems[municionCompradaI].maxAmmo.ToString();
            }

  /*          WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }

#endif

            datos.AddField("idItem", GameDataController.gameData.ammoUpgradeEquipped.ToString());
            datos.AddField("diamantes", di);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;*/

            GameDataController.gameData.diamondCurrency -= (uint)di;
            GameDataController.Save();
        //}
    }

    public void ComprarYldoonInternet()
    {
      /*  WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {*/

            int di = 0;

            for (int i = 0; i < ShopManager.shopData.GetUpgrade(79).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetUpgrade(79).costPacks[i].currency.ToString() == "DIAMONDS")
                {  
                    di = ShopManager.shopData.GetUpgrade(79).costPacks[i].cost;
                    Debug.Log("Diamantes a restar: " + di);
                }
            }

         //   GameManager.SubtractDiamonds(di);


            GameDataController.gameData.worldsObtained[0] = true;
        //    GameDataController.Save();
            YldonComprado.SetActive(true);
            YldonComprar.SetActive(false);

/*            WWWForm datos = new WWWForm();
            WWW itemsData;

            datos.AddField("idTipo", 4);
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
            }
            else
            {
                datos.AddField("idUsuario", Social.localUser.id.ToString());
            }
          
#endif

            datos.AddField("idItem", 79);
            datos.AddField("diamantes", di);

            itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

            yield return itemsData;

            int diBaseI = int.Parse(itemsData.text);
            uint diBase = 0;

            if (diBaseI < 0) diBase = 0;
            else diBase = (uint)diBaseI;*/

            GameDataController.gameData.diamondCurrency -= (uint)di;
            GameDataController.Save();
        //}
    }

}
