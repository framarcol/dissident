﻿using UnityEngine;
using System.Collections;

public class MovimientoProtaTienda : MonoBehaviour {

    Animator monopatinAnimacion;
	
	void Start () {

        monopatinAnimacion = GetComponent<Animator>();
	}

    IEnumerator CargarAnimaciones()
    {
        yield return new WaitForSeconds(5);

        string num = Random.Range(4, 9).ToString();

        monopatinAnimacion.SetTrigger("Anim" + num);

        MetodoCargarAnimaciones();

    }

    void OnEnable()
    {
        GetComponent<Transform>().localPosition = new Vector3(0, 0, 0);
        GetComponent<Transform>().localRotation = new Quaternion(0, 0, 0, 0);
        StartCoroutine(CargarAnimaciones());
    }
    void OnDisable()
    {
        StopAllCoroutines();
    }

    void MetodoCargarAnimaciones()
    {  
        StartCoroutine(CargarAnimaciones());
    }
}
