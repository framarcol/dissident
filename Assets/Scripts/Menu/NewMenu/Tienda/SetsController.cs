﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetsController : MonoBehaviour {

    public Text precioEstrellas, precioDiamantes, precioDiamantesPanelDiamantes, tituloSet, txtPopapInfo;
    public GameObject panelEquiparse, panelDiamantes, panelEyD, panelNoMoney, panelConfirmar;
    public Animator popapInfo;
    public SkinnedMeshRenderer meshBody, meshMonopatin;
    public MeshFilter armaIzq, armaDer;
    public MeshRenderer armaIzqR, armaDerR;
    public GameObject btnFlechaDerecha, btnFlechaIzquierda;
    public Image spriteArma;
    public Toggle tgDiamonds;

    public GameObject skins, monopatines, armas, controladorIdMenu;

    int idBodySet, idMonopatin, idArma, idSet;
    public static bool[] setCambiado;

    int[] ObjetosComprados;
    public int numMaxEleComprar;
    bool esComprado;

    public GameObject popapConfirmarLogueo;
    public Text txtPopapLogueo;
     
    int numeroSet;

    void OnEnable()
    {
        setCambiado = new bool[3];
        setCambiado[0] = false;
        setCambiado[1] = false;
        setCambiado[2] = false;
        esComprado = false;
        btnFlechaIzquierda.SetActive(false);
        btnFlechaDerecha.SetActive(true);

        ObjetosComprados = new int[numMaxEleComprar];

        for (int i = 0; i < ObjetosComprados.Length; i++)
        {
            if (ControladorId.ObjetoCompradosPorTipo[3, i] != 0)
                ObjetosComprados[i] = (int)ControladorId.ObjetoCompradosPorTipo[3, i];
            else
            {
                //ObjetosComprados[i] = idItemDefecto;
                break;
            }
        }

        numeroSet = 0;
        CargarSet(numeroSet);
    }

    public void OnDisable()
    {
        panelConfirmar.SetActive(false);
        panelNoMoney.SetActive(false);
    }

    public void BtnIzquierda()
    {
        numeroSet--;
        //Debug.Log(numeroSet);
        esComprado = false;
        CargarSet(numeroSet);
 
        panelConfirmar.SetActive(false);
        panelNoMoney.SetActive(false);
        btnFlechaDerecha.SetActive(true);
        
        if (numeroSet > 0) btnFlechaIzquierda.SetActive(true);
        else btnFlechaIzquierda.SetActive(false);
    }


 /*   void Update()
    {
        if(ShopManager.shopData != null)
        {
            if (numeroSet > ShopManager.shopData.shopItemSets.Count - 2) btnFlechaDerecha.SetActive(false);
            else btnFlechaDerecha.SetActive(true);
        } 
    }*/

    public void BtnDerecha()
    {
        esComprado = false;
        numeroSet++;
        CargarSet(numeroSet);
        panelConfirmar.SetActive(false);
        panelNoMoney.SetActive(false);
        btnFlechaIzquierda.SetActive(true);
        

        if (numeroSet > ShopManager.shopData.shopItemSets.Count - 2) btnFlechaDerecha.SetActive(false);   
        else btnFlechaDerecha.SetActive(true);

    }

    void CargarSet(int numSet)
    {
        idBodySet = ShopManager.shopData.shopItemSets[numSet].linkedItemIds[0];
        idMonopatin = ShopManager.shopData.shopItemSets[numSet].linkedItemIds[1];
        idArma = ShopManager.shopData.shopItemSets[numSet].linkedItemIds[2];
        idSet = ShopManager.shopData.shopItemSets[numSet].itemId;

        meshBody.sharedMesh = ShopManager.shopData.GetBodySkin(idBodySet).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
        meshMonopatin.sharedMesh = ShopManager.shopData.GetSkateSkin(idMonopatin).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;

        meshBody.material = ShopManager.shopData.GetBodySkin(idBodySet).itemOverrideMaterial;
        meshMonopatin.material = ShopManager.shopData.GetSkateSkin(idMonopatin).itemOverrideMaterial;

        armaDer.sharedMesh = ShopManager.shopData.GetWeaponSkin(idArma).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[0].sharedMesh;
        armaIzq.sharedMesh = ShopManager.shopData.GetWeaponSkin(idArma).lowpolyItemPresentation.GetComponentsInChildren<MeshFilter>()[1].sharedMesh;

        armaDerR.sharedMaterial = ShopManager.shopData.GetWeaponSkin(idArma).lowpolyItemOverrideMaterial;
        armaIzqR.sharedMaterial = ShopManager.shopData.GetWeaponSkin(idArma).lowpolyItemOverrideMaterial;

        spriteArma.sprite = ShopManager.shopData.GetWeaponSkin(idArma).itemThumbnail;

        tituloSet.text = ShopManager.shopData.shopItemSets[numSet].itemName;

        precioDiamantes.text = ShopManager.shopData.shopItemSets[numSet].costPacks[0].cost.ToString();
        precioDiamantesPanelDiamantes.text = precioDiamantes.text;
        if(ShopManager.shopData.shopItemSets[numSet].costPacks.Count > 1) precioEstrellas.text = ShopManager.shopData.shopItemSets[numSet].costPacks[1].cost.ToString();

        for (int i = 0; i < ObjetosComprados.Length; i++)
        {
            if(ObjetosComprados[i] == idSet)
            {
             //   Debug.Log("Gola");
                esComprado = true;
            }
        }

        if (esComprado)
        {
            panelDiamantes.SetActive(false);
            panelEyD.SetActive(false);
            panelEquiparse.SetActive(true);
        }
        else
        {
            panelEquiparse.SetActive(false);
            if(ShopManager.shopData.shopItemSets[numSet].costPacks.Count > 1)
            {
                panelEyD.SetActive(true);
                panelDiamantes.SetActive(false);
            }
            else
            {
                panelDiamantes.SetActive(true);
                panelEyD.SetActive(false);
            }
        }

    }



    public void IniciarCompra()
    {
        if (!CargaIntro.modoOffline) {
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                StartCoroutine(ComprobarConexion());
            }
            else
            {
                if (Social.localUser.authenticated)
                {
                    StartCoroutine(ComprobarConexion());
                }
                else
                {
                    popapInfo.SetTrigger("Fade");
                    txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                    controladorIdMenu.GetComponent<ControladorId>().Autentificar();
                }
            }
#elif UNITY_IOS
            if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
            {
                StartCoroutine(ComprobarConexion());
            }
            else
            {
                if (Social.localUser.authenticated)
                {
                    StartCoroutine(ComprobarConexion());
                }
                else
                {
                    popapInfo.SetTrigger("Fade");
                    txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                    controladorIdMenu.GetComponent<ControladorId>().Autentificar();
                }
            }
#endif

        }
        else
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 17];
        }

    }

    public IEnumerator ComprobarConexion()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {
            uint di = 0;

            for (int i = 0; i < ShopManager.shopData.GetSet(idSet).costPacks.Count; i++)
            {
                if (ShopManager.shopData.GetSet(idSet).costPacks[i].currency.ToString() == "DIAMONDS")
                {
                    di = (uint)ShopManager.shopData.GetSet(idSet).costPacks[i].cost;
                }
            }
            int diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - di);
            //Debug.Log(diamantesRestantes);
            if (diamantesRestantes < 0)  panelNoMoney.SetActive(false);
            else panelConfirmar.SetActive(true);
        }
    }

    public void RechazarCompra()
    {
        panelConfirmar.SetActive(false);
    }

    public IEnumerator ComprobarConexionCompra()
    {
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = "Para comprar necesita conexion a internet.";
        }
        else
        {
            //  PlayerPrefs.SetInt("MonopatinSeleccionado", idMonopatin);
            // PlayerPrefs.SetInt("SkinSeleccionada", idBodySet);
            // PlayerPrefs.SetInt("ArmaSeleccionada", idArma);

            if (ControladorId.Logueado)
            {
                int di = 0;

                for (int i = 0; i < ShopManager.shopData.GetSet(idSet).costPacks.Count; i++)
                {
                    if (ShopManager.shopData.GetSet(idSet).costPacks[i].currency.ToString() == "DIAMONDS")
                    {
                        di = ShopManager.shopData.GetSet(idSet).costPacks[i].cost;
                    }
                }

            //    GameManager.SubtractDiamonds(di);

                WWWForm datos = new WWWForm();
                WWW itemsData;
#if UNITY_ANDROID
                if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
                {
                    datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
                }
                else
                {
                    datos.AddField("idUsuario", Social.localUser.id.ToString());
                }
#elif UNITY_IOS
                if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
                {
                    datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
                }
                else
                {
                    datos.AddField("idUsuario", Social.localUser.id.ToString());
                }
                
#endif

                datos.AddField("idItem", idSet);
                datos.AddField("idMonopatin", idMonopatin);
                datos.AddField("idArma", idArma);
                datos.AddField("idBody", idBodySet);
                datos.AddField("diamantes", di);

                itemsData = new WWW("https://agapornigames.com/dodongo/guardarItemSet2.php", datos);

                yield return itemsData;

                Debug.Log(itemsData.text);

                int diBaseI = int.Parse(itemsData.text);
                uint diBase = 0;

                if (diBaseI < 0) diBase = 0;
                else diBase = (uint)diBaseI;

                GameDataController.gameData.diamondCurrency = diBase;
                GameDataController.Save();

                //      GameDataController.Save();
                // Debug.Log(itemsData.text);

                for (int i = 0; i < 50; i++)
                {
                    if (ControladorId.ObjetoCompradosPorTipo[3, i] == 0)
                    {
                        ControladorId.ObjetoCompradosPorTipo[3, i] = (uint)idSet;
                        break;
                    }
                }

                for (int i = 0; i < ObjetosComprados.Length; i++)
                {
                    if (ObjetosComprados[i] == 0)
                    {
                        ObjetosComprados[i] = idSet;
                        break;
                    }
                }

                skins.SetActive(true);
                monopatines.SetActive(true);
                armas.SetActive(true);

                skins.GetComponent<SkinsController>().AnadirObjetoALaCompra(idBodySet);
                monopatines.GetComponent<SkinsController>().AnadirObjetoALaCompra(idMonopatin);
                armas.GetComponent<SkinsController>().AnadirObjetoALaCompra(idArma);


                panelConfirmar.SetActive(false);
                panelDiamantes.SetActive(false);
                panelEyD.SetActive(false);
                panelEquiparse.SetActive(true);

                skins.SetActive(false);
                monopatines.SetActive(false);
                armas.SetActive(false);
            }
            else
            {
                popapConfirmarLogueo.SetActive(true);
                txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
            }

           
        }
    }

    public void ComprarItem()
    {
        StartCoroutine(ComprobarConexionCompra());
    }

    public void BtnRechazarMoney()
    {
        panelNoMoney.SetActive(false);
    }

    public void BtnComprarDiamantes()
    {
        panelNoMoney.SetActive(false);
        tgDiamonds.isOn = true;
    }

    public void BtnEquiparse()
    {
        for (int i = 0; i < setCambiado.Length; i++) setCambiado[i] = true;
        GameDataController.gameData.skateSkinEquipped = (uint)idMonopatin;
        GameDataController.gameData.weaponSkinEquipped = (uint)idArma;
        GameDataController.gameData.bodySkinEquipped = (uint)idBodySet;
        PlayerPrefs.SetInt("MonopatinSeleccionado", idMonopatin);
        GameDataController.Save();
    }
}
