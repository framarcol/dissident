﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkinsController : MonoBehaviour {

    public Image[] spritesSkins;
    public GameObject[] skins;
    public GameObject[] candados;
    public GameObject[] equipados;
    public int idListado;
    public int NumElementosTabla;
    public string nombreElementoAGuardar;
    public int numMaximoElementosParaComprar;
    public int idItemDefecto;
    int toguelAnterior;

    public SkinnedMeshRenderer renderBody;
    public MeshFilter renderArma;
    public MeshRenderer materialArma;

    public GameObject BotonIzq, BotonDer;
    public GameObject objetoMesh;
    GameObject miObjeto;
    public GameObject popapConfirmarLogueo;
    public Text txtPopapLogueo;

    public GameObject imagenPropiedadArma;
    public GameObject[] textosVersiones;
    public Sprite[] spritesPropiedadesArmas;

    int[] tablaIds;
    bool[] tablaCompradosId;
    int idToguelActual;
    int objetoEquipado;
    int[] ObjetosComprados; //En el futuro estara guardado en el save

    int comienzoIndice, numIndice, indiceElemento, posicionElementoIndice;

    bool horaDeCambiar;

    bool hayEstrellas;

    bool esFinal;

    public Text precioEstrellas;
    public Text precioDiamantes;
    public Text precioDiamantesPanelDiamantes;

    public GameObject panelEstrellasYDiamantes, panelDiamantes, panelEquiparse;

    public Animator popapInfo;
    public Text txtPopapInfo;

    public GameObject controladorIdMenu;

    //Estado del objeto 3D
    public GameObject imgEquip, imgCand;

    public GameObject popapConfirmarCompra;
    public GameObject noMoneyPopap;
    public Toggle tgDiamonds;

    public Toggle[] tgElementos;

    public Text letrasTitulo;

    void Update()
    {
           for(int i = 0; i < candados.Length; i++)
            {
            if(i == idToguelActual && tgElementos[idToguelActual].isOn)
            {
                Color miColor = candados[i].GetComponent<Image>().color;
                miColor.a = 0;
                candados[i].GetComponent<Image>().color = miColor;

                Color miColor2 = equipados[i].GetComponent<Image>().color;
                miColor2.a = 0;
                equipados[i].GetComponent<Image>().color = miColor;
            }
            else
            {
                Color miColor = candados[i].GetComponent<Image>().color;
                miColor.a = 1;
                candados[i].GetComponent<Image>().color = miColor;

                Color miColor2 = equipados[i].GetComponent<Image>().color;
                miColor2.a = 1;
                equipados[i].GetComponent<Image>().color = miColor;
            }
        }
        if (SetsController.setCambiado[idListado])
        {
            CargarElementoPrincipio();
        }

        if(comienzoIndice == 0)
        {
            BotonDer.SetActive(true);
            BotonIzq.SetActive(false);
        }


        int total = ShopManager.shopData.standardShopItems[idListado].standardItemList.Count / NumElementosTabla;

        if(ShopManager.shopData.standardShopItems[idListado].standardItemList.Count % NumElementosTabla != 0)
        {
            total++;
        }

        //Debug.Log(total + "/" + comienzoIndice);

        if(comienzoIndice == (total-1) * NumElementosTabla)
        {
            esFinal = true;
            BotonDer.SetActive(false);
            BotonIzq.SetActive(true);
        }



    }

    public void OnEnable()
    {
        miObjeto = null;
        tablaCompradosId = new bool[spritesSkins.Length];
        hayEstrellas = false;
        idToguelActual = -1;
        numIndice = 0;
        comienzoIndice = 0;
        indiceElemento = 0;
        ObjetosComprados = new int[numMaximoElementosParaComprar];      

        for (int i = 0; i < ObjetosComprados.Length; i++)
        {
            if (ControladorId.ObjetoCompradosPorTipo[idListado, i] != 0)
            {              
                ObjetosComprados[i] = (int)ControladorId.ObjetoCompradosPorTipo[idListado, i];
            //    Debug.Log(nombreElementoAGuardar + ": " + ObjetosComprados[i]);
            }
            else
            {
                ObjetosComprados[i] = idItemDefecto;
                break;
            }
        }

        tablaIds = new int[spritesSkins.Length];
        //GuardarObjketos para pruebas 

        CargarElementoPrincipio();
    }

    void OnDisable()
    {
        popapConfirmarCompra.SetActive(false);
        noMoneyPopap.SetActive(false);
        for(int i = 0; i < tgElementos.Length; i++)
        {
            tgElementos[i].isOn = false;
        }
    }

    public void CargarElementoPrincipio()
    {
        if (SetsController.setCambiado[idListado])
        {
            SetsController.setCambiado[idListado] = false;
            for (int i = 0; i < tgElementos.Length; i++) tgElementos[i].isOn = false;
        }

        for (int i = 0, j = 0, k = 0; i < ShopManager.shopData.standardShopItems[idListado].standardItemList.Count; i++)
        {
            if (k == NumElementosTabla)
            {
                k = 0;
                j += NumElementosTabla;
            }

            switch (nombreElementoAGuardar)
            {
                case "ArmaSeleccionada":
                    if (GameDataController.gameData.weaponSkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                    {
                        comienzoIndice = j;
                        idToguelActual = k;
                        toguelAnterior = idToguelActual;
                        imgCand.SetActive(false);
                        tablaIds[k] = (int)GameDataController.gameData.weaponSkinEquipped;
                        tgElementos[k].isOn = true;
                        panelDiamantes.SetActive(false);
                        panelEstrellasYDiamantes.SetActive(false);
                    }
                    break;
                case "SkinSeleccionada":
                    if (GameDataController.gameData.bodySkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                    {
                        comienzoIndice = j;
                        idToguelActual = k;
                        toguelAnterior = idToguelActual;
                        imgCand.SetActive(false);
                        tablaIds[k] = (int)GameDataController.gameData.bodySkinEquipped;
                        tgElementos[k].isOn = true;
                        panelDiamantes.SetActive(false);
                        panelEstrellasYDiamantes.SetActive(false);
                    }
                    break;
                case "MonopatinSeleccionado":
                    if (GameDataController.gameData.skateSkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                    {
                        comienzoIndice = j;
                        idToguelActual = k;
                        toguelAnterior = idToguelActual;
                        imgCand.SetActive(false);
                        tablaIds[k] = (int)GameDataController.gameData.skateSkinEquipped;
                        tgElementos[k].isOn = true;
                        panelDiamantes.SetActive(false);
                        panelEstrellasYDiamantes.SetActive(false);
                    }
                    break;
            }

            
            k++;
        }

        CargaSkins(comienzoIndice);

    }

    public void idTg(int id)
    {
        idToguelActual = id;
    }

    public void CambioSkin(bool Value)
    {
        imgEquip.SetActive(false);

        if (Value)
        {
            indiceElemento = numIndice;
            posicionElementoIndice = idToguelActual;        
            hayEstrellas = false;

            for (int i = 0; i < ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks.Count; i++)
            {
                
                if (ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "STARS")
                {
                    precioEstrellas.text = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    hayEstrellas = true;
                }
                else
                {
                    precioDiamantes.text = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    precioDiamantesPanelDiamantes.text = precioDiamantes.text; 
                }
            }

      //      Destroy(miObjeto);

            //miObjeto = (GameObject)Instantiate(ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).itemPresentation, objetoMesh.transform);
            //miObjeto.transform.localPosition = new Vector3(0, 0, 0);

            renderBody.sharedMesh = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
            renderBody.sharedMaterial = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).itemOverrideMaterial;

            letrasTitulo.text = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).itemName.ToString();

            if (tablaCompradosId[idToguelActual])
            {
                imgCand.SetActive(false);
                panelEstrellasYDiamantes.SetActive(false);
                panelDiamantes.SetActive(false);

                if (GameDataController.gameData.bodySkinEquipped != ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).itemId)
                {
                    panelEquiparse.SetActive(true);              
                }
                else
                {
                    panelEquiparse.SetActive(false);
                    imgEquip.SetActive(true);
                }
         
            }
            else
            {
                imgCand.SetActive(true);
                if (hayEstrellas)
                {
                    panelEstrellasYDiamantes.SetActive(true);
                    panelDiamantes.SetActive(false);
                    panelEquiparse.SetActive(false);
                }
                else
                {
                    panelEstrellasYDiamantes.SetActive(false);
                    panelDiamantes.SetActive(true);
                    panelEquiparse.SetActive(false);
                }
            }         
        }
     

    }

    public void CambioSkinArmas(bool Value)
    {
        imgEquip.SetActive(false);

        if (Value)
        {
            indiceElemento = numIndice;
            posicionElementoIndice = idToguelActual;
            hayEstrellas = false;

            for (int i = 0; i < ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
            {

                if (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "STARS")
                {
                    precioEstrellas.text = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    hayEstrellas = true;
                }
                else
                {
                    precioDiamantes.text = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    precioDiamantesPanelDiamantes.text = precioDiamantes.text;
                }
            }

          
            renderArma.sharedMesh = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).itemPresentation.GetComponentInChildren<MeshFilter>().sharedMesh;
 
            materialArma.sharedMaterial = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).itemOverrideMaterial;

            switch(ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).itemDescription.ToUpper())
            {
                case "AMMO":
                    imagenPropiedadArma.GetComponent<Image>().sprite = spritesPropiedadesArmas[0];
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    textosVersiones[0].SetActive(true);
                    textosVersiones[1].SetActive(true);
                    textosVersiones[0].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().effectProbability * 100).ToString() + "%";
                    textosVersiones[1].GetComponent<Text>().text = "+" + ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().ammoRefill.GetComponent<AmmoRefill>().ammo.ToString();
                    break;
                case "IDOL":
                    imagenPropiedadArma.GetComponent<Image>().sprite = spritesPropiedadesArmas[1];
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    textosVersiones[2].SetActive(true);
                    imagenPropiedadArma.SetActive(true);
                    textosVersiones[2].GetComponent<Text>().text = "x " + ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<IdolCatcherProjectile>().startingWeaponUses.ToString();
                    break;
                case "SCORE":
                    imagenPropiedadArma.GetComponent<Image>().sprite = spritesPropiedadesArmas[2];
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    imagenPropiedadArma.SetActive(true);
                    textosVersiones[3].SetActive(true);
                    textosVersiones[4].SetActive(true);
                    textosVersiones[3].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<ScoreBoosterProjectile>().effectProbability * 100).ToString() + "%";
                    textosVersiones[4].GetComponent<Text>().text = "x " + ((ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<ScoreBoosterProjectile>().scoreBoost) + 1).ToString();
                    break;
                case "HEAL":
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    imagenPropiedadArma.SetActive(true);
                    textosVersiones[5].SetActive(true);
                    textosVersiones[5].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<AmmoRefillerProjectile>().effectProbability * 100).ToString() + "%";
                    imagenPropiedadArma.GetComponent<Image>().sprite = spritesPropiedadesArmas[3];
                    break;
                case "RICOCHET":
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    imagenPropiedadArma.SetActive(true);
                    textosVersiones[6].SetActive(true);
                    textosVersiones[6].GetComponent<Text>().text = (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).lowpolyItemPresentation.GetComponent<Weapon>().projectilePrefab.GetComponent<RicochetProjectile>().effectProbability * 100).ToString() + "%";
                    imagenPropiedadArma.GetComponent<Image>().sprite = spritesPropiedadesArmas[4];
                    break;
                default:
                    for (int i = 0; i < textosVersiones.Length; i++) textosVersiones[i].SetActive(false);
                    imagenPropiedadArma.SetActive(false);
                    break;
            }

            letrasTitulo.text = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).itemName.ToString();

            if (tablaCompradosId[idToguelActual])
            {
                imgCand.SetActive(false);
                panelEstrellasYDiamantes.SetActive(false);
                panelDiamantes.SetActive(false);
                if (GameDataController.gameData.weaponSkinEquipped != ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).itemId)
                {
                    panelEquiparse.SetActive(true);

                }
                else
                {
                    panelEquiparse.SetActive(false);
                    imgEquip.SetActive(true);
                }

            }
            else
            {
                imgCand.SetActive(true);
                if (hayEstrellas)
                {
                    panelEstrellasYDiamantes.SetActive(true);
                    panelDiamantes.SetActive(false);
                    panelEquiparse.SetActive(false);
                }
                else
                {
                    panelEstrellasYDiamantes.SetActive(false);
                    panelDiamantes.SetActive(true);
                    panelEquiparse.SetActive(false);
                }
            }


        }
    }

    public void CambioSkinMonopatines(bool Value)
    {
        imgEquip.SetActive(false);

        if (Value)
        {
            indiceElemento = numIndice;
            posicionElementoIndice = idToguelActual;
            hayEstrellas = false;

            for (int i = 0; i < ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
            {

                if (ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "STARS")
                {
                    precioEstrellas.text = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    hayEstrellas = true;
                }
                else
                {
                    precioDiamantes.text = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].cost.ToString();
                    precioDiamantesPanelDiamantes.text = precioDiamantes.text;
                }
            }

            //       Destroy(miObjeto);

            //     miObjeto = (GameObject)Instantiate(ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemPresentation, objetoMesh.transform);
            //   miObjeto.transform.localPosition = new Vector3(0, 0, 0);

            //   miObjeto.GetComponentInChildren<SkinnedMeshRenderer>().material = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemOverrideMaterial;
            if(tablaIds[idToguelActual] == 72)
            {
                renderBody.sharedMesh = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemPresentation.GetComponentInChildren<MeshFilter>().sharedMesh;
            }
            else
            {
                renderBody.sharedMesh = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
            }
           

            renderBody.sharedMaterial = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemOverrideMaterial;

            letrasTitulo.text = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemName.ToString();

            if (tablaCompradosId[idToguelActual])
            {
                imgCand.SetActive(false);
                panelEstrellasYDiamantes.SetActive(false);
                panelDiamantes.SetActive(false);
                if (GameDataController.gameData.skateSkinEquipped != ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).itemId) panelEquiparse.SetActive(true);
                else
                {
                    panelEquiparse.SetActive(false);
                    imgEquip.SetActive(true);
                }

            }
            else
            {
                imgCand.SetActive(true);
                if (hayEstrellas)
                {
                    panelEstrellasYDiamantes.SetActive(true);
                    panelDiamantes.SetActive(false);
                    panelEquiparse.SetActive(false);
                }
                else
                {
                    panelEstrellasYDiamantes.SetActive(false);
                    panelDiamantes.SetActive(true);
                    panelEquiparse.SetActive(false);
                }
            }


        }
    }

    public void BtnIzquierda()
    {
        if (comienzoIndice > 0)
        {
            for (int i = 0; i < tgElementos.Length; i++)
            {
                if (tgElementos[i].isOn == true)
                {
                    indiceElemento = numIndice;
                    posicionElementoIndice = i;
                    // indiceElementoGuardado = tablaIds[i];
                }
                tgElementos[i].isOn = false;
            }

            comienzoIndice -= NumElementosTabla;

            //BotonDer.SetActive(true);
            numIndice--;

            for (int i = 0; i < tgElementos.Length; i++)
            {
                if (indiceElemento == numIndice && posicionElementoIndice == i)
                {
                    // tablaIds[i] = indiceElementoGuardado;
                    horaDeCambiar = true;
                  //  tgElementos[i].isOn = true;
                }
            }

            CargaSkins(comienzoIndice);

        } 

        if(esFinal)
        {
            BotonDer.SetActive(true);
            esFinal = false;
        }
    }

    public void BtnDerecha()
    {
        for (int i = 0; i < tgElementos.Length; i++)
        {
            if (tgElementos[i].isOn == true)
            {
                indiceElemento = numIndice;
                posicionElementoIndice = i;
            }
            tgElementos[i].isOn = false;
        }

        comienzoIndice += NumElementosTabla;
        numIndice++;

        for (int i = 0; i < tgElementos.Length; i++)
        {
            if (indiceElemento == numIndice && posicionElementoIndice == i)
            {
                horaDeCambiar = true;
            //    tgElementos[i].isOn = true;
            }
        }

        CargaSkins(comienzoIndice);

    }

    void CargaSkins(int indice)
    {
        for (int i = 0; i < skins.Length; i++)
        {
            skins[i].SetActive(false);
            candados[i].SetActive(true);
            equipados[i].SetActive(false);
            tablaCompradosId[i] = false;
        }
        

        for (int i = indice, j = 0; j < spritesSkins.Length ; i++, j++)
        {
            if(i < ShopManager.shopData.standardShopItems[idListado].standardItemList.Count)
            {
                spritesSkins[j].sprite = ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemThumbnail;
                tablaIds[j] = ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId;
                skins[j].SetActive(true);
                switch (nombreElementoAGuardar)
                {
                    case "ArmaSeleccionada":
                        if (GameDataController.gameData.weaponSkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                        {
                            imgCand.SetActive(false);
                            imgEquip.SetActive(true);
                            equipados[j].SetActive(true);
                            candados[j].SetActive(false);
                            objetoEquipado = j;
                        }
                        break;
                    case "SkinSeleccionada":
                        if (GameDataController.gameData.bodySkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                        {
                            imgCand.SetActive(false);
                            imgEquip.SetActive(true);
                            equipados[j].SetActive(true);
                            candados[j].SetActive(false);
                            objetoEquipado = j;
                        }
                        break;
                    case "MonopatinSeleccionado":
                        if (GameDataController.gameData.skateSkinEquipped == ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId)
                        {
                            imgCand.SetActive(false);
                            imgEquip.SetActive(true);
                            equipados[j].SetActive(true);
                            candados[j].SetActive(false);
                            objetoEquipado = j;
                        }
                        break;
                }
              
            }
           // else BotonDer.SetActive(false);

            for(int k = 0; k < ObjetosComprados.Length; k++)
            {
                if (i < ShopManager.shopData.standardShopItems[idListado].standardItemList.Count)
                {
                    //if (ObjetosComprados[k] == 22) Debug.Log("Soy 22");
                    if (ShopManager.shopData.standardShopItems[idListado].standardItemList[i].itemId == ObjetosComprados[k]) {
                        candados[j].SetActive(false);
                        tablaCompradosId[j] = true;
                    } 
                }
            }
            
        }

        if (horaDeCambiar)
        {
            tgElementos[posicionElementoIndice].isOn = true;
            horaDeCambiar = false;
        }

        if (comienzoIndice == 0)  BotonIzq.SetActive(false);
        else BotonIzq.SetActive(true);
    }

    public void IniciarCompra()
    {
        ComprobarConexion();
        /*  if (!CargaIntro.modoOffline)
          {
  #if UNITY_ANDROID
              if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
              {
                  StartCoroutine(ComprobarConexion());
              }
              else
              {
                  if (Social.localUser.authenticated)
                  {
                      StartCoroutine(ComprobarConexion());
                  }
                  else
                  {
                      popapInfo.SetTrigger("Fade");
                      txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                      controladorIdMenu.GetComponent<ControladorId>().Autentificar();
                  }
              }
  #elif UNITY_IOS
              if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
              {
                  StartCoroutine(ComprobarConexion());
              }
              else
              {
                  if (Social.localUser.authenticated)
                  {
                      StartCoroutine(ComprobarConexion());
                  }
                  else
                  {
                      popapInfo.SetTrigger("Fade");
                      txtPopapInfo.text = SeleccionaIdiomas.gameText[20, 11];
                      controladorIdMenu.GetComponent<ControladorId>().Autentificar();
                  }
              }
  #endif

          }
          else
          {
              popapInfo.SetTrigger("Fade");
              txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 17];
          }*/
    }

    public void ComprobarConexion()
    {
      /*  WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16]; ;
        }
        else
        {*/
            int di = 0;
            switch (nombreElementoAGuardar)
            {
                case "ArmaSeleccionada":
                    for (int i = 0; i < ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                    {
                        if (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                        {
                            di = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].cost;
                        }
                    }

                    break;
                case "SkinSeleccionada":
                    for (int i = 0; i < ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                    {
                        if (ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                        {
                            di = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].cost;
                        }
                    }

                    break;
                case "MonopatinSeleccionado":
                    for (int i = 0; i < ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                    {
                        if (ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                        {
                            di = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].cost;
                        }
                    }

                    break;
            }

            int diamantesRestantes = (int)(GameDataController.gameData.diamondCurrency - di);

            if (diamantesRestantes < 0)
            {
                noMoneyPopap.SetActive(true);
            }
            else
            {
                popapConfirmarCompra.SetActive(true);
            }
        //}
    }

    public void RechazarCompra()
    {
        popapConfirmarCompra.SetActive(false);
    }

    public void ComprobarConexionCompra()
    {
     /*   WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            popapInfo.SetTrigger("Fade");
            txtPopapInfo.text = SeleccionaIdiomas.gameText[21, 16];
        }
        else
        {*/
          /*  if (ControladorId.Logueado)
            {*/
                int di = 0;
                switch (nombreElementoAGuardar)
                {
                    case "ArmaSeleccionada":
                        for (int i = 0; i < ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                        {
                            if (ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                            {
                                di = ShopManager.shopData.GetWeaponSkin(tablaIds[idToguelActual]).costPacks[i].cost;
                            }
                        }

                        break;
                    case "SkinSeleccionada":
                        for (int i = 0; i < ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                        {
                            if (ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                            {
                                di = ShopManager.shopData.GetBodySkin(tablaIds[idToguelActual]).costPacks[i].cost;
                            }
                        }

                        break;
                    case "MonopatinSeleccionado":
                        for (int i = 0; i < ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks.Count; i++)
                        {
                            if (ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].currency.ToString() == "DIAMONDS")
                            {
                                di = ShopManager.shopData.GetSkateSkin(tablaIds[idToguelActual]).costPacks[i].cost;
                            }
                        }

                        break;
                }

              //   GameManager.SubtractDiamonds(di);
             //   GameDataController.Save();
 /*               WWWForm datos = new WWWForm();
                WWW itemsData;

                datos.AddField("idTipo", idListado);
#if UNITY_ANDROID
                if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
                {
                    datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
                }
                else
                {
                    datos.AddField("idUsuario", Social.localUser.id.ToString());
                }
#elif UNITY_IOS
                if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
                {
                    datos.AddField("idUsuario", SystemInfo.deviceUniqueIdentifier);
                }
                else
                {
                    datos.AddField("idUsuario", Social.localUser.id.ToString());
                }
                    
#endif

                datos.AddField("idItem", tablaIds[idToguelActual]);
                datos.AddField("diamantes", di);

                itemsData = new WWW("https://agapornigames.com/dodongo/guardarItem2.php", datos);

                yield return itemsData;

                int diBaseI = int.Parse(itemsData.text);
                uint diBase = 0;

                if (diBaseI < 0) diBase = 0;
                else diBase = (uint)diBaseI;*/

                GameDataController.gameData.diamondCurrency -= (uint)di;
                GameDataController.Save();

                //    Debug.Log(itemsData.text);

                for (int i = 0; i < 50; i++)
                {
                    if (ControladorId.ObjetoCompradosPorTipo[idListado, i] == 0)
                    {
                        ControladorId.ObjetoCompradosPorTipo[idListado, i] = (uint)tablaIds[idToguelActual];
                        break;
                    }
                }

                for (int i = 0; i < ObjetosComprados.Length; i++)
                {
                    if (ObjetosComprados[i] == 0)
                    {
                        ObjetosComprados[i] = tablaIds[idToguelActual];
                        break;
                    }
                }

                CargaSkins(comienzoIndice);
                popapConfirmarCompra.SetActive(false);
                panelDiamantes.SetActive(false);
                panelEstrellasYDiamantes.SetActive(false);
                panelEquiparse.SetActive(true);
                imgCand.SetActive(false);
           /* }
            else
            {
                popapConfirmarLogueo.SetActive(true);
                txtPopapLogueo.text = SeleccionaIdiomas.gameText[22, 11];
            }*/
          
      //  }
    }

    public void ComprarItem()
    {
        ComprobarConexionCompra();
    }

    public void BtnRechazarMoney()
    {
        noMoneyPopap.SetActive(false);
    }

    public void BtnComprarDiamantes()
    {
        noMoneyPopap.SetActive(false);
        tgDiamonds.isOn = true;
    }

    public void BotonEquiparse()
    {
        for(int i = 0; i < equipados.Length; i++)
        {
            equipados[i].SetActive(false);
        }
        equipados[idToguelActual].SetActive(false);
        switch (nombreElementoAGuardar)
        {
            case "ArmaSeleccionada":
                GameDataController.gameData.weaponSkinEquipped = (uint)tablaIds[idToguelActual];
                break;
            case "SkinSeleccionada":
                GameDataController.gameData.bodySkinEquipped = (uint)tablaIds[idToguelActual];
                break;
            case "MonopatinSeleccionado":
                GameDataController.gameData.skateSkinEquipped = (uint)tablaIds[idToguelActual];
                break;
        }

        GameDataController.Save();
        panelEquiparse.SetActive(false);
        imgEquip.SetActive(true);
        CargaSkins(comienzoIndice);
    }

    public void AnadirObjetoALaCompra(int idObjeto)
    {
        for (int i = 0; i < ObjetosComprados.Length; i++)
        {
            if (ObjetosComprados[i] == 0)
            {
                ObjetosComprados[i] = idObjeto;
                break;
            }
        }

    }

}
