﻿using UnityEngine;
using System.Collections;

public class TgUpgrades : MonoBehaviour {

    public GameObject barra;
    public GameObject panel;
    public GameObject panelConfirmar;
    public GameObject NoMoney;

	public void OnValueChanged(bool Value)
    {
        if (Value)
        {
            barra.gameObject.SetActive(true);
            panel.gameObject.SetActive(true);
            panelConfirmar.SetActive(false);
            NoMoney.SetActive(false);
        }
        else
        {
            panel.gameObject.SetActive(false);
            barra.gameObject.SetActive(false);
            panelConfirmar.SetActive(false);
            NoMoney.SetActive(false);
        }
    }
}
