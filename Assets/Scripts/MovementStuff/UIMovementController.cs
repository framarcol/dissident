﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIMovementController : MonoBehaviour {

    public PlayerMovementController movementController;
    //public Camera uiCamera;

    public float dashTolerance = 50f;
    public float continuousTolerance = 100f;

    RectTransform screenPanel;

    bool touchStarted = false;
    int fingerMovementId = -1;
    Vector2 initPos;
    Vector2 lastPos;
    bool longMoveMade = false;
    
    Vector2 moveVector;

    // Use this for initialization
    void Start() {

        screenPanel = GetComponent<RectTransform>();

        if(movementController == null /*|| uiCamera == null*/) {

            Debug.LogError("UIMovementController - Some variables must be assigned");
            enabled = false;

        }else if (screenPanel == null) {

            Debug.LogError("UIMovementController - This object must be an UI object with its RectTransform component");
            enabled = false;

        }

    }

    // Update is called once per frame
    void Update() {

        //If the player cant dash then cancel the touches
        /*if (!movementController.canDash) {

            //Reset the variables
            touchStarted = false;
            fingerMovementId = -1;

            return;

        }*/

        foreach (Touch touch in Input.touches) {

            switch (touch.phase) {

                case TouchPhase.Began:

                    //If there's already an ID don't continue
                    if (fingerMovementId != -1) break;

                    //If the touch began on the selected panel...
                    if (RectTransformUtility.RectangleContainsScreenPoint(screenPanel, touch.position)) {

                        //If the player is already moving then don't do anything
                        //if (movementController.dashingPlayer) break;

                        //Initialize related variables (getting the id touch reference)
                        fingerMovementId = touch.fingerId;
                        initPos = touch.position;
                        touchStarted = true;

                    }
                    break;

                case TouchPhase.Moved:
                case TouchPhase.Stationary:

                    //If the touch already started and it's the same finger (id)...
                    if (touchStarted && fingerMovementId == touch.fingerId) {

                        //If the player is already moving then don't do anything
                        //if (movementController.dashingPlayer) break;

                        //Get the touched vector and check if a long/continuous move has been made
                        Vector2 actualVector = touch.position - initPos;

                        if (actualVector.x >= continuousTolerance) {
                            //movementController.CalculateAndDashRight();
                            longMoveMade = true;
                        } else if (actualVector.x <= -continuousTolerance) {
                            //movementController.CalculateAndDashLeft();
                            longMoveMade = true;
                        }

                    }
                    break;

                case TouchPhase.Ended:

                    //If the touch already started and it's the same finger (id)...
                    if (touchStarted && fingerMovementId == touch.fingerId) {
                        
                        //Reset the variables
                        touchStarted = false;
                        fingerMovementId = -1;

                        //Check if a long move has been made, in that case dont do anything
                        if (longMoveMade) break;

                        //Get the end position and calculate the dash movement for the player
                        lastPos = touch.position;
                        CalculateMovement();

                    }
                    break;

            }

        }

    }

    public void CalculateMovement() {

        //Calculate the movement vector
        moveVector = lastPos - initPos;

        //Check if the vector is long enough to make a dash
        //if (moveVector.x >= dashTolerance) movementController.CalculateAndDashRight();
        //else if (moveVector.x <= -dashTolerance) movementController.CalculateAndDashLeft();

    }

}
