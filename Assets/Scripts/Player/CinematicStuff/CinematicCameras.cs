﻿using UnityEngine;
using System.Collections;

public class CinematicCameras : MonoBehaviour {

    [System.Serializable]
    public struct CinematicCamera {

        public string cameraName;
        public Transform cameraTransform;

    }

    public CinematicCamera[] cinematicCameras;
    
    public Transform GetCinematicCamera(string name) {

        Transform cameraTransform = null;

        for(int i = 0; i < cinematicCameras.Length; i++) {

            if(cinematicCameras[i].cameraName == name) {

                cameraTransform = cinematicCameras[i].cameraTransform;

            }

        }

        if (cameraTransform != null) cameraTransform.parent.gameObject.SetActive(true);
        return cameraTransform;

    }

    public void DisableCamera(string name) {

        for (int i = 0; i < cinematicCameras.Length; i++) {

            if (cinematicCameras[i].cameraName == name) {

                cinematicCameras[i].cameraTransform.parent.gameObject.SetActive(false);
                break;

            }

        }

    }

}
