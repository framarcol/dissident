﻿using UnityEngine;
using System.Collections;

public class JailCin : MonoBehaviour {

    public bool exit;

    public GameObject door;
    public GameObject explosion;

    bool entering = false;

    PlayerMovementController playerMovementController;
    PlayerCinematicController playerCinematicController;
    PlayerAnimEvents playerAnimEvents;
    
	// Use this for initialization
	void Start () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        playerCinematicController = playerMovementController.GetComponent<PlayerCinematicController>();

        playerAnimEvents = playerMovementController.GetComponentInChildren<PlayerAnimEvents>();

	}
	
	// Update is called once per frame
	void Update () {

        if (exit) return;

        if (entering && playerAnimEvents.doorShoot) {

            playerAnimEvents.doorShoot = false;
            entering = false;

            explosion.SetActive(true);
            door.SetActive(false);

        }

	}

    public void GoIntroJail() {

        playerCinematicController.JailIntro();

    }

    public void SideLimitNoJump() {

        playerMovementController.ChangeSideLimit(0f, 0f);
        playerMovementController.canJump = false;

        entering = true;

    }

    public void GoExitJail() {

        playerCinematicController.JailExit();

    }

}
