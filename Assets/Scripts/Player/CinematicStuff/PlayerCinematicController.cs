﻿using UnityEngine;
using System.Collections;

public class PlayerCinematicController : MonoBehaviour {

    public GameObject startPlatform;
    public bool initLevel;
    
    PlayerMovementController playerMovementController;
    ShootController shootController;
    CameraFollowingController cameraFollowingController;

    CinematicCameras cinematicCameras;

    CanvasGroup inGameHUD;
    AmmoUI ammoUI;

	// Use this for initialization
	void Awake () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        shootController = GameObject.FindGameObjectWithTag("Player").GetComponent<ShootController>();
        cameraFollowingController = FindObjectOfType<CameraFollowingController>();

        cinematicCameras = FindObjectOfType<CinematicCameras>();

        inGameHUD = GameObject.FindGameObjectWithTag("HUD").transform.Find("InGameHUD").GetComponent<CanvasGroup>();
        ammoUI = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<AmmoUI>(true);

        if (initLevel) InitLevelCinematic();
        else if (GameManager.activeCheckpoint != -1) ammoUI.gameObject.SetActive(true); 

    }

    //INIT LEVEL
    public void InitLevelCinematic() {
        
        playerMovementController.canMove = false;
        playerMovementController.canMoveToSides = false;
        playerMovementController.canJump = false;

        shootController.canShoot = false;

        playerMovementController.playerAnimator.transform.localPosition = new Vector3(0f, 2.472386f, -3.505395f);

        playerMovementController.playerAnimator.SetBool("initLevel", true);
        playerMovementController.skateAnimator.SetBool("initLevel", true);

        Transform initLevelCamera = cinematicCameras.GetCinematicCamera("InitLevelCamera");
        cameraFollowingController.transform.position = initLevelCamera.position;
        cameraFollowingController.transform.rotation = initLevelCamera.rotation;
        cameraFollowingController.SetCameraArm(initLevelCamera, CameraArmPositions.Default);
        

        inGameHUD.interactable = false;
        if(ammoUI != null) ammoUI.gameObject.SetActive(false);

        if (startPlatform != null) startPlatform.SetActive(true);
        StartCoroutine(LightFade());
        
    }

    IEnumerator LightFade() {

        Light directionalLight = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();

        directionalLight.intensity = 0.1f;

        yield return new WaitForSeconds(1.5f);

        while(directionalLight.intensity < 1f) {

            directionalLight.intensity += 0.75f * Time.deltaTime;

            yield return null;

        }

        directionalLight.intensity = 1f;

    }

    //JAIL INTRO
    public void JailIntro() {

        if (inGameHUD != null) inGameHUD.interactable = false;

        playerMovementController.playerAnimator.SetTrigger("goRun");
        playerMovementController.skateAnimator.SetTrigger("goRun");

        playerMovementController.playerAnimator.SetBool("running", true);
        playerMovementController.isRunning = true;

        playerMovementController.canJump = false;
        playerMovementController.ChangeSideLimit(0, 0);

    }

    public void JailExit() {

        if (inGameHUD != null) inGameHUD.interactable = false;

        //playerMovementController.skateAnimator.transform.Find("Graphics").gameObject.SetActive(true);

        playerMovementController.playerAnimator.SetTrigger("goSkate");
        playerMovementController.skateAnimator.SetTrigger("goSkate");

        playerMovementController.playerAnimator.SetBool("running", false);
        playerMovementController.isRunning = false;
        
    }

}
