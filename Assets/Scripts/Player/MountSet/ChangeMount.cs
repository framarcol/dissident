﻿using UnityEngine;
using System.Collections;

public class ChangeMount : MonoBehaviour {

    public PlayerMountSet.Mount newMount;

    public bool immediately = false;

    bool entered = false;

    PlayerMovementController playerMovementController;
    ScreenEffects screenEffects;

    void Start() {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        screenEffects = GameObject.FindGameObjectWithTag("ScreenEffects").GetComponent<ScreenEffects>();

    }

    void OnTriggerEnter(Collider other) {
        
        if(!entered && other.tag == "Player") {

            entered = true;

            if (immediately) {

                TriggerChangeMount(newMount);
                
            } else {

                switch (newMount) {

                    case PlayerMountSet.Mount.SKATE:
                        playerMovementController.SetTrigger("goSkate");
                        break;

                    case PlayerMountSet.Mount.BIKE:
                        playerMovementController.SetTrigger("goMoto");
                        break;

                    case PlayerMountSet.Mount.LAEGOO:
                        playerMovementController.SetTrigger("goLaegoo");
                        break;

                    case PlayerMountSet.Mount.BEAR:
                        playerMovementController.SetTrigger("goBear");
                        break;

                    case PlayerMountSet.Mount.GORILLA:
                        playerMovementController.SetTrigger("goGorilla");
                        break;
                        
                }
                
            }

            //playerMovementController.canJump = true;

        }

    }

    public void TriggerChangeMount(PlayerMountSet.Mount newMount) {

        playerMovementController.ChangeMount(newMount);

    }

    public void TriggerFadeOut() {

        screenEffects.EnableEffect("DarkFade", false);

    }

    public void TriggerFadeIn() {

        screenEffects.EnableEffect("DarkFade", true);

    }

    public void DisableJump() {

        playerMovementController.canJump = false;

    }

}
