﻿using UnityEngine;
using System.Collections;

public class Mounts : MonoBehaviour {

    [System.Serializable]
	public struct Mount {

        public PlayerMountSet.Mount mountType;
        public GameObject mountObject;
        public Transform topMountBone;

    }

    public int activeMount = 0;

    public Mount[] mounts;

}
