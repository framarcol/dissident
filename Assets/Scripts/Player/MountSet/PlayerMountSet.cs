﻿using UnityEngine;

public class PlayerMountSet : ScriptableObject {

    public enum Mount { SKATE = 0, LAEGOO = 1, BEAR = 2, BIKE = 3, GORILLA = 4, NO_MOUNT = 4 }

    [System.Serializable]
    public struct MountSetup {

        public Mount mountType;

        public RuntimeAnimatorController playerMountAnimator;

        public float playerTargetHeight;

        public GameObject mountRagdoll;
        
    }

    public MountSetup[] mountSet;

}
