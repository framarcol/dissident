﻿using UnityEngine;
using System.Collections;

public class SkateFakeMount : MonoBehaviour {

    PlayerConfiguration playerConfiguration;

	// Use this for initialization
	void Start () {

        playerConfiguration = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerConfiguration>();

        if (playerConfiguration.skateSkin != null) {

            SkinnedMeshRenderer skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();

            skinnedMeshRenderer.sharedMesh = playerConfiguration.skateSkin.itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
            skinnedMeshRenderer.sharedMaterial = playerConfiguration.skateSkin.itemOverrideMaterial;

        }

    }

}
