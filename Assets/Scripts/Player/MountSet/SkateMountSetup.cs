﻿using UnityEngine;
using System.Collections;

public class SkateMountSetup : MonoBehaviour {

    PlayerConfiguration playerConfiguration;
    PlayerHealth playerHealth;
    Mounts mountList;

    PlayerMovementController playerMovementController;

	void Start () {

        playerConfiguration = Resources.Load("DataObjects/PlayerConfiguration", typeof(PlayerConfiguration)) as PlayerConfiguration;

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        playerHealth = playerMovementController.GetComponentInChildren<PlayerHealth>();
        mountList = playerMovementController.GetComponentInChildren<Mounts>();

        ConfigurateSkate();

    }
	
	private void ConfigurateSkate() {

        /*if (playerConfiguration.skateId == 0) return;

        //Disable basic skate & enable new skate
        transform.GetChild(0).gameObject.SetActive(false);
        GameObject skate = transform.GetChild(playerConfiguration.skateId).gameObject;
        skate.SetActive(true);

        //Set related stuff
        playerMovementController.skateAnimator = skate.GetComponent<Animator>();
        //TODO playerHealth.skateRagdoll = skate_ragdoll;
        mountList.mounts[0].mountObject = skate;
        //TODO mountList.mounts[0].topMountBone = hueso_skate;
        */


    }

}
