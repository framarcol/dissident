﻿using UnityEngine;
using System.Collections;

public class PlayerAmbienceController : MonoBehaviour {

    public ParticleSystem ambienceSystem;
    public bool enable = false;

    bool entered = false;

    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            if (enable) ambienceSystem.Play();
            else ambienceSystem.Stop();

        }

    }

}
