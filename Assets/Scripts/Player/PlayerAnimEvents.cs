﻿using UnityEngine;
using System.Collections;

public class PlayerAnimEvents : MonoBehaviour {

    PlayerMovementController playerMovementController;
    ShootController shootController;

    CameraFollowingController cameraFollowingController;
    CameraMovementController cameraMovementController;

    CinematicCameras cinematicCameras;

    CanvasGroup inGameHUD;
    AmmoUI ammoUI;

    SkateSoundController skateSoundController;

    //PauseUI pause;
    //GameManager gameManager;

    //[Header("Shoot stuff")]
    public Transform[] attachLocations;
    public Weapon weapon;
    /*public GameObject leftMuzzleFlash;
    public GameObject rightMuzzleFlash;*/

    //INTRO JAIL
    [Header("Jail stuff")]
    public bool doorShoot;
    bool loweringSpeed;

    public delegate void PlayerAnimDelegate();
    public static PlayerAnimDelegate OnPlayerInitLevel;

    //float targetTimeScale = 1.0f;

    void Awake() {

        OnPlayerInitLevel = null;

    }

	// Use this for initialization
	void Start () {

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        shootController = GameObject.FindGameObjectWithTag("Player").GetComponent<ShootController>();

        cameraFollowingController = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<CameraFollowingController>();
        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();

        cinematicCameras = GameObject.FindGameObjectWithTag("Player").transform.parent.gameObject.GetComponentInChildren<CinematicCameras>();

        inGameHUD = GameObject.FindGameObjectWithTag("HUD").transform.Find("InGameHUD").GetComponent<CanvasGroup>();
        ammoUI = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<AmmoUI>(true);

        skateSoundController = playerMovementController.GetComponentInChildren<SkateSoundController>();

        weapon.SetWeaponPosition(attachLocations);

        //pause = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<PauseUI>();

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

    }

    /*void Update() {

        //if(GameManager.gameState != GameState.PAUSED && targetTimeScale != 1.0f) Time.timeScale = Mathf.Lerp(Time.timeScale, targetTimeScale, 5f * Time.deltaTime);

        Debug.Log(Time.timeScale);

    }*/

    public void SlowMo(float targetTimeScale) {

        //this.targetTimeScale = targetTimeScale;

        StartCoroutine(TimeScaling(targetTimeScale));

    }

    /*public void StopSlowMo() {

        StartCoroutine(TimeScaling(1));

    }*/

    IEnumerator TimeScaling(float targetScale) {

        while(Mathf.Abs(Time.timeScale - targetScale) > 0.075f) {

            Time.timeScale = Mathf.Lerp(Time.timeScale, targetScale, 5f * Time.deltaTime);

            //Debug.Log((Mathf.Abs(Time.timeScale - targetScale)));

            yield return null;

        }

        Time.timeScale = targetScale;

    }

    public void StartLoweringSpeed() {

        loweringSpeed = true;

        StartCoroutine(LowerSpeed());

    }

    public void StopLoweringSpeed() {

        loweringSpeed = false;

        //

    }

    IEnumerator LowerSpeed() {

        while (loweringSpeed) {

            //Time.timeScale = Mathf.Clamp(playerMovementController.playerAnimator.GetFloat("introJailCinematicSpeed") + 0.4f, 0.4f, 1f );

            playerMovementController.SetSpeed(playerMovementController.playerAnimator.GetFloat("CinematicSpeed") * playerMovementController.initSpeed);

            yield return null;

        }

    }

    //SHOOT STUFF
    public void ShootProjectiles(int selection) {

        weapon.ShootProjectiles(selection);

        /*if (selection == 1) weaponPair.left.ShootProjectiles();
        else if (selection == 2) weaponPair.right.ShootProjectiles();
        else {

            weaponPair.left.ShootProjectiles();
            weaponPair.right.ShootProjectiles();

        }*/

    }

    public void ShowMuzzleFlash(int selection) {

        weapon.ShowMuzzleFlash(selection);

        /*if(selection == 1) weaponPair.left.ShowMuzzleFlash();
        else if(selection == 2) weaponPair.right.ShowMuzzleFlash();
        else {

            weaponPair.left.ShowMuzzleFlash();
            weaponPair.right.ShowMuzzleFlash();*/
            
    }

    public void HideMuzzleFlash() {

        weapon.HideMuzzleFlash();

        /*weaponPair.left.HideMuzzleFlash();
        weaponPair.right.HideMuzzleFlash();*/

    }

    //INIT LEVEL
    //Called when Init Level cinematic ends
    public void InitLevelEnd() {

        playerMovementController.canMove = true;
        playerMovementController.canMoveToSides = true;
        playerMovementController.canJump = true;

        shootController.canShoot = true;

        if(ammoUI != null && ammoUI.ammoDisplayOption == 0) ammoUI.gameObject.SetActive(true);

        //playerMovementController.playerAnimator.transform.Find("Projector").gameObject.SetActive(false);

        cameraFollowingController.SetCameraArm(cameraMovementController.followingTarget, CameraArmPositions.Default);

        cinematicCameras.DisableCamera("InitLevelCamera");

        inGameHUD.interactable = true;

        if (skateSoundController != null) skateSoundController.TriggerFlySkate();

        if(OnPlayerInitLevel != null) OnPlayerInitLevel();

    }

    //JAIL INTRO
    //
    public void DoorShoot() {

        doorShoot = true;

        playerMovementController.skateAnimator.transform.Find("Graphics").gameObject.SetActive(false);

    }

    public void EnableProjector() {

        //playerMovementController.playerAnimator.transform.Find("Projector").gameObject.SetActive(true);

    }
    
    public void StartRunning() {

        playerMovementController.SetSpeed(10f);

        playerMovementController.ChangeSideLimit(-1.8f, 1.8f);

        if(inGameHUD != null) inGameHUD.interactable = true;

        //cameraMovementController.ChangeForwardPosition(-1f);

    }
    
    //JAIL EXIT
    public void SpawnSkate() {

        playerMovementController.skateAnimator.transform.Find("Graphics").gameObject.SetActive(true);

        //playerMovementController.skateAnimator.GetComponentInChildren<Projector>().enabled = false;

    }

    public void SpawnProjector() {

        //playerMovementController.skateAnimator.GetComponentInChildren<Projector>().enabled = true;

        //playerMovementController.playerAnimator.transform.Find("Projector").gameObject.SetActive(false);

    }

    public void RaiseSpeed() {

        playerMovementController.SetSpeed(20f);

    }

    public void EnableJump() {

        playerMovementController.canJump = true;

        if (inGameHUD != null) inGameHUD.interactable = true;

    }

    //CHANGE MOUNT LOGIC
    public void ChangeMount(PlayerMountSet.Mount mount) {

        playerMovementController.ChangeMount(mount);

        playerMovementController.canJump = true;

        playerMovementController.SetSpeed(playerMovementController.initSpeed);

        playerMovementController.ChangeSideLimit(-4f, 4f);

        Collider[] colliders = new Collider[1];
        Physics.OverlapSphereNonAlloc(transform.position, 2f, colliders, LayerMask.GetMask("FakeMount"));
        colliders[0].gameObject.SetActive(false);
        
    }

    //X->SKATE
    public void SkateChange() {

        ChangeMount(PlayerMountSet.Mount.SKATE);

        if (skateSoundController != null) skateSoundController.TriggerSkateIn();

    }

    //SKATE->LAEGOO
    public void LaegooChange() {

        ChangeMount(PlayerMountSet.Mount.LAEGOO);

        if (skateSoundController != null) skateSoundController.TriggerSkateOut();

    }
    
    //SKATE->MOTO
    public void MotoChange() {

        ChangeMount(PlayerMountSet.Mount.BIKE);

        if (skateSoundController != null) skateSoundController.TriggerSkateOut();

    }

    //SKATE->BEAR
    public void BearChange() {

        ChangeMount(PlayerMountSet.Mount.BEAR);

        if (skateSoundController != null) skateSoundController.TriggerSkateOut();

    }

    //SKATE->GORILLA
    public void GorillaChange() {

        ChangeMount(PlayerMountSet.Mount.GORILLA);

        if (skateSoundController != null) skateSoundController.TriggerSkateOut();

    }

}
