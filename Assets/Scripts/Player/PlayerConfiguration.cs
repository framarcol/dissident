﻿using UnityEngine;
using System.Collections;

public class PlayerConfiguration : MonoBehaviour {

    public PlayerAnimEvents playerAnimEvents;
    public SkinnedMeshRenderer playerGraphics;
    public SkinnedMeshRenderer skateGraphics;

    public bool forceKeepWeapon = false;

    [HideInInspector]
    public ShopData.StandardShopItem weaponSkin = null;
    [HideInInspector]
    public ShopData.StandardShopItem skateSkin = null;
    [HideInInspector]
    public ShopData.StandardShopItem bodySkin = null;
    [HideInInspector]
    public ShopData.UpgradeItem lifeUpgrade = null;
    [HideInInspector]
    public ShopData.UpgradeItem ammoUpgrade = null;

    PlayerHealth playerHealth;
    ShootController shootController;

    void Awake() {

        ShopData shopData = ShopManager.shopData;

        if (shopData == null) {

            weaponSkin = null;
            skateSkin = null;
            bodySkin = null;

            lifeUpgrade = null;
            ammoUpgrade = null;

            return;

        }

        playerHealth = GetComponentInChildren<PlayerHealth>();
        shootController = GetComponentInChildren<ShootController>();

        if (GameDataController.gameData.weaponSkinEquipped != 71 && !forceKeepWeapon) {

            weaponSkin = shopData.GetWeaponSkin((int)GameDataController.gameData.weaponSkinEquipped);

            if (weaponSkin != null && weaponSkin.hasLowpolyVersion) {

                playerAnimEvents.weapon.gameObject.SetActive(false);

                GameObject weapons = (GameObject)Instantiate(weaponSkin.lowpolyItemPresentation, playerAnimEvents.transform);

                Weapon weaponComponent = weapons.GetComponent<Weapon>();
                playerAnimEvents.weapon = weaponComponent;

                weaponComponent.weaponObjects[0].GetComponent<MeshRenderer>().sharedMaterial = weaponSkin.lowpolyItemOverrideMaterial;
                weaponComponent.weaponObjects[1].GetComponent<MeshRenderer>().sharedMaterial = weaponSkin.lowpolyItemOverrideMaterial;

            }

        } else weaponSkin = null;

        if (GameDataController.gameData.skateSkinEquipped != 72) {

            skateSkin = shopData.GetSkateSkin((int)GameDataController.gameData.skateSkinEquipped);

            if (skateSkin != null) {

                skateGraphics.sharedMesh = skateSkin.itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
                skateGraphics.sharedMaterial = skateSkin.itemOverrideMaterial;

            }

        } else skateSkin = null;

        if (GameDataController.gameData.bodySkinEquipped != 1) {

            bodySkin = shopData.GetBodySkin((int)GameDataController.gameData.bodySkinEquipped);

            if (bodySkin != null) {

                playerGraphics.sharedMesh = bodySkin.itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
                playerGraphics.sharedMaterial = bodySkin.itemOverrideMaterial;

            }

        } else bodySkin = null;

        lifeUpgrade = shopData.GetUpgrade((int)GameDataController.gameData.lifeUpgradeEquipped);

        if(lifeUpgrade != null && lifeUpgrade.updgradeType == ShopData.UpgradeItem.UpgradeType.LIFE) {

            playerHealth.maxLife = lifeUpgrade.extraLife;

        }

        ammoUpgrade = shopData.GetUpgrade((int)GameDataController.gameData.ammoUpgradeEquipped);

        if(ammoUpgrade != null && ammoUpgrade.updgradeType == ShopData.UpgradeItem.UpgradeType.MAX_AMMO) {

            shootController.maxAmmo = ammoUpgrade.maxAmmo;

        }

    }
    
}
