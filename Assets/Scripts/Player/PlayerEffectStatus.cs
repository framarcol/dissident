﻿using UnityEngine;
using System.Collections;

public class PlayerEffectStatus : MonoBehaviour {

    public ParticleSystem hitEffect;
    public ParticleSystem confuseEffect;
    
}
