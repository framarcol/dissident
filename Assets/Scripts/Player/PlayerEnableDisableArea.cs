﻿using UnityEngine;
using System.Collections;

public class PlayerEnableDisableArea : MonoBehaviour {

    LayerMask layerMask;

	// Use this for initialization
	void Start () {

        layerMask = LayerMask.GetMask("Shootable");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {

        if ((1 << other.gameObject.layer & layerMask) > 0) {

            other.gameObject.transform.Find("Graphics").gameObject.SetActive(true);

        }

    }

    void OnTriggerExit(Collider other) {

        if ((1 << other.gameObject.layer & layerMask) > 0) {

            other.gameObject.transform.Find("Graphics").gameObject.SetActive(false);

        }

    }

}
