﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    //Actual lifes and maximum life possible for the player
    public int lifes;
    public int maxLife = 3;

    //Variables to control the invulnerability of the player once damaged
    public bool invulnerability = false;
    public float invulnerabilityTime = 0.5f;
    float invulTimer = 0f;

    //Dead state
    public bool dead = false;
    public bool wentRagdoll = false;

    public bool hit = false;
    //public ParticleSystem hitEffect;
    PlayerEffectStatus playerEffectStatus;

    public GameObject spiderwebPiece1;
    public GameObject spiderwebPiece2;
    public bool stuck = false;

    public GameObject playerRagdoll;
    public Transform topPlayerBone;
    public float force = 250f;
    public GameObject skateRagdoll;
    public Transform skateTopBone;

    public AudioClip[] firstHits;
    public AudioClip[] secondHits;
    public AudioClip[] lastHits;
    AudioSource audioSource;
    int oneThird;
    SkateSoundController skateSoundController;

    public List<GameObject> ragdollInstances = new List<GameObject>();

    Vector3 ragdollImpulse;

    //GameManager gameManager;
    ScoreController scoreController;

    PlayerConfiguration playerConfiguration;

    //Player Health Delegates
    public delegate void PlayerHealthDelegateNoArgs();
    public delegate void PlayerHealthDelegate(int lifes);
    public PlayerHealthDelegate OnPlayerDamaged;
    public PlayerHealthDelegate OnPlayerHealed;
    public PlayerHealthDelegateNoArgs OnPlayerKilled;

    public int totalDamage = 0;

    float sfxVolumeScale;

    void Start () {
        
        //At the start set the player's life to the maximum
        lifes = maxLife;
        oneThird = maxLife / 3;

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        playerEffectStatus = GetComponent<PlayerEffectStatus>();

        audioSource = GetComponent<AudioSource>();

        skateSoundController = GetComponentInChildren<SkateSoundController>();

        sfxVolumeScale = PlayerPrefs.GetFloat(GameManager.SoundEffectVolumePref, 1f);
        audioSource.volume = sfxVolumeScale;

        playerConfiguration = GetComponentInParent<PlayerConfiguration>();

    }
	
	void Update () {

        //Check if dead
        if (dead) {
            KillPlayer();

            

            enabled = false;
            return;
        }

        //Check and treat the invulnerable state, timing it
        if (invulnerability) {

            invulTimer += Time.deltaTime;

            if(invulTimer >= invulnerabilityTime) {

                invulnerability = false;
                invulTimer = 0f;

            }

        }

    }

    public void Heal() {

        lifes++;

        if (lifes > maxLife) lifes = maxLife;

        OnPlayerHealed(1);

    }

    //Heal the player to the maximum
    public void HealMax() {

        OnPlayerHealed(maxLife - lifes);

        lifes = maxLife;

    }

    //Damage the player (1 HP)
    /*public void Damage() {

        lock(this){

            if (invulnerability) return;

            lives--;

            if (lives == 0) dead = true;

            invulnerability = true;

            hit = true;

        }

    }*/

    public void EnableInvulnerability() {

        invulnerability = true;

    }

    //Damage the player (X HP)
    public void Damage(int lifes = 1){

        lock (this) {

            if (invulnerability) return;

            //Hit impact sounds
            if(this.lifes <= oneThird * 2) {

                audioSource.PlayOneShot(secondHits[Random.Range(0, secondHits.Length)]);

            } else {

                audioSource.PlayOneShot(firstHits[Random.Range(0, firstHits.Length)]);

            }

            skateSoundController.TriggerHit();

            this.lifes -= lifes;

            if (this.lifes <= 0) {
                
                KillPlayer(true);

                

                //dead = true;

                //if (!goRagdoll)


                /*else
                    KillPlayer(true);*/

            }

            invulnerability = true;

            hit = true;

            //if (hitEffect != null) hitEffect.Play();

            if (playerEffectStatus.hitEffect != null) playerEffectStatus.hitEffect.Play();

            //If damaged reset the multiplier
            scoreController.ResetMultiplier();

            //Call handler
            if (!dead) OnPlayerDamaged(lifes);

            totalDamage += lifes;

        }

    }

    /*public void KillPlayer() {

        FindObjectOfType<GameManager>().TriggerGameOver();

        dead = true;

    }*/

    //Kill the player
    public void KillPlayer(bool goRagdoll = false) {

        if (dead) return;

        //Call handler
        OnPlayerDamaged(lifes);

        totalDamage += lifes;

        //gameManager.TriggerGameOver();
        lifes = 0;
        dead = true;

        AudioSource.PlayClipAtPoint(lastHits[Random.Range(0, lastHits.Length)], Camera.main.transform.position, audioSource.volume);
        skateSoundController.TriggerDestruction();

        OnPlayerKilled();

        if (goRagdoll && playerRagdoll != null && skateRagdoll != null) {

            wentRagdoll = true;

            //transform.Find("Graphics").gameObject.SetActive(false);

            ThrowRagdoll(playerRagdoll, topPlayerBone, playerConfiguration.bodySkin);

            Mounts mountList = GetComponentInChildren<Mounts>();
            ThrowRagdoll(skateRagdoll, mountList.mounts[mountList.activeMount].topMountBone, playerConfiguration.skateSkin);

            //Instantiate(skateRagdoll, skateTopBone.position, skateTopBone.rotation);

            gameObject.SetActive(false);

        } else if (!goRagdoll && skateRagdoll != null) {

            /*GameObject skate = Instantiate(skateRagdoll, skateTopBone.position, skateTopBone.rotation) as GameObject;

            skate.GetComponent<Rigidbody>().AddForce(skate.transform.forward * 20f, ForceMode.Impulse);
            
            skateTopBone.gameObject.SetActive(false);*/

            Mounts mountList = GetComponentInChildren<Mounts>();
            ThrowRagdoll(skateRagdoll, mountList.mounts[mountList.activeMount].topMountBone);
            mountList.mounts[mountList.activeMount].mountObject.SetActive(false);

            foreach(Collider col in GetComponents<Collider>()) {

                col.enabled = false;

            }

        }

        //TEST
        //GameManager.CheckAndSave();
        //UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);

    }

    public Transform KillPlayerRagdoll(Vector3 impulse) {

        if (dead) return null;

        //Call handler
        OnPlayerDamaged(lifes);

        //gameManager.TriggerGameOver();
        lifes = 0;
        dead = true;

        wentRagdoll = true;

        //if (playerRagdoll != null && skateRagdoll != null) {

        //transform.Find("Graphics").gameObject.SetActive(false);

        ragdollImpulse = impulse;
        Transform topRagdollBone = ThrowRagdoll(playerRagdoll, topPlayerBone);
        ragdollImpulse = Vector3.zero;

        Mounts mountList = GetComponentInChildren<Mounts>();
        ThrowRagdoll(skateRagdoll, mountList.mounts[mountList.activeMount].topMountBone);

        //Instantiate(skateRagdoll, skateTopBone.position, skateTopBone.rotation);

        gameObject.SetActive(false);

        return topRagdollBone;

        //}

    }

    private Transform ThrowRagdoll(GameObject rd, Transform topBone, ShopData.StandardShopItem skin = null) {

        GameObject ragdoll = Instantiate(rd) as GameObject;
        ragdollInstances.Add(ragdoll);

        //if(skin != null) Debug.Log(skin.itemName);

        if(skin != null) {

            SkinnedMeshRenderer ragdollGraphics = ragdoll.GetComponentInChildren<SkinnedMeshRenderer>();

            ragdollGraphics.sharedMesh = skin.itemPresentation.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
            ragdollGraphics.sharedMaterial = skin.itemOverrideMaterial;

        }

        return TransferPoseAndExplosion(ragdoll, topBone);

    }

    private Transform GetRagdollTopBone(Transform ragdollPiece, Transform topBone) {

        Transform ragdollTopBone = null;

        if(ragdollPiece.name == topBone.name) return ragdollPiece;

        foreach (Transform child in ragdollPiece) {

            if (child.name == topBone.name) {

                return child;

            } else {

                ragdollTopBone = GetRagdollTopBone(child, topBone);

            }

        }

        return ragdollTopBone;

    }

    private Transform TransferPoseAndExplosion(GameObject ragdollInstance, Transform topBone) {

        Transform topRagdollBone = GetRagdollTopBone(ragdollInstance.transform, topBone);

        Transform[] ragdollBones = topRagdollBone.GetComponentsInChildren<Transform>();
        Transform[] entityBones = topBone.GetComponentsInChildren<Transform>();

        int j = 0;
        for(int i = 0; i < ragdollBones.Length; i++) {

            if(ragdollBones[i].name == entityBones[j].name) {

                ragdollBones[i].position = entityBones[j].position;
                ragdollBones[i].rotation = entityBones[j].rotation;

            } else {

                if (ragdollBones[i].CompareTag("NotBone")) j--;
                else i--;

            }

            j++;

        }

        if (force == 0) return null;

        /*if (ragdollImpulse != Vector3.zero)
            topRagdollBone.GetComponent<Rigidbody>().AddForce(ragdollImpulse, ForceMode.Impulse);
        else*/
            topRagdollBone.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);

        return topRagdollBone;

    }

    public void KillStuckPlayer() {

        if (dead) return;

        //Call handler
        OnPlayerDamaged(lifes);

        //gameManager.TriggerGameOver();

        GameObject skateRag = (GameObject)Instantiate(skateRagdoll, skateTopBone.position, skateTopBone.rotation);
        skateRag.GetComponentInChildren<Rigidbody>().AddForce(skateRag.transform.forward * force, ForceMode.Impulse);

        Mounts mountList = GetComponentInChildren<Mounts>();
        mountList.mounts[mountList.activeMount].mountObject.SetActive(false);

        totalDamage += lifes;

        lifes = 0;
        dead = true;
        stuck = true;

    }

    public void RevivePlayer() {

        ClearRagdolls();
        HealMax();

        dead = false;
        wentRagdoll = false;
        hit = false;
        invulnerability = false;

    }

    public void ClearRagdolls() {

        for(int i = 0; i < ragdollInstances.Count; i++) {

            ragdollInstances[i].SetActive(false);

        }

        ragdollInstances.Clear();

    }

}
