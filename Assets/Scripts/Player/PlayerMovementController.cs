﻿using UnityEngine;
using System.Collections;

public class PlayerMovementController : MonoBehaviour {

    public float initSpeed = 20f;
    public float lerpedSpeed = 0f;
    float targetSpeed;
    [HideInInspector] public float turnSpeed;

    public float moveScaleFromFinger = 0.25f;
    public float moveLerpScale = 5f;
    public float leftSideLimit = -4f;
    public float rightSideLimit = 4f;
    //public Vector2 deltaOffset;

    public Collider playerCollider;
    public BoxCollider skateCollider;
    public float jumpTolerance = 100f;
    public float jumpTime = 1f;
    public float jumpFallTime = 0.5f;
    public float jumpHeight = 2.5f;
    public float jumpStepHeight;

    //public float dashDistance = 2f;
    //public float dashTimeBetweenPositions = 0.5f;
    
    public static bool useAccelerometer = true;
    public Vector3 acceleration;
    //public Vector3 gyroRotationRate;
    public float moveAccelerometerScale = 2.5f;
    public float accelerometerJumpTolerance = 0.5f;

    float lowPassKernelWidthInSeconds = 0.5f;
    //The greater the value of LowPassKernelWidthInSeconds, the slower the filtered value will converge towards current input sample (and vice versa). 
    //You should be able to use LowPassFilter() function instead of avgSamples().
    float accelerometerUpdateInterval = 1.0f / 60.0f;
    float lowPassFilterFactor;
 
    Vector3 lowPassValue = Vector3.zero; // should be initialized with 1st sample
 
    Vector3 deviceAcc;
    Vector3 deviceDeltaAcc;

    float jumpSensitivityScale;
    float realAccelerometerTolerance;
    int jumpOption;

    //public float gyroTolerance = 0.5f;

    //Vector3 gyroRotation = Vector3.zero;
    //float rotationRateAxis;

    public bool canMove = true;
    public bool canMoveToSides = true;
    public bool canJump = true;

    public bool isStepJump = false;
    public bool falling = false;

    /////
    public float turnMagnitude;

    private bool _turn;
    public bool turn {

        get { return _turn; }
        set {

            if (playerAnimator != null) {
                playerAnimator.SetBool("turn", value);
                playerAnimator.SetFloat("turnSpeed", turnMagnitude);
            }
            if (skateAnimator != null) {
                skateAnimator.SetBool("turn", value);
                skateAnimator.SetFloat("turnSpeed", turnMagnitude);
            }

            if (value) skateSoundController.TriggerTurn(true);
            else skateSoundController.TriggerTurn(false);

            _turn = value;

        }

    }
    ////

    private bool _turnLeft;
    public bool turnLeft {
        
        get { return _turnLeft; }

        set {

            if(playerAnimator != null) playerAnimator.SetBool("turnLeft", value);
            if(skateAnimator != null) skateAnimator.SetBool("turnLeft", value);

            _turnLeft = value;

        }

    }
    private bool _turnRight;
    public bool turnRight {

        get { return _turnRight; }

        set {

            if (playerAnimator != null) playerAnimator.SetBool("turnRight", value);
            if (skateAnimator != null) skateAnimator.SetBool("turnRight", value);

            _turnRight = value;

        }

    }

    public bool isDeadYet = false;

    //public bool invertedControls = false;
    float inversionFactor = 1;

    int moveFingerId = -1;
    Vector2 oldPosition;

    Vector2 initPos;
    public bool jumping = false;
    bool goingDown = false;
    float jumpTimer = 0f;
    float oldDistance = 0f;

    CameraMovementController cameraMovementController;
    [HideInInspector]
    public GameObject playerStuff;
    Transform sideMovementFocus;
    PlayerHealth playerHealth;

    LayerMask mask;

#if UNITY_EDITOR
    bool tracking = false;
#endif

    public RotationArea rotationArea;
    public bool rotatingPlayer;
    public float radius;
    public float angleLeft;

    Rigidbody playerRigidbody;

    public Animator playerAnimator;
    public Animator skateAnimator;

    public Transform cameraPositions;

    public bool isRunning = false;

    public bool moveRight;
    public bool moveLeft;
    public float moveScale = 1f;
    public float manualMovementScale = 1f;

    PlayerMountSet playerMountSet;
    Mounts mountList;
    //public GameObject[] mounts = new GameObject[4];
    //public int activeMount = 0;

    float smoothAccelMagnitude;

    //GameManager gameManager;

    SkateSoundController skateSoundController;
    
    // Use this for initialization
    void Awake () {
        
        targetSpeed = initSpeed;
        turnSpeed = 0.75f * targetSpeed;

        playerRigidbody = GetComponent<Rigidbody>();
        playerHealth = GetComponentInChildren<PlayerHealth>();
        playerHealth.OnPlayerKilled += PlayerKilled;

        //Mask for the raycast to select the player's layer
        mask = LayerMask.GetMask("Touchable");

        cameraMovementController = FindObjectOfType<CameraMovementController>();
        playerStuff = transform.Find("PlayerStuff").gameObject;
        sideMovementFocus = transform.Find("SideMovementFocus");

        //MOUNT SETUP
        playerMountSet = Resources.Load("DataObjects/PlayerMountSet", typeof(PlayerMountSet)) as PlayerMountSet;
        mountList = GetComponentInChildren<Mounts>();

        skateSoundController = GetComponentInChildren <SkateSoundController>();

        //TEST
        //Input.gyro.enabled = true;
        //useAccelerometer = true;

#if UNITY_EDITOR
        useAccelerometer = false;
#endif

        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        lowPassValue = Input.acceleration;

        //Jump option and sensitivity
        jumpOption = PlayerPrefs.GetInt(GameManager.JumpOptionPref, 0);
        jumpSensitivityScale = PlayerPrefs.GetFloat(GameManager.JumpSensitivityPref, 1f);
        realAccelerometerTolerance = accelerometerJumpTolerance / jumpSensitivityScale;

        moveScale = 1f;
        manualMovementScale = 1f;

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        GameManager.OnGameCompleted += FinishedGame;

    }
	
    //Move the player forward using the kinematic rigidbody
    void FixedUpdate() {

        if (playerHealth.dead) return;

        if (canMove) {
            
            //Move the player forward
            playerRigidbody.MovePosition(playerRigidbody.position + (transform.forward * lerpedSpeed * Time.fixedDeltaTime));
            
        }
        
        if (useAccelerometer && jumpOption == 0) {

            deviceAcc = Input.acceleration;
            deviceDeltaAcc = deviceAcc - LowPassFilter(deviceAcc);

            /*if (Mathf.Abs(deviceDeltaAcc.x) >= .3) {
                // Do something
            }
            if (Mathf.Abs(deviceDeltaAcc.y) >= .3) {
                // Do something
            }*/

            if (canJump && !jumping) {

                if (Mathf.Abs(deviceDeltaAcc.z) >= realAccelerometerTolerance) {

                    /*ResetMovementFocus();

                    skateCollider.enabled = false;

                    jumping = true;

                    int jumpSelector = Random.Range(0, 2);

                    if (playerAnimator != null) {
                        playerAnimator.SetFloat("jumpSelector", jumpSelector);
                        playerAnimator.SetTrigger("jump");
                    }
                    if (skateAnimator != null) {
                        skateAnimator.SetFloat("jumpSelector", jumpSelector);
                        skateAnimator.SetTrigger("jump");
                    }

                    ///// TEST /////
                    moveAccelerometerScale /= 2;
                    moveScaleFromFinger /= 4;
                    ////////////////
                    */
                    
                    Jump();

                }

            }

        }

    }

	// Update is called once per frame
	void Update () {

        PerformAnimationStuff();

        if (playerHealth.dead) return;


        if(canMove) lerpedSpeed = Mathf.Lerp(lerpedSpeed, targetSpeed, 2.5f * Time.deltaTime);

        if (rotatingPlayer) {
            
            RotatePlayer();

        }

        /*if (jumping) {

            if (isStepJump) PerformHighJump();
            //else PerformJump();

            //return;

        }*/

        /*if (falling) {

            PerformFall();
            //return;

        }*/

#if UNITY_EDITOR
        if (canJump) {

            CheckIfJump();

        }
#endif

        //If the player can move to sides...
        if (canMoveToSides) {

            if (!useAccelerometer) MovePlayer();
            else if(jumpOption == 0) CalculateAcceleration();

            if (moveRight) {
                
                sideMovementFocus.localPosition += sideMovementFocus.right * inversionFactor * moveScale * manualMovementScale * Time.deltaTime;

                moveScale = Mathf.Lerp(moveScale, 20f, Time.deltaTime);

                if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, 0, 0);
                else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, 0, 0);

            } else if (moveLeft) {

                sideMovementFocus.localPosition -= sideMovementFocus.right * inversionFactor * moveScale * manualMovementScale * Time.deltaTime;

                moveScale = Mathf.Lerp(moveScale, 20f, Time.deltaTime);

                if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, 0, 0);
                else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, 0, 0);

            }

            Vector3 targetPos = Vector3.Lerp(playerStuff.transform.localPosition, sideMovementFocus.localPosition, moveLerpScale * Time.deltaTime);
            targetPos.y = playerStuff.transform.localPosition.y;

            playerStuff.transform.localPosition = targetPos;

        }

    }

    public void PlayerKilled() {

        if (playerHealth.dead) {

            lerpedSpeed = 0f;
            ResetMovementFocus();

            if (jumping) {

                jumping = false;

                oldDistance = 0f;
                jumpTimer = 0f;
                goingDown = false;
                skateCollider.enabled = true;

                if (jumpHeight != 2.5f) jumpHeight = 2.5f;

                ///// TEST /////
                moveAccelerometerScale *= 2;
                moveScaleFromFinger *= 4;
                manualMovementScale *= 4;
                ////

            }

        }

    }

    public void MovePlayer() {

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide)) {

                if (hit.collider.tag == "MovementTouchFocus") {

                    oldPosition = Input.mousePosition;
                    
                    tracking = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {
            
            tracking = false;

        }

        if (tracking) {

            Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector3 deltaOffset = mousePos - oldPosition;

            sideMovementFocus.position += sideMovementFocus.right * deltaOffset.x * inversionFactor * Time.deltaTime;
            if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, sideMovementFocus.localPosition.y,0);
            else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, sideMovementFocus.localPosition.y, 0);

            oldPosition = Input.mousePosition;

        }

#endif

        foreach (Touch touch in Input.touches) {

            if (touch.phase == TouchPhase.Began && moveFingerId == -1) {

                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide)) {

                    if (hit.collider.tag == "MovementTouchFocus") {

                        moveFingerId = touch.fingerId;

                        oldPosition = touch.position;

                        //
                        initPos = touch.position;
                        //

                    }

                }

            } else if (touch.phase == TouchPhase.Moved && moveFingerId == touch.fingerId) {

                Vector2 deltaOffset = touch.position - oldPosition;

                sideMovementFocus.position += sideMovementFocus.right * deltaOffset.x * moveScaleFromFinger * inversionFactor * Time.deltaTime;
                if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, 0, 0);
                else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, 0, 0);

                oldPosition = touch.position;

                //
                //if (!canJump) return;

                Vector2 jumpVector = touch.position - initPos;

                if (jumpVector.y > jumpTolerance) {

                    Jump();

                }
                //

            } else if (touch.phase == TouchPhase.Ended && moveFingerId == touch.fingerId) {
                
                moveFingerId = -1;

            }

        }

    }

    public void RestrictJump(bool canJump) {

        this.canJump = canJump;

    }

    public void Jump() {

        if (!canJump || falling) return;

        ResetMovementFocus();

        skateCollider.enabled = false;

        jumping = true;
        moveFingerId = -1;

        int jumpSelector = Random.Range(0, 2);

        if (playerAnimator != null) {
            playerAnimator.SetFloat("jumpSelector", jumpSelector);
            playerAnimator.SetTrigger("jump");
        }
        if (skateAnimator != null) {
            skateAnimator.SetFloat("jumpSelector", jumpSelector);
            skateAnimator.SetTrigger("jump");
        }

        if (skateSoundController != null) skateSoundController.TriggerJump();

        ///// TEST /////
        moveAccelerometerScale /= 2;
        moveScaleFromFinger /= 4;
        manualMovementScale /= 4;
        ////////////////

        if (isStepJump)
            StartCoroutine(PerformHighJump());
        else
            StartCoroutine(PerformJump());

    }

#if UNITY_EDITOR

    public void CheckIfJump() {
        
        if (Input.GetMouseButtonDown(0)) {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide)) {

                if (hit.collider.tag == "MovementTouchFocus") {

                    initPos = Input.mousePosition;
                    
                    tracking = true;

                }

            }

        } else if (Input.GetMouseButtonUp(0)) {
            
            tracking = false;

        }

        if (tracking) {

            Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector2 jumpVector = mousePos - initPos;

            if (jumpVector.y > jumpTolerance) {

                ResetMovementFocus();

                skateCollider.enabled = false;

                jumping = true;
                tracking = false;

                if (skateSoundController != null) skateSoundController.TriggerJump();

                ///// TEST /////
                moveAccelerometerScale /= 2;
                moveScaleFromFinger /= 4;
                manualMovementScale /= 4;
                ////////////////

                int jumpSelector = Random.Range(0, 2);

                if (playerAnimator != null) {
                    playerAnimator.SetFloat("jumpSelector", jumpSelector);
                    playerAnimator.SetTrigger("jump");
                }
                if (skateAnimator != null) {
                    skateAnimator.SetFloat("jumpSelector", jumpSelector);
                    skateAnimator.SetTrigger("jump");
                }

                //Debug.Log(jumpSelector);

                /*if (playerAnimator != null) playerAnimator.SetTrigger("jump");
                if (skateAnimator != null) skateAnimator.SetTrigger("jump");*/

                if (isStepJump)
                    StartCoroutine(PerformHighJump());
                else
                    StartCoroutine(PerformJump());

            }

        }
        
    }

#endif

    /*public void PerformJump() {

        jumpTimer += Time.deltaTime;

        if (jumpTimer > jumpUpTime) {

            jumpTimer = jumpUpTime;

            if (goingDown) jumping = false;
            else goingDown = true;

        }

        float perc = jumpTimer / jumpUpTime;

        if (!goingDown) perc = Mathf.Sin(perc * Mathf.PI * 0.5f);
        else perc = 1 - Mathf.Cos(perc * Mathf.PI * 0.5f);

        float distance = perc * jumpHeight;
        float deltaDistance = distance - oldDistance;
        oldDistance = distance;

        if (!goingDown) playerStuff.transform.position += new Vector3(0, deltaDistance, 0);
        else playerStuff.transform.position += new Vector3(0, -deltaDistance, 0);

        if (jumpTimer == jumpUpTime) {

            oldDistance = 0f;
            jumpTimer = 0f;

            if (!jumping && goingDown) {
                
                goingDown = false;
                skateCollider.enabled = true;

                if (jumpHeight != 2.5f) jumpHeight = 2.5f;

                ///// TEST /////
                moveAccelerometerScale *= 2;
                moveScaleFromFinger *= 4;
                ////////////////

            }

        }

    }*/

    IEnumerator PerformJump() {
       
        float initHeight = playerStuff.transform.localPosition.y;

        float perc;
        float timer = 0f;
        while (timer < jumpTime) {

            timer += Time.deltaTime;

            perc = timer / jumpTime;
            perc = Mathf.Sin(perc * Mathf.PI);

            Vector3 newPosition = playerStuff.transform.localPosition;
            newPosition.y = initHeight + perc * jumpHeight;

            playerStuff.transform.localPosition = newPosition;

            yield return null;

        }

        playerStuff.transform.localPosition = new Vector3(playerStuff.transform.localPosition.x, initHeight, playerStuff.transform.localPosition.z);

        Debug.Log(initHeight + " - " + playerStuff.transform.localPosition.y);

        skateCollider.enabled = true;

        ///// TEST /////
        moveAccelerometerScale *= 2;
        moveScaleFromFinger *= 4;
        manualMovementScale *= 4;
        ////////////////

        jumping = false;
        
    }

    //public void PerformHighJump() {

    //    jumpTimer += Time.deltaTime;

    //    if(jumpTimer > jumpTime) {

    //        jumpTimer = jumpTime;

    //        if (goingDown) jumping = false;
    //        else goingDown = true;

    //    }

    //    float perc = jumpTimer / jumpTime;

    //    if (!goingDown) perc = Mathf.Sin(perc * Mathf.PI * 0.5f);
    //    else perc = 1 - Mathf.Cos(perc * Mathf.PI * 0.5f);

    //    float distance = perc * jumpStepHeight;
    //    float deltaDistance = distance - oldDistance;
    //    oldDistance = distance;

    //    if(!goingDown) /*playerStuff.*/transform.position += new Vector3(0, deltaDistance, 0);
    //    else /*playerStuff.*/transform.position += new Vector3(0, -deltaDistance, 0);

    //    if (jumpTimer == jumpTime) {
            
    //        oldDistance = 0f;
    //        jumpTimer = 0f;

    //        if (!jumping && goingDown) {

    //            if (isStepJump) {

    //                jumpStepHeight = 2f * jumpStepHeight;
    //                isStepJump = false;

    //            }

    //            goingDown = false;
    //            skateCollider.enabled = true;

    //            ///// TEST /////
    //            moveAccelerometerScale *= 2;
    //            moveScaleFromFinger *= 4;
    //            ////////////////

    //        } else if (isStepJump && goingDown) {

    //            jumpStepHeight = 0.5f * jumpStepHeight;

    //            //cameraMovementController.ChangeHeight(jumpStepHeight);

    //        }

    //    }

    //}

    IEnumerator PerformHighJump() {

        playerCollider.enabled = false;

        float initHeight = /*playerStuff.*/transform.localPosition.y;
        float cameraInitHeigth = cameraMovementController.heightOffset;

        float perc;
        float timer = 0f;

        while (timer < jumpTime) {

            timer += Time.deltaTime;

            perc = timer / jumpTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            Vector3 newPosition = /*playerStuff.*/transform.localPosition;
            newPosition.y = initHeight + perc * jumpStepHeight;

            /*playerStuff.*/transform.localPosition = newPosition;

            cameraMovementController.heightOffset = cameraInitHeigth + perc * jumpStepHeight;

            //cameraMovementController.heightSpot.localPosition = new Vector3(cameraMovementController.heightSpot.localPosition.x, cameraInitHeigth + perc * jumpStepHeight, cameraMovementController.heightSpot.localPosition.z);

            yield return null;

        }
        
        /*playerStuff.*/transform.localPosition = new Vector3(/*playerStuff.*/transform.localPosition.x, initHeight + jumpStepHeight, /*playerStuff.*/transform.localPosition.z);
        cameraMovementController.heightOffset = cameraInitHeigth + jumpStepHeight;
        //cameraMovementController.heightSpot.localPosition = new Vector3(cameraMovementController.heightSpot.localPosition.x, cameraInitHeigth + jumpStepHeight, cameraMovementController.heightSpot.localPosition.z);

        //cameraMovementController.ChangeHeight(jumpStepHeight);
        //cameraMovementController.transform.localPosition = new Vector3(cameraMovementController.transform.localPosition.x, cameraInitHeigth + jumpStepHeight, cameraMovementController.transform.localPosition.z);

        //sideMovementFocus.localPosition = playerStuff.transform.localPosition;

        playerCollider.enabled = true;
        skateCollider.enabled = true;

        ///// TEST /////
        moveAccelerometerScale *= 2;
        moveScaleFromFinger *= 4;
        manualMovementScale *= 4;
        ////////////////

        jumping = false;
        isStepJump = false;

    }

    /*public void PerformFall() {

        jumpTimer += Time.deltaTime;

        if(jumpTimer >= jumpFallTime) {

            jumpTimer = jumpFallTime;
            falling = false;

        }

        float perc = jumpTimer / jumpFallTime;
        //perc = 1 - Mathf.Cos(perc * Mathf.PI * 0.5f);
        perc = perc * perc * (3f - 2f * perc);

        float distance = perc * jumpStepHeight;
        float deltaDistance = distance - oldDistance;
        oldDistance = distance;

        transform.position -= new Vector3(0, deltaDistance, 0);
        //playerStuff.transform.position -= new Vector3(0, deltaDistance, 0);
        //cameraMovementController.ChangeHeight(-deltaDistance);
        //sideMovementFocus.localPosition = playerStuff.transform.localPosition;

        if (!falling) {

            jumpTimer = 0f;
            oldDistance = 0f;

            if (jumpFallTime == 1.25f) jumpFallTime = 0.5f;

            canJump = true;
            canMoveToSides = true;

            //sideMovementFocus.localPosition = new Vector3(playerStuff.transform.localPosition.x, playerStuff.transform.localPosition.y, playerStuff.transform.localPosition.z);
            //cameraMovementController.ChangeHeight(-jumpStepHeight);

        }

    }*/

    public void Fall(float animFactor) {

        falling = true;
        canJump = false;
        canMoveToSides = false;
        
        StartCoroutine(PerformFall(animFactor));

    }

    IEnumerator PerformFall(float animFactor) {

        yield return new WaitWhile(() => { return jumping; });

        //bool longFall = false;
        if (animFactor != -1){

            playerAnimator.SetFloat("fallFactor", 1f / animFactor);

            //longFall = true;

        } else playerAnimator.SetFloat("fallFactor", 1f);

        SetTrigger("fall");

        float initHeigth = /*playerStuff.*/transform.localPosition.y;
        //float cameraMovementInitHeight = cameraMovementController.transform.position.y;
        float cameraInitHeigth = cameraMovementController.heightOffset;

        float perc;
        float timer = 0f;

        while(timer < jumpFallTime) {

            timer += Time.deltaTime;

            perc = timer / jumpFallTime;
            perc = 1f - Mathf.Cos(perc * Mathf.PI * 0.5f);

            Vector3 newPosition = /*playerStuff.*/transform.localPosition;
            newPosition.y = initHeigth - perc * jumpStepHeight;

            /*playerStuff.*/transform.localPosition = newPosition;

            //sideMovementFocus.localPosition = playerStuff.transform.localPosition;

            cameraMovementController.heightOffset = cameraInitHeigth - perc * jumpStepHeight;

            //if (longFall)
                //cameraMovementController.ChangeHeight(-perc * jumpStepHeight);
                //cameraMovementController.transform.localPosition = new Vector3(cameraMovementController.transform.localPosition.x, cameraInitHeigth - perc * jumpStepHeight, cameraMovementController.transform.localPosition.z);
                //cameraMovementController.targetDistance = (playerStuff.transform.position - cameraMovementController.transform.position).magnitude;
            //else
                //cameraMovementController.heightSpot.localPosition = new Vector3(cameraMovementController.heightSpot.localPosition.x, cameraInitHeigth - perc * jumpStepHeight, cameraMovementController.heightSpot.localPosition.z);

            yield return null;

        }

        //cameraMovementController.transform.position = new Vector3(cameraMovementController.transform.position.x, cameraMovementInitHeight - jumpStepHeight, cameraMovementController.transform.position.z);
        //cameraMovementController.targetDistance = (playerStuff.transform.position - cameraMovementController.transform.position).magnitude;

        /*playerStuff.*/transform.localPosition = new Vector3(/*playerStuff.*/transform.localPosition.x, initHeigth - jumpStepHeight, /*playerStuff.*/transform.localPosition.z);

        //sideMovementFocus.localPosition = playerStuff.transform.localPosition;

        cameraMovementController.heightOffset = cameraInitHeigth - jumpStepHeight;
        
        //cameraPositions.transform.localPosition -= new Vector3(0, jumpStepHeight, 0);

        //cameraMovementController.ChangeHeight(-jumpStepHeight);
        //cameraMovementController.transform.localPosition = new Vector3(cameraMovementController.transform.localPosition.x, cameraInitHeigth - jumpStepHeight, cameraMovementController.transform.localPosition.z);

        if (jumpFallTime == 1f) jumpFallTime = 0.75f;

        falling = false;
        canJump = true;
        canMoveToSides = true;

    }

    //Refer to the RotateCamera function in CameraMovementController, because they share the same logic
    public void RotatePlayer() {

        float arcLength = lerpedSpeed * Time.deltaTime;
        
        float degreesToRotate = Mathf.Rad2Deg * (arcLength / radius);

        if(angleLeft - degreesToRotate <= 0) {

            canMove = true;
            //canMoveToSides = true;

            if(!isRunning) canJump = true;

            //moveFingerId = -1;
            //tracking = false;

            rotatingPlayer = false;

            targetSpeed = turnSpeed / 0.75f;

            transform.RotateAround(rotationArea.pivot, rotationArea.actualAxis, angleLeft);

            transform.rotation = rotationArea.finalRotation;

            /*turnLeft = false;
            turnRight = false;*/
            turn = false;

            //When finishing the turn disabe the previous part of the road
            rotationArea.roadPoint.road.DisablePreviousPart();

        } else {

            angleLeft -= degreesToRotate;

            transform.RotateAround(rotationArea.pivot, rotationArea.actualAxis, degreesToRotate);

        }
        
    }
    
    public void ResetMovementFocus() {

        /*if(isStepJump) sideMovementFocus.localPosition = new Vector3(playerStuff.transform.localPosition.x, playerStuff.transform.localPosition.y + (0.5f * jumpStepHeight), playerStuff.transform.localPosition.z);
        else*/ sideMovementFocus.localPosition = new Vector3(playerStuff.transform.localPosition.x, sideMovementFocus.transform.localPosition.y, playerStuff.transform.localPosition.z);

    }

    public void SetSpeed(float speed) {

        targetSpeed = speed;
        turnSpeed = 0.75f * targetSpeed;

    }

    public void PushPlayer(float amount) {

        sideMovementFocus.localPosition += new Vector3(amount, 0, 0);

        if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, sideMovementFocus.localPosition.y, 0);
        else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, sideMovementFocus.localPosition.y, 0);

    }

    public void SlowInTurn(){

        targetSpeed = turnSpeed;

    }

    public void ChangeSideLimit(float newLeftSideLimit, float newRightSideLimit) {

        leftSideLimit = newLeftSideLimit;
        rightSideLimit = newRightSideLimit;

        if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, sideMovementFocus.localPosition.y, 0);
        else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, sideMovementFocus.localPosition.y, 0);

    }

    public static void SetGyro(bool enabled) {

        useAccelerometer = enabled;

    }

    public void CalculateAcceleration() {

        acceleration = Input.acceleration;

        float accelerationMagnitude = acceleration.x;

        if (Mathf.Abs(accelerationMagnitude) < 0.05f) accelerationMagnitude = 0;

        sideMovementFocus.position += sideMovementFocus.right * accelerationMagnitude * moveAccelerometerScale * inversionFactor * Time.deltaTime;
        if (sideMovementFocus.localPosition.x > rightSideLimit) sideMovementFocus.localPosition = new Vector3(rightSideLimit, 0, 0);
        else if (sideMovementFocus.localPosition.x < leftSideLimit) sideMovementFocus.localPosition = new Vector3(leftSideLimit, 0, 0);

        //gyroRotationRate = Input.gyro.rotationRate;

        //Debug.Log("HEY");

        ////Get gyro rotation rate Vector3
        //gyroRotation = Input.gyro.rotationRate;

        ////Get our rotation axis for the player (in this case gyro Y axis)
        //rotationRateAxis = gyroRotation.y;

        ////If the device rotated (gyro rotation rate) then apply the movement
        //if (!dashingPlayer && Mathf.Abs(rotationRateAxis) > gyroTolerance) {

        //    //oldXPosition = transform.position.x;

        //    if (rotationRateAxis > 0)
        //        CalculateAndDashRight();
        //    else if (rotationRateAxis < 0)
        //        CalculateAndDashLeft();

        //}

    }

    public void MoveRight() {

        if (moveLeft) return;

        moveRight = !moveRight;

        if (!moveRight) moveScale = 1f;

    }

    public void MoveLeft() {

        if (moveRight) return;

        moveLeft = !moveLeft;

        if (!moveLeft) moveScale = 1f;

    }

    private Vector3 LowPassFilter(Vector3 newSample) {
        lowPassValue = Vector3.Lerp(lowPassValue, newSample, lowPassFilterFactor);
        return lowPassValue;
    }

    public void SetInvertedControls(bool inversion) {

        if (inversion) inversionFactor = -1;
        else inversionFactor = 1;

    }

    public void SetTrigger(string animation) {

        playerAnimator.SetTrigger(animation);
        skateAnimator.SetTrigger(animation);

    }

    private void PerformAnimationStuff() {

        if (Time.timeScale == 0f) return;

        if (playerAnimator != null && skateAnimator != null) {

            if (!playerHealth.dead) {

                if (useAccelerometer) {

                    smoothAccelMagnitude = Mathf.Lerp(smoothAccelMagnitude, acceleration.x, 5f * Time.deltaTime);

                    playerAnimator.SetFloat("sideSpeed", smoothAccelMagnitude * 2);
                    if (skateAnimator.gameObject.activeSelf) skateAnimator.SetFloat("sideSpeed", smoothAccelMagnitude * 2);

                } else {

                    Vector3 diffPos = (sideMovementFocus.localPosition - playerStuff.transform.localPosition);

                    float magnitude = (sideMovementFocus.position - playerStuff.transform.position).sqrMagnitude;

                    if (diffPos.x < 0) {

                        playerAnimator.SetFloat("sideSpeed", -magnitude /* 6f * Time.deltaTime*/);
                        if (skateAnimator.gameObject.activeSelf) skateAnimator.SetFloat("sideSpeed", -magnitude /* 6f * Time.deltaTime*/);

                    } else {

                        playerAnimator.SetFloat("sideSpeed", magnitude /* 6f * Time.deltaTime*/);
                        if(skateAnimator.gameObject.activeSelf) skateAnimator.SetFloat("sideSpeed", magnitude /* 6f * Time.deltaTime*/);

                    }

                }

            }

            if(playerHealth.stuck && playerHealth.dead && !isDeadYet) {

                playerAnimator.SetTrigger("stuck");

                isDeadYet = true;

            } else if (playerHealth.dead && !playerHealth.wentRagdoll && !isDeadYet) {

                playerAnimator.SetTrigger("dead");
                if(skateAnimator.gameObject.activeSelf) skateAnimator.SetTrigger("dead");

                isDeadYet = true;

            }

            if (playerHealth.hit && !isDeadYet) {

                if (playerAnimator.GetBool("onCinematic")) {

                    playerHealth.hit = false;
                    return;

                }

                playerAnimator.SetTrigger("hit");
                if (skateAnimator.gameObject.activeSelf) skateAnimator.SetTrigger("hit");

                playerHealth.hit = false;

            }

        }

    }

    public void ChangeMount(PlayerMountSet.Mount newMount) {

        for(int i = 0; i < playerMountSet.mountSet.Length; i++) {

            PlayerMountSet.MountSetup mountSetup = playerMountSet.mountSet[i];

            if(mountSetup.mountType == newMount) {

                if (mountList.activeMount == i) return;

                //Enable the mount
                mountList.mounts[mountList.activeMount].mountObject.SetActive(false);
                //if (mountList.activeMount == 0) mountList.mounts[mountList.activeMount].topMountBone.localPosition = Vector3.zero;
                GameObject mount = mountList.mounts[i].mountObject;
                mount.SetActive(true);
                mountList.activeMount = i;

                //Set the correct new heights
                playerStuff.transform.localPosition = new Vector3(playerStuff.transform.localPosition.x, /*playerStuff.transform.localPosition.y +*/ mountSetup.playerTargetHeight, playerStuff.transform.localPosition.z);
                sideMovementFocus.localPosition = new Vector3(sideMovementFocus.localPosition.x, /*sideMovementFocus.transform.localPosition.y +*/ mountSetup.playerTargetHeight, sideMovementFocus.localPosition.z);

                //Set the correct animations
                playerAnimator.runtimeAnimatorController = mountSetup.playerMountAnimator;
                skateAnimator = mount.GetComponent<Animator>();
                skateAnimator.Rebind();
                if (i == 0) skateAnimator.Play("IdleDash", 0);
                else skateAnimator.Play("Run", 0);

                //Set the mount ragdoll
                playerHealth.skateRagdoll = mountSetup.mountRagdoll;

                return;

            }

        }

    }

    public void ResetPosition() {

        for (int i = 0; i < playerMountSet.mountSet.Length; i++) {

            PlayerMountSet.MountSetup mountSetup = playerMountSet.mountSet[i];

            if (mountSetup.mountType == mountList.mounts[mountList.activeMount].mountType) {

                playerStuff.transform.localPosition = new Vector3(0, mountSetup.playerTargetHeight, 0);
                sideMovementFocus.localPosition = new Vector3(0, mountSetup.playerTargetHeight, 0);
                
                return;

            }

        }

    }

    public void FinishedGame() {

        canMoveToSides = false;
        canJump = false;

    }

    //void OnGUI() {

    //    GUILayout.Label("Tolerance -> " + moveAccelerometerScale);

    //    GUILayout.Label(Application.persistentDataPath);

    //    GUILayout.Label(Screen.width.ToString());
    //    GUILayout.Label(deltaOffset.x.ToString());

    //    GUILayout.Label(SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.Depth).ToString());

    //}

}
