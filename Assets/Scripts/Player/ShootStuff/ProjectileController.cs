﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {

    public float lifeTime = 5f;

    public LayerMask targetMask;

	// Use this for initialization
	void Start () {

        Destroy(gameObject, lifeTime);

	}
	
	// Update is called once per frame
	void Update () {



	}

    void OnCollisionEnter(Collision collision) {

        if ((1 << collision.gameObject.layer & targetMask) > 0) {

            collision.gameObject.SendMessage("Shot", collision.contacts[0].point, SendMessageOptions.DontRequireReceiver);

        }

    }

}
