﻿using UnityEngine;
using System.Collections;

public class ScoreController : MonoBehaviour {

    public float totalScore = 0;

    public float multiplier = 1f;

    public int hits = 0;
    public int hitsPerMultiplier = 5;

    [HideInInspector]
    public int maxMultiplier = 1;

    void Start() {

        maxMultiplier = 1;

    }

    public void AddPoints(float score) {

        totalScore += score * multiplier;

    }

    public void AddHit() {

        hits++;

        if (hits % hitsPerMultiplier == 0) AddMultiplier();

    }

    public void AddHit(int hits) {

        for (int i = 0; i < hits; i++) {

            AddHit();

        }
        
    }

    public void AddMultiplier() {
        
        multiplier++;

        if (multiplier > maxMultiplier) maxMultiplier = (int)multiplier;

    }

    public void ResetMultiplier() {

        hits = 0;
        multiplier = 1f;

    }

}
