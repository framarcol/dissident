﻿using UnityEngine;
using System.Collections;

public class BasicParabolicShoot : ShootController {

    public GameObject projectile;
    public float force = 500f;

    Vector3 targetVector;

    protected override void Shoot() {

        Ray shootRay = new Ray();
        RaycastHit hitInfo;

#if UNITY_EDITOR

        Vector2 actualMousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (Input.GetMouseButtonDown(0) && screenRect.Contains(actualMousePosition) ) {

            shootRay = playerCamera.ScreenPointToRay(Input.mousePosition);
            
            bool hit = Physics.Raycast(shootRay, out hitInfo, 500, shootableMask);
            if (hit) {

                targetVector = hitInfo.point - transform.position;

                Debug.DrawLine(transform.position, hitInfo.point, Color.red, 2f);

                targetVector.Normalize();

            }

            //Debug.Log(hitInfo.collider.name);

            GameObject projectileInstance = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
            projectileInstance.GetComponent<Rigidbody>().AddForce(targetVector * force, ForceMode.Impulse);

            if (shootReady) InitFireDelay();
            //else if (canShootRight) InitRightFireDelay();

        }

#endif

        foreach (Touch touch in Input.touches) {

            switch (touch.phase) {

                case TouchPhase.Began:

                    Vector2 actualTouchPosition = new Vector2(touch.position.x, Screen.height - touch.position.y);

                    if (screenRect.Contains(actualTouchPosition)){

                        shootRay = playerCamera.ScreenPointToRay(touch.position);
                        
                        bool hit = Physics.Raycast(shootRay, out hitInfo, 500, shootableMask);
                        if (hit) {

                            targetVector = hitInfo.point - transform.position;
                            targetVector.Normalize();

                        }

                        GameObject projectileInstance = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
                        projectileInstance.GetComponent<Rigidbody>().AddForce(targetVector * force, ForceMode.Impulse);

                        if (shootReady) InitFireDelay();
                        //else if (canShootRight) InitRightFireDelay();

                    }
                    break;

            }

        }

    }

}
