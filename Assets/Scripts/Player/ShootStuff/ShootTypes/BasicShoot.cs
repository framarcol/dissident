﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class BasicShoot : ShootController {
    
    AudioSource audioSource;

    Weapon weapon;

    public Transform ammoPin;

    //public GameObject shootLine;

    //public GameObject hitEffect;

    public override void Start() {

        base.Start();

        audioSource = GetComponent<AudioSource>();

        weapon = GetComponentInChildren<PlayerAnimEvents>().weapon;

    }

	//Shoot control (called per frame)
	protected override void Shoot () {

        if (GameManager.gameState == GameState.PAUSED) return;

        Ray shootRay = new Ray();
        RaycastHit hitInfo;

#if UNITY_EDITOR

        //EDITOR TEST
        Vector2 actualMousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (Input.GetMouseButtonDown(0) && screenRect.Contains(actualMousePosition)) {

            if (!EventSystem.current.IsPointerOverGameObject()) {

                shootRay = playerCamera.ScreenPointToRay(Input.mousePosition);

                if (shootReady && Time.timeScale > 0) InitFireDelay();

            }

        }

#endif
        
        foreach (Touch touch in Input.touches) {

            switch (touch.phase) {

                case TouchPhase.Began:

                    Vector2 actualTouchPosition = new Vector2(touch.position.x, Screen.height - touch.position.y);

                    if (screenRect.Contains(actualTouchPosition)) {

                        if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId)) {

                            shootRay = playerCamera.ScreenPointToRay(touch.position);

                            if (shootReady && Time.timeScale > 0) InitFireDelay();

                        }

                    }
                    break;

            }

        }

        IdolCatcherProjectile projectile = weapon.projectilePrefab.GetComponent<IdolCatcherProjectile>();
        if (projectile != null)
#if UNITY_EDITOR
            shootableMask = LayerMask.GetMask("Shootable", "IdolShootable", "Touchable");
#else
            shootableMask = LayerMask.GetMask("Shootable", "IdolShootable");
#endif

        //bool hit = Physics.Raycast(shootRay, out hitInfo, Mathf.Infinity, shootableMask);
        bool hit = Physics.SphereCast(shootRay, 1f,  out hitInfo, Mathf.Infinity, shootableMask, QueryTriggerInteraction.Collide);

        if (hit) {

            if (hitInfo.collider.tag == "MovementTouchFocus") return;

            if (actualAmmo <= 0) return;

            //MUZZLE FLASH
            //StartCoroutine(ShowMuzzleFlash());

            //PLAY SOUND
            PlayFXSound();

            //Prepare the package
            shootPackage = new ShootPackage();

            //Init the package
            shootPackage.damage = shootDamage;

            //Pass the entity if exists
            Entity entity = hitInfo.collider.GetComponent<Entity>();
            if (entity != null) shootPackage.entity = entity;

            //Pass the hitpoint
            shootPackage.hitPoint = hitInfo.point;

            //Pass the relative hit point if the entity exists
            if (entity != null) shootPackage.relativeHitPoint = entity.transform.InverseTransformPoint(shootPackage.hitPoint);

            //If we hit something else than the limit plane
            if (hitInfo.collider.name != "LimitPlane") {

                //Tell the thing that it has been hit
                hitInfo.collider.gameObject.SendMessage("Shot", shootPackage, SendMessageOptions.DontRequireReceiver);

            } else {
                
                if (scoreController != null) scoreController.ResetMultiplier();
                missedShots++;

            }

            totalShots++;

            actualAmmo--;

            if (playerAnimator != null) {
                
                Vector3 localHitPosition = transform.InverseTransformPoint(hitInfo.point);
                float diff;

                if (playerMovementController.jumping) {

                    playerAnimator.SetTrigger("jumpShoot");
                    weapon.ShootProjectiles(-1);

                } else {

                    switch (playerCamera.GetComponentInParent<CameraFollowingController>().targetPosition) {

                        case CameraArmPositions.Default:

                            diff = localHitPosition.x - transform.localPosition.x;

                            if (playerMovementController.rotatingPlayer) {

                                if (playerMovementController.turnMagnitude < 0) {

                                    if (diff < -4f) {
                                        playerAnimator.SetTrigger("shootTurnLeftSide");
                                        weapon.ShootProjectiles(0);
                                    } else {
                                        playerAnimator.SetTrigger("shootTurnLeftForward");
                                        weapon.ShootProjectiles(0);
                                    }

                                } else if (playerMovementController.turnMagnitude > 0) {

                                    if (diff > 4f) {
                                        playerAnimator.SetTrigger("shootTurnRightSide");
                                        weapon.ShootProjectiles(1);
                                    } else {
                                        playerAnimator.SetTrigger("shootTurnRightForward");
                                        weapon.ShootProjectiles(1);
                                    }

                                }

                            } else {

                                if (Mathf.Abs(diff) > 5f) {

                                    if (diff > 0) {
                                        playerAnimator.SetTrigger("shootRight");
                                        weapon.ShootProjectiles(1);
                                    } else {
                                        playerAnimator.SetTrigger("shootLeft");
                                        weapon.ShootProjectiles(0);
                                    }

                                } else {

                                    playerAnimator.SetTrigger("shootForward");
                                    weapon.ShootProjectiles(-1);

                                }

                            }

                            break;

                        case CameraArmPositions.Left:
                            playerAnimator.SetTrigger("shootSideRight");
                            weapon.ShootProjectiles(1);
                            break;

                        case CameraArmPositions.Right:
                            playerAnimator.SetTrigger("shootSideLeft");
                            weapon.ShootProjectiles(0);
                            break;

                        case CameraArmPositions.Forward:

                            diff = localHitPosition.x - transform.localPosition.x;

                            if (diff > 0) {
                                playerAnimator.SetTrigger("shootBackRight");
                                weapon.ShootProjectiles(1);
                            } else {
                                playerAnimator.SetTrigger("shootBackLeft");
                                weapon.ShootProjectiles(0);
                            }

                            break;

                    }

                }
                



            }

        }

    }

    /*IEnumerator ShowMuzzleFlash() {

        yield return new WaitForSeconds(0.15f);

        if (leftMuzzleFlash != null) leftMuzzleFlash.SetActive(true);
        if (rightMuzzleFlash != null) rightMuzzleFlash.SetActive(true);

        yield return new WaitForSeconds(0.15f);

        if (leftMuzzleFlash != null) leftMuzzleFlash.SetActive(false);
        if (rightMuzzleFlash != null) rightMuzzleFlash.SetActive(false);

    }*/

    public void RestrictShoot(bool alow) {

        canShoot = alow;

    }

    void PlayFXSound() {

        if(audioSource != null) audioSource.PlayOneShot(weapon.shootEffects[Random.Range(0, weapon.shootEffects.Length)]);

    }

}
