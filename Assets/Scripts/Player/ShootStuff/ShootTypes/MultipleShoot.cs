﻿using UnityEngine;
using System.Collections.Generic;

public class MultipleShoot : ShootController {

    public int maxTargets = 3;

    bool selectingTargets = false;
    List<GameObject> targets = new List<GameObject>();

    int fingerMovementId = -1;

    protected override void Shoot() {

        Ray shootRay = new Ray();
        RaycastHit hitInfo;

/*#if UNITY_EDITOR

        //EDITOR TEST
        if (Input.GetMouseButtonDown(0)) {

            selectingTargets = true;

        } else if (Input.GetMouseButtonUp(0)) {

            selectingTargets = false;

            ShootThem();

        }

        if (selectingTargets) {

            if (targets.Count >= maxTargets) return;

            shootRay = playerCamera.ScreenPointToRay(Input.mousePosition);

            bool hit = Physics.Raycast(shootRay, out hitInfo, 20, shootableMask);

            if (hit) {

                GameObject go = hitInfo.collider.gameObject;

                if(!targets.Contains(go)) targets.Add(go);

            }

        }

#elif UNITY_ANDROID*/

        foreach (Touch touch in Input.touches) {

            switch (touch.phase) {

                case TouchPhase.Began:

                    //if (!RectTransformUtility.RectangleContainsScreenPoint(shootScreenPanel, touch.position/*, UICamera*/)) break;

                    if (fingerMovementId != -1) break;

                    fingerMovementId = touch.fingerId;
                    break;

                case TouchPhase.Moved:

                    if (fingerMovementId != touch.fingerId) break;

                    if (targets.Count >= maxTargets) return;

                    shootRay = playerCamera.ScreenPointToRay(touch.position);

                    bool hit = Physics.Raycast(shootRay, out hitInfo, 20, shootableMask);

                    if (hit) {

                        GameObject go = hitInfo.collider.gameObject;

                        if (!targets.Contains(go)) targets.Add(go);

                    }

                    break;

                case TouchPhase.Ended:

                    if (fingerMovementId != touch.fingerId) break;

                    ShootThem();

                    fingerMovementId = -1;

                    break;

            }

            

        }
        
//#endif

    }

    private void ShootThem() {

        if (targets.Count == 0) return;

        foreach(GameObject go in targets) {

            Destroy(go);

        }

        targets.Clear();

        InitFireDelay();

    }

}
