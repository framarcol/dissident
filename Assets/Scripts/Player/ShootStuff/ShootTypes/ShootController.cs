﻿using UnityEngine;

public struct ShootPackage {

    public int damage;
    public Vector3 hitPoint;
    public Vector3 relativeHitPoint;
    public Entity entity;

    public bool visuallyUsed;

}

public class ShootController : MonoBehaviour {
    
    [HideInInspector]
    public Rect screenRect;

    public float screenPercentage = 1f;

    public LayerMask shootableMask;

    public int shootDamage = 1;
    public float fireRateTime = 2f;
    float fireRateTimer;
    public int maxAmmo;
    public int actualAmmo;

    public bool shootReady = true;

    public bool canShoot = true;

    public Animator playerAnimator;

    [HideInInspector]
    public ShootPackage shootPackage;

    protected ScoreController scoreController;

    protected PlayerMovementController playerMovementController;

    public Camera playerCamera;
    PlayerHealth playerHealth;

    public int totalShots = 0;
    public int missedShots = 0;

    // Use this for initialization
    public virtual void Start () {

        playerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        if (playerCamera == null) {

            Debug.LogError("ShootController - Some variables must be correctly assigned");

            enabled = false;

        }

        UpdateScreenLimits();

        //Cache the score controller of the scene if exists
        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        

        //Init the ammo to the maximum
        actualAmmo = maxAmmo;

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        playerHealth = playerMovementController.GetComponentInChildren<PlayerHealth>();

#if UNITY_EDITOR

        shootableMask = LayerMask.GetMask( "Shootable", "Touchable" );
        
#endif

    }
	
	// Update is called once per frame
	void Update () {

        if (playerHealth.dead) {
            enabled = false;
            return;
        }

        //Fire rate controller
        if (!shootReady) {

            fireRateTimer += Time.deltaTime;

            if (fireRateTimer > fireRateTime) {

                fireRateTimer = 0f;
                shootReady = true;

            }

        }

        if (shootReady && canShoot) Shoot();

        if (Time.timeScale == 0) shootReady = true;

    }

    protected virtual void Shoot() { }

    protected void InitFireDelay() {

        shootReady = false;

    }

    public void AddAmmo(int ammo) {

        actualAmmo += ammo;

        if (actualAmmo > maxAmmo) actualAmmo = maxAmmo;

    }

    public void SetAmmoNoLimit(int ammo) {

        actualAmmo = ammo;

    }

    /*public void AddCriticalShot() {

        shootPackage.damage *= 2;

    }*/

    public void UpdateScreenLimits() {

        screenRect = new Rect(0f, 0f, Screen.width, screenPercentage * Screen.height);

    }

    public void SetPackageVisuallyUsed() {

        shootPackage.visuallyUsed = true;

    }

}
