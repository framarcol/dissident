﻿using UnityEngine;
using System.Collections;

public class AmmoRefillerProjectile : Projectile {

    public GameObject ammoRefill;

    public override void TriggerEffect() {

        GameObject ammoRefillInstance = Instantiate(ammoRefill, transform.position, ammoRefill.transform.rotation, null) as GameObject;

        Transform graphics = gameObject.transform.Find("Graphics");
        if(graphics != null) graphics.gameObject.SetActive(false);

        longestTiming += 5f;
        StartCoroutine(AnimateAmmoRefill(ammoRefillInstance));

    }

    IEnumerator AnimateAmmoRefill(GameObject ammoRefill) {

        Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        while (ammoRefill.activeSelf) {

            Vector3 trajectory = (player.position - ammoRefill.transform.position).normalized;

            ammoRefill.transform.position += trajectory * 35f * Time.deltaTime;
            yield return null;

        }

    }

}
