﻿using UnityEngine;
using System.Collections;

public class HealerProjectile : Projectile {

    public GameObject heal;

    public override void TriggerEffect() {

        GameObject healInstance = Instantiate(heal, transform.position, heal.transform.rotation, null) as GameObject;

        Transform graphics = gameObject.transform.Find("Graphics");
        if(graphics != null) graphics.gameObject.SetActive(false);

        longestTiming += 5f;
        StartCoroutine(AnimateAmmoRefill(healInstance));

    }

    IEnumerator AnimateAmmoRefill(GameObject heal) {

        Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        while (heal.activeSelf) {

            Vector3 trajectory = (player.position - heal.transform.position).normalized;

            heal.transform.position += trajectory * 35f * Time.deltaTime;
            yield return null;

        }

    }

}
