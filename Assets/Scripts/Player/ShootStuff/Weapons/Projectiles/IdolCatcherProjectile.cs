﻿using UnityEngine;
using System.Collections;

public class IdolCatcherProjectile : Projectile {
    
    const string idolLayer = "IdolShootable";

    public int startingWeaponUses = 1;

    public static int weaponUses = 0;
    static bool locked = false;

    GameManager gameManager;
    static int levelId = -1;

    public override void Start() {

        base.Start();

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        if (levelId == -1 || levelId != gameManager.levelId) {

            levelId = (int)gameManager.levelId;
            weaponUses = startingWeaponUses;

        }

    }

    void OnEnable() {

        locked = false;

    }

    void OnTriggerEnter(Collider other) {

        if (LayerMask.LayerToName(other.gameObject.layer) == idolLayer) {
            
            if(!locked && weaponUses > 0) {

                locked = true;

                weaponUses--;

                Debug.Log("Uses left: " + weaponUses);

                longestTiming += 5f;

                StartCoroutine(AnimateCatchedIdol(other.gameObject));

            }
            
        }

    }

    IEnumerator AnimateCatchedIdol(GameObject idol) {

        Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        while (idol.activeSelf) {

            Vector3 trajectory = (player.position - idol.transform.position).normalized;

            idol.transform.position += trajectory * 35f * Time.deltaTime;

            yield return null;

        }

    }

}
