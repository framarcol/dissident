﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public GameObject graphics;

    public GameObject normalTrail;
    public GameObject criticalTrail;

    public float lineAnimationDuration = 1f;

    public ParticleSystem normalImpact;
    public ParticleSystem criticalImpact;

    public float longestTiming;

    public Entity entity;
    public Vector3 hitPoint;

    [Range(0f, 1f)]
    public float effectProbability = 0f;

    public virtual void Start() {

        longestTiming = 0f;

        if (normalTrail != null) {

            TrailRenderer trail = normalTrail.GetComponent<TrailRenderer>();

            if(trail != null)
                longestTiming = trail.time;

            ParticleSystem particles = normalImpact.GetComponent<ParticleSystem>();

            if (particles != null)
                longestTiming = particles.duration;

        }

        if (criticalTrail != null) {

            TrailRenderer trail = criticalTrail.GetComponent<TrailRenderer>();

            if(trail != null && longestTiming < trail.time) longestTiming = trail.time;

            ParticleSystem particles = criticalTrail.GetComponent<ParticleSystem>();

            if (particles != null && longestTiming < particles.duration) longestTiming = particles.duration;
            
        }

        if(normalImpact != null) {

            if (longestTiming < normalImpact.duration) longestTiming = normalImpact.duration;

        }

        if (criticalImpact != null) {

            if (longestTiming < criticalImpact.duration) longestTiming = criticalImpact.duration;

        }

    }

    public void ActivateTrail(bool isCritical) {

        if (isCritical) {

            if(criticalTrail != null) {

                LineRenderer criticalLine = criticalTrail.GetComponent<LineRenderer>();

                if(criticalLine != null) {

                    StartCoroutine(AnimateTrajectoryLine(criticalLine));
                    
                } else {

                    criticalTrail.SetActive(true);

                }

            }

        } else {

            if (normalTrail != null) {

                LineRenderer normalLine = normalTrail.GetComponent<LineRenderer>();

                if (normalLine != null) {

                    StartCoroutine(AnimateTrajectoryLine(normalLine));

                } else {

                    normalTrail.SetActive(true);

                }

            }

        }

    }

    IEnumerator AnimateTrajectoryLine(LineRenderer line) {

        line.gameObject.SetActive(true);
        //line.SetPosition(0, transform.InverseTransformPoint(transform.parent.position));
        line.SetPosition(1, transform.InverseTransformPoint(hitPoint));
        
        float timer = 0f;
        while(timer < 1f) {

            timer += Time.deltaTime;

            float perc = timer / 1f;

            line.SetWidth(0, 1f - perc);

            //line.SetPosition(0, transform.InverseTransformPoint(transform.parent.position));
            line.SetPosition(1, transform.InverseTransformPoint(hitPoint));

            line.sharedMaterial.mainTextureOffset += new Vector2(20f * Time.deltaTime, 0);

            yield return null;

        }

        line.SetWidth(1,1);
        line.gameObject.SetActive(false);

    }

    public void ActivateImpact(bool isCritical) {

        if (isCritical) {

            if(criticalImpact != null) criticalImpact.Play();

        } else {

            if(normalImpact != null) normalImpact.Play();

        }

    }

    public void Reset() {

        normalTrail.SetActive(false);
        criticalTrail.SetActive(false);
        normalImpact.Stop();
        criticalImpact.Stop();
        
    }

    public bool CheckProbability() {

        if (effectProbability == 0f) return false;
        else if (Random.value <= effectProbability) return true;
        else return false;

    }

    public virtual void TriggerEffect() { }

}
