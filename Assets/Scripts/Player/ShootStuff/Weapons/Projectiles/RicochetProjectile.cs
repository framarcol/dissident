﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RicochetProjectile : Projectile {

    public override void TriggerEffect() {

        Collider[] cols = Physics.OverlapSphere(transform.position, 30f, LayerMask.GetMask("Shootable"), QueryTriggerInteraction.Collide);
        List<Collider> colliders = new List<Collider>(cols);

        Collider entityCollider = (entity != null) ? entity.GetComponent<Collider>() : null;

        if(entityCollider != null) colliders.Remove(entityCollider);

        Entity targetEntity = null;

        if (colliders.Count > 0) targetEntity = colliders[Random.Range(0, colliders.Count)].GetComponent<Entity>();

        if (targetEntity == null || targetEntity == entity) return;

        /*for(int i = 0; i < colliders.Length; i++) {

            Debug.Log(colliders[i].name);
            
            targetEntity = colliders[i].GetComponent<Entity>();
            if (targetEntity == entity) targetEntity = null;

            if (targetEntity != null) break;
            
        }
        if (targetEntity == null) return;*/
        
        targetEntity.lifes = 0;

        Transform graphics = gameObject.transform.Find("Graphics");
        if (graphics != null) graphics.gameObject.SetActive(false);

        StartCoroutine(AnimateRicochet(targetEntity));

    }

    IEnumerator AnimateRicochet(Entity targetEntity) {

        GameObject projectileInstance = Instantiate(gameObject, null) as GameObject;

        Projectile projectile = projectileInstance.GetComponent<Projectile>();
        projectile.ActivateTrail(true);

        Vector3 initPos = transform.position;
        Vector3 targetPos = targetEntity.transform.position;

        Vector3 trajectory = targetPos - initPos;

        projectileInstance.transform.position = initPos;
        //projectileInstance.transform.rotation = weaponCannons[index].rotation;

        float projectileFlyTime = trajectory.magnitude / 200f;
        longestTiming = projectileFlyTime + 2f;

        //Debug.Log(projectileFlyTime);

        float timer = 0f;
        while (timer < projectileFlyTime) {

            timer += Time.deltaTime;

            float perc = timer / projectileFlyTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            projectileInstance.transform.position = initPos + (perc * trajectory);

            trajectory = targetEntity.transform.position - initPos;

            yield return null;

        }

        //Visually kill the entity if still alive
        if (targetEntity != null && targetEntity.gameObject.activeSelf) {

            projectile.ActivateImpact(true);

            if (targetEntity.lifes <= 0) {

                targetEntity.GetScore();
                targetEntity.Die();

            }

        }

        yield return new WaitForSeconds(2f);

        projectileInstance.SetActive(false);

    }

}
