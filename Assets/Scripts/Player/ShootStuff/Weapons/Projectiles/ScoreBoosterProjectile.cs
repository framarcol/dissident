﻿using UnityEngine;
using System.Collections;

public class ScoreBoosterProjectile : Projectile {

    //[Range(0f,1f)]
    public float scoreBoost = 0.1f;

    public override void TriggerEffect() {

        if (entity != null) {

            if(entity.lifes <= 0) entity.score += scoreBoost * entity.score;

        }

    }

}
