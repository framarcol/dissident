﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour {

    public GameObject[] weaponObjects;

    public Transform[] weaponCannons;

    public GameObject projectilePrefab;
    public float projectileSpeed;

    public GameObject[] muzzleFlashes;
    Queue<GameObject> projectilePool;

    public AudioClip[] shootEffects;

    ShootController shootController;

    List<Entity> entitiesToKill = new List<Entity>();

    //static ShootPackage shootPackage;

    void Start() {

        shootController = GameObject.FindGameObjectWithTag("Player").GetComponent<ShootController>();

        if (projectilePrefab != null) {

            projectilePool = new Queue<GameObject>();

            for (int i = 0; i < 10; i++) {

                GameObject projectileInstance = Instantiate(projectilePrefab, transform) as GameObject;
                //projectileInstance.transform.position = transform.position;
                projectileInstance.SetActive(false);
                projectilePool.Enqueue(projectileInstance);

            }

        }
        
    }

    public void SetWeaponPosition(Transform[] attachLocations) {

        for (int i = 0; i < attachLocations.Length; i++) {

            Transform weapon = weaponObjects[i].transform;

            //Transform child = transform.GetChild(i);

            if (weapon != null) {

                weapon.SetParent(attachLocations[i]);
                weapon.localPosition = new Vector3(0, 0, 0);
                weapon.localRotation = Quaternion.identity;

            }

        }

    }

    public void ShootProjectiles(int selection) {
        
        if (projectilePool != null) {

            if (selection == -1) {

                for (int i = 0; i < weaponCannons.Length; i++) {

                    StartCoroutine(AnimateProjectileAndImpact(i));

                }

            } else {

                StartCoroutine(AnimateProjectileAndImpact(selection));

            }

        } else {

            Entity entity = shootController.shootPackage.entity;

            if (entity != null && entity.gameObject.activeSelf) {

                if (entity.lifes <= 0) {

                    entity.Die();

                }

            }

        }

    }

    IEnumerator AnimateProjectileAndImpact(int index) {

        ShootPackage shootPackage = shootController.shootPackage;

        Entity entity = shootPackage.entity;
        if (entity != null && !entitiesToKill.Contains(entity)) entitiesToKill.Add(entity);
        bool isCritical = false;
        if(entity != null) isCritical = entity.isCritical;

        GameObject projectileInstance = projectilePool.Dequeue();
        projectileInstance.SetActive(true);
        projectileInstance.transform.SetParent(null);
        //if (!projectileInstance.GetComponent<Collider>().enabled) projectileInstance.GetComponent<Collider>().enabled = true;

        Projectile projectile = projectileInstance.GetComponent<Projectile>();
        projectile.entity = entity;
        projectile.hitPoint = shootPackage.hitPoint;
        if (projectile.graphics != null) projectile.graphics.SetActive(true);

        Vector3 initPos = weaponCannons[index].position;
        Vector3 targetPos = shootPackage.hitPoint;

        Vector3 trajectory = targetPos - initPos;

        projectileInstance.transform.position = weaponCannons[index].position;
        projectileInstance.transform.rotation = weaponCannons[index].rotation;

        projectile.ActivateTrail(isCritical);
        
        float projectileFlyTime = trajectory.magnitude / projectileSpeed;

        //Debug.Log(projectileFlyTime);

        float timer = 0f;
        while(timer < projectileFlyTime) {

            timer += Time.deltaTime;

            float perc = timer / projectileFlyTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            projectileInstance.transform.position = initPos + (perc * trajectory);

            if (entity != null) trajectory = entity.transform.TransformPoint(shootPackage.relativeHitPoint) - initPos;

            yield return null;

        }
        
        //Visually kill the entity if still alive
        if (entity != null && entity.gameObject.activeSelf) {

            if (entitiesToKill.Contains(entity)) {

                entitiesToKill.Remove(entity);

                projectile.ActivateImpact(isCritical);

                if (projectile.graphics != null) projectile.graphics.SetActive(false);

                //TEST -> UNCOMMENT THIS!
                if (isCritical && projectile.CheckProbability())
                    projectile.TriggerEffect();

                if (entity.lifes <= 0) {

                    entity.GetScore();
                    entity.Die();

                }

            }

            /*if (!shootController.shootPackage.visuallyUsed) {

                projectile.ActivateImpact(isCritical);

                if (entity.lifes <= 0) {

                    entity.Die();

                }

                shootController.SetPackageVisuallyUsed();

            }*/

        }

        

        //Wait until the trails fades out
        //TrailRenderer trail = projectileInstance.GetComponentInChildren<TrailRenderer>();
        if (projectile.longestTiming > 0) {

            yield return new WaitForSeconds(projectile.longestTiming);

        }

        projectile.Reset();

        projectileInstance.transform.SetParent(transform);
        projectileInstance.SetActive(false);
        projectilePool.Enqueue(projectileInstance);

    }

    public void ShowMuzzleFlash(int selection) {

        if (selection == -1) {

            for(int i = 0; i < muzzleFlashes.Length; i++) {

                if (muzzleFlashes[i] != null) {

                    ParticleSystem muzzle = muzzleFlashes[i].GetComponent<ParticleSystem>();

                    if (muzzle != null) muzzle.Play();
                    else muzzleFlashes[i].SetActive(true);

                }

            }

        } else {

            if (muzzleFlashes[selection] != null) {

                ParticleSystem muzzle = muzzleFlashes[selection].GetComponent<ParticleSystem>();

                if (muzzle != null) muzzle.Play();
                else muzzleFlashes[selection].SetActive(true);

            }

        }

    }

    public void HideMuzzleFlash() {

        for (int i = 0; i < muzzleFlashes.Length; i++) {

            if (muzzleFlashes[i] != null) {

                ParticleSystem muzzle = muzzleFlashes[i].GetComponent<ParticleSystem>();

                if (muzzle != null) muzzle.Stop();
                else muzzleFlashes[i].SetActive(false);

            }

        }

    }

}
