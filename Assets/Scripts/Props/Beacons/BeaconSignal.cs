﻿using UnityEngine;
using System.Collections;

public class BeaconSignal : MonoBehaviour {
    
    public float signalRate = 0.5f;

    float signalTimer = 0f;
    int childIndex = 0;
    
    void Update () {

        signalTimer += Time.deltaTime;
        
        if(signalTimer >= signalRate) {
            
            transform.GetChild(childIndex).GetChild(0).Find("Halo").gameObject.SetActive(true);
            transform.GetChild(childIndex).GetChild(1).Find("Halo").gameObject.SetActive(true);

            if(childIndex > 0) {

                transform.GetChild(childIndex - 1).GetChild(0).Find("Halo").gameObject.SetActive(false);
                transform.GetChild(childIndex - 1).GetChild(1).Find("Halo").gameObject.SetActive(false);

            }else if (childIndex == 0) {

                transform.GetChild(transform.childCount - 1).GetChild(0).Find("Halo").gameObject.SetActive(false);
                transform.GetChild(transform.childCount - 1).GetChild(1).Find("Halo").gameObject.SetActive(false);

            }

            signalTimer = 0f;
            childIndex++;

            if (childIndex >= transform.childCount) {

                childIndex = 0;

            }

        }

	}

}
