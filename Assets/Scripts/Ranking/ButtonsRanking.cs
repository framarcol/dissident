﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonsRanking : MonoBehaviour
{
    public Canvas enterName, points;
    public InputField myName;
    public Text txtResultado;
    public int puntos;
    WWW itemsData;

    public void BtnViewRanking()
    {
        txtResultado.text = "";

        puntos = GameObject.Find("Scripts").GetComponent<CheckPoints>().pointsStage;

        if (myName.text == "")
        {
            txtResultado.text = "Introduce un nombre antes de continuar";
        }
        else
        {
            StartCoroutine(SubirPuntuacion());
            enterName.gameObject.SetActive(false);
            points.gameObject.SetActive(true);
        }
    }

    public IEnumerator SubirPuntuacion()
    {
        itemsData = new WWW("agapornigames.com/dodongo/subirPuntuacion.php?nick=" + myName.text + "&puntuacion=" + puntos);

        yield return itemsData;
    }
}