﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckPoints : MonoBehaviour {

    public Canvas enterName, points;
    public int pointsStage = 0, puntosBD = 0;
    WWW itemsData;
    string[] puntuaciones;


    void Start () {

        enterName.gameObject.SetActive(false);
        points.gameObject.SetActive(false);

        StartCoroutine(CompruebaPuntuacion());

	}

    public IEnumerator CompruebaPuntuacion()
    {
        itemsData = new WWW("agapornigames.com/dodongo/cargarPuntuacion.php");

        yield return itemsData;

        puntuaciones = itemsData.text.Split(';');

        puntosBD = int.Parse(puntuaciones[9]);

        if (pointsStage > puntosBD)
        {
            enterName.gameObject.SetActive(true);
        }
        else
        {
            points.gameObject.SetActive(true);
        }
    }
}
