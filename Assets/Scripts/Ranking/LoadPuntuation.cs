﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadPuntuation : MonoBehaviour {


    WWW itemsData, itemsData2;
    public GameObject menuControlador;
    public Canvas canvasPoints;
    public Text[] textosRanking = new Text[11];
    string[] puntuaciones;
    int nivelActual;
    int idUsuario;
    string puestoRanking;

    public void CargarPuntos()
    {
        StartCoroutine(RankingMundialFase(false));
    }

    public void CargarPuntosAmigos()
    {
        Limpiar();
        StartCoroutine(RankingMundialFase(true));
    }

    public IEnumerator RankingMundialFase(bool esAmigo)
    {
        
      //  MenuControler m = menuControlador.GetComponent<MenuControler>();

      //  nivelActual = m.currentLevel;
    //    idUsuario = m.idUsuario;

        WWWForm datos1 = new WWWForm();

        datos1.AddField("num", nivelActual);
        datos1.AddField("id", idUsuario);


        if (esAmigo)
        {
            itemsData = new WWW("http://agapornigames.com/dodongo/cargarPuntuacionAmigos.php", datos1);
        }else
        {
            itemsData = new WWW("http://agapornigames.com/dodongo/cargarPuntuacion.php", datos1);
        }

        yield return itemsData;

        puntuaciones = itemsData.text.Split(';');  

        int contador = 0;

        if (puntuaciones.Length > 1){

            while (contador < 10 && contador < (puntuaciones.Length - 1) - 2)
            {
                textosRanking[contador].text = puntuaciones[contador]; ;
                textosRanking[contador + 1 ].text = puntuaciones[contador + 1];
                contador += 2;
            }

            textosRanking[10].text = "Tu puntuación: " + puntuaciones[puntuaciones.Length - 2] + "º  " + puntuaciones[puntuaciones.Length-1];

        }

        

    }

    public void Limpiar()
    {
        int contadorLimpiar = 0;
        while (contadorLimpiar < 11)
        {
            textosRanking[contadorLimpiar].text = "";
            contadorLimpiar++;
        }
    }
}
