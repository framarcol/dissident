﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class CancelDashArea : MonoBehaviour { 
    
	void Start () {

        BoxCollider collider = GetComponent<BoxCollider>();
        collider.isTrigger = true;

	}

    void OnTriggerEnter(Collider other) {

        //If the player approaches to this area cancel the side movement
        if(other.tag == "Player") {
            
            //other.GetComponentInParent<PlayerMovementController>().canMoveToSides = false;

        }

    }

}
