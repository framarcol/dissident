﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RoadController))]
public class RoadControllerEditor : Editor {

    private RoadController roadController;

    public override void OnInspectorGUI() {

        roadController = (RoadController)target;

        //Object fields to select the rail and rotation area prefabs to be used for the framework
        roadController.rail = EditorGUILayout.ObjectField("Rail node prefab: ", roadController.rail, typeof(GameObject), false) as GameObject;
        roadController.rotationArea = EditorGUILayout.ObjectField("Rotation area prefab: ", roadController.rotationArea, typeof(GameObject), false) as GameObject;
        roadController.loopRequisitesController = EditorGUILayout.ObjectField("Loop requisites checker prefab: ", roadController.loopRequisitesController, typeof(GameObject), false) as GameObject;

        roadController.roadPartsSystemEnabled = EditorGUILayout.Toggle("Road parts system enable: ", roadController.roadPartsSystemEnabled);
        EditorGUILayout.Space();

        //EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Road Length: ");
        EditorGUILayout.TextField(roadController.roadLength.ToString());
        
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Calculate Length")) {
            roadController.CalculateRoadLength(); 
        }
        EditorGUI.indentLevel++;
        for(int i = 0; i < roadController.checkpointDistances.Count; i++) {
            //EditorGUILayout.PrefixLabel("Checkpoint " + (i+1));
            EditorGUILayout.FloatField("Checkpoint " + (i + 1), roadController.checkpointDistances[i]);
        }
        EditorGUI.indentLevel--;
        EditorGUILayout.Space();

        //If none of the before where set then don't show the create node button
        if (roadController.rail != null && roadController.rotationArea != null && GUILayout.Button("Add node")) {

            roadController.CreatePoint(false);

        }

        //If there's at least one node, offer the choice of deleting nodes
        if (roadController.points.Count > 0 && GUILayout.Button("Delete node")) {

            roadController.DeletePoint();

        }

        if (roadController.points.Count > 0 && GUILayout.Button("Update road")) {

            roadController.UpdateRoad();

        }

        EditorUtility.SetDirty(roadController);

    }

}
