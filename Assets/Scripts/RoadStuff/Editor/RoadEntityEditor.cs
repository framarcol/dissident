﻿using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(RoadEntity))][CanEditMultipleObjects]
public class RoadEntityEditor : Editor {

    private RoadEntity entity;

    SerializedProperty alwaysVisibleProp;
    SerializedProperty threatLevelProp;

    void OnEnable() {

        alwaysVisibleProp = serializedObject.FindProperty("alwaysVisible");
        threatLevelProp = serializedObject.FindProperty("threatLevel");

    }

	public override void OnInspectorGUI() {

        serializedObject.Update();

        EditorGUILayout.PropertyField(threatLevelProp, new GUIContent("Threat Level"));

        EditorGUILayout.PropertyField(alwaysVisibleProp, new GUIContent("Always visible"));
        
        /*if(GUILayout.Button("Get thumbnail")) {

            GetPreviewStuff();

        }*/

        serializedObject.ApplyModifiedProperties();

    }

    void OnSceneGUI() {

        entity = (RoadEntity)target;

        if (entity.associatedPoint == null) return;

        Event ev = Event.current;

        if(ev.isKey && ev.type == EventType.KeyDown) {

            if(ev.keyCode == KeyCode.Delete) {

                Event.current.Use();

                entity.associatedPoint.DeleteEntity(entity.gameObject);
                
            } else if (ev.keyCode == KeyCode.D) {

                Event.current.Use();

                entity.associatedPoint.DuplicateEntity(entity.gameObject);

            }

        }

        Handles.color = Color.yellow;
        Handles.SphereCap(0, entity.associatedPoint.transform.position, entity.associatedPoint.transform.rotation, HandleUtility.GetHandleSize(entity.associatedPoint.transform.position) * 0.2f);

    }

    /*private void GetPreviewStuff() {

        PreviewRenderUtility pru = new PreviewRenderUtility();

        pru.BeginStaticPreview(new Rect(0, 0, 512, 512));

        pru.m_Camera.transform.position = -Vector3.forward * 10f;

        MeshFilter[] meshFilters = entity.gameObject.GetComponentsInChildren<MeshFilter>();

        foreach (MeshFilter meshFilter in meshFilters) {

            pru.DrawMesh(meshFilter.sharedMesh, Vector3.zero, Quaternion.identity, meshFilter.GetComponent<MeshRenderer>().sharedMaterial, 0);

        }

        pru.m_Camera.Render();

        Texture2D preview = pru.EndStaticPreview();

        string originalName = PrefabUtility.GetPrefabParent(entity.gameObject).name;

        File.WriteAllBytes(Application.dataPath + "/Editor Default Resources/Thumbnails/" + originalName + ".png", preview.EncodeToPNG());

        AssetDatabase.Refresh();

    }*/

}
