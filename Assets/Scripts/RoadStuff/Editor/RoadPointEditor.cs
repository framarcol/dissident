﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

[CustomEditor(typeof(RoadPoint))]
public class RoadPointEditor : Editor {

    private RoadPoint roadPoint;

    //private GUIStyle buttonStyle;
    private Vector2 modulesScrollPosition;

    //private float actualWidth;
    private int entitiesColumns = 3;
    private float entityButtonHeight = 75f;

    private static int selectedTheme = 0;
    private static bool selectedCommon = true;

    void OnEnable() {

        Tools.hidden = true;
        //BuildPipeline.BuildAssetBundles("", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    void OnDisable() {

        Tools.hidden = false;

    }

    public override void OnInspectorGUI() {

        roadPoint = (RoadPoint)target;
        
        //If there's no road controller set on this node then don't show anything else
        if (roadPoint.road == null) {

            //Show the rail list so it can be populated
            EditorGUILayout.PropertyField(serializedObject.FindProperty("rails"), new GUIContent("Rails: "), true);

            return;

        }

        GUILayout.Label("World Position: " + roadPoint.transform.position);

        //If the node is not a master turn node, then offer the option to become one
        if (!roadPoint.masterTurn && GUILayout.Button("Create turn node")) {

            roadPoint.CreateTurnNode();

        }

        //If the node is already a master turn node...
        if (roadPoint.masterTurn) {

            //Select the axis of the turn (X, Y, Z)
            Axis oldAxis = roadPoint.turnPivotAxis;
            roadPoint.turnPivotAxis = (Axis)EditorGUILayout.EnumPopup("Axis: ", roadPoint.turnPivotAxis);

            //Select the direction of the turn (Positive, Negative)
            TurnDirection oldDirection = roadPoint.turnPivotDirection;
            roadPoint.turnPivotDirection = (TurnDirection)EditorGUILayout.EnumPopup("Turn Direction: ", roadPoint.turnPivotDirection);

            //Set the offset for the pivot for the turn
            float oldOffset = roadPoint.pivotOffset;
            roadPoint.pivotOffset = EditorGUILayout.Slider("Pivot offset: ", roadPoint.pivotOffset, 0f, 100f);

            //Always recalculate where the pivot is gonna be depending of the previous choices
            roadPoint.CalculatePivotCenter(roadPoint.pivotOffset);

            //Show where the pivot is gonna be in world coordinates
            EditorGUILayout.LabelField("Pivot center: " + roadPoint.turnPivotCenter);

            //Set the angle for the turn, until 180º
            float beforeAngle = roadPoint.turnAngle;
            roadPoint.turnAngle = EditorGUILayout.Slider("Turn angle: ", roadPoint.turnAngle, 0f, 180f);

            //Calculate some variables that indicates possible changes in the setup for the turning
            bool axisChanged = oldAxis != roadPoint.turnPivotAxis;
            bool directionChanged = oldDirection != roadPoint.turnPivotDirection;
            bool offsetChanged = oldOffset != roadPoint.pivotOffset;

            //If something changed in the options, then reset the slave node and recalculate the turning with the new data
            if (axisChanged || directionChanged || offsetChanged) {

                roadPoint.ResetSlave();

                roadPoint.TurnSlave(roadPoint.turnAngle);


            } else {

                //If nothing important changed then turn the slave node with the new delta angle if there's any
                float deltaAngle = roadPoint.turnAngle - beforeAngle;
                roadPoint.TurnSlave(deltaAngle);

            }

            EditorGUILayout.BeginHorizontal();
            roadPoint.isBiRoadLeft  = EditorGUILayout.Toggle("BiRoad Left: ", roadPoint.isBiRoadLeft);
            roadPoint.isBiRoadRight = EditorGUILayout.Toggle("BiRoad Right: ", roadPoint.isBiRoadRight);
            EditorGUILayout.EndHorizontal();

            //Set the time for the turning, this will be just passed to the rotation area that manages the turn
            //roadPoint.rotationTime = EditorGUILayout.FloatField("Turn duration: ", roadPoint.rotationTime);

            //At last update the rotation area associated with the info from the master node
            roadPoint.UpdateRotationArea();

            EditorGUILayout.Space();

            roadPoint.isLimitPoint = EditorGUILayout.Toggle("Is Limit Point: ", roadPoint.isLimitPoint);

            EditorGUILayout.Space();

            ////////////////////
            //ROTATION MODULES
            ////////////////////
            GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.richText = true;

            //If there's already a module in this node...
            if (roadPoint.moduleOnPoint != null) {

                EditorGUILayout.BeginHorizontal();

                //Show the name of the associated module
                EditorGUILayout.LabelField("<b>Actual rotation module: </b>" + roadPoint.moduleOnPoint.name, labelStyle);

                //Offer the choice of selecting the associated module
                if (GUILayout.Button("Select", GUILayout.Width(100f))) {

                    Selection.activeGameObject = roadPoint.moduleOnPoint;

                }

                //Offer the choice to destroy the associated module
                if (GUILayout.Button("-", GUILayout.Width(35f))) {

                    DestroyImmediate(roadPoint.moduleOnPoint);

                    Debug.Log("Module deleted");

                }

                EditorGUILayout.EndHorizontal();

            }
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Rotation modules", EditorStyles.boldLabel);

            modulesScrollPosition = GUILayout.BeginScrollView(modulesScrollPosition, "Box", GUILayout.Height(100f));

            //Get the prefab files in the rotation modules path (aka "Assets/Prefabs/Modules/Rotation...")
            string[] modulesFiles = Directory.GetFiles(roadPoint.road.modulesPath + "Rotation/", "*.prefab");

            //Iterate through each file path in the array
            foreach (string path in modulesFiles) {

                EditorGUILayout.BeginHorizontal();

                //Show the name of the prefab
                EditorGUILayout.LabelField(Path.GetFileName(path));

                //Show an add button to create the module selected and associate it to the node
                if (GUILayout.Button("+", GUILayout.Width(35f))) {

                    //Get the prefab through the selected path
                    GameObject original = (GameObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));

                    if (original != null) {

                        if (roadPoint.moduleOnPoint != null) {

                            DestroyImmediate(roadPoint.moduleOnPoint);

                        }

                        roadPoint.moduleOnPoint = PrefabUtility.InstantiatePrefab(original) as GameObject;
                        roadPoint.moduleOnPoint.transform.position = roadPoint.transform.position;
                        roadPoint.moduleOnPoint.transform.rotation = roadPoint.transform.rotation;
                        roadPoint.moduleOnPoint.name = original.name;
                        roadPoint.moduleOnPoint.transform.parent = roadPoint.road.modulesContainer.transform;

                        Debug.Log("Module created from: " + path);

                    }

                }

                EditorGUILayout.EndHorizontal();

            }

            GUILayout.EndScrollView();
            EditorGUILayout.Space();

            if (GUILayout.Button("Create loop requisite checker")) {

                roadPoint.CreateLoopRequisiteChecker();

            }


        //If it's not a master node...
        } else {

            if(GUILayout.Button("Set player")) {

                GameObject playerSetup = GameObject.Find("/PlayerSetup");

                if(playerSetup != null) {

                    Vector3 position = roadPoint.transform.position + roadPoint.transform.up;

                    playerSetup.transform.position = position;
                    playerSetup.transform.rotation = roadPoint.transform.rotation;

                }

            }

            ////////////////
            //MODULES MENU
            //Set a foldout for the list of the available modules
            ////////////////
            GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.richText = true;

            //If there's already a module in this node...
            if (roadPoint.moduleOnPoint != null) {

                EditorGUILayout.BeginHorizontal();

                //Show the name of the associated module
                EditorGUILayout.LabelField("<b>Actual module: </b>" + roadPoint.moduleOnPoint.name, labelStyle);

                //Offer the choice of selecting the associated module
                if (GUILayout.Button("Select", GUILayout.Width(100f))) {

                    Selection.activeGameObject = roadPoint.moduleOnPoint;

                }

                //Offer the choice to destroy the associated module
                if (GUILayout.Button("-", GUILayout.Width(35f))) {

                    DestroyImmediate(roadPoint.moduleOnPoint);

                    Debug.Log("Module deleted");

                }

                EditorGUILayout.EndHorizontal();

            }
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Modules", EditorStyles.boldLabel);

            modulesScrollPosition = GUILayout.BeginScrollView(modulesScrollPosition, "Box", GUILayout.Height(100f));

            //Get the prefab files in the modules path (aka "Assets/Prefabs/Modules/...")
            string[] modulesFiles = Directory.GetFiles(roadPoint.road.modulesPath, "*.prefab");

            //Iterate through each file path in the array
            foreach (string path in modulesFiles) {

                EditorGUILayout.BeginHorizontal();

                //Show the name of the prefab
                EditorGUILayout.LabelField(Path.GetFileName(path));

                //Show an add button to create the module selected and associate it to the node
                if (GUILayout.Button("+", GUILayout.Width(35f))) {

                    //Get the prefab through the selected path
                    GameObject original = (GameObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));

                    if (original != null) {

                        if (roadPoint.moduleOnPoint != null) {

                            DestroyImmediate(roadPoint.moduleOnPoint);

                        }

                        roadPoint.moduleOnPoint = PrefabUtility.InstantiatePrefab(original) as GameObject;
                        roadPoint.moduleOnPoint.transform.position = roadPoint.transform.position;
                        roadPoint.moduleOnPoint.transform.rotation = roadPoint.transform.rotation;
                        roadPoint.moduleOnPoint.name = original.name;
                        roadPoint.moduleOnPoint.transform.parent = roadPoint.road.modulesContainer.transform;

                        Debug.Log("Module created from: " + path);

                    }

                }

                EditorGUILayout.EndHorizontal();

            }

            GUILayout.EndScrollView();
            
        }

        /////////////////
        //ENTITIES MENU
        /////////////////
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Entities", EditorStyles.boldLabel);

        //EditorGUI.indentLevel++;
        AssetPreview.SetPreviewTextureCacheSize(256);

        GUIStyle foldoutStyle = EditorStyles.foldout;
        foldoutStyle.fontStyle = FontStyle.Bold;

        //Get the directories treating them like entitie's categories
        string[] entitiesThemes = Directory.GetDirectories(roadPoint.road.entitiesPath);

        //foreach (string theme in entitiesThemes) {

        for (int i = 0; i < entitiesThemes.Length; i++) {

            EditorGUI.indentLevel++;

            string themeName = Path.GetFileName(entitiesThemes[i]);

            bool isSelected = selectedTheme == i;
            bool beforeSelected = isSelected;
            isSelected = EditorGUILayout.Foldout(isSelected, themeName.ToUpper(), foldoutStyle);
            //EditorGUILayout.Label(themeName);
            
            if (isSelected) {

                selectedTheme = i;

                string[] entitiesCategories = Directory.GetDirectories(entitiesThemes[i]);
                //Iterate through the categories taking the entities and drawing them
                foreach (string category in entitiesCategories) {

                    EditorGUI.indentLevel++;

                    string categoryName = Path.GetFileName(category);

                    EditorGUILayout.LabelField(categoryName);

                    string[] entities = Directory.GetFiles(category, "*.prefab");
                    DrawCategory(categoryName, entities);

                    EditorGUI.indentLevel--;

                }

                //Get the uncategorized entities if there's any and draw them
                string[] entitiesMisc = Directory.GetFiles(entitiesThemes[i], "*.prefab");
                if (entitiesMisc.Length > 0) {

                    EditorGUI.indentLevel++;

                    EditorGUILayout.LabelField("Other");

                    DrawCategory(null, entitiesMisc);

                    EditorGUI.indentLevel--;

                }

                EditorGUILayout.Space();

            } else if(beforeSelected && !isSelected){

                selectedTheme = -1;

            }

            //
            EditorGUI.indentLevel--;

        }

        //Get the uncategorized entities if there's any and draw them
        string[] entitiesCommon = Directory.GetFiles(roadPoint.road.entitiesPath, "*.prefab");
        if (entitiesCommon.Length > 0) {

            EditorGUI.indentLevel++;

            selectedCommon = EditorGUILayout.Foldout(selectedCommon, "COMMON", foldoutStyle);

            if (selectedCommon) {

                DrawCategory(null, entitiesCommon);

            }

            EditorGUI.indentLevel--;

        }

        EditorGUI.indentLevel--;

        //Always make a possible undo for the object and set the scene dirty
        Undo.RecordObject(roadPoint, "Properties changed");
        EditorUtility.SetDirty(roadPoint);

    }

    //Takes the entities of a category and draws the buttons for them in a grid pattern
    private void DrawCategory(string category, string[] items) {

        EditorGUILayout.BeginHorizontal();

        GUILayout.Space(30f);

        int rows = (items.Length % entitiesColumns == 0) ? items.Length / entitiesColumns : items.Length / entitiesColumns + 1;
        Rect controlRect = EditorGUILayout.GetControlRect(false, entityButtonHeight * rows);

        float desiredWidth = controlRect.width / entitiesColumns;

        for (int i = 0; i < items.Length; i++) {

            Rect buttonPos = new Rect(controlRect.x + ((i % entitiesColumns) * desiredWidth), controlRect.y + ((i / entitiesColumns) * entityButtonHeight), /*actualWidth*/desiredWidth, entityButtonHeight);

            string itemName = Path.GetFileNameWithoutExtension(items[i]);

            //Texture buttonTexture = EditorGUIUtility.Load(/*"Thumbnails/" + category + "/" + */"Thumbnails/" + itemName + ".png") as Texture;

            GameObject original = (GameObject)AssetDatabase.LoadAssetAtPath(items[i], typeof(GameObject));

            Texture2D preview = AssetPreview.GetAssetPreview(original);

            //while (AssetPreview.IsLoadingAssetPreviews()) { } 
            //Texture preview = GetPreviewStuff(original, buttonPos);

            bool click = (preview != null) ? GUI.Button(buttonPos, new GUIContent(preview, itemName)) : GUI.Button(buttonPos, itemName);

            if (click) {
                
                if (original.GetComponent<RoadEntity>() != null) {

                    roadPoint.CreateEntity(original, category);

                }

            }

        }

        EditorGUILayout.EndHorizontal();

    }

    void OnSceneGUI() {

        roadPoint = (RoadPoint)target;

        //If the node is not a slave turning node then...
        if (!(roadPoint.isTurnNode && !roadPoint.masterTurn)) {

            EditorGUI.BeginChangeCheck();

            //Draw only a Z axis for the point to constrain the movement in just that axis
            Vector3 newPointPosition = Handles.Slider(roadPoint.transform.position, roadPoint.transform.forward, HandleUtility.GetHandleSize(roadPoint.transform.position), Handles.ArrowCap, 1.0f);

            //If it moves then update it and record the object in the undo stack
            if (EditorGUI.EndChangeCheck()) {
                
                Undo.RecordObject(roadPoint.transform, "Node moved");
                EditorUtility.SetDirty(roadPoint);

                roadPoint.MovePointPosition(newPointPosition);

            }

        }

        //Keyboard shortcut for creating new nodes from other nodes
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyDown) {

            //If the user press N in the scene view while selecting a node, then create a new node after the selected one
            if (e.keyCode == KeyCode.N) {

                Event.current.Use();

                if (roadPoint.masterTurn) {

                    roadPoint.road.CreatePoint(false);
                    Selection.activeGameObject = roadPoint.road.points[roadPoint.road.points.Count - 1].gameObject;

                } else {

                    Selection.activeGameObject = roadPoint.road.CreatePoint(false, roadPoint.road.points.IndexOf(roadPoint));

                }

            //If the user press M then delete the selected node
            }else if (e.keyCode == KeyCode.M) {

                Event.current.Use();

                if (roadPoint.masterTurn) {

                    roadPoint.road.DeletePoint();

                } else {

                    roadPoint.road.DeletePoint(roadPoint.road.points.IndexOf(roadPoint));

                }

            } else if (e.keyCode == KeyCode.B) {

                Event.current.Use();

                Vector3 targetPos = roadPoint.transform.position;

                SceneView.lastActiveSceneView.LookAt(targetPos, SceneView.lastActiveSceneView.rotation, 10f);
                SceneView.lastActiveSceneView.Repaint();

            }
            
        }

    }

}
