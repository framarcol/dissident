﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class LetDashArea : MonoBehaviour {

    void Start() {

        BoxCollider collider = GetComponent<BoxCollider>();
        collider.isTrigger = true;

    }

    //When the player exits this area then let it move to the sides again
    void OnTriggerExit(Collider other) {

        if (other.tag == "Player") {

            other.GetComponent<PlayerMovementController>().canMoveToSides = true;

        }

    }

}
