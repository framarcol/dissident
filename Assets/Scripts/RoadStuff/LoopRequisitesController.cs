﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

[Serializable]
public class OnCheckEvent : UnityEvent<GameObject> { }

public class LoopRequisitesController : MonoBehaviour {

    [HideInInspector]
    public GameObject rotationArea;
    
    public OnCheckEvent onCheck;

    //public CheckStatus checkStatus;
    
    void Start () {

        //checkStatus = new CheckStatus();
        
    }
	
	// Update is called once per frame
	void Update () {
	    


	}

    void OnTriggerEnter(Collider other) {

        if(other.tag == "Player") {

            onCheck.Invoke(rotationArea);
            
        }

    }
    
}
