﻿using UnityEngine;
//using UnityEditor;
using System.Collections.Generic;

[ExecuteInEditMode, AddComponentMenu("Road/Road Controller"), System.Serializable]
public class RoadController : MonoBehaviour {

    public GameObject rail;
    public GameObject rotationArea;
    public GameObject loopRequisitesController;

    public GameObject blankContainer;

    public List<RoadPoint> points = new List<RoadPoint>();

    public float roadLength = 0;

    public bool roadPartsSystemEnabled = false;
    public int actualLimitPoint = -1;
    public int oldLimitPoint = -1;
    public bool needToDisable = false;

    public string modulesPath = "Assets/Prefabs/Modules/";
    public string entitiesPath = "Assets/Prefabs/Entities/";

    public GameObject entitiesContainer;
    public GameObject modulesContainer;

    PlayerMovementController playerMovementController;

    public List<float> checkpointDistances = new List<float>();

    void Start() {

#if UNITY_EDITOR

        if (!Application.isPlaying) {

            if (blankContainer == null) {

                blankContainer = new GameObject("Blank");

                blankContainer.transform.position = transform.position;
                blankContainer.transform.rotation = transform.rotation;
                blankContainer.transform.parent = transform;

                blankContainer.hideFlags = HideFlags.HideInHierarchy;

            }

            entitiesContainer = GameObject.Find("/Entities");

            if (entitiesContainer == null) {

                entitiesContainer = new GameObject();
                entitiesContainer.name = "Entities";

            }

            modulesContainer = GameObject.Find("/Modules");

            if (modulesContainer == null) {

                modulesContainer = new GameObject();
                modulesContainer.name = "Modules";

            }

            playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        }

#endif

        //When run the game make sure all the road, except the first part and the permanent entities, is disabled
        if (Application.isPlaying && roadPartsSystemEnabled) {

            bool limitFound = false;

            foreach (RoadPoint roadPoint in points) {

                if (limitFound) {

                    if(roadPoint.moduleOnPoint != null) roadPoint.moduleOnPoint.SetActive(false);
                    foreach(GameObject entity in roadPoint.entitiesOnPoint) {

                        RoadEntity roadEntity = entity.GetComponent<RoadEntity>();

                        if(!roadEntity.alwaysVisible) entity.SetActive(false);

                    }

                }

                if (roadPoint.isLimitPoint && !limitFound) {

                    limitFound = true;
                    actualLimitPoint = points.IndexOf(roadPoint);
                    
                }

            }

        }

    }

    public void CalculateRoadLength() {

        float distance = 0;

        int count = points.Count;

        checkpointDistances.Clear();

        for (int i = 1; i < count; i++) {
            
            if (points[i].isTurnNode && !points[i].masterTurn) {

                float radius = (points[i - 1].rails[2].position - points[i - 1].turnPivotCenter).magnitude;

                distance += (Mathf.Deg2Rad * points[i - 1].turnAngle) * radius;

            } else {

                distance += points[i].transform.localPosition.z;

            }

            
            points[i].FilterEntities();
            List<GameObject> entities = points[i].entitiesOnPoint;
            for(int j = 0; j < entities.Count; j++) {

                CheckpointArea checkpoint = entities[j].GetComponent<CheckpointArea>();

                if (checkpoint != null) {

                    float checkpointDistance = 0;

                    Vector3 relativeCheckpointPosition = points[i].transform.InverseTransformPoint(checkpoint.transform.position);
                    if(relativeCheckpointPosition.z < 0)
                        checkpointDistance = distance - (checkpoint.transform.position - points[i].transform.position).magnitude;
                    else
                        checkpointDistance = distance + (checkpoint.transform.position - points[i].transform.position).magnitude;

                    /*if (j < checkpointDistances.Count) checkpointDistances[j] = checkpointDistance;
                    else*/
                    checkpointDistances.Add(checkpointDistance);

                    //checkpoint.distanceFromStart = distance + (checkpoint.transform.position - points[i].transform.position).magnitude;

                    //Debug.Log(checkpoint.name);

                }

            }

        }

        roadLength = distance;

    }

    public GameObject CreatePoint(bool isTurnNode, int index = -1) {

        RoadPoint roadPoint;

        //Cases when the index is a correct one (not -1, not the last one, not when just one point)
        if (index != -1 && index < points.Count-1 && points.Count > 1) {

            //Take the future next and before point
            Transform nextPoint = points[index + 1].transform;
            Transform beforePoint = points[index].transform;

            //Calculate the new position for the point depending of it is a turn node or not
            Vector3 newPos;

            if (isTurnNode) {

                newPos = beforePoint.position;

            } else {

                newPos = (nextPoint.position + beforePoint.position) / 2;

            }

            //Create it at the calculated position
            roadPoint = ((GameObject)Instantiate(rail, newPos, points[index].transform.rotation)).GetComponent<RoadPoint>();

            //Set its name to the original's name
            roadPoint.name = rail.name;

            //Update the parenting to include the new point
            roadPoint.transform.parent = beforePoint;
            nextPoint.transform.parent = roadPoint.transform;

            //Initialize some variables
            roadPoint.road = this;
            roadPoint.isTurnNode = isTurnNode;

            //Insert it in the list in the correct position
            points.Insert(index + 1, roadPoint);
        
        //If the index is not suitable then add the point to the end
        } else {

            if (points.Count == 0) {

                roadPoint = ((GameObject)Instantiate(rail, transform.position + new Vector3(0, -1f, 0), transform.rotation)).GetComponent<RoadPoint>();

                //The firt point created must be a child of the invisible container. That way all the point hierarchy will be hidden from the user
                roadPoint.transform.parent = blankContainer.transform;

            } else {

                Transform lastPoint = points[points.Count - 1].transform;

                roadPoint = ((GameObject)Instantiate(rail, lastPoint.position, lastPoint.rotation)).GetComponent<RoadPoint>();
                roadPoint.transform.parent = lastPoint;

                if (isTurnNode) roadPoint.transform.localPosition = Vector3.zero;
                else roadPoint.transform.localPosition = new Vector3(0, 0, 40);

            }

            roadPoint.road = this;
            roadPoint.isTurnNode = isTurnNode;

            points.Add(roadPoint);

        }

        //Return the point created
        return roadPoint.gameObject;

    }

    public void DeletePoint(int index = -1) {

        RoadPoint point;

        //Cases when the index is acceptable (not -1, not 0, not the last one)
        if (index > 0 && index < points.Count - 1) {

            //Take the point after and before this
            RoadPoint nextPoint = points[index + 1];
            RoadPoint beforePoint = points[index - 1];

            //Take the point to be deleted
            point = points[index];
            
            //If it's a turn point then reset everything in the point before (the master)
            if (point.isTurnNode) {
                
                beforePoint.masterTurn = false;
                if (!points[index - 2].masterTurn) beforePoint.isTurnNode = false;
                beforePoint.ResetSlave();
                beforePoint.slaveNode = null;
                beforePoint.Reset();

            }

            //Remove it from the list
            points.Remove(point);

            //Update the parenting between the after and before point
            nextPoint.transform.parent = beforePoint.transform;

        //If the index is not suitable then delete the last point
        } else {

            point = points[points.Count - 1];
            points.Remove(point);

            if (point.isTurnNode && !point.masterTurn) {

                RoadPoint master = points[points.Count - 1];
                master.masterTurn = false;

                if (!points[points.Count - 2].masterTurn) master.isTurnNode = false;

                master.slaveNode = null;
                master.Reset();

            }

        }

        //Destroy the associated entities and module if exist and clear the data lists
        if(point.entitiesOnPoint.Count > 0) {

            for (int i = 0; i<point.entitiesOnPoint.Count; i++) {

                DestroyImmediate(point.entitiesOnPoint[i]);

            }

        }
        if(point.moduleOnPoint != null) {

            DestroyImmediate(point.moduleOnPoint);

        }

        //Destroy the point
        DestroyImmediate(point.gameObject);

    }

    //Get the entity category object if exists or create it
    public GameObject GetCategoryContainer(string category) {

        GameObject categoryContainer = GameObject.Find(category);

        if(categoryContainer == null) {

            categoryContainer = new GameObject();
            categoryContainer.name = category;
            categoryContainer.transform.parent = entitiesContainer.transform;

        }

        return categoryContainer;

    }

    //Update road stuff
    public void UpdateRoad() {

        foreach (RoadPoint roadPoint in points) {

            if (roadPoint.masterTurn) roadPoint.UpdateRotationArea();
            roadPoint.UpdateEntitiesPositions();

        }

    }

    public void EnableNextPart() {

        if (actualLimitPoint == -1) return;

        for (int i = actualLimitPoint+1; i < points.Count; i++) {

            if (points[i].moduleOnPoint != null) points[i].moduleOnPoint.SetActive(true);

            foreach (GameObject entity in points[i].entitiesOnPoint) {

                if (entity != null)  entity.SetActive(true);
                
            }

            if (points[i].isLimitPoint) {

                oldLimitPoint = actualLimitPoint;
                actualLimitPoint = i;

                needToDisable = true;

                return;

            }

        }

        oldLimitPoint = actualLimitPoint;
        needToDisable = true;

    }

    public void DisablePreviousPart() {

        if (!needToDisable) return;

        if (oldLimitPoint == -1) return;

        needToDisable = false;

        for (int i = oldLimitPoint-1; i >= 0; i--) {

            if (points[i].moduleOnPoint != null) points[i].moduleOnPoint.SetActive(false);

            foreach (GameObject entity in points[i].entitiesOnPoint) {

                if (entity != null) {

                    RoadEntity roadEntity = entity.GetComponent<RoadEntity>();

                    if (!roadEntity.alwaysVisible) entity.SetActive(false);

                }

            }

            if (points[i].isLimitPoint) return;

        }

    }

    //Drawing in the scene view all the road stuff (straight red lines and blue turns)
    void OnDrawGizmos() {

#if UNITY_EDITOR

        if (!Application.isEditor) return;

        UnityEditor.Handles.color = Color.blue;

        for (int i = 1; i < points.Count; i++) {

            RoadPoint beforePoint = points[i - 1];
            RoadPoint afterPoint  = points[i];

            if (beforePoint.masterTurn) {

                beforePoint.CalculatePivotCenter(beforePoint.pivotOffset);

                if (beforePoint.turnPivotAxis == Axis.x) {

                    if (beforePoint.turnPivotDirection == TurnDirection.Positive) {

                        for (int j = 0; j < beforePoint.rails.Count; j++) {

                            Vector3 pivot = beforePoint.rails[j].transform.position - (beforePoint.rails[j].transform.up * beforePoint.pivotOffset);

                            UnityEditor.Handles.DrawWireArc(pivot, beforePoint.rails[j].transform.right, beforePoint.rails[j].transform.up, beforePoint.turnAngle, beforePoint.pivotOffset);

                        }

                    } else if (beforePoint.turnPivotDirection == TurnDirection.Negative) {

                        for (int j = 0; j < beforePoint.rails.Count; j++) {

                            Vector3 pivot = beforePoint.rails[j].transform.position + (beforePoint.rails[j].transform.up * beforePoint.pivotOffset);

                            UnityEditor.Handles.DrawWireArc(pivot, -beforePoint.rails[j].transform.right, -beforePoint.rails[j].transform.up, beforePoint.turnAngle, beforePoint.pivotOffset);

                        }

                    }

                } else if (beforePoint.turnPivotAxis == Axis.y) {
                    
                    Vector3 pivot = beforePoint.turnPivotCenter;

                    if (beforePoint.turnPivotDirection == TurnDirection.Positive) {

                        Vector3 normal = beforePoint.transform.up;

                        for (int j = 0; j < beforePoint.rails.Count; j++) {

                            UnityEditor.Handles.DrawWireArc(pivot, normal, -beforePoint.rails[j].transform.right, beforePoint.turnAngle, Vector3.Distance(beforePoint.rails[j].transform.position, beforePoint.turnPivotCenter));

                        }

                    } else if (beforePoint.turnPivotDirection == TurnDirection.Negative) {

                        Vector3 normal = -beforePoint.transform.up;

                        for (int j = 0; j < beforePoint.rails.Count; j++) {

                            UnityEditor.Handles.DrawWireArc(pivot, normal, beforePoint.rails[j].transform.right, beforePoint.turnAngle, Vector3.Distance(beforePoint.rails[j].transform.position, beforePoint.turnPivotCenter));

                        }

                    }

                } else if (beforePoint.turnPivotAxis == Axis.z) {

                    Gizmos.color = Color.blue;

                    for (int j = 0; j < beforePoint.rails.Count; j++) {

                        Gizmos.DrawLine(afterPoint.rails[j].position, beforePoint.rails[j].position);

                    }

                }

            } else {

                Gizmos.color = Color.red;

                for (int j = 0; j < afterPoint.rails.Count; j++) {

                    Gizmos.DrawLine(afterPoint.rails[j].position, beforePoint.rails[j].position);

                }

            }

        }

        Gizmos.color = Color.white;
        Gizmos.DrawSphere(transform.position, 0.5f);

#endif

    }

}
