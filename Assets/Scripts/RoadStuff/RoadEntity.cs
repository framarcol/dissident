﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode, AddComponentMenu("Road/Road Entity")]
public class RoadEntity : MonoBehaviour {

    public enum ThreatLevel { NO_THREAT = 0, DANGEROUS = 1 }

    public RoadPoint associatedPoint = null;

    public ThreatLevel threatLevel;

    public bool alwaysVisible;

    Vector3 oldPosition;

#if UNITY_EDITOR

    void Start() {
        
        oldPosition = transform.position;
        
    }

    void LateUpdate() {

        if (Application.isPlaying) return;

        //If the position changed then update local position list of the point if present
        if (transform.position != oldPosition) {

            if (associatedPoint != null) {

                associatedPoint.UpdateEntityLocalPosition(gameObject);

            }

        }

        oldPosition = transform.position;
        
    }

#endif

}
