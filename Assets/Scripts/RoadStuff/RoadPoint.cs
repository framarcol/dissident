﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class RoadPoint : MonoBehaviour {

    public RoadController road;

    public List<Transform> rails = new List<Transform>();

    public bool isTurnNode = false;
    public bool masterTurn = false;
    public bool isBiRoadLeft = false;
    public bool isBiRoadRight = false;

    public bool isLimitPoint = false;

    public GameObject slaveNode;
    public RotationArea rotationArea;

    public float pivotOffset = 0f;
    public Vector3 turnPivotCenter;
    public Axis turnPivotAxis = Axis.y;
    public TurnDirection turnPivotDirection = TurnDirection.Positive;
    public float turnAngle = 0;
    //public float rotationTime = 2f;

    public GameObject moduleOnPoint;
    //public GameObject rotationModuleOnPoint;

    public List<GameObject> entitiesOnPoint = new List<GameObject>();
    public List<Vector3> localEntitiesPositions = new List<Vector3>();

    Vector3 oldPosition;

#if UNITY_EDITOR

    void Start() {
        
        oldPosition = transform.position;

        FilterEntities();
        
    }

    void Update() {

        if (Application.isPlaying) return;

        //Update the module following the position and rotation of the node
        if(moduleOnPoint != null) {

            moduleOnPoint.transform.position = transform.position;
            moduleOnPoint.transform.rotation = transform.rotation;

        }

        UpdateEntitiesPositions();
        
    }

#endif

    public void MovePointPosition(Vector3 newPosition) {

        transform.position = newPosition;

    }

    public void FilterEntities() {

        for (int i = 0; i < entitiesOnPoint.Count; i++) {

            if (entitiesOnPoint[i] == null) {
                entitiesOnPoint.RemoveAt(i);
                localEntitiesPositions.RemoveAt(i);
            }

        }

    }

    public void UpdateEntitiesPositions() {

        //Update entities position and rotation base on this point
        if (entitiesOnPoint.Count == 0) return;
        if (oldPosition != transform.position) {

            for (int i = 0; i < entitiesOnPoint.Count; i++) {

                entitiesOnPoint[i].transform.position = transform.TransformPoint(localEntitiesPositions[i]);
                //entitiesOnPoint[i].transform.rotation = transform.rotation;

            }

        }

        oldPosition = transform.position;

    }

    //Change this node to be a master turn node
    public void CreateTurnNode() {

        //Set the appropiate variables
        isTurnNode = true;
        masterTurn = true;

        //Calculate the pivot of the turn with no initial offset
        CalculatePivotCenter(0f);

        //Instatiate a rotation area, parent it to the node and update its data
        rotationArea = ((GameObject)Instantiate(road.rotationArea, transform.position, transform.rotation)).GetComponent<RotationArea>();
        rotationArea.transform.parent = transform;
        
        //Create a new node to be the slave of this one
        slaveNode = road.CreatePoint(true, road.points.IndexOf(this));

        UpdateRotationArea();

        //If there were entities with this point, pass them to the slave instead
        if (entitiesOnPoint.Count > 0) {

            RoadPoint slavePoint = slaveNode.GetComponent<RoadPoint>();
            
            foreach (GameObject entity in entitiesOnPoint) {
                
                entity.GetComponent<RoadEntity>().associatedPoint = slavePoint;

            }

            slavePoint.entitiesOnPoint.AddRange(entitiesOnPoint);
            slavePoint.localEntitiesPositions.AddRange(localEntitiesPositions);

            entitiesOnPoint.Clear();
            localEntitiesPositions.Clear();

        }
        //If there's was a module on this point, pass it to the slave
        if(moduleOnPoint != null) {

            RoadPoint slavePoint = slaveNode.GetComponent<RoadPoint>();

            slavePoint.moduleOnPoint = moduleOnPoint;
            moduleOnPoint = null;

        }

    }

    //Update the data of the associated rotation area
    public void UpdateRotationArea() {

        rotationArea.roadPoint = this;

        rotationArea.pivot = turnPivotCenter;
        rotationArea.axis = turnPivotAxis;
        rotationArea.turnDirection = turnPivotDirection;
        rotationArea.rotationAngle = turnAngle;

        rotationArea.biRoadLeft = isBiRoadLeft;
        rotationArea.biRoadRight = isBiRoadRight;
        //rotationArea.rotationTime = rotationTime;

        rotationArea.finalRotation = slaveNode.transform.rotation;

    }

    //Calculate the pivot position given an offset
    public void CalculatePivotCenter(float offset) {

        //The pivot will have different positions depending on the actual axis, the turning direction and the offset
        switch (turnPivotAxis) {

            case Axis.x:

                if (turnPivotDirection == TurnDirection.Positive) turnPivotCenter = transform.position - (transform.up * offset);
                else if (turnPivotDirection == TurnDirection.Negative) turnPivotCenter = transform.position + (transform.up * offset);

                break;

            case Axis.y:

                if (turnPivotDirection == TurnDirection.Positive) turnPivotCenter = rails[rails.Count - 1].position + (transform.right * offset);
                else if (turnPivotDirection == TurnDirection.Negative) turnPivotCenter = rails[0].position - (transform.right * offset);

                break;

            case Axis.z:

                turnPivotCenter = transform.position + (transform.forward * offset);
                slaveNode.transform.position = turnPivotCenter;

                break;

        }

    }

    //Turn the slave node given a delta angle
    public void TurnSlave(float deltaAngle) {

        //The slave will turn differently depending on the pivot axis and turning direction
        switch (turnPivotAxis) {

            case Axis.x:

                if (turnPivotDirection == TurnDirection.Positive) slaveNode.transform.RotateAround(turnPivotCenter, transform.right, deltaAngle);
                else if (turnPivotDirection == TurnDirection.Negative) slaveNode.transform.RotateAround(turnPivotCenter, transform.right, -deltaAngle);

                break;

            case Axis.y:

                if (turnPivotDirection == TurnDirection.Positive) slaveNode.transform.RotateAround(turnPivotCenter, transform.up, deltaAngle);
                else if (turnPivotDirection == TurnDirection.Negative) slaveNode.transform.RotateAround(turnPivotCenter, transform.up, -deltaAngle);

                break;

            case Axis.z:

                if (turnPivotDirection == TurnDirection.Positive) slaveNode.transform.RotateAround(turnPivotCenter, transform.forward, deltaAngle);
                else if (turnPivotDirection == TurnDirection.Negative) slaveNode.transform.RotateAround(turnPivotCenter, transform.forward, -deltaAngle);

                break;

        }
        
    }

    //Reset the node data
    public void Reset() {

        pivotOffset = 0f;
        turnPivotCenter = Vector3.zero;
        turnPivotAxis = Axis.y;
        turnPivotDirection = TurnDirection.Positive;
        turnAngle = 0;

        isLimitPoint = false;

        DestroyImmediate(rotationArea.gameObject);

    }

    //Reset the slave node, returning it to the same position and rotation than the master
    public void ResetSlave() {

        slaveNode.transform.position = transform.position;
        slaveNode.transform.rotation = transform.rotation;

    }

#if UNITY_EDITOR

    //Create and entity from an original object using the prefab link, setting its variables to the corresponding values
    public void CreateEntity(GameObject original, string category = null) {

        if (original != null) {

            GameObject entity = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(original);

            entity.name = original.name;

            entity.transform.position = transform.position;
            entity.transform.rotation = transform.rotation;

            if (category == null) {

                entity.transform.parent = road.entitiesContainer.transform;

            }else {

                entity.transform.parent = road.GetCategoryContainer(category).transform;

            }
            
            entity.GetComponent<RoadEntity>().associatedPoint = this;

            entitiesOnPoint.Add(entity);
            localEntitiesPositions.Add(transform.InverseTransformPoint(entity.transform.position));

            UnityEditor.Selection.activeGameObject = entity;

            Debug.Log("Entity " + entity.name + " created");

        }

    }

    //Duplicate the selected entity, creating a new one and setting it up
    public void DuplicateEntity(GameObject entity) {

        if (entitiesOnPoint.Contains(entity)) {

            GameObject duplicatedEntity = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(UnityEditor.PrefabUtility.GetPrefabParent(entity));

            duplicatedEntity.transform.position = entity.transform.position;
            duplicatedEntity.transform.rotation = entity.transform.rotation;

            duplicatedEntity.transform.parent = entity.transform.parent;

            duplicatedEntity.GetComponent<RoadEntity>().associatedPoint = this;

            entitiesOnPoint.Add(duplicatedEntity);
            localEntitiesPositions.Add(transform.InverseTransformPoint(duplicatedEntity.transform.position));

            UnityEditor.Selection.activeGameObject = duplicatedEntity;

            Debug.Log("Duplicated entity");

        }

    }

    public void DeleteEntity(GameObject entity) {

        if (entitiesOnPoint.Contains(entity)) {

            localEntitiesPositions.RemoveAt(entitiesOnPoint.IndexOf(entity));
            entitiesOnPoint.Remove(entity);
            
        }

        Debug.Log("Entity " + entity.name + " deleted.");

        DestroyImmediate(entity);

    }

#endif

    public void UpdateEntityLocalPosition(GameObject entity) {

        if (entitiesOnPoint.Contains(entity)) {

            int entityIndex = entitiesOnPoint.IndexOf(entity);

            localEntitiesPositions[entityIndex] = transform.InverseTransformPoint(entity.transform.position);

        }

    }

    public void CreateLoopRequisiteChecker() {

        GameObject loopRequisiteChecker = Instantiate(road.loopRequisitesController, transform.position, transform.rotation) as GameObject;

        loopRequisiteChecker.name = road.loopRequisitesController.name;
        loopRequisiteChecker.GetComponent<LoopRequisitesController>().rotationArea = rotationArea.gameObject;

    }

    //Drawing the gizmos for each rail and the pivot
    void OnDrawGizmos() {

#if UNITY_EDITOR

        if (isTurnNode)
            Gizmos.color = Color.blue;
        else
            Gizmos.color = Color.red;

        foreach (Transform trans in rails) {

            Gizmos.DrawSphere(trans.position, 0.5f);

        }

        if (masterTurn) {

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(turnPivotCenter, 0.5f);

        }

#endif

    }

}
