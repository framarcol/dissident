﻿using UnityEngine;

public enum Axis { x, y, z }
public enum TurnDirection { Positive, Negative }

public class RotationArea : MonoBehaviour {

    public RoadPoint roadPoint;

    public Vector3 pivot;
    public float rotationAngle;
    public Axis axis;
    public TurnDirection turnDirection;

    public bool biRoadLeft;
    public bool biRoadRight;

    public bool entered = false;

    [HideInInspector]
    public Vector3 actualAxis;

    public Quaternion finalRotation;

    PlayerMovementController playerMovementController;
    CameraMovementController cameraMovementController;
    
	void Start () {

        UpdateActualAxis();

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        cameraMovementController = GameObject.FindGameObjectWithTag("CameraArm").GetComponent<CameraMovementController>();

	}

    void Update() {

#if UNITY_EDITOR

        UpdateActualAxis();

#endif

    }

    public void UpdateActualAxis() {

        //Calculate the actual vector axis based on the axis and turning direction
        switch (axis) {

            case Axis.x:

                actualAxis = (turnDirection == TurnDirection.Positive) ? transform.right : -transform.right;
                break;

            case Axis.y:

                actualAxis = (turnDirection == TurnDirection.Positive) ? transform.up : -transform.up;
                break;

            case Axis.z:

                actualAxis = (turnDirection == TurnDirection.Positive) ? transform.forward : -transform.forward;
                break;

        }

    }

    //If the player or the camera become too close to this area then enable the rotation for them
    void OnTriggerEnter(Collider other) {
        
        if(other.tag == "Player" && !entered) {

            entered = true;

            //PlayerMovementController playerMovementController = other.GetComponentInParent<PlayerMovementController>();
            playerMovementController.rotationArea = this;
            playerMovementController.rotatingPlayer = true;
            playerMovementController.radius = (playerMovementController.transform.position - pivot).magnitude;
            playerMovementController.angleLeft = rotationAngle;

            playerMovementController.canMove = false;
            //playerMovementController.canMoveToSides = false;
            playerMovementController.canJump = false;

            if(axis == Axis.y) {

                /*playerMovementController.turnRight = false;
                playerMovementController.turnLeft = false;

                if (turnDirection == TurnDirection.Positive) playerMovementController.turnRight = true;
                else if (turnDirection == TurnDirection.Negative) playerMovementController.turnLeft = true;*/

                if (turnDirection == TurnDirection.Positive) playerMovementController.turnMagnitude = rotationAngle / 180f;
                else if (turnDirection == TurnDirection.Negative) playerMovementController.turnMagnitude = -(rotationAngle / 180f);
                playerMovementController.turn = true;
                
            }

            playerMovementController.SlowInTurn();

            /*cameraMovementController.targetCameraFOV = 80f;
            cameraMovementController.interpolationMultiplier = 1f;*/

            //When entering a turn enable the next part
            if(roadPoint.isLimitPoint) roadPoint.road.EnableNextPart();
            
        } else if(other.tag == "CameraArm") {

            //CameraMovementController cameraMovementController = other.GetComponent<CameraMovementController>();
            cameraMovementController.rotationArea = this;
            cameraMovementController.rotatingCamera = true;
            cameraMovementController.radius = (cameraMovementController.transform.position - pivot).magnitude;
            cameraMovementController.angleLeft = rotationAngle;

            cameraMovementController.canMove = false;

            cameraMovementController.RollCamera();
            
        }

    }

    void OnTriggerExit(Collider other) {

        if (other.tag == "Player" && entered) entered = false;

    }

}
