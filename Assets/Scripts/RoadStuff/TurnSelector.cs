﻿using UnityEngine;

public class TurnSelector : MonoBehaviour {
    
    public RotationArea rotationAreaLeft;
    public RotationArea rotationAreaRight;

    Transform player;
    PlayerMovementController playerMovementController;

    bool entered = false;
    
    void Start() {

        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerMovementController = player.GetComponentInParent <PlayerMovementController>();

    }

	void Update () {

        if (entered && playerMovementController.rotatingPlayer) {
            entered = false;
            enabled = false;
        }

        if (entered) {

            if(player.localPosition.x > 0f) {
                
                rotationAreaLeft.gameObject.SetActive(false);
                rotationAreaRight.gameObject.SetActive(true);

            }else if(player.localPosition.x <= 0f) {
                
                rotationAreaLeft.gameObject.SetActive(true);
                rotationAreaRight.gameObject.SetActive(false);

            }

        }

	}

    void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            entered = true;

            //player = other.transform;

        } else {

            RotationArea rotationArea = other.GetComponent<RotationArea>();

            if (rotationArea != null) {

                if (rotationArea.biRoadLeft) rotationAreaLeft = rotationArea;
                if (rotationArea.biRoadRight) rotationAreaRight = rotationArea;
                
            }

        }

    }

    void OnTriggerExit(Collider other) {

        if (other.tag == "Player") {

            entered = false;

        }

    }

}
