﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DarkFadeEffect : Effect {
    
    CanvasRenderer canvasRenderer;
    Image darkImage;

	// Use this for initialization
	void OnEnable () {

        canvasRenderer = GetComponent<CanvasRenderer>();
        darkImage = GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () {

        if (canvasRenderer.GetAlpha() <= 0f) {

            gameObject.SetActive(false);

        }

    }

    public override void EnableEffect(bool state) {

        gameObject.SetActive(true);

        if (state) {

            canvasRenderer.SetAlpha(1f);

            darkImage.CrossFadeAlpha(0f, 1f, false);

        } else {

            canvasRenderer.SetAlpha(0f);

            darkImage.CrossFadeAlpha(1f, 1f, false);

        }
        
    }

}
