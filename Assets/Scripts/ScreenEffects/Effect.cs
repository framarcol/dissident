﻿using UnityEngine;
using System.Collections;

public class Effect : MonoBehaviour {

    public virtual void EnableEffect() { }

    public virtual void EnableEffect(bool state) { }

}
