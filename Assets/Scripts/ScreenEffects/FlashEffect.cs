﻿using UnityEngine;
using UnityEngine.UI;

public class FlashEffect : Effect {
    
    void Update() {

        if(GetComponent<CanvasRenderer>().GetAlpha() <= 0f) {

            gameObject.SetActive(false);

        }

    }

    public override void EnableEffect() {

        gameObject.SetActive(true);

        GetComponent<CanvasRenderer>().SetAlpha(1f);

        GetComponent<Image>().CrossFadeAlpha(0f, 1f, false);

    }

}
