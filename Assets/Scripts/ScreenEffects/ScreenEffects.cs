﻿using UnityEngine;
using System.Collections;

public class ScreenEffects : MonoBehaviour {

    [System.Serializable]
    public struct EffectData {

        public string name;
        public Effect effect;

    }

    public EffectData[] effects;
    
    public void EnableEffect(string name) {

        Effect effect = GetEffect(name);

        effect.EnableEffect();

    }

    public void EnableEffect(string name, bool state) {

        Effect effect = GetEffect(name);

        effect.EnableEffect(state);

    }

    public Effect GetEffect(string name) {

        for(int i = 0; i < effects.Length; i++) {

            if(effects[i].name == name) {

                return effects[i].effect;

            }

        }

        return null;

    }

}
