﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BuildShopBundle {

    //[MenuItem("Assets/Build Shop Bundle")]
    public static void CreateShopBundle() {

        AssetBundleBuild[] assetBundleBuildMap = new AssetBundleBuild[1];
        assetBundleBuildMap[0].assetBundleName = "shop/shop_data.sp";
        assetBundleBuildMap[0].assetNames[0] = "Assets/Shop/ShopData.asset";

        if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android) {
            
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Shop/Android", assetBundleBuildMap, BuildAssetBundleOptions.None, BuildTarget.Android);
            Debug.Log("Built Shop Bundle Android");

        } else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS) {

            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Shop/iOS", assetBundleBuildMap, BuildAssetBundleOptions.None, BuildTarget.iOS);

            Debug.Log("Built Shop Bundle iOS");

        }

    }

}
