﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateShopObject {

    [MenuItem("Assets/Create/Shop Data")]
    public static void CreateShopData() {

        ShopData shopData = ScriptableObject.CreateInstance<ShopData>();

        AssetDatabase.CreateAsset(shopData, "Assets/Shop/ShopData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = shopData;

    }

}
