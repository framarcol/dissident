﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ShopData))]
public class ShopDataEditor : Editor {

    ShopData shopData;

    GUIStyle headerStyle;
    GUIStyle boxHeaderStyle;

    public override void OnInspectorGUI() {

        shopData = (ShopData)target;

        CreateStyles();

        EditorGUILayout.LabelField("DISSIDENT's SHOP", headerStyle, GUILayout.Height(35f));
        EditorGUILayout.LabelField("Open 24 hrs");
        EditorGUILayout.Space();

        PopulateStandardItemCategories();

        DrawStandardItemList();

        DrawSetItemList();

        DrawUpgradeItemList();

        DrawVirtualMoneyItemList();

        EditorUtility.SetDirty(shopData);
        Undo.RecordObject(shopData, "Shop modified");

    }

    private void PopulateStandardItemCategories() {

        if (shopData.standardShopItems.Count != 0) return;

        Array values = Enum.GetValues(typeof(ShopData.ShopStandardCategory));
        int count = values.Length;

        for (int i = 0; i < count; i++) {

            shopData.standardShopItems.Add(

                new ShopData.StandardItemsCategorized((ShopData.ShopStandardCategory)values.GetValue(i))
                
            );
            shopData.foldoutList.Add(true);

        }
        
    }

    private void DrawStandardItemList() {

        if(GUILayout.Button("Standard Items", boxHeaderStyle)) {

            shopData.displayStandardItems = !shopData.displayStandardItems;

        }

        if (!shopData.displayStandardItems) return;

        EditorGUI.indentLevel++;

        foreach (ShopData.StandardItemsCategorized itemsCategorized in shopData.standardShopItems) {
            //foreach (ShopData.ShopStandardCategory category in shopData.standardShopItems.Keys) {

            ShopData.ShopStandardCategory category = itemsCategorized.category;

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            shopData.foldoutList[(int)category] = EditorGUILayout.Foldout(shopData.foldoutList[(int)category], shopData.readableCategoryNames[(int)category]);
            if (GUILayout.Button("Add", GUILayout.MaxWidth(100f))) {

                itemsCategorized.standardItemList.Add(new ShopData.StandardShopItem());

                //shopData.standardShopItems[category].Add(new ShopData.StandardShopItem());
                shopData.selectedCurrency.Add(0);

            }
            EditorGUILayout.EndHorizontal();

            if (shopData.foldoutList[(int)category]) {

                //EditorGUI.indentLevel++;

                for (int i = 0; i < itemsCategorized.standardItemList.Count; i++) {

                    ShopData.StandardShopItem standardItem = itemsCategorized.standardItemList[i];
                    DrawStandardItem(itemsCategorized, standardItem);

                }

                //EditorGUI.indentLevel--;

            }

        }

        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

        if (GUILayout.Button("Clear standard items")) {
            shopData.standardShopItems.Clear();
            shopData.foldoutList.Clear();
            for (int i = 0; i < shopData.standardShopItems.Count; i++) shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
        }

        //EditorGUILayout.Space();

    }

    private void DrawStandardItem(ShopData.StandardItemsCategorized itemsCategorized, ShopData.StandardShopItem standardItem) {

        int index = itemsCategorized.standardItemList.IndexOf(standardItem);// shopData.standardShopItems[category].IndexOf(standardItem);
        //int count = 0;

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();
                    
                    standardItem.hidden = GUILayout.Toggle(standardItem.hidden, "", GUILayout.Width(7f));

                    if (standardItem.hidden) GUI.enabled = false;

                    standardItem.itemThumbnail = (Sprite)EditorGUILayout.ObjectField(standardItem.itemThumbnail, typeof(Sprite), false, GUILayout.Width(90f), GUILayout.Height(75f));

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.LabelField("ID");
                        standardItem.itemId = EditorGUILayout.IntField(standardItem.itemId, GUILayout.MaxWidth(80f));

                        EditorGUILayout.LabelField("Name");
                        standardItem.itemName = EditorGUILayout.TextField(standardItem.itemName, GUILayout.MaxWidth(150f));

                        EditorGUILayout.EndVertical();
                    }

                    GUILayout.FlexibleSpace();

                    if (standardItem.hidden)

                        EditorGUILayout.LabelField("Hidden", GUILayout.Width(60f));

                    else if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        itemsCategorized.standardItemList.Remove(standardItem);
                        shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count-1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                standardItem.itemDescription = EditorGUILayout.TextArea(standardItem.itemDescription, GUILayout.Height(50f));

                standardItem.itemPresentation = (GameObject)EditorGUILayout.ObjectField("Item Object", standardItem.itemPresentation, typeof(GameObject), false);
                
                standardItem.itemOverrideMaterial = (Material)EditorGUILayout.ObjectField("Material override", standardItem.itemOverrideMaterial, typeof(Material), false);

                standardItem.hasLowpolyVersion = EditorGUILayout.ToggleLeft("Has Lowpoly version", standardItem.hasLowpolyVersion);
                if (standardItem.hasLowpolyVersion) {

                    EditorGUI.indentLevel++;

                    standardItem.lowpolyItemPresentation = (GameObject)EditorGUILayout.ObjectField("Lowpoly Object", standardItem.lowpolyItemPresentation, typeof(GameObject), false);

                    standardItem.lowpolyItemOverrideMaterial = (Material)EditorGUILayout.ObjectField("Lowpoly Material", standardItem.lowpolyItemOverrideMaterial, typeof(Material), false);

                    EditorGUI.indentLevel--;

                }

                standardItem.hasUnlockEvent = EditorGUILayout.ToggleLeft("Can be unlocked", standardItem.hasUnlockEvent);
                if (standardItem.hasUnlockEvent) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUI.indentLevel++;

                        standardItem.unlockItemEvent.specialLevel = EditorGUILayout.ToggleLeft("Special", standardItem.unlockItemEvent.specialLevel, GUILayout.Width(100f));
                        standardItem.unlockItemEvent.levelId = EditorGUILayout.IntField("Level Id", standardItem.unlockItemEvent.levelId);

                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndHorizontal();
                    }

                }

                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Item Cost", GUILayout.Width(100f));

                    GUILayout.FlexibleSpace();

                    shopData.selectedCurrency[index] = EditorGUILayout.Popup(shopData.selectedCurrency[index], shopData.readableCurrencyNames.ToArray(), GUILayout.MaxWidth(150f));
                    if (GUILayout.Button("+")) {

                        if(!shopData.ContainsCurrency(standardItem.costPacks, (ShopData.ShopCurrency)shopData.selectedCurrency[index])) {

                            standardItem.costPacks.Add(new ShopData.CostPack((ShopData.ShopCurrency)shopData.selectedCurrency[index]));

                        }

                        /*if (!standardItem.itemCost.ContainsKey((ShopData.ShopCurrency)shopData.selectedCurrency[index])) {
                            standardItem.itemCost.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], 0);
                            //standardItem.discountEnable.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], false);
                        }*/

                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;

                for(int i = 0; i < standardItem.costPacks.Count; i++){

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (standardItem.onSale) {

                                    standardItem.costPacks[i].gotDiscount = EditorGUILayout.Toggle(standardItem.costPacks[i].gotDiscount, GUILayout.Width(60f));
                                    //standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                standardItem.costPacks[i].cost = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)standardItem.costPacks[i].currency], standardItem.costPacks[i].cost);
                                //standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (standardItem.costPacks[i].cost < 0) standardItem.costPacks[i].cost = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    standardItem.costPacks.Remove(standardItem.costPacks[i]);
                                    //standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                /*List<ShopData.ShopCurrency> keys = new List<ShopData.ShopCurrency>(standardItem.itemCost.Keys);
                foreach (ShopData.ShopCurrency currency in keys) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (standardItem.onSale) {

                                    //standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (standardItem.itemCost[currency] < 0) standardItem.itemCost[currency] = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }*/

                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    if (standardItem.costPacks.Count > 0) standardItem.onSale = GUILayout.Toggle(standardItem.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));
                    else standardItem.onSale = false;

                    if (standardItem.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        standardItem.salePercentage = EditorGUILayout.Slider("%", standardItem.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

        if (standardItem.hidden) GUI.enabled = true;

    }

    private void DrawSetItemList() {

        GUILayout.Space(20f);

        EditorGUILayout.BeginHorizontal();

        if(GUILayout.Button("Item Sets", boxHeaderStyle)) {

            shopData.displaySets = !shopData.displaySets;

        }

        GUILayout.FlexibleSpace();

        if(GUILayout.Button("Add", GUILayout.MaxWidth(100f))) {

            shopData.shopItemSets.Add(new ShopData.ShopItemSet());
            shopData.selectedCurrency.Add(0);

        }

        EditorGUILayout.EndHorizontal();

        if (!shopData.displaySets) return;

        EditorGUI.indentLevel++;

        for(int i = 0; i < shopData.shopItemSets.Count; i++) {

            DrawItemSet(shopData.shopItemSets[i]);

        }

        EditorGUILayout.Space();

        if (shopData.shopItemSets.Count > 0 && GUILayout.Button("Clear item sets")) {
            shopData.shopItemSets.Clear();
            for (int i = 0; i < shopData.shopItemSets.Count; i++) shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count-1);
        }

        EditorGUI.indentLevel--;

    }

    private void DrawItemSet(ShopData.ShopItemSet itemSet) {

        int index = shopData.shopItemSets.IndexOf(itemSet);

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();

                    //standardItem.hidden = EditorGUILayout.Toggle("", standardItem.hidden, GUILayout.Width(15f));
                    itemSet.hidden = GUILayout.Toggle(itemSet.hidden, "", GUILayout.Width(7f));

                    if (itemSet.hidden) GUI.enabled = false;

                    itemSet.itemThumbnail = (Sprite)EditorGUILayout.ObjectField(itemSet.itemThumbnail, typeof(Sprite), false, GUILayout.Width(90f), GUILayout.Height(75f));

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.LabelField("ID");
                        itemSet.itemId = EditorGUILayout.IntField(itemSet.itemId, GUILayout.MaxWidth(80f));

                        EditorGUILayout.LabelField("Name");
                        itemSet.itemName = EditorGUILayout.TextField(itemSet.itemName, GUILayout.MaxWidth(150f));

                        EditorGUILayout.EndVertical();
                    }

                    GUILayout.FlexibleSpace();

                    if (itemSet.hidden)

                        EditorGUILayout.LabelField("Hidden", GUILayout.Width(60f));

                    else if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        shopData.shopItemSets.Remove(itemSet);
                        shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                itemSet.itemDescription = EditorGUILayout.TextArea(itemSet.itemDescription, GUILayout.Height(50f));

                itemSet.itemPresentation = (GameObject)EditorGUILayout.ObjectField("Item Object", itemSet.itemPresentation, typeof(GameObject), false);

                //if (itemSet.itemPresentation != null)
                    itemSet.itemOverrideMaterial = (Material)EditorGUILayout.ObjectField("Material override", itemSet.itemOverrideMaterial, typeof(Material), false);

                /*itemSet.hasUnlockEvent = EditorGUILayout.ToggleLeft("Can be unlocked", itemSet.hasUnlockEvent);

                if (itemSet.hasUnlockEvent) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUI.indentLevel++;

                        itemSet.unlockItemEvent.specialLevel = EditorGUILayout.ToggleLeft("Special", itemSet.unlockItemEvent.specialLevel, GUILayout.Width(100f));
                        itemSet.unlockItemEvent.levelId = EditorGUILayout.IntField("Level Id", itemSet.unlockItemEvent.levelId);

                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndHorizontal();
                    }

                }*/

                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Linked item Ids");

                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("+")) {

                        itemSet.linkedItemIds.Add(-1);

                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;
                for (int i = 0; i < itemSet.linkedItemIds.Count; i++) {

                    {
                        EditorGUILayout.BeginHorizontal();

                        itemSet.linkedItemIds[i] = EditorGUILayout.IntField("Item ID " + (i + 1), itemSet.linkedItemIds[i]);
                        if (GUILayout.Button("x")) {
                            itemSet.linkedItemIds.RemoveAt(i);
                        }
                        GUILayout.FlexibleSpace();

                        EditorGUILayout.EndHorizontal();
                    }
                    
                }
                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Item Cost", GUILayout.Width(100f));

                    GUILayout.FlexibleSpace();

                    shopData.selectedCurrency[index] = EditorGUILayout.Popup(shopData.selectedCurrency[index], shopData.readableCurrencyNames.ToArray(), GUILayout.MaxWidth(150f));
                    if (GUILayout.Button("+")) {

                        if (!shopData.ContainsCurrency(itemSet.costPacks, (ShopData.ShopCurrency)shopData.selectedCurrency[index])) {

                            itemSet.costPacks.Add(new ShopData.CostPack((ShopData.ShopCurrency)shopData.selectedCurrency[index]));

                        }

                        /*if (!itemSet.itemCost.ContainsKey((ShopData.ShopCurrency)shopData.selectedCurrency[index])) {
                            itemSet.itemCost.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], 0);
                            itemSet.discountEnable.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], false);
                        }*/

                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;

                for (int i = 0; i < itemSet.costPacks.Count; i++) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (itemSet.onSale) {

                                    itemSet.costPacks[i].gotDiscount = EditorGUILayout.Toggle(itemSet.costPacks[i].gotDiscount, GUILayout.Width(60f));
                                    //standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                itemSet.costPacks[i].cost = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)itemSet.costPacks[i].currency], itemSet.costPacks[i].cost);
                                //standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (itemSet.costPacks[i].cost < 0) itemSet.costPacks[i].cost = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    itemSet.costPacks.Remove(itemSet.costPacks[i]);
                                    //standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                /*List<ShopData.ShopCurrency> keys = new List<ShopData.ShopCurrency>(itemSet.itemCost.Keys);
                foreach (ShopData.ShopCurrency currency in keys) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();

                                if (itemSet.onSale) {

                                    itemSet.discountEnable[currency] = EditorGUILayout.Toggle(itemSet.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                itemSet.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], itemSet.itemCost[currency]);
                                if (itemSet.itemCost[currency] < 0) itemSet.itemCost[currency] = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    itemSet.itemCost.Remove(currency);
                                    itemSet.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }
                */

                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    if (itemSet.costPacks.Count > 0) itemSet.onSale = GUILayout.Toggle(itemSet.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));
                    else itemSet.onSale = false;

                    if (itemSet.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        itemSet.salePercentage = EditorGUILayout.Slider("%", itemSet.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();

        }

        if (itemSet.hidden) GUI.enabled = true;

    }

    private void DrawUpgradeItemList() {

        GUILayout.Space(20f);

        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Upgrade Items", boxHeaderStyle)) {

                shopData.displayUpgrades = !shopData.displayUpgrades;

            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Add", GUILayout.MaxWidth(100f))) {

                shopData.upgradeItems.Add(new ShopData.UpgradeItem());
                shopData.selectedCurrency.Add(0);

            }

            EditorGUILayout.EndHorizontal();
        }

        if (!shopData.displayUpgrades) return;

        EditorGUI.indentLevel++;

        for (int i = 0; i < shopData.upgradeItems.Count; i++) {
            
            DrawUpgradeItem(shopData.upgradeItems[i]);

        }

        EditorGUILayout.Space();

        if (shopData.upgradeItems.Count > 0 && GUILayout.Button("Clear upgrade")) {
            shopData.upgradeItems.Clear();
            for (int i = 0; i < shopData.upgradeItems.Count; i++) shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
        }
        
        EditorGUI.indentLevel--;

    }

    private void DrawUpgradeItem(ShopData.UpgradeItem upgradeItem) {

        int index = shopData.upgradeItems.IndexOf(upgradeItem);

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();

                    //standardItem.hidden = EditorGUILayout.Toggle("", standardItem.hidden, GUILayout.Width(15f));
                    upgradeItem.hidden = GUILayout.Toggle(upgradeItem.hidden, "", GUILayout.Width(7f));

                    if (upgradeItem.hidden) GUI.enabled = false;

                    upgradeItem.itemThumbnail = (Sprite)EditorGUILayout.ObjectField(upgradeItem.itemThumbnail, typeof(Sprite), false, GUILayout.Width(90f), GUILayout.Height(75f));

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.LabelField("ID");
                        upgradeItem.itemId = EditorGUILayout.IntField(upgradeItem.itemId, GUILayout.MaxWidth(80f));

                        EditorGUILayout.LabelField("Name");
                        upgradeItem.itemName = EditorGUILayout.TextField(upgradeItem.itemName, GUILayout.MaxWidth(150f));

                        EditorGUILayout.EndVertical();
                    }

                    GUILayout.FlexibleSpace();

                    if (upgradeItem.hidden)

                        EditorGUILayout.LabelField("Hidden", GUILayout.Width(60f));

                    else if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        shopData.upgradeItems.Remove(upgradeItem);
                        shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                upgradeItem.itemDescription = EditorGUILayout.TextArea(upgradeItem.itemDescription, GUILayout.Height(50f));

                upgradeItem.itemPresentation = (GameObject)EditorGUILayout.ObjectField("Item Object", upgradeItem.itemPresentation, typeof(GameObject), false);

                //if (upgradeItem.itemPresentation != null)
                    upgradeItem.itemOverrideMaterial = (Material)EditorGUILayout.ObjectField("Material override", upgradeItem.itemOverrideMaterial, typeof(Material), false);

                /*upgradeItem.hasUnlockEvent = EditorGUILayout.ToggleLeft("Can be unlocked", upgradeItem.hasUnlockEvent);

                if (upgradeItem.hasUnlockEvent) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUI.indentLevel++;

                        standardItem.unlockItemEvent.specialLevel = EditorGUILayout.ToggleLeft("Special", standardItem.unlockItemEvent.specialLevel, GUILayout.Width(100f));
                        standardItem.unlockItemEvent.levelId = EditorGUILayout.IntField("Level Id", standardItem.unlockItemEvent.levelId);

                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndHorizontal();
                    }

                }*/
                
                upgradeItem.updgradeType = (ShopData.UpgradeItem.UpgradeType)EditorGUILayout.EnumPopup("Upgrade type", upgradeItem.updgradeType);
                EditorGUI.indentLevel++;
                switch (upgradeItem.updgradeType) {

                    case ShopData.UpgradeItem.UpgradeType.LIFE:
                        upgradeItem.extraLife = EditorGUILayout.IntField("Extra life", upgradeItem.extraLife);
                        break;

                    case ShopData.UpgradeItem.UpgradeType.MAX_AMMO:
                        upgradeItem.maxAmmo = EditorGUILayout.IntField("Max ammo", upgradeItem.maxAmmo);
                        break;

                    case ShopData.UpgradeItem.UpgradeType.WORLD:
                        upgradeItem.worldIdToUnlock = EditorGUILayout.IntField("World to Unlock", upgradeItem.worldIdToUnlock);
                        upgradeItem.worldBackground = (Sprite)EditorGUILayout.ObjectField("World background", upgradeItem.worldBackground, typeof(Sprite), false);
                        break;

                }
                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Item Cost", GUILayout.Width(100f));

                    GUILayout.FlexibleSpace();

                    shopData.selectedCurrency[index] = EditorGUILayout.Popup(shopData.selectedCurrency[index], shopData.readableCurrencyNames.ToArray(), GUILayout.MaxWidth(150f));
                    if (GUILayout.Button("+")) {

                        if (!shopData.ContainsCurrency(upgradeItem.costPacks, (ShopData.ShopCurrency)shopData.selectedCurrency[index])) {

                            upgradeItem.costPacks.Add(new ShopData.CostPack((ShopData.ShopCurrency)shopData.selectedCurrency[index]));

                        }

                        /*if (!upgradeItem.itemCost.ContainsKey((ShopData.ShopCurrency)shopData.selectedCurrency[index])) {
                            upgradeItem.itemCost.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], 0);
                            upgradeItem.discountEnable.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], false);
                        }*/

                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;

                for (int i = 0; i < upgradeItem.costPacks.Count; i++) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (upgradeItem.onSale) {

                                    upgradeItem.costPacks[i].gotDiscount = EditorGUILayout.Toggle(upgradeItem.costPacks[i].gotDiscount, GUILayout.Width(60f));
                                    //standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                upgradeItem.costPacks[i].cost = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)upgradeItem.costPacks[i].currency], upgradeItem.costPacks[i].cost);
                                //standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (upgradeItem.costPacks[i].cost < 0) upgradeItem.costPacks[i].cost = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    upgradeItem.costPacks.Remove(upgradeItem.costPacks[i]);
                                    //standardItem.itemCost.Remove(currency);
                                    //standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                /*List<ShopData.ShopCurrency> keys = new List<ShopData.ShopCurrency>(upgradeItem.itemCost.Keys);
                foreach (ShopData.ShopCurrency currency in keys) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();
                                
                                if (upgradeItem.onSale) {

                                    upgradeItem.discountEnable[currency] = EditorGUILayout.Toggle(upgradeItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                upgradeItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], upgradeItem.itemCost[currency]);
                                if (upgradeItem.itemCost[currency] < 0) upgradeItem.itemCost[currency] = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    upgradeItem.itemCost.Remove(currency);
                                    upgradeItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }*/

                EditorGUI.indentLevel--;

                {
                    EditorGUILayout.BeginHorizontal();

                    if (upgradeItem.costPacks.Count > 0) upgradeItem.onSale = GUILayout.Toggle(upgradeItem.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));
                    else upgradeItem.onSale = false;

                    if (upgradeItem.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        upgradeItem.salePercentage = EditorGUILayout.Slider("%", upgradeItem.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

        if (upgradeItem.hidden) GUI.enabled = true;

    }

    private void DrawVirtualMoneyItemList() {

        GUILayout.Space(20f);

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Virtual Money Packs", boxHeaderStyle)) {

            shopData.displayVirtualMoneyPacks = !shopData.displayVirtualMoneyPacks;

        }

        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Add", GUILayout.MaxWidth(100f))) {

            shopData.virtualMoneyPacks.Add(new ShopData.VirtualMoneyItem());
            //shopData.selectedCurrency.Add(0);

        }

        EditorGUILayout.EndHorizontal();

        if (!shopData.displayVirtualMoneyPacks) return;

        EditorGUI.indentLevel++;

        for (int i = 0; i < shopData.virtualMoneyPacks.Count; i++) {
            
            DrawVirtualMoneyPack(shopData.virtualMoneyPacks[i]);

        }

        EditorGUILayout.Space();

        if (shopData.virtualMoneyPacks.Count > 0 && GUILayout.Button("Clear virtual money packs")) {
            shopData.virtualMoneyPacks.Clear();
            //for (int i = 0; i < shopData.shopItemSets.Count; i++) shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
        }

        EditorGUI.indentLevel--;

    }

    private void DrawVirtualMoneyPack(ShopData.VirtualMoneyItem virtualMoneyPack) {

        //int index = shopData.virtualMoneyPacks.IndexOf(virtualMoneyPack);
        //int count = 0;

        EditorGUILayout.Space();

        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(30);

            {
                EditorGUILayout.BeginVertical("Box");

                {
                    EditorGUILayout.BeginHorizontal();

                    //standardItem.hidden = EditorGUILayout.Toggle("", standardItem.hidden, GUILayout.Width(15f));
                    virtualMoneyPack.hidden = GUILayout.Toggle(virtualMoneyPack.hidden, "", GUILayout.Width(7f));

                    if (virtualMoneyPack.hidden) GUI.enabled = false;

                    virtualMoneyPack.itemThumbnail = (Sprite)EditorGUILayout.ObjectField(virtualMoneyPack.itemThumbnail, typeof(Sprite), false, GUILayout.Width(90f), GUILayout.Height(75f));

                    {
                        EditorGUILayout.BeginVertical();

                        EditorGUILayout.LabelField("ID");
                        virtualMoneyPack.itemId = EditorGUILayout.IntField(virtualMoneyPack.itemId, GUILayout.MaxWidth(80f));

                        EditorGUILayout.LabelField("Name");
                        virtualMoneyPack.itemName = EditorGUILayout.TextField(virtualMoneyPack.itemName, GUILayout.MaxWidth(150f));

                        EditorGUILayout.EndVertical();
                    }

                    GUILayout.FlexibleSpace();

                    if (virtualMoneyPack.hidden)

                        EditorGUILayout.LabelField("Hidden", GUILayout.Width(60f));

                    else if (GUILayout.Button("X", GUILayout.Width(50f))) {

                        shopData.virtualMoneyPacks.Remove(virtualMoneyPack);
                        //shopData.selectedCurrency.RemoveAt(shopData.selectedCurrency.Count - 1);
                        return;

                    }

                    EditorGUILayout.EndHorizontal();
                }

                virtualMoneyPack.itemDescription = EditorGUILayout.TextArea(virtualMoneyPack.itemDescription, GUILayout.Height(50f));

                virtualMoneyPack.itemPresentation = (GameObject)EditorGUILayout.ObjectField("Item Object", virtualMoneyPack.itemPresentation, typeof(GameObject), false);

                //if (virtualMoneyPack.itemPresentation != null)
                    virtualMoneyPack.itemOverrideMaterial = (Material)EditorGUILayout.ObjectField("Material override", virtualMoneyPack.itemOverrideMaterial, typeof(Material), false);

                /*standardItem.hasUnlockEvent = EditorGUILayout.ToggleLeft("Can be unlocked", virtualMoneyPack.hasUnlockEvent);

                if (standardItem.hasUnlockEvent) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUI.indentLevel++;

                        standardItem.unlockItemEvent.specialLevel = EditorGUILayout.ToggleLeft("Special", standardItem.unlockItemEvent.specialLevel, GUILayout.Width(100f));
                        standardItem.unlockItemEvent.levelId = EditorGUILayout.IntField("Level Id", standardItem.unlockItemEvent.levelId);

                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndHorizontal();
                    }

                }*/

                /*{
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Item Cost", GUILayout.Width(100f));

                    GUILayout.FlexibleSpace();

                    shopData.selectedCurrency[index] = EditorGUILayout.Popup(shopData.selectedCurrency[index], shopData.readableCurrencyNames.ToArray(), GUILayout.MaxWidth(150f));
                    if (GUILayout.Button("+")) {

                        if (!standardItem.itemCost.ContainsKey((ShopData.ShopCurrency)shopData.selectedCurrency[index])) {
                            standardItem.itemCost.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], 0);
                            standardItem.discountEnable.Add((ShopData.ShopCurrency)shopData.selectedCurrency[index], false);
                        }

                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.indentLevel++;

                List<ShopData.ShopCurrency> keys = new List<ShopData.ShopCurrency>(standardItem.itemCost.Keys);
                foreach (ShopData.ShopCurrency currency in keys) {

                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(30);

                        GUI.backgroundColor = new Color(0.65f, 0.65f, 1, 1);
                        {
                            EditorGUILayout.BeginVertical(GUI.skin.button);
                            GUI.backgroundColor = Color.white;

                            {
                                EditorGUILayout.BeginHorizontal();


                                if (standardItem.onSale) {

                                    standardItem.discountEnable[currency] = EditorGUILayout.Toggle(standardItem.discountEnable[currency], GUILayout.Width(60f));

                                } else {

                                    EditorGUILayout.LabelField("", GUILayout.Width(60f));

                                }

                                standardItem.itemCost[currency] = EditorGUILayout.IntField(shopData.readableCurrencyNames[(int)currency], standardItem.itemCost[currency]);
                                if (standardItem.itemCost[currency] < 0) standardItem.itemCost[currency] = 0;

                                if (GUILayout.Button("x", GUILayout.MaxWidth(40f))) {
                                    standardItem.itemCost.Remove(currency);
                                    standardItem.discountEnable.Remove(currency);
                                }

                                EditorGUILayout.EndHorizontal();
                            }

                            EditorGUILayout.EndVertical();
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                }

                EditorGUI.indentLevel--;
                */

                {
                    EditorGUILayout.BeginHorizontal();
                    virtualMoneyPack.virtualCurrencyReward = EditorGUILayout.IntField("Virtual currency reward", virtualMoneyPack.virtualCurrencyReward);
                    virtualMoneyPack.targetCurrency = (ShopData.ShopCurrency)EditorGUILayout.Popup((int)virtualMoneyPack.targetCurrency, shopData.readableCurrencyNames.ToArray());
                    EditorGUILayout.EndHorizontal();
                }
                
                {
                    EditorGUILayout.BeginHorizontal();
                    virtualMoneyPack.realMoneyCost = (decimal)EditorGUILayout.FloatField("Real Money Cost", (float)virtualMoneyPack.realMoneyCost);
                    if (virtualMoneyPack.realMoneyCost < 0m) virtualMoneyPack.realMoneyCost = 0.00m;
                    EditorGUILayout.LabelField("€", GUILayout.Width(30f));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal();
                }

                {
                    EditorGUILayout.BeginHorizontal();

                    virtualMoneyPack.onSale = GUILayout.Toggle(virtualMoneyPack.onSale, "On Sale", GUI.skin.button, GUILayout.MaxWidth(75f));

                    if (virtualMoneyPack.onSale) {
                        EditorGUIUtility.labelWidth = 50f;
                        virtualMoneyPack.salePercentage = EditorGUILayout.Slider("%", virtualMoneyPack.salePercentage, 0f, 1f, GUILayout.Width(200f));
                        EditorGUIUtility.labelWidth = 0;
                        GUILayout.FlexibleSpace();
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

        if (virtualMoneyPack.hidden) GUI.enabled = true;

    }

    private void CreateStyles() {

        if (headerStyle == null) {
            
            headerStyle = new GUIStyle(GUI.skin.label);

            headerStyle.fontSize = 30;
            headerStyle.fontStyle = FontStyle.Bold;

        }

        if(boxHeaderStyle == null) {

            boxHeaderStyle = new GUIStyle(GUI.skin.box);

            boxHeaderStyle.fontStyle = FontStyle.Bold;
            boxHeaderStyle.normal.textColor = new Color(0.7f,0.7f,0.7f);

            boxHeaderStyle.hover.background = boxHeaderStyle.normal.background;
            boxHeaderStyle.hover.textColor = new Color(0.9f, 0.9f, 0.9f);

        }
        
    }

}
