﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ShopData : ScriptableObject {

    [Serializable]
    public enum ShopCurrency { DIAMONDS = 0, STARS = 1, FRAGMENTS = 2 }
    public List<string> readableCurrencyNames = new List<string> { "Diamonds", "Stars", "Fragments" };

    [Serializable]
	public enum ShopStandardCategory { SKIN_BODY = 0, SKIN_WEAPON = 1, SKIN_SKATE = 2/*, UPGRADES = 3 */}
    public List<string> readableCategoryNames = new List<string>{ "Body skins" , "Weapon skins", "Skate skins"/*, "Upgrades"*/ };

    [Serializable]
    public class UnlockItemEvent{

        public bool specialLevel;

        public int levelId;

        public UnlockItemEvent() {

            specialLevel = false;
            levelId = -1;

        }

    }

    [Serializable]
    public class CostPack {

        public ShopCurrency currency;

        public int cost;

        public bool gotDiscount;

        public CostPack(ShopCurrency currency) {

            this.currency = currency;
            cost = 0;
            gotDiscount = false;

        }

    }

    [Serializable]
    public class ShopItem {

        public bool hidden;

        public int itemId;

        public string itemName;

        public string itemDescription;

        public Sprite itemThumbnail;

        public GameObject itemPresentation;
        public Material itemOverrideMaterial;

        public List<CostPack> costPacks;
        
        public Dictionary<ShopCurrency, int> itemCost;
        public Dictionary<ShopCurrency, bool> discountEnable;
        
        public bool onSale;
        public float salePercentage;

        public ShopItem() {

            hidden = false;
            itemId = 0;
            itemName = "ITEM";
            itemDescription = "Item description";
            itemThumbnail = null;

            itemPresentation = null;
            itemOverrideMaterial = null;

            costPacks = new List<CostPack>();

            itemCost = new Dictionary<ShopCurrency, int>();
            discountEnable = new Dictionary<ShopCurrency, bool>();

            onSale = false;
            salePercentage = 0f;

        }
        
    }
    
    [Serializable]
    public class StandardShopItem : ShopItem {

        public bool hasLowpolyVersion;
        public GameObject lowpolyItemPresentation;
        public Material lowpolyItemOverrideMaterial;

        public bool hasUnlockEvent;
        public UnlockItemEvent unlockItemEvent;

        public StandardShopItem() : base() {

            hasLowpolyVersion = false;
            lowpolyItemPresentation = null;
            lowpolyItemOverrideMaterial = null;

            hasUnlockEvent = false;
            unlockItemEvent = new UnlockItemEvent();

        }

    }

    [Serializable]
    public class StandardItemsCategorized {

        public ShopStandardCategory category;

        public List<StandardShopItem> standardItemList;

        public StandardItemsCategorized(ShopStandardCategory category) {

            this.category = category;
            standardItemList = new List<StandardShopItem>();


        }

    }

    [Serializable]
    public class ShopItemSet : ShopItem {

        public List<int> linkedItemIds;

        public ShopItemSet() : base() {

            linkedItemIds = new List<int>();

        }

    }

    [Serializable]
    public class UpgradeItem : ShopItem {

        public enum UpgradeType { LIFE = 0, MAX_AMMO = 1, WORLD = 2 }
        public UpgradeType updgradeType;

        public int extraLife;

        public int maxAmmo;

        public int worldIdToUnlock;
        public Sprite worldBackground;

        public UpgradeItem() : base() {

            updgradeType = UpgradeType.LIFE;

            extraLife = 0;
            maxAmmo = 0;
            worldIdToUnlock = -1;
            worldBackground = null;

        }

    }

    [Serializable]
    public class VirtualMoneyItem : ShopItem {

        public ShopCurrency targetCurrency;

        public int virtualCurrencyReward;

        public decimal realMoneyCost;

        public VirtualMoneyItem() : base() {

            targetCurrency = ShopCurrency.DIAMONDS;
            virtualCurrencyReward = 0;
            realMoneyCost = 0.00m;

        }

    }

    //Standard items (skins)
    //public Dictionary<ShopStandardCategory, List<StandardShopItem>> standardShopItems = new Dictionary<ShopStandardCategory, List<StandardShopItem>>();
    public List<StandardItemsCategorized> standardShopItems = new List<StandardItemsCategorized>();
    public bool displayStandardItems;

    //Set of skins
    public List<ShopItemSet> shopItemSets = new List<ShopItemSet>();
    public bool displaySets;

    //Upgrades
    public List<UpgradeItem> upgradeItems = new List<UpgradeItem>();
    public bool displayUpgrades;

    //Packs of virtual currency (diamonds)
    public List<VirtualMoneyItem> virtualMoneyPacks = new List<VirtualMoneyItem>();
    public bool displayVirtualMoneyPacks;

    //public ShopStandardCategory categoryOptions;
    public List<bool> foldoutList = new List<bool>();
    public List<int> selectedCurrency = new List<int>();

    public bool ContainsCurrency(List<CostPack> packs, ShopCurrency currency) {

        for(int i = 0; i < packs.Count; i++) {

            if (packs[i].currency == currency) return true;

        }

        return false;

    }

    public StandardShopItem GetBodySkin(int itemId) {

        for(int i = 0; i < standardShopItems.Count; i++) {

            if(standardShopItems[i].category == ShopStandardCategory.SKIN_BODY) {

                for(int j = 0; j < standardShopItems[i].standardItemList.Count; j++) {

                    if(standardShopItems[i].standardItemList[j].itemId == itemId) {

                        return standardShopItems[i].standardItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public StandardShopItem GetSkateSkin(int itemId) {

        for (int i = 0; i < standardShopItems.Count; i++) {

            if (standardShopItems[i].category == ShopStandardCategory.SKIN_SKATE) {

                for (int j = 0; j < standardShopItems[i].standardItemList.Count; j++) {

                    if (standardShopItems[i].standardItemList[j].itemId == itemId) {

                        return standardShopItems[i].standardItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public StandardShopItem GetWeaponSkin(int itemId) {

        for (int i = 0; i < standardShopItems.Count; i++) {

            if (standardShopItems[i].category == ShopStandardCategory.SKIN_WEAPON) {

                for (int j = 0; j < standardShopItems[i].standardItemList.Count; j++) {

                    if (standardShopItems[i].standardItemList[j].itemId == itemId) {

                        return standardShopItems[i].standardItemList[j];

                    }

                }

            }

        }

        return null;

    }

    public ShopItemSet GetSet(int itemId) {

        for(int i = 0; i < shopItemSets.Count; i++) {

            if (shopItemSets[i].itemId == itemId) return shopItemSets[i];

        }

        return null;

    }

    public UpgradeItem GetUpgrade(int itemId) {

        for (int i = 0; i < upgradeItems.Count; i++) {

            if (upgradeItems[i].itemId == itemId) return upgradeItems[i];

        }

        return null;

    }

    public VirtualMoneyItem GetVirtualMoneyItem(int itemId) {

        for (int i = 0; i < virtualMoneyPacks.Count; i++) {

            if (virtualMoneyPacks[i].itemId == itemId) return virtualMoneyPacks[i];

        }

        return null;
        
    }

}
