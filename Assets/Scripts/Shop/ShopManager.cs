﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ShopManager
{

	//public static string url = "file://C:/Users/ivanchez/Proyectos/Dissident/Assets/AssetBundles/Android/shop/shop_data.sp";

	static readonly string shopAssetBundleURL = "https://agapornigames.es/dodongo2/AssetBundles/";
	static readonly string shopAssetBundleURLBETA = "https://agapornigames.es/dodongo2/AssetBundles/";
	static readonly string shopAssetBundleVersion = "https://agapornigames.com/dodongo/checkShop.php";

	public static AssetBundle shopBundle;
	public static int shopBundleVersion;
	public static ShopData shopData = null;

	public static bool test;
	public static bool tiendaDescargada;
    public static string porcentajeDescargado;
	public static bool offline;
	//EQUIPPED SKINs AND UPGRADEs
	public static ShopData.StandardShopItem equippedBodySkin = null;
	public static ShopData.StandardShopItem equippedSkateSkin = null;
	public static ShopData.StandardShopItem equippedWeaponSkin = null;
	public static List<ShopData.UpgradeItem> equippedUpgrades = new List<ShopData.UpgradeItem>();

    public static void Iniciar()
    {
        shopData = Resources.Load("Shopdata") as ShopData;
    }

	public static IEnumerator Initialize()
	{

		offline = false;
		yield return GetShopBundleVersion();
		yield return GetShopBundle();
		if (shopBundle != null) shopData = Resources.Load("Shopdata") as ShopData;

	}

	public static IEnumerator InitializeOffline()
	{
		offline = true;
		yield return GetShopBundle();

		if (shopBundle != null) shopData = Resources.Load("Shopdata") as ShopData;

    }

	public static IEnumerator GetShopBundleVersion()
	{

		if (test)
		{

			shopBundleVersion = 1;
			yield break;

		}

		string enlaceFinal = shopAssetBundleVersion;

		using (WWW www = new WWW(enlaceFinal))
		{

			yield return www;

			int version;
			if (int.TryParse(www.text, out version))
			{

				shopBundleVersion = version;
				PlayerPrefs.SetInt("ShopCached", version);

			}
			else
			{

				shopBundleVersion = 1;

			}

		}

	}



	public static bool checkShopCache()
	{
      //Get the most recent version numbers of the bundles from the server database
		uint actualVersion = (uint)PlayerPrefs.GetInt("ShopCached");
		//Debug.Log("Version tienda: " + actualVersion);
#if UNITY_ANDROID

		if (Caching.IsVersionCached(shopAssetBundleURL + "Android/shop/shop_data.sp", (int)actualVersion)) return true;
		else return false;

#elif UNITY_IOS
		if (Caching.IsVersionCached(shopAssetBundleURL + "iOS/shop/shop_data.sp", (int)actualVersion)) return true;
		else return false;
#endif

	}

    private static IEnumerator GetShopBundle() {

        //if(test) Caching.CleanCache();

        if (shopData != null) yield break;

        while (!Caching.ready) yield return null;

        WWW itemsData;

        WWWForm datos = new WWWForm();
#if UNITY_ANDROID
        if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
        }
        else
        {
            datos.AddField("id", Social.localUser.id.ToString());
        }
           
#endif


        string enlaceFinal;
        itemsData = new WWW("https://agapornigames.com/dodongo/checkBetaTester.php", datos);

        yield return itemsData;

      //  Debug.Log(itemsData.text);

        if (itemsData.text == "0")
            enlaceFinal = shopAssetBundleURL;
        else
            enlaceFinal = shopAssetBundleURLBETA;


#if UNITY_ANDROID
        using (WWW www = WWW.LoadFromCacheOrDownload(enlaceFinal + "Android/shop/shop_data.sp", PlayerPrefs.GetInt("ShopCached")))
            {

#elif UNITY_IOS

        using(WWW www = WWW.LoadFromCacheOrDownload(enlaceFinal + "iOS/shop/shop_data.sp", PlayerPrefs.GetInt("ShopCached"))) {

#endif
            while (!www.isDone)
            {
                porcentajeDescargado = ((int)(www.progress * 100)).ToString();
                yield return null;
            }

        yield return www;

                if (offline)
                {
                    shopBundle = www.assetBundle;
                   // Debug.Log("Shop Bundle retrieved");
                }
                else
                {
                    if (www.error != null)
                    {

                        Debug.LogError(www.error);

                    }
                    else
                    {
                        shopBundle = www.assetBundle;
                      //  Debug.Log("Shop Bundle retrieved");

                    }
                }


            }

    }

}
