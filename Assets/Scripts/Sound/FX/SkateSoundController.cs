﻿using UnityEngine;
using System.Collections;

public class SkateSoundController : MonoBehaviour {

    public AudioClip skateIdle;
    public AudioClip skateFly;
    public AudioClip skateTurn;
    public AudioClip skateJump;
    public AudioClip[] skateHit;
    public AudioClip[] skateDestruction;

    //public ParticleSystem explosion;
    //public ParticleSystem smoke;

    public float globalVolume = 1f;
    public float transitionDuration = 0.5f;

    AudioSource audioSource;

    //PlayerMovementController playerMovementController;
    PlayerCinematicController playerCinematicController;

    [HideInInspector]
    public float sfxVolumeScale;

    public bool skateSoundEnabled = true;
    
	void Start () {

        audioSource = GetComponent<AudioSource>();
        
        //playerMovementController = GetComponentInParent<PlayerMovementController>();
        playerCinematicController = GetComponentInParent<PlayerCinematicController>();

        audioSource.loop = true;

        sfxVolumeScale = PlayerPrefs.GetFloat(GameManager.SoundEffectVolumePref, 1f);
        audioSource.volume = sfxVolumeScale * globalVolume;

        if (playerCinematicController.initLevel) {

            StartCoroutine(LoopFadeTransition(skateIdle, transitionDuration));

        } else {
            
            StartCoroutine(LoopFadeTransition(skateFly, transitionDuration));

        }

    }
	
    public void TriggerFlySkate() {

        if(skateSoundEnabled) StartCoroutine(LoopFadeTransition(skateFly, 0.1f));

    }

    public void TriggerJump() {

        if (skateSoundEnabled) StartCoroutine(JumpTransition());

    }

    public void TriggerTurn(bool on) {

        if (!skateSoundEnabled) return;

        if (on) StartCoroutine(LoopFadeTransition(skateTurn, 0.1f));
        else StartCoroutine(LoopFadeTransition(skateFly, 0.1f));

    }

    public void TriggerSkateIn() {

        if (skateSoundEnabled) return;

        StartCoroutine(LoopFadeTransition(skateFly, 0.5f));

        skateSoundEnabled = true;

    }

    public void TriggerSkateOut() {

        if (!skateSoundEnabled) return;

        StartCoroutine(LoopFadeTransition(null, 0.5f));

        skateSoundEnabled = false;

    }

    public void TriggerHit() {

        if (!skateSoundEnabled) return;

        audioSource.PlayOneShot(skateHit[Random.Range(0, skateHit.Length)]);

    }

    public void TriggerDestruction() {

        if (!skateSoundEnabled) return;

        audioSource.Stop();

        AudioSource.PlayClipAtPoint(skateDestruction[Random.Range(0, skateDestruction.Length)], Camera.main.transform.position);

        /*explosion.Play();
        smoke.Play();*/

    }

    IEnumerator JumpTransition() {

        yield return LoopFadeTransition(skateJump, 0f);

        yield return new WaitForSeconds(1f);

        yield return LoopFadeTransition(skateFly, 0f);

    }

    IEnumerator LoopFadeTransition(AudioClip audioClip, float transitionDuration) {

        float timer;

        if (audioSource.clip != null) {

            timer = 0f;
            while (timer < transitionDuration) {

                timer += Time.deltaTime;

                float perc = timer / transitionDuration;
                perc = 0.5f + Mathf.Cos(perc * Mathf.PI) / 2;

                audioSource.volume = perc * sfxVolumeScale * globalVolume;

                yield return null;

            }

            audioSource.volume = 0f;
            
        }

        if (audioClip == null) {

            audioSource.enabled = false;
            yield break;

        }

        if (!audioSource.enabled) audioSource.enabled = true;
        
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.Play();

        timer = 0f;
        while (timer < transitionDuration) {

            timer += Time.deltaTime;

            float perc = timer / transitionDuration;
            perc = 0.5f + Mathf.Cos(perc * Mathf.PI + Mathf.PI) / 2;

            audioSource.volume = perc * sfxVolumeScale * globalVolume;

            yield return null;

        }

        audioSource.volume = sfxVolumeScale * globalVolume;

    }

}
