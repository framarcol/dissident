﻿using UnityEngine;

public class InGameSound : MonoBehaviour {

    public enum SoundLayerTypes { LAYER_BASE = 0, LAYER1 = 1, LAYER2 = 2, LAYER_EXTRA = 3 }

    public SoundLayer layerBase;
    public SoundLayer layer1;
    public SoundLayer layer2;
    public SoundLayer layerExtra;

    public int initLoopLayerBase = -1;
    public int initLoopLayer1 = -1;
    public int initLoopLayer2 = -1;
    public int initLoopLayerExtra = -1;

	void Start () {

        layerBase.PlayLoop(initLoopLayerBase, 1f);
        layer1.PlayLoop(initLoopLayer1, 1f);
        layer2.PlayLoop(initLoopLayer2, 1f);
        layerExtra.PlayLoop(initLoopLayerExtra, 1f);

	}
    
    public void ChangeLoop(SoundLayerTypes soundLayerType, int soundId, float transitionDuration, float targetVolume = 1, bool waitForNewCycle = false) {

        SoundLayer targetLayer = null;

        switch (soundLayerType) {

            case SoundLayerTypes.LAYER_BASE:

                targetLayer = layerBase;

                break;
            case SoundLayerTypes.LAYER1:

                targetLayer = layer1;

                break;
            case SoundLayerTypes.LAYER2:

                targetLayer = layer2;

                break;
            case SoundLayerTypes.LAYER_EXTRA:

                targetLayer = layerExtra;

                break;
            
        }

        targetLayer.PlayLoop(soundId, transitionDuration, targetVolume, waitForNewCycle);

    }

}
