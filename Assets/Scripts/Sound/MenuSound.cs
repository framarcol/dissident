﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MenuSound : MonoBehaviour {

    private AudioSource audioSource;
    
	void Start () {

        audioSource = GetComponent<AudioSource>();

	}
	
    public void PlaySound(AudioClip audioClip) {

        if(audioSource != null) audioSource.PlayOneShot(audioClip);

    }

}
