﻿using UnityEngine;
using System.Collections;

public class MusicChanger : MonoBehaviour {

    public InGameSound.SoundLayerTypes soundLayerType;
    public int loopId;
    public float transitionDuration = 1f;
    public float targetVolume = 1f;

    public bool waitForNewCycle = false;

    InGameSound inGameSound;

    bool entered = false;

	void Start () {

        inGameSound = FindObjectOfType<InGameSound>();

	}
	
    void OnTriggerEnter(Collider other) {

        if(!entered && other.tag == "Player") {

            entered = true;

            inGameSound.ChangeLoop(soundLayerType, loopId, transitionDuration, targetVolume, waitForNewCycle);

        }

    }

}
