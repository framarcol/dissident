﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public bool sync = false;

    AudioSource audioSource;

    float musicVolumeScale;
    
	// Use this for initialization
	void Start () {

        audioSource = GetComponent<AudioSource>();

        GameManager.OnGameCompleted += TriggerLowVolume;
        GameManager.OnGamePaused += TriggerLowVolume;
        GameManager.OnGameOver += TriggerLowVolume;

        GameManager.OnGameResumed += TriggerResetVolume;

        musicVolumeScale = PlayerPrefs.GetFloat(GameManager.MusicVolumePref, 1f);
        audioSource.volume = musicVolumeScale;

        if (sync) PlayerAnimEvents.OnPlayerInitLevel += StartMusic;
        else StartMusic();

    }

    public void StartMusic() {

        audioSource.Play();

    }

    private void Update()
    {
        musicVolumeScale = PlayerPrefs.GetFloat(GameManager.MusicVolumePref, 1f);
        audioSource.volume = musicVolumeScale;
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            if(audioSource!=null)
            audioSource.Play();
        }
        else
        {
            audioSource.Pause();
        }
    }

    /*void Update() {

        float realMusicVolumeScale = PlayerPrefs.GetFloat(GameManager.MusicVolumePref, 1f);

        if (musicVolumeScale != realMusicVolumeScale) {

            musicVolumeScale = realMusicVolumeScale;
            audioSource.volume = musicVolumeScale;

        }

    }*/

    public void TriggerLowVolume() {

        audioSource.volume = 0.2f * musicVolumeScale;

    }

    public void TriggerResetVolume() {

        audioSource.volume = 1f * musicVolumeScale;

    }

}
