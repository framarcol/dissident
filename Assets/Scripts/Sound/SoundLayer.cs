﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class SoundLayer : MonoBehaviour {

    public List<AudioClip> audioLoops;

    public SoundLayer masterLayer;
    public bool slaveLayer;

    AudioSource audioSource;
    public AudioSource getAudioSource { get { return audioSource; } }

    float musicVolumeScale;
    
    public int activeClip = -1;
    
	void Awake () {

        audioSource = GetComponent<AudioSource>();

        if (!audioSource.loop) audioSource.loop = true;

        musicVolumeScale = PlayerPrefs.GetFloat(GameManager.MusicVolumePref, 1f);
        audioSource.volume = musicVolumeScale;
        
    }

    void Start() {

        GameManager.OnGameCompleted += LowMusic;
        GameManager.OnGameOver += LowMusic;

    }

    public void LowMusic() {

        audioSource.volume = musicVolumeScale * 0.25f;

    }

    public void PlayLoop(int id, float transitionDuration, float targetVolume = 1, bool waitForNewCycle = false) {

        if (targetVolume > 1) targetVolume = 1;
        else if (targetVolume < 0) targetVolume = 0;

        StartCoroutine(LoopFadeTransition(id, transitionDuration, targetVolume, waitForNewCycle));
        
    }

    IEnumerator LoopFadeTransition(int id, float transitionDuration, float targetVolume, bool waitForNewCycle) {

        if (waitForNewCycle) {
            
            if (slaveLayer) {

                int oldTimeSamples = masterLayer.getAudioSource.timeSamples;

                while (masterLayer.getAudioSource.timeSamples >= oldTimeSamples) {
                    oldTimeSamples = masterLayer.getAudioSource.timeSamples;
                    yield return null;
                }

            } else {

                int oldTimeSamples = audioSource.timeSamples;

                while (audioSource.timeSamples >= oldTimeSamples) {
                    oldTimeSamples = audioSource.timeSamples;
                    yield return null;
                }


            }

        }

        float timer;

        if (id == activeClip) yield break;

        if (activeClip != -1 || id == -1) {

            timer = 0f;
            while (timer < transitionDuration) {

                timer += Time.deltaTime;

                float perc = timer / transitionDuration;
                perc = 0.5f + Mathf.Cos(perc * Mathf.PI) / 2;

                audioSource.volume = perc * musicVolumeScale;

                yield return null;

            }

            audioSource.volume = 0f;

            if(id == -1) {

                activeClip = -1;
                audioSource.enabled = false;
                yield break;

            }

        }

        if (!audioSource.enabled) audioSource.enabled = true;

        int timeSamples = 0;
        if (slaveLayer) {

            timeSamples = masterLayer.getAudioSource.timeSamples;

        }
        audioSource.Stop();
        audioSource.clip = audioLoops[id];
        audioSource.Play();
        audioSource.timeSamples = timeSamples;

        timer = 0f;
        while (timer < transitionDuration) {

            timer += Time.deltaTime;

            float perc = timer / transitionDuration;
            perc = 0.5f + Mathf.Cos(perc * Mathf.PI + Mathf.PI) / 2;

            audioSource.volume = perc * musicVolumeScale * targetVolume;

            yield return null;

        }

        audioSource.volume = musicVolumeScale * targetVolume;
        
        activeClip = id;

    }

}
