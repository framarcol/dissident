﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraPlanesTestUI : MonoBehaviour {

    public Slider nearPlane;
    public Slider farPlane;

    public Toggle depthToggle;

	// Use this for initialization
	void Start () {

        if (nearPlane != null) nearPlane.value = Camera.main.nearClipPlane;
        if (farPlane != null) farPlane.value = Camera.main.farClipPlane;

        if (depthToggle != null) depthToggle.isOn = Camera.main.depthTextureMode == DepthTextureMode.Depth;

    }
	
    public void depthChanged(bool value) {

        Camera.main.depthTextureMode = (value) ? DepthTextureMode.Depth : DepthTextureMode.None;

    }

    public void nearPlaneChanged(float value) {

        Camera.main.nearClipPlane = value;
        
    }

    public void farPlaneChanged(float value) {

        Camera.main.farClipPlane = value;

    }

}
