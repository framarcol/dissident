﻿using UnityEngine;
using System.Collections;

public class BackgroundPanels : MonoBehaviour {

    public RectTransform topPanel;
    public RectTransform bottomPanel;

    public float apertureTime = 0.5f;

    bool closed;
    
    public bool IsClosed() {

        return closed;

    }

    public bool IsOpened() {

        return !closed;

    }
    
    public void ClosePanels() {

        topPanel.gameObject.SetActive(true);
        bottomPanel.gameObject.SetActive(true);

        topPanel.anchoredPosition = new Vector2(0, 420);
        bottomPanel.anchoredPosition = new Vector2(0, -730);

        StartCoroutine(CloseAnimation());

    }

    IEnumerator CloseAnimation() {

        float lastTimer = Time.realtimeSinceStartup;
        float actualTime = 0f;

        float topY = topPanel.anchoredPosition.y;
        float bottomY = bottomPanel.anchoredPosition.y;

        while(actualTime < apertureTime) {

            actualTime += Time.realtimeSinceStartup - lastTimer;
            lastTimer = Time.realtimeSinceStartup;

            float perc = actualTime / apertureTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            //Debug.Log(apertureTime + " - " + actualTime + " - " + perc);

            float topHeight = topY - (topY * perc);
            float bottomHeight = bottomY - (bottomY * perc);

            topPanel.anchoredPosition = new Vector2(0, topHeight);
            bottomPanel.anchoredPosition = new Vector2(0, bottomHeight);

            yield return null;

        }

        topPanel.anchoredPosition = new Vector2(0, 0);
        bottomPanel.anchoredPosition = new Vector2(0, 0);

        closed = true;

    }

    public void OpenPanels() {

        StartCoroutine(OpenAnimation());

    }

    IEnumerator OpenAnimation() {

        float lastTimer = Time.realtimeSinceStartup;
        float actualTime = 0f;

        float targetTopY = 420f;
        float targetBottomY = -730f;

        while (actualTime < apertureTime) {

            actualTime += Time.realtimeSinceStartup - lastTimer;
            lastTimer = Time.realtimeSinceStartup;

            float perc = actualTime / apertureTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            //Debug.Log(apertureTime + " - " + actualTime + " - " + perc);

            float topHeight = (targetTopY * perc);
            float bottomHeight = (targetBottomY * perc);

            topPanel.anchoredPosition = new Vector2(0, topHeight);
            bottomPanel.anchoredPosition = new Vector2(0, bottomHeight);

            yield return null;

        }

        topPanel.gameObject.SetActive(false);
        bottomPanel.gameObject.SetActive(false);

        topPanel.anchoredPosition = new Vector2(0, 0);
        bottomPanel.anchoredPosition = new Vector2(0, 0);

        closed = false;

    }

}
