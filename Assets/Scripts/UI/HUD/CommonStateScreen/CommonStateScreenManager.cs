﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommonStateScreenManager : MonoBehaviour {

    public GameObject inGameHUD;

    public GameObject countdownModule;

    public BackgroundPanels backgroundPanels;

    public GameObject modulesParent;

    public float fadeModulesTime = 0.5f;

    public List<GameObject> pauseModules;
    public List<GameObject> gameOverModules;
    public List<GameObject> gameCompletedModules;

    CanvasGroup inGameCanvas;
    
    void Awake() {

        inGameCanvas = inGameHUD.GetComponent<CanvasGroup>();

    }

    public void TriggerCommonStateScreen() {

        StartCoroutine(FollowOpenEvents());
        
    }
    
    IEnumerator FollowOpenEvents() {

        //Disable in-game HUD
        inGameCanvas.interactable = false;

        //If it's game over wait a moment before closing the panels
        if (GameManager.gameState == GameState.GAME_OVER || GameManager.gameState == GameState.COMPLETED) yield return new WaitForSecondsRealtime(2.5f);

        //Close the panels
        backgroundPanels.ClosePanels();
        //Wait until closed
        yield return new WaitUntil(backgroundPanels.IsClosed);

        //Deactivate in-game HUD
        //inGameCanvas.gameObject.SetActive(false);

        //Activate the correct modules
        switch (GameManager.gameState) {

            case GameState.PAUSED:
                for (int i = 0; i < pauseModules.Count; i++) {

                    if(pauseModules[i] != null) pauseModules[i].SetActive(true);

                }
                break;

            case GameState.GAME_OVER:
                for (int i = 0; i < gameOverModules.Count; i++) {

                    if (gameOverModules[i] != null) gameOverModules[i].SetActive(true);

                }
                break;

            case GameState.COMPLETED:
                for(int i = 0; i < gameCompletedModules.Count; i++) {

                    if (gameCompletedModules[i] != null) gameCompletedModules[i].SetActive(true);

                }
                break;

        }
        
        //Make them appear
        yield return StartCoroutine(FadeInModules());

        int count;
        //Run each of their animations in the correct order
        switch (GameManager.gameState) {

            case GameState.PAUSED: break;

            case GameState.GAME_OVER:

                count = gameOverModules.Count;
                for (int i = 0; i < count; i++) {

                    if (gameOverModules[i] == null) continue;

                    ModuleUI module = gameOverModules[i].GetComponent<ModuleUI>();

                    if (module != null) {

                        module.OnAnimate();

                        yield return new WaitUntil(module.Animated);
                    }

                }

                break;

            case GameState.COMPLETED:

                count = gameCompletedModules.Count;
                for (int i = 0; i < count; i++) {

                    if (gameCompletedModules[i] == null) continue;

                    ModuleUI module = gameCompletedModules[i].GetComponent<ModuleUI>();

                    if (module != null) {

                        module.OnAnimate();

                        yield return new WaitUntil(module.Animated);
                    }

                }

                break;

        }
        

    }

    public void TriggerUnpauseScreen() {
        
        StartCoroutine(FollowUnpauseEvents());

    }

    IEnumerator FollowUnpauseEvents() {

        if (Time.timeScale != 0f) Time.timeScale = 0f;

        //Activate in-game HUD
        //inGameCanvas.gameObject.SetActive(true);

        //Fade-out the modules
        yield return StartCoroutine(FadeOutModules());

        //Deactivate them, once faded
        for (int i = 0; i < pauseModules.Count; i++) {

            if(pauseModules[i] != null) pauseModules[i].SetActive(false);

        }

        //Open the panels
        backgroundPanels.OpenPanels();
        //Wait until opened
        yield return new WaitUntil(backgroundPanels.IsOpened);

        //Play the countdown
        countdownModule.SetActive(true);
        ModuleUI countdown = countdownModule.GetComponent<ModuleUI>();
        countdown.OnAnimate();
        yield return new WaitUntil(countdown.Animated);
        countdownModule.SetActive(false);

        //Enable in-game HUD
        inGameCanvas.interactable = true;

        //Resume the game
        GameManager.ResumeGame();

    }
    
    public void TriggerCountdown() {

        StartCoroutine(AnimateCountdown());

    }

    IEnumerator AnimateCountdown() {

        if (Time.timeScale != 0f) Time.timeScale = 0f;

        //Disable in-game HUD
        inGameCanvas.interactable = false;

        //Play the countdown
        countdownModule.SetActive(true);
        ModuleUI countdown = countdownModule.GetComponent<ModuleUI>();
        countdown.OnAnimate();
        yield return new WaitUntil(countdown.Animated);
        countdownModule.SetActive(false);

        //Enable in-game HUD
        inGameCanvas.interactable = true;

        Time.timeScale = 1f;

    }

    IEnumerator FadeInModules() {

        CanvasGroup canvasGroup = modulesParent.GetComponent<CanvasGroup>();

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while(timer < fadeModulesTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / fadeModulesTime;

            canvasGroup.alpha = perc;

            yield return null;

        }

        canvasGroup.alpha = 1f;

    }

    IEnumerator FadeOutModules() {

        CanvasGroup canvasGroup = modulesParent.GetComponent<CanvasGroup>();

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < fadeModulesTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / fadeModulesTime;

            canvasGroup.alpha = 1 - perc;

            yield return null;

        }

        canvasGroup.alpha = 0f;

    }

}
