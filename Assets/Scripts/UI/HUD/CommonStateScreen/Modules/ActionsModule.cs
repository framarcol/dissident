﻿using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.Advertisements;
using System.Collections;

public class ActionsModule : ModuleUI {

    public GameObject resumeButton;
    public GameObject nextLevelButton;
    public GameObject respawnOption;

    AsyncOperation asyncOperation;

    GameManager gameManager;
    StarsRequisites starsRequisites;

    Weapon weapon;

    void Awake() {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        starsRequisites = Resources.Load("DataObjects/StarsRequisites", typeof(StarsRequisites)) as StarsRequisites;

        weapon = gameManager.playerAnimEvents.weapon;

    }

    public override void OnActivate() {

        switch(GameManager.gameState){

            case GameState.PAUSED:
                resumeButton.SetActive(true);
                nextLevelButton.SetActive(false);
                respawnOption.SetActive(false);
                break;

            case GameState.GAME_OVER:
                resumeButton.SetActive(false);
                nextLevelButton.SetActive(false);
                if(GameManager.activeCheckpoint != -1) respawnOption.SetActive(true);
                else respawnOption.SetActive(false);
                PlayerPrefs.SetInt("Muertes", PlayerPrefs.GetInt("Muertes") + 1);
                PlayerPrefs.SetInt("MuertesPu", PlayerPrefs.GetInt("MuertesPu") + 1);
                break;

            case GameState.COMPLETED:
                resumeButton.SetActive(false);
                if (gameManager.endPoint) nextLevelButton.SetActive(false);
                else nextLevelButton.SetActive(true);
                respawnOption.SetActive(false);
                PlayerPrefs.SetInt("NivelesConseguidos", PlayerPrefs.GetInt("NivelesConseguidos") + 1);
                PlayerPrefs.SetInt("NivelesConseguidosPu", PlayerPrefs.GetInt("NivelesConseguidosPu") + 1);
                StartCoroutine(ActualizarUsuario());
                break;
                
        }

    }

    IEnumerator ActualizarUsuario()
    {

        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error == null)
        {
            WWWForm datos = new WWWForm();

            float estrellas = 0, estrellasUno = 0, estrellasDos = 0, estrellasTres = 0;

            for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++)
                {

                    estrellas += GameDataController.gameData.stars[j, i];
                    if (GameDataController.gameData.stars[j, i] == 1)
                    {
                        estrellasUno++;
                    }
                    else if (GameDataController.gameData.stars[j, i] == 2)
                    {
                        estrellasDos++;
                    }
                    else if (GameDataController.gameData.stars[j, i] == 3)
                    {
                        estrellasTres++;
                    }
                }

            for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;
#if UNITY_ANDROID
            if (PlayerPrefs.GetInt("PermisosPlayJuegosAceptados") != 1)
            {
                datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
                datos.AddField("idG", SystemInfo.deviceUniqueIdentifier);
                datos.AddField("SO", "Android");
            }
            else
            {
                datos.AddField("id", Social.localUser.userName.ToString());
                datos.AddField("idG", Social.localUser.id.ToString());
                datos.AddField("SO", "Android");
            }
#endif
#if UNITY_IOS
        if (PlayerPrefs.GetInt("PermisosAppleAceptados") != 1)
        {
            datos.AddField("id", SystemInfo.deviceUniqueIdentifier);
            datos.AddField("idG", SystemInfo.deviceUniqueIdentifier);
        datos.AddField("SO", "Apple");
        }
        else
        {
            datos.AddField("id", Social.localUser.userName.ToString());
            datos.AddField("idG", Social.localUser.id.ToString());
        datos.AddField("SO", "Apple");
        }


#endif

            string FasesSuperadas = "";

            for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++) if (GameDataController.gameData.stars[j, i] > 0) FasesSuperadas += j + ";" + i + ";" + GameDataController.gameData.stars[j, i].ToString() + ";";

            for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) FasesSuperadas += 10 + ";" + i + ";" + 1 + ";";

#if UNITY_EDITOR
            FasesSuperadas = "10;12;1;10;14;1;10;4;1;";
#endif
            string IdolosConseguidos = "";

            for (int j = 0; j < 4; j++) for (int i = 0; i < 9; i++) for (int k = 0; k < 10; k++) if (GameDataController.gameData.idols[j, i, k]) IdolosConseguidos += j + ";" + i + ";" + k + ";";

#if UNITY_EDITOR
            IdolosConseguidos = "0;2;3;";
#endif

            datos.AddField("SOV", SystemInfo.operatingSystem.ToString());
            datos.AddField("Stars", estrellas.ToString());
            datos.AddField("StarsU", estrellasUno.ToString());
            datos.AddField("StarsD", estrellasDos.ToString());
            datos.AddField("StarsT", estrellasTres.ToString());
            datos.AddField("MuertesJugador", PlayerPrefs.GetInt("Muertes").ToString());
            datos.AddField("NivelesCompletados", PlayerPrefs.GetInt("NivelesConseguidos").ToString());
            datos.AddField("AnunciosVistos", PlayerPrefs.GetInt("Anuncios"));
            datos.AddField("CheckPoints", PlayerPrefs.GetInt("CheckPoint"));
            datos.AddField("VecesTienda", PlayerPrefs.GetInt("VecesTienda"));
            datos.AddField("idioma", PlayerPrefs.GetString(GameManager.LanguagePref));
            datos.AddField("FasesSuperadas", FasesSuperadas);
            datos.AddField("IdolosConseguidos", IdolosConseguidos);

            //r(int i = 0; i < GameDataController.gameData.playerRecords.LongLength)
            uint puntosTotales = 0;
            foreach (uint i in GameDataController.gameData.playerRecords)
            {
                puntosTotales += i;
            }

            datos.AddField("PuntosTotalesJuego", puntosTotales.ToString());


            WWW itemsData = new WWW("https://agapornigames.com/dodongo/checkId2.php", datos);

            yield return itemsData;
        }
    }

    public void NextLevel() {

        if (GameManager.activeCheckpoint != -1) gameManager.SetCheckpoint(null);

        int sceneIndex = -1;
        int nextLevel = -1;

        int buildIndex = SceneManager.GetActiveScene().buildIndex;

        //FROM BUNDLE
        if(buildIndex == -1) {

            //SPECIAL
            if (gameManager.specialLevel) {

                nextLevel = gameManager.specialLevelRequisites.nextLevel;

                if(nextLevel == -1) {

                    sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);

                    LoadSc.nextScene = sceneIndex + 1;
                    LoadSc.specialNextLevelScene = true;
                    LoadSc.specialPrevLevelScene = true;

                    LoadSc.fromBundle = true;

                } else {

                    LoadSc.nextScene = nextLevel;
                    LoadSc.specialPrevLevelScene = true;

                    LoadSc.fromBundle = true;

                }

            //NORMAL
            } else {

                nextLevel = starsRequisites.GetNextLevel(gameManager.world, gameManager.levelId);

                if(nextLevel == -1) {

                    sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);

                    LoadSc.nextScene = sceneIndex + 1;

                    LoadSc.fromBundle = true;

                } else {

                    LoadSc.nextScene = nextLevel;
                    LoadSc.specialNextLevelScene = true;

                    LoadSc.fromBundle = true;

                }

            }

        //NOT FROM BUNDLE
        } else {

            //SPECIAL
            if (gameManager.specialLevel) {

                nextLevel = gameManager.specialLevelRequisites.nextLevel;

                if(nextLevel == -1) {

                    sceneIndex = SceneManager.GetActiveScene().buildIndex;

                    LoadSc.nextScene = sceneIndex + 1;
                    LoadSc.specialNextLevelScene = true;
                    LoadSc.specialPrevLevelScene = true;

                } else {

                    LoadSc.nextScene = nextLevel;
                    LoadSc.specialPrevLevelScene = true;

                }

            //NORMAL
            } else {

                nextLevel = starsRequisites.GetNextLevel(gameManager.world, gameManager.levelId);

                if(nextLevel == -1) {

                    sceneIndex = SceneManager.GetActiveScene().buildIndex;

                    LoadSc.nextScene = sceneIndex + 1;

                } else {

                    LoadSc.nextScene = nextLevel;
                    LoadSc.specialNextLevelScene = true;
                    
                }

            }

        }
        
        /*
        if (BundleManager.scenesList == null || BundleManager.scenesList.Count == 0) { 

            sceneIndex = SceneManager.GetActiveScene().buildIndex;
            LoadSc.nextScene = sceneIndex + 1;

        } else {

            //int nextLevel = -1;

            if (gameManager.specialLevel) nextLevel = gameManager.specialLevelRequisites.nextLevel;
            else {

                nextLevel = starsRequisites.GetNextLevel(gameManager.world, gameManager.levelId);
                
            }

            if (nextLevel == -1) {

                if (gameManager.specialLevel) {

                    sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);

                    LoadSc.specialPrevLevelScene = true;
                    LoadSc.specialNextLevelScene = true;

                } else {

                    sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);

                }

                if ( (!gameManager.specialLevel && BundleManager.scenesList.Count - 1 < sceneIndex + 1) || (gameManager.specialLevel && BundleManager.specialScenesList.Count - 1 < sceneIndex + 1)) LoadSc.nextScene = -1;
                else LoadSc.nextScene = sceneIndex + 1;

            } else {

                if (gameManager.specialLevel) {

                    sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);

                    LoadSc.specialPrevLevelScene = true;

                } else {

                    sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);

                    LoadSc.specialNextLevelScene = true;

                }

                LoadSc.nextScene = nextLevel;

            }
            
        }
        */
        LoadSc.prevScene = sceneIndex;
        
        //LoadSc.prevScene = SceneManager.GetActiveScene().buildIndex;
        //LoadSc.nextScene = LoadSc.prevScene + 1;

        asyncOperation = SceneManager.LoadSceneAsync("LoadScreen");

        int numeroFinal = PlayerPrefs.GetInt("Muertes") + PlayerPrefs.GetInt("NivelesConseguidos");

        string mostrarAnuncio = "MostrarAnuncio";

        if(!PlayerPrefs.HasKey(mostrarAnuncio)) PlayerPrefs.SetInt(mostrarAnuncio, 0);


        if (GameDataController.gameData.stars[0, 2] > 0)
        {
            if ((!GameDataController.gameData.premiumState && (numeroFinal % 3 == 0) && PlayerPrefs.GetInt(mostrarAnuncio) == 0) || (GameDataController.gameData.betaUser && (numeroFinal % 3 == 0) && PlayerPrefs.GetInt(mostrarAnuncio) == 0))
            {
                PlayerPrefs.SetInt(mostrarAnuncio, 1);
                PlayerPrefs.SetInt("Anuncios", PlayerPrefs.GetInt("Anuncios") + 1);
                //ShowAd();
            }
            else if (numeroFinal % 3 == 1) PlayerPrefs.SetInt(mostrarAnuncio, 2);
            else if (numeroFinal % 3 == 2) PlayerPrefs.SetInt(mostrarAnuncio, 0);
            else if (numeroFinal % 3 == 0) PlayerPrefs.SetInt(mostrarAnuncio, 0);
        }


    }

    public void Restart()
    {

        if (weapon != null)
        {
            IdolCatcherProjectile idolCatcher = weapon.projectilePrefab.GetComponent<IdolCatcherProjectile>();

            if (idolCatcher != null) IdolCatcherProjectile.weaponUses = idolCatcher.startingWeaponUses;
        }

        if (GameManager.activeCheckpoint != -1) gameManager.SetCheckpoint(null);

        int sceneIndex;

        if (BundleManager.scenesList == null || BundleManager.scenesList.Count == 0 || LoadSc.worldSelected == 0)
            sceneIndex = SceneManager.GetActiveScene().buildIndex;
        else
        {
            if (gameManager.specialLevel)
            {
                sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);
                LoadSc.specialPrevLevelScene = true;
                LoadSc.specialNextLevelScene = true;
                LoadSc.fromBundle = true;
            }
            else
            {
                sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);
                LoadSc.fromBundle = true;
            }
        }

        LoadSc.prevScene = sceneIndex;
        LoadSc.nextScene = sceneIndex;





        //LoadSc.prevScene = SceneManager.GetActiveScene().buildIndex;
        //LoadSc.nextScene = LoadSc.prevScene;

        asyncOperation = SceneManager.LoadSceneAsync("LoadScreen");

        int numeroFinal = PlayerPrefs.GetInt("Muertes") + PlayerPrefs.GetInt("NivelesConseguidos");

        string mostrarAnuncio = "MostrarAnuncio";

        if (!PlayerPrefs.HasKey(mostrarAnuncio)) PlayerPrefs.SetInt(mostrarAnuncio, 0);


        if (GameDataController.gameData.stars[0, 2] > 0)
        {
            if ((!GameDataController.gameData.premiumState && (numeroFinal % 3 == 0) && PlayerPrefs.GetInt(mostrarAnuncio) == 0) || (GameDataController.gameData.betaUser && (numeroFinal % 3 == 0) && PlayerPrefs.GetInt(mostrarAnuncio) == 0))
            {
                PlayerPrefs.SetInt(mostrarAnuncio, 1);
                PlayerPrefs.SetInt("Anuncios", PlayerPrefs.GetInt("Anuncios") + 1);
                //ShowAd();
            }
            else if (numeroFinal % 3 == 1) PlayerPrefs.SetInt(mostrarAnuncio, 2);
            else if (numeroFinal % 3 == 2) PlayerPrefs.SetInt(mostrarAnuncio, 0);
            else if (numeroFinal % 3 == 0) PlayerPrefs.SetInt(mostrarAnuncio, 0);
        }

    }

    public void MainMenu() {

        if (weapon != null) {
            IdolCatcherProjectile idolCatcher = weapon.projectilePrefab.GetComponent<IdolCatcherProjectile>();

            if (idolCatcher != null) IdolCatcherProjectile.weaponUses = idolCatcher.startingWeaponUses;
        }

        if (GameManager.activeCheckpoint != -1) gameManager.SetCheckpoint(null);

        if (Time.timeScale != 1f) Time.timeScale = 1f;

        int sceneIndex;

        if (BundleManager.scenesList == null || BundleManager.scenesList.Count == 0)
            sceneIndex = SceneManager.GetActiveScene().buildIndex;
        else {
            if (gameManager.specialLevel) {
                sceneIndex = BundleManager.specialScenesList.IndexOf(SceneManager.GetActiveScene().name);
                LoadSc.specialPrevLevelScene = true;
            } else {
                sceneIndex = BundleManager.scenesList.IndexOf(SceneManager.GetActiveScene().name);
            }
        }

        LoadSc.prevScene = sceneIndex;
        LoadSc.nextScene = -1;

        //LoadSc.prevScene = SceneManager.GetActiveScene().buildIndex;
        //LoadSc.nextScene = -1;

        asyncOperation = SceneManager.LoadSceneAsync("LoadScreen");

        /*	#if UNITY_ANDROID
            if (!GameDataController.gameData.premiumState) ShowAd();
            #endif*/

    }


    void ShowAd() {

        if (Debug.isDebugBuild) return;

     /*   if (Advertisement.IsReady()) {

            asyncOperation.allowSceneActivation = false;

            ShowOptions showOptions = new ShowOptions();
		
            showOptions.resultCallback += LoadSceneScreen;
			
            Advertisement.Show(null, showOptions);

        }*/

    }


 
	/*void LoadSceneScreen(ShowResult showResult) {

	    asyncOperation.allowSceneActivation = true;

	}*/


}
