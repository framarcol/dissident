﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountdownModule : ModuleUI {

    public RectTransform threeCount;
    public RectTransform twoCount;
    public RectTransform oneCount;

    CanvasGroup threeCanvas;
    CanvasGroup twoCanvas;
    CanvasGroup oneCanvas;
    
	void Awake () {

        threeCanvas = threeCount.GetComponent <CanvasGroup>();
        twoCanvas = twoCount.GetComponent<CanvasGroup>();
        oneCanvas = oneCount.GetComponent<CanvasGroup>();

	}

    public override void OnAnimate() {

        StartCoroutine(AnimateCountdown());

    }

    IEnumerator AnimateCountdown() {

        yield return StartCoroutine(AnimateNumber(threeCount, threeCanvas));
        
        yield return StartCoroutine(AnimateNumber(twoCount, twoCanvas));

        yield return StartCoroutine(AnimateNumber(oneCount, oneCanvas));


        animated = true;

    }

    IEnumerator AnimateNumber(RectTransform rectTransform, CanvasGroup canvas) {

        rectTransform.gameObject.SetActive(true);

        float diffSize = 320f - rectTransform.sizeDelta.x;

        canvas.alpha = 0f;
        rectTransform.sizeDelta = new Vector2(320f,320f);
        
        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < 1f) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / 1f;

            float sizeDelta = 320f - (perc * diffSize);
            rectTransform.sizeDelta = new Vector2(sizeDelta, sizeDelta);

            canvas.alpha = perc;

            yield return null;

        }

        //rectTransform.sizeDelta = new Vector2(250f, 250f);
        rectTransform.gameObject.SetActive(false);

    }

}
