﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DiamondModule : ModuleUI {

    public Text totalDiamondsText;

    uint totalDiamonds;

    public void SetTotalDiamonds(uint diamonds) {

        totalDiamonds = diamonds;

        totalDiamondsText.text = totalDiamonds.ToString();

    }

    public void AddToCounter() {

        totalDiamondsText.text = (++totalDiamonds).ToString();

    }

}
