﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GoldModule : ModuleUI {

    public Text totalGold;
    public Text goldAmount;

    public float goldAnimationTime = 2f;

    public GameObject goldIconPrefab;
    public RectTransform goldIconTarget;
    public int deltaGold = 5;
    public float flyGoldTime = 0.75f;

    Queue<GameObject> goldIconPool = new Queue<GameObject>();

    uint earningGold;
    uint goldCurrency;

    ScoreController scoreController;

    void Awake() {

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        for(int i = 0; i < 50; i++) {

            GameObject coin = Instantiate(goldIconPrefab, transform) as GameObject;
            coin.SetActive(false);
            goldIconPool.Enqueue(coin);

        }

    }

    public override void OnActivate() {

        //Display total gold before death
        goldCurrency = GameDataController.gameData.goldCurrency;
        totalGold.text = goldCurrency.ToString();

        //Calculate gold earnings until death
        earningGold = (uint)Mathf.FloorToInt((GameManager.GoldConversionRate * scoreController.totalScore));
        goldAmount.text = "+" + earningGold.ToString();

        //Add the gold now, before the animation
        GameManager.AddGold(earningGold);

    }

    public override void OnAnimate() {

        if (earningGold == 0) {

            animated = true;
            return;

        }

        StartCoroutine(AnimateGold());

    }

    IEnumerator AnimateGold() {

        yield return new WaitForSeconds(1f);

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        uint targetGold = GameDataController.gameData.goldCurrency;
        uint diff = targetGold - goldCurrency;

        int goldAccum = 0;
        int lastGold = 0;

        while (timer < goldAnimationTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / goldAnimationTime;
            
            uint leftGold = earningGold - (uint)(perc * earningGold);
            uint totalGoldCurrency = goldCurrency + (uint)(perc * diff);

            goldAmount.text = "+" + leftGold.ToString();
            totalGold.text = totalGoldCurrency.ToString();
            
            float actualGold = perc * earningGold;
            goldAccum += (int)actualGold - lastGold;
            lastGold = (int)actualGold;
            
            if(goldAccum > deltaGold) {

                GameObject coin;

                if(goldIconPool.Count > 0) {

                    coin = goldIconPool.Dequeue();
                    coin.SetActive(true);

                } else {

                    coin = Instantiate(goldIconPrefab, transform) as GameObject;

                }

                StartCoroutine(FlyGold(coin));

                goldAccum = 0;

            }
            
            yield return null;

        }

        goldAmount.text = "+0";
        totalGold.text = targetGold.ToString();

        animated = true;

    }

    IEnumerator FlyGold(GameObject coin) {

        RectTransform coinTransform = coin.GetComponent<RectTransform>();

        Vector3 startPos = goldAmount.rectTransform.position + new Vector3(Random.Range(-50f, 50f), 50f, 0);
        Vector3 trajectory = goldIconTarget.position - startPos;

        coinTransform.anchoredPosition = startPos;

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while(timer < flyGoldTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / flyGoldTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            coinTransform.position = startPos + (perc * trajectory);

            yield return null;

        }

        coin.SetActive(false);
        goldIconPool.Enqueue(coin);

    }

}
