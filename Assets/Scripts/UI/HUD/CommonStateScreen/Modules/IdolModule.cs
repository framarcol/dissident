﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class IdolModule : ModuleUI {

    public GameObject placeholdersParent;
    public GameObject idolIconsParent;

    Image[] placeholders;
    Image[] idolIcons;

    public GameObject idolSprite;
    public List<Sprite> idolWorldIcons;

    GameManager gameManager;

    void Awake() {

        placeholders = placeholdersParent.GetComponentsInChildren<Image>();
        idolIcons = idolIconsParent.GetComponentsInChildren<Image>();

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        idolSprite.GetComponent<Image>().sprite = idolWorldIcons[gameManager.worldId];

        for (int i = 0; i < GameManager.MaxIdolsPerLevel; i++) {

            idolIcons[i].sprite = gameManager.levelIdols[i].idolIcon;

        }
        
        /*for(int i = 0; i < gameManager.idolCounter; i++) {

            placeholders[i].color = new Color(1,1,1,1);
            idolIcons[i].color = new Color(1,1,1,1);

        }*/

    }

    public override void OnActivate() {
        
        for(int i = 0; i < GameManager.MaxIdolsPerLevel; i++) {

            if (gameManager.idolsCheck[i]) {

                placeholders[i].color = new Color(1,1,1,1);
                idolIcons[i].color = new Color(1,1,1,1);

            }

        }

    }

}
