﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelModule : ModuleUI {

    public Text levelLabel;

    GameManager gameManager;

    void Awake() {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

    } 

    public override void OnActivate() {

        if (gameManager.specialLevel) {

            levelLabel.text = (gameManager.specialLevelRequisites != null) ? gameManager.specialLevelRequisites.levelName : "404 - NOT FOUND";
            return;

        }

        string levelName = "";

        switch (gameManager.worldId) {

            case 0: levelName = "Qualdrom"; break;
            case 1: levelName = "Yldoon"; break;
            case 2: levelName = "Myräkkä"; break;
            case 3: levelName = "Wulussa"; break;

        }

        levelLabel.text = levelName + " - " + (gameManager.levelId + 1);

        //Debug.Log(gameManager.worldId);

    }

}
