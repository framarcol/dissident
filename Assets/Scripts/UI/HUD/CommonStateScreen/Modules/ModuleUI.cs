﻿using UnityEngine;
using System.Collections;

public class ModuleUI : MonoBehaviour {

    protected bool animated = false;

    void OnEnable() {

        animated = false;

        OnActivate();

    }

    public bool Animated() {

        return animated;

    }

    public virtual void OnActivate() { }

    public virtual void OnAnimate() {

        animated = true;

    }

}
