﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreModule : ModuleUI {

    public Text scoreCount;

    public GameObject maxScoreLine;
    public Text maxScoreCount;

    public GameObject recordAchievement;
    public float recordAchieveTime = 0.75f;

    ScoreController scoreController;
    GameManager gameManager;

    uint recordScore;
    uint totalScore;

    void Awake() {

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        if (gameManager.specialLevel) {

            maxScoreLine.SetActive(false);


        } else {
            recordScore = GameDataController.gameData.playerRecords[gameManager.worldId, gameManager.levelId];
            maxScoreCount.text = recordScore.ToString();
        }

    }

    public override void OnActivate() {

        totalScore = (uint)Mathf.FloorToInt(scoreController.totalScore);
        scoreCount.text = totalScore.ToString();

        if (gameManager.specialLevel) return;

        //Check if the score is a player record
        if (GameManager.gameState == GameState.PAUSED) return;
        if (recordScore < totalScore) {

            GameManager.SetNewRecord(gameManager.worldId, gameManager.levelId, totalScore);

            StartCoroutine(CheckInternetConnection((uint)((gameManager.worldId * 15) + gameManager.levelId), totalScore));
            Debug.Log((uint)((gameManager.worldId * 15) + gameManager.levelId) + " - " + totalScore);

            maxScoreLine.SetActive(false);

        }
        
    }

    public override void OnAnimate() {

        if (gameManager.specialLevel) {
            animated = true;
            return;
        }

        if (totalScore <= recordScore) {

            animated = true;
            return;

        }
        
        StartCoroutine(AnimateRecordAchievement());

    }

    IEnumerator CheckInternetConnection(uint fase, uint puntuacion){
        WWW www = new WWW("https://agapornigames.com/dodongo/");
        yield return www;
        if (www.error != null)
        {
            Debug.LogError(www.error);
        }
        else
        {
            ControladorBaseDeDatos.SubirPuntuacion(fase, puntuacion);
        }
    }

    IEnumerator AnimateRecordAchievement() {
        
        recordAchievement.SetActive(true);

        Text recordText = recordAchievement.GetComponent<Text>();

        Vector2 targetSize = recordText.rectTransform.sizeDelta;
        Color32 targetColor = recordText.color;

        Vector2 initSize = new Vector2(245f,52.5f);
        Color32 initColor = new Color32(targetColor.r, targetColor.g, targetColor.b, 0);

        scoreCount.color = targetColor;

        recordText.rectTransform.sizeDelta = initSize;
        recordText.color = initColor;

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < recordAchieveTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / recordAchieveTime;

            recordText.rectTransform.sizeDelta = Vector2.Lerp(initSize, targetSize, perc);
            recordText.color = Color32.Lerp(initColor, targetColor, perc);

            yield return null;

        }

        animated = true;

    }

}
