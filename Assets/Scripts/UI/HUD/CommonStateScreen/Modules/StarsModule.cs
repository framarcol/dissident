﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StarsModule : ModuleUI {

    public GameObject oneStar;
    public GameObject twoStar;
    public GameObject threeStar;

    public List<Image> starOffIcons = new List<Image>(3);
    public List<Image> starOnIcons = new List<Image>(3);
    List<bool> starsAchieved;

    public Text completedText;
    public Text twoStarsScore;
    public Text twoStarsIdols;
    public Text threeStarsScore;
    public Text threeStarsIdols;

    public GameObject specialScore;
    public GameObject specialIdols;
    public Text specialRequisiteScore;
    public Text specialRequisiteIdols;

    public DiamondModule diamondModule;
    public RectTransform targetDiamondFly;
    public List<RectTransform> diamondIcons;
    public float textColoringTime = 0.5f;
    public float starAnimationTime = 1f;
    public float diamondFlyTime = 1f;

    float totalScore;
    int numIdols;

    GameManager gameManager;
    ScoreController scoreController;

    bool specialLevelCompleted;

    void Awake() {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        if (gameManager.specialLevel) {

            oneStar.GetComponent<RectTransform>().anchoredPosition = new Vector2(-25f, 0);
            twoStar.SetActive(false);
            threeStar.SetActive(false);

            if (gameManager.specialLevelRequisites != null) {

                if (gameManager.specialLevelRequisites.score > 0 && gameManager.specialLevelRequisites.idols > 0) {

                    specialScore.SetActive(true);
                    specialIdols.SetActive(true);
                    completedText.gameObject.SetActive(false);

                    specialRequisiteScore.text = gameManager.specialLevelRequisites.score.ToString();
                    specialRequisiteIdols.text = gameManager.specialLevelRequisites.idols.ToString();

                } else if (gameManager.specialLevelRequisites.score > 0) {

                    specialScore.SetActive(true);
                    specialScore.GetComponent<RectTransform>().anchoredPosition = new Vector2(specialScore.GetComponent<RectTransform>().anchoredPosition.x, 0f);
                    completedText.gameObject.SetActive(false);

                    specialRequisiteScore.text = gameManager.specialLevelRequisites.score.ToString();

                } else if (gameManager.specialLevelRequisites.idols > 0) {

                    specialIdols.SetActive(true);
                    specialIdols.GetComponent<RectTransform>().anchoredPosition = new Vector2(specialIdols.GetComponent<RectTransform>().anchoredPosition.x, 0f);
                    completedText.gameObject.SetActive(false);

                    specialRequisiteIdols.text = gameManager.specialLevelRequisites.idols.ToString();

                }

                specialLevelCompleted = GameDataController.gameData.specialLevelsCompleted[gameManager.specialLevelRequisites.levelId];
                //specialLevelCompleted = PlayerPrefs.GetInt("SpecialLevel" + gameManager.specialLevelRequisites.levelId, 0);
                if(specialLevelCompleted) starOnIcons[0].gameObject.SetActive(true);

            } else {

                completedText.text = "404 - NOT FOUND";

            }

        } else {

            //Write the score and idols requisites
            twoStarsScore.text = gameManager.twoStarsScore.ToString();
            twoStarsIdols.text = gameManager.twoStarsIdols.ToString();
            threeStarsScore.text = gameManager.threeStarsScore.ToString();
            threeStarsIdols.text = gameManager.threeStarsIdols.ToString();

            starsAchieved = new List<bool>();
            starsAchieved.Add(false); starsAchieved.Add(false); starsAchieved.Add(false);

            //Draw stars previously achieved if proceed
            int starsGained = GameDataController.gameData.stars[gameManager.worldId, gameManager.levelId];
            for (int i = 0; i < starsGained; i++) {

                //starOffIcons[i].gameObject.SetActive(false);
                starOnIcons[i].gameObject.SetActive(true);
                starsAchieved[i] = true;

            }

        }

    }

    public override void OnActivate() {

        totalScore = scoreController.totalScore;
        numIdols = gameManager.idolCounter;

        diamondModule.SetTotalDiamonds(GameDataController.gameData.diamondCurrency);

        //Check and set stars achievements
        if (GameManager.gameState == GameState.COMPLETED) gameManager.UpdateStars();
        
    }

    public override void OnAnimate() {

        if (GameManager.gameState != GameState.COMPLETED) {
            animated = true;
            return;
        }

        StartCoroutine(AnimateStarModule());

    }

    IEnumerator AnimateStarModule() {

        yield return new WaitForSeconds(1f);

        if (gameManager.specialLevel) {

            bool specialScoreReq;
            bool specialIdolReq;

            if (gameManager.specialLevelRequisites != null) {

                //bool specialLevelCompleted = GameDataController.gameData.specialLevelsCompleted[gameManager.specialLevelRequisites.levelId];

                //int specialLevelCompleted = PlayerPrefs.GetInt("SpecialLevel" + gameManager.specialLevelRequisites.levelId, 0);

                if (gameManager.specialLevelRequisites.score > 0 && gameManager.specialLevelRequisites.idols > 0) {

                    specialScoreReq = totalScore > gameManager.specialLevelRequisites.score;
                    specialIdolReq = numIdols >= gameManager.specialLevelRequisites.idols;

                    StartCoroutine(ColorText(specialRequisiteScore, specialScoreReq));
                    yield return StartCoroutine(ColorText(specialRequisiteIdols, specialIdolReq));

                    if (specialScoreReq && specialIdolReq) {
                        yield return StartCoroutine(StarAnimation(starOnIcons[0]));
                        //TODO CHECK IF YET ACHIEVED
                        if (!specialLevelCompleted) {
                            for (int i = 0; i < gameManager.specialLevelRequisites.diamondReward; i++) {
                                yield return StartCoroutine(FlyDiamond(diamondIcons[0]));
                            }

                        }
                    }

                } else if (gameManager.specialLevelRequisites.score > 0) {

                    specialScoreReq = totalScore > gameManager.specialLevelRequisites.score;

                    yield return StartCoroutine(ColorText(specialRequisiteScore, specialScoreReq));

                    if (specialScoreReq) {
                        yield return StartCoroutine(StarAnimation(starOnIcons[0]));
                        //TODO CHECK IF YET ACHIEVED
                        if (!specialLevelCompleted)
                            for (int i = 0; i < gameManager.specialLevelRequisites.diamondReward; i++) {
                                yield return StartCoroutine(FlyDiamond(diamondIcons[0]));
                            }
                    }

                } else if (gameManager.specialLevelRequisites.idols > 0) {

                    specialIdolReq = numIdols >= gameManager.specialLevelRequisites.idols;

                    yield return StartCoroutine(ColorText(specialRequisiteIdols, specialIdolReq));

                    if (specialIdolReq) {
                        yield return StartCoroutine(StarAnimation(starOnIcons[0]));
                        //TODO CHECK IF YET ACHIEVED
                        if (!specialLevelCompleted)
                            for (int i = 0; i < gameManager.specialLevelRequisites.diamondReward; i++) {
                                yield return StartCoroutine(FlyDiamond(diamondIcons[0]));
                            }
                    }

                } else {

                    yield return StartCoroutine(ColorText(completedText, true));
                    yield return StartCoroutine(StarAnimation(starOnIcons[0]));
                    //TODO CHECK IF YET ACHIEVED
                    if (!specialLevelCompleted)
                        for (int i = 0; i < gameManager.specialLevelRequisites.diamondReward; i++) {
                            yield return StartCoroutine(FlyDiamond(diamondIcons[0]));
                        }

                }

            } else {

                completedText.text = "404 - NOT FOUND";

            }

            yield break;

        }

        //1-Star
        yield return StartCoroutine(ColorText(completedText, true));
        yield return StartCoroutine(StarAnimation(starOnIcons[0]));
        if (!starsAchieved[0]) yield return StartCoroutine(FlyDiamond(diamondIcons[0]));

        yield return new WaitForSeconds(1f);

        //2-Star
        bool twoStarsScoreReq = totalScore > gameManager.twoStarsScore;
        bool twoStarsIdolReq = numIdols >= gameManager.twoStarsIdols;

        StartCoroutine(ColorText(twoStarsScore, twoStarsScoreReq));
        yield return StartCoroutine(ColorText(twoStarsIdols, twoStarsIdolReq));

        if (twoStarsScoreReq && twoStarsIdolReq) {
            yield return StartCoroutine(StarAnimation(starOnIcons[1]));
            if(!starsAchieved[1]) yield return StartCoroutine(FlyDiamond(diamondIcons[1]));
        }

        yield return new WaitForSeconds(1f);

        //3-Star
        bool threeStarsScoreReq = totalScore > gameManager.threeStarsScore;
        bool threeStarsIdolReq = numIdols >= gameManager.threeStarsIdols;

        StartCoroutine(ColorText(threeStarsScore, threeStarsScoreReq));
        yield return StartCoroutine(ColorText(threeStarsIdols, threeStarsIdolReq));

        if (threeStarsScoreReq && threeStarsIdolReq) {
            yield return StartCoroutine(StarAnimation(starOnIcons[2]));
            if (!starsAchieved[2]) yield return StartCoroutine(FlyDiamond(diamondIcons[2]));
        }

        animated = true;

    }

    IEnumerator StarAnimation(Image star) {

        star.gameObject.SetActive(true);

        float targetSize = star.rectTransform.sizeDelta.x;
        Color32 targetColor = star.color;

        float initSize = targetSize * 16f;
        Color32 initColor = new Color32(targetColor.r, targetColor.g, targetColor.b, 0);

        star.color = initColor;
        star.rectTransform.sizeDelta = new Vector2(initSize, initSize);

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < starAnimationTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / starAnimationTime;

            star.color = Color32.Lerp(initColor, targetColor, perc);
            star.rectTransform.sizeDelta = Vector2.Lerp(new Vector2(initSize, initSize), new Vector2(targetSize, targetSize), perc);

            yield return null;

        }

        star.color = targetColor;
        star.rectTransform.sizeDelta = new Vector2(targetSize, targetSize);

    }

    IEnumerator ColorText(Text text, bool achieved) {

        Color32 initColor = text.color;
        Color32 targetColor = (achieved) ? Color.green : Color.red;

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < textColoringTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / textColoringTime;

            text.color = Color32.Lerp(initColor, targetColor, perc);

            yield return null;

        }


    }

    IEnumerator FlyDiamond(RectTransform diamond) {

        diamond.gameObject.SetActive(true);

        Vector3 initPos = diamond.position;
        Vector3 trajectory = targetDiamondFly.position - initPos;

        float timer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while (timer < diamondFlyTime) {

            timer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = timer / diamondFlyTime;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            diamond.position = initPos + (perc * trajectory);

            yield return null;

        }

        diamond.gameObject.SetActive(false);

        diamond.position = initPos;

        diamondModule.AddToCounter();

    }

}
