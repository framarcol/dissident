﻿using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour {

    //GameManager gameManager;

    //public GameObject inGameHUD;
    //CanvasGroup inGameCanvas;

    //DeathUI deathUI;
    //WinUI winUI;
    
    CommonStateScreenManager commonStateScreenManager;

	// Use this for initialization
	void Start () {

        //Get the Game Manager and subscribe to events
        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        GameManager.OnGamePaused += TriggerCommonStateScreen;
        //GameManager.OnGameResumed += TriggerGameResumed;
        GameManager.OnGameOver += TriggerCommonStateScreen;
        GameManager.OnGameCompleted += TriggerCommonStateScreen;

        //inGameCanvas = inGameHUD.GetComponent<CanvasGroup>();

        commonStateScreenManager = GetComponentInChildren<CommonStateScreenManager>();
        //deathUI = GetComponentInChildren<DeathUI>(true);
        //winUI = GetComponentInChildren<WinUI>(true);

	}

    public void TriggerCommonStateScreen() {

        commonStateScreenManager.TriggerCommonStateScreen();

    }

    /*public void TriggerGamePaused() {

        
        
    }

    public void TriggerGameOver() {

        //deathUI.gameObject.SetActive(true);

        //inGameCanvas.interactable = false;

        commonStateScreenManager.TriggerCommonStateScreen();

    }

    public void TriggerGameCompletedUI() {

        //winUI.gameObject.SetActive(true);

        commonStateScreenManager.TriggerCommonStateScreen();

    }*/

}
