﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoUI : MonoBehaviour {

    public float followFactor = 50f;

    Text ammoCount;

    BasicShoot basicShoot;

    int onFontSize;
    int offFontSize;

    Color onColor;
    Color offColor;

    GameManager gameManager;

    [HideInInspector]
    public int ammoDisplayOption;

    void Awake() {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        if (gameManager.specialLevel && gameManager.specialLevelID == 1) {
            ammoDisplayOption = 0;
        } else {
            ammoDisplayOption = PlayerPrefs.GetInt(GameManager.ammoDisplayPref, 0);
        }

        if (ammoDisplayOption != 0)
        {
            gameObject.SetActive(false);
        }
        
    }

    void Start () {
        
        basicShoot = GameObject.FindGameObjectWithTag("Player").GetComponent<BasicShoot>();

        ammoCount = GetComponent<Text>();

        if (basicShoot.ammoPin != null) {

            transform.position = Camera.main.WorldToScreenPoint(basicShoot.ammoPin.position);

        }

        onColor = ammoCount.color;
        offColor = Color.red;

        onFontSize = ammoCount.fontSize;
        offFontSize = 60;

    }
	
	void Update () {

        if(GameManager.gameState != GameState.RUNNING && GameManager.gameState != GameState.PAUSED) {

            gameObject.SetActive(false);
            return;

        }

        int actualAmmo = basicShoot.actualAmmo;

        if (actualAmmo <= 0) {

            ammoCount.color = offColor;
            ammoCount.fontSize = offFontSize;

        } else {

            ammoCount.color = onColor;
            ammoCount.fontSize = onFontSize;

        }

        ammoCount.text = actualAmmo.ToString();

        if(basicShoot.ammoPin != null) {

            transform.position = Vector3.Lerp(transform.position, Camera.main.WorldToScreenPoint(basicShoot.ammoPin.position), followFactor * Time.deltaTime);

        }

	}

    public void Show() {

        if (ammoDisplayOption == 0) gameObject.SetActive(true);

    }

}
