﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoUITopBar : MonoBehaviour {
    
    public Text ammoCount;

    public Slider ammoBar;

    BasicShoot basicShoot;

    GameManager gameManager;
    
	void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        int ammoDisplayOption;

        if (gameManager.specialLevel && gameManager.specialLevelID == 1) {
            ammoDisplayOption = 0;
        } else {
            ammoDisplayOption = PlayerPrefs.GetInt(GameManager.ammoDisplayPref, 0);
        }

        if (ammoDisplayOption != 1) {
            gameObject.SetActive(false);
            return;
        }


        basicShoot = GameObject.FindGameObjectWithTag("Player").GetComponent<BasicShoot>();

        ammoBar.maxValue = basicShoot.maxAmmo;

    }
	
	void Update () {

        int actualAmmo = basicShoot.actualAmmo;

        ammoCount.text = actualAmmo.ToString();

        ammoBar.value = actualAmmo;

	}

}
