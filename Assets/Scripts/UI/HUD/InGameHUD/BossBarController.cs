﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossBarController : MonoBehaviour {

    public int initLifes;

    public GameObject barFillParent;
    public GameObject barBackgroundParent;

    List<GameObject> barFills;
    List<GameObject> barBackgrounds;

    int fillIndex;

	// Use this for initialization
	void Start () {

        barFills = new List<GameObject>();
        barBackgrounds = new List<GameObject>();

        for(int i = 0; i < barFillParent.transform.childCount; i++) {

            GameObject background = barBackgroundParent.transform.GetChild(i).gameObject;
            GameObject fill = barFillParent.transform.GetChild(i).gameObject;

            if (i >= initLifes) {

                background.SetActive(false);
                fill.SetActive(false);

            }

            barFills.Add(fill);
            barBackgrounds.Add(background);

        }

        fillIndex = initLifes;

	}
	
    public void HitBar(int hits = 1) {
        
        for(int i = 0; i < hits; i++) {

            if (fillIndex < 1) return;

            barFills[fillIndex - 1].SetActive(false);
            fillIndex--;

        }

    }

    public void ResetBar() {

        if (barFills == null) return;

        for (int i = 0; i < initLifes; i++) {

            barFills[i].SetActive(true);

        }
        fillIndex = initLifes;

    }

    public void ResetBar(int numLifes) {

        if (barFills == null) return;

        if (numLifes > 10) numLifes = 10;

        for (int i = 0; i < barFills.Count; i++) {
            
            if (i >= numLifes) {

                barFills[i].SetActive(false);
                barBackgrounds[i].SetActive(false);

            } else {

                barFills[i].SetActive(true);
                barBackgrounds[i].SetActive(true);

            }
            
        }

        fillIndex = numLifes;

    }

}
