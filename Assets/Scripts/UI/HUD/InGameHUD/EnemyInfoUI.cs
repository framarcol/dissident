﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyInfoUI : MonoBehaviour {

    public GameObject enemyHealthBarPrefab;

    Queue<Slider> enemyHealthBarQueue = new Queue<Slider>();
    
	void Awake () {
	
        //Instantiate and cache some health bars at start
        for(int i = 0; i < 10; i++) {

            Slider bar = Instantiate(enemyHealthBarPrefab).GetComponent<Slider>();
            bar.transform.SetParent(transform);
            bar.gameObject.SetActive(false);

            enemyHealthBarQueue.Enqueue(bar);

        }

	}

    public Slider GetEnemyHealthBar() {

        Slider bar = enemyHealthBarQueue.Dequeue();
        bar.gameObject.SetActive(true);

        return bar;

    }

    public void DisposeEnemyHealthBar(Slider bar) {

        bar.gameObject.SetActive(false);
        bar.value = 0f;

        enemyHealthBarQueue.Enqueue(bar);

    }

}
