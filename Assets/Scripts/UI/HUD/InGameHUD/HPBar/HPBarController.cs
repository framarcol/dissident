﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HPBarController : MonoBehaviour {

    public GameObject backgroundPieceLeft;
    public GameObject backgroundPieceMid;
    public GameObject backgroundPieceRight;

    public GameObject hpFillLeft;
    public GameObject hpFillMid;
    public GameObject hpFillRight;

    List<GameObject> hpFilling = new List<GameObject>();
    int activeIndex;

    Queue<GameObject> leftFillPool = new Queue<GameObject>();
    Queue<GameObject> midFillPool = new Queue<GameObject>();
    Queue<GameObject> rightFillPool = new Queue<GameObject>();

    PlayerHealth playerHealth;

    GameObject dangerFill;

	void Start () {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

        //Subscribe to damage event
        playerHealth.OnPlayerDamaged += DamageUpdate;
        playerHealth.OnPlayerHealed += HealedUpdate;

        //Populate filling pools
        for(int i = 0; i < 5; i++) {

            GameObject fillLeft = Instantiate(hpFillLeft, transform) as GameObject;
            fillLeft.SetActive(false);
            leftFillPool.Enqueue( fillLeft );

            GameObject fillMid = Instantiate(hpFillMid, transform) as GameObject;
            fillMid.SetActive(false);
            midFillPool.Enqueue( fillMid );

            GameObject fillRight = Instantiate(hpFillRight, transform) as GameObject;
            fillRight.SetActive(false);
            rightFillPool.Enqueue( fillRight );

        }

        //Draw hp bar
        DrawHPBar();

	}

    public void DamageUpdate(int lifes) {

        //If 0 lifes come, then kill the bar
        if (lifes == 0) lifes = activeIndex + 1;

        for(int i = 0; i < lifes; i++) {

            //TEST// TODO->Animate the damage
            hpFilling[activeIndex].GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            hpFilling[activeIndex].SetActive(false);

            StartCoroutine(ThrowPiece(activeIndex));
            ////

            activeIndex--;

        }

        if(activeIndex == 0) {

            StartCoroutine(WarningFill());
            
            dangerFill = leftFillPool.Peek();
            if (dangerFill != null) dangerFill.GetComponent<Image>().color = Color.red;

        }

    }

    public void HealedUpdate(int lifes) {

        for (int i = 0; i < lifes; i++) {

            activeIndex++;

            //TEST// TODO->Animate the damage
            //hpFilling[activeIndex].SetActive(true);
            StartCoroutine(ResetPiece(activeIndex));
            
        }
        
        hpFilling[0].GetComponent<Image>().color = Color.white;
        hpFilling[0].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        if (dangerFill != null) dangerFill.GetComponent<Image>().color = Color.white;

    }

    IEnumerator ThrowPiece(int index) {

        //GameObject piece = hpFilling[index];
        GameObject piece;

        if (index == 0) piece = leftFillPool.Dequeue();
        else if (index == playerHealth.maxLife - 1) piece = rightFillPool.Dequeue();
        else piece = midFillPool.Dequeue();

        Rigidbody2D rb = piece.GetComponent<Rigidbody2D>();
        Image image = piece.GetComponent<Image>();

        piece.transform.position = hpFilling[index].transform.position;
        piece.transform.rotation = Quaternion.identity;
        piece.SetActive(true);

        //Vector2 startPos = piece.transform.position;

        rb.isKinematic = false;
        rb.AddForce(new Vector2(Random.Range(-50f, 50f), -25f), ForceMode2D.Impulse);
        rb.AddTorque(Random.Range(-5f, 5f), ForceMode2D.Impulse);

        float timer = 0f;

        while(timer < 1f) {

            timer += Time.deltaTime;

            float perc = timer / 1f;

            image.color = new Color(image.color.r,image.color.g,image.color.b, 1 - perc);

            yield return null;

        }
        
        rb.isKinematic = true;
        image.color = new Color(1,1,1,1);
        //piece.transform.position = startPos;
        piece.SetActive(false);

        if (index == 0) leftFillPool.Enqueue(piece);
        else if (index == playerHealth.maxLife - 1) rightFillPool.Enqueue(piece);
        else midFillPool.Enqueue(piece);

    }

    IEnumerator ResetPiece(int index) {

        GameObject piece = hpFilling[index];

        piece.SetActive(true);

        RectTransform rectTransform = piece.GetComponent<RectTransform>();

        Vector3 initScale = new Vector3(0,0,0);
        Vector3 targetScale = new Vector3(1,1,1);

        Vector3 diffScale = targetScale - initScale;

        float timer = 0f;
        while (timer < 0.25f) {

            timer += Time.deltaTime;

            float perc = timer / 0.25f;

            rectTransform.localScale = initScale + (perc * diffScale);

            yield return null;

        }

        rectTransform.localScale = targetScale;

    }

    IEnumerator WarningFill() {

        if (activeIndex != 0) yield break;

        hpFilling[activeIndex].GetComponent<Image>().color = Color.red;

        float timer = 0f;
        while (activeIndex == 0) {

            timer += Time.deltaTime;

            if (timer > 1f) timer = 0f;

            float perc = timer / 1f;

            perc = Mathf.Sin( perc * Mathf.PI );

            hpFilling[activeIndex].GetComponent<RectTransform>().localScale = new Vector3(1 + (perc * 0.2f), 1 + (perc * 0.2f), 1 + (perc * 0.2f));

            yield return null;

        }
        
    }

    void DrawHPBar() {

        int lifes = playerHealth.maxLife;

        float width = backgroundPieceMid.GetComponent<RectTransform>().rect.width;
        
        float xInit;
        
        if (lifes % 2 != 0) {

            xInit = -Mathf.Floor(lifes / 2f) * width;

        } else {

            xInit = -(((lifes / 2f) - 1f) * width + (width / 2f));

        }

        float xPos = xInit;

        DrawPiece(backgroundPieceLeft, xPos);
        hpFilling.Add(DrawPiece(hpFillLeft, xPos));
        for (int i = 1; i < lifes - 1; i++) {

            xPos += width;
            DrawPiece(backgroundPieceMid, xPos);
            /*if(playerHealth.lifes > i)*/ hpFilling.Add(DrawPiece(hpFillMid, xPos));

        }
        xPos += width;
        DrawPiece(backgroundPieceRight, xPos);
        /*if (playerHealth.lifes == lifes)*/ hpFilling.Add(DrawPiece(hpFillRight, xPos));

        activeIndex = lifes - 1;

    }

    GameObject DrawPiece(GameObject piece, float xPos) {

        GameObject pieceInstance = Instantiate(piece, transform) as GameObject;

        RectTransform rectTransform = pieceInstance.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(xPos, 0f);
        rectTransform.localScale = new Vector3(1,1,1);

        return pieceInstance;

    }

}
