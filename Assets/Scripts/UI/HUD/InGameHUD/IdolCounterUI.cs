﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class IdolCounterUI : MonoBehaviour {

    public Text idolCount;

    public RectTransform idolCountAnim;
    public Image idolIcon;

    public RectTransform idolTargetPos;

    public GameObject idolSprite;
    public List<Sprite> idolWorldIcons;

    GameManager gameManager;

    //float animTimer;

	void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        gameManager.OnIdolTaken += UpdateIdolCounter;

        idolSprite.GetComponent<Image>().sprite = idolWorldIcons[gameManager.worldId];

        idolCount.text = gameManager.idolCounter.ToString() + "/6";

    }

    public void UpdateIdolCounter() {

        IdolItem idol = gameManager.levelIdols[gameManager.idolCounter - 1];

        idolCountAnim.position = Camera.main.WorldToScreenPoint(idol.transform.position);
        idolIcon.sprite = idol.idolIcon;

        idolCountAnim.gameObject.SetActive(true);

        StartCoroutine(IdolAnimation());
        
    }

    IEnumerator IdolAnimation() {

        Vector2 startPosition = idolCountAnim.position;
        Vector2 trajectory = idolTargetPos.position - idolCountAnim.position;

        float animTimer = 0f;
        float lastTime = Time.realtimeSinceStartup;

        while(animTimer <= 1f) {

            animTimer += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float perc = animTimer / 1f;
            perc = Mathf.Sin(perc * Mathf.PI * 0.5f);

            Vector2 actualPos = perc * trajectory;

            idolCountAnim.position = startPosition + actualPos;

            yield return null;

        }

        idolCountAnim.position = idolTargetPos.position;

        idolCountAnim.gameObject.SetActive(false);
        idolCount.text = gameManager.idolCounter.ToString() + "/6";

        //animTimer = 0f;

    }

}
