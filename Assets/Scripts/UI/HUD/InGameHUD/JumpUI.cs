﻿using UnityEngine;
using System.Collections;

public class JumpUI : MonoBehaviour {
    
    PlayerMovementController playerMovementController;
    
	void Start () {

        int jumpOption = PlayerPrefs.GetInt(GameManager.JumpOptionPref, 0);

        if(jumpOption == 0) {

            gameObject.SetActive(false);
            return;

        }

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();

        RectTransform jumpTransform = GetComponent<RectTransform>();

        int handOption = PlayerPrefs.GetInt(GameManager.HandPref, 0);

        if(handOption != 0) {

            float xAnchoredPosition = jumpTransform.anchoredPosition.x;

            jumpTransform.anchorMin = new Vector2(0, 0);
            jumpTransform.anchorMax = new Vector2(0, 0);

            jumpTransform.anchoredPosition = new Vector2(-xAnchoredPosition, jumpTransform.anchoredPosition.y);

        }
    }
	
    public void Jump() {

        if(!playerMovementController.jumping)
            playerMovementController.Jump();

    }

    public void MoveRight() {

        playerMovementController.MoveRight();

    }

    public void MoveLeft() {

        playerMovementController.MoveLeft();

    }

}
