﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseUI : MonoBehaviour {

    //public GameObject pausePanel;
    //public Toggle touchToggle;

    //public GameObject countDown;

    //[HideInInspector]
    //public bool paused = false;

    ScreenOrientation actualOrientation;
    
    void Start() {

        //touchToggle.isOn = !PlayerMovementController.useAccelerometer;

        actualOrientation = Screen.orientation;

    }

    void Update() {

        if (Screen.orientation != actualOrientation) {

            if(GameManager.gameState != GameState.PAUSED) PauseResumeGame();

        }

        actualOrientation = Screen.orientation;

    }

    /*public void PauseResumeGameControl(bool value) {

        paused = value;

        if (paused) {

            if (pausePanel != null) pausePanel.SetActive(true);
            Time.timeScale = 0f;

        } else {

            if (pausePanel != null) pausePanel.SetActive(false);
            
            countDown.SetActive(true);

            GetComponent<Toggle>().enabled = false;

            StartCoroutine(CountDown());

        }

    }*/

    void OnApplicationPause(bool paused) {

        if (paused) PauseResumeGame();

    }

    public void PauseResumeGame() {

        if (GameManager.gameState == GameState.RUNNING) {

            GameManager.PauseGame();

            //if(pausePanel != null) pausePanel.SetActive(true);
            
            
        } /*else if(GameManager.gameState == GameState.PAUSED) {

            if (pausePanel != null) pausePanel.SetActive(false);

            if (useCountDown) {

                countDown.SetActive(true);

                GetComponent<Toggle>().enabled = false;

                StartCoroutine(CountDown());

            } else {

                GameManager.ResumeGame();

            }

        }*/

    }

    /*private IEnumerator CountDown() {

        yield return new WaitForRealSeconds(1f);
        countDown.GetComponentInChildren<Text>().text = (2).ToString();
        yield return new WaitForRealSeconds(1f);
        countDown.GetComponentInChildren<Text>().text = (1).ToString();
        yield return new WaitForRealSeconds(1f);

        GameManager.ResumeGame();

        countDown.GetComponentInChildren<Text>().text = (3).ToString();
        countDown.SetActive(false);

        GetComponent<Toggle>().enabled = true;
        
        yield return null;

    }*/

}
