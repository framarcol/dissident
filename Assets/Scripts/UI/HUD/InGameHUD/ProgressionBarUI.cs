﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ProgressionBarUI : MonoBehaviour {

    public float actualProgression = 0f;

    public float maxLength;

    public RectTransform checkpointOff;
    public RectTransform checkpointOn;
    
    Slider bar;

    PlayerMovementController playerMovementController;
    PlayerHealth playerHealth;
    GameManager gameManager;

    List<RectTransform> checkpointOffList = new List<RectTransform>();
    List<RectTransform> checkpointOnList = new List<RectTransform>();
    int markerIndex = 0;

	// Use this for initialization
	void Start () {

        bar = GetComponent<Slider>();

        bar.value = 0;
        bar.maxValue = maxLength;

        playerMovementController = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovementController>();
        playerHealth = playerMovementController.GetComponentInChildren<PlayerHealth>();

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        DrawCheckpointMarkers();

        InitCheckpointMarkers();

	}
	
	// Update is called once per frame
	void Update () {

        if (!playerHealth.dead) {

            actualProgression += playerMovementController.lerpedSpeed * Time.deltaTime;

            bar.value = actualProgression;

        }

	}

    public void InitCheckpointMarkers() {
        
        if(GameManager.activeCheckpoint != -1) {

            for(int i = 0; i < GameManager.activeCheckpoint + 1; i++) {

                ActivateCheckpointMarker();

            }

        }

    }

    public void ActivateCheckpointMarker() {

        checkpointOffList[markerIndex].gameObject.SetActive(false);
        checkpointOnList[markerIndex].gameObject.SetActive(true);

        markerIndex++;

    }

    private void DrawCheckpointMarkers() {

        if(gameManager != null) {

            for(int i = 0; i < gameManager.checkpoints.Count; i++) {

                CheckpointArea checkpoint = gameManager.checkpoints[i];

                if (checkpoint.distanceFromStart == 0) continue;

                float perc = checkpoint.distanceFromStart / maxLength;

                //Debug.Log(perc);

                RectTransform checkpointOffInstance = Instantiate(checkpointOff, bar.handleRect.parent, true) as RectTransform;
                RectTransform checkpointOnInstance = Instantiate(checkpointOn, bar.handleRect.parent, true) as RectTransform;

                //checkpointOffInstance.SetParent(bar.handleRect.parent);
                //checkpointOnInstance.SetParent(bar.handleRect.parent);

                checkpointOffInstance.gameObject.SetActive(true);

                checkpointOffInstance.anchorMin = new Vector2(perc, 0);
                checkpointOffInstance.anchorMax = new Vector2(perc, 1);
                checkpointOffInstance.anchoredPosition = new Vector2(0, checkpointOffInstance.anchoredPosition.y);

                checkpointOnInstance.anchorMin = new Vector2(perc, 0);
                checkpointOnInstance.anchorMax = new Vector2(perc, 1);
                checkpointOnInstance.anchoredPosition = new Vector2(0, checkpointOnInstance.anchoredPosition.y);

                checkpointOffList.Add(checkpointOffInstance);
                checkpointOnList.Add(checkpointOnInstance);

            }

        }

    }

}
