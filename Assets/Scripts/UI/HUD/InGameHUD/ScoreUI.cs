﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreUI : MonoBehaviour {

    public Text scoreText;
    public Text scoreMultiplier;

    ScoreController scoreController;

	// Use this for initialization
	void Start () {

        scoreController = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>();

        if (scoreController == null) gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

        scoreText.text = Mathf.FloorToInt(scoreController.totalScore).ToString();
        scoreMultiplier.text = scoreController.multiplier.ToString();

	}

}
