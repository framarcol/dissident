﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialSpawnUI : MonoBehaviour {

    public GameObject tutorialPanel;

    public string tutorialMessage;

    public int blockNumber = -1;
    public int lineNumber = -1;

    Text tutorialText;

    bool entered = false;

    //PauseUI pause;

    CanvasGroup inGameHUD;
    
	void Start () {

        tutorialText = tutorialPanel.GetComponentInChildren<Text>();

        inGameHUD = GameObject.FindGameObjectWithTag("HUD").transform.Find("InGameHUD").GetComponent<CanvasGroup>();

	}

    public void UnpauseWithNoCountDown() {

        GameManager.gameState = GameState.RUNNING;
        Time.timeScale = 1f;

        inGameHUD.interactable = true;

    }

    void OnTriggerEnter(Collider other) {

        if (!entered && other.tag == "Player") {

            entered = true;

            tutorialPanel.SetActive(true);

            if (blockNumber == -1 || lineNumber == -1) {

                tutorialText.text = tutorialMessage;

            } else {

                string tut = SeleccionaIdiomas.gameText[blockNumber, lineNumber];

                if (string.IsNullOrEmpty(tut))
                    tutorialText.text = tutorialMessage;
                else
                    tutorialText.text = tut;

            }
            
            GameManager.gameState = GameState.PAUSED;
            Time.timeScale = 0f;

            inGameHUD.interactable = false;

            //pause.PauseResumeGame();
            //pause.pausePanel.SetActive(false);
            
        }

    }

}
