﻿using UnityEngine;
using System.Collections;

public class DeathUI : MonoBehaviour {

    public GameObject[] UIInGameToDisable;

    void OnEnable() {

        for (int i = 0; i < UIInGameToDisable.Length; i++) {

            UIInGameToDisable[i].SetActive(false);

        }

        //Get player score before death
        float totalScore = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>().totalScore;
        //Calculate gold earnings until death
        uint earningGold = (uint)(GameManager.GoldConversionRate * totalScore);
        GameManager.AddGold(earningGold);

    }

}
