﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthUI : MonoBehaviour {

    //The text used for displaying the health
    public Text hpText;

    PlayerHealth playerHealth;

	void Start () {

        playerHealth = FindObjectOfType<PlayerHealth>();

	}
	void Update () {

        //Update the text with the player's actual lifes
        hpText.text = playerHealth.lifes.ToString();

	}

}
