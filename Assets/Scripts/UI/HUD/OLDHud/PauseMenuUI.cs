﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenuUI : MonoBehaviour {

	public void TouchMotionToggle(bool value) {

        PlayerMovementController.useAccelerometer = !value;

    }

    public void ExitButton() {

        Time.timeScale = 1f;

        LoadSc.nextScene = 0;
        SceneManager.LoadScene("LoadScreen");

    }

    public void DeleteDataButton() {

        GameDataController.DeleteGameData();

    }

}
