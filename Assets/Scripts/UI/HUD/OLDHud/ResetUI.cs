﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetUI : MonoBehaviour {

    public void ResetButton() {

        Time.timeScale = 1f;

        //Load the actual scene again
        LoadSc.nextScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene("LoadScreen");
         

    }

}
