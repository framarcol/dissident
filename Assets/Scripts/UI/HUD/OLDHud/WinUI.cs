﻿using UnityEngine;
using UnityEngine.UI;

public class WinUI : MonoBehaviour {

    public GameObject[] UIInGameToDisable;

    public Text scoreText;
    public Text recordText;

    GameManager gameManager;

    void OnEnable() {

        int length = UIInGameToDisable.Length;
        for (int i = 0; i < length; i++) {

            UIInGameToDisable[i].SetActive(false);

        }

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        
        //Check player score and his record
        float totalScore = GameObject.FindGameObjectWithTag("ScoreController").GetComponent<ScoreController>().totalScore;
        float recordScore = GameDataController.gameData.playerRecords[gameManager.worldId, gameManager.levelId];
        if (recordScore < totalScore) GameManager.SetNewRecord(gameManager.worldId, gameManager.levelId, (uint)totalScore);
        //Show them
        scoreText.text = totalScore.ToString();
        recordText.text = GameDataController.gameData.playerRecords[gameManager.worldId, gameManager.levelId].ToString();

        //Calculate gold earnings
        uint earningGold = (uint)(GameManager.GoldConversionRate * totalScore);
        GameManager.AddGold(earningGold);

        //Calculate stars
        gameManager.CheckStars();

    }

}
