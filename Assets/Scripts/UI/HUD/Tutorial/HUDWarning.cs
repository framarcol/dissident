﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HUDWarning : MonoBehaviour {

    public GameObject tiltWarningPrefab;
    public GameObject shakeWarningPrefab;

    public GameObject jumpWarningPrefab;

    public GameObject tapWarningPrefab;
    public GameObject tapDangerWarningPrefab;
    public GameObject critWarningPrefab;

    public GameObject pickupWarningPrefab;
    public GameObject healthPickupWarningPrefab;
    public GameObject ammoPickupWarningPrefab;

    public GameObject focusWarningPrefab;

    public int poolSize = 5;

    //POOLS
    Queue<GameObject> tiltWarningPool;
    Queue<GameObject> shakeWarningPool;

    Queue<GameObject> jumpWarningPool;

    Queue<GameObject> tapWarningPool;
    Queue<GameObject> tapDangerWarningPool;
    Queue<GameObject> critWarningPool;

    Queue<GameObject> pickupWarningPool;
    Queue<GameObject> healthPickupWarningPool;
    Queue<GameObject> ammoPickupWarningPool;

    Queue<GameObject> focusWarningPool;
    
	void Awake () {

        PopulatePool(ref tiltWarningPool, tiltWarningPrefab);
        PopulatePool(ref shakeWarningPool, shakeWarningPrefab);
        PopulatePool(ref jumpWarningPool, jumpWarningPrefab);
        PopulatePool(ref tapWarningPool, tapWarningPrefab);
        PopulatePool(ref tapDangerWarningPool, tapDangerWarningPrefab);
        PopulatePool(ref critWarningPool, critWarningPrefab);
        PopulatePool(ref pickupWarningPool, pickupWarningPrefab);
        PopulatePool(ref healthPickupWarningPool, healthPickupWarningPrefab);
        PopulatePool(ref ammoPickupWarningPool, ammoPickupWarningPrefab);
        PopulatePool(ref focusWarningPool, focusWarningPrefab);
        
    }

    void PopulatePool(ref Queue<GameObject> pool, GameObject prefab) {

        if (prefab == null) return;

        pool = new Queue<GameObject>();

        for (int i = 0; i < poolSize; i++) {

            GameObject instance = Instantiate(prefab, transform) as GameObject;

            Vector3 anchoredPosition = prefab.GetComponent<RectTransform>().anchoredPosition;
            instance.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;

            instance.SetActive(false);

            pool.Enqueue(instance);

        }

    }

    public GameObject GetTiltWarning() {

        if (tiltWarningPool.Count <= 0) return null;
        else return tiltWarningPool.Dequeue();

    }

    public GameObject GetShakeWarning() {

        if (shakeWarningPool.Count <= 0) return null;
        else return shakeWarningPool.Dequeue();

    }

    public GameObject GetJumpWarning() {

        if (jumpWarningPool.Count <= 0) return null;
        else return jumpWarningPool.Dequeue();

    }

    public GameObject GetTapWarning() {

        if (tapWarningPool.Count <= 0) return null;
        else return tapWarningPool.Dequeue();

    }

    public GameObject GetTapDangerWarning() {

        if (tapDangerWarningPool.Count <= 0) return null;
        else return tapDangerWarningPool.Dequeue();

    }

    public GameObject GetCritWarning() {

        if (critWarningPool.Count <= 0) return null;
        else return critWarningPool.Dequeue();

    }

    public GameObject GetPickupWarning() {

        if (pickupWarningPool.Count <= 0) return null;
        else return pickupWarningPool.Dequeue();

    }

    public GameObject GetHealthPickupWarning() {

        if (healthPickupWarningPool.Count <= 0) return null;
        else return healthPickupWarningPool.Dequeue();

    }

    public GameObject GetAmmoPickupWarning() {

        if (ammoPickupWarningPool.Count <= 0) return null;
        else return ammoPickupWarningPool.Dequeue();

    }

    public GameObject GetFocusWarning() {

        if (focusWarningPool.Count <= 0) return null;
        else return focusWarningPool.Dequeue();

    }
    
    public void DisposeTiltWarning(GameObject tiltWarning) {

        if (tiltWarning != null) {
            tiltWarning.SetActive(false);
            tiltWarningPool.Enqueue(tiltWarning);
        }

    }

    public void DisposeShakeWarning(GameObject shakeWarning) {

        if (shakeWarning != null) {
            shakeWarning.SetActive(false);
            tiltWarningPool.Enqueue(shakeWarning);
        }

    }

    public void DisposeJumpWarning(GameObject jumpWarning) {

        if (jumpWarning != null) {
            jumpWarning.SetActive(false);
            jumpWarningPool.Enqueue(jumpWarning);
        }

    }

    public void DisposeTapWarning(GameObject tapWarning) {

        if (tapWarning != null) {
            tapWarning.SetActive(false);
            tapWarningPool.Enqueue(tapWarning);
        }

    }

    public void DisposeTapDangerWarning(GameObject tapDangerWarning) {

        if (tapDangerWarning != null) {
            tapDangerWarning.SetActive(false);
            tapDangerWarningPool.Enqueue(tapDangerWarning);
        }

    }

    public void DisposeCritWarning(GameObject critWarning) {

        if (critWarning != null) {
            critWarning.SetActive(false);
            critWarningPool.Enqueue(critWarning);
        }

    }

    public void DisposePickupWarning(GameObject pickupWarning) {

        if (pickupWarning != null) {
            pickupWarning.SetActive(false);
            pickupWarningPool.Enqueue(pickupWarning);
        }

    }

    public void DisposeHealthPickupWarning(GameObject healthPickupWarning) {

        if (healthPickupWarning != null) {
            healthPickupWarning.SetActive(false);
            healthPickupWarningPool.Enqueue(healthPickupWarning);
        }

    }

    public void DisposeAmmoPickupWarning(GameObject ammoPickupWarning) {

        if (ammoPickupWarning != null) {
            ammoPickupWarning.SetActive(false);
            ammoPickupWarningPool.Enqueue(ammoPickupWarning);
        }

    }

    public void DisposeFocusWarning(GameObject focusWarning) {

        if (focusWarning != null) {
            //focusWarning.GetComponent<Animator>().Rebind();
            focusWarning.SetActive(false);
            focusWarningPool.Enqueue(focusWarning);
        }

    }

}
