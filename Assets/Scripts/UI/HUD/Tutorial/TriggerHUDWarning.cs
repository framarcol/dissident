﻿using UnityEngine;
using System.Collections;

public enum WarningType { Tilt, Shake, Jump, Tap, DangerTap, Crit, Pickup, HealthPickup, AmmoPickup }

public class TriggerHUDWarning : MonoBehaviour {

    public WarningType warningType;

    public Transform anchor;

    public GameObject hudSignal;

    public float warningTime = 1.5f;
    float timer = 0f;

    public bool useFocus;

    HUDWarning hudWarning;

    GameObject warningInstance;
    GameObject focusInstance;

    Entity entity;

    bool entered;

	void Start () {

        hudWarning = GameObject.FindGameObjectWithTag("HUD").GetComponentInChildren<HUDWarning>();

	}
	
	void Update () {
	
        if(warningInstance != null) {

            timer += Time.deltaTime;

            if (anchor != null) {

                warningInstance.transform.position = Camera.main.WorldToScreenPoint(anchor.position);
                
                if (useFocus && focusInstance != null) {

                    focusInstance.transform.position = warningInstance.transform.position;

                }

                if (entity != null && entity.lifes <= 0) {
                    DisposeWarning(warningInstance);
                    warningInstance = null;
                    if(useFocus && focusInstance != null) hudWarning.DisposeFocusWarning(focusInstance);

                    if (hudSignal != null) hudSignal.SetActive(false);

                    timer = 0f;
                    return;

                }

            }

            if(timer > warningTime) {

                DisposeWarning(warningInstance);
                warningInstance = null;
                if (useFocus && focusInstance != null) hudWarning.DisposeFocusWarning(focusInstance);

                if (hudSignal != null) hudSignal.SetActive(false);

                timer = 0f;

            }
            
        }

	}

    void OnTriggerEnter(Collider other) {
        
        if(!entered && other.tag == "Player") {

            entered = true;

            if(anchor != null) {

                entity = anchor.GetComponent<Entity>();

                //if (entity == null || entity.lifes <= 0) return;

            }

            warningInstance = GetWarning();
            warningInstance.SetActive(true);

            if (useFocus) {
                focusInstance = hudWarning.GetFocusWarning();
                focusInstance.SetActive(true);
            }

            if (hudSignal != null) hudSignal.SetActive(true);

        }

    }

    private GameObject GetWarning() {

        GameObject warning = null;

        switch (warningType) {

            case WarningType.Tilt: warning = hudWarning.GetTiltWarning();  break;

            case WarningType.Shake: warning = hudWarning.GetShakeWarning(); break;

            case WarningType.Jump: warning = hudWarning.GetJumpWarning(); break;

            case WarningType.Tap: warning = hudWarning.GetTapWarning(); break;

            case WarningType.DangerTap: warning = hudWarning.GetTapDangerWarning(); break;

            case WarningType.Crit: warning = hudWarning.GetCritWarning(); break;

            case WarningType.Pickup: warning = hudWarning.GetPickupWarning(); break;

            case WarningType.HealthPickup: warning = hudWarning.GetHealthPickupWarning(); break;

            case WarningType.AmmoPickup: warning = hudWarning.GetAmmoPickupWarning(); break;

        }

        return warning;

    }

    private void DisposeWarning(GameObject instance) {

        switch (warningType) {

            case WarningType.Tilt: hudWarning.DisposeTiltWarning(instance); break;

            case WarningType.Shake: hudWarning.DisposeShakeWarning(instance); break;

            case WarningType.Jump: hudWarning.DisposeJumpWarning(instance); break;

            case WarningType.Tap: hudWarning.DisposeTapWarning(instance); break;

            case WarningType.DangerTap: hudWarning.DisposeTapDangerWarning(instance); break;

            case WarningType.Crit: hudWarning.DisposeCritWarning(instance); break;

            case WarningType.Pickup: hudWarning.DisposePickupWarning(instance); break;

            case WarningType.HealthPickup: hudWarning.DisposeHealthPickupWarning(instance); break;

            case WarningType.AmmoPickup: hudWarning.DisposeAmmoPickupWarning(instance); break;

        }

    }

}
