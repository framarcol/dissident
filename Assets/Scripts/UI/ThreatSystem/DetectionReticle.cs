﻿using UnityEngine;
using System.Collections;

public class DetectionReticle : MonoBehaviour {

    public Entity entity;
    public Transform target;
    
	void Update () {
        
        if(target != null) {

            Vector3 screenPoint = Camera.main.WorldToScreenPoint(target.position);

            if (screenPoint.x > Screen.width || screenPoint.y > Screen.height) Destroy(gameObject);
            if (entity.lifes <= 0) Destroy(gameObject);

            transform.position = screenPoint;

        }
            
    }

}
