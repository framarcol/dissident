﻿using UnityEngine;
using System.Collections;

public class HairCrossUI : MonoBehaviour {

    public Entity entity;

    public Transform target;

    [Header("SoundFX")]
    public AudioClip hairCrossEffect;
    AudioSource audioSource;

    void Start() {

        audioSource = GetComponent<AudioSource>();

        audioSource.PlayOneShot(hairCrossEffect);

    }

    void LateUpdate() {

        transform.position = Camera.main.WorldToScreenPoint(target.position);

    }

    public void GoCritical() {
        
        if(entity != null) entity.isCritical = true;

    }

	public void Finish() {
        
        if(entity != null) entity.isCritical = false;

        Destroy(gameObject);

    }

}
