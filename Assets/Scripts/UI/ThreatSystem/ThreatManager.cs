﻿using UnityEngine;
using System.Collections;

public class ThreatManager : MonoBehaviour {

    public GameObject threatHairCross;
    public GameObject detectionReticle;

    public HairCrossUI CreateThreat(Entity entity, Transform target) {

        if(threatHairCross != null) {

            HairCrossUI hairCross = ((GameObject)Instantiate(threatHairCross, Vector3.zero, Quaternion.identity)).GetComponent<HairCrossUI>();

            hairCross.transform.SetParent(transform);

            hairCross.entity = entity;
            hairCross.target = target;

            return hairCross;

        }

        return null;

    }

    public void CreateDetection(Entity entity, Transform target) {

        if(detectionReticle != null) {

            Vector3 screenPoint = Camera.main.WorldToScreenPoint(target.position);
            if (screenPoint.z < 0 || screenPoint.x > Screen.width || screenPoint.y > Screen.height || screenPoint.x < 0 || screenPoint.y < 0) return;

            //Debug.Log(screenPoint);

            DetectionReticle detection = ((GameObject)Instantiate(detectionReticle, Vector3.zero, Quaternion.identity)).GetComponent<DetectionReticle>();

            detection.transform.SetParent(transform);

            detection.entity = entity;
            detection.target = target;

        }

    }

}
