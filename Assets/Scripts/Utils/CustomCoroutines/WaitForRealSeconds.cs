﻿using UnityEngine;
using System.Collections;
using System;

public class WaitForRealSeconds : CustomYieldInstruction {

    float endTime;

    public override bool keepWaiting {
        get { return Time.realtimeSinceStartup < endTime; }
    }

    public WaitForRealSeconds(float time) {

        endTime = Time.realtimeSinceStartup + time;

    }

}
