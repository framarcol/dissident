﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(WeaponPair))]
public class PairDrawer : PropertyDrawer {

    const int rows = 3;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        EditorGUI.LabelField(position, label, EditorStyles.boldLabel);

        float deltaHeight = position.height / rows;

        Rect aRect = new Rect(position.x, position.y + deltaHeight, position.width, deltaHeight);
        Rect bRect = new Rect(position.x, position.y + (deltaHeight * 2), position.width, deltaHeight);

        EditorGUI.indentLevel++;

        EditorGUI.PropertyField(aRect, property.FindPropertyRelative("left"), new GUIContent("Left"));
        EditorGUI.PropertyField(bRect, property.FindPropertyRelative("right"), new GUIContent("Right"));

        EditorGUI.indentLevel--;

        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return base.GetPropertyHeight(property, label) * rows;
    }

}
