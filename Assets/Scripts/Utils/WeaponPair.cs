﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WeaponPair {

    public Weapon left;
    public Weapon right;

    public WeaponPair(Weapon left, Weapon right) {

        this.left = left;
        this.right = right;

    }

}
