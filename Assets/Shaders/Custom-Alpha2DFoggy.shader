﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Alpha2D Foggy" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//_BumpMap("Normalmap", 2D) = "bump" {}

		//_FogColor("Fog Color", Color) = (1,1,1,1)
		_FogLimit("Fog Limit", Float) = 100
		_FogDensity("Fog Density", Range(0,0.001)) = 0.0002
	}
	SubShader {
		
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Lambert keepalpha vertex:vert finalcolor:final
		#pragma multi_compile __ FOG_ON

		uniform fixed4 _Color;
		uniform sampler2D _MainTex;
		//uniform sampler2D _BumpMap;

		uniform float4 _FogColor;
		uniform float _FogLimit;
		uniform float _FogDensity;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
		};

		void vert(inout appdata_full v, out Input data) {

			UNITY_INITIALIZE_OUTPUT(Input, data);

			data.worldPos = mul(unity_ObjectToWorld, v.vertex);

		}

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * c.a;
			//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
			o.Alpha = c.a;
		}

		void final(Input IN, SurfaceOutput o, inout fixed4 color) {

#if FOG_ON

			float3 distanceVector = (_WorldSpaceCameraPos - IN.worldPos);
			float distance = (distanceVector.x * distanceVector.x) + (distanceVector.y * distanceVector.y) + (distanceVector.z * distanceVector.z);

			float diff = saturate((distance - (_FogLimit * _FogLimit)) * _FogDensity);

			color.rgb = lerp(color.rgb, _FogColor, diff);

#endif

		}
		ENDCG

	}
	FallBack "Transparent/VertexLit"
}
