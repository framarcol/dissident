﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Alpha2D Additive" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		//_FogColor("Fog Color", Color) = (1,1,1,1)
		//_FogLimit("Fog Limit", Float) = 100
		//_FogDensity("Fog Density", Range(0,0.001)) = 0.0002
	}
	SubShader {
		
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		
		Cull Off ZWrite Off
		Blend One One

		Pass{

		CGPROGRAM

		#include "UnityCG.cginc"
		#pragma vertex vert
		#pragma fragment frag

		struct v2f {

			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;

		};

		uniform fixed4 _Color;
		uniform sampler2D _MainTex;

		uniform float4 _MainTex_ST;

		v2f vert(appdata_base v)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			return o;
		}

		fixed4 frag(v2f i) : COLOR
		{

			fixed4 texcol = tex2D(_MainTex, i.uv);

			texcol *= _Color * texcol.a;

			return texcol;
		}

		ENDCG

			}

	}
	//FallBack "Transparent/VertexLit"
}
