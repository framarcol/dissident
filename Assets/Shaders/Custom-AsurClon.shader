﻿Shader "Custom/Asur Clon" {
	Properties {
		//_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal", 2D) = "bump" {}
		//_GlowMap("Glow map", 2D) = "black" {}
		//_GlowIntensity("Glow intensity", float) = 1

		/*_AlphaMap("Dissolve map", 2D) = "white" {}
		_Cutout("Alpha cutout", Range(0,1)) = 0
		_EdgeColorIn("Edge color in", Color) = (1,1,1,1)
		_EdgeColorOut("Edge color out", Color) = (1,1,1,1)
		_EdgeOffset("Edge offset", Range(0,0.5)) = 0

		_EdgeGradientScale("Edge Gradient Scale", float) = 1*/

		_Alpha("Alpha value", Range(0,1)) = 1
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True"}
		LOD 200
		
		Blend SrcAlpha OneMinusSrcAlpha

		Pass{
			ZWrite On
			ColorMask 0
		}

		//ZWrite Off

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd noshadow alpha:blend 

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _AlphaMap;
		//sampler2D _GlowMap;
		//fixed _GlowIntensity;

		struct Input {
			float2 uv_MainTex;
		};

		//fixed4 _Color;

		/*fixed _Cutout;
		fixed4 _EdgeColorIn;
		fixed4 _EdgeColorOut;
		fixed _EdgeOffset;
		fixed _EdgeGradientScale;*/

		fixed _Alpha;

		void surf (Input IN, inout SurfaceOutput o) {
			
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 alpha = tex2D(_AlphaMap, IN.uv_MainTex);
			//fixed4 emission = tex2D(_GlowMap, IN.uv_MainTex);

			//clip(alpha.a - _Cutout);

			//o.Albedo = mix(_EdgeColorIn.rgb, c.rgb, alpha.a - (_Cutout + _EdgeOffset)/* < 0*/);

			//Emission = fixed3(emission);// *_GlowIntensity;

			o.Albedo = c.rgb;

			/*if (alpha.a - (_Cutout + _EdgeOffset) < 0)
				o.Emission = lerp(_EdgeColorIn, _EdgeColorOut, (alpha.a - _Cutout) * _EdgeGradientScale);
			else
				o.Albedo = c.rgb;*/

			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
			o.Alpha = _Alpha;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
