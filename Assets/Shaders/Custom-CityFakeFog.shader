// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/City Fake Fog" {

Properties{

	_Color("Fog Color", Color) = (1,1,1,1)
	_SecondaryColor("Secondary Color", Color) = (1,1,1,1)

	_MainTex("Texture", 2D) = "white" {}

	//_MidHeight("Middle Height", Float) = 0
	//_TopHeight("Top Height", Float) = 0
	//_FogHeightFade("Fog Height Fade", Range(0,0.1)) = 1
	_SoftFade("Soft Fade", Range(0,0.1)) = 1

	_GlobalFade("Global Fade", Range(0,1)) = 0

	_MoveSpeedX("Move Speed X", Float) = 0
	_MoveSpeedY("Move Speed Y", Float) = 0

}

SubShader{

	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	Cull Off Lighting Off ZWrite Off

	Pass{

	CGPROGRAM

	#include "UnityCG.cginc"
	#pragma vertex vert
	#pragma fragment frag

	struct v2f {
	
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
		//float3 worldPos : TEXCOORD1;
		float4 screenPos : TEXCOORD2;
	
	};

	uniform fixed4 _Color;
	uniform fixed4 _SecondaryColor;
	
	uniform sampler2D _MainTex;

	//uniform float _MidHeight;
	//uniform float _TopHeight;
	//uniform float _FogHeightFade;
	uniform float _SoftFade;
	uniform float _GlobalFade;

	uniform float _MoveSpeedX;
	uniform float _MoveSpeedY;

	//uniform sampler2D _CameraDepthTexture;
	uniform float4 _MainTex_ST;

	v2f vert(appdata_base v)
	{

		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		//o.worldPos = mul(_Object2World, v.vertex);
		o.screenPos = ComputeScreenPos(o.pos);
		return o;

	}

	fixed4 frag(v2f i) : COLOR
	{

		fixed4 col = _Color;
		
		float2 scrolledUV = i.uv + float2(_MoveSpeedX * _Time.x, _MoveSpeedY * _Time.x);
		fixed4 texCol = tex2D(_MainTex, scrolledUV);

		//float depthValue = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);
		//float partZ = i.screenPos.z;

		//float depthDiff = saturate(abs(depthValue - partZ) * _SoftFade * texCol.a);

		//col.a = lerp(col.a, 0, 1 - depthDiff);
		col.rgb = lerp(col, _SecondaryColor, texCol.a);

		col.a = lerp(col.a, 0, _GlobalFade);

		return col;

	}

	ENDCG
}

}

}