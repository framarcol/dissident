﻿Shader "Custom/Crystal" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Emission("Emission Color", Color) = (1,1,1,1)

		_Intensity("Emission Intensity", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert 

		struct Input {
			float2 uv_MainTex;
			//float vertexColor : COLOR;
		};

		fixed4 _Color;
		fixed4 _Emission;
		float _Intensity;

		void surf (Input IN, inout SurfaceOutput o) {

			o.Albedo = _Color;
			o.Emission = _Emission * _Intensity;
			//o.Emission = IN.vertexColor * _Intensity;
			o.Alpha = 1.0;

		}
		ENDCG
	}
	//Fallback "Mobile/Diffuse"
}
