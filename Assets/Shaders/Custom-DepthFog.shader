﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Depth Fog" {

	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
		_FogStart("Fog Start", Float) = 0
		_FogDensity("Fog Density", Range(0,0.1)) = 1

		//_SkyboxTolerance("Skybox Tolerance", Float) = 1
	} 

	SubShader{

		Cull Off ZWrite Off ZTest Always

		Pass{

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				//float4 interpolatedRay : TEXCOORD2;
			};
			
			uniform float4 _Color;
			uniform sampler2D _MainTex;
			uniform float _FogStart;
			uniform float _FogDensity;

			//uniform float _SkyboxTolerance;

			//uniform float4x4 _FrustumCorners;

			uniform sampler2D _CameraDepthTexture;

			v2f vert(appdata v) {

				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.screenPos = ComputeScreenPos(o.pos);

				/*half index = v.vertex.z;
				o.interpolatedRay = _FrustumCorners[(int)index];
				o.interpolatedRay.w = index;*/

				return o;

			}

			half4 frag(v2f i) : COLOR{

				half4 color;

				color = tex2D(_MainTex, i.uv);

				float depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);
				float diff = saturate((depth - _FogStart) * _FogDensity);

				/*float dpth = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, i.uv)));
				float3 worldPos = (_WorldSpaceCameraPos + dpth * i.interpolatedRay);*/

				//float heightDiff = saturate(abs(worldPos.z - 10) * _FogDensity);

				//depth = Linear01Depth(depth);//float rawDepth = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, i.uv)));//Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);
				//if (depth >= 498.2) diff = 0;

				color = lerp(color, _Color, diff);

				return color;

			}

			ENDCG
		}
	}

}
