﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Double Sided" {
	
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}

		//_FogColor("Fog Color", Color) = (1,1,1,1)
		_FogLimit("Fog Limit", Float) = 100
		_FogDensity("Fog Density", Range(0,0.001)) = 0.0002
	}

	SubShader{

		Tags{ "RenderType" = "Opaque" }
		Cull Off

		CGPROGRAM

		#pragma surface surf Lambert noforwardadd vertex:vert finalcolor:final
		#pragma multi_compile __ FOG_ON

		uniform sampler2D _MainTex;

		uniform fixed4 _FogColor;
		uniform float _FogLimit;
		uniform float _FogDensity;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
		};

		void vert(inout appdata_full v, out Input data) {

			UNITY_INITIALIZE_OUTPUT(Input, data);

			data.worldPos = mul(unity_ObjectToWorld, v.vertex);

		}

		void surf(Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		void final(Input IN, SurfaceOutput o, inout fixed4 color) {

#if FOG_ON

			float3 distanceVector = (_WorldSpaceCameraPos - IN.worldPos);
			float distance = (distanceVector.x * distanceVector.x) + (distanceVector.y * distanceVector.y) + (distanceVector.z * distanceVector.z);

			float diff = saturate((distance - (_FogLimit * _FogLimit)) * _FogDensity);

			color.rgb = lerp(color.rgb, _FogColor, diff);

#endif

		}

		ENDCG

	}

	FallBack "Mobile/Diffuse"

}
