// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Fake Fog" {

Properties{

	//_Color("Fog Color", Color) = (1,1,1,1)

	_MainTex("Texture", 2D) = "white" {}

	_MidHeight("Middle Height", Float) = 0
	_TopHeight("Top Height", Float) = 0
	_OpaqueHeightScale("Opaque Height Scale", Float) = 1
	_TransparentHeightScale("Transparent Height Scale", Float) = 1
	//_SoftFade("Soft Fade", Range(0,0.1)) = 1

	_GlobalFade("Global Fade", Range(0,1)) = 0

	_MoveSpeedX("Move Speed X", Float) = 0
	_MoveSpeedY("Move Speed Y", Float) = 0

}

SubShader{

	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	/*Cull Off*/ Lighting Off ZWrite Off

	Pass{

	CGPROGRAM

	#include "UnityCG.cginc"
	#pragma vertex vert
	#pragma fragment frag

	struct v2f {
	
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
		float3 worldPos : TEXCOORD1;
		//float4 screenPos : TEXCOORD2;
	
	};

	uniform fixed4 _FogColor;
	
	uniform sampler2D _MainTex;

	uniform float _MidHeight;
	uniform float _TopHeight;
	uniform float _OpaqueHeightScale;
	uniform float _TransparentHeightScale;
	//uniform float _SoftFade;
	uniform float _GlobalFade;

	uniform float _MoveSpeedX;
	uniform float _MoveSpeedY;

	//uniform sampler2D _CameraDepthTexture;
	uniform float4 _MainTex_ST;

	v2f vert(appdata_base v)
	{

		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		o.worldPos = v.vertex;//mul(unity_ObjectToWorld, v.vertex);
		//o.screenPos = ComputeScreenPos(o.pos);
		return o;

	}

	fixed4 frag(v2f i) : COLOR
	{

		fixed4 col = _FogColor;
		
		float2 scrolledUV = i.uv + float2(_MoveSpeedX * _Time.x, _MoveSpeedY * _Time.x);
		fixed4 texCol = tex2D(_MainTex, scrolledUV);

		float midDiff = saturate((i.worldPos.y - _MidHeight) * _OpaqueHeightScale);
		col.a = lerp(1, texCol.a, midDiff);

		float heightDiff = saturate((i.worldPos.y - _MidHeight) * _TransparentHeightScale);
		col.a = lerp(col.a, 0, heightDiff);
		
		/*float depthValue = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);
		float partZ = i.screenPos.z;

		float depthDiff = saturate(abs(depthValue - partZ) * _SoftFade);

		col.a = lerp(col.a, 0, 1 - depthDiff);*/

		col.a = lerp(col.a, 0, _GlobalFade);

		return col;

	}

	ENDCG
}

}
//FallBack "Transparent/VertexLit"

}