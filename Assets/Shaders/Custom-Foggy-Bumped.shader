// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Simplified Bumped shader. Differences from regular Bumped one:
// - no Main Color
// - Normalmap uses Tiling/Offset of the Base texture
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/Foggy Bumped Diffuse" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_FogColor ("Fog Color", Color) = (1,1,1,1)
	_FogHeight("Fog Height", Float) = 0
	_FogFade("Fog Fade Scale", Range(0,0.5)) = 1
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250

CGPROGRAM
#pragma surface surf Lambert noforwardadd finalcolor:final vertex:vert

sampler2D _MainTex;
sampler2D _BumpMap;
float4 _FogColor;
float _FogHeight;
float _FogFade;

struct Input {
	float2 uv_MainTex;
	float4 worldSpacePosition;
};

void vert(inout appdata_full v, out Input data) {

	UNITY_INITIALIZE_OUTPUT(Input, data);

	data.worldSpacePosition = mul(unity_ObjectToWorld, v.vertex);

}

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = c.rgb;
	o.Alpha = c.a;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
}

void final(Input IN, SurfaceOutput o, inout fixed4 color) {

	float diff = saturate((_FogHeight - IN.worldSpacePosition.y) * _FogFade);

	color = lerp(color, _FogColor, diff);

}

ENDCG  
}

FallBack "Mobile/VertexLit"
}
