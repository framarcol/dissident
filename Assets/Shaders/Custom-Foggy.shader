// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Foggy Diffuse" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_FogColor ("Fog Color", Color) = (1,1,1,1)
	_FogHeight("Fog Height", Float) = 0
	_FogFade("Fog Fade Scale", Range(0,0.5)) = 1
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250

CGPROGRAM
#pragma surface surf Lambert noforwardadd finalcolor:final vertex:vert

sampler2D _MainTex;
float4 _FogColor;
float _FogHeight;
float _FogFade;

struct Input {
	float2 uv_MainTex;
	float4 worldSpacePosition;
};

void vert(inout appdata_full v, out Input data) {

	UNITY_INITIALIZE_OUTPUT(Input, data);

	data.worldSpacePosition = mul(unity_ObjectToWorld, v.vertex);

}

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}

void final(Input IN, SurfaceOutput o, inout fixed4 color) {

	float diff = saturate((_FogHeight - IN.worldSpacePosition.y) * _FogFade);

	color = lerp(color, _FogColor, diff);

}

ENDCG  
}

FallBack "Mobile/VertexLit"
}
