﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "Custom/Ghost/Bumped Diffuse"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap("Bump", 2D) = "bump" {}
		_FadeFactor("Fade Factor", Range(0,1)) = 1
		_FarDistance("Far Distance", Float) = 0
		_FarFade("Far Fade", Range(0,1)) = 0
	}
	SubShader
	{	
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back Lighting On ZWrite Off

		CGPROGRAM
		#pragma surface surf Lambert alpha:blend noforwardadd finalcolor:final vertex:vert

		uniform sampler2D _MainTex;
		uniform sampler2D _BumpMap;
		
		uniform float _FadeFactor;

		uniform float _FarDistance;
		uniform float _FarFade;

		struct Input {
			float2 uv_MainTex;
			float4 worldSpacePosition;
		};

		void vert(inout appdata_full v, out Input data) {

			UNITY_INITIALIZE_OUTPUT(Input, data);

			data.worldSpacePosition = mul(unity_ObjectToWorld, v.vertex);

		}

		void surf(Input IN, inout SurfaceOutput o) {

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));

		}

		void final(Input IN, SurfaceOutput o, inout fixed4 color) {

			float3 vectorDistance = (_WorldSpaceCameraPos - IN.worldSpacePosition);
			float sqrDistance = vectorDistance.x * vectorDistance.x + vectorDistance.y * vectorDistance.y + vectorDistance.z * vectorDistance.z;

			float diffDistance = _FarDistance * _FarDistance - sqrDistance;

			float farAlpha = saturate((diffDistance) * _FarFade);
			float closeAlpha = saturate(abs(sqrDistance) * _FadeFactor);

			float chosenAlpha = min(farAlpha, closeAlpha);
			color.a = chosenAlpha;

		}
		ENDCG

	}

	FallBack "Legacy Shaders/Transparent/Diffuse"

}

/*Shader "Custom/Ghost/Colored Unlit"{

Properties{

_MainTex("Texture", 2D) = "white" {}
_FadeFactor("Fade Factor", Range(0,1)) = 1
_FarDistance("Far Distance", Float) = 0
_FarFade("Far Fade", Range(0,1)) = 0

}

SubShader{

Tags{ "Queue" = "Geometry" "IgnoreProjector" = "True" "RenderType" = "Opaque" }
Blend SrcAlpha OneMinusSrcAlpha
Cull Back Lighting Off ZWrite Off

Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF

#include "UnityCG.cginc"

struct appdata
{
float4 vertex : POSITION;
float2 uv : TEXCOORD0;
#ifdef LIGHTMAP_ON
float2 uv2 : TEXCOORD1;
#endif
};

struct v2f
{
float2 uv : TEXCOORD0;
#ifdef LIGHTMAP_ON
float2 uv2 : TEXCOORD1;
#endif
float4 vertex : SV_POSITION;
float4 worldPos : TEXCOORD2;
};

uniform sampler2D _MainTex;
uniform float4 _MainTex_ST;

uniform float _FadeFactor;

uniform float _FarDistance;
uniform float _FarFade;

//sampler2D unity_Lightmap;
//float4 unity_LightmapST;

v2f vert(appdata v)
{
v2f o;
o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);

o.uv = v.uv * _MainTex_ST.xy + _MainTex_ST.zw;

#ifdef LIGHTMAP_ON
o.uv2 = v.uv2.xy * unity_LightmapST.xy + unity_LightmapST.zw;
#endif

o.worldPos = mul(_Object2World, v.vertex);
return o;
}

fixed4 frag(v2f i) : SV_Target
{
fixed4 col = tex2D(_MainTex, i.uv);
#ifdef LIGHTMAP_ON
col.rgb *= DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv2));
#endif

float3 vectorDistance = (_WorldSpaceCameraPos - i.worldPos);
float sqrDistance = vectorDistance.x * vectorDistance.x + vectorDistance.y * vectorDistance.y + vectorDistance.z * vectorDistance.z;

float diffDistance = _FarDistance - sqrDistance;

float farAlpha = saturate((diffDistance)* _FarFade);
float closeAlpha = saturate(abs(sqrDistance) * _FadeFactor);

float chosenAlpha = min(farAlpha, closeAlpha);

col.a = min(farAlpha, closeAlpha);

return col;
}
ENDCG
}

}

Fallback "Mobile/Diffuse"

}*/
