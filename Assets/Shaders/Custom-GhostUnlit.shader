﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Ghost/Unlit"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_FadeFactor("Fade Factor", Range(0,1)) = 1
	}
	SubShader
	{	
		Tags{ "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back Lighting Off ZWrite On

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 worldPos : TEXCOORD1;
			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float _FadeFactor;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv * _MainTex_ST.xy + _MainTex_ST.zw;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}
		
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				float3 vectorDistance = (_WorldSpaceCameraPos - i.worldPos);
				float sqrDistance = vectorDistance.x * vectorDistance.x + vectorDistance.y * vectorDistance.y + vectorDistance.z * vectorDistance.z;

				col.a = saturate(abs(sqrDistance) * _FadeFactor);

				return col;
			}
			ENDCG
		}
	}

	Fallback "Mobile/Diffuse"

}
