﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Hologram" {
	
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MarginColor("Margins Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Bands("Bands", 2D) = "white" {}
		[NoScaleOffset] _OverlayBands("Overlay Bands", 2D) = "white" {}
		_MinOpacity("Minimum opacity", float) = 0
		_Scale("Scale", float) = 1
		_Power("Power", float) = 1
		
		_BandingVelocity("Banding Velocity", float) = 0
		_BandsTiling("Bands Tiling", float) = 1

		_OverlayBandsOffset("Overlay bands offset", float) = 0
		
		_CenterReference("Center Reference", float) = 0
		_ClipRadius("Clip Radius", float) = 2
		_VisRadius("Visibility Radius", float) = 1.5
		_VisScale("Visibility Scale", float) = 1
		_VisPower("Visibility Power", float) = 1

		_GlobalAlpha("Global Alpha", Range(0,1)) = 1

	}

	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"  }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200
		
		Pass{

			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag

			uniform fixed4 _Color;
			uniform fixed4 _MarginColor;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float _MinOpacity;
			uniform sampler2D _Bands;
			uniform float4 _Bands_ST;
			uniform sampler2D _OverlayBands;

			uniform float _Scale;
			uniform float _Power;

			uniform float _BandingVelocity;
			uniform float _BandsTiling;

			uniform float _OverlayBandsOffset;

			uniform float _CenterReference;
			uniform float _ClipRadius;
			uniform float _VisRadius;
			uniform float _VisScale;
			uniform float _VisPower;

			uniform float _GlobalAlpha;

			struct v2f {
				
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;

				float rim : TEXCOORD2;
				float3 worldPos : TEXCOORD3;
				float4 screenPos : TEXCOORD4;

			};

			v2f vert(appdata_base v) {

				v2f o;
				
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

				float3 vertexPos = mul(unity_ObjectToWorld, v.vertex);

				float3 normal = normalize(mul(unity_ObjectToWorld, float4(v.normal, 0.0)));
				float3 cameraDir = normalize(vertexPos - _WorldSpaceCameraPos.xyz);

				o.rim = _Scale * pow(1 + dot(normal, cameraDir), _Power);

				o.worldPos = vertexPos;

				o.screenPos = ComputeScreenPos(o.pos);

				return o;

			}

			half4 frag(v2f f) : COLOR{

				float widthDiff = -(abs(_CenterReference + f.worldPos.x) - _ClipRadius);
				clip(widthDiff);

				half4 mainColor = tex2D(_MainTex, f.uv) * _Color;

				float2 screenUVs = f.screenPos.xy / f.screenPos.w;
				float2 scrolledBands = float2(screenUVs.x, (_BandsTiling * screenUVs.y) + _Time.x  * _BandingVelocity);
				half4 bandsColor = tex2D(_Bands, scrolledBands);

				half4 overlayBandsColor = tex2D(_OverlayBands, float2(screenUVs.x, screenUVs.y + _OverlayBandsOffset));

				half4 color = mainColor;
				color = lerp(color, _Color, f.rim);

				half marginDiff = _VisScale * pow(saturate(abs(_CenterReference + f.worldPos.x) - _VisRadius), _VisPower);
				color = lerp(color, _MarginColor, marginDiff);

				half alpha = min(mainColor.a, _MinOpacity);
				alpha = lerp(alpha, bandsColor.a, f.rim);
				alpha = min(alpha, overlayBandsColor.a);

				color.a = alpha * _GlobalAlpha;

				return color;

			}
			ENDCG

		}
	}

}
