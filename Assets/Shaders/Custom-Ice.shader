﻿Shader "Custom/Ice" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_ReflectionColor("Reflection Color", Color) = (1,1,1,1)
		_BumpMap("Bumpmap", 2D) = "bump" {}

		_Blur("Blur", Float) = 0
		_Shininess("Shininess", Range(0.03, 1)) = 0.078125

	}
	
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		//Cull Off
		CGPROGRAM
		#pragma surface surf MobileBlinnPhong

		inline fixed4 LightingMobileBlinnPhong(SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten){

			fixed diff = max(0, dot(s.Normal, lightDir));
			fixed nh = max(0, dot(s.Normal, halfDir));
			fixed spec = pow(nh, s.Specular * 128) * s.Gloss;

			fixed4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
			UNITY_OPAQUE_ALPHA(c.a);
			return c;

		}

		struct Input {
			//float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 worldRefl;
			INTERNAL_DATA
		};

		//sampler2D _MainTex;
		uniform float4 _Color;
		uniform float4 _ReflectionColor;
		//uniform float _Fade;
		uniform float _Blur;
		uniform float _Shininess;
		sampler2D _BumpMap;
		//samplerCUBE _ReflectionMap;

		void surf(Input IN, inout SurfaceOutput o) {

			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			//float4 val = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, WorldReflectionVector(IN, o.Normal));
			float4 val = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, WorldReflectionVector(IN, o.Normal), _Blur);
			//float4 val = texCUBE(_ReflectionMap, WorldReflectionVector(IN, o.Normal)).rgb;
			val.rgb = DecodeHDR(val, unity_SpecCube0_HDR);

			o.Albedo = _Color.rgb;
			o.Emission = val.rgb * _ReflectionColor.rgb;
			o.Specular = _Shininess;
			o.Gloss = _Color.a;
			o.Alpha = 1.0;

		}
		ENDCG
	}
		Fallback "Diffuse"
}