﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/Outline Bumped Diffuse" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_OutlineColor("Outline color", Color) = (1,1,1,1)
		_OutlinePower("Outline power", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd vertex:vert finalcolor:final

		uniform sampler2D _MainTex;
		uniform fixed4 _Color;
		uniform fixed4 _OutlineColor;
		uniform float _OutlinePower;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 normal;
			float3 viewDir;
		};

		void vert(inout appdata_full v, out Input data) {

			UNITY_INITIALIZE_OUTPUT(Input, data);

			data.normal = normalize( mul(float4(v.normal, 0.0), unity_WorldToObject).xyz );
			data.viewDir = normalize( _WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz );

		}

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		void final(Input IN, SurfaceOutput o, inout fixed4 color) {

			float3 normalDirection = normalize(IN.normal);
			float3 viewDirection = normalize(IN.viewDir);

			float interpolation = abs(dot(viewDirection, normalDirection));

			color = lerp(_OutlineColor, color, pow(interpolation, _OutlinePower));

		}
		ENDCG
	}
	FallBack "Mobile/Diffuse"
}
