﻿Shader "Custom/Poppy Bumped" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_BumpMap("Normalmap", 2D) = "bump" {}
		_PopTex("Popping Texture", 2D) = "white" {}
		_AlphaCutoff("Alpha cutoff", Range(0,1)) = 0
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 250

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd alphatest:_AlphaCutoff addshadow

		uniform sampler2D _MainTex;
		uniform sampler2D _BumpMap;
		uniform sampler2D _PopTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 popC = tex2D(_PopTex, IN.uv_MainTex);
			
			o.Albedo = c.rgb;
			o.Alpha = popC.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));

		}
		ENDCG
	}

		FallBack "Mobile/Diffuse"
}

