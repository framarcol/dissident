﻿Shader "Custom/Rim Bumped" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_BumpMap("Normalmap", 2D) = "bump" {}

		_RimColor("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower("Rim Power", Range(0.5,8.0)) = 3.0
	}

	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 250

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		uniform sampler2D _MainTex;
		uniform sampler2D _BumpMap;

		uniform float4 _RimColor;
		uniform float _RimPower;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));

			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _RimColor.rgb * pow(rim, _RimPower);
		}
		ENDCG
	}

	FallBack "Mobile/Diffuse"
}
