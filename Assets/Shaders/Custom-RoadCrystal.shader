﻿Shader "Custom/Road Crystal" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		//_SpecColor("Specular Color", Color) = (0.5, 0.5, 0.5, 0)
		_Shininess("Shininess", Range(0.01, 1)) = 0.078125
		//_MainTex("Base (RGB) TransGloss (A)", 2D) = "white" {}
	}

	SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Cull Off

		CGPROGRAM
		#pragma surface surf MobileBlinnPhong alpha:fade

		inline fixed4 LightingMobileBlinnPhong(SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten) {

			fixed diff = max(0, dot(s.Normal, lightDir));
			fixed nh = max(0, dot(s.Normal, halfDir));
			fixed spec = pow(nh, s.Specular * 128) * s.Gloss;

			fixed4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
			c.a = s.Alpha;
			//UNITY_OPAQUE_ALPHA(c.a);
			return c;

		}

			//sampler2D _MainTex;
		fixed4 _Color;
		half _Shininess;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			//fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = _Color.rgb;
			o.Gloss = _Color.a;
			o.Alpha = _Color.a;
			o.Specular = _Shininess;
		}
		ENDCG
	}

	Fallback "Legacy Shaders/Transparent/VertexLit"
}
