﻿Shader "Custom/Scrollable" {
	
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}

		_ScrollX("Scroll X", Float) = 0
		_ScrollY("Scroll Y", Float) = 0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		sampler2D _MainTex;
		sampler2D _BumpMap;

		struct Input {
			float2 uv_MainTex;
		};

		fixed _ScrollX;
		fixed _ScrollY;

		void surf (Input IN, inout SurfaceOutput o) {

			fixed2 scrolledUVs = fixed2(IN.uv_MainTex.x + (_ScrollX * _Time.x), IN.uv_MainTex.y + (_ScrollY * _Time.x));
			fixed4 c = tex2D(_MainTex, scrolledUVs);

			o.Albedo = c.rgb;
			o.Normal = UnpackNormal(tex2D(_BumpMap, scrolledUVs));
			o.Alpha = c.a;

		}
		ENDCG
	}

	FallBack "Diffuse"

}
