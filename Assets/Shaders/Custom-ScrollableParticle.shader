﻿Shader "Custom/Scrollable Particle (Alpha Blended)" {

	Properties{
		_Color("Main Color", Color) = (1, 1, 1, 1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		//_BumpMap("Normal Map", 2D) = "bump" {}

		_ScrollX("Scroll X", Float) = 0
		_ScrollY("Scroll Y", Float) = 0
	}

	SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		//LOD 200

		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off

		CGPROGRAM
		#pragma surface surf Lambert alpha:blend noforwardadd 

		fixed4 _Color;
		sampler2D _MainTex;
		//sampler2D _BumpMap;

		struct Input {
			float2 uv_MainTex;
		};

		fixed _ScrollX;
		fixed _ScrollY;

		void surf(Input IN, inout SurfaceOutput o) {

			fixed2 scrolledUVs = fixed2(IN.uv_MainTex.x + (_ScrollX * _Time.x), IN.uv_MainTex.y + (_ScrollY * _Time.x));
			fixed4 c = tex2D(_MainTex, scrolledUVs);

			o.Albedo = c.rgb * _Color;
			//o.Normal = UnpackNormal(tex2D(_BumpMap, scrolledUVs));
			o.Alpha = c.a;

		}
		ENDCG
	}

	FallBack "Diffuse"

}
