﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Shield" {
	Properties{
		_Offset("Time", Float) = 0.0
		_Color("Tint (RGB)", Color) = (1,1,1,1)
		_SurfaceTex("Texture (RGB)", 2D) = "white" {}
		_RampTex("Facing Ratio Ramp (RGB)", 2D) = "white" {}
		_RampScale("Ramp Scale", Float) = 1
	}
	SubShader{
		ZWrite Off
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend One One
		Cull Off

		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc" 

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 normal : TEXCOORD2;
			};

			uniform float _Offset;

			v2f vert(appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);

				float3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
				o.uv = v.texcoord.xy;
				o.uv2 = float2(abs(dot(viewDir, v.normal)), 0.5);
				o.normal = v.normal;
				return o;
			}

			uniform float4 _Color;
			uniform sampler2D _RampTex;
			uniform sampler2D _SurfaceTex;
			uniform float _RampScale;

			half4 frag(v2f f) : COLOR {

				f.normal = normalize(f.normal);

				half4 ramp = tex2D(_RampTex, f.uv2 * _RampScale) * _Color.a ;

				float2 scrolledUV = f.uv + float2(_Offset * _Time.x, _Offset * _Time.x);
				half4 thisTex = tex2D(_SurfaceTex, scrolledUV) * ramp * _Color;

				return half4 (thisTex.r, thisTex.g, thisTex.b, ramp.r);

			}

			ENDCG

		}
	}

	Fallback "Transparent/VertexLit"

}