// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Simplified Additive Particle shader. Differences from regular Additive Particle one:
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "SkatePunk/Particles/AdditiveCustom" {
Properties {
	_Color("Main Color", Color) = (1,1,1,1)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_SoftFactor("Soft Factor", Float) = 1
	_ScrollU("Scroll U Scale", Float) = 0
	_ScrollV("Scroll V Scale", Float) = 0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha One
	Cull Off Lighting Off ZWrite Off //Fog { Color (0,0,0,0) }
	
	/*BindChannels {
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}*/
	
	SubShader {
		Pass {

			CGPROGRAM

			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				//float4 worldSpacePosition : TEXCOORD1;
			};
			
			uniform fixed4 _Color;
			uniform sampler2D _MainTex;
			uniform float _SoftFactor;
			uniform float _ScrollU;
			uniform float _ScrollV;

			uniform sampler2D _CameraDepthTexture;
			uniform float4 _MainTex_ST;


			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				o.screenPos = ComputeScreenPos(o.pos);
				//o.worldSpacePosition = v.worldSpacePosition;
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{	

				float2 scrolledUV = i.uv + float2(_ScrollU * _Time.x, _ScrollV * _Time.x);

				fixed4 texcol = tex2D(_MainTex, scrolledUV);
				
				texcol *= _Color;

				float depthValue = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);

				//Actual distance to the camera
				float partZ = i.screenPos.z;

				float diff = saturate(abs(depthValue - partZ) * _SoftFactor);

				texcol.a = lerp(texcol.a, 0, 1 - diff);

				return texcol;
			}

			ENDCG
		}
	}
}
}