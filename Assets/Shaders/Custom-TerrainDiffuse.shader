﻿Shader "Custom/Terrain/Diffuse" {
	Properties{
		[HideInInspector] _Control("Control (RGBA)", 2D) = "red" {}
		[HideInInspector] _Splat3("Layer 3 (A)", 2D) = "white" {}
		[HideInInspector] _Splat2("Layer 2 (B)", 2D) = "white" {}
		[HideInInspector] _Splat1("Layer 1 (G)", 2D) = "white" {}
		[HideInInspector] _Splat0("Layer 0 (R)", 2D) = "white" {}
		[HideInInspector] _Normal3("Normal 3 (A)", 2D) = "bump" {}
		[HideInInspector] _Normal2("Normal 2 (B)", 2D) = "bump" {}
		[HideInInspector] _Normal1("Normal 1 (G)", 2D) = "bump" {}
		[HideInInspector] _Normal0("Normal 0 (R)", 2D) = "bump" {}
		// used in fallback on old cards & base map
		[HideInInspector] _MainTex("BaseMap (RGB)", 2D) = "white" {}
		[HideInInspector] _Color("Main Color", Color) = (1,1,1,1)

		_Offset("Offset", Float) = 0

	}

		CGINCLUDE
#pragma surface surf Lambert vertex:SplatmapVert finalcolor:SplatmapFinalColor finalprepass:SplatmapFinalPrepass finalgbuffer:SplatmapFinalGBuffer
//#pragma multi_compile_fog
#include "TerrainSplatmapCommon.cginc"

			uniform float _Offset;

	void surf(Input IN, inout SurfaceOutput o)
	{
		half4 splat_control = tex2D(_Control, IN.tc_Control);
		/*half weight;
		fixed4 mixedDiffuse;
		SplatmapMix(IN, splat_control, weight, mixedDiffuse, o.Normal);
		o.Albedo = mixedDiffuse.rgb;*/

		fixed3 col;
		col = splat_control.r * tex2D(_Splat0, IN.uv_Splat0).rgb;
		float2 scrolledUV = IN.uv_Splat1 + float2( _Offset * _Time.x, _Offset * _Time.x);
		col += splat_control.g * tex2D(_Splat1, scrolledUV).rgb;
		col += splat_control.b * tex2D(_Splat2, IN.uv_Splat2).rgb;
		col += splat_control.a * tex2D(_Splat3, IN.uv_Splat3).rgb;
		o.Albedo = col;

#ifdef _TERRAIN_NORMAL_MAP
		fixed4 nrm = 0.0f;
		nrm += splat_control.r * tex2D(_Normal0, IN.uv_Splat0);
		//float2 nrmScrolledUV = IN.uv_Splat1 + float2(_Offset * _Time.x, _Offset * _Time.x);
		nrm += splat_control.g * tex2D(_Normal1, scrolledUV);
		nrm += splat_control.b * tex2D(_Normal2, IN.uv_Splat2);
		nrm += splat_control.a * tex2D(_Normal3, IN.uv_Splat3);
		o.Normal = UnpackNormal(nrm);
#endif

		o.Alpha = 1.0;
	}
	ENDCG

	Category{
		Tags{
			"Queue" = "Geometry-99"
			"RenderType" = "Opaque"
		}
		// TODO: Seems like "#pragma target 3.0 _TERRAIN_NORMAL_MAP" can't fallback correctly on less capable devices?
		// Use two sub-shaders to simulate different features for different targets and still fallback correctly.
		SubShader{ // for sm3.0+ targets
			CGPROGRAM
			#pragma target 3.0
			#pragma multi_compile __ _TERRAIN_NORMAL_MAP
			ENDCG
		}
		SubShader{ // for sm2.0 targets
			CGPROGRAM
			ENDCG
		}
	}

	Dependency "AddPassShader" = "Hidden/TerrainEngine/Splatmap/Diffuse-AddPass"
	Dependency "BaseMapShader" = "Diffuse"
	/*Dependency "Details0" = "Hidden/TerrainEngine/Details/Vertexlit"
	Dependency "Details1" = "Hidden/TerrainEngine/Details/WavingDoublePass"
	Dependency "Details2" = "Hidden/TerrainEngine/Details/BillboardWavingDoublePass"
	Dependency "Tree0" = "Hidden/TerrainEngine/BillboardTree"*/

	Fallback "Diffuse"

}