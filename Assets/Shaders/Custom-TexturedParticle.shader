// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Particles/Textured Particle" {

Properties {
	_Color("Main Color", Color) = (1,1,1,1)
	_MainTex ("Particle Texture", 2D) = "white" {}
}

SubShader{

	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	Cull Off Lighting Off ZWrite Off

	Pass{

	CGPROGRAM

	#include "UnityCG.cginc"
	#pragma vertex vert
	#pragma fragment frag

	struct v2f {
	
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	
	};

	uniform fixed4 _Color;
	uniform sampler2D _MainTex;

	uniform float4 _MainTex_ST;

	v2f vert(appdata_base v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		return o;
	}

	fixed4 frag(v2f i) : COLOR
	{

		fixed4 texcol = tex2D(_MainTex, i.uv);

		texcol *= _Color;

		return texcol;
	}

	ENDCG
}
}

}