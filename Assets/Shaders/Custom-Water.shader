﻿Shader "Custom/Water" {
	
	Properties {
		_Color ("Base Color", Color) = (1,1,1,1)
		_SecColor("Second Color", Color) = (1,1,1,1)
		_ThirdColor("Third Color", Color) = (1,1,1,1)
		//_TopFactor("Top Factor", Float) = 0
		//_BotFactor("Bottom Factor", Float) = 0
		//_MainTex ("Albedo (RGB)", 2D) = "white" {}
		
		_WaveAmplitude("Wave Amplitude", Range(0,1)) = 0
		_WaveDistorsion("Wave Distorsion", Float) = 0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		//LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert noforwardadd vertex:vert //finalcolor:final

		struct Input {
			float2 uv_MainTex;
			float vertexHeight;
		};

		uniform fixed4 _Color;
		uniform fixed4 _SecColor;
		uniform fixed4 _ThirdColor;
		//uniform sampler2D _MainTex;
		//uniform float _TopFactor;
		//uniform float _BotFactor;

		uniform float _WaveAmplitude;
		uniform float _WaveDistorsion;

		float rand(float2 co) {
			return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
		}

		void vert(inout appdata_full v, out Input data) {

			UNITY_INITIALIZE_OUTPUT(Input, data);
			
			float distorsion = sin(rand(v.texcoord.xy) * _Time.x * _WaveDistorsion);

			//v.vertex.y += _WaveAmplitude * distorsion;

			v.vertex.xyz += v.normal * (_WaveAmplitude * distorsion);

			data.vertexHeight = distorsion;

		}

		void surf (Input IN, inout SurfaceOutput o) {
			
			float a = IN.vertexHeight + 1;
			float4 color = lerp(_Color, _SecColor, saturate(a));
			a -= 1;
			color = lerp(color, _ThirdColor, saturate(a));

			o.Albedo = color.rgb;
			o.Alpha = color.a;

		}

		/*void final(Input IN, SurfaceOutput o, inout fixed4 color){

			//float diff = saturate(1 - IN.vertexHeight);

			//color = lerp(_SecColor, color, diff * _TopFactor);

		}*/
		
		ENDCG
	}

	FallBack "Diffuse"

}
