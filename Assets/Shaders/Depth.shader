﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Render Depth" {
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		Pass{
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct v2f {
			float4 pos : SV_POSITION;
			float4 screenPos : TEXCOORD1;
		};

		sampler2D _CameraDepthTexture;

		v2f vert(appdata_base v) {
			
			v2f o;

			o.pos = UnityObjectToClipPos(v.vertex);
			o.screenPos = ComputeScreenPos(o.pos);

			return o;

		}

		half4 frag(v2f i) : COLOR{
			
			float depth = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)).r);

			half4 color;
			color.r = depth;
			color.g = depth;
			color.b = depth;

			color.a = 1;

			return color;

		}
			ENDCG
		}
	}

	FallBack "Diffuse"
}
