﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Halo Custom" {
	SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite Off /*Cull Off*/	// NOTE: 'Cull off' is important as the halo meshes flip handedness each time... BUG: #1220
		Blend OneMinusDstColor One
		ColorMask RGB

		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _HaloFalloff;
			float4 _HaloFalloff_ST;

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			v2f vert(appdata_t v){

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord; //TRANSFORM_TEX(v.texcoord,_HaloFalloff);
				return o;

			}

			fixed4 frag(v2f i) : SV_Target{

				fixed a = tex2D(_HaloFalloff, i.texcoord).a;
				fixed4 col = fixed4(i.color.rgb * a, a);
				return col;

			}
			ENDCG
		}
	}
}
