﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class puntuacionPanelNiveles : MonoBehaviour {

    Text[] textos;


    void Start () {

        textos = GetComponentsInChildren<Text>();

        float estrellas = 0, estrellasUno = 0, estrellasDos = 0, estrellasTres = 0;

        for (int j = 0; j < 4; j++) for (int i = 0; i < 15; i++)
            {

                estrellas += GameDataController.gameData.stars[j, i];
                if (GameDataController.gameData.stars[j, i] == 1)
                {
                    estrellasUno++;
                }
                else if (GameDataController.gameData.stars[j, i] == 2)
                {
                    estrellasDos++;
                }
                else if (GameDataController.gameData.stars[j, i] == 3)
                {
                    estrellasTres++;
                }
            }

        for (int i = 0; i < GameDataController.gameData.specialLevelsCompleted.Length; i++) if (GameDataController.gameData.specialLevelsCompleted[i]) estrellas++;

        textos[0].text = estrellas.ToString();


        textos[3].text = estrellasUno.ToString();

        textos[4].text = estrellasDos.ToString();

        textos[5].text = estrellasTres.ToString();



        uint puntosTotales = 0;
        foreach (uint i in GameDataController.gameData.playerRecords)
        {
            puntosTotales += i;
        }

        textos[1].text = puntosTotales.ToString();

        CargarPuntuaciones();
     
    }

    public void CargarPuntuaciones()
    {
        textos[2].text = GameDataController.gameData.diamondCurrency.ToString();
    }

	
}
